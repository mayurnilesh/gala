package com.nearme.smartbuy.firebasedata;

import android.content.Context;
import android.content.SharedPreferences;

import com.nearme.smartbuy.model.User;

public class SharedPreferenceHelper  {
    private static SharedPreferenceHelper instance = null;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static String SHARE_USER_INFO = "userinfo";
    private static String SHARE_KEY_NAME = "name";
    private static String SHARE_KEY_EMAIL = "contact";
    private static String SHARE_KEY_AVATA = "avata";
    private static String SHARE_KEY_UID = "uid";
    private static String SHARE_USER_NUM = "";



    private SharedPreferenceHelper() {}

    public static SharedPreferenceHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPreferenceHelper();
            preferences = context.getSharedPreferences(SHARE_USER_INFO, Context.MODE_PRIVATE);
            editor = preferences.edit();
        }
        return instance;
    }

    public void saveusercontact(String contact)
    {
        editor.putString(SHARE_USER_NUM, contact);
        editor.apply();
        editor.commit();
    }

    public void saveUserInfo(User user) {
        editor.putString(SHARE_KEY_NAME, user.name);
        editor.putString(SHARE_KEY_EMAIL, user.contact);
        editor.putString(SHARE_KEY_AVATA, user.avata);
        editor.putString(SHARE_KEY_UID, StaticConfig.UID);
        editor.apply();
    }

    public  void updateUID(String uid)
    {
        editor.putString(SHARE_KEY_UID, uid);
        editor.apply();

    }

    public User getUserInfo(){
        String userName = preferences.getString(SHARE_KEY_NAME, "");
        String email = preferences.getString(SHARE_KEY_EMAIL, "");
        String avatar = preferences.getString(SHARE_KEY_AVATA, "default");

        User user = new User();
        user.name = userName;
        user.contact = email;
        user.avata = avatar;
        user.latitude = "0";
        user.longitude = "0";

        return user;
    }

    public String getusercontact()
    {
        return preferences.getString(SHARE_USER_NUM, "");
    }

    public String getUID(){
        return preferences.getString(SHARE_KEY_UID, "");
    }

}


