package com.nearme.smartbuy.jobs;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.PostJobsSubCategoryAdapter;
import com.nearme.smartbuy.rest.supportedsubjobs;

import java.util.ArrayList;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class PostJobsSubCategoryFragment extends Fragment {

    View view;
    String TAG = "JobsAroundU";
    RecyclerView rec_list;
    Context mContext;
    boolean b = true;
    PostJobsSubCategoryAdapter apercuAdapter;
    ArrayList<supportedsubjobs> supportedsubjobsArrayList;

    public PostJobsSubCategoryFragment() {

    }

    public PostJobsSubCategoryFragment(ArrayList<supportedsubjobs> supportedsubjobsArrayList) {
        this.supportedsubjobsArrayList = supportedsubjobsArrayList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_postjobsubcategory, container, false);
        mContext = container.getContext();

        initialize();

        view.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content_main, new FragmentPostJob()).addToBackStack("");
                ft.commit();
            }
        });

        return view;
    }

    private void initialize() {

        rec_list = view.findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(mContext, 1);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new PostJobsSubCategoryAdapter(supportedsubjobsArrayList);
        rec_list.setAdapter(apercuAdapter);


    }

}
