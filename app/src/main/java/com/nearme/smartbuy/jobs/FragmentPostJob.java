package com.nearme.smartbuy.jobs;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.nearme.smartbuy.R;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class FragmentPostJob extends Fragment {

    View view;
    Context mContext;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_postjob, container, false);
        mContext = container.getContext();


        return view;
    }


}
