package com.nearme.smartbuy.jobs;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.JobsCategoryAdapter;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.supportedjobs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class JobsCategoryFragment extends Fragment {

    View view;
    String TAG = "JobsAroundU";
    RecyclerView rec_list;
    Context mContext;
    boolean b = true;
    JobsCategoryAdapter apercuAdapter;
    ArrayList<supportedjobs> stringArrayList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_jobcategory, container, false);
        mContext = container.getContext();

        initialize();
        getData();

        return view;
    }

    private void getData() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<supportedjobs[]> call = apiService.getsupportedjobs();
        call.enqueue(new Callback<supportedjobs[]>() {
            @Override
            public void onResponse(Call<supportedjobs[]> call, Response<supportedjobs[]> response) {
                supportedjobs[] jpr = response.body();
                for (int i = 0; i < jpr.length; i++) {
                    stringArrayList.add(jpr[i]);
                }
                apercuAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<supportedjobs[]> call, Throwable t) {
                Log.d(TAG, "onResponse: " + t.toString());
            }
        });
    }

    private void initialize() {


        rec_list = view.findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(mContext, 2);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new JobsCategoryAdapter(stringArrayList);
        rec_list.setAdapter(apercuAdapter);


    }

}
