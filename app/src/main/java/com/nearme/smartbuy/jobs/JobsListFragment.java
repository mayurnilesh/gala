package com.nearme.smartbuy.jobs;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.JobsListAdapter;
import com.nearme.smartbuy.adapter.JobsSubCategoryAdapter;

import java.util.ArrayList;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class JobsListFragment extends Fragment {

    View view;
    String TAG = "JobsAroundU";
    RecyclerView rec_list;
    Context mContext;
    boolean b = true;
    JobsListAdapter apercuAdapter;
    ArrayList<String> stringArrayList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_jobcategory, container, false);
        mContext = container.getContext();

        initialize();

        return view;
    }

    private void initialize() {

        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");
        stringArrayList.add("");

        rec_list = view.findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(mContext,1);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new JobsListAdapter(stringArrayList);
        rec_list.setAdapter(apercuAdapter);


    }

}
