package com.nearme.smartbuy.startup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.SlotAdapter;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.model.Merchantslot;
import com.nearme.smartbuy.model.SlotItem;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnExpDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthScrollListener;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.vo.DateData;

public class MerchantSlotActivity extends AppCompatActivity {

    RecyclerView recList;
    SlotAdapter slotAdapter;
    ArrayList<SlotItem> slotItemArrayList = new ArrayList<>();
    ArrayList<SlotItem> tmpArrayList = new ArrayList<>();
    private ExpCalendarView expCalendarView;
    private DateData selectedDate;
    private ImageView expandIV;
    TextView tv_year, tv_date;
    List<Merchantslot> merchantslotList = new ArrayList<>();
    private String merchantId;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_slot);
        merchantId = getIntent().getStringExtra("mid");
        Log.d("TAG", "onCreate: " + merchantId);

        initialization();
        calender();
        clicks();
    }

    @SuppressLint("SetTextI18n")
    private void calender() {
        Calendar calendar = Calendar.getInstance();
        selectedDate = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        tv_year.setText(calendar.get(Calendar.YEAR) + " " + getMonthName(calendar.get(Calendar.MONTH) + 1));
        tv_date.setText(selectedDate.getYear() + "/" + selectedDate.getMonth() + "/" + selectedDate.getDay());

        expandIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ifExpand) {
                    CellConfig.Month2WeekPos = CellConfig.middlePosition;
                    CellConfig.ifMonth = false;
                    expandIV.setImageResource(R.drawable.icon_arrow_down);
                    CellConfig.weekAnchorPointDate = selectedDate;
                    expCalendarView.shrink();
                } else {
                    CellConfig.Week2MonthPos = CellConfig.middlePosition;
                    CellConfig.ifMonth = true;
                    expandIV.setImageResource(R.drawable.icon_arrow_up);
                    expCalendarView.expand();
                }
                ifExpand = !ifExpand;
            }
        });

        expCalendarView.getMarkedDates().getAll().clear();
        expCalendarView.getMarkedDates().removeAdd();

        expCalendarView.setOnDateClickListener(new OnExpDateClickListener()).setOnMonthScrollListener(new OnMonthScrollListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onMonthChange(int year, int month) {
                tv_year.setText(year + " " + getMonthName(month));
            }

            @Override
            public void onMonthScroll(float positionOffset) {
//                Log.i("listener", "onMonthScroll:" + positionOffset);
            }
        });

    }

    @SuppressLint("SetTextI18n")
    private void clicks() {
        findViewById(R.id.back).setOnClickListener(v -> finish());
        expCalendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                selectedDate = date;
                tmpArrayList.clear();
                String _date = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
                Log.d("TAG", "onDateClick: " + _date);

                for (int i = 0; i < slotItemArrayList.size(); i++) {
                    if (slotItemArrayList.get(i).getDate().equalsIgnoreCase(_date)) {
                        tmpArrayList.add(slotItemArrayList.get(i));
                    }
                }
                slotAdapter.notifyDataSetChanged();
                tv_date.setText(selectedDate.getYear() + "/" + selectedDate.getMonth() + "/" + selectedDate.getDay());
            }
        });
    }

    private String getMonthName(int number) {
        switch (number) {
            case 1:
                return "JAN";

            case 2:
                return "FEB";

            case 3:
                return "MAR";

            case 4:
                return "APR";

            case 5:
                return "MAY";

            case 6:
                return "JUN";

            case 7:
                return "JUL";

            case 8:
                return "AUG";

            case 9:
                return "SEP";

            case 10:
                return "OCT";

            case 11:
                return "NOV";

            case 12:
                return "DEC";

            default:
                return "";
        }
    }

    private boolean ifExpand = false;

    private void initialization() {
        recList = findViewById(R.id.recList);
        tv_year = findViewById(R.id.tv_year);
        tv_date = findViewById(R.id.tv_date);
        pb = findViewById(R.id.pb);
        expandIV = (ImageView) findViewById(R.id.main_expandIV);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recList.setLayoutManager(linearLayoutManager);

        slotAdapter = new SlotAdapter(tmpArrayList, this::registerSlot);
        recList.setAdapter(slotAdapter);
        expCalendarView = ((ExpCalendarView) findViewById(R.id.calendar_exp));

        expCalendarView.shrink();
        CellConfig.ifMonth = false;
    }

    private void registerSlot(SlotItem slotItem) {

        final Dialog dialog = new Dialog(MerchantSlotActivity.this, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialogue_people);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText edtmerchant = dialog.findViewById(R.id.edtPeople);
        int diff = Integer.parseInt(slotItem.getMaxToken());
        TextView tvsubmit = dialog.findViewById(R.id.tvsubmit);
        tvsubmit.setOnClickListener(v -> {
            if (edtmerchant.getText().toString().equalsIgnoreCase("")) {
                edtmerchant.setError("Enter no of peoples");
                edtmerchant.requestFocus();
            } else if (Integer.parseInt(edtmerchant.getText().toString()) > 2) {
                edtmerchant.setError("Only 2 peoples are allowed");
                edtmerchant.requestFocus();
            } else if (Integer.parseInt(edtmerchant.getText().toString()) > diff) {
                edtmerchant.setError("Slots are full");
                edtmerchant.requestFocus();
            } else {
                dialog.dismiss();
                List<userdetails> temp = userdetails.listAll(userdetails.class);
                String phoneNumber = "";
                if (temp.size() > 0)
                    phoneNumber = temp.get(0).ContactNumber;

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<String> call = apiService.registerSlots(merchantId, phoneNumber, slotItem.getSlot_ID(),
                        slotItem.getEpochStarttime(), edtmerchant.getText().toString());
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.body() == null) {
                            Toast.makeText(MerchantSlotActivity.this, "technical error occurred.", Toast.LENGTH_SHORT).show();
                        } else if (response.body().equals("-1")) {
                            Toast.makeText(MerchantSlotActivity.this, "Slot id not available.", Toast.LENGTH_SHORT).show();
                        } else if (response.body().equals("-2")) {
                            Toast.makeText(MerchantSlotActivity.this, "Already registered for this slot.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MerchantSlotActivity.this, "Slot registered successfully.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("TAG", "onFailure: ");
                        Toast.makeText(MerchantSlotActivity.this, "Slot registered successfully", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                });
            }
        });

    }

    private void apiCall() {
        pb.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault());
        String date = dateFormat.format(calendar.getTime()) + " 00:00:00";

        calendar.add(Calendar.MONTH, 1);
        String afterDate = dateFormat.format(calendar.getTime()) + " 23:59:59";

        Log.d("TAG", "apiCall: " + date + " " + afterDate);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Merchantslot[]> call = apiService.getSlots(merchantId, date, afterDate);
        call.enqueue(new Callback<Merchantslot[]>() {
            @Override
            public void onResponse(Call<Merchantslot[]> call, Response<Merchantslot[]> response) {
                Log.d("TAG", "onResponse: successs");
                Merchantslot[] data = response.body();
                merchantslotList.clear();
                if (data != null) {
                    merchantslotList.addAll(Arrays.asList(data));
                    manageData();
                }
                pb.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Merchantslot[]> call, Throwable t) {
                Log.d("TAG", "onResponse: failure " + t.getMessage());
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void manageData() {
        slotItemArrayList.clear();

        for (int i = 0; i < merchantslotList.size(); i++) {
            Date fromDate = new Date(Long.parseLong(merchantslotList.get(i).FromTime) * 1000L);
            Date toDate = new Date(Long.parseLong(merchantslotList.get(i).ToTime) * 1000L);
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy", Locale.getDefault());
            SimpleDateFormat sdf2 = new SimpleDateFormat("MM", Locale.getDefault());
            SimpleDateFormat sdf3 = new SimpleDateFormat("dd", Locale.getDefault());
            SimpleDateFormat sdf4 = new SimpleDateFormat("HH", Locale.getDefault());
            SimpleDateFormat sdf5 = new SimpleDateFormat("mm", Locale.getDefault());
            sdf1.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf2.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf3.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf4.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf5.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            int year = Integer.parseInt(sdf1.format(fromDate));
            int month = Integer.parseInt(sdf2.format(fromDate));
            int day = Integer.parseInt(sdf3.format(fromDate));
            int hour = Integer.parseInt(sdf4.format(fromDate));
            int minute = Integer.parseInt(sdf5.format(fromDate));
            int hour_ = Integer.parseInt(sdf4.format(toDate));
            int minute_ = Integer.parseInt(sdf5.format(toDate));
//
            expCalendarView.markDate(new DateData(year, month, day)
                    .setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, Color.GREEN)));
            Log.d("TAG", "manageData: " + day);


            SlotItem slotItem = new SlotItem();
            slotItem.setMaxToken(String.valueOf(merchantslotList.get(i).MaxToken));
            slotItem.setFromTime(hour + ":" + (minute == 0 ? "00" : minute));
            slotItem.setToTime(hour_ + ":" + (minute_ == 0 ? "00" : minute_));
            slotItem.setDate(year + "-" + month + "-" + day);
            slotItem.setCurToken(String.valueOf(merchantslotList.get(i).CurToken));
            slotItem.setSlot_ID(String.valueOf(merchantslotList.get(i).Slot_ID));
            slotItem.setEpochStarttime(merchantslotList.get(i).FromTime);
            slotItemArrayList.add(slotItem);
        }
        tmpArrayList.clear();
        String _date = selectedDate.getYear() + "-" + selectedDate.getMonth() + "-" + selectedDate.getDay();
        for (int j = 0; j < slotItemArrayList.size(); j++) {
            if (slotItemArrayList.get(j).getDate().equalsIgnoreCase(_date)) {
                tmpArrayList.add(slotItemArrayList.get(j));
            }
        }
        slotAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                apiCall();
            }
        }, 2000);
    }

    public interface SlotCallback {
        public void callBack(SlotItem slotItem);
    }

}