package com.nearme.smartbuy.startup;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nearme.smartbuy.R;

public class WebviewActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        webView = findViewById(R.id.webview);

//        String myPdfUrl = "http://example.com/awesome.pdf";
//        String url = "http://docs.google.com/gview?embedded=true&url=" + myPdfUrl;
        // Log.i(TAG, "Opening PDF: " + url);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadUrl(getIntent().getStringExtra("link"));

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + getIntent().getStringExtra("link"));
        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
               // progressbar.setVisibility(View.GONE);
            }
        });


    }
}
