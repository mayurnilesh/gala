package com.nearme.smartbuy.startup;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;

import com.nearme.smartbuy.R;

public class ZoomImageActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_view);
        Bundle bundle = getIntent().getExtras();

        String imageUri = bundle.getString("imageURI");
        ImageView imageView =findViewById(R.id.expanded_image);
        imageView.setImageURI(Uri.parse(imageUri));
    }

}
