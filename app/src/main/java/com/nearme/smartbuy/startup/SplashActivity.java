package com.nearme.smartbuy.startup;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.versionclass;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SplashActivity extends Activity {
    Animation animFadeIn;
    LinearLayout linearLayout;
    ProgressDialog bar;
    Integer curVer = 0;
    String curVersion = "0";


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(() -> permissionCheck(), 1500);

    }

    private void permissionCheck() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CAMERA
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            setup();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(SplashActivity.this, "You need to approve all permission to use our features", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void setup() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
        }

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            curVersion = pInfo.versionName;
            String str = curVersion.substring(0, curVersion.indexOf("."));
            curVer = Integer.parseInt(str);
            getLatestVerionId();


        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
            startActivity(i);
            finish();//
        }

    }

    @Override
    public void onBackPressed() {
        this.finish();
        // super.onBackPressed();
    }


    public void getLatestVerionId() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<versionclass> call = apiService.getVersion();
        call.enqueue(new Callback<versionclass>() {
            @Override
            public void onResponse(Call<versionclass> call, Response<versionclass> response) {
                if (response == null || response.body() == null) {
                    Toast.makeText(SplashActivity.this, "Server is not responding. \n Launching the existing version of app \n . Expect some bugs , crashes", Toast.LENGTH_SHORT).show();
                } else {
                    versionclass latestversion = response.body();
                    if (latestversion == null)
                        return;
                    Integer latestVer = Integer.parseInt(latestversion.version);

                    if (latestVer > curVer) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=com.nearme.gala"));
                        startActivity(intent);

                    } else {
                        Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<versionclass> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(SplashActivity.this, "Failed with the following error \n " + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, t.toString());
                Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }
        });
    }


//


}


