package com.nearme.smartbuy.startup;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.DrawerRecycler;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.firebasedata.SharedPreferenceHelper;
import com.nearme.smartbuy.firebasedata.StaticConfig;
//import com.nearme.smartbuy.fragments.ChatFragment;
import com.nearme.smartbuy.fragments.ChatListActivity;
import com.nearme.smartbuy.fragments.ChoosenLocationFragment;
import com.nearme.smartbuy.fragments.ExclusiveFragment;
import com.nearme.smartbuy.fragments.FragmentContact;
import com.nearme.smartbuy.fragments.FragmentCovidHelp;
import com.nearme.smartbuy.fragments.FragmentFavourite;
import com.nearme.smartbuy.fragments.FragmentTerms;
import com.nearme.smartbuy.fragments.GroupOffersStatusFragment;
import com.nearme.smartbuy.fragments.JobsAroundU;
import com.nearme.smartbuy.fragments.NegotiationsFragment;
import com.nearme.smartbuy.fragments.OfferListFragment;
import com.nearme.smartbuy.fragments.Privacypolicy;
import com.nearme.smartbuy.fragments.Receiptfragment;
import com.nearme.smartbuy.fragments.RegisterSlotFragment;
import com.nearme.smartbuy.fragments.RegisteredSlotFragment;
import com.nearme.smartbuy.fragments.Tokenfragment;
import com.nearme.smartbuy.jobs.JobsCategoryFragment;
import com.nearme.smartbuy.jobs.PostJobsCategoryFragment;
import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.model.DrawerMainModel;
import com.nearme.smartbuy.model.DrawerSubItem;
import com.nearme.smartbuy.model.Status;
import com.nearme.smartbuy.utility.ConnectionDetector;
import com.nearme.smartbuy.utility.Constant;
import com.nearme.smartbuy.utility.PrefManager;
import com.nearme.smartbuy.utility.ServiceUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

//import com.nearme.smartbuy.fragments.PartiesAroundU;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static int notificationCountCart = 0;
    public String selectedFrag = "0";
    public static String phoneNumber = "";
    public static String name = "";
    public static String imageUrl = "";
    public static String DOB = "";
    public static String YOB = "";
    public static String MOB = "";
    public SimpleDraweeView mUserDp = null;
    public TextView mUserName;
    public TextView mPoints;
    public static MainActivity mMainActivity = null;
    FloatingActionButton fab;
    private FirebaseAuth mAuth;
    public static FirebaseUser user;
    TextView CustIDTextView, CustNoTextView, TimeTextView, socialTextView;
    ImageView imageView;
    public static String timeMSg = "";
    String TAG = "MainActivity";
    private PrefManager prefManager;
    ImageView side_menu;

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        prefManager = new PrefManager(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        side_menu = findViewById(R.id.side_menu);
        navigationView.setNavigationItemSelectedListener(this);
        CustIDTextView = navigationView.getHeaderView(0).findViewById(R.id.CustIDTextView);
        CustNoTextView = navigationView.getHeaderView(0).findViewById(R.id.CustNoTextView);
        socialTextView = navigationView.getHeaderView(0).findViewById(R.id.SocialTextView);
        TimeTextView = navigationView.getHeaderView(0).findViewById(R.id.TimeTextView);
        imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            timeMSg = "Good Morning  ";
//            Toast.makeText(this, "Good Morning", Toast.LENGTH_SHORT).show();
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            timeMSg = "Good Afternoon  ";
//            Toast.makeText(this, "Good Afternoon", Toast.LENGTH_SHORT).show();
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            timeMSg = "Good Evening  ";
//            Toast.makeText(this, "Good Evening", Toast.LENGTH_SHORT).show();
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            timeMSg = "Good Night  ";
//            Toast.makeText(this, "Good Night", Toast.LENGTH_SHORT).show();
        }
        TimeTextView.setText(timeMSg);

        List<userdetails> temp = new ArrayList<>();//
        try {
            temp = userdetails.listAll(userdetails.class);
        } catch (Exception ex) {
            Log.d(TAG, "onCreate: ");
        }

        if (temp.size() == 0) {
            Log.d(TAG, "onCreate: ");
        } else {

            phoneNumber = temp.get(0).ContactNumber;
            name = temp.get(0).Name;
            imageUrl = temp.get(0).ImageUrl;
            DOB = temp.get(0).DOB;
            MOB = temp.get(0).MOB;
            YOB = temp.get(0).YOB;

            CustIDTextView.setText(name);
            CustNoTextView.setText("Points :" + temp.get(0).points);
//            socialTextView.setText(temp.get(0).points);
//            Picasso.get().load(imageUrl).resize(100, 100)
//                    .centerInside().into(imageView);
            this.startService(new Intent(getApplicationContext(), MyLocationListener.class));

            Iterator<userdetails> obj = userdetails.findAll(userdetails.class);

            while (obj.hasNext()) {
                userdetails userdetailsobj = obj.next();
                CustIDTextView.setText(userdetailsobj.Name);
                String points = "Points: " + userdetailsobj.points.toString();
                CustNoTextView.setText(points);
            }

            mMainActivity = this;
            mAuth = FirebaseAuth.getInstance();
            user = mAuth.getCurrentUser();
            if (user != null) {
                StaticConfig.UID = user.getUid();
                ServiceUtils.Uid = user.getUid();
                SharedPreferenceHelper.getInstance(getBaseContext()).updateUID(user.getUid());
            } else {
                // mAuth.
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        FragmentManager fm1 = getSupportFragmentManager();
        FragmentTransaction ft1 = fm1.beginTransaction();

        SharedPreferences prefs = getSharedPreferences(Constant.appPref, 0);
        boolean isFirstLaunch = prefs.getBoolean(Constant.isFirst, true);
        if (isFirstLaunch) {
            ft1.replace(R.id.content_main, new OfferListFragment(), "OfferListFragment");
            showFavouriteDialog();
        } else {
            int fav = prefs.getInt(Constant.favType, 0);

            switch (fav) {
                case 1:
                    ft1.replace(R.id.content_main, new Tokenfragment(), "Tokenfragment");
                    break;
                case 2:
                    ft1.replace(R.id.content_main, new RegisterSlotFragment(), "RegisterSlotFragment");

                    break;
                case 3:
                    ft1.replace(R.id.content_main, new NegotiationsFragment(), "NegotiationsFragment");

                    break;
                case 4:
                    ft1.replace(R.id.content_main, new GroupOffersStatusFragment(), "GroupOffersStatusFragment");

                case 5:
                    ft1.replace(R.id.content_main, new FragmentCovidHelp(), "GroupOffersStatusFragment");

                    break;
                default:
                    ft1.replace(R.id.content_main, new OfferListFragment(), "OfferListFragment");

            }
        }

        ft1.commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
        Iterator<userdetails> obj = userdetails.findAll(userdetails.class);
        if (obj.hasNext()) {
            userdetails userdetailsobj = obj.next();
            String points = "Points: " + userdetailsobj.points.toString();
            CustNoTextView.setText(points);
            invalidateOptionsMenu();
        }

        checkInternetOrGps();
    }

    private void checkInternetOrGps() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!ConnectionDetector.isConnectingToInternet(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("Please Check your Internet Connection");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            if (!statusOfGPS) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Alert");
                builder.setMessage("Please Enable GPS");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
                builder.show();

            }
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        String id = item.getTitle().toString();
        switch (id) {

            case "Logout":
                prefManager.setFirstTimeLaunch(false);
                userdetails.deleteAll(userdetails.class);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                break;

            case "Digital Token":
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content_main, new Tokenfragment(), "TokenFragment");
                ft.commit();
                break;

            case "Digital Slots":
                changeFragment(new RegisterSlotFragment());
                break;

            case "Registered Slots":
                changeFragment(new RegisteredSlotFragment());
                break;

            case "Receipt":
                changeFragment(new Receiptfragment());
                break;

            case "Offers around You":
                FragmentManager fm1 = getSupportFragmentManager();
                FragmentTransaction ft1 = fm1.beginTransaction();
                ft1.replace(R.id.content_main, new OfferListFragment(), "OfferListFragment");
                ft1.commit();
                break;
            case "Jobs around You":
                changeFragment(new JobsCategoryFragment());
                break;
            case "Post Jobs":
                changeFragment(new PostJobsCategoryFragment());
                break;
            case "Exclusive for You":
                changeFragment(new ExclusiveFragment());
                break;

            case "Negotiations":
                changeFragment(new NegotiationsFragment());
                break;

            case "Offers in other locations":
                FragmentManager fm2 = getSupportFragmentManager();
                FragmentTransaction ft2 = fm2.beginTransaction();
                ft2.replace(R.id.content_main, new ChoosenLocationFragment(), "ChooseLocationFragment");
                ft2.commit();
                break;

            case "Covid Help Advertise":
                changeFragment(new FragmentCovidHelp());
                break;

            case "Available group offers status":
                changeFragment(new GroupOffersStatusFragment());
                break;
            case "Chat With Merchant":
                changeFragment(new ChatListActivity());
                break;
            case "Favourite":
                changeFragment(new FragmentFavourite());
                break;
//            case "Groups Around U":
//                changeFragment(new PartiesAroundU());
//                break;
//            case "Parties Around U":
//                changeFragment(new PartiesAroundU());
//                break;
            case "Contact Us":
                changeFragment(new FragmentContact());
                break;
            case "Terms & Conditions":
                changeFragment(new FragmentTerms());
                break;
            case "Privacy Policy":
                changeFragment(new Privacypolicy());
                break;

            default:
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changeFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_main, fragment);
        ft.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!Constant.isenable) {
            Tokenfragment announcementBizSendingFragment = (Tokenfragment) getSupportFragmentManager().findFragmentByTag("TokenFragment");
            if (announcementBizSendingFragment != null)
                announcementBizSendingFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            Constant.isenable = false;
            if (!Constant.isSearchother) {
                OfferListFragment announcementBizSendingFragment = (OfferListFragment) getSupportFragmentManager().findFragmentByTag("OfferListFragment");
                if (announcementBizSendingFragment != null)
                    announcementBizSendingFragment.onActivityResult(requestCode, resultCode, data);
            } else {
                ChoosenLocationFragment announcementBizSendingFragment = (ChoosenLocationFragment) getSupportFragmentManager().findFragmentByTag("ChooseLocationFragment");
                if (announcementBizSendingFragment != null)
                    announcementBizSendingFragment.onActivityResult(requestCode, resultCode, data);
            }
        }

    }

    private void showFavouriteDialog() {

        SharedPreferences prefs = getSharedPreferences(Constant.appPref, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constant.isFirst, false).apply();

        final Dialog dialog = new Dialog(this, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialogue_favourite_info);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        RadioGroup radioGroup = dialog.findViewById(R.id.rgFavourite);
        TextView tvSubmit = dialog.findViewById(R.id.submitBtn);

        tvSubmit.setOnClickListener(v -> {
            RadioButton checkedRadioButton = dialog.findViewById(radioGroup.getCheckedRadioButtonId());
            int idx = radioGroup.indexOfChild(checkedRadioButton);
            Log.d(TAG, "onClick: " + idx);
            editor.putInt(Constant.favType, idx).apply();
            dialog.dismiss();
        });

    }

}
