package com.nearme.smartbuy.startup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.ViewPagerAdapter;

public class IntroScreenActivity extends AppCompatActivity {

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        // Set an Adapter on the ViewPager
        mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));


        findViewById(R.id.linNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mViewPager.getCurrentItem() == 0){
                    mViewPager.setCurrentItem(1);
                }
                else if(mViewPager.getCurrentItem() == 1){
                    mViewPager.setCurrentItem(2);
                }
                else if(mViewPager.getCurrentItem() == 2){
                    mViewPager.setCurrentItem(3);
                }
                else{
                    startActivity(new Intent(IntroScreenActivity.this,LoginActivity.class));
                    finish();
                }
            }
        });

    }
}