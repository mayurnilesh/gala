package com.nearme.smartbuy.startup;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
//com.nearme.smartbuy.fragments
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.ChatAdapter;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.model.User;
import com.nearme.smartbuy.photedit.EditImageActivity;
import com.nearme.smartbuy.utility.ChatMessage;
import com.nearme.smartbuy.utility.MyMerchantPOJO;
import com.nearme.smartbuy.utility.PrefManager;
import com.nearme.smartbuy.utility.StorageUtil;

import java.io.ByteArrayOutputStream;
//import java.sql.Timestamp;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.collections.MapsKt;
import kotlin.jvm.functions.Function1;

//import com.google.firebase.Timestamp;
//import com.google.firebase.Timestamp;

import static android.provider.MediaStore.MediaColumns.DOCUMENT_ID;
//import com.google.firebase.firestore.CollectionReference;
//import com.google.type.DateTime;


public class ChatActivity extends Activity {
    private final int RC_SELECT_IMAGE = 2;
    //    View view;
    private static final String TAG = "ChatActivity";
    //    Context context;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore firestore;
    ArrayList<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
    ListenerRegistration chatRegistration = null;
    String roomId = "126";
    ImageView send_btn;
    EditText edittext_chat;
    RecyclerView recyleView;
    ChatAdapter adapter;
    ImageView fab_send_image;
    private DatabaseReference mDatabase;
    private PrefManager prefManager;
    String merchant_id;
    String UserName;
    ArrayList<HashMap<String, Object>> myMerchantPOJOs;
    String ImageUrl;
    TextView title;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_with_merchant);
        mAuth = FirebaseAuth.getInstance();
        click();
        user = mAuth.getCurrentUser();
        title = findViewById(R.id.title);
        prefManager = new PrefManager(this);
        Intent intent = getIntent();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        title.setText(intent.getStringExtra("name"));

        Bundle bundle = intent.getExtras();
        try {
            //ImageUri
            merchant_id = bundle.getString("name");
            ImageUrl = bundle.getString("ImageUri");
            if (merchant_id == null) {
                merchant_id = "default";
            }
            boolean tryToAdd = bundle.getBoolean("add");
            if (tryToAdd) {
                //Add to FireDB user_merchant
                Query query = mDatabase
                        .child(user.getUid() + "_merchants");
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        MyMerchantPOJO myMerchantPOJO = new MyMerchantPOJO();
                        HashMap<String, Object> myObjectAsDict = new HashMap<>();
                        if (ImageUrl != null) {
                            myMerchantPOJO.setImageref(ImageUrl);
                            myObjectAsDict.put("imageref",ImageUrl);
                        } else {//defualt image
                            myMerchantPOJO.setImageref("/sSzW2t6uYRWgL9oFZu1dCTX9vah2/messages/2144b07e-0cce-3286-80d1-64cd56684cd4");
                            myObjectAsDict.put("imageref","/sSzW2t6uYRWgL9oFZu1dCTX9vah2/messages/2144b07e-0cce-3286-80d1-64cd56684cd4");
                        }
                        myMerchantPOJO.setGuid(user.getUid());
                        myObjectAsDict.put("guid",user.getUid());
                        myMerchantPOJO.setName(merchant_id);
                        myObjectAsDict.put("name",merchant_id);

                        if (dataSnapshot.getChildrenCount() > 0) {
                            //username found
                            myMerchantPOJOs = (ArrayList<HashMap<String, Object>>) dataSnapshot.getValue();
                        } else {
                            // username not found
                            myMerchantPOJOs = new ArrayList<>();
                            mDatabase.child(user.getUid() + "_merchants").push();
                        }
                        boolean found =false;
                        for(int i=0;i<myMerchantPOJOs.size();i++)
                        {
                            if(myMerchantPOJOs.get(i).get("name").equals(myMerchantPOJO.getName()))
                            {
                                found =true;
                            }
                        }
                        if ((myMerchantPOJOs != null) && !found) {


                            myMerchantPOJOs.add(myObjectAsDict);

                            mDatabase.child(user.getUid() + "_merchants").setValue(myMerchantPOJOs);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
//                DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()+"_merchants");
                Query merquery = mDatabase
                        .child("MerchantUsers/"+merchant_id);
                merquery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        MyMerchantPOJO myMerchantPOJO = new MyMerchantPOJO();
                        HashMap<String, Object> myObjectAsDict = new HashMap<>();
                        List<userdetails> temp = userdetails.listAll(userdetails.class);
                        ImageUrl = temp.get(0).ImageUrl;
                        if (ImageUrl != null) {
                            myMerchantPOJO.setImageref(ImageUrl);//need to update to user image
                            myObjectAsDict.put("imageref",ImageUrl);
                        } else {//defualt image
                            myMerchantPOJO.setImageref("/sSzW2t6uYRWgL9oFZu1dCTX9vah2/messages/2144b07e-0cce-3286-80d1-64cd56684cd4");
                            myObjectAsDict.put("imageref","/sSzW2t6uYRWgL9oFZu1dCTX9vah2/messages/2144b07e-0cce-3286-80d1-64cd56684cd4");
                        }
                        myMerchantPOJO.setGuid(user.getUid());
                        myObjectAsDict.put("guid",user.getUid());
                        myMerchantPOJO.setName(temp.get(0).Name);

                        myObjectAsDict.put("name",temp.get(0).Name);

                        if (dataSnapshot.getChildrenCount() > 0) {
                            //username found
                            myMerchantPOJOs = (ArrayList<HashMap<String, Object>>) dataSnapshot.getValue();
                        } else {
                            mDatabase.child("MerchantUsers").child(merchant_id).push();//creating merchantID
                            myMerchantPOJOs = new ArrayList<>();
                        }
                        boolean found =false;
                        for(int i=0;i<myMerchantPOJOs.size();i++)
                        {
                            if(myMerchantPOJOs.get(i).get("name")!=null && myMerchantPOJOs.get(i).get("name").toString().equals(myMerchantPOJO.getName()))
                            {
                                found =true;
                            }
                        }
                        if (myMerchantPOJOs != null && !found) {
                            myMerchantPOJOs.add(myObjectAsDict);

                            mDatabase.child("MerchantUsers").child(merchant_id).setValue(myMerchantPOJOs);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
//        String merchant_id="merchant";
        UserName = user.getUid();
        roomId = merchant_id;
//
        firestore = FirebaseFirestore.getInstance();
        //firestore.collection("rooms");
        //button_send
        send_btn = (ImageView) findViewById(R.id.button_send);
        edittext_chat = findViewById(R.id.edittext_chat);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = edittext_chat.getText().toString();
                edittext_chat.setText("");
//                Long tsLong = System.currentTimeMillis()/1000;
//                String ts = tsLong.toString();
                Pair[] pairs = new Pair[]{new Pair("text", message),
                        new Pair("user", user.getUid()),
                        new Pair("timestamp", String.valueOf(System.currentTimeMillis() / 1000))};

                CollectionReference collectionReference = firestore.collection(roomId + "/" + UserName + "/messages");
//                collectionReference.document(UserName+"_"+user.getUid()).collection("messages");
//                        .document(roomId+"/"+UserName).collection("messages");
                collectionReference.add(MapsKt.mapOf(pairs));
            }
        });
        fab_send_image = findViewById(R.id.fab_send_image);
        fab_send_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/jpeg", "image/png"});
//                context.startActivity(intent);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), RC_SELECT_IMAGE);
            }
        });
        initList();
//        setViewListeners();
    }

    private void click() {
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private final void initList() {
        if (this.user != null) {
            recyleView = (RecyclerView) findViewById(R.id.list_chat);
            recyleView.setLayoutManager((RecyclerView.LayoutManager) (new LinearLayoutManager(getApplicationContext())));
            List chatList = (List) this.chatMessages;
            String userID = this.user.getUid();
            adapter = new ChatAdapter(chatList, userID);
            recyleView = (RecyclerView) findViewById(R.id.list_chat);
            recyleView.setAdapter((ChatAdapter) adapter);
            this.listenForChatMessages();
        }
    }

    @NotNull
    public final ArrayList getChatMessages() {
        return this.chatMessages;
    }

    private final void listenForChatMessages() {
//        this.roomId = this.getIntent().getStringExtra("INTENT_EXTRA_ROOMID");//TODO
        if (this.roomId == null) {
//            this.finish();
        } else {
            CollectionReference collreference = this.firestore.collection(roomId + "/" + UserName + "/messages");

            //main onedocument(UserName+"_"+user.getUid()).collection("messages")
            try {
                this.chatRegistration = collreference
                        .orderBy("timestamp").addSnapshotListener
                                (getEventListener());
            } catch (Exception ex) {

            }

        }
    }

    @NotNull
    private EventListener getEventListener() {

        try {
            return new EventListener() {

                // $FF: synthetic method
                // $FF: bridge method
                public void onEvent(Object var1, FirebaseFirestoreException var2) {
                    this.onEvent((QuerySnapshot) var1, var2);
                }

                public final void onEvent(@Nullable QuerySnapshot
                                                  messageSnapshot, @Nullable FirebaseFirestoreException exception) {
                    if (messageSnapshot != null && !messageSnapshot.isEmpty()) {
                        getChatMessages().clear();
                        Iterator var4 = messageSnapshot.getDocuments().iterator();
                        try {
                            while (var4.hasNext()) {
                                DocumentSnapshot messageDocument = (DocumentSnapshot) var4.next();
                                ArrayList chatMessages = getChatMessages();
                                ChatMessage collreference = new ChatMessage();
                                Object vartext = messageDocument.get("text");
                                String text = (String) vartext;

                                Object varUser = messageDocument.get("user");
                                String user = (String) varUser;

                                Object timestamp = messageDocument.get("timestamp");
                                if (timestamp == null) {
                                    throw new TypeCastException("null cannot be cast to non-null type java.util.Date");
                                }
                                Object imgArray = messageDocument.get("image");
                                if (imgArray == null) {
//----------------TODO handle properly--------
                                } else {
                                    collreference.setImage((String) imgArray);
                                }
                                try {
                                    collreference.setText(text);
                                    collreference.setUser(user);
                                    collreference.setTimestamp(String.valueOf(timestamp));
                                    chatMessages.add(collreference);
                                } catch (Exception ec) {

                                }
                            }
                        } catch (Exception ex) {

                        }
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }

                    }
                }

            };
        } catch (Exception ex) {
            return new EventListener() {
                @Override
                public void onEvent(@Nullable Object value, @Nullable FirebaseFirestoreException error) {

                }
            };
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in.
        // TODO: Add code to check if user is signed in.
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SELECT_IMAGE && resultCode == Activity.RESULT_OK &&
                data != null) {
            try {
                Uri selectedImagePath = data.getData();

                Bitmap selectedImageBmp = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), selectedImagePath);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

                selectedImageBmp.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                byte[] selectedImageBytes = outputStream.toByteArray();
                String message = edittext_chat.getText().toString();
                edittext_chat.setText("");

                Intent in = new Intent(ChatActivity.this, EditImageActivity.class);
                in.putExtra("image", selectedImagePath.toString());
                startActivityForResult(in,100001);

            } catch (Exception ex) {

            }
        }

        if(requestCode == 100001){
            Uri selectedImagePath = Uri.parse(data.getStringExtra("DATA"));

            Bitmap selectedImageBmp = null;
            try {
                selectedImageBmp = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), selectedImagePath);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            selectedImageBmp.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
            byte[] selectedImageBytes = outputStream.toByteArray();
            StorageUtil.INSTANCE.uploadMessageImage(selectedImageBytes, (Function1) (new Function1() {
                // $FF: synthetic method
                // $FF: bridge method
                public Object invoke(Object var1) {
                    this.invoke((String) var1);
                    return Unit.INSTANCE;
                }

                public final void invoke(@NotNull String imagePath) {
                    Pair[] pairs = new Pair[]{new Pair("text", ""),
                            new Pair("user", user.getUid()),
                            new Pair("timestamp", String.valueOf(System.currentTimeMillis() / 1000)),
                            new Pair("image", imagePath)};
                    CollectionReference collectionReference = firestore.collection(roomId + "/" + UserName + "/messages");
//                        collectionReference.document(UserName+"_"+user.getUid()).collection("messages");
                    collectionReference.add(MapsKt.mapOf(pairs));
                }
            }));
        }

        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);
    }
}
