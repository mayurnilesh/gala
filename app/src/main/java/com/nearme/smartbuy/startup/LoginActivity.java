package com.nearme.smartbuy.startup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.firebase.consumerFirebasepayload;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.consumerpayload;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Context curContext;
    EditText userNameTxt;
    TextView loginBtn;

    LinearLayout signUp;
    CallbackManager callbackManager;
    LoginButton loginButton;
    private String firebaseToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.login_activity);
        curContext = this.getApplicationContext();

        userNameTxt = findViewById(R.id.userNameTxt);
        loginBtn =findViewById(R.id.loginBtn);
        signUp =findViewById(R.id.signUp);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                firebaseToken = instanceIdResult.getToken();
                Log.d("Toen", "onSuccess: " + firebaseToken);
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(curContext, "Success "+loginResult.getAccessToken(), Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "onSuccess: "+loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(curContext, "Cancel", Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "cancel: ");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(curContext, "Error", Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "error: ");
                    }
                });


        fbCode();

        loginBtn.setOnClickListener(view -> {
            if (!userNameTxt.getText().toString().trim().equals("") && userNameTxt.length() == 10) {

                char number = userNameTxt.getText().toString().charAt(0);

                if (Integer.parseInt(Character.toString(number)) < 6) {
                    Toast.makeText(curContext, "Please Enter Correct Mobile Number.", Toast.LENGTH_SHORT).show();
                    return;
                }

                String enteredNumber = "+91" + userNameTxt.getText().toString();
                if(firebaseToken==null){
                    Toast.makeText(curContext, "Please wait we are trying to enable notification feature for you", Toast.LENGTH_SHORT).show();
                    return;

                }
                signInWithPhoneAuthCredential(enteredNumber);

            } else {
                Toast.makeText(curContext, "Please Enter Correct Mobile Number.", Toast.LENGTH_SHORT).show();
            }

        });

        signUp.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this,RegisterActivity.class)));

    }

    private void signInWithPhoneAuthCredential(final String number) {

        consumerFirebasepayload firebasepayload = new consumerFirebasepayload();
        firebasepayload.FirebaseInstanceID = firebaseToken;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<consumerpayload> call = apiService.LoginUser(number, new Gson().toJson(firebasepayload));
        call.enqueue(new Callback<consumerpayload>() {
            @Override
            public void onResponse(Call<consumerpayload> call, Response<consumerpayload> response) {
                if (response.body() != null && response.body().contact != null) {
                    userdetails.deleteAll(userdetails.class);
                    AuthCredential credential = EmailAuthProvider
                            .getCredential(number + "@smartbuy.com",number);

                    FirebaseAuth.getInstance().signInWithCredential(credential)
                            .addOnCompleteListener(LoginActivity.this, task -> {
                                if (task.isSuccessful()) {
                                    //Got user back
                                    startActivity(new Intent(curContext, PhoneActivity.class).putExtra("number", number));
                                }
                            });

                } else {
                    Toast.makeText(LoginActivity.this, "This number is not registered.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<consumerpayload> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Technical error occurred.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fbCode() {
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(curContext, "Success "+loginResult.getAccessToken(), Toast.LENGTH_SHORT).show();
                Log.d("TAG", "onSuccess: "+loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(curContext, "Cancel", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "cancel: ");
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(curContext, "Error", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "error: ");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 1001) {
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        }
    }


}

