package com.nearme.smartbuy.startup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.firebase.consumerFirebasepayload;
import com.nearme.smartbuy.firebasedata.SharedPreferenceHelper;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.consumerpayload;
import com.nearme.smartbuy.utility.PrefManager;

import java.util.concurrent.TimeUnit;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneActivity extends Activity implements
        View.OnClickListener {
    private OtpTextView otpTextView;
    private String enteredName;
    private String enteredNumber;
    private String dob = "01";
    private String mob = "01";
    private String yob = "1970";
    private PrefManager prefManager;
    private Button mResendButton;
    TextView mVerifyButton;
    private String userid;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private final String TAG = "PhoneActivity";
    private int selectedSex = 0;
    boolean isReg = false;
    private CountDownTimer cdt;
    private TextView counttime, tvContact;
    private ProgressBar spinner;
    private String firebaseToken,dateOfBirth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(com.nearme.smartbuy.R.layout.activity_verifycontact);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                firebaseToken = instanceIdResult.getToken();
            }
        });


        mAuth = FirebaseAuth.getInstance();


        isReg = false;
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        enteredNumber = bundle.getString("number");

        if (bundle.containsKey("isRegistraion")) {
            isReg = bundle.getBoolean("isRegistraion");
            enteredName = bundle.getString("name");
            selectedSex = bundle.getInt("selectedSex");
            dateOfBirth = bundle.getString("dob");
        }
        otpTextView = findViewById(com.nearme.smartbuy.R.id.otp_view);
        tvContact = findViewById(com.nearme.smartbuy.R.id.tvContact);
        tvContact.setText(enteredNumber);

        mVerifyButton = findViewById(com.nearme.smartbuy.R.id.button_verify_phone);
        mResendButton = findViewById(com.nearme.smartbuy.R.id.button_resend);
        spinner = findViewById(R.id.progressBar1);

        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);

        mVerifyButton.setEnabled(false);
        mResendButton.setEnabled(false);

        counttime = findViewById(R.id.counttime);
        counttime.setText("02:00");
        mAuth = FirebaseAuth.getInstance();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {

                if (cdt != null)
                    cdt.cancel();
                mVerifyButton.setEnabled(false);
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (cdt != null)
                    cdt.start();
                else {
                    cdt = new CountDownTimer(120000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            String time = "";
                            long lefttmr = millisUntilFinished / 1000;
                            if (lefttmr > 60) {
                                time = "Resend in 01:" + String.valueOf(lefttmr - 61);
                                mResendButton.setEnabled(false);
                                mResendButton.setVisibility(View.INVISIBLE);
                            } else {
                                time = "Resend in 00:" + String.valueOf(lefttmr - 1);
                                mResendButton.setEnabled(false);
                                mResendButton.setVisibility(View.INVISIBLE);
                                if (lefttmr < 1) {
                                    time = "Resend in 00:00";
                                    mResendButton.setEnabled(true);
                                    mResendButton.setVisibility(View.VISIBLE);
                                }
                            }
                            counttime.setText(String.valueOf(time));
                            //counter++;

                        }

                        @Override
                        public void onFinish() {
                            mResendButton.setVisibility(View.VISIBLE);
                        }
                    }.start();
                }
                counttime.setVisibility(View.VISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                spinner.setVisibility(View.GONE);
                Log.w(TAG, "onVerificationFailed" + e.getMessage());
                Toast.makeText(PhoneActivity.this, "This contact number is invalid", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Log.d(TAG, "onCodeSent:" + verificationId);
                cdt = new CountDownTimer(120000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        String time = "";
                        long lefttmr = millisUntilFinished / 1000;
                        if (lefttmr >= 60) {
                            time = "Resend in 01:" + String.valueOf(lefttmr - 61);
                            mResendButton.setEnabled(true);
                            mResendButton.setVisibility(View.INVISIBLE);
                        } else {
                            time = "Resend in 00:" + String.valueOf(lefttmr - 1);
                            mResendButton.setEnabled(false);
                            mResendButton.setVisibility(View.INVISIBLE);
                            if (lefttmr < 1) {
                                time = "Resend in 00:00";
                                mResendButton.setEnabled(true);
                                mResendButton.setVisibility(View.VISIBLE);
                                cdt.cancel();
                            }
                        }
                        counttime.setText(time);
                    }

                    @Override
                    public void onFinish() {
                        mResendButton.setVisibility(View.VISIBLE);
                    }
                }.start();
                mVerificationId = verificationId;
                mResendToken = token;
                mVerifyButton.setEnabled(true);
                spinner.setVisibility(View.GONE);
            }
        };

        startPhoneNumberVerification(enteredNumber);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        if (!isReg) {

            consumerFirebasepayload firebasepayload = new consumerFirebasepayload();
            firebasepayload.FirebaseInstanceID = firebaseToken;
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                        }
                    });
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<consumerpayload> call = apiService.LoginUser(enteredNumber, new Gson().toJson(firebasepayload));
            call.enqueue(new Callback<consumerpayload>() {
                @Override
                public void onResponse(Call<consumerpayload> call, Response<consumerpayload> response) {
                    if (response.body() != null && response.body().contact != null) {
                        final userdetails obj = new userdetails(response.body().idconsumers, response.body().contact, response.body().consumername, response.body().dob, mob, yob, response.body().sex, response.body().dpurl, 0, "0");
                        obj.save();
                        prefManager = new PrefManager(PhoneActivity.this);
                        prefManager.setFirstTimeLaunch(true);
                        startActivity(new Intent(PhoneActivity.this, MainActivity.class));
                    } else {
                        startActivity(new Intent(PhoneActivity.this, LoginActivity.class));
                        Toast.makeText(PhoneActivity.this, "Please Sign up First.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<consumerpayload> call, Throwable t) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    mVerifyButton.setEnabled(true);
                    mResendButton.setEnabled(true);

                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    spinner.setVisibility(View.GONE);
                    mVerifyButton.setVisibility(View.VISIBLE);
                    mResendButton.setVisibility(View.VISIBLE);
                }
            });
        } else {
            mAuth.createUserWithEmailAndPassword(enteredNumber + "@smartbuy.com", enteredNumber)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                        }
                    });
            mAuth.signInWithCredential(EmailAuthProvider
                    .getCredential(enteredNumber + "@smartbuy.com", enteredNumber))
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                initNewUserInfo();
                            } else {
                                Toast.makeText(PhoneActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    });
        }
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        try {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
        } catch (Exception e) {
            Log.d(TAG, "startPhoneNumberVerification: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_verify_phone:
                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                String code = otpTextView.getOtp();
                if (TextUtils.isEmpty(otpTextView.getOtp())) {
                    Toast.makeText(PhoneActivity.this, "Please enter OTP", Toast.LENGTH_SHORT).show();
                    return;
                }
                mVerifyButton.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);
                verifyPhoneNumberWithCode(mVerificationId, code);

                break;
            case R.id.button_resend:
                mVerifyButton.setEnabled(false);
                mResendButton.setEnabled(false);
                spinner.setVisibility(View.GONE);
                resendVerificationCode(enteredNumber, mResendToken);
                break;
        }

    }


    public void initNewUserInfo() {

        consumerFirebasepayload firebasepayload = new consumerFirebasepayload();
        firebasepayload.FirebaseInstanceID = firebaseToken;

        consumerpayload payload = new consumerpayload();
        payload.consumername = enteredName;
        payload.contact = enteredNumber;
        payload.dob = dateOfBirth;
        payload.dpurl = "";
        payload.sex = selectedSex;
        payload.FirebaseDetails = firebasepayload;
        String payLoadStr = new Gson().toJson(payload);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<String> call = apiService.RegUser(payLoadStr);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                userid = response.body();
                final userdetails obj = new userdetails(userid, enteredNumber, enteredName, dob, mob, yob, selectedSex, "", 0, "0");
                obj.save();
                SharedPreferenceHelper.getInstance(PhoneActivity.this).saveusercontact("registered");
                spinner.setVisibility(View.GONE);
                startActivity(new Intent(PhoneActivity.this, MainActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(PhoneActivity.this, "Technical error", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}


