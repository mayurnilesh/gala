package com.nearme.smartbuy.startup;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.utility.PrefManager;

import java.util.List;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PrefManager prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
        }
        else {
            try {
                List<userdetails> temp = userdetails.listAll(userdetails.class);
                if(temp.size()<=0) {
                    startActivity(new Intent(WelcomeActivity.this, IntroScreenActivity.class));
                }
                else
                {
                    startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                }
                finish();
            } catch (Exception ex) {

                launchHomeScreen();
                finish();
            }
        }
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);

    }


    private void launchHomeScreen() {
        startActivity(new Intent(WelcomeActivity.this, IntroScreenActivity.class));
        finish();
    }

}
