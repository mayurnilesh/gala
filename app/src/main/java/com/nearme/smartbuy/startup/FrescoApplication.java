package com.nearme.smartbuy.startup;

import android.content.Context;
import androidx.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.nearme.smartbuy.cache.ImagePipelineConfigFactory;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.orm.SugarApp;

/**
 * Created by 06peng on 2015/6/24.
 */
public class FrescoApplication extends SugarApp {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        Fresco.initialize(this, ImagePipelineConfigFactory.getImagePipelineConfig(this));
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
