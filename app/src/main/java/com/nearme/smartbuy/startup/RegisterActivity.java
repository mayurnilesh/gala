package com.nearme.smartbuy.startup;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.firebase.consumerFirebasepayload;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.consumerpayload;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText userNameTxt;
    private EditText userName;
    private Spinner spGender;
    private String firebaseToken;
    private ProgressBar pb;
    private LinearLayout login;
    private TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        userNameTxt = findViewById(R.id.mbno);
        pb = findViewById(R.id.pb);
        userName = findViewById(R.id.name);
        login = findViewById(R.id.login);
        spGender = findViewById(R.id.spgender);
        tvDate = findViewById(R.id.tvDate);
        findViewById(R.id.signup).setOnClickListener(view -> {

            if (userName.getText().toString().trim().isEmpty()) {
                Toast.makeText(RegisterActivity.this, "Enter Name", Toast.LENGTH_SHORT).show();
                return;
            }

            if (userNameTxt.getText().toString().length() == 10) {
                if(tvDate.getText().toString().equalsIgnoreCase("Select Date Of Birth")){
                    Toast.makeText(RegisterActivity.this, "Please select DOB.", Toast.LENGTH_SHORT).show();
                    return;
                }
                String enteredNumber = "+91" + userNameTxt.getText().toString();
                userdetails.deleteAll(userdetails.class);
                pb.setVisibility(View.VISIBLE);
                checkALreadyLogin(enteredNumber);
            } else {
                Toast.makeText(RegisterActivity.this, "Please Enter Correct Mobile Number.", Toast.LENGTH_SHORT).show();
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            firebaseToken = instanceIdResult.getToken();
            Log.d("Toen", "onSuccess: " + firebaseToken);
        });

        login.setOnClickListener(v -> finish());

        findViewById(R.id.rlDob).setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.datepicker, (view, year1, month1, dayOfMonth) -> tvDate.setText(dayOfMonth + ":" + (month1 + 1) + ":" + year1), year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });

    }

    private void checkALreadyLogin(final String number) {

        consumerFirebasepayload firebasepayload = new consumerFirebasepayload();
        firebasepayload.FirebaseInstanceID = firebaseToken;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<consumerpayload> call = apiService.LoginUser(number, new Gson().toJson(firebasepayload));
        call.enqueue(new Callback<consumerpayload>() {
            @Override
            public void onResponse(Call<consumerpayload> call, Response<consumerpayload> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null && response.body().contact != null) {
                    Toast.makeText(RegisterActivity.this, "This number is already registered. please try to Login", Toast.LENGTH_SHORT).show();
                } else {
                    Intent PIntent = new Intent(RegisterActivity.this, PhoneActivity.class);
                    PIntent.putExtra("isRegistraion", true);
                    PIntent.putExtra("number", number);
                    PIntent.putExtra("name", userName.getText().toString());
                    PIntent.putExtra("selectedSex", spGender.getSelectedItemPosition() + 1);
                    PIntent.putExtra("dob", tvDate.getText().toString());
                    startActivity(PIntent);
                }
            }

            @Override
            public void onFailure(Call<consumerpayload> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Technical error ocuured.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}