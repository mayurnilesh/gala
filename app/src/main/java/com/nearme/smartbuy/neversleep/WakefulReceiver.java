package com.nearme.smartbuy.neversleep;

import android.content.Context;
import android.content.Intent;
import androidx.legacy.content.WakefulBroadcastReceiver;

import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.peoplearound.peoplearoundservice;

public class WakefulReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, MyLocationListener.class);
        startWakefulService(context,service);
        service = new Intent(context,peoplearoundservice.class);
        startWakefulService(context,service);
    }
}
