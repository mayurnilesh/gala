package com.nearme.smartbuy;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.JobPayLoad;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobsAroundForm extends AppCompatActivity {

    TextView edtlocationurl, etdate, submit;
    String TAG = "JobsAroundForm";
    RadioGroup rggender;
    ProgressBar pb;
    Spinner sp_education;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_around_form);


        initialize();
        etdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(JobsAroundForm.this, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etdate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

            }
        });

    }

    private boolean checkemail(EditText et) {
        String email = et.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern)) {
            return true;
        }
        et.setError("Enter correct email");
        et.requestFocus();
        return false;
    }

    private void initialize() {
        edtlocationurl = findViewById(R.id.edtlocationurl);
        sp_education = findViewById(R.id.sp_education);
        pb = findViewById(R.id.pb);
        rggender = findViewById(R.id.rggender);
        etdate = findViewById(R.id.etdate);
        submit = findViewById(R.id.submit);
        edtlocationurl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lat = String.valueOf(MyLocationListener.mCurrentLocation.getLatitude());
                String lng = String.valueOf(MyLocationListener.mCurrentLocation.getLongitude());
                String mapurl = "https://www.google.com/maps/?q=" + lat + "," + lng;

                edtlocationurl.setText(mapurl);

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });

        findViewById(R.id.edttiming).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
                final Calendar c = Calendar.getInstance();

                TimePickerDialog timePickerDialog = new TimePickerDialog(JobsAroundForm.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ((TextView) findViewById(R.id.edttiming)).setText(hourOfDay + ":" + minute);
                    }
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);

                timePickerDialog.show();
            }
        });
    }

//

    public void onSubmit() {

        if (checkEditText((EditText) findViewById(R.id.edtname), "Enter employee name") &&
                checkTextView((TextView) findViewById(R.id.edtlocationurl), "Select Location") &&
                checkEditText((EditText) findViewById(R.id.edtdesc), "Enter eJOb Description") &&
                checkEditText((EditText) findViewById(R.id.edtlandmark), "Enter Landmark") &&
                checkEditText((EditText) findViewById(R.id.edtpost), "Enter Post") &&
                checkEditText((EditText) findViewById(R.id.edtexp), "Enter experience") &&
                checkEditText((EditText) findViewById(R.id.edtagelimit), "Enter age limit") &&
                checkEditText((EditText) findViewById(R.id.edtnumber), "Enter contact number") &&
                checkEditText((EditText) findViewById(R.id.edtemail), "Enter email") &&
                checkemail((EditText) findViewById(R.id.edtemail)) &&
                checkTextView((TextView) findViewById(R.id.etdate), "Select Date") &&
                checkEditText((EditText) findViewById(R.id.edtsalary), "Enter salary")) {

            pb.setVisibility(View.VISIBLE);
            String sex = "";
            if (rggender.getCheckedRadioButtonId() == R.id.male) {
                sex = "male";
            } else {
                sex = "female";
            }


            JobPayLoad jobPayLoad = new JobPayLoad();
            jobPayLoad.geoHash = GeoHash.fromCoordinates(MyLocationListener.mCurrentLocation.getLatitude(), MyLocationListener.mCurrentLocation.getLongitude(), 5).toString();
            jobPayLoad.employername = ((EditText) findViewById(R.id.edtname)).getText().toString();
            jobPayLoad.employerlocationurl = ((TextView) findViewById(R.id.edtlocationurl)).getText().toString();
            jobPayLoad.jobDescription = ((EditText) findViewById(R.id.edtdesc)).getText().toString();
            jobPayLoad.locationLandmark = ((EditText) findViewById(R.id.edtlandmark)).getText().toString();
            jobPayLoad.offeringpost = ((EditText) findViewById(R.id.edtpost)).getText().toString();
            jobPayLoad.educationQualification = sp_education.getSelectedItem().toString();
            jobPayLoad.experienceReq = ((EditText) findViewById(R.id.edtexp)).getText().toString();
            jobPayLoad.sex = sex;
            jobPayLoad.ageLimitation = ((EditText) findViewById(R.id.edtagelimit)).getText().toString();
            jobPayLoad.contact = ((EditText) findViewById(R.id.edtnumber)).getText().toString();
            jobPayLoad.emailId = ((EditText) findViewById(R.id.edtemail)).getText().toString();
            jobPayLoad.interviewDate = ((TextView) findViewById(R.id.etdate)).getText().toString();
            jobPayLoad.shiftTimings = ((TextView) findViewById(R.id.edttiming)).getText().toString();
            jobPayLoad.salary = ((EditText) findViewById(R.id.edtsalary)).getText().toString();
            jobPayLoad.lat = String.valueOf(MyLocationListener.mCurrentLocation.getLatitude());
            jobPayLoad.lng = String.valueOf(MyLocationListener.mCurrentLocation.getLongitude());
            jobPayLoad.postedon = "abcd";

            Gson gson = new Gson();
            String jsonObject = gson.toJson(jobPayLoad);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<String> call = apiService.getPushJob(jsonObject);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.d(TAG, "onResponse: " + response.code());
                    pb.setVisibility(View.GONE);
                    finish();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.d(TAG, "onResponse: " + t.getMessage());
                    pb.setVisibility(View.GONE);
                    finish();
                }
            });

        }

    }

    private boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkTextView(TextView et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

}
