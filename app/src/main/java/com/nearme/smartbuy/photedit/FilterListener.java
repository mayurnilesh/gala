package com.nearme.smartbuy.photedit;

import ja.burhanrashid52.photoeditor.PhotoFilter;

public interface FilterListener {
    void onFilterSelected(PhotoFilter photoFilter);
}