package com.nearme.smartbuy.product;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.maxadidreceived;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.fragments.ImageListFragment;
import com.nearme.smartbuy.notification.NotificationCountSetClass;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.startup.MainActivity;
import com.nearme.smartbuy.utility.ImageUrlUtils;
import com.nearme.smartbuy.utility.PointsClass;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class ItemDetailsActivity extends AppCompatActivity {
    int imagePosition;
    String stringImageUri;
    String validTill;// = adValues.get(position).validTill;
    String offercode;// = adValues.get(position).offerCode;
    String shopName;// = adValues.get(position).shopName;
    String locUrl;// = adValues.get(position).location;
    String offerDescription;
    Integer viewType = 0;
    public static Activity curActivity = null;


    ImageButton mImageView;
    TextView textViewAddToCart;
    TextView textViewBuyNow;
    TextView textViewshopName;
    TextView textViewOfferCode;
    TextView textViewValidTill;
    TextView textViewOfferDescription;
    //        final EditText topicName = (EditText)findViewById(R.id.topicName);
//        final EditText topicMsg = (EditText)findViewById(R.id.topicMessage);
    ImageButton scanBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        mImageView = findViewById(R.id.detailedimage1);
        textViewAddToCart = (TextView) findViewById(R.id.text_action_bottom1);
        textViewBuyNow = (TextView) findViewById(R.id.text_action_bottom2);
        textViewshopName = (TextView) findViewById(R.id.idItemDetailsShopName);
        textViewOfferCode = (TextView) findViewById(R.id.idItemDetailsOfferCode);
        textViewValidTill = (TextView) findViewById(R.id.idItemDetailsOfferValidTill);
        textViewOfferDescription = (TextView) findViewById(R.id.idItemDetailOfferDescription);
//        final EditText topicName = (EditText)findViewById(R.id.topicName);
//        final EditText topicMsg = (EditText)findViewById(R.id.topicMessage);
        scanBtn = (ImageButton) findViewById(R.id.scan);
        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IntentIntegrator integrator = new IntentIntegrator(curActivity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode for points");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(true);
                new IntentIntegrator(curActivity).initiateScan(); // `this` is the current Activity

            }
        });


        ImageButton locationBtn = (ImageButton) findViewById(R.id.ic_wishlist);

        //Getting image uri from previous screen
        Intent intent = getIntent();
//        intent = getIntent();
        if (intent.hasExtra(ImageListFragment.STRING_IMAGE_URI) == true) {
            stringImageUri = intent.getStringExtra(ImageListFragment.STRING_IMAGE_URI);
            imagePosition = intent.getIntExtra(ImageListFragment.STRING_IMAGE_URI, 0);
            validTill = intent.getStringExtra(ImageListFragment.STRING_OFFER_VALIDTILL);
            offercode = intent.getStringExtra(ImageListFragment.STRING_OFFER_CODE);
            shopName = intent.getStringExtra(ImageListFragment.STRING_SHOP_NAME);
            locUrl = intent.getStringExtra(ImageListFragment.STRING_SHOP_LOCATION);
            offerDescription = intent.getStringExtra(ImageListFragment.STRING_OFFER_DESCRIPTION);
            String viewTypeStr = intent.getStringExtra(ImageListFragment.STRING_VIEW_TYPE);
            viewType = Integer.valueOf(viewTypeStr);
            if (viewType == 3) {
                textViewAddToCart.setText("ADD TO CART");
                textViewBuyNow.setText("BUY NOW");
            }
        }
        Uri uri = Uri.parse(stringImageUri);
       // mImageView.setImageURI(uri);
        Glide.with(this)
                .load(uri)
                .into(mImageView);
        textViewOfferDescription.setText(offerDescription);
        textViewshopName.setText(shopName);
        textViewOfferCode.setText(offercode);
        textViewValidTill.setText(validTill);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Intent intent = new Intent(ItemDetailsActivity.this, ViewPagerActivity.class);
//                    intent.putExtra("position", imagePosition);
//                    startActivity(intent);
                //   new firebaseAsyncMsgPush().execute();


            }
        });


        textViewAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
                imageUrlUtils.addCartListImageUri(stringImageUri);
                Toast.makeText(ItemDetailsActivity.this, "Item added to cart.", Toast.LENGTH_SHORT).show();
                MainActivity.notificationCountCart++;
                NotificationCountSetClass.setNotifyCount(MainActivity.notificationCountCart);
            }
        });

        textViewBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
//                imageUrlUtils.addCartListImageUri(stringImageUri);
//                MainActivity.notificationCountCart++;
//                NotificationCountSetClass.setNotifyCount(MainActivity.notificationCountCart);
//                startActivity(new Intent(ItemDetailsActivity.this, CartListActivity.class));

                Uri uri = Uri.parse(locUrl);
                Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW, uri);

                startActivity(mapIntent);

            }
        });

        curActivity = this;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final ItemDetailsActivity curActivity = this;

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {

            if (result.getContents() != null) {
                String msg = result.getContents();
                if(!(msg != null && msg.length() > 0))
                {
                    new AlertDialog.Builder(curActivity)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(@NonNull DialogInterface dialog, int which) {
                                    curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                    curActivity.finish();
                                }
                            })
                            .setCancelable(false)
                            .setMessage("Failed to scan. contact customer support")
                            .show();                    return;
                }
                String merchantId = msg.substring(0, msg.indexOf(":"));
                if(!(merchantId != null && merchantId.length() > 0))
                {
                    new AlertDialog.Builder(curActivity)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(@NonNull DialogInterface dialog, int which) {
                                    curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                    curActivity.finish();
                                }
                            })
                            .setCancelable(false)
                            .setMessage("Failed to scan. Invalid merchant jobid")
                            .show();                    return;
                }
                String offCode = msg.substring(msg.indexOf(":")+1);
                if(!(offCode != null && offCode.length() > 0))
                {
                    new AlertDialog.Builder(curActivity)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(@NonNull DialogInterface dialog, int which) {
                                    curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                    curActivity.finish();
                                }
                            })
                            .setCancelable(false)
                            .setMessage("Failed to scan. Invalid offer code")
                            .show();
                    return;
                }
                if (offCode.equals(textViewOfferCode.getText()) == false)
                {
                    new AlertDialog.Builder(curActivity)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(@NonNull DialogInterface dialog, int which) {
                                    curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                    curActivity.finish();
                                }
                            })
                            .setCancelable(false)
                            .setMessage("Failed to scan.Invalid offer code")
                            .show();
                }
                else {

                    List<userdetails> temp = userdetails.listAll(userdetails.class);
                    final String phoneNumber = temp.get(0).ContactNumber;

                    ApiInterface apiService =
                            ApiClient.getClient().create(ApiInterface.class);

                    final Gson gson = new Gson();

                    String strmerchantId = gson.toJson(merchantId);
                    String strphoneNumber = gson.toJson(phoneNumber);
                    String stroffercode = gson.toJson(offCode);


                    Call<PointsClass> call = apiService.updateTransaction(
                            strmerchantId, strphoneNumber, stroffercode);
                    call.enqueue(new Callback<PointsClass>() {
                        @Override
                        public void onResponse(Call<PointsClass> call, retrofit2.Response<PointsClass> response) {

                            if(response== null || response.body()==null)
                                new AlertDialog.Builder(curActivity)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(@NonNull DialogInterface dialog, int which) {
                                                curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                                curActivity.finish();
                                            }
                                        })
                                        .setCancelable(false)
                                        .setMessage("PLease retry to scan. Some network failure")
                                        .show();

                            userdetails adObj = (maxadidreceived.find(userdetails.class, "contact_number = ?", phoneNumber).get(0));

                            PointsClass pointsObj = response.body();
                            int receivedPoints = pointsObj.existingpoints;
                            adObj.points = adObj.points + receivedPoints;
                            adObj.save();

                            String str = "Congrats!!!. You got "+receivedPoints+" for this transaction. \n Total points in your account" +
                                    " = "+adObj.points;

                            new AlertDialog.Builder(curActivity)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(@NonNull DialogInterface dialog, int which) {
                                            curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                            curActivity.finish();
                                        }
                                    })
                                    .setCancelable(false)
                                    .setMessage(str)
                                    .show();



                        }

                        @Override
                        public void onFailure(Call<PointsClass> call, Throwable t) {
                            new AlertDialog.Builder(curActivity)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(@NonNull DialogInterface dialog, int which) {
                                            curActivity.startActivity(new Intent(curActivity, MainActivity.class));
                                            curActivity.finish();
                                        }
                                    })
                                    .setCancelable(false)
                                    .setMessage("Failed to scan")
                                    .show();
                            return;
                        }

                    });
//cancel

                }

            } else {

                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
