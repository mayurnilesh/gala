package com.nearme.smartbuy.model;

/**
 * Created by gopikomanduri on 05/05/18.
 */

public class Group extends Room{
    public String id;
    public String email;
    public String roomId;
    public ListFriend listFriend;

    public Group(){
        listFriend = new ListFriend();
    }
}
