package com.nearme.smartbuy.model;

public class SlotItem {
    String fromTime;
    String toTime;
    String date;
    String maxToken;
    String totalToken;
    String Slot_ID;
    String CurToken;
    String EpochStarttime;

    public String getEpochStarttime() {
        return EpochStarttime;
    }

    public void setEpochStarttime(String epochStarttime) {
        EpochStarttime = epochStarttime;
    }

    public String getSlot_ID() {
        return Slot_ID;
    }

    public void setSlot_ID(String slot_ID) {
        Slot_ID = slot_ID;
    }

    public String getCurToken() {
        return CurToken;
    }

    public void setCurToken(String curToken) {
        CurToken = curToken;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMaxToken() {
        return maxToken;
    }

    public void setMaxToken(String maxToken) {
        this.maxToken = maxToken;
    }

    public String getTotalToken() {
        return totalToken;
    }

    public void setTotalToken(String totalToken) {
        this.totalToken = totalToken;
    }
}
