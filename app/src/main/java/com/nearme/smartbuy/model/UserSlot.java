package com.nearme.smartbuy.model;

public class UserSlot {
    public int id;
    public String MerchantId;
    public String MerchantName;
    public String MerchantLat;
    public String MerchantLng;
    public String selectedSlotStartHash;
    public String selectedSlotEndHash;
    public int tokensRequested;
}