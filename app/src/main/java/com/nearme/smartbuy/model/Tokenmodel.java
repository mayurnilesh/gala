package com.nearme.smartbuy.model;

public class Tokenmodel {

    String merchantid, token, merchanturl, merchantname,position;

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMerchanturl() {
        return merchanturl;
    }

    public void setMerchanturl(String merchanturl) {
        this.merchanturl = merchanturl;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
