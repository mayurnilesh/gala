package com.nearme.smartbuy.model;

/**
 * Created by gopikomanduri on 05/05/18.
 */

public class User {
    public String name;
    public String contact;
    public String avata;
    public Integer sex;
    public Status status;
    public Message message;
    public String latitude;
    public String longitude;


    public User(){
        status = new Status();
        message = new Message();
        status.isOnline = false;
        status.timestamp = 0;
        message.idReceiver = "0";
        message.idSender = "0";
        message.text = "";
        message.timestamp = 0;
        latitude = "0";
        longitude = "0";
        sex = 1; // 0 is male.
    }
}
