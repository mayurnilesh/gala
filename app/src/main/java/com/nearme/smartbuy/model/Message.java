package com.nearme.smartbuy.model;

/**
 * Created by gopikomanduri on 05/05/18.
 */

public class Message{
    public String idSender;
    public String idReceiver;
    public String text;
    public long timestamp;
}