package com.nearme.smartbuy.model;

public class DrawerSubItem {

    public DrawerSubItem(String name, String parentName, String id) {
        this.name = name;
        this.parentName = parentName;
        this.id = id;
    }

   public DrawerSubItem(String name, String parentName) {
        this.name = name;
        this.parentName = parentName;

    }

    public String name;
    public String parentName;
    public String id;
}
