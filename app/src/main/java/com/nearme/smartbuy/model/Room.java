package com.nearme.smartbuy.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gopikomanduri on 05/05/18.
 */

public class Room {
    public ArrayList<String> member;
    public Map<String, String> groupInfo;

    public Room(){
        member = new ArrayList();
        groupInfo = new HashMap<String, String>();
    }
}
