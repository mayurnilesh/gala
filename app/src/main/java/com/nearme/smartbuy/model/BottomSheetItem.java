package com.nearme.smartbuy.model;

public class BottomSheetItem {
    public String name;
    public boolean isSelected = true;

    public BottomSheetItem(String name) {
        this.name = name;
    }
}
