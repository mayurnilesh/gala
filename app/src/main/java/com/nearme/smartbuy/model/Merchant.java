package com.nearme.smartbuy.model;

import com.orm.SugarRecord;

public class Merchant extends SugarRecord {
    public String merchantName;
    public String merchantPhn;
    public String merchantId;
    public String latitude;
    public String longitude;
    public String geoHash;
    public double distance;
    public String category;

    public Merchant(Merchant merchant)
    {
       this.merchantName= merchant.merchantName;
       this.merchantPhn= merchant.merchantPhn;
       this.merchantId= merchant.merchantId;
       this.latitude= merchant.latitude;
       this.longitude= merchant.longitude;
       this.geoHash= merchant.geoHash;
       this.distance= merchant.distance;
       this.category= merchant.category;
    }
    public  Merchant(){

    }
}
