package com.nearme.smartbuy.utility;

import java.util.List;

public class UserStatsPOJO {

    public int getFemaleCount() {
        return _fcount;
    }

    public void SetfemaleCount(int fcount) {
        this._fcount = fcount;
    }

    int _fcount;


    public int getMaleCount() {
        return _mcount;
    }

    public void SetmaleCount(int mcount) {
        this._mcount = mcount;
    }

    int _mcount;

    public int getBdCount() {
        return _bdcount;
    }

    public void SetbdayCount(int bdcount) {
        this._bdcount = bdcount;
    }

    int _bdcount;

    public List<String> getUsers() {
        return _users;
    }

    public void SetUsers(List<String> users) {
        this._users = users;
    }

    List<String> _users;
}
