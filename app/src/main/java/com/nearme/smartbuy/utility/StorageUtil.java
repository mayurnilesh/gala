package com.nearme.smartbuy.utility;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import java.util.UUID;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

public final class StorageUtil {
    public static final StorageUtil INSTANCE;

    private final FirebaseStorage getStorageInstance() {
        return FirebaseStorage.getInstance();
    }

    private final StorageReference getCurrentUserRef() {
        StorageReference var10000 = this.getStorageInstance().getReference();
        FirebaseAuth var10001 = FirebaseAuth.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(var10001, "FirebaseAuth.getInstance()");
        FirebaseUser var1 = var10001.getCurrentUser();
        if (var1 != null) {
            String var2 = var1.getUid();
            if (var2 != null) {
                var10000 = var10000.child(var2);
                Intrinsics.checkExpressionValueIsNotNull(var10000, "storageInstance.referenc…xception(\"UID is null.\"))");
                return var10000;
            }
        }
        return null;
    }

    public final void uploadProfilePhoto(@NotNull byte[] imageBytes, @NotNull final Function1 onSuccess) {
        Intrinsics.checkParameterIsNotNull(imageBytes, "imageBytes");
        Intrinsics.checkParameterIsNotNull(onSuccess, "onSuccess");
        final StorageReference ref = this.getCurrentUserRef().child("profilePictures/" + UUID.nameUUIDFromBytes(imageBytes));
        ref.putBytes(imageBytes).addOnSuccessListener((OnSuccessListener)(new OnSuccessListener() {
            // $FF: synthetic method
            // $FF: bridge method
            public void onSuccess(Object var1) {
                this.onSuccess((TaskSnapshot)var1);
            }

            public final void onSuccess(TaskSnapshot it) {
                Function1 var10000 = onSuccess;
                StorageReference var10001 = ref;
                Intrinsics.checkExpressionValueIsNotNull(var10001, "ref");
                String var2 = var10001.getPath();
                Intrinsics.checkExpressionValueIsNotNull(var2, "ref.path");
                var10000.invoke(var2);
            }
        }));
    }

    public final void uploadMessageImage(@NotNull byte[] imageBytes, @NotNull final Function1 onSuccess) {

        final StorageReference ref = this.getCurrentUserRef().child("messages/" + UUID.nameUUIDFromBytes(imageBytes));
        ref.putBytes(imageBytes).addOnSuccessListener((OnSuccessListener)(new OnSuccessListener() {
            // $FF: synthetic method
            // $FF: bridge method
            public void onSuccess(Object var1) {
                this.onSuccess((TaskSnapshot)var1);
            }

            public final void onSuccess(TaskSnapshot it) {
                Function1 var10000 = onSuccess;
                StorageReference var10001 = ref;
                String var2 = var10001.getPath();
                var10000.invoke(var2);
            }
        }));
    }

    @NotNull
    public final StorageReference pathToReference(@NotNull String path) {
//        Intrinsics.checkParameterIsNotNull(path, "path");
        return this.getStorageInstance().getReference(path);
    }

    private StorageUtil() {
    }

    static {
        StorageUtil var0 = new StorageUtil();
        INSTANCE = var0;
    }
}