package com.nearme.smartbuy.utility;

import java.math.BigInteger;
import java.util.HashMap;

public class MyAdvStatsPOJO {
    public int getDownloadCount() {
        return _dCount;
    }

    public void SetDownloadCount(int _dCount) {
        this._dCount = _dCount;
    }

    public int getViewCount() {
        return _vCount;
    }

    public void setViewCount(int _vCount) {
        this._vCount = _vCount;
    }

    int _dCount;
    int _vCount;

    String guid;
    @Override
    public boolean equals(Object o) {
        if (o instanceof MyAdvStatsPOJO) {
            MyAdvStatsPOJO p = (MyAdvStatsPOJO) o;
            return this.guid.equals(p.getViewCount());
        } else if (o instanceof HashMap && ((HashMap) o).containsKey("ViewCount")) {
            return ((HashMap) o).get("ViewCount").equals(this._vCount);
        } else
            return false;
    }
}
