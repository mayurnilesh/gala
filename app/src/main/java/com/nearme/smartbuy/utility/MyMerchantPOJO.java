package com.nearme.smartbuy.utility;

import java.util.HashMap;

public class MyMerchantPOJO {
    public String getImageref() {
        return imageref;
    }

    public void setImageref(String imageref) {
        this.imageref = imageref;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String imageref;
    String name;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    String guid;
    @Override
    public boolean equals(Object o) {
        if (o instanceof MyMerchantPOJO) {
            MyMerchantPOJO p = (MyMerchantPOJO) o;
            return this.guid.equals(p.getGuid());
        } else if (o instanceof HashMap && ((HashMap) o).containsKey("guid")) {
            return ((HashMap) o).get("guid").equals(this.guid);
        } else
            return false;
    }
}
