package com.nearme.smartbuy.utility;

import com.nearme.smartbuy.db.liquorannouncements;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.db.myexclusiveannouncements;
import com.nearme.smartbuy.db.mysocialannouncements;
import com.nearme.smartbuy.fragments.list_item_base;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by 06peng on 20015/6/24.
 */
public class ImageUrlUtils {
    static ArrayList<String> cartListImageUri = new ArrayList();



    public static List<list_item_base> getCommercials() {

        List<list_item_base> ads = new ArrayList<>();
        ads.clear();

         Calendar calendar;
        int year, month, day;

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);

        List<mybizannouncements> temp = mybizannouncements.findWithQuery(mybizannouncements.class,
                "SELECT * FROM mybizannouncements " +
                        "ORDER BY id DESC");

            for (int i = 0; i < temp.size(); i++) {
                String imageurl = temp.get(i).imgUrl;
                String shopDp = temp.get(i).shopDp;
                if(Integer.parseInt(temp.get(i).tillyear) < year)
                    continue;
                if(Integer.parseInt(temp.get(i).tillmonth) < month && Integer.parseInt(temp.get(i).tillyear) == year)
                    continue;
                if(Integer.parseInt(temp.get(i).tilldate) < day
                        && Integer.parseInt(temp.get(i).tillmonth) == month
                        && Integer.parseInt(temp.get(i).tillyear) == year )
                    continue;

                String validTill = temp.get(i).tilldate + "/" + temp.get(i).tillmonth + "/" + temp.get(i).tillyear;
                String offer = temp.get(i).offercode;
                String shopid = temp.get(i).merchantid;
//            shopdetails shop = shopdetails.findById(shopdetails.class,shopid);
                Double lat = temp.get(i).lat;
                Double lng = temp.get(i).lng;
                String shopname = temp.get(i).shopname;
//            String mapurl = "https://www.google.com/maps/?q="+shop.lat+","+shop.lng;
                String mapurl = "https://www.google.com/maps/?q=" + lat + "," + lng;
                String offerDesc = temp.get(i).itemdesc;
                int notificationId = temp.get(i).notificationid;
                Integer negotiate = temp.get(i).negotiate;
                Integer minBusiness = temp.get(i).minBusiness;
//            list_item_commercial obj = new list_item_commercial(imageurl,validTill,shop.shopName,offer,mapurl);

                list_item_base obj = new list_item_base(imageurl, validTill, shopname,
                        offer, mapurl, offerDesc, notificationId,shopDp, negotiate, minBusiness,temp.get(i).contatNumber);
                ads.add(obj);
            }
            return ads;

    }



    // Methods for Cart
    public void addCartListImageUri(String wishlistImageUri) {
        this.cartListImageUri.add(0,wishlistImageUri);
    }


}
