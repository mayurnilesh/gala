package com.nearme.smartbuy.utility;

import android.os.AsyncTask;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SendMailAsync extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... strings) {

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String dateToStr = format.format(today);
        System.out.println(dateToStr);
        String emailCntnt = strings[0];
        String sub = strings[1] + dateToStr;
        try {
            GMailSender sender = new GMailSender("gkomanduri@sportz.club", "Password@123");
            sender.sendMail(sub,
                    emailCntnt,
                    "gkomanduri@sportz.club",
                    "gkomanduri@sportz.club");
        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
        }

        return null;
    }
}
