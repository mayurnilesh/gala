package com.nearme.smartbuy.notification;

import android.os.AsyncTask;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class firebaseAsyncMsgPush extends AsyncTask<String, Void, String> {
    public static String topicName;

    String getPostJsonData(String msg, String topic)
    {
        return "{'winCondition':'HIGH_SCORE',"
                + "'name':'Bowling',"
                + "'round':4,"
                + "'lastSaved':1367702411696,"
                + "'dateStarted':1367702378785,"
                + "'players':["
                + "{'name':'" + "player1" + "','history':[10,8,6,7,8],'color':-13388315,'total':39},"
                + "{'name':'" + "player2" + "','history':[6,10,5,10,10],'color':-48060,'total':41}"
                + "]}";
    }

    String doPostRequest(final OkHttpClient client, String url, String json, String topicName) throws IOException {
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        firebaseNotificationformat  fnf = new firebaseNotificationformat();
        fnf.data = new HashMap<>();
        fnf.to = "/topics/"+topicName;
        fnf.time_to_live = 3600;
        fnf.data.put("cat",json);
        fnf.priority = "high";
        String fnfJson = new Gson().toJson(fnf,firebaseNotificationformat.class);
        RequestBody body = RequestBody.create(JSON, fnfJson);
        final Request request = new Request.Builder()
                .header("Authorization", "key=AAAAq680Rlg:APA91bGgfmS6FVidlmYuBd078eY9A8_LVyWBIh5V4BiHj_l-mE0zeuB70g-KQNyUHzi1L62wiQ9SI1JZV3BqfAfFnKyp8rd8znEYk3xOlj3E0EqZ9sE8mYqcxdC-O_NlWK3g8z1wvVTc4is525n1pDziLJqzfg5Fcg")
                .url(url)
                .post(body)
                .build();
        final Response[] response = {null};
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                     response[0] = client.newCall(request).execute();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
        response[0] = client.newCall(request).execute();
        return response[0].body().string();
    }

    @Override
    protected String doInBackground(String... strings) {

        String msg = strings[0];
        String type = strings[1];
        OkHttpClient client = new OkHttpClient();

        String postResponse = null;
        try {
            if(type == "0")
            {
                FirebaseMessaging.getInstance().subscribeToTopic(msg);
                topicName = msg;
            }
            if(type == "2")
            {
                FirebaseMessaging.getInstance().subscribeToTopic(msg);
                topicName = msg;
                postResponse = doPostRequest(client,"https://fcm.googleapis.com/fcm/send", msg, topicName);
            }
            if(type == "1")
            {
                postResponse = doPostRequest(client,"https://fcm.googleapis.com/fcm/send",msg, topicName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = postResponse;

        return postResponse;
    }
}
