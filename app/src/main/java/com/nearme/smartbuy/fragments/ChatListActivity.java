package com.nearme.smartbuy.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.ChatListAdapter;
import com.nearme.smartbuy.model.ChatInfo;
import com.nearme.smartbuy.utility.PrefManager;

import java.util.ArrayList;


public class ChatListActivity extends Fragment {
    View view;
    ChatListAdapter simpleAdapter;
    RecyclerView androidListView;
    ProgressBar pb;
    TextView tvEmpty;
    ChatInfo[] app_info;
    Activity context;
    private PrefManager prefManager;
    ArrayList<ChatInfo> ChatInfoArrayList = new ArrayList<>();
    private FirebaseAuth mAuth;
    FirebaseUser user;

    public static class chatItem {

        public String getImgref() {
            return imgref;
        }

        public void setImgref(String imgref) {
            this.imgref = imgref;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String imgref;
        public String name;

        public chatItem() {
            // ...
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.chat_list_activity, container, false);
        context = (Activity) container.getContext();
        prefManager = new PrefManager(context);
        androidListView = view.findViewById(R.id.chat_list_view);
        pb = view.findViewById(R.id.pb);
        tvEmpty = view.findViewById(R.id.tvEmpty);
        androidListView.setLayoutManager(new LinearLayoutManager(context));
        pb.setVisibility(View.VISIBLE);
        getChatDialog();
        return view;
    }

    private void getChatDialog() {
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if (user != null) {
            String user_id = user.getUid();//RESOLVE

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user_id + "_merchants");

            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                            chatItem _chatItem = dataSnapshot.child(String.valueOf(i)).getValue(chatItem.class);
                            if (_chatItem != null) {
                                ChatInfo info = new ChatInfo();
                                info.imgIcon = _chatItem.getImgref();
                                info.txtId = String.valueOf(i + 1);
                                info.txtTitle = _chatItem.getName();
                                ChatInfoArrayList.add(info);
                                simpleAdapter = new ChatListAdapter(context, ChatInfoArrayList);
                                androidListView.setAdapter(simpleAdapter);
                                simpleAdapter.notifyDataSetChanged();
                            }
                        }

                    } else {
                        tvEmpty.setVisibility(View.VISIBLE);
                    }
                    pb.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }


}



