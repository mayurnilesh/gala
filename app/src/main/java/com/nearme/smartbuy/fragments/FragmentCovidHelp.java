package com.nearme.smartbuy.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cloudinary.Cloudinary;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.rest.AdPayLoad;
import com.nearme.smartbuy.rest.AdRequest;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.MerchantPayload;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCovidHelp extends Fragment {

    View view;
    Context context;
    ProgressBar pb;
    String idConsumer;
    String number;
    String name;
    ImageView btnCapture, btnAdCapture;
    Uri imageUri;
    String realPath;
    TextView submitBtn, exp_date, postAdButton;
    EditText proof, offercode, offerdesc;
    LinearLayout linMerchantRegister, linUploadPost;
    private Calendar calendar;
    private int year, month, day;
    private Integer tilldate;
    private Integer tillmonth;
    private Integer tillyear;
    private String merchantId;

    String currentPhotoPath;
    File image;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_covid, container, false);
        context = container.getContext();

        initialize();

        return view;
    }

    private void initialize() {
        pb = view.findViewById(R.id.pb);
        btnCapture = view.findViewById(R.id.btnCapture);
        offerdesc = view.findViewById(R.id.offerdesc);
        offercode = view.findViewById(R.id.offercode);
        postAdButton = view.findViewById(R.id.postAdButton);
        exp_date = view.findViewById(R.id.exp_date);
        btnAdCapture = view.findViewById(R.id.btnAdCapture);
        submitBtn = view.findViewById(R.id.submitBtn);
        proof = view.findViewById(R.id.proof);
        linMerchantRegister = view.findViewById(R.id.linMerchantRegister);
        linUploadPost = view.findViewById(R.id.linUploadPost);
        getMerchantDetail();


        btnCapture.setOnClickListener(v -> {
//            CropImage.activity()
//                    .setGuidelines(CropImageView.Guidelines.ON)
//                    .setActivityTitle(getResources().getString(R.string.app_name))
//                    .setCropShape(CropImageView.CropShape.RECTANGLE)
//                    .setCropMenuCropButtonTitle("Done")
//                    .setRequestedSize(400, 400)
//                    .setOutputCompressQuality(50)
//                    .start(context, FragmentCovidHelp.this);
            ImagePicker.Companion.with(FragmentCovidHelp.this)
                    .crop()	    			//Crop image(Optional), Check Customization for more option
                    .compress(1024)			//Final image size will be less than 1 MB(Optional)
                    .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                    .start();
        });

        submitBtn.setOnClickListener(v -> {

            if (proof.getText().toString().trim().isEmpty()) {
                Toast.makeText(context, "Please enter id proof", Toast.LENGTH_LONG).show();
                return;
            }

            if (realPath == null) {
                Toast.makeText(context, "Please select image", Toast.LENGTH_LONG).show();
                return;
            }

            pb.setVisibility(View.VISIBLE);

            new AsyncTaskUpload().execute(realPath, idConsumer);
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        tillyear = calendar.get(Calendar.YEAR);
        tillmonth = calendar.get(Calendar.MONTH) + 1;
        tilldate = calendar.get(Calendar.DAY_OF_MONTH);

        showDate(year, month + 1, day);

        exp_date.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                    myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        });

        postAdButton.setOnClickListener(v -> {
            if (offercode.getText().toString().trim().isEmpty()) {
                Toast.makeText(context, "Please enter code", Toast.LENGTH_LONG).show();
                return;
            }

            if (offerdesc.getText().toString().trim().isEmpty()) {
                Toast.makeText(context, "Please enter description", Toast.LENGTH_LONG).show();
                return;
            }

            if (realPath == null) {
                Toast.makeText(context, "Please select image", Toast.LENGTH_LONG).show();
                return;
            }

            pb.setVisibility(View.VISIBLE);
            new AsyncTaskAdUpload().execute(realPath, merchantId);
        });

        btnAdCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .setActivityTitle(getResources().getString(R.string.app_name))
//                        .setCropShape(CropImageView.CropShape.RECTANGLE)
//                        .setCropMenuCropButtonTitle("Done")
//                        .setRequestedSize(400, 400)
//                        .setOutputCompressQuality(50)
//                        .start(context, FragmentCovidHelp.this);
//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, 100);

//              try  {
//                     image = File.createTempFile(String.valueOf(System.currentTimeMillis()), ".jpg");
//                    Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", image);
//                    currentPhotoPath = image.getAbsolutePath();
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//                    startActivityForResult(intent, 100);
//                }
//              catch(Exception e){
//                  Log.d("TAG", "onClick: "+e.toString());
//              }
                ImagePicker.Companion.with(FragmentCovidHelp.this)
                        .crop()	    			//Crop image(Optional), Check Customization for more option
                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                        .start();


            }
        });

    }

    private void postAd(String imageUrl) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        double lat = 0.0;
        double lng = 0.0;
        String geoHash = "";

        if (MyLocationListener.mCurrentLocation != null) {
            lat = MyLocationListener.mCurrentLocation.getLatitude();
            lng = MyLocationListener.mCurrentLocation.getLongitude();

            geoHash = GeoHash.fromCoordinates(MyLocationListener.mCurrentLocation.getLatitude(), MyLocationListener.mCurrentLocation.getLongitude(), 5).toString();
        } else {
            Toast.makeText(context, "Please enable location as we are not able to found location", Toast.LENGTH_LONG).show();
            return;
        }

        AdRequest obj = new AdRequest(geoHash, String.valueOf(lat), String.valueOf(lng), merchantId,
                "536870912", tilldate, tillmonth, tillyear,
                calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR),
                offercode.getText().toString(), offerdesc.getText().toString(), imageUrl,
                0, 0, "", "", "");

        String jsonConvertedlastReceivedAdStructList = new Gson().toJson(obj, AdRequest.class);


        Call<AdPayLoad> call = apiService.pushAd(jsonConvertedlastReceivedAdStructList);

        call.enqueue(new Callback<AdPayLoad>() {
            @Override
            public void onResponse(Call<AdPayLoad> call, Response<AdPayLoad> response) {

                realPath = null;
                imageUri = null;
                offercode.setText("");
                offerdesc.setText("");
                btnAdCapture.setImageResource(R.drawable.offer);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Successful");
                builder.setMessage("Ad posted successfully. Please check merchant app for history of ads posted.");
                builder.setPositiveButton("Go to Playstore", (dialog, which) -> {
                    dialog.dismiss();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.nearme.smartsell")));
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
               builder.show();
            }

            @Override
            public void onFailure(Call<AdPayLoad> call, Throwable t) {
                String error = "Ad sent failed. " + t.getMessage() + " Please re-try!";
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private DatePickerDialog.OnDateSetListener myDateListener = (arg0, arg1, arg2, arg3) -> {
        tillyear = arg1;
        tillmonth = arg2 + 1;
        tilldate = arg3;
        showDate(arg1, arg2 + 1, arg3);
    };

    private void showDate(int year, int month, int day) {
        exp_date.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void getMerchantDetail() {

        List<userdetails> temp = new ArrayList<>();//
        try {
            temp = userdetails.listAll(userdetails.class);
        } catch (Exception ex) {
            Log.d("TAG", "onCreate: ");
        }

        if (temp.size() == 0) {
            Toast.makeText(context, "User detail not found please logout and login again", Toast.LENGTH_LONG).show();
            return;
        }

        idConsumer = temp.get(0).idconsumers;
        number = temp.get(0).ContactNumber;
        name = temp.get(0).Name;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<MerchantPayload> call = apiService.getMerchantDetails(number);

        call.enqueue(new Callback<MerchantPayload>() {
            @Override
            public void onResponse(Call<MerchantPayload> call, Response<MerchantPayload> response) {
                pb.setVisibility(View.GONE);
                if (response.body().merchantId == null) {
                    linMerchantRegister.setVisibility(View.VISIBLE);
                    linUploadPost.setVisibility(View.GONE);
                } else {
                    merchantId = response.body().merchantId;
                    linUploadPost.setVisibility(View.VISIBLE);
                    linMerchantRegister.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MerchantPayload> call, Throwable t) {
                pb.setVisibility(View.GONE);
                Log.d("TAG", "onError: " + t.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            if(data!=null && data.getData()!=null) {
                Uri uri = data.getData();
                imageUri = uri;
                realPath = imageUri.toString();
                Picasso.get().load(imageUri).into(btnCapture);
                Picasso.get().load(imageUri).into(btnAdCapture);
            }
        }
    }

    private class AsyncTaskUpload extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            @SuppressLint("AuthLeak") Cloudinary cloudinaryObj = new Cloudinary("cloudinary://648251253437633:T4ZnOgkQ1yzhWTIFF8FHlqWCkLE@locator");
            Uri imgPath = Uri.parse(strings[0]);
            Map x = null;
            String mImageUrl = "";
            try {
                Map<String, String> options = new HashMap<>();
                options.put("folder", strings[1]);
                InputStream in = new FileInputStream(imgPath.getPath());
                x = cloudinaryObj.uploader().upload(in, options);

                if (x != null && x.containsKey("url")) {
                    mImageUrl = String.valueOf(x.get("url"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return mImageUrl;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TAG", "onPostExecute: " + s);
            pb.setVisibility(View.GONE);
            registerMerchant(s);
        }
    }

    private class AsyncTaskAdUpload extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            @SuppressLint("AuthLeak") Cloudinary cloudinaryObj = new Cloudinary("cloudinary://648251253437633:T4ZnOgkQ1yzhWTIFF8FHlqWCkLE@locator");
            Uri imgPath = Uri.parse(strings[0]);
            Map x = null;
            String mImageUrl = "";
            try {
                Map<String, String> options = new HashMap<>();
                options.put("folder", strings[1]);
                InputStream in = new FileInputStream(imgPath.getPath());
                x = cloudinaryObj.uploader().upload(in, options);

                if (x != null && x.containsKey("url")) {
                    mImageUrl = String.valueOf(x.get("url"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return mImageUrl;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TAG", "onPostExecute: " + s);
            pb.setVisibility(View.GONE);
            postAd(s);
        }
    }

    private void registerMerchant(String url) {
        pb.setVisibility(View.VISIBLE);

        double lat = 0.0;
        double lng = 0.0;
        String geoHash = "";

        if (MyLocationListener.mCurrentLocation != null) {
            lat = MyLocationListener.mCurrentLocation.getLatitude();
            lng = MyLocationListener.mCurrentLocation.getLongitude();

            geoHash = GeoHash.fromCoordinates(MyLocationListener.mCurrentLocation.getLatitude(), MyLocationListener.mCurrentLocation.getLongitude(), 5).toString();
        } else {
            Toast.makeText(context, "Please enable location as we are not able to found location", Toast.LENGTH_LONG).show();
            return;
        }

        MerchantPayload merobj = new MerchantPayload(name, number, "", "", "", "Shop_9",
                lat, lng, geoHash, "", String.valueOf(0000000000000000000000000000000000), "https://desiawaaz.com/assets/images/logo.svg", "", "",
                "", "", "0000", 0, proof.getText().toString(), url);

        String jsonConvertedlastReceivedAdStructList = new Gson().toJson(merobj, MerchantPayload.class);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<MerchantPayload> call = apiService.RegMerchant(jsonConvertedlastReceivedAdStructList);

        call.enqueue(new Callback<MerchantPayload>() {

            @Override
            public void onResponse(Call<MerchantPayload> call, Response<MerchantPayload> response) {
                MerchantPayload regRes = response.body();
                if (regRes == null || regRes.merchantId.equalsIgnoreCase("-1")) {
                    Toast.makeText(context, "This number is already registered", Toast.LENGTH_LONG).show();
                } else if (regRes.merchantId.equalsIgnoreCase("-2")) {
                    Toast.makeText(context, "Registration falied! Please try again later.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "merchant registered", Toast.LENGTH_LONG).show();
                    linMerchantRegister.setVisibility(View.VISIBLE);
                    linUploadPost.setVisibility(View.GONE);
                    realPath = null;
                    imageUri = null;
                    btnAdCapture.setImageResource(R.drawable.offer);
                    getMerchantDetail();
                }
                pb.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<MerchantPayload> call, Throwable t) {
                Toast.makeText(context, "failed to register as a Merchant", Toast.LENGTH_LONG).show();
                pb.setVisibility(View.GONE);
            }

        });
    }

}
