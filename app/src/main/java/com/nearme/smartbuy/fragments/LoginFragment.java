package com.nearme.smartbuy.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.firebase.consumerFirebasepayload;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.consumerpayload;
import com.nearme.smartbuy.startup.PhoneActivity;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginFragment extends Fragment implements View.OnClickListener {
    Context curContext;

    View curView;

    EditText userNameTxt;
    Button loginBtn;
    FragmentActivity curActivity;

    private String firebaseToken;
    CallbackManager callbackManager;
    LoginButton loginButton;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        curContext = this.getContext();
        curActivity = this.getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        AppEventsLogger.activateApp(curActivity);
        callbackManager = CallbackManager.Factory.create();
        curView = inflater.inflate(R.layout.login_fragment, container, false);
        userNameTxt = curView.findViewById(R.id.userNameTxt);
        loginBtn = curView.findViewById(R.id.signin);

        loginButton = curView.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("EMAIL"));
        loginButton.setFragment(this);
        loginBtn.setOnClickListener(view -> {
            if (!userNameTxt.getText().toString().trim().equals("") && userNameTxt.length() == 10) {

                char number = userNameTxt.getText().toString().charAt(0);

                if (Integer.parseInt(Character.toString(number)) < 6) {
                    Toast.makeText(curContext, "Please Enter Correct Mobile Number.", Toast.LENGTH_SHORT).show();
                    return;
                }

                String enteredNumber = "+91" + userNameTxt.getText().toString();
                signInWithPhoneAuthCredential(enteredNumber);

            } else {
                Toast.makeText(curContext, "Please Enter Correct Mobile Number.", Toast.LENGTH_SHORT).show();
            }

        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                firebaseToken = instanceIdResult.getToken();
                Log.d("Toen", "onSuccess: " + firebaseToken);
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(curContext, "Success "+loginResult.getAccessToken(), Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "onSuccess: "+loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(curContext, "Cancel", Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "cancel: ");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(curContext, "Error", Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "error: ");
                    }
                });


        fbCode();

        return curView;
    }

    private void fbCode() {
        loginButton.setReadPermissions(Arrays.asList("email"));
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(curContext, "Success "+loginResult.getAccessToken(), Toast.LENGTH_SHORT).show();
                Log.d("TAG", "onSuccess: "+loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(curContext, "Cancel", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "cancel: ");
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(curContext, "Error", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "error: ");
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 1001) {
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        }
    }


    private void signInWithPhoneAuthCredential(final String number) {

        consumerFirebasepayload firebasepayload = new consumerFirebasepayload();
        firebasepayload.FirebaseInstanceID = firebaseToken;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<consumerpayload> call = apiService.LoginUser(number, new Gson().toJson(firebasepayload));
        call.enqueue(new Callback<consumerpayload>() {
            @Override
            public void onResponse(Call<consumerpayload> call, Response<consumerpayload> response) {
                if (response.body() != null && response.body().contact != null) {
                    userdetails.deleteAll(userdetails.class);
                    AuthCredential credential = EmailAuthProvider
                            .getCredential(number + "@smartbuy.com",number);

                    FirebaseAuth.getInstance().signInWithCredential(credential)
                            .addOnCompleteListener(curActivity, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        //Got user back
                                        startActivity(new Intent(curContext, PhoneActivity.class).putExtra("number", number));
                                    }
                                }
                            });

                } else {
                    Toast.makeText(getActivity(), "This number is not registered.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<consumerpayload> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error ocuured.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
    }


}
