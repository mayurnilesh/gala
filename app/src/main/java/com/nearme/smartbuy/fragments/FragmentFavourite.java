package com.nearme.smartbuy.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.jobsreceived;
import com.nearme.smartbuy.utility.Constant;

import java.util.List;

public class FragmentFavourite extends Fragment {

    View view;
    Context context;
    RadioGroup radioGroup;
    SharedPreferences prefs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favourite, container, false);
        context = container.getContext();
        prefs = context.getSharedPreferences(Constant.appPref, 0);

        initialize();

        return view;
    }

    private void initialize() {
        SharedPreferences.Editor editor = prefs.edit();
        radioGroup = view.findViewById(R.id.rgFavourite);
        view.findViewById(R.id.submitBtn).setOnClickListener(v -> {
            RadioButton checkedRadioButton = view.findViewById(radioGroup.getCheckedRadioButtonId());
            int idx = radioGroup.indexOfChild(checkedRadioButton);
            Log.d("TAG", "onClick: " + idx);
            editor.putInt(Constant.favType, idx).apply();
            Toast.makeText(context, "Favourites updated", Toast.LENGTH_SHORT).show();
        });

        int fav = prefs.getInt(Constant.favType, 0);

        switch (fav) {
            case 1:
                radioGroup.check(R.id.rvToken);
                break;
            case 2:
                radioGroup.check(R.id.rvSlots);
                break;
            case 3:
                radioGroup.check(R.id.rvNegotiation);
                break;
            case 4:
                radioGroup.check(R.id.rvStatus);
                break;
            case 5:
                radioGroup.check(R.id.rvCovid);
                break;
            default:
                radioGroup.check(R.id.rvOffers);

        }

    }


}
