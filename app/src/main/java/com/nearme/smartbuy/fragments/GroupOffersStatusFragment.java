package com.nearme.smartbuy.fragments;


import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.JoinneListAdapter;
import com.nearme.smartbuy.adapter.OfferstatusAdapter;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.JoinCountPayLoad;
import com.nearme.smartbuy.rest.Statuspayload;
import com.nearme.smartbuy.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GroupOffersStatusFragment extends Fragment {

    View view;
    String TAG = "ExclusiveFragment";
    Context context;
    RecyclerView rec_job;
    List<Statuspayload> offersstatuslist = new ArrayList<>();
    OfferstatusAdapter apercuAdapter;
    TextView tvnodata;
    String userId;
    public static String status_contactNumber;
    ProgressBar pb;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_offerstatus, container, false);
        context = container.getContext();

        List<userdetails> temp = userdetails.listAll(userdetails.class);
        userId = String.valueOf(temp.get(0).idconsumers);
        status_contactNumber = String.valueOf(temp.get(0).ContactNumber);

        initialize();
        return view;
    }

    private void initialize() {

        tvnodata = view.findViewById(R.id.tvnodata);
        pb = view.findViewById(R.id.pb);

        rec_job = view.findViewById(R.id.rec_job);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_job.setLayoutManager(linearLayoutManager);
        rec_job.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new OfferstatusAdapter(offersstatuslist, new StatusInterface() {
            @Override
            public void onStatusClick(String title) {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.nearme.smartbuy"))
                        .setQuote(title)
                        .build();
                ShareDialog.show(GroupOffersStatusFragment.this, content);

            }
        });
        rec_job.setAdapter(apercuAdapter);

        if (offersstatuslist.size() > 0)
            tvnodata.setVisibility(View.GONE);
        else
            tvnodata.setVisibility(View.VISIBLE);

        rec_job.addOnItemTouchListener(new RecyclerTouchListener(context, rec_job, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                TextView joincount = view.findViewById(R.id.joincount);
                TextView joinnow = view.findViewById(R.id.joinnow);

                joinnow.setOnClickListener(v -> {

                    JoinCountPayLoad payload = new JoinCountPayLoad();
                    payload.id = -1;
                    payload.statusId = offersstatuslist.get(position).idstatus;
                    payload.userid = Integer.valueOf(userId);
                    String payloadstr = new Gson().toJson(payload);
                    ApiInterface apiService =
                            ApiClient.getClient().create(ApiInterface.class);
                    Call<String[]> call1 = apiService.JoinStatus(payloadstr);
                    call1.enqueue(new Callback<String[]>() {
                        @Override
                        public void onResponse(Call<String[]> call, Response<String[]> response) {
                            Log.d(TAG, "onResponse: Success");
                            offersstatuslist.get(position).isalreadyJoined = 1;
                            offersstatuslist.get(position).joinedcount = offersstatuslist.get(position).joinedcount + 1;
                            apercuAdapter.notifyDataSetChanged();
                            Toast.makeText(getContext(), "Joined Successfully", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<String[]> call, Throwable t) {
                            Log.d(TAG, "onFailure: falilure");
                        }
                    });
                });

                joincount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!offersstatuslist.get(position).registeredcontactnumber.equalsIgnoreCase(status_contactNumber))
                            return;

                        ApiInterface apiService =
                                ApiClient.getClient().create(ApiInterface.class);
                        Call<String[]> call1 = apiService.fetchalljoinees(String.valueOf(offersstatuslist.get(position).idstatus), GeoHash.fromCoordinates(MyLocationListener.mCurrentLocation.getLatitude(), MyLocationListener.mCurrentLocation.getLongitude(), 6).toString());
                        call1.enqueue(new Callback<String[]>() {
                            @Override
                            public void onResponse(Call<String[]> call, Response<String[]> response) {
                                String[] joineesList = response.body();

                                ArrayList<String> joinneArrayList = new ArrayList<>();
                                joinneArrayList.addAll(Arrays.asList(joineesList));

                                Log.d(TAG, "joinnelist: " + joineesList[0].toString());

                                final Dialog dialog = new Dialog(context, R.style.Theme_CustomDialog);
                                dialog.setContentView(R.layout.dialogue_joinnelist);
                                dialog.show();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.setCancelable(true);

                                RecyclerView recjoinee = dialog.findViewById(R.id.recjoinee);
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                                recjoinee.setLayoutManager(linearLayoutManager);
                                recjoinee.setItemAnimator(new DefaultItemAnimator());
                                JoinneListAdapter joinneListAdapter = new JoinneListAdapter(joinneArrayList);
                                recjoinee.setAdapter(joinneListAdapter);

                            }

                            @Override
                            public void onFailure(Call<String[]> call, Throwable t) {
                                Toast.makeText(context, "No Joinee there.", Toast.LENGTH_SHORT).show();


                            }
                        });

                    }
                });


            }

            @Override
            public void onLongClick(View view, final int pos) {


            }
        }));

        pb.setVisibility(View.VISIBLE);
        fetchStatus();

    }

    private void fetchStatus() {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        if (MyLocationListener.mCurrentLocation == null) {
            pb.setVisibility(View.GONE);
            return;
        }
        Call<Statuspayload[]> call = apiService.fetchstatuses(GeoHash.fromCoordinates(MyLocationListener.mCurrentLocation.getLatitude(), MyLocationListener.mCurrentLocation.getLongitude(), 5).toString(), "-1", userId);
        call.enqueue(new Callback<Statuspayload[]>() {
            @Override
            public void onResponse(Call<Statuspayload[]> call, Response<Statuspayload[]> response) {

                pb.setVisibility(View.GONE);
                Statuspayload[] jpr = response.body();

                offersstatuslist.addAll(Arrays.asList(jpr));
                apercuAdapter.notifyDataSetChanged();

                if (offersstatuslist.size() > 0)
                    tvnodata.setVisibility(View.GONE);
                else
                    tvnodata.setVisibility(View.VISIBLE);


            }

            @Override
            public void onFailure(Call<Statuspayload[]> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                pb.setVisibility(View.GONE);
            }
        });
    }

    public interface StatusInterface {
        public void onStatusClick(String title);
    }

}
