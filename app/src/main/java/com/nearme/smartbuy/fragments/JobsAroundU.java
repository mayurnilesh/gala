package com.nearme.smartbuy.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nearme.smartbuy.JobsAroundForm;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.JobsArounduAdapter;
import com.nearme.smartbuy.db.jobsreceived;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class JobsAroundU extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener {

    View view;
    String TAG = "JobsAroundU";
    Context context;
    RecyclerView rec_list;
    List<jobsreceived> books;
    List<jobsreceived> tmps = new ArrayList<>();
    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    Context mContext;
    boolean b = true;
    JobsArounduAdapter apercuAdapter;
    LinearLayout linmain;
    ImageView close;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_jobaroundu, container, false);
        context = container.getContext();
        mContext = container.getContext();

        books = jobsreceived.listAll(jobsreceived.class);
        tmps.addAll(books);
        Log.d(TAG, "onCreateView: " + books.size());

        initialize();


        return view;
    }

    private void initialize() {

        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragmentSupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFrag.getMapAsync(this);

        rec_list = view.findViewById(R.id.rec_list);
        close = view.findViewById(R.id.close);
        linmain = view.findViewById(R.id.linmain);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new JobsArounduAdapter(tmps);
        rec_list.setAdapter(apercuAdapter);


        view.findViewById(R.id.addbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, JobsAroundForm.class));
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linmain.setVisibility(View.GONE);
            }
        });

        view.findViewById(R.id.addinfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context, R.style.Theme_CustomDialog);
                dialog.setContentView(R.layout.dialogue_add_info);
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
            }
        });
    }

    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        mGoogleMap.setOnMarkerClickListener(this);

        return mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
        /*.snippet(snippet)*/
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        if (b) {
            b = false;
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        linmain.setVisibility(View.VISIBLE);
        tmps.clear();
        for (int i = 0; i < books.size(); i++) {
            if (marker.getPosition().equals(new LatLng(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng)))) {
                tmps.add(books.get(i));
            }
        }
        apercuAdapter.notifyDataSetChanged();

        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
                for (int i = 0; i < books.size(); i++) {

//                Log.d(TAG, "onMapReady: " + ads.get(i).itemdesc + " " + ads.get(i).lat + " " + ads.get(i).lng);
                    if (books.get(i).lat != null) {
                        if (books.get(i).educationQualification.equalsIgnoreCase("Secondary")) {
                            createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin1);
                        } else if (books.get(i).educationQualification.equalsIgnoreCase("Senior secondary")) {
                            createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin2);

                        } else if (books.get(i).educationQualification.equalsIgnoreCase("Graduate")) {
                            createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin3);

                        } else if (books.get(i).educationQualification.equalsIgnoreCase("Post Graduate")) {
                            createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin4);

                        }
                    }
                }
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
            for (int i = 0; i < books.size(); i++) {

//                Log.d(TAG, "onMapReady: " + ads.get(i).itemdesc + " " + ads.get(i).lat + " " + ads.get(i).lng);
                if (books.get(i).lat != null) {
                    if (books.get(i).educationQualification.equalsIgnoreCase("Secondary")) {
                        createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin1);
                    } else if (books.get(i).educationQualification.equalsIgnoreCase("Senior secondary")) {
                        createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin2);

                    } else if (books.get(i).educationQualification.equalsIgnoreCase("Graduate")) {
                        createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin3);

                    } else if (books.get(i).educationQualification.equalsIgnoreCase("Post Graduate")) {
                        createMarker(Double.parseDouble(books.get(i).lat), Double.parseDouble(books.get(i).lng), books.get(i).jobDescription, "", R.drawable.pin4);

                    }
                }
            }
        }

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getContext())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
}
