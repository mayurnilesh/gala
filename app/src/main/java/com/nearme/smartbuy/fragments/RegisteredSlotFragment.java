package com.nearme.smartbuy.fragments;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.StoreListAdapter;
import com.nearme.smartbuy.adapter.UserSlotAdapter;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.geohashutil.BoundingHashes;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.geohashutil.latlngcls;
import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.model.UserSlot;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisteredSlotFragment extends Fragment {

    View view;
    Context context;
    ProgressBar pb;
    RecyclerView rec_userslot;
    ArrayList<UserSlot> userSlotsList = new ArrayList<>();
    UserSlotAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_registered_slot, container, false);
        context = container.getContext();

        initialize();
        callAPi();
        return view;
    }

    private void initialize() {
        pb = view.findViewById(R.id.pb);
        rec_userslot = view.findViewById(R.id.rec_userslot);

        rec_userslot.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UserSlotAdapter(userSlotsList);
        rec_userslot.setAdapter(adapter);
    }

    private void callAPi() {
        pb.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault());
        String date = dateFormat.format(calendar.getTime());

        calendar.add(Calendar.MONTH, 2);
        String afterDate = dateFormat.format(calendar.getTime());

        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String id = "";
        if (temp.size() > 0)
            id = temp.get(0).ContactNumber;

        Log.d("TAG", "apiCall: " + date + " " + afterDate + " " + id);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<UserSlot[]> call = apiService.getRegisteredSlots(id, date, afterDate);

        call.enqueue(new Callback<UserSlot[]>() {
            @Override
            public void onResponse(Call<UserSlot[]> call, Response<UserSlot[]> response) {
                Log.d("TAG", "onResponse: ");
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    UserSlot[] userSlots = response.body();
                    for (int i = 0; i < userSlots.length; i++) {
                        Date fromDate = new Date(Long.parseLong(userSlots[i].selectedSlotStartHash) * 1000L);
                        Date toDate = new Date(Long.parseLong(userSlots[i].selectedSlotEndHash) * 1000L);
                        SimpleDateFormat sdf1 = new SimpleDateFormat("HH", Locale.getDefault());
                        SimpleDateFormat sdf2 = new SimpleDateFormat("mm", Locale.getDefault());
                        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy", Locale.getDefault());
                        SimpleDateFormat sdf4 = new SimpleDateFormat("MM", Locale.getDefault());
                        SimpleDateFormat sdf5 = new SimpleDateFormat("dd", Locale.getDefault());
                        sdf1.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        sdf2.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        sdf3.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        sdf4.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        sdf5.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        int hour = Integer.parseInt(sdf1.format(fromDate));
                        int minute = Integer.parseInt(sdf2.format(fromDate));
                        int year = Integer.parseInt(sdf3.format(fromDate));
                        int month = Integer.parseInt(sdf4.format(fromDate));
                        int day = Integer.parseInt(sdf5.format(fromDate));

                        int hour_ = Integer.parseInt(sdf1.format(toDate));
                        int minute_ = Integer.parseInt(sdf2.format(toDate));

                        userSlots[i].selectedSlotEndHash = hour_+":"+minute_+" on "+day+"/"+month+"/"+year;
                        userSlots[i].selectedSlotStartHash = hour+":"+minute;
                    }
                    Collections.addAll(userSlotsList, userSlots);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<UserSlot[]> call, Throwable t) {
                Log.d("TAG", "onFailure: ");
                pb.setVisibility(View.GONE);
            }
        });

    }

}
