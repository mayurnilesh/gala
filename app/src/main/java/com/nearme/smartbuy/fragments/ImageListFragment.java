/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nearme.smartbuy.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.db.negotiationrequests;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.events.updateUi;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.product.ItemDetailsActivity;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.NegotationPayLoad;
import com.nearme.smartbuy.startup.ChatActivity;
import com.nearme.smartbuy.startup.MainActivity;
import com.nearme.smartbuy.utility.ImageUrlUtils;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static com.nearme.smartbuy.location.MyLocationListener.ads;


public class ImageListFragment extends Fragment implements DatePickerDialog.OnDateSetListener, SwipeRefreshLayout.OnRefreshListener, SwipeRefreshLayout.OnTouchListener {

    public static final String STRING_IMAGE_URI = "ImageUri";
    public static final String STRING_IMAGE_POSITION = "ImagePosition";
    public static final String STRING_OFFER_DESCRIPTION = "OfferDecription";
    public static final String STRING_OFFER_ID = "OfferId";
    public static final String STRING_OFFER_CODE = "OfferCode";
    public static final String STRING_OFFER_VALIDTILL = "OfferValidTill";
    public static final String STRING_SHOP_NAME = "ShopName";
    public static final String STRING_SHOP_LOCATION = "ShopLocation";
    public static final String STRING_VIEW_TYPE = "ViewType";
    public String viewType = "0";
    private static final String DATEPICKER_TAG = "datepicker";
    static DatePickerDialog datePickerDialog;
    private static MainActivity mActivity;
    public SimpleStringRecyclerViewAdapter commercialAdapter = null;
    RecyclerView rv = null;
    SwipeRefreshLayout swipelayout;
    boolean tempswipe = false;
    public FragFriendClickFloatButton onClickFloatButton;
    static TextView temp;
    static TextView tempmap;
    TextView tvnodata;


    public ImageListFragment() {
        onClickFloatButton = new FragFriendClickFloatButton();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

//    @Override
//    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {
//        temp.setText(i2 + "-" + i1 + "-" + i);
//    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    public class FragFriendClickFloatButton implements View.OnClickListener {
        Context context;
        LovelyProgressDialog dialogWait;

        public FragFriendClickFloatButton() {
        }

        public FragFriendClickFloatButton getInstance(Context context) {
            this.context = context;
            dialogWait = new LovelyProgressDialog(context);
            return this;
        }

        @Override
        public void onClick(final View view) {

            mybizannouncements.deleteAll(mybizannouncements.class);
            EventBus.getDefault().post(new updateUi());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ads = ImageUrlUtils.getCommercials();

//        final Calendar calendar = Calendar.getInstance();
//        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        swipelayout = (SwipeRefreshLayout) inflater.inflate(R.layout.layout_recylerview_list, container, false);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        return swipelayout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv = getActivity().findViewById(R.id.recyclerview);
        tvnodata = getActivity().findViewById(R.id.tvnodata);
        rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int x = 30;
                x++;
            }

        });
        rv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                int y = 30;
                y++;
                return false;
            }
        });
        setupRecyclerView(rv);
    }


    @Subscribe
    public void onEvent(updateUi obj) {

        commercialAdapter.notifyDataSetChanged();
        tempswipe = true;

    }

    private void setupRecyclerView(RecyclerView recyclerView) {

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        if (commercialAdapter == null) {
            commercialAdapter = new SimpleStringRecyclerViewAdapter(recyclerView, ads, getContext());
        }
        recyclerView.setAdapter(commercialAdapter);

        if (ads.size() > 0)
            tvnodata.setVisibility(View.GONE);
        else
            tvnodata.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRefresh() {

        int x = 30;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        return false;
    }


    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        public List<list_item_base> adValues;
        public Context ctx;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageButton mImageView;
            public final de.hdodenhof.circleimageview.CircleImageView mDp;
            public final TextView fragName;
            public final TextView validTill;
            public final TextView shopName;
            public final LinearLayout mLayoutItem;
            public final TextView offerCode;
            public final ImageButton mImageViewWishlist;
            public final ImageView negotiation;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                //  mImageView = (SimpleDraweeView) view.findViewById(R.id.image1);
                mDp = view.findViewById(R.id.dp);
                mImageView = view.findViewById(R.id.image1);
                negotiation = view.findViewById(R.id.negotition);
                validTill = view.findViewById(R.id.idValidTill);
                shopName = view.findViewById(R.id.idShopName);
                offerCode = view.findViewById(R.id.idOfferCode);
                mLayoutItem = view.findViewById(R.id.layout_item);
                mImageViewWishlist = view.findViewById(R.id.ic_wishlist);
                fragName = view.findViewById(R.id.fragId);

            }
        }

        public SimpleStringRecyclerViewAdapter(RecyclerView recyclerView, List<list_item_base> items, Context context) {

            adValues = items;
            ctx = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            final Uri uri = Uri.parse(adValues.get(position).imageUrl);
            final String validTill = adValues.get(position).validTill;
            final String offercode = adValues.get(position).offerCode;
            final String shopName = adValues.get(position).shopName;
            final String locUrl = adValues.get(position).location;
            final String offerDescription = adValues.get(position).offerDescription;
            final String merchantDp = adValues.get(position).shopDp;
            final Integer notificationId = adValues.get(position).notificationid;

            final int frag = getItemViewType(position);
            Glide.with(ctx)
                    .load(uri)
                    .into(holder.mImageView);

            holder.offerCode.setText(offercode);
            holder.validTill.setText(validTill);
            holder.shopName.setText(shopName);
            Glide.with(ctx)
                    .load(merchantDp)
                    .into(holder.mDp);
            holder.mLayoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, ItemDetailsActivity.class);

                    intent.putExtra(ImageListFragment.STRING_IMAGE_POSITION, position);
                    intent.putExtra(ImageListFragment.STRING_IMAGE_URI, adValues.get(position).imageUrl);
                    //           intent.putExtra(STRING_OFFER_ID,adValues.get(position).offerId);
                    intent.putExtra(ImageListFragment.STRING_OFFER_DESCRIPTION, adValues.get(position).offerDescription);
                    intent.putExtra(ImageListFragment.STRING_OFFER_VALIDTILL, adValues.get(position).validTill);
                    intent.putExtra(ImageListFragment.STRING_SHOP_NAME, adValues.get(position).shopName);
                    intent.putExtra(ImageListFragment.STRING_SHOP_LOCATION, adValues.get(position).location);
                    intent.putExtra(ImageListFragment.STRING_OFFER_CODE, adValues.get(position).offerCode);
                    intent.putExtra(ImageListFragment.STRING_VIEW_TYPE, mActivity.selectedFrag);
                    mActivity.startActivity(intent);
                }
            });

            if (adValues.get(position).negotiate == 0) {
                holder.negotiation.setVisibility(View.GONE);
            } else {
                holder.negotiation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final Dialog dialog = new Dialog(ctx, R.style.Theme_CustomDialog);
                        dialog.setContentView(R.layout.dialogue_negotiation);
                        dialog.show();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.setCancelable(true);

                        final EditText minSale = dialog.findViewById(R.id.MinSale);
                        final String minVal = String.valueOf(adValues.get(position).minBusiness);
                        minSale.setText(minVal);
                        final EditText maxSale = dialog.findViewById(R.id.MaxSale);
                        final EditText discount = dialog.findViewById(R.id.discount);
                        final TextView shoppingdates = dialog.findViewById(R.id.shoppingdates);
                        final TextView shoppintoDate = dialog.findViewById(R.id.shoppingtodates);
                        final TextView choose_locatoin = dialog.findViewById(R.id.choose_locatoin);
                        final EditText detail_address = dialog.findViewById(R.id.detail_address);
                        final EditText description = dialog.findViewById(R.id.description);
                        choose_locatoin.setEnabled(false);
                        choose_locatoin.setText(adValues.get(position).location);
                        choose_locatoin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                try {
//                                    tempmap = choose_locatoin;
//                                    Intent intent =
//                                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
//                                                    .build(mActivity);
//                                    mActivity.startActivityForResult(intent, 1);
//                                } catch (GooglePlayServicesRepairableException e) {
//                                    // TODO: Handle the error.
//                                } catch (GooglePlayServicesNotAvailableException e) {
//                                    // TODO: Handle the error.
//                                }
                                tempmap = choose_locatoin;
                                Places.initialize(mActivity, "AIzaSyCYH_ZrSBHf3r_Ov04IuHNS8TaJDb-Fp_M");
                                List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));
                                Intent autocompleteIntent =
                                        new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                                                .build(mActivity);
                                mActivity.startActivityForResult(autocompleteIntent, 1);
                            }
                        });

                        shoppingdates.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final Calendar c = Calendar.getInstance();
                                int year = c.get(Calendar.YEAR);
                                int month = c.get(Calendar.MONTH);
                                int day = c.get(Calendar.DAY_OF_MONTH);

                                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        shoppingdates.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                                    }
                                }, year, month, day);
                                datePickerDialog.show();
                            }
                        });

                        shoppintoDate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final Calendar c = Calendar.getInstance();
                                int year = c.get(Calendar.YEAR);
                                int month = c.get(Calendar.MONTH);
                                int day = c.get(Calendar.DAY_OF_MONTH);

                                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        shoppintoDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                                    }
                                }, year, month, day);
                                datePickerDialog.show();
                            }
                        });

                        TextView close = dialog.findViewById(R.id.tvsubmit);

                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                if (checkEditText(minSale, "Enter Minsale") && checkMinvalueNumber(minSale, minVal) && checkEditText(maxSale, "Enter Maxsale")
                                        && checkEditTextfordisc(discount, "Enter Discount") && checkEditText(description, "Enter Description")
                                        && checkTextView(shoppingdates, "Enter Minsale") && checkTextView(shoppintoDate, "Enter Minsale")
                                        && checkMinvalue(minSale, maxSale) && checkDate(shoppingdates, shoppintoDate)) {
                                    final NegotationPayLoad npl = new NegotationPayLoad();
                                    List<mybizannouncements> bizValues = mybizannouncements.find(mybizannouncements.class, "notificationid = ? and lat=? and lng=?", new String[]{String.valueOf(adValues.get(position).notificationid), String.valueOf(adValues.get(position).lat), String.valueOf(adValues.get(position).lng)}, null, null, null);

                                    List<userdetails> temp = userdetails.listAll(userdetails.class);
                                    String phoneNumber = temp.get(0).ContactNumber;
                                    npl.customercontact = phoneNumber;
                                    npl.delieverd = 0;
                                    final String geoHash = GeoHash.fromCoordinates(adValues.get(position).lat, adValues.get(position).lng, 5).toString();
                                    npl.geohash = geoHash;// bizValues.get(0).geoHash;
//                                    npl.geohash = bizValues.get(0).geoHash;
                                    npl.merchantid = bizValues.get(0).merchantid;
                                    npl.description = description.getText().toString();
                                    npl.minamount = Integer.valueOf(minSale.getText().toString());
                                    npl.maxamount = Integer.valueOf(maxSale.getText().toString());
                                    npl.DiscountExpectation = Integer.valueOf(discount.getText().toString());
                                    npl.ShoppingProbableDates = "from: " + shoppingdates.getText().toString() + ", to: " + shoppintoDate.getText().toString();
                                    npl.notificationid = notificationId;


                                    ApiInterface apiService =
                                            ApiClient.getClient().create(ApiInterface.class);

                                    String nplStr = new Gson().toJson(npl);


                                    Call<String> call = apiService.StartNegotiate(nplStr);
                                    call.enqueue(new Callback<String>() {
                                        @Override
                                        public void onResponse(Call<String> call, Response<String> response) {


                                            String negId = response.body().toString();
                                            negotiationrequests neg = new negotiationrequests(npl.notificationid,
                                                    npl.geohash, Integer.valueOf(negId));

                                            try {
                                                neg.save();


                                            } catch (Exception ex) {
                                                int y = 40;
                                                ++y;
                                            }


                                            Toast.makeText(ctx, "Negotation started", Toast.LENGTH_LONG).show();
                                            dialog.dismiss();

                                        }

                                        @Override
                                        public void onFailure(Call<String> call, Throwable t) {

                                            Toast.makeText(ctx, "Negotation Failed. Please re-send", Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
            //Set click action for wishlist
            holder.mImageViewWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//
                    Uri uri = Uri.parse(locUrl);
                    Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW, uri);

                    mActivity.startActivity(mapIntent);
                    Toast.makeText(mActivity, "Redirecting to Maps.", Toast.LENGTH_SHORT).show();


                }
            });


            //  animate(holder);

        }

        @Override
        public int getItemCount() {
            // return mValues.length;
            return adValues.size();
        }

        public void insert(int position, list_item_base data) {
            adValues.add(position, data);
            notifyItemInserted(position);
        }

        // Remove a RecyclerView item containing a specified Data object
        public void remove(list_item_base data) {
            int position = adValues.indexOf(data);
            adValues.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            if (resultCode == getActivity().RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.d("TAG", "onActivityResult: " + place.getName());
                tempmap.setText(place.getName());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

//    private static void openDateDialogue(TextView editText) {
//        temp = editText;
//        datePickerDialog.setVibrate(false);
//        datePickerDialog.setYearRange(1985, 2028);
//        datePickerDialog.setCloseOnSingleTapDay(true);
//        datePickerDialog.show(mActivity.getSupportFragmentManager(), DATEPICKER_TAG);
//
//    }

    private static boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkEditTextfordisc(EditText et, String msg) {
        if (et.getText().toString().equals("") && Integer.parseInt(et.getText().toString()) <= 99) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkTextView(TextView et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkMinvalue(EditText et, EditText msg) {
        if (Integer.parseInt(et.getText().toString()) > Integer.parseInt(msg.getText().toString())) {
            et.setError("Enter Less value then Max value");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkMinvalueNumber(EditText et, String msg) {
        if (Integer.parseInt(et.getText().toString()) > Integer.parseInt(msg)) {
            et.setError("Minimum business required for negotiation is " + msg + " Rupees.");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkDate(TextView et, TextView et1) {

        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(et.getText().toString());
            Date enddate = sdf.parse(et1.getText().toString());

            if (strDate.getTime() > enddate.getTime()) {
                et.setError("Enter lesser date from to date");
                et.requestFocus();
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }
}
