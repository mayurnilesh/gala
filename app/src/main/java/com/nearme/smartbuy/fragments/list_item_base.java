package com.nearme.smartbuy.fragments;

public class list_item_base {
    public String imageUrl;
    public String validTill;
    public String shopName;
    public String offerCode;
    public String location;
    public String offerDescription;
    public String shopDp;
    public Integer notificationid;
    public Integer negotiate;
    public Integer minBusiness;
    public Double lat;
    public Double lng;
    public String merchantID;
    public String contactNumber;




    public list_item_base() {

    }

    public list_item_base(String imageUrl, String validTill, String shopName,
                          String offerCode, String location,
                          String offerDescription, int notificationid,
                          String shopDp, Integer negotiate,
                          Integer minBusiness, String contactNumber) {
        this.imageUrl = imageUrl;
        this.validTill = validTill;
        this.shopName = shopName;
        this.offerCode = offerCode;
        this.location = location;
        this.offerDescription = offerDescription;
        this.notificationid = notificationid;
        this.shopDp = shopDp;
        this.negotiate = negotiate;
        this.minBusiness = minBusiness;
        this.contactNumber = contactNumber;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getShopDp() {
        return shopDp;
    }

    public void setShopDp(String shopDp) {
        this.shopDp = shopDp;
    }

    public int getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(int notificationid) {
        this.notificationid = notificationid;
    }

    public Integer getNegotiate() {
        return negotiate;
    }

    public void setNegotiate(Integer negotiate) {
        this.negotiate = negotiate;
    }

    public Integer getMinBusiness() {
        return minBusiness;
    }

    public void setMinBusiness(Integer minBusiness) {
        this.minBusiness = minBusiness;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setNotificationid(Integer notificationid) {
        this.notificationid = notificationid;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }
}
