package com.nearme.smartbuy.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.karumi.dexter.Dexter;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.TokenAdapter;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.events.raisenotificationevent;
import com.nearme.smartbuy.model.Tokenmodel;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.tokenregistrationpayload;
import com.nearme.smartbuy.rest.tokenstatus;
import com.nearme.smartbuy.rest.tokenstatuspayload;
import com.nearme.smartbuy.utility.Constant;
import com.nearme.smartbuy.utility.RecyclerTouchListener;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class Tokenfragment extends Fragment {

    private View view;
    private String TAG = "Tokenfragment";
    private Context context;
    private LinearLayout linmain;
    private RelativeLayout rlmain;
    private TextView scannowfirst, scannow;
    private ArrayList<Tokenmodel> stringArrayList = new ArrayList<>();
    private TokenAdapter apercuAdapter;
    private ProgressBar pb;
    private String merchantid;
    TextView tv;
    String firebaseToken;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gettoken, container, false);
        assert container != null;
        context = container.getContext();

        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.CAMERA);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                firebaseToken = instanceIdResult.getToken();
                Log.d("Toen", "onSuccess: " + firebaseToken);
            }
        });

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                new IntentFilter("TAG_TOKEN"));

        initialize();
        clicks();
//
        return view;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getTokenStatus();
        }
    };

    private void initialize() {
        linmain = view.findViewById(R.id.linmain);
        rlmain = view.findViewById(R.id.rlmain);
        pb = view.findViewById(R.id.pb);
        scannowfirst = view.findViewById(R.id.scannowfirst);
        scannow = view.findViewById(R.id.scannow);
        RecyclerView rec = view.findViewById(R.id.rec);

        getPreferenceData();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec.setLayoutManager(linearLayoutManager);
        rec.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new TokenAdapter(stringArrayList, new TokenAdapter.onItemClick() {

            @Override
            public void onClick(Tokenmodel tokenmodel, String type) {
                if (type.equalsIgnoreCase("deregister")) {
                    pb.setVisibility(View.VISIBLE);
                    deleteToken(tokenmodel.getMerchantid());
                    deregistertoken(tokenmodel);
                } else {
                    pb.setVisibility(View.VISIBLE);
                    merchantid = tokenmodel.getMerchantid();
                    regenerateToken(tokenmodel);
                }
            }
        });
        rec.setAdapter(apercuAdapter);
        getTokenStatus();

        view.findViewById(R.id.refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTokenStatus();
            }
        });

    }


    public void getTokenStatus() {

        if (stringArrayList.size() != 0) {
            pb.setVisibility(View.VISIBLE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<tokenstatuspayload[]> call = apiService.getalltokenstatus(generateTokenPayload());
            call.enqueue(new Callback<tokenstatuspayload[]>() {
                @Override
                public void onResponse(Call<tokenstatuspayload[]> call, Response<tokenstatuspayload[]> response) {
                    tokenstatuspayload[] payload = response.body();
                    if (payload != null && payload.length != 0) {
                        ArrayList<Tokenmodel> tmpList = new ArrayList<>();
                        for (int i = 0; i < payload.length; i++) {
                            if (!payload[i].GeneratedToken.equalsIgnoreCase("-2") &&
                                    !payload[i].GeneratedToken.equalsIgnoreCase("-1") &&
                                    !payload[i].GeneratedToken.equalsIgnoreCase("0")) {
                                Tokenmodel tokenmodel = new Tokenmodel();
                                tokenmodel.setMerchantid(payload[i].merchantid);
                                tokenmodel.setToken(payload[i].token);
                                tokenmodel.setMerchanturl(getMerchantUrl(payload[i].merchantid));
                                tokenmodel.setMerchantname(getMerchantName(payload[i].merchantid));
                                tokenmodel.setPosition(payload[i].GeneratedToken);
                                tmpList.add(tokenmodel);
                            }
                        }
                        stringArrayList.clear();
                        stringArrayList.addAll(tmpList);
                    } else {
                        stringArrayList.clear();
                    }
                    saveTokenData();
                    getPreferenceData();
                    apercuAdapter.notifyDataSetChanged();
                    pb.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<tokenstatuspayload[]> call, Throwable t) {
                    Log.d(TAG, "onFailure: ");
                    pb.setVisibility(View.GONE);
                }
            });
        }
    }

    private String getMerchantUrl(String id) {
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).getMerchantid().equalsIgnoreCase(id)) {
                return stringArrayList.get(i).getMerchanturl();
            }
        }
        return "";
    }

    private String getMerchantName(String id) {
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).getMerchantid().equalsIgnoreCase(id)) {
                return stringArrayList.get(i).getMerchantname();
            }
        }
        return "";
    }

    private String generateTokenPayload() {
        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String phoneNumber = temp.get(0).ContactNumber;
        ArrayList<tokenstatuspayload> list = new ArrayList<>();
        for (int i = 0; i < stringArrayList.size(); i++) {
            tokenstatuspayload tkPayload = new tokenstatuspayload();
            tkPayload.contact = phoneNumber;
            tkPayload.merchantid = stringArrayList.get(i).getMerchantid();
            tkPayload.token = stringArrayList.get(i).getToken();
            tkPayload.existingStatus = stringArrayList.get(i).getPosition();
            tkPayload.GeneratedToken = "-1";
            list.add(tkPayload);
        }

        return new Gson().toJson(list);
    }

    private void deleteToken(String merchantid) {
        int position = 0;
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).getMerchantid().equalsIgnoreCase(merchantid))
                position = i;
        }
        stringArrayList.remove(position);
        if (stringArrayList.size() == 0) {
            rlmain.setVisibility(View.GONE);
            linmain.setVisibility(View.VISIBLE);
        }
        saveTokenData();
        getPreferenceData();
    }

    private void getPreferenceData() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        if (!pref.getString("tokenData", "n/a").equalsIgnoreCase("n/a")) {
            ArrayList<Tokenmodel> tokenmodelArrayList = new Gson().fromJson(pref.getString("tokenData", "n/a"), new TypeToken<ArrayList<Tokenmodel>>() {
            }.getType());
            stringArrayList.clear();
            if (tokenmodelArrayList != null && !tokenmodelArrayList.isEmpty()) {
                stringArrayList.addAll(tokenmodelArrayList);
                linmain.setVisibility(View.GONE);
                rlmain.setVisibility(View.VISIBLE);
            } else {
                linmain.setVisibility(View.VISIBLE);
                rlmain.setVisibility(View.GONE);
            }
        }


    }

    private void clicks() {
        scannowfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMerchantDialogue();
            }
        });

        scannow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMerchantDialogue();

            }
        });

    }

    private void openDialogue(final tokenregistrationpayload tokendetailpayload) {

        final Dialog dialog = new Dialog(context, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialogue_merchanttoken);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        TextView tvsubmit = dialog.findViewById(R.id.tvsubmit);
        TextView name = dialog.findViewById(R.id.name);
        TextView token = dialog.findViewById(R.id.token);
        TextView ytoken = dialog.findViewById(R.id.ytoken);
        ImageView img = dialog.findViewById(R.id.img);

        name.setText(tokendetailpayload.getMerchantname());
        token.setText("Max Token : " + tokendetailpayload.getCurrentMaxToken());
        ytoken.setText("Current Token : " + tokendetailpayload.getCurrentRunningToken());

        Uri uri = Uri.parse(tokendetailpayload.getMerchanturl());

        Glide.with(context)
                .load(uri)
                .into(img);

        tvsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb.setVisibility(View.VISIBLE);
                linmain.setVisibility(View.GONE);
                rlmain.setVisibility(View.VISIBLE);
                registerToken(tokendetailpayload);
                dialog.dismiss();
            }
        });

    }

    private void openMerchantDialogue() {

        final Dialog dialog = new Dialog(context, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialogue_merchantid);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText edtmerchant = dialog.findViewById(R.id.edtmerchant);

        TextView tvsubmit = dialog.findViewById(R.id.tvsubmit);
        TextView scannow = dialog.findViewById(R.id.scannow);
        tvsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtmerchant.getText().toString().equalsIgnoreCase("")) {
                    edtmerchant.setError("Enter merchant id");
                    edtmerchant.requestFocus();
                } else {
                    if (checkTokenAlreadyRegister(edtmerchant.getText().toString().trim())) {
                        Toast.makeText(context, "Token is already register for this merchant", Toast.LENGTH_SHORT).show();
                    } else {
                        dialog.dismiss();
                        callMerchantTokenDetail(edtmerchant.getText().toString());
                        merchantid = edtmerchant.getText().toString();
                        pb.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        scannow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode for merchant details");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(true);
                new IntentIntegrator(getActivity()).initiateScan();
                dialog.dismiss();
            }
        });

    }

    private boolean checkTokenAlreadyRegister(String merchantId) {
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).getMerchantid().equalsIgnoreCase(merchantId)) {
                return true;
            }
        }
        return false;
    }

    private void callMerchantTokenDetail(String id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<tokenregistrationpayload> call = apiService.getmerchanttokendetails(id);
        call.enqueue(new Callback<tokenregistrationpayload>() {
            @Override
            public void onResponse(Call<tokenregistrationpayload> call, Response<tokenregistrationpayload> response) {
                tokenregistrationpayload tokendetailpayload = response.body();//
                Log.d(TAG, "onResponse: yes here");
                Log.d(TAG, "onResponse: " + tokendetailpayload.getMerchantname());
                pb.setVisibility(View.GONE);
                if (tokendetailpayload.getMerchantname().length() == 0 || tokendetailpayload.getMerchanturl().length() == 0) {
                    Toast.makeText(context, "Counters not yet opened ", Toast.LENGTH_LONG).show();
                } else {
                    openDialogue(tokendetailpayload);
                }
            }

            @Override
            public void onFailure(Call<tokenregistrationpayload> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void registerToken(final tokenregistrationpayload tokendetailpayload) {

        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String phoneNumber = temp.get(0).ContactNumber;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<tokenstatus> call = apiService.registerfortoken(merchantid, phoneNumber, firebaseToken);
        call.enqueue(new Callback<tokenstatus>() {
            @Override
            public void onResponse(Call<tokenstatus> call, Response<tokenstatus> response) {
                if (response.body() == null) {
                    Toast.makeText(getContext(), "Technical Error Occurred. Please re-login to use this feature", Toast.LENGTH_SHORT).show();
                    pb.setVisibility(View.GONE);
                    return;
                }
                pb.setVisibility(View.GONE);
                if (response.body().getToken() == null) {
                    Toast.makeText(getContext(), "Getting token null", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getYouareat() == null) {
                    Toast.makeText(getContext(), "Getting your position null", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (checkMerchantExist(response.body().getToken(), response.body().getYouareat())) {
                    Tokenmodel tokenmodel = new Tokenmodel();
                    tokenmodel.setMerchantid(merchantid);
                    tokenmodel.setToken(String.valueOf(response.body().getToken()));
                    tokenmodel.setMerchanturl(tokendetailpayload.getMerchanturl());
                    tokenmodel.setMerchantname(tokendetailpayload.getMerchantname());
                    tokenmodel.setPosition(String.valueOf(response.body().getYouareat()));
                    stringArrayList.add(tokenmodel);
                }
                saveTokenData();
                apercuAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<tokenstatus> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void saveTokenData() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("tokenData", new Gson().toJson(stringArrayList));
        editor.apply();
    }

    private boolean checkMerchantExist(int token, int position) {
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).getMerchantid().equalsIgnoreCase(merchantid)) {
                stringArrayList.get(i).setToken(String.valueOf(token));
                stringArrayList.get(i).setPosition(String.valueOf(position));
                return false;
            }
        }
        return true;
    }

    private void deregistertoken(Tokenmodel tokenmodel) {
        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String phoneNumber = temp.get(0).ContactNumber;
        final SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = apiService.deregister(tokenmodel.getMerchantid(), phoneNumber, tokenmodel.getPosition(), firebaseToken);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                pb.setVisibility(View.GONE);
                Toast.makeText(context, "Token Deregistered", Toast.LENGTH_SHORT).show();
                getTokenStatus();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, "failure", Toast.LENGTH_SHORT).show();
                pb.setVisibility(View.GONE);

            }
        });

    }

    private void regenerateToken(final Tokenmodel tokendetailpayload) {
        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String phoneNumber = temp.get(0).ContactNumber;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = apiService.regenrateToken(tokendetailpayload.getMerchantid(), tokendetailpayload.getToken());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body() != null) {
                    if (response.body().equalsIgnoreCase("0")) {
                        getTokenStatus();
                        getTokenStatus();//putted this temporary for -5 issue
                    } else {
                        Toast.makeText(getActivity(), "Counters are closed", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Counters are closed", Toast.LENGTH_SHORT).show();
                }

                apercuAdapter.notifyDataSetChanged();
                pb.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() != null) {
                Log.d(TAG, "onActivityResult: getConent");
                String msg = result.getContents();
                Log.d(TAG, "onActivityResult1: getConent" + msg);
                if (msg != null && !msg.equalsIgnoreCase("")) {
                    if (checkTokenAlreadyRegister(msg)) {
                        Toast.makeText(context, "Token is already register for this merchant", Toast.LENGTH_LONG).show();
                    } else {
                        callMerchantTokenDetail(msg);
                        merchantid = msg;
                        pb.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(context, "Scan failed", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Scan failed", Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(context, "Scan failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getTokenStatus();
    }

    @Override
    public void onPause() {
        Constant.isTokenScreen = false;
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Constant.isTokenScreen = false;
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }
}
