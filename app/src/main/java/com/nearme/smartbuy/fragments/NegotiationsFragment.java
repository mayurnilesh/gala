package com.nearme.smartbuy.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.NegotiationDataAdapter;
import com.nearme.smartbuy.adapter.NegotiationsItemListAdapter;
import com.nearme.smartbuy.db.NegotiationData;
import com.nearme.smartbuy.db.NegotiationResponse;
import com.nearme.smartbuy.db.negotiationresponsesreceived;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class NegotiationsFragment extends Fragment {

    private View view;
    private String TAG = "NegotiationsFragment";
    private Context context;
    private TextView tvnodata;
    private List<NegotiationData> negotiationDataList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_negotiations, container, false);
        context = container.getContext();
        RelativeLayout negoFragment = view.findViewById(R.id.negoFrag);
        tvnodata = view.findViewById(R.id.tvnodata);
        negoFragment.setVisibility(View.VISIBLE);


        negotiationDataList = NegotiationData.listAll(NegotiationData.class);
        Log.d(TAG, "onCreateView: " + negotiationDataList.size());

        getNefotiationData();

        return view;
    }

    private void getNefotiationData() {

        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String consumeContact = String.valueOf(temp.get(0).ContactNumber);

        int lastId = 0;
        try {
            lastId = negotiationDataList.get(negotiationDataList.size() - 1).idnegotations;
        } catch (Exception e) {
            lastId =-1;
            e.printStackTrace();
        }

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<NegotiationResponse[]> call = apiService.fetchConsumerNegotiations(consumeContact, String.valueOf(lastId));

        call.enqueue(new Callback<NegotiationResponse[]>() {
            @Override
            public void onResponse(Call<NegotiationResponse[]> call, Response<NegotiationResponse[]> response) {
                if (response.body() != null) {
                    NegotiationResponse[] negotiationResponse = response.body();

                    if (negotiationResponse.length != 0) {
                        for (NegotiationResponse value : negotiationResponse) {
                            NegotiationData negotiationData = new NegotiationData(
                                    value.getCustomercontact(),
                                    value.getMerchantid(),
                                    value.getGeohash(),
                                    value.getMinamount(),
                                    value.getMaxamount(),
                                    value.getDiscountExpectation(),
                                    value.getShoppingProbableDates(),
                                    value.getDescription(),
                                    value.getDelieverd(),
                                    value.getResponse(),
                                    value.getNotificationid(),
                                    value.getIdnegotations(),
                                    value.getAdvance(),
                                    value.getRespondedOn(),
                                    value.getAdObj().imgUrl,
                                    value.getAdObj().getOffercode(),
                                    value.getAdObj().getItemdesc()
                            );
                            negotiationData.save();
                        }
                    }
                }
                initialize();
            }

            @Override
            public void onFailure(Call<NegotiationResponse[]> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    private void initialize() {
        RecyclerView rec_list = view.findViewById(R.id.rec);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        negotiationDataList = NegotiationData.listAll(NegotiationData.class);
        NegotiationDataAdapter negotiationDataAdapter = new NegotiationDataAdapter(negotiationDataList);
        rec_list.setAdapter(negotiationDataAdapter);

        if (negotiationDataList.size() == 0)
            tvnodata.setVisibility(View.VISIBLE);
        else
            tvnodata.setVisibility(View.GONE);

    }

}
