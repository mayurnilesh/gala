package com.nearme.smartbuy.fragments;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.StoreListAdapter;
import com.nearme.smartbuy.geohashutil.BoundingHashes;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.geohashutil.latlngcls;
import com.nearme.smartbuy.location.MyLocationListener;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterSlotFragment extends Fragment {

    View view;
    Context context;
    RecyclerView rec_slot;
    StoreListAdapter mAdapter;
    ArrayList<Merchant> merchantArrayList = new ArrayList<>();
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register_slot, container, false);
        context = container.getContext();

        initialize();
        callAPi();
        return view;
    }

    private void initialize() {

        mAdapter = new StoreListAdapter(merchantArrayList);
        rec_slot = view.findViewById(R.id.rec_slot);
        progressBar = view.findViewById(R.id.pb);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_slot.setLayoutManager(linearLayoutManager);
        rec_slot.setAdapter(mAdapter);
    }

    private void callAPi() {
        if(MyLocationListener.mCurrentLocation==null){
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        double latitude = MyLocationListener.mCurrentLocation.getLatitude();
        double longitude = MyLocationListener.mCurrentLocation.getLongitude();
        List<latlngcls> boundedLatLngs = BoundingHashes.create_geohash(latitude, longitude, 2000, 5, false, 1, 12);

        List<String> uniquesHash = new ArrayList<>();
        for (int i = 0; i < boundedLatLngs.size(); i++) {
            latlngcls temp = boundedLatLngs.get(i);
            String geoHash = GeoHash.fromCoordinates(temp.x, temp.y, 5).toString();
            if (uniquesHash.contains(geoHash))
                continue;
            uniquesHash.add(geoHash);
        }

        Log.d("TAG", "callAPi: " + uniquesHash.size());

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Merchant[]> call = apiService.getAroundMerchant(new Gson().toJson(uniquesHash));
        call.enqueue(new Callback<Merchant[]>() {
            @Override
            public void onResponse(Call<Merchant[]> call, Response<Merchant[]> response) {
                merchantArrayList.clear();
                Merchant[] merchants = response.body();
                if (merchants != null) {
                    for (Merchant value : merchants) {
                        value.distance = calculateDistance(value.latitude, value.longitude);
                        merchantArrayList.add(value);
                    }
                    mAdapter.notifyDataSetChanged();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Merchant[]> call, Throwable t) {
                Log.d("TAG", "onFailure: ");
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private double calculateDistance(String lat, String lng) {
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(MyLocationListener.mCurrentLocation.getLatitude());
        startPoint.setLongitude(MyLocationListener.mCurrentLocation.getLongitude());

        Location endPoint = new Location("locationB");
        endPoint.setLatitude(Double.parseDouble(lat));
        endPoint.setLongitude(Double.parseDouble(lng));

        double distance = startPoint.distanceTo(endPoint);
        Log.d("TAG", "calculateDistance: " + distance);
        return distance / 1000;
    }

}
