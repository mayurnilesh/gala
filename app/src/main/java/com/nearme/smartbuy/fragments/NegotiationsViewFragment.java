package com.nearme.smartbuy.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.NegotiationsAdapter;
import com.nearme.smartbuy.db.negotiationresponsesreceived;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class NegotiationsViewFragment extends Fragment {

    View view;
    String TAG = "NegotiationsFragment";
    Context context;
    RecyclerView rec_job;
    List<negotiationresponsesreceived> books;
    List<negotiationresponsesreceived> tmps = new ArrayList<>();
    RecyclerView rec_list;
    RelativeLayout negoFragment;
    FloatingActionButton negofab;
    int position;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_negotiations, container, false);
        context = container.getContext();
        negofab = view.findViewById(R.id.negofab);
//        negofab.setVisibility(View.INVISIBLE);
        negoFragment=view.findViewById(R.id.negoFrag);
        negoFragment.setVisibility(View.VISIBLE);
        Bundle bundle = this.getArguments();// getActivity().getIntent().getExtras();
        String Desc = bundle.getString("desc");
        tmps.clear();
        books = negotiationresponsesreceived.listAll(negotiationresponsesreceived.class);
        for (int i = 0; i < books.size(); i++) {
            if (tmps != null && books.get(i).description.equals(Desc)) {
                tmps.add(books.get(i));
                position = i;
            }
        }

        negofab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("filter_string");
                intent.putExtra("key", position);
                intent.putExtra("Negoid",books.get(position).notificationid);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
        Log.d(TAG, "onCreateView: " + books.size());

        initialize();

        return view;
    }

    private void initialize() {
        rec_list = view.findViewById(R.id.rec);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        NegotiationsAdapter apercuAdapter = new NegotiationsAdapter(tmps);
        rec_list.setAdapter(apercuAdapter);

    }


}
