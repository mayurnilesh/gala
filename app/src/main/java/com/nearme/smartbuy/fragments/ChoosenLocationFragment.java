package com.nearme.smartbuy.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.BottomSheetAdapter;
import com.nearme.smartbuy.adapter.OfferAdapter;
import com.nearme.smartbuy.db.Categories;
import com.nearme.smartbuy.db.maxadidreceived;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.db.negotiationrequests;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.events.updateUi;
import com.nearme.smartbuy.geohashutil.BoundingHashes;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.geohashutil.latlngcls;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.rest.AdPayLoad;
import com.nearme.smartbuy.rest.AdPayLoadResponse;
import com.nearme.smartbuy.rest.AdRequestPayload;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.LastReceivedAdStruct;
import com.nearme.smartbuy.rest.NegotationPayLoad;
import com.nearme.smartbuy.startup.MainActivity;
import com.nearme.smartbuy.utility.Constant;
import com.nearme.smartbuy.utility.MyAdvStatsPOJO;
import com.nearme.smartbuy.utility.MyMerchantPOJO;
import com.nearme.smartbuy.utility.PrefManager;
import com.nearme.smartbuy.utility.RecyclerTouchListener;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;


public class ChoosenLocationFragment extends Fragment implements DatePickerDialog.OnDateSetListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener {

    View view;
    String TAG = "ChoosenLocationFragment";
    Context context;
    RecyclerView rec_job;
    private TextView searchbox;
    double latitude;
    double longitude;
    List<AdPayLoad> adPayLoadResponses = new ArrayList<>();
    List<Integer> notificationid = new ArrayList<>();
    TextView tempmap, temp;
    private static final String DATEPICKER_TAG = "datepicker";
    static DatePickerDialog datePickerDialog;
    TextView tvnodata;
    public static HashMap<String, Integer> maxIdsReceived = new HashMap<>();
    public static HashMap<String, Integer> maxJobIdsReceived = new HashMap<>();
    public static List<list_item_base> _ads = new ArrayList<>();
    public List<list_item_base> adsTemp = new ArrayList<>();
    List<Categories> categories = new ArrayList<>();
    Map<Marker, Merchant> markerMap = new HashMap<Marker, Merchant>();
    View v;
    SupportMapFragment mapFrag;
    private PrefManager prefManager;
    boolean b = true;
    GoogleMap mGoogleMap;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    LinearLayout linmain;
    ImageView close;
    RecyclerView rec_list;
    OfferAdapter adapter;
    List<list_item_base> adValues = new ArrayList<>();
    List<mybizannouncements> ads;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    int tmp = 0;
    private static MainActivity mActivity;
    private static TextView choose_locatoin;
    private DatabaseReference mDatabase;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_choosenlocation, container, false);
        context = container.getContext();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        linmain = view.findViewById(R.id.linmain);
        close = view.findViewById(R.id.close);
        rec_list = view.findViewById(R.id.rec_list);

        prefManager = new PrefManager(context);
        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragmentSupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFrag.getMapAsync(this);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linmain.setVisibility(View.GONE);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        adapter = new OfferAdapter(adValues, getContext());
        rec_list.setAdapter(adapter);

        rec_list.addOnItemTouchListener(new RecyclerTouchListener(context, rec_list, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

            }

            @Override
            public void onLongClick(View view, final int pos) {


            }
        }));

        initialize();
        return view;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(getContext())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        getAdsanMarkonMap(mybizannouncements.listAll(mybizannouncements.class),null);
    }

    private void getAdsanMarkonMap(List<mybizannouncements> currentAds,List<Merchant> merchants) {
        if(merchants==null) {
            if (ads != null)
                ads.clear();
            ads = currentAds;
        }
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        merchants=Merchant.listAll(Merchant.class);
        LatLng latLng = new LatLng(latitude, longitude);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
                if(merchants!=null) {
                    for (int i = 0; i < merchants.size(); i++) {
//                    Log.d(TAG, "onMapReady: " + ads.get(i).itemdesc + " " + ads.get(i).lat + " " + ads.get(i).lng);
//                    Date c = Calendar.getInstance().getTime();
//                    Log.d(TAG, "Current time => " + c);
//
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//                    boolean datesInRange = false;
//                    try {
//                        Date fromdate = sdf.parse(ads.get(i).fromyear + "-" + ads.get(i).frommonth + "-" + ads.get(i).fromdate);
//                        Date tilldate = sdf.parse(ads.get(i).tillyear + "-" + ads.get(i).tillmonth + "-" + ads.get(i).tilldate);
//                        Date CurDate = sdf.parse(sdf.format(c));
//
//                        if (!tilldate.before(CurDate)) {
//                            Log.i("app", "Date1 is after Date2");
//                            datesInRange = true;
//                        }
//
//                    } catch (Exception ex) {
//
//                    }
                        if (merchants.get(i).latitude != null && merchants.get(i).longitude != null) {
                            boolean merchantHasAds=false;
                            for (int j = 0; j < ads.size(); j++) {
                                if(ads.get(j).merchantid.equals(merchants.get(i).merchantId)){
                                    merchantHasAds=true;
                                    break;
                                }

                            }
                            if(merchantHasAds)
                                createMarker(Double.parseDouble(merchants.get(i).latitude),
                                        Double.parseDouble(merchants.get(i).longitude),
                                        merchants.get(i), "", R.drawable.offershop);
                            else
                            {
                                createMarker(Double.parseDouble(merchants.get(i).latitude),
                                        Double.parseDouble(merchants.get(i).longitude),
                                        merchants.get(i), "", R.drawable.shoppingstore);
                            }
//                            if (tmp == 0) {
//
//                                tmp = 1;
//                            } else if (tmp == 1) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin2);
//                                tmp = 2;
//                            } else if (tmp == 2) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin3);
//                                tmp = 3;
//                            } else if (tmp == 3) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin4);
//                                tmp = 0;
//                            }
                        }
                    }
                }
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
            if(merchants!=null) {
                for (int i = 0; i < merchants.size(); i++) {
                    Log.d(TAG, "onMapReady: " + ads.get(i).itemdesc + " " + ads.get(i).lat + " " + ads.get(i).lng);
                    if (merchants.get(i).latitude != null && merchants.get(i).longitude != null) {
                        boolean merchantHasAds=false;
                        for (int j = 0; j < ads.size(); j++) {
                            if(ads.get(j).merchantid.equals(merchants.get(i).merchantId)){
                                merchantHasAds=true;
                                break;
                            }

                        }
                        if(merchantHasAds)
                            createMarker(Double.parseDouble(merchants.get(i).latitude),
                                    Double.parseDouble(merchants.get(i).longitude),
                                    merchants.get(i), "", R.drawable.offershop);
                        else
                        {
                            createMarker(Double.parseDouble(merchants.get(i).latitude),
                                    Double.parseDouble(merchants.get(i).longitude),
                                    merchants.get(i), "", R.drawable.shoppingstore);
                        }
//                            if (tmp == 0) {
//
//                                tmp = 1;
//                            } else if (tmp == 1) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin2);
//                                tmp = 2;
//                            } else if (tmp == 2) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin3);
//                                tmp = 3;
//                            } else if (tmp == 3) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin4);
//                                tmp = 0;
//                            }
                    }
                }
            }
        }
    }

    private void createMarker(double latitude, double longitude, Merchant merchant, String snippet, int iconResID) {

        mGoogleMap.setOnMarkerClickListener(this);

        Marker marker =  mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(merchant.merchantName)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
        if(!markerMap.containsKey(marker))
            markerMap.put(marker,merchant);
    }


    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        if (b) {
            b = false;
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(context, "permission denied", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Merchant merchantInfo = markerMap.get(marker);
        Log.d(TAG, "onMarkerClick: " + marker.getTitle());
        linmain.setVisibility(View.VISIBLE);
        adValues.clear();
        for (int i = 0; i < ads.size(); i++) {
            if (merchantInfo!=null && merchantInfo.merchantId.equalsIgnoreCase(ads.get(i).merchantid)) {
                if (checkDuplicate(ads.get(i).offercode, ads.get(i).itemdesc)) {
                    list_item_base itemBase = new list_item_base();
                    itemBase.setImageUrl(ads.get(i).imgUrl);
                    itemBase.setValidTill(ads.get(i).tilldate + "/" + ads.get(i).tillmonth + "/" + ads.get(i).tillyear);
                    itemBase.setShopName(ads.get(i).shopname);
                    itemBase.setOfferCode(ads.get(i).offercode);
                    itemBase.setLocation("");
                    itemBase.setOfferDescription(ads.get(i).itemdesc);
                    itemBase.setShopDp(ads.get(i).shopDp);
                    itemBase.setNotificationid(ads.get(i).notificationid);
                    itemBase.setNegotiate(ads.get(i).negotiate);
                    itemBase.setMinBusiness(ads.get(i).minBusiness);
                    itemBase.setLng(ads.get(i).lng);
                    itemBase.setLat(ads.get(i).lat);
                    itemBase.setMerchantID(ads.get(i).merchantid);
                    String mapurl = "https://www.google.com/maps/?q=" + ads.get(i).lat + "," + ads.get(i).lng;
                    itemBase.setLocation(mapurl);
                    adValues.add(itemBase);
                }
            }
        }
        adapter.notifyDataSetChanged();

        return false;
    }


    private Boolean checkDuplicate(String offerCode, String desc) {
        for (int i = 0; i < adValues.size(); i++) {
            if (adValues.get(i).offerCode.equalsIgnoreCase(offerCode) && adValues.get(i).offerDescription.equalsIgnoreCase(desc)) {
                return false;
            }
        }
        return true;
    }

    private void initialize() {

        tvnodata = view.findViewById(R.id.tvnodata);

        searchbox = view.findViewById(R.id.searchbox);
        searchbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.isenable = true;
                Constant.isSearchother = true;
                Places.initialize(getActivity(), "AIzaSyCYH_ZrSBHf3r_Ov04IuHNS8TaJDb-Fp_M");
                List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));
                Intent autocompleteIntent =
                        new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                                .build(getActivity());
                startActivityForResult(autocompleteIntent, 1);

            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName());
                if(searchbox!=null) {
                    searchbox.setText("" + place.getName());
                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    invokeads();

                    LatLng coordinate = new LatLng(latitude, longitude); //Store these lat lng values somewhere. These should be constant.
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            coordinate, 15);
                    mGoogleMap.animateCamera(location);

                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
    }


    private void invokeads() {
        List<String> geoHashes = new ArrayList<>();
        final String geoHash = GeoHash.fromCoordinates(latitude, longitude, 5).toString();
        Log.d(TAG, "invokeads: " + geoHash);
        List<LastReceivedAdStruct> adStructs = new ArrayList<>();
        List<latlngcls> boundedLatLngs = BoundingHashes.create_geohash(latitude, longitude, 2000, 5, false, 1, 12);

        for (int i = 0; i < boundedLatLngs.size(); i++) {
            latlngcls temp = boundedLatLngs.get(i);
            String _geoHash = GeoHash.fromCoordinates(temp.x, temp.y, 5).toString();
            if (geoHashes.contains(_geoHash))
                continue;
            geoHashes.add(_geoHash);
            //geoHashes.add(GeoHash.fromCoordinates(temp.x, temp.y, 5).toString());
        }

        categories = Categories.listAll(Categories.class);

        for (int i = 0; i < categories.size(); i++) {
            LastReceivedAdStruct lastReceivedAdStruct = new LastReceivedAdStruct();
            lastReceivedAdStruct.geoHash = geoHashes;
            lastReceivedAdStruct.lastReceivedAdId = "-1";
            lastReceivedAdStruct.Category = (categories.get(i).catid - 1);
            adStructs.add(lastReceivedAdStruct);
        }

        AdRequestPayload adRequestPayload = new AdRequestPayload();
        adRequestPayload.lastReceivedAdStructList = adStructs;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<AdPayLoadResponse[]> call = apiService.getText(adStructs);
        call.enqueue(new Callback<AdPayLoadResponse[]>() {
            @Override
            public void onResponse(Call<AdPayLoadResponse[]> call, Response<AdPayLoadResponse[]> response) {
                AdPayLoadResponse[] receivedAds = response.body();
                Log.d(TAG, "onResponse: " + receivedAds.length);

                ArrayList<mybizannouncements> adList = new ArrayList<>();
                if (receivedAds.length > 0) {
                    for (AdPayLoadResponse receivedAd : receivedAds) {
                        //merchant_adid
                        //if merchant_adid exist increment download count add userid who downloaded
                        //merchant_adid viewcount shall be maintained at
                        try {
                            Query query = mDatabase
                                    .child("AdsStas/" + receivedAd.merchantid + "_" + receivedAd.geo + "_" + receivedAd.Id);
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    HashMap<String, Object> myAdvStatsPOJO = new HashMap<String, Object>();
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        //username found
                                        myAdvStatsPOJO = (HashMap<String, Object>) dataSnapshot.getValue();
                                    } else {
                                        // username not found
                                        //myMerchantPOJOs = new ArrayList<>();
                                        mDatabase.child("AdsStas/" + receivedAd.merchantid + "_" + receivedAd.geo + "_" + receivedAd.Id).push();
                                    }
                                    int _dcount =0;
                                    int _vCount=0;
                                    try {
                                        _dcount = Integer.parseInt(myAdvStatsPOJO.get("downloadCount").toString());// getDownloadCount();
                                        _vCount = Integer.parseInt(myAdvStatsPOJO.get("viewCount").toString());
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    _dcount++;
                                    MyAdvStatsPOJO _myAdvStatsPOJO=new MyAdvStatsPOJO();

                                    _myAdvStatsPOJO.SetDownloadCount(_dcount);
                                    _myAdvStatsPOJO.setViewCount(_vCount);

                                    mDatabase.child("AdsStas").
                                            child(receivedAd.merchantid + "_" + receivedAd.geo + "_" + receivedAd.Id).setValue(_myAdvStatsPOJO);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                        catch (Exception ex)
                        {
                            System.out.println("Failed to update count");
                        }
                        mybizannouncements obj = new mybizannouncements(receivedAd.Id, receivedAd.geo, receivedAd.lat, receivedAd.lng, receivedAd.merchantid,
                                receivedAd.cat, receivedAd.tilldate, receivedAd.tillmonth,
                                receivedAd.tillyear, receivedAd.fromdate, receivedAd.frommonth
                                , receivedAd.fromyear, receivedAd.offercode,
                                receivedAd.itemdesc, receivedAd.imgUrl, receivedAd.shopname,
                                receivedAd.shopDp, receivedAd.negotiate, receivedAd.minBusiness, receivedAd.contatNumber);
                        adList.add(obj);
                    }
                }
                //getAdsanMarkonMap(adList);
            }

            @Override
            public void onFailure(Call<AdPayLoadResponse[]> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
        boolean callAPI=false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            Date strDate = sdf.parse(prefManager.getFirstTimeDate());

            if ( (sdf.parse(sdf.format(Calendar.getInstance().getTime()))).after(strDate)) {
                callAPI = true;
            }
        }
        catch (Exception ex)
        {
            callAPI=true;
        }
//        if(callAPI) {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            String formattedDate = df.format(c);
            prefManager.setFirstTimeDate(formattedDate);
            Call<Merchant[]> merscall = apiService.getAroundMerchant(new Gson().toJson(geoHashes));
            merscall.enqueue(new Callback<Merchant[]>() {
                @Override
                public void onResponse(Call<Merchant[]> call, Response<Merchant[]> response) {
                    Merchant[] receivedMerchs = response.body();
                    Log.d(TAG, "onResponse: " + receivedMerchs.length);

                    ArrayList<Merchant> mersList = new ArrayList<>();
                    if (receivedMerchs.length > 0) {
                        for (Merchant merchant : receivedMerchs) {
                            //mersList.add(merchant);
                            new Merchant(merchant).save();
                        }
                    }
                    getAdsanMarkonMap(null, mersList);
                }

                @Override
                public void onFailure(Call<Merchant[]> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                }
            });
//        }
    }


    private boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkTextView(TextView et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkMinvalue(EditText et, EditText msg) {
        if (Integer.parseInt(et.getText().toString()) > Integer.parseInt(msg.getText().toString())) {
            et.setError("Enter Less value then Max value");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkMinvalueNumber(EditText et, String msg) {
        if (Integer.parseInt(et.getText().toString()) < Integer.parseInt(msg)) {
            et.setError("Minimum business required for negotiation is " + msg + " Rupees.");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkDate(TextView et, TextView et1) {

        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(et.getText().toString());
            Date enddate = sdf.parse(et1.getText().toString());

            if (strDate.getTime() > enddate.getTime()) {
                et.setError("Enter lesser date from to date");
                et.requestFocus();
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        temp.setText(year + "-" + month + "-" + dayOfMonth);
    }
}
