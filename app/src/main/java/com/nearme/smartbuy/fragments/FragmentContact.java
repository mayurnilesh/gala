package com.nearme.smartbuy.fragments;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.jobsreceived;

import java.util.List;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class FragmentContact extends Fragment {

    View view;
    String TAG = "JobsAroundU";
    Context context;
    RecyclerView rec_job;
    List<jobsreceived> books;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact, container, false);
        context = container.getContext();

        return view;
    }

    private void initialize() {

    }


}
