package com.nearme.smartbuy.fragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.ExclusiveAdapter;
import com.nearme.smartbuy.db.negotiationresponsesreceived;
import com.nearme.smartbuy.utility.RecyclerTouchListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ExclusiveFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    View view;
    String TAG = "ExclusiveFragment";
    Context context;
    RecyclerView rec_job;
    private List<negotiationresponsesreceived> books;
    static List<negotiationresponsesreceived> tmpBooks = new ArrayList<>();
    private final String DATEPICKER_TAG = "datepicker";
    DatePickerDialog datePickerDialog;
    TextView temp;
    ProgressBar pb;
    TextView tvnodata;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_exclusive, container, false);
        context = container.getContext();

        pb = view.findViewById(R.id.pb);

//        final Calendar calendar = Calendar.getInstance();
//        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


        tmpBooks.clear();
        books = checkDuplicate(negotiationresponsesreceived.listAll(negotiationresponsesreceived.class));
        tmpBooks.addAll(books);
        Log.d(TAG, "onCreateView: " + books.size());

        initialize();
        return view;
    }

    private List<negotiationresponsesreceived> checkDuplicate(List<negotiationresponsesreceived> books) {
        List<negotiationresponsesreceived> tmplist = new ArrayList<>();

        for (int i = 0; i < books.size(); i++) {
            boolean b = true;
            for (int j = 0; j < tmplist.size(); j++) {
                if (tmplist.get(j).idnegotiationresponse.equals(books.get(i).idnegotiationresponse))
                    b = false;
            }

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            if (b)
                tmplist.add(books.get(i));
        }

        return tmplist;
    }

    private void initialize() {

        rec_job = view.findViewById(R.id.rec_job);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_job.setLayoutManager(linearLayoutManager);
        rec_job.setItemAnimator(new DefaultItemAnimator());
        ExclusiveAdapter apercuAdapter = new ExclusiveAdapter(books);
        rec_job.setAdapter(apercuAdapter);

        tvnodata = view.findViewById(R.id.tvnodata);
        if (books.size() > 0)
            tvnodata.setVisibility(View.GONE);
        else
            tvnodata.setVisibility(View.VISIBLE);

    }


    @Override
    public void onDateSet(DatePicker view, int i, int i1, int i2) {
        temp.setText(i2 + ":" + i1 + ":" + i);
    }

    public static void updatePayload(int position) {
        negotiationresponsesreceived negOBJ = tmpBooks.get(position);
        negOBJ.setCanPostToStatus(0);
        negOBJ.save();
    }

}
