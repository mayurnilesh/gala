package com.nearme.smartbuy.fragments;


import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.ReceiptAdapter;
import com.nearme.smartbuy.db.contactspecificinfodat;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.contactspecificinfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopi.komanduri on 10/09/18.
 */

public class Receiptfragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    View view;
    String TAG = "Receiptfragment";
    Context context;
    RecyclerView rec;
    TextView date;
    private static final String DATEPICKER_TAG = "datepicker";
    static DatePickerDialog datePickerDialog;
    List<contactspecificinfo> contactspecificinfoArrayList = new ArrayList<>();
    List<contactspecificinfo> tmplist = new ArrayList<>();
    ReceiptAdapter apercuAdapter;
    Spinner sp, sp_price;
    ArrayList<String> merchantNameList = new ArrayList<>();
    FloatingActionButton refresh;
    Calendar calendar;
    TextView nodata;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_receipt, container, false);
        context = container.getContext();
//        calendar = Calendar.getInstance();
//        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        initialize();
        return view;
    }

    private void initialize() {
        date = view.findViewById(R.id.date);
        refresh = view.findViewById(R.id.refresh);
        sp = view.findViewById(R.id.sp);
        sp_price = view.findViewById(R.id.sp_price);
        nodata = view.findViewById(R.id.nodata);


        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        date.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                        sp.setSelection(0);
                        sp_price.setSelection(0);

                        String dt = year + "-" + (month + 01) + "-" + dayOfMonth;

                        try {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD", Locale.getDefault());
                            Date d1 = formatter.parse(dt);

                            contactspecificinfoArrayList.clear();
                            for (int j = 0; j < tmplist.size(); j++) {
                                Date d2 = formatter.parse(tmplist.get(j).getDate());
                                if (d1.equals(d2))
                                    contactspecificinfoArrayList.add(tmplist.get(j));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (contactspecificinfoArrayList.size() == 0)
                            nodata.setVisibility(View.VISIBLE);
                        else
                            nodata.setVisibility(View.GONE);

                        apercuAdapter.notifyDataSetChanged();

                    }
                }, year, month, day);
//                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                date.setText("Choose Date");
                sp_price.setSelection(0);
                sp.setSelection(0);

                contactspecificinfoArrayList.clear();
                contactspecificinfoArrayList.addAll(tmplist);
                apercuAdapter.notifyDataSetChanged();

                if (contactspecificinfoArrayList.size() == 0)
                    nodata.setVisibility(View.VISIBLE);
                else
                    nodata.setVisibility(View.GONE);
            }
        });

        rec = view.findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec.setLayoutManager(linearLayoutManager);
        rec.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new ReceiptAdapter(contactspecificinfoArrayList);
        rec.setAdapter(apercuAdapter);

        setdata();

        getReceipt();
    }

    private void filterdata(int minprice, int maxprice) {
        contactspecificinfoArrayList.clear();
        for (int i = 0; i < tmplist.size(); i++) {
            if (Float.parseFloat(tmplist.get(i).getBillamount()) <= maxprice &&
                    Float.parseFloat(tmplist.get(i).getBillamount()) >= minprice) {
                contactspecificinfoArrayList.add(tmplist.get(i));
            }
        }

        if (contactspecificinfoArrayList.size() == 0)
            nodata.setVisibility(View.VISIBLE);
        else
            nodata.setVisibility(View.GONE);
        apercuAdapter.notifyDataSetChanged();
    }

    private boolean checkduplicate(String s) {
        for (int i = 0; i < merchantNameList.size(); i++) {
            if (merchantNameList.get(i).equalsIgnoreCase(s))
                return false;
        }
        return true;
    }

//    @SuppressLint("SetTextI18n")
//    @Override
//    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {
//
//        sp.setSelection(0);
//        sp_price.setSelection(0);
//
//        String dt = i + "-" + (i1 + 01) + "-" + i2;
//        date.setText(dt);
//
//        try {
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD", Locale.getDefault());
//            Date d1 = formatter.parse(dt);
//
//            contactspecificinfoArrayList.clear();
//            for (int j = 0; j < tmplist.size(); j++) {
//                Date d2 = formatter.parse(tmplist.get(j).getDate());
//                if (d1.equals(d2))
//                    contactspecificinfoArrayList.add(tmplist.get(j));
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        if (contactspecificinfoArrayList.size() == 0)
//            nodata.setVisibility(View.VISIBLE);
//        else
//            nodata.setVisibility(View.GONE);
//
//        apercuAdapter.notifyDataSetChanged();
//
//    }

    private void getReceipt() {
        String lastid = "-1";
        List<contactspecificinfodat> list = contactspecificinfodat.listAll(contactspecificinfodat.class);
        if (list.size() > 0)
            lastid = String.valueOf(list.get(list.size() - 1).getLastId());


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String phoneNumber = temp.get(0).ContactNumber;
        Call<contactspecificinfo[]> call = apiService.getforspecificcontact(phoneNumber, lastid);
        call.enqueue(new Callback<contactspecificinfo[]>() {
            @Override
            public void onResponse(Call<contactspecificinfo[]> call, Response<contactspecificinfo[]> response) {
                contactspecificinfo[] contactspecificinfos = response.body();
                if (contactspecificinfos != null && contactspecificinfos.length != 0) {
                    for (int i = 0; i < contactspecificinfos.length; i++) {
                        Log.d(TAG, "OnDATAResponse: " + contactspecificinfos[i].getId());

                        if (contactspecificinfos[i].getId() != -2) {
                            contactspecificinfodat cft = new contactspecificinfodat(
                                    contactspecificinfos[i].getBillamount(),
                                    contactspecificinfos[i].getImgurl(),
                                    contactspecificinfos[i].getType(),
                                    contactspecificinfos[i].getMerchantname(),
                                    contactspecificinfos[i].getDate(),
                                    contactspecificinfos[i].getMerchantreceiptid(),
                                    contactspecificinfos[i].getId()
                            );
                            cft.save();
                        }

                    }
                    merchantNameList.clear();
                    tmplist.clear();
                    contactspecificinfoArrayList.clear();
                    setdata();
                }
            }

            @Override
            public void onFailure(Call<contactspecificinfo[]> call, Throwable t) {
                Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setdata() {
        List<contactspecificinfodat> list = contactspecificinfodat.listAll(contactspecificinfodat.class);

        merchantNameList.add("--Select--");

        for (int i = 0; i < list.size(); i++) {
            contactspecificinfo cpt = new contactspecificinfo();
            cpt.setDate(list.get(i).getDate());
            cpt.setId(list.get(i).getLastId());
            cpt.setImgurl(list.get(i).getImgurl());
            cpt.setMerchantname(list.get(i).getMerchantname());
            cpt.setMerchantreceiptid(list.get(i).getMerchantreceiptid());
            cpt.setType(list.get(i).getType());
            cpt.setBillamount(list.get(i).getBillamount());
            contactspecificinfoArrayList.add(cpt);
            tmplist.add(cpt);

            if (checkduplicate(list.get(i).getMerchantname()))
                merchantNameList.add(list.get(i).getMerchantname());

        }
        Log.d(TAG, "initialize: " + list.size());

        if (contactspecificinfoArrayList.size() == 0)
            nodata.setVisibility(View.VISIBLE);
        else
            nodata.setVisibility(View.GONE);

        apercuAdapter.notifyDataSetChanged();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, merchantNameList);
        sp.setAdapter(adapter);


        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position != 0) {
                    date.setText("Choose Date");
                    sp_price.setSelection(0);
                    contactspecificinfoArrayList.clear();
                    for (int j = 0; j < tmplist.size(); j++) {
                        if (tmplist.get(j).getMerchantname().equalsIgnoreCase(sp.getSelectedItem().toString())) {
                            contactspecificinfoArrayList.add(tmplist.get(j));
                        }
                        //
                    }

                    if (contactspecificinfoArrayList.size() == 0)
                        nodata.setVisibility(View.VISIBLE);
                    else
                        nodata.setVisibility(View.GONE);
                    apercuAdapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_price.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    date.setText("Choose Date");
                    sp.setSelection(0);
                }

                if (position == 1) {
                    filterdata(0, 500);
                } else if (position == 2) {
                    filterdata(500, 1000);
                } else if (position == 3) {
                    filterdata(1000, 5000);
                } else if (position == 4) {
                    filterdata(5000, 10000);
                } else if (position == 5) {
                    filterdata(10000, 100000);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }
}
