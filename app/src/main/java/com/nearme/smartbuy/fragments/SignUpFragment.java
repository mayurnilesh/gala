package com.nearme.smartbuy.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.auth0.android.Auth0;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.firebase.consumerFirebasepayload;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.consumerpayload;
import com.nearme.smartbuy.startup.PhoneActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpFragment extends Fragment {
    private Context curContext;
    private EditText userNameTxt;
    private EditText userName;
    private Spinner spGender;
    private String firebaseToken;
    private ProgressBar pb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        curContext = this.getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View curView = inflater.inflate(R.layout.signup_fragment, container, false);
        userNameTxt = curView.findViewById(R.id.mbno);
        pb = curView.findViewById(R.id.pb);
        userName = curView.findViewById(R.id.name);
        spGender = curView.findViewById(R.id.spgender);
        curView.findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userName.getText().toString().trim().isEmpty()) {
                    Toast.makeText(curContext, "Enter Name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!(userNameTxt.getText().toString().trim().equals("")) && userNameTxt.getText().toString().length() == 10) {
                    String enteredNumber = "+91" + userNameTxt.getText().toString();
                    userdetails.deleteAll(userdetails.class);
                    pb.setVisibility(View.VISIBLE);
                    checkALreadyLogin(enteredNumber);
                } else {
                    Toast.makeText(curContext, "Please Enter Correct Mobile Number.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                firebaseToken = instanceIdResult.getToken();
                Log.d("Toen", "onSuccess: " + firebaseToken);
            }
        });


        return curView;
    }

    private void checkALreadyLogin(final String number) {

        consumerFirebasepayload firebasepayload = new consumerFirebasepayload();
        firebasepayload.FirebaseInstanceID = firebaseToken;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<consumerpayload> call = apiService.LoginUser(number, new Gson().toJson(firebasepayload));
        call.enqueue(new Callback<consumerpayload>() {
            @Override
            public void onResponse(Call<consumerpayload> call, Response<consumerpayload> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null && response.body().contact != null) {
                    Toast.makeText(getActivity(), "This number is already registered. please try to Login", Toast.LENGTH_SHORT).show();
                } else {
                    Intent PIntent = new Intent(curContext, PhoneActivity.class);
                    PIntent.putExtra("isRegistraion", true);
                    PIntent.putExtra("number", number);
                    PIntent.putExtra("name", userName.getText().toString());
                    PIntent.putExtra("selectedSex", spGender.getSelectedItemPosition() + 1);
                    startActivity(PIntent);
                }
            }

            @Override
            public void onFailure(Call<consumerpayload> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error ocuured.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
