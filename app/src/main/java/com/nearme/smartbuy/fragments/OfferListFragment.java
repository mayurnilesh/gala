package com.nearme.smartbuy.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.gson.Gson;
import com.nearme.smartbuy.JobsAroundForm;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.adapter.BottomSheetAdapter;
import com.nearme.smartbuy.adapter.MultiSelectorAdapter;
import com.nearme.smartbuy.adapter.OfferAdapter;
import com.nearme.smartbuy.db.Categories;
import com.nearme.smartbuy.db.maxadidreceived;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.db.negotiationrequests;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.geohashutil.BoundingHashes;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.geohashutil.latlngcls;
import com.nearme.smartbuy.model.BottomSheetItem;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.rest.AdPayLoadResponse;
import com.nearme.smartbuy.rest.AdRequestPayload;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.CategoriesPayLoad;
import com.nearme.smartbuy.rest.LastReceivedAdStruct;
import com.nearme.smartbuy.rest.NegotationPayLoad;
import com.nearme.smartbuy.startup.MainActivity;
import com.nearme.smartbuy.utility.Constant;
import com.nearme.smartbuy.utility.MyAdvStatsPOJO;
import com.nearme.smartbuy.utility.PrefManager;
import com.nearme.smartbuy.utility.RecyclerTouchListener;
import com.nearme.smartbuy.utility.UserStatsPOJO;
import com.qhutch.bottomsheetlayout.BottomSheetLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import kotlin.Pair;
import kotlin.collections.MapsKt;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;

public class OfferListFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener, DatePickerDialog.OnDateSetListener {

    View v;
    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    Context mContext;
    boolean b = true;
    String TAG = "OfferListFragment";
    LinearLayout linmain;
    ImageView close;
    RecyclerView rec_list, rec_category;
    TextView categorySubmit;
    OfferAdapter adapter;
    List<list_item_base> adValues = new ArrayList<>();
    List<mybizannouncements> ads;
    int tmp = 0;
    TextView tempmap;
    List<BottomSheetItem> categoryList = new ArrayList<>();
    List<Categories> categories = new ArrayList<>();
    BottomSheetAdapter bottomSheetAdapter;
    boolean isLoadAd = true;
    BottomSheetLayout bottomSheetLayout;
    LatLng myLatLng;
    TextView selectedCate;
    private DatabaseReference mDatabase;
    private Spinner spCategory;
    private PrefManager prefManager;
    Map<Marker, Merchant> markerMap = new HashMap<Marker, Merchant>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.layout_offer_list, container, false);
        linmain = v.findViewById(R.id.linmain);
        close = v.findViewById(R.id.close);
        rec_list = v.findViewById(R.id.rec_list);
        rec_category = v.findViewById(R.id.rec_category);
        categorySubmit = v.findViewById(R.id.categorySubmit);
        bottomSheetLayout = v.findViewById(R.id.bottomSheetLayout);
        selectedCate = v.findViewById(R.id.selectedCate);
        spCategory = v.findViewById(R.id.spCategory);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mContext = container.getContext();
        prefManager = new PrefManager(mContext);
        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragmentSupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFrag.getMapAsync(this);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linmain.setVisibility(View.GONE);
            }
        });

        List<userdetails> temp = userdetails.listAll(userdetails.class);

        Log.d(TAG, "onCreateView: " + String.valueOf(temp.get(0).idconsumers));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        adapter = new OfferAdapter(adValues, getContext());
        rec_list.setAdapter(adapter);

        rec_list.addOnItemTouchListener(new RecyclerTouchListener(mContext, rec_list, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                final LinearLayout lv_negotiation = view.findViewById(R.id.lv_negotiation);

                lv_negotiation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startNegotiation(position);
                    }
                });
            }

            @Override
            public void onLongClick(View view, final int pos) {
            }
        }));

        setupCategoryData();

        return v;
    }

    @SuppressLint("SetTextI18n")
    public void setCategory() {
        int count = 0;
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).isSelected) {
                count++;
            }
        }
        selectedCate.setText(count + " category selected");
    }

    private void setupCategoryData() {
        categories = Categories.listAll(Categories.class);
        if (categories.size() < 1) {
            getCategories();
            return;
        }

        if (loadArray("category", mContext).size() == 0) {
            for (int i = 0; i < categories.size(); i++) {
                categories.get(i).isSelected = true;
            }
        } else {
            int value = 0;
            ArrayList<Boolean> booleanArrayList = loadArray("category", mContext);
            for (int i = 0; i < booleanArrayList.size(); i++) {
                categories.get(i).isSelected = booleanArrayList.get(i);
                if (booleanArrayList.get(i))
                    value++;
            }
            selectedCate.setText(value + " category selected");
        }

        final MultiSelectorAdapter adapterTagSpinnerItem = new MultiSelectorAdapter(OfferListFragment.this, mContext, 0, categories, spCategory);
        spCategory.setAdapter(adapterTagSpinnerItem);

        v.findViewById(R.id.filter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Boolean> booleanArrayList = new ArrayList<>();
                for (int i = 0; i < categories.size(); i++) {
                    booleanArrayList.add(categories.get(i).isSelected);
                }
                storeArray(booleanArrayList, "category", mContext);
                getAds(myLatLng);
            }
        });

    }
    private void UpdateMyLocation(String geoHash) {
        try {

            Query query = mDatabase
                    .child("userLocation/" + geoHash);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    HashMap<String, Object> myUserStatsPOJO = new HashMap<String, Object>();
                    if (dataSnapshot.getChildrenCount() > 0) {
                        //username found
                        myUserStatsPOJO = (HashMap<String, Object>) dataSnapshot.getValue();
                    } else {
                        // username not found
                        //myMerchantPOJOs = new ArrayList<>();
//                        mDatabase.child("userLocation/" + geoHash).push();

                        //only merchants push initally
                    }
                    int _mcount = 0;
                    int _fcount = 0;
                    int _bdayCount = 0;
                    try {
                        _mcount = Integer.parseInt(myUserStatsPOJO.get("maleCount").toString());// getDownloadCount();
                        _fcount = Integer.parseInt(myUserStatsPOJO.get("femaleCount").toString());
                        _bdayCount = Integer.parseInt(myUserStatsPOJO.get("bdayCount").toString());
                    } catch (Exception ex) {

                    }
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String todaysdate = dateFormat.format(calendar.getTime());
                    userdetails curUser = userdetails.first(userdetails.class);
                    List<String> users = (List<String>) myUserStatsPOJO.get("users");

                    if (users == null) {
                        users = new ArrayList<>();
                    }
                    if (!(users).contains(curUser.idconsumers)) {
                        users.add(curUser.idconsumers);
                        if (curUser.Sex == 1) {
                            _mcount++;
                        } else if (curUser.Sex == 2) {
                            _fcount++;
                        }
                        if (todaysdate.equals(curUser.DOB)) {
                            _bdayCount++;
                        }
                    }


                    UserStatsPOJO _myAdvStatsPOJO = new UserStatsPOJO();
                    _myAdvStatsPOJO.SetfemaleCount(_fcount);
                    _myAdvStatsPOJO.SetmaleCount(_mcount);
                    _myAdvStatsPOJO.SetbdayCount(_bdayCount);
                    _myAdvStatsPOJO.SetUsers(users);
                    try {
                        HashMap<String, Object> myUserStatPOJO = new HashMap<String, Object>();
                        myUserStatPOJO.put(curUser.idconsumers,geoHash);
                        mDatabase.child("userLocation").child("userLiveGeoHash").updateChildren(myUserStatPOJO);
                    }
                    catch (Exception ex)
                    {
                        System.out.println("Failed to update loc");
                    }
                    mDatabase.child("userLocation").
                            child(geoHash).setValue(_myAdvStatsPOJO);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        catch (Exception ex)
        {
            System.out.println("Failed to update count");
        }
    }
    private void getAds(LatLng latLng) {
        isLoadAd = false;
        List<LastReceivedAdStruct> adStructs = new ArrayList<>();
        ArrayList<String> geoHashes = new ArrayList<>();
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;
        String geoHash = GeoHash.fromCoordinates(latitude, longitude, 5).toString();
        UpdateMyLocation(geoHash);
        List<latlngcls> boundedLatLngs = BoundingHashes.create_geohash(latitude, longitude, 2000, 5, false, 1, 12);

        for (int i = 0; i < boundedLatLngs.size(); i++) {
            latlngcls temp = boundedLatLngs.get(i);
            geoHashes.add(GeoHash.fromCoordinates(temp.x, temp.y, 5).toString());
        }

        HashSet<String> unique = new HashSet<String>(geoHashes);
        List<String> uniqueGeoHash = new ArrayList<String>(unique);

        for (int i = 0; i < categories.size(); i++) {
            LastReceivedAdStruct lastReceivedAdStruct = new LastReceivedAdStruct();
            lastReceivedAdStruct.geoHash = uniqueGeoHash;
            lastReceivedAdStruct.lastReceivedAdId = getLastId(categories.get(i).catid);
            lastReceivedAdStruct.Category = (categories.get(i).catid - 1);
            adStructs.add(lastReceivedAdStruct);
        }

        AdRequestPayload adRequestPayload = new AdRequestPayload();
        adRequestPayload.lastReceivedAdStructList = adStructs;

        Gson gson = new Gson();
        String jsonConvertedlastReceivedAdStructList = gson.toJson(adStructs);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<AdPayLoadResponse[]> call = apiService.getText(adStructs);
        call.enqueue(new Callback<AdPayLoadResponse[]>() {
            @Override
            public void onResponse(Call<AdPayLoadResponse[]> call, Response<AdPayLoadResponse[]> response) {
                AdPayLoadResponse[] receivedAds = response.body();
                if (receivedAds != null && receivedAds.length > 0) {
                    for (int i = 0; i < receivedAds.length; i++) {
                        final AdPayLoadResponse receivedAd = receivedAds[i];
                        try {
                            Query query = mDatabase
                                    .child("AdsStas/" + receivedAd.merchantid + "_" + receivedAd.geo + "_" + receivedAd.Id);
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    HashMap<String, Object> myAdvStatsPOJO = new HashMap<String, Object>();
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        //username found
                                        myAdvStatsPOJO = (HashMap<String, Object>) dataSnapshot.getValue();
                                    } else {
                                        // username not found
                                        //myMerchantPOJOs = new ArrayList<>();
                                        mDatabase.child("AdsStas/" + receivedAd.merchantid + "_" + receivedAd.geo + "_" + receivedAd.Id).push();
                                    }
                                    int _dcount = 0;
                                    int _vCount = 0;
                                    try {
                                        _dcount = Integer.parseInt(myAdvStatsPOJO.get("downloadCount").toString());// getDownloadCount();
                                        _vCount = Integer.parseInt(myAdvStatsPOJO.get("viewCount").toString());
                                    } catch (Exception ex) {

                                    }
                                    _dcount++;
//                                    HashMap<String, Object> myObjectAsDict = new HashMap<>();

                                    MyAdvStatsPOJO _myAdvStatsPOJO = new MyAdvStatsPOJO();

                                    _myAdvStatsPOJO.SetDownloadCount(_dcount);
                                    _myAdvStatsPOJO.setViewCount(_vCount);
                                    mDatabase.child("AdsStas").
                                            child(receivedAd.merchantid + "_" + receivedAd.geo + "_" + receivedAd.Id).setValue(_myAdvStatsPOJO);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } catch (Exception ex) {
                            System.out.println("Failed to update count");
                        }
                        mybizannouncements obj = new mybizannouncements(receivedAds[i].Id, receivedAds[i].geo, receivedAds[i].lat, receivedAds[i].lng, receivedAds[i].merchantid,
                                receivedAds[i].cat, receivedAds[i].tilldate, receivedAds[i].tillmonth,
                                receivedAds[i].tillyear, receivedAds[i].fromdate, receivedAds[i].frommonth
                                , receivedAds[i].fromyear, receivedAds[i].offercode,
                                receivedAds[i].itemdesc, receivedAds[i].imgUrl, receivedAds[i].shopname,
                                receivedAds[i].shopDp, receivedAds[i].negotiate, receivedAds[i].minBusiness,receivedAds[i].contatNumber);
                        obj.save();
                    }
                    //mapFrag.getMapAsync(OfferListFragment.this);
                }
            }

            @Override
            public void onFailure(Call<AdPayLoadResponse[]> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
        //Merchant.last(Merchant.class)
        boolean callAPI = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            Date strDate = sdf.parse(prefManager.getFirstTimeDate());

            if ((sdf.parse(sdf.format(Calendar.getInstance().getTime()))).after(strDate)) {
                callAPI = true;
            }
        } catch (Exception ex) {
            callAPI = true;
        }
        if (callAPI) {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            String formattedDate = df.format(c);
            prefManager.setFirstTimeDate(formattedDate);

            Call<Merchant[]> merscall = apiService.getAroundMerchant(new Gson().toJson(geoHashes));
            merscall.enqueue(new Callback<Merchant[]>() {
                @Override
                public void onResponse(Call<Merchant[]> call, Response<Merchant[]> response) {
                    Merchant[] receivedMerchs = response.body();
                    Log.d(TAG, "onResponse: " + receivedMerchs.length);
                    Merchant.deleteAll(Merchant.class);
                    if (receivedMerchs.length > 0) {
                        for (Merchant merchant : receivedMerchs) {
                            try {

                                new Merchant(merchant).save();
                            } catch (Exception ex) {
                                System.out.print("eror");
                            }
                        }
                    }
                    mapFrag.getMapAsync(OfferListFragment.this);
                }

                @Override
                public void onFailure(Call<Merchant[]> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                }
            });
        } else {
            mapFrag.getMapAsync(OfferListFragment.this);
        }
    }

    private String getLastId(int catId) {
        List<mybizannouncements> postedAds = mybizannouncements.listAll(mybizannouncements.class);
        int maxId = 0;
        for (int i = 0; i < postedAds.size(); i++) {
            String cat = postedAds.get(i).cat;
            String bit = Long.toBinaryString(Long.parseLong(cat));
            String mainBit = new StringBuilder(bit).reverse().toString();
            if (mainBit.length() >= catId) {
                if (Integer.parseInt(String.valueOf(mainBit.charAt(catId - 1))) == 1) {
                    if (postedAds.get(i).notificationid > maxId) {
                        maxId = postedAds.get(i).notificationid;
                    }
                }
            }
        }
        return maxId == 0 ? "-1" : String.valueOf(maxId);
    }

    private void getCategories() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesPayLoad[]> call = apiService.getCategories();
        call.enqueue(new Callback<CategoriesPayLoad[]>() {
            @Override
            public void onResponse(Call<CategoriesPayLoad[]> call, Response<CategoriesPayLoad[]> response) {
                CategoriesPayLoad[] categoriesPayLoad = response.body();
                if (categoriesPayLoad != null) {
                    for (CategoriesPayLoad aCategoriesPayLoad : categoriesPayLoad) {
                        Categories categories = new Categories();
                        categories.catid = aCategoriesPayLoad.catid;
                        categories.catimg = aCategoriesPayLoad.catimg;
                        categories.catname = aCategoriesPayLoad.catname;
                        categories.save();
                    }
                }
                setupCategoryData();
            }

            @Override
            public void onFailure(Call<CategoriesPayLoad[]> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        ads = mybizannouncements.listAll(mybizannouncements.class);
        List<Merchant> merchants = Merchant.listAll(Merchant.class);
        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
                if (merchants != null) {
                    for (int i = 0; i < merchants.size(); i++) {
//                    Log.d(TAG, "onMapReady: " + ads.get(i).itemdesc + " " + ads.get(i).lat + " " + ads.get(i).lng);
//                    Date c = Calendar.getInstance().getTime();
//                    Log.d(TAG, "Current time => " + c);
//
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//                    boolean datesInRange = false;
//                    try {
//                        Date fromdate = sdf.parse(ads.get(i).fromyear + "-" + ads.get(i).frommonth + "-" + ads.get(i).fromdate);
//                        Date tilldate = sdf.parse(ads.get(i).tillyear + "-" + ads.get(i).tillmonth + "-" + ads.get(i).tilldate);
//                        Date CurDate = sdf.parse(sdf.format(c));
//
//                        if (!tilldate.before(CurDate)) {
//                            Log.i("app", "Date1 is after Date2");
//                            datesInRange = true;
//                        }
//
//                    } catch (Exception ex) {
//
//                    }

                        if (merchants.get(i).latitude != null && merchants.get(i).longitude != null) {
                            boolean merchantHasAds = false;
                            for (int j = 0; j < ads.size(); j++) {
                                if (ads.get(j).merchantid.equals(merchants.get(i).merchantId)) {
                                    merchantHasAds = true;
                                    break;
                                }
                            }
                            if (merchantHasAds) {
                                boolean hasMedCat = false;
                                for (int j = 0; j < ads.size(); j++) {
                                    if (ads.get(j).cat.equals("536870912")) {
                                        hasMedCat = true;
                                        break;
                                    }
                                }

                                createMarker(Double.parseDouble(merchants.get(i).latitude),
                                        Double.parseDouble(merchants.get(i).longitude),
                                        merchants.get(i), "", R.drawable.offershop);

                                if(hasMedCat) {
                                    createMarker(Double.parseDouble(merchants.get(i).latitude),
                                            Double.parseDouble(merchants.get(i).longitude),
                                            merchants.get(i), "", R.drawable.medstr);
                                }


                            }
                            else {
                                createMarker(Double.parseDouble(merchants.get(i).latitude),
                                        Double.parseDouble(merchants.get(i).longitude),
                                        merchants.get(i), "", R.drawable.shoppingstore);
                            }
//                            if (tmp == 0) {
//
//                                tmp = 1;
//                            } else if (tmp == 1) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin2);
//                                tmp = 2;
//                            } else if (tmp == 2) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin3);
//                                tmp = 3;
//                            } else if (tmp == 3) {
//                                createMarker(ads.get(i).lat, ads.get(i).lng, ads.get(i).shopname, "", R.drawable.pin4);
//                                tmp = 0;
//                            }
                        }
                    }
                }
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
            if (merchants != null) {
                for (int i = 0; i < merchants.size(); i++) {
                    Log.d(TAG, "onMapReady: " + ads.get(i).itemdesc + " " + ads.get(i).lat + " " + ads.get(i).lng);
                    if (merchants.get(i).latitude != null && merchants.get(i).longitude != null) {

                        boolean merchantHasAds = false;
                        for (int j = 0; j < ads.size(); j++) {
                            if (ads.get(j).merchantid.equals(merchants.get(i).merchantId)) {
                                merchantHasAds = true;
                                break;
                            }

                        }
                        if (merchantHasAds) {
                            boolean hasMedCat = false;
                            for (int j = 0; j < ads.size(); j++) {
                                if (ads.get(j).cat.equals("536870912")) {
                                    hasMedCat = true;
                                    break;
                                }
                            }

                            createMarker(Double.parseDouble(merchants.get(i).latitude),
                                    Double.parseDouble(merchants.get(i).longitude),
                                    merchants.get(i), "", R.drawable.offershop);

                            if(hasMedCat) {
                                createMarker(Double.parseDouble(merchants.get(i).latitude),
                                        Double.parseDouble(merchants.get(i).longitude),
                                        merchants.get(i), "", R.drawable.medstr);
                            }

                        }
                        else {
                            createMarker(Double.parseDouble(merchants.get(i).latitude),
                                    Double.parseDouble(merchants.get(i).longitude),
                                    merchants.get(i), "", R.drawable.shoppingstore);
                        }
                    }
                }
            }
        }
    }


    protected Marker createMarker(double latitude, double longitude, Merchant merchant, String snippet, int iconResID) {

        mGoogleMap.setOnMarkerClickListener(this);

        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(merchant.merchantName)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
        if (!markerMap.containsKey(marker))
            markerMap.put(marker, merchant);
        /*.snippet(snippet)*/
        return marker;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getContext())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(mContext, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            } catch (Exception ex) {

            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (getActivity() != null) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);
        myLatLng = latLng;
        if (isLoadAd)
            getAds(latLng);

        //move map camera
        if (b) {
            b = false;
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Merchant merchantInfo = markerMap.get(marker);
        Log.d(TAG, "onMarkerClick: " + marker.getTitle());
        linmain.setVisibility(View.VISIBLE);
        adValues.clear();
        for (int i = 0; i < ads.size(); i++) {
            if (merchantInfo != null && merchantInfo.merchantId.equals(ads.get(i).merchantid) && merchantInfo.geoHash.equalsIgnoreCase(ads.get(i).geoHash)) {
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                boolean datesInRange = false;
                try {
                    Date tilldate = sdf.parse(ads.get(i).tillyear + "-" + ads.get(i).tillmonth + "-" + ads.get(i).tilldate);
                    Date CurDate = sdf.parse(sdf.format(c));

                    if (!CurDate.after(tilldate)) {
                        Log.i("app", "Date1 is after Date2");
                        datesInRange = true;
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onMarkerClick: ");
                }
                if (datesInRange) {
                    if (checkDuplicate(ads.get(i).offercode, ads.get(i).itemdesc)) {
                        list_item_base itemBase = new list_item_base();
                        itemBase.setImageUrl(ads.get(i).imgUrl);
                        itemBase.setValidTill(ads.get(i).tilldate + "/" + ads.get(i).tillmonth + "/" + ads.get(i).tillyear);
                        itemBase.setShopName(ads.get(i).shopname);
                        itemBase.setOfferCode(ads.get(i).offercode);
                        itemBase.setLocation(ads.get(i).geoHash);
                        itemBase.setOfferDescription(ads.get(i).itemdesc);
                        itemBase.setShopDp(ads.get(i).shopDp);
                        itemBase.setNotificationid(ads.get(i).notificationid);
                        itemBase.setNegotiate(ads.get(i).negotiate);
                        itemBase.setMinBusiness(ads.get(i).minBusiness);
                        itemBase.setLng(ads.get(i).lng);
                        itemBase.setLat(ads.get(i).lat);
                        itemBase.setContactNumber(ads.get(i).contatNumber);
                        String mapurl = "https://www.google.com/maps/?q=" + ads.get(i).lat + "," + ads.get(i).lng;
                        itemBase.setLocation(mapurl);
                        adValues.add(itemBase);
                    }
                } else {
                    mybizannouncements tempData = ads.get(i);
                    tempData.delete();
                }
            }

        }
        adapter.notifyDataSetChanged();

        return false;
    }


    private Boolean checkDuplicate(String offerCode, String desc) {
        for (int i = 0; i < adValues.size(); i++) {
            if (adValues.get(i).offerCode.equalsIgnoreCase(offerCode) && adValues.get(i).offerDescription.equalsIgnoreCase(desc)) {
                return false;
            }
        }
        return true;
    }

    public void startNegotiation(final int position) {
        Log.d(TAG, "startNegotiation: " + adValues.get(position).validTill);
        final Dialog dialog = new Dialog(mContext, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialogue_negotiation);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText minSale = dialog.findViewById(R.id.MinSale);
        final String minVal = String.valueOf(adValues.get(position).minBusiness);
        minSale.setText(minVal);
        final EditText maxSale = dialog.findViewById(R.id.MaxSale);
        final EditText discount = dialog.findViewById(R.id.discount);
        final TextView shoppingdates = dialog.findViewById(R.id.shoppingdates);
        final TextView shoppintoDate = dialog.findViewById(R.id.shoppingtodates);
        final TextView choose_locatoin = dialog.findViewById(R.id.choose_locatoin);
        final EditText detail_address = dialog.findViewById(R.id.detail_address);
        final EditText description = dialog.findViewById(R.id.description);
        choose_locatoin.setEnabled(false);
        choose_locatoin.setText(adValues.get(position).location);
        choose_locatoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.isenable = true;
                Constant.isSearchother = false;
                tempmap = choose_locatoin;
                Places.initialize(getActivity(), "AIzaSyCYH_ZrSBHf3r_Ov04IuHNS8TaJDb-Fp_M");
                List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));
                Intent autocompleteIntent =
                        new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                                .build(getActivity());
                startActivityForResult(autocompleteIntent, 1);
            }
        });

        shoppingdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        shoppingdates.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        shoppintoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        shoppintoDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        TextView close = dialog.findViewById(R.id.tvsubmit);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkEditText(minSale, "Enter Minsale") && checkMinvalueNumber(minSale, minVal) && checkEditText(maxSale, "Enter Maxsale")
                        && checkEditTextfordisc(discount, "Enter Discount") && checkEditText(description, "Enter Description")
                        && checkTextView(shoppingdates, "Enter Minsale") && checkTextView(shoppintoDate, "Enter Minsale")
                        && checkMinvalue(minSale, maxSale) && checkDate(shoppingdates, shoppintoDate)
                        && compareDate(getActivity(), adValues.get(position).validTill, shoppingdates, shoppintoDate)) {
                    final NegotationPayLoad npl = new NegotationPayLoad();
                    List<mybizannouncements> bizValues = mybizannouncements.find(mybizannouncements.class, "notificationid = ? and lat=? and lng=?", new String[]{String.valueOf(adValues.get(position).notificationid), String.valueOf(adValues.get(position).lat), String.valueOf(adValues.get(position).lng)}, null, null, null);

                    List<userdetails> temp = userdetails.listAll(userdetails.class);
                    String phoneNumber = temp.get(0).ContactNumber;
                    npl.customercontact = phoneNumber;
                    npl.delieverd = 0;
                    final String geoHash = GeoHash.fromCoordinates(adValues.get(position).lat, adValues.get(position).lng, 5).toString();
                    npl.geohash = geoHash;// bizValues.get(0).geoHash;
                    npl.merchantid = bizValues.get(0).merchantid;
                    npl.description = description.getText().toString();
                    npl.minamount = Integer.valueOf(minSale.getText().toString());
                    npl.maxamount = Integer.valueOf(maxSale.getText().toString());
                    npl.DiscountExpectation = Integer.valueOf(discount.getText().toString());
                    npl.ShoppingProbableDates = shoppingdates.getText().toString() + " to " + shoppintoDate.getText().toString();
                    npl.notificationid = adValues.get(position).notificationid;


                    ApiInterface apiService =
                            ApiClient.getClient().create(ApiInterface.class);

                    String nplStr = new Gson().toJson(npl);

                    Call<String> call = apiService.StartNegotiate(nplStr);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {


                            String negId = response.body().toString();
                            Log.d("MYNEGOVALUES", npl.notificationid + " " + npl.geohash + " " + Integer.valueOf(negId));
                            negotiationrequests neg = new negotiationrequests(npl.notificationid,
                                    npl.geohash, Integer.valueOf(negId));

                            try {
                                neg.save();

                            } catch (Exception ex) {
                                int y = 40;
                                ++y;
                            }


                            Toast.makeText(mContext, "Negotation started", Toast.LENGTH_LONG).show();
                            dialog.dismiss();

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                            Toast.makeText(mContext, "Negotation Failed. Please re-send", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    });
                }
            }
        });


    }

    private static boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkEditTextfordisc(EditText et, String msg) {
        if (et.getText().toString().equals("") || Integer.parseInt(et.getText().toString()) > 99) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkTextView(TextView et, String msg) {
        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkMinvalue(EditText et, EditText msg) {
        if (Integer.parseInt(et.getText().toString()) > Integer.parseInt(msg.getText().toString())) {
            et.setError("Enter Less value then Max value");
            et.requestFocus();
            return false;
        }
        if (Integer.parseInt(et.getText().toString()) > 65535) {
            et.setError("Unit is too large!");
            et.requestFocus();
            return false;
        }
        if (Integer.parseInt(msg.getText().toString()) > 65535) {
            et.setError("Unit is too large!");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkMinvalueNumber(EditText et, String msg) {
        if (Integer.parseInt(et.getText().toString()) < Integer.parseInt(msg)) {
            et.setError("Minimum business required for negotiation is " + msg + " Rupees.");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean checkDate(TextView et, TextView et1) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(et.getText().toString());
            Date enddate = sdf.parse(et1.getText().toString());

            if (strDate.getTime() > enddate.getTime()) {
                et.setError("Enter lesser date from to date");
                et.requestFocus();
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static boolean compareDate(Context context, String date, TextView et, TextView et1) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date1 = sdf.parse(date.replace("/", "-"));
            Date date2 = sdf.parse(et.getText().toString());
            Date date3 = sdf.parse(et1.getText().toString());

            assert date2 != null;
            assert date1 != null;
            if (date2.getTime() > date1.getTime()) {
                et.setText("");
                et.setError("Enter lesser date from shopping date");
                et.requestFocus();
                Toast.makeText(context, "Enter lesser date from shopping date", Toast.LENGTH_SHORT).show();
                return false;
            }

            assert date3 != null;
            if (date3.getTime() > date1.getTime()) {
                et1.setText("");
                et1.setError("Enter lesser date from shopping date");
                Toast.makeText(context, "Enter lesser date from shopping date", Toast.LENGTH_SHORT).show();
                et1.requestFocus();
                return false;
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.d("TAG", "onActivityResult: " + place.getName());
                tempmap.setText(place.getName());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    public boolean storeArray(ArrayList<Boolean> array, String arrayName, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("categoryPref", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(arrayName + "_size", array.size());
        for (int i = 0; i < array.size(); i++)
            editor.putBoolean(arrayName + "_" + i, array.get(i));
        return editor.commit();
    }

    public ArrayList<Boolean> loadArray(String arrayName, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("categoryPref", 0);
        int size = prefs.getInt(arrayName + "_size", 0);
        ArrayList<Boolean> array = new ArrayList<>();
        for (int i = 0; i < size; i++)
            array.add(prefs.getBoolean(arrayName + "_" + i, false));
        return array;
    }

}
