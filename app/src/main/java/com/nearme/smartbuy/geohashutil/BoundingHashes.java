package com.nearme.smartbuy.geohashutil;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.ceil;
import static java.lang.Math.cos;
import static java.lang.Math.pow;

public class BoundingHashes {
    static boolean in_circle_check(double latitude, double longitude, double centre_lat, double centre_lon, double radius) {

        double x_diff = longitude - centre_lon;
        double y_diff = latitude - centre_lat;

        if (pow(x_diff, 2) + pow(y_diff, 2) <= pow(radius, 2))
            return true;

        return false;
    }

   public static latlngcls convert_to_latlon(double y, double x, double latitude, double longitude, Double final_lat, Double final_lon) {

        latlngcls obj = new latlngcls();
        double pi = 3.14159265359;

        long r_earth = 6371000;

        double lat_diff = (y / r_earth) * (180 / pi);
        double lon_diff = (x / r_earth) * (180 / pi) / cos(latitude * pi / 180);

        obj.x = latitude + lat_diff;
        obj.y = longitude + lon_diff;

        return obj;

    }

    static latlngcls get_centroid(double latitude, double longitude, double height, double width, Double x_cen, Double y_cen) {
        latlngcls obj = new latlngcls();


        obj.y = latitude + (height / 2);
        obj.x = longitude + (width / 2);

        return obj;//(x_cen,y_cen)
    }

   public static List<latlngcls> create_geohash(double latitude, double longitude, double radius, int precision, boolean georaptor_flag, int minlevel, int maxlevel) {

        double x = 0.0;
        double y = 0.0;

        List<Double> latpoints = new ArrayList<Double>();
        List<Double> lngpoints = new ArrayList<Double>();
        List<latlngcls> latlngpoints = new ArrayList<>();
        //  points = []
        List<String> geoHashList = new ArrayList<String>();
        //  geohashes = []

        Double[] grid_width = new Double[]{5009400.0, 1252300.0, 156500.0, 39100.0, 4900.0, 1200.0, 152.9, 38.2, 4.8, 1.2, 0.149, 0.0370};
        Double[] grid_height = new Double[]{4992600.0, 624100.0, 156000.0, 19500.0, 4900.0, 609.4, 152.4, 19.0, 4.8, 0.595, 0.149, 0.0199};

        double height = ((grid_height[precision - 1]) / 2);
        double width = ((grid_width[precision - 1]) / 2);

        Double lat_moves = ceil(radius / height);// #4
        Double lon_moves = ceil(radius / width);// #2

        //    for i in range(0, lat_moves):


        double temp_lat = 0.0;
        double temp_lon = 0.0;
        Double x_cen = new Double(0.0);
        Double y_cen = new Double(0.0);
        Double lat = new Double(0.0);
        Double lon = new Double(0.0);

        for (int i = 0; i < lat_moves; i++) {

            temp_lat = y + height * i;

            for (int j = 0; j < lon_moves; j++) {
                temp_lon = x + width * j;

                if (in_circle_check(temp_lat, temp_lon, y, x, radius) == true) {


                    latlngcls cenobj = get_centroid(temp_lat, temp_lon, height, width, x_cen, y_cen);

                    latlngcls latlon = convert_to_latlon(cenobj.y, cenobj.x, latitude, longitude, lat, lon);

                    latpoints.add(latlon.x);
                    lngpoints.add(latlon.y);
                    latlngpoints.add(latlon);

                    //  points += [[lat, lon]]
                    latlon = convert_to_latlon(-cenobj.y, cenobj.x, latitude, longitude, lat, lon);
                    latpoints.add(latlon.x);
                    lngpoints.add(latlon.y);
                    latlngpoints.add(latlon);
                    //    points += [[lat, lon]]
                    latlon = convert_to_latlon(cenobj.y, -cenobj.x, latitude, longitude, lat, lon);
                    latpoints.add(latlon.x);
                    lngpoints.add(latlon.y);
                    latlngpoints.add(latlon);
                    // points += [[lat, lon]]
                    latlon = convert_to_latlon(-cenobj.y, -cenobj.x, latitude, longitude, lat, lon);
                    latpoints.add(latlon.x);
                    lngpoints.add(latlon.y);
                    latlngpoints.add(latlon);
                    // points += [[lat, lon]]
                }
            }
        }

//            for point in points:
//    geohashes += [Geohash.encode(point[0], point[1], precision)]
//
//            if georaptor_flag:
//    georaptor_out = georaptor.compress(set(geohashes), int(minlevel), int(maxlevel))
//            return ','.join(georaptor_out)
//
//    else:
//            return ','.join(set(geohashes))
        return latlngpoints;
    }
}
