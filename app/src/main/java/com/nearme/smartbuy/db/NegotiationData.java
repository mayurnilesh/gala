package com.nearme.smartbuy.db;

import com.nearme.smartbuy.rest.AdPayLoad;
import com.orm.SugarRecord;

public class NegotiationData extends SugarRecord {

    public String customercontact;
    public String merchantid;
    public String geohash;
    public Integer minamount;
    public Integer maxamount;
    public Integer DiscountExpectation;
    public String ShoppingProbableDates;
    public String description;
    public Integer delieverd;
    public Integer response;
    public Integer notificationid;
    public Integer idnegotations;
    public Integer advance;
    public String respondedOn;
    public String imgUrl;
    public String offercode;
    public String itemdesc;

    public NegotiationData(){

    }

    public NegotiationData(String customercontact, String merchantid, String geohash, Integer minamount, Integer maxamount, Integer discountExpectation, String shoppingProbableDates, String description, Integer delieverd, Integer response, Integer notificationid, Integer idnegotations, Integer advance, String respondedOn, String imgUrl, String offercode, String itemdesc) {
        this.customercontact = customercontact;
        this.merchantid = merchantid;
        this.geohash = geohash;
        this.minamount = minamount;
        this.maxamount = maxamount;
        DiscountExpectation = discountExpectation;
        ShoppingProbableDates = shoppingProbableDates;
        this.description = description;
        this.delieverd = delieverd;
        this.response = response;
        this.notificationid = notificationid;
        this.idnegotations = idnegotations;
        this.advance = advance;
        this.respondedOn = respondedOn;
        this.imgUrl = imgUrl;
        this.offercode = offercode;
        this.itemdesc = itemdesc;
    }
}
