package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

/**
 * Created by gopi.komanduri on 12/10/18.
 */

public class negotiationrequests extends SugarRecord {
    public Integer notificationid;
    public String geohash;
    public Integer negotiationid;


    public negotiationrequests(Integer notificationid, String geohash, Integer negotiationid) {
        this.notificationid = notificationid;
        this.geohash = geohash;
        this.negotiationid = negotiationid;
    }

    public negotiationrequests()
    {

    }
}
