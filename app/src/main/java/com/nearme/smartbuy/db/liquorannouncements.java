package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

public class liquorannouncements extends SugarRecord {

    public int notificationid;
    public String timestamp;
    public String validFromD;
    public String validFromM;
    public String validFromY;
    public String validTillD;
    public String validTillM;
    public String validTillY;
    public Integer category;
    public String geohash;
    public String areaname;
    public String imgurl;
    public String imghash;
    public String offer;
    public Long shopid;
    public String offerDescription;
    public Integer negotiate;
    public Integer minBusiness;


    public liquorannouncements(int notificationid, String timestamp,
                               String validFromD, String validFromM,
                               String validFromY, String validTillD, String validTillM,
                               String validTillY, Integer category, String geohash,
                               String areaname, String imgurl, String imghash,
                               String offer, Long shopid, String offerDescription) {
        this.notificationid = notificationid;
        this.timestamp = timestamp;
        this.validFromD = validFromD;
        this.validFromM = validFromM;
        this.validFromY = validFromY;
        this.validTillD = validTillD;
        this.validTillM = validTillM;
        this.validTillY = validTillY;
        this.category = category;
        this.geohash = geohash;
        this.areaname = areaname;
        this.imgurl = imgurl;
        this.imghash = imghash;
        this.offer = offer;
        this.shopid = shopid;
        this.offerDescription = offerDescription;
    }

    public liquorannouncements() {
    }
}
