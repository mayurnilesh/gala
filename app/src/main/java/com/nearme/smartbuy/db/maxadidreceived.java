package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

/**
 * Created by gopi.komanduri on 26/07/18.
 */

public class maxadidreceived  extends SugarRecord {
    public String geoHash;
    public int notificationid;
    public int status=0;

    public maxadidreceived(String geoHash, int notificationid) {
        this.geoHash = geoHash;
        this.notificationid = notificationid;
    }
    public maxadidreceived()
    {

    }
}
