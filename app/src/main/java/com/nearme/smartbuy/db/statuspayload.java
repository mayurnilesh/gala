package com.nearme.smartbuy.db;

public class statuspayload {
    public Integer idnotification;
    public String registeredcontactnumber;
    public Integer finaldiscount;
    public String validfrom;
    public String validto;
    public String contactOn;
    public String geohashes;
    public String place;
    public Integer join;
    public Integer extraforbringing;
    public Integer joinedcount;
    public String shoppingdate;
    public String offerdesc;
    public String Imgurl;
    public Integer value;
    public Integer flat;
    public Integer ispercentage;

    public String getImgurl() {
        return Imgurl;
    }

    public void setImgurl(String imgurl) {
        this.Imgurl = imgurl;
    }

    public String getOfferdesc() {
        return offerdesc;
    }

    public void setOfferdesc(String offerdesc) {
        this.offerdesc = offerdesc;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getFlat() {
        return flat;
    }

    public void setFlat(Integer flat) {
        this.flat = flat;
    }

    public Integer getIspercentage() {
        return ispercentage;
    }

    public void setIspercentage(Integer ispercentage) {
        this.ispercentage = ispercentage;
    }

    public Integer getIdnotification() {
        return idnotification;
    }

    public void setIdnotification(Integer idnotification) {
        this.idnotification = idnotification;
    }

    public String getRegisteredcontactnumber() {
        return registeredcontactnumber;
    }

    public void setRegisteredcontactnumber(String registeredcontactnumber) {
        this.registeredcontactnumber = registeredcontactnumber;
    }

    public Integer getFinaldiscount() {
        return finaldiscount;
    }

    public void setFinaldiscount(Integer finaldiscount) {
        this.finaldiscount = finaldiscount;
    }

    public String getValidfrom() {
        return validfrom;
    }

    public void setValidfrom(String validfrom) {
        this.validfrom = validfrom;
    }

    public String getValidto() {
        return validto;
    }

    public void setValidto(String validto) {
        this.validto = validto;
    }

    public String getContactOn() {
        return contactOn;
    }

    public void setContactOn(String contactOn) {
        this.contactOn = contactOn;
    }

    public String getGeohashes() {
        return geohashes;
    }

    public void setGeohashes(String geohashes) {
        this.geohashes = geohashes;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Integer getJoin() {
        return join;
    }

    public void setJoin(Integer join) {
        this.join = join;
    }

    public Integer getExtraforbringing() {
        return extraforbringing;
    }

    public void setExtraforbringing(Integer extraforbringing) {
        this.extraforbringing = extraforbringing;
    }

    public Integer getJoinedcount() {
        return joinedcount;
    }

    public void setJoinedcount(Integer joinedcount) {
        this.joinedcount = joinedcount;
    }

    public String getShoppingdate() {
        return shoppingdate;
    }

    public void setShoppingdate(String shoppingdate) {
        this.shoppingdate = shoppingdate;
    }
}
