package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

public class contactspecificinfodat extends SugarRecord {

    public String imgurl;
    public Integer type;
    public String merchantname;
    public String date;
    public String merchantreceiptid;
    public String billamount;
    public Integer lastId;



    public contactspecificinfodat(String billamount,String imgurl, Integer type, String merchantname, String date, String merchantreceiptid, Integer lastId) {
        this.billamount = billamount;
        this.imgurl = imgurl;
        this.type = type;
        this.merchantname = merchantname;
        this.date = date;
        this.merchantreceiptid = merchantreceiptid;
        this.lastId = lastId;
    }

    public contactspecificinfodat() {
    }

    public String getBillamount() {
        return billamount;
    }

    public void setBillamount(String billamount) {
        this.billamount = billamount;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMerchantreceiptid() {
        return merchantreceiptid;
    }

    public void setMerchantreceiptid(String merchantreceiptid) {
        this.merchantreceiptid = merchantreceiptid;
    }

    public Integer getLastId() {
        return lastId;
    }

    public void setId(Integer id) {
        lastId = id;
    }
}
