package com.nearme.smartbuy.db;

import com.nearme.smartbuy.rest.AdPayLoad;
import com.orm.SugarRecord;

public class NegotiationResponse  {
    private String customercontact;
    private String merchantid;
    private String geohash;
    private Integer minamount;
    private Integer maxamount;
    private Integer DiscountExpectation;
    private String ShoppingProbableDates;
    private String description;
    private Integer delieverd;
    private Integer response;
    private Integer notificationid;
    private Integer idnegotations;
    private Integer advance;
    private String respondedOn;
    AdPayLoad adObj;

    public NegotiationResponse() {
    }

    public NegotiationResponse(String customercontact, String merchantid, String geohash, Integer minamount, Integer maxamount, Integer discountExpectation, String shoppingProbableDates, String description, Integer delieverd, Integer response, Integer notificationid, Integer idnegotations, Integer advance, String respondedOn, AdPayLoad adObj) {
        this.customercontact = customercontact;
        this.merchantid = merchantid;
        this.geohash = geohash;
        this.minamount = minamount;
        this.maxamount = maxamount;
        DiscountExpectation = discountExpectation;
        ShoppingProbableDates = shoppingProbableDates;
        this.description = description;
        this.delieverd = delieverd;
        this.response = response;
        this.notificationid = notificationid;
        this.idnegotations = idnegotations;
        this.advance = advance;
        this.respondedOn = respondedOn;
        this.adObj = adObj;
    }

    public AdPayLoad getAdObj() {
        return adObj;
    }

    public void setAdObj(AdPayLoad adObj) {
        this.adObj = adObj;
    }

    public String getCustomercontact() {
        return customercontact;
    }

    public void setCustomercontact(String customercontact) {
        this.customercontact = customercontact;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public Integer getMinamount() {
        return minamount;
    }

    public void setMinamount(Integer minamount) {
        this.minamount = minamount;
    }

    public Integer getMaxamount() {
        return maxamount;
    }

    public void setMaxamount(Integer maxamount) {
        this.maxamount = maxamount;
    }

    public Integer getDiscountExpectation() {
        return DiscountExpectation;
    }

    public void setDiscountExpectation(Integer discountExpectation) {
        DiscountExpectation = discountExpectation;
    }

    public String getShoppingProbableDates() {
        return ShoppingProbableDates;
    }

    public void setShoppingProbableDates(String shoppingProbableDates) {
        ShoppingProbableDates = shoppingProbableDates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDelieverd() {
        return delieverd;
    }

    public void setDelieverd(Integer delieverd) {
        this.delieverd = delieverd;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public Integer getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(Integer notificationid) {
        this.notificationid = notificationid;
    }

    public Integer getIdnegotations() {
        return idnegotations;
    }

    public void setIdnegotations(Integer idnegotations) {
        this.idnegotations = idnegotations;
    }

    public Integer getAdvance() {
        return advance;
    }

    public void setAdvance(Integer advance) {
        this.advance = advance;
    }

    public String getRespondedOn() {
        return respondedOn;
    }

    public void setRespondedOn(String respondedOn) {
        this.respondedOn = respondedOn;
    }
}
