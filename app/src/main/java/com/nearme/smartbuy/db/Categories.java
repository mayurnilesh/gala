package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

public class Categories extends SugarRecord {
    public int catid;
    public String catname;
    public String catimg;
    public boolean isSelected = false;
}