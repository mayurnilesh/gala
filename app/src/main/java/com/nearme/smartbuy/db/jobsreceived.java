package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

/**
 * Created by gopi.komanduri on 04/10/18.
 */

public class jobsreceived extends SugarRecord {
    public String geoHash;
    public String employername;
    public String employerlocationurl;
    public String jobDescription;
    public String locationLandmark;
    public String offeringpost;
    public String educationQualification;
    public String experienceReq;
    public String sex;
    public String ageLimitation;
    public String contact;
    public String emailId;
    public String interviewDate;
    public String shiftTimings;
    public String salary;
    public String postedon;
    public Integer jobid;
    public String upgradeTest;
    public String lat;
    public String lng;

    public jobsreceived(String geoHash,
                        String employername, String employerlocationurl,
                        String jobDescription, String locationLandmark,
                        String offeringpost, String educationQualification,
                        String experienceReq, String sex,
                        String ageLimitation, String contact, String emailId,
                        String interviewDate, String shiftTimings, String salary,
                        String postedon, Integer id, String lat, String lng) {
        this.geoHash = geoHash;
        this.employername = employername;
        this.employerlocationurl = employerlocationurl;
        this.jobDescription = jobDescription;
        this.locationLandmark = locationLandmark;
        this.offeringpost = offeringpost;
        this.educationQualification = educationQualification;
        this.experienceReq = experienceReq;
        this.sex = sex;
        this.ageLimitation = ageLimitation;
        this.contact = contact;
        this.emailId = emailId;
        this.interviewDate = interviewDate;
        this.shiftTimings = shiftTimings;
        this.salary = salary;
        this.postedon = postedon;
        this.jobid = id;
        this.lat = lat;
        this.lng = lng;
    }

    public jobsreceived() {
    }

    public String getGeoHash() {
        return geoHash;
    }

    public String getEmployername() {
        return employername;
    }

    public String getEmployerlocationurl() {
        return employerlocationurl;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public String getLocationLandmark() {
        return locationLandmark;
    }

    public String getOfferingpost() {
        return offeringpost;
    }

    public String getEducationQualification() {
        return educationQualification;
    }

    public String getExperienceReq() {
        return experienceReq;
    }

    public String getSex() {
        return sex;
    }

    public String getAgeLimitation() {
        return ageLimitation;
    }

    public String getContact() {
        return contact;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getInterviewDate() {
        return interviewDate;
    }

    public String getShiftTimings() {
        return shiftTimings;
    }

    public String getSalary() {
        return salary;
    }

    public String getPostedon() {
        return postedon;
    }

    public Integer getJobid() {
        return jobid;
    }

    public String getUpgradeTest() {
        return upgradeTest;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
