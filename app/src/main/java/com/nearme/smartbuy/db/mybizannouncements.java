package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

public class mybizannouncements extends SugarRecord {

    public int notificationid;
    public   String geoHash;
    public Double lat;
    public Double lng;
    public  String merchantid;
    public String cat;
    public  String tilldate;
    public String tillmonth;
    public String tillyear;
    public  String fromdate;
    public  String frommonth;
    public  String fromyear;
    public  String offercode;
    public  String itemdesc;
    public  String imgUrl;
    public  String shopname;
    public String shopDp;
    public String contatNumber;
    public Integer negotiate;
    public Integer minBusiness;




    public mybizannouncements(int notificationid,String geoHash,
                              Double lat, Double lng, String merchantid,
                              String cat, String tilldate, String tillmonth,
                              String tillyear, String fromdate, String frommonth,
                              String fromyear, String offercode, String itemdesc,
                              String imgUrl, String shopname, String shopDp,
                               Integer negotiate, Integer minBusiness,String contatNumber) {
        this.notificationid=notificationid;
        this.geoHash = geoHash;
        this.lat = lat;
        this.lng = lng;
        this.merchantid = merchantid;
        this.cat = cat;
        this.tilldate = tilldate;
        this.tillmonth = tillmonth;
        this.tillyear = tillyear;
        this.fromdate = fromdate;
        this.frommonth = frommonth;
        this.fromyear = fromyear;
        this.offercode = offercode;
        this.itemdesc = itemdesc;
        this.imgUrl = imgUrl;
        this.shopname = shopname;
        this.shopDp = shopDp;
        this.negotiate = negotiate;
        this.minBusiness = minBusiness;
        this.contatNumber = contatNumber;
    }

    public mybizannouncements() {
    }
}
