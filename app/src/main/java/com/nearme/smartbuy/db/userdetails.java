package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

public class userdetails extends SugarRecord {
    public String idconsumers;
    public String ContactNumber;
    public String Name;
    public String DOB;
    public String MOB;
    public String YOB;
    public Integer Sex;
    public String ImageUrl;
    public Integer points;
    public String SocialCount;

    public userdetails(String idconsumers, String contactNumber, String name,
                       String DOB, String MOB, String YOB, Integer sex,
                       String ImageUrl, Integer points, String SocialCount) {
        this.idconsumers = idconsumers;
        ContactNumber = contactNumber;
        Name = name;
        this.DOB = DOB;
        Sex = sex;
        this.ImageUrl = ImageUrl;
        this.MOB = MOB;
        this.YOB = YOB;
        this.points = points;
        this.SocialCount = SocialCount;
    }

    public userdetails() {
    }
}

