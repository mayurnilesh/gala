package com.nearme.smartbuy.db;

import com.orm.SugarRecord;

/**
 * Created by gopi.komanduri on 09/10/18.
 */

public class negotiationresponsesreceived extends SugarRecord {
    public Integer idnegotiationresponse;
    public Integer advanceNeeded;
    public Integer negotiationresponse;

    public String customercontact;
    public String merchantid;
    public String geohash;
    public Integer minamount;
    public Integer maxamount;
    public Integer discountexpectation;
    public String shoppingdates;
    public String description;
    public Integer response;
    public Integer notificationid;
    public Integer canPostToStatus;



    public negotiationresponsesreceived(Integer idnegotiationresponse, Integer advanceNeeded, Integer negotiationresponse, String customercontact, String merchantid, String geohash, Integer minamount, Integer maxamount, Integer discountexpectation, String shoppingdates, String description, Integer response, Integer notificationid) {
        this.idnegotiationresponse = idnegotiationresponse;
        this.advanceNeeded = advanceNeeded;
        this.negotiationresponse = negotiationresponse;
        this.customercontact = customercontact;
        this.merchantid = merchantid;
        this.geohash = geohash;
        this.minamount = minamount;
        this.maxamount = maxamount;
        this.discountexpectation = discountexpectation;
        this.shoppingdates = shoppingdates;
        this.description = description;
        this.response = response;
        this.notificationid = notificationid;
    }

    public Integer getCanPostToStatus() {
        return canPostToStatus;
    }

    public void setCanPostToStatus(Integer canPostToStatus) {
        this.canPostToStatus = canPostToStatus;
    }

    public negotiationresponsesreceived() {
    }

    public Integer getIdnegotiationresponse() {
        return idnegotiationresponse;
    }

    public Integer getAdvanceNeeded() {
        return advanceNeeded;
    }

    public Integer getNegotiationresponse() {
        return negotiationresponse;
    }

    public String getCustomercontact() {
        return customercontact;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public String getGeohash() {
        return geohash;
    }

    public Integer getMinamount() {
        return minamount;
    }

    public Integer getMaxamount() {
        return maxamount;
    }

    public Integer getDiscountexpectation() {
        return discountexpectation;
    }

    public String getShoppingdates() {
        return shoppingdates;
    }

    public String getDescription() {
        return description;
    }

    public Integer getResponse() {
        return response;
    }

    public Integer getNotificationid() {
        return notificationid;
    }
}
