package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.fragments.ImageListFragment;
import com.nearme.smartbuy.fragments.list_item_base;
import com.nearme.smartbuy.product.ItemDetailsActivity;
import com.nearme.smartbuy.startup.ChatActivity;

import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.ViewHolder> {

    private String[] mValues;
    public List<list_item_base> adValues;
    public Context ctx;

    public OfferAdapter(List<list_item_base> items, Context context) {
        adValues = items;
        ctx = context;
    }

    @NonNull
    @Override
    public OfferAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferAdapter.ViewHolder holder, final int position) {

        final Uri uri = Uri.parse(adValues.get(position).imageUrl);
        final String validTill = adValues.get(position).validTill;
        final String offercode = adValues.get(position).offerCode;
        final String shopName = adValues.get(position).shopName;
        final String locUrl = adValues.get(position).location;
        final String merchantDp = adValues.get(position).shopDp;

        Glide.with(ctx)
                .load(uri)
                .into(holder.mImageView);

        holder.offerCode.setText("Offer Code: "+offercode);
        holder.validTill.setText(validTill);
        holder.shopName.setText(shopName.replace(" ShopNo :",""));

        holder.contactNumber.setText("Contact: "+adValues.get(position).contactNumber);

        Glide.with(ctx)
                .load(merchantDp)
                .into(holder.mDp);
        holder.mLayoutItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, ItemDetailsActivity.class);

                intent.putExtra(ImageListFragment.STRING_IMAGE_POSITION, position);
                intent.putExtra(ImageListFragment.STRING_IMAGE_URI, adValues.get(position).imageUrl);
                //           intent.putExtra(STRING_OFFER_ID,adValues.get(position).offerId);
                intent.putExtra(ImageListFragment.STRING_OFFER_DESCRIPTION, adValues.get(position).offerDescription);
                intent.putExtra(ImageListFragment.STRING_OFFER_VALIDTILL, adValues.get(position).validTill);
                intent.putExtra(ImageListFragment.STRING_SHOP_NAME, adValues.get(position).shopName);
                intent.putExtra(ImageListFragment.STRING_SHOP_LOCATION, adValues.get(position).location);
                intent.putExtra(ImageListFragment.STRING_OFFER_CODE, adValues.get(position).offerCode);
                intent.putExtra(ImageListFragment.STRING_VIEW_TYPE, "1");
                ctx.startActivity(intent);
            }
        });
        holder.mImageViewWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(locUrl);
                Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                ctx.startActivity(mapIntent);
                Toast.makeText(ctx, "Redirecting to Maps.", Toast.LENGTH_SHORT).show();
            }
        });
        holder.chatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                Intent intent = new Intent(ctx, ChatActivity.class);
                List<mybizannouncements> bizValues = mybizannouncements.find(mybizannouncements.class, "notificationid = ? and lat=? and lng=?", new String[]{String.valueOf(adValues.get(position).notificationid), String.valueOf(adValues.get(position).lat), String.valueOf(adValues.get(position).lng)}, null, null, null);

                if(bizValues!=null && bizValues.size() > 0) {
                    intent.putExtra("name",bizValues.get(0).merchantid);//merchantID
                    intent.putExtra(ImageListFragment.STRING_IMAGE_URI, bizValues.get(0).imgUrl);
                }else{
                    intent.putExtra("name", adValues.get(position).merchantID);//merchantID
                    intent.putExtra(ImageListFragment.STRING_IMAGE_URI, "");
                }
                intent.putExtra("add", true);

                ctx.startActivity(intent);


            }
        });
        if (adValues.get(position).negotiate == 0) {
            holder.lv_negotiation.setVisibility(View.GONE);
        } else
            holder.lv_negotiation.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return adValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageButton mImageView;
        public de.hdodenhof.circleimageview.CircleImageView mDp;
        public TextView fragName;
        public TextView shopName;
        public TextView validTill;
        public TextView contactNumber;
        public LinearLayout mLayoutItem;
        public TextView offerCode;
        public ImageButton mImageViewWishlist;
        public LinearLayout lv_negotiation;
        public final ImageView chatView;
        public final TextView chattxtView;
        public final LinearLayout chatLayout;

        public ViewHolder(View view) {
            super(view);
            //  mImageView = (SimpleDraweeView) view.findViewById(R.id.image1);
            mDp = view.findViewById(R.id.dp);
            mImageView = view.findViewById(R.id.image1);
            lv_negotiation = view.findViewById(R.id.lv_negotiation);
            validTill = view.findViewById(R.id.idValidTill);
            shopName = view.findViewById(R.id.idShopName);
            offerCode = view.findViewById(R.id.idOfferCode);
            mLayoutItem = view.findViewById(R.id.layout_item);
            mImageViewWishlist = view.findViewById(R.id.ic_wishlist);
            contactNumber = view.findViewById(R.id.contactNumber);
            fragName = view.findViewById(R.id.fragId);
            chatView=view.findViewById(R.id.chat);
            chattxtView=view.findViewById(R.id.chattxtVw);
            chatLayout =view.findViewById(R.id.layout_chat_desc);
        }
    }
}
