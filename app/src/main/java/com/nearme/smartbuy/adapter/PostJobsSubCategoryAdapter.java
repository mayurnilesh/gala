package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.rest.supportedsubjobs;

import java.util.List;


public class PostJobsSubCategoryAdapter extends RecyclerView.Adapter<PostJobsSubCategoryAdapter.Holder> {
    List<supportedsubjobs> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public PostJobsSubCategoryAdapter(List<supportedsubjobs> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_post_jobs_subcategory, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.catname.setText(homeArrayList.get(position).subjobname);
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView catname;

        public Holder(View itemView) {
            super(itemView);
            catname = itemView.findViewById(R.id.catname);

        }
    }
}
