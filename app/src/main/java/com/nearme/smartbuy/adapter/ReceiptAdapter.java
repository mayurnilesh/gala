package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.rest.contactspecificinfo;
import com.nearme.smartbuy.startup.WebviewActivity;

import java.util.List;


public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.Holder> {
    List<contactspecificinfo> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public ReceiptAdapter(List<contactspecificinfo> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.reeipt_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        Log.d("Mydata", "onBindViewHolder: " + homeArrayList.get(position).getImgurl());
//        Glide.with(mContext)
//                .load(homeArrayList.get(position).getImgurl())
//                .into(holder.imageView);
        holder.recid.setText(homeArrayList.get(position).getMerchantreceiptid());
        holder.name.setText(homeArrayList.get(position).getMerchantname());
        holder.amount.setText(homeArrayList.get(position).getBillamount());
        holder.date.setText(homeArrayList.get(position).getDate());
        holder.link.setText(homeArrayList.get(position).getImgurl());

        holder.link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext,WebviewActivity.class).putExtra("link",homeArrayList.get(position).getImgurl()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        // ImageView imageView;
        TextView recid, name, amount, date, link;

        public Holder(View itemView) {
            super(itemView);

//        //  imageView = itemView.findViewById(R.id.image);
            recid = itemView.findViewById(R.id.recid);
            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
            date = itemView.findViewById(R.id.date);
            link = itemView.findViewById(R.id.link);

        }
    }
}
