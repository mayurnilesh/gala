package com.nearme.smartbuy.adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.db.negotiationresponsesreceived;
import com.nearme.smartbuy.fragments.ExclusiveFragment;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.Statuspayload;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExclusiveAdapter extends RecyclerView.Adapter<ExclusiveAdapter.Holder> {
    List<negotiationresponsesreceived> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    TextView choose_locatoin;
    TextView contactnumber;
    TextView validfrom;
    TextView validto;
    TextView shppingdate;
    TextView offer_desc;
    EditText discount;
    CheckBox cbdesc;
    RadioButton extraforbring;
    RadioButton rbjoin;
    Spinner FlatOrDisc;
    TextView tempmap, temp;
    DatePickerDialog datePickerDialog;
    private int year, month, day;
    List<mybizannouncements> cvr;

    public ExclusiveAdapter(List<negotiationresponsesreceived> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.exclusive_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        cvr = mybizannouncements.find(mybizannouncements.class,
                "notificationid = ? ",
                homeArrayList.get(position).notificationid.toString());

        holder.contact.setText(homeArrayList.get(position).getCustomercontact());
        holder.minammount.setText(String.valueOf(homeArrayList.get(position).getMinamount()));
        holder.maxmaount.setText(String.valueOf(homeArrayList.get(position).getMaxamount()));
        holder.discount.setText(String.valueOf(homeArrayList.get(position).getDiscountexpectation()));
        holder.date.setText(homeArrayList.get(position).getShoppingdates());
        holder.desc.setText(homeArrayList.get(position).getDescription());
        holder.advance.setText(String.valueOf(homeArrayList.get(position).getAdvanceNeeded()));

        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this.mContext, myDateListener, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }

        if (homeArrayList.get(position).getCanPostToStatus() == 0 || homeArrayList.get(position).getResponse() == 2) {
            holder.showstatus.setVisibility(View.GONE);
        } else {
            holder.showstatus.setVisibility(View.VISIBLE);
        }

        if (homeArrayList.get(position).getResponse() == 2) {
            holder.tvError.setVisibility(View.VISIBLE);
        } else {
            holder.tvError.setVisibility(View.GONE);
        }


        holder.showstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(mContext, R.style.Theme_CustomDialog);
                dialog.setContentView(R.layout.dialogue_status);
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                choose_locatoin = dialog.findViewById(R.id.place);
                contactnumber = dialog.findViewById(R.id.contactnumber);
                validfrom = dialog.findViewById(R.id.validfrom);
                validto = dialog.findViewById(R.id.validto);
                shppingdate = dialog.findViewById(R.id.shppingdate);
                offer_desc = dialog.findViewById(R.id.offer_desc);
                discount = dialog.findViewById(R.id.discount);
                cbdesc = dialog.findViewById(R.id.cbdesc);
                extraforbring = dialog.findViewById(R.id.extraforbring);
                rbjoin = dialog.findViewById(R.id.rbjoin);
                FlatOrDisc = dialog.findViewById(R.id.sp_type);
                choose_locatoin.setEnabled(false);

                cbdesc.setVisibility(View.GONE);

                validfrom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            temp = validfrom;
                            datePickerDialog.show();

                        } catch (Exception ex) {

                        }
                    }
                });

                validto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        temp = validto;
                        datePickerDialog.show();
                    }
                });

                shppingdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        temp = shppingdate;
                        datePickerDialog.show();

                    }
                });


                TextView submitView = dialog.findViewById(R.id.tvsubmit);
                submitView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            if (contactnumber.getText().toString().isEmpty() ||
                                    contactnumber.getText().toString().length() != 10) {
                                contactnumber.setError("Enter valid number");
                                return;
                            }
                            if (!isEmpty(validfrom) || !isEmpty(validto) || !isEmpty(shppingdate) || !isEmpty(offer_desc)
                                    || !isEmpty(discount)) {
                                return;
                            }
                            if (cbdesc.isChecked() && discount.getText().toString().length() > 2) {
                                discount.setError("Please enter 0-99 discount");
                                discount.requestFocus();
                                return;
                            }

                            if (!checkDate(validfrom, validto)) {
                                Toast.makeText(mContext, "From date must be less then end date", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (!checkDate(shppingdate, validto)) {
                                Toast.makeText(mContext, "Shopping date must be less then end date", Toast.LENGTH_SHORT).show();
                                return;
                            }


                            Statuspayload payload = new Statuspayload();
                            payload.contactOn = contactnumber.getText().toString();
                            payload.validfrom = validfrom.getText().toString();
                            payload.validto = validto.getText().toString();
                            payload.shoppingdate = shppingdate.getText().toString();
//                            payload.validfrom ="10:02:2020";
//                            payload.validto = "20:02:2020";
//                            payload.shoppingdate = "14:02:2020";
                            payload.offerdesc = offer_desc.getText().toString();
                            payload.finaldiscount = Integer.parseInt(discount.getText().toString());
                            if (cbdesc.isChecked())
                                payload.ispercentage = 1;
                            else
                                payload.ispercentage = 0;

                            if (extraforbring.isChecked())
                                payload.extraforbringing = 1;
                            else
                                payload.extraforbringing = 0;

                            if (rbjoin.isChecked())
                                payload.join = 1;
                            else
                                payload.join = 0;
                            payload.joinedcount = 0;

                            //fill from actual data
                            payload.idnotification = homeArrayList.get(position).notificationid;
                            payload.imgurl = cvr.size() == 0 ? "" : cvr.get(0).imgUrl;
                            payload.geohashes = homeArrayList.get(position).geohash;
                            payload.idstatus = 1;
                            payload.registeredcontactnumber = homeArrayList.get(position).customercontact;
                            if (FlatOrDisc.getSelectedItem().toString().equals("Flatprice")) {
                                payload.flat = 1;
                            } else {
                                payload.flat = 0;
                            }
//                            payload.place=cvr.get(0).lat;
                            payload.value = Integer.parseInt(discount.getText().toString());

                            String payloadstr = new Gson().toJson(payload);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);

                            Call<String> call = apiService.PushStatus(payloadstr);
                            call.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {

                                    int z = 60;
                                    ++z;
                                    dialog.cancel();
                                    homeArrayList.get(position).setCanPostToStatus(0);
                                    notifyDataSetChanged();
                                    ExclusiveFragment.updatePayload(position);
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {

                                    int z = 50;
                                    ++z;
                                    dialog.cancel();
                                }
                            });
                        } catch (Exception ex) {
                            Log.d("TAG", "onClick: " + ex.getMessage());
                        }
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView contact;
        TextView tvError;
        TextView minammount;
        TextView maxmaount;
        TextView discount;
        TextView date;
        TextView desc;
        TextView advance;
        TextView showstatus;
        CardView cardview;


        public Holder(View itemView) {
            super(itemView);


            contact = itemView.findViewById(R.id.contact);
            tvError = itemView.findViewById(R.id.tvError);
            minammount = itemView.findViewById(R.id.minammount);
            maxmaount = itemView.findViewById(R.id.maxmaount);
            discount = itemView.findViewById(R.id.discount);
            date = itemView.findViewById(R.id.date);
            desc = itemView.findViewById(R.id.desc);
            advance = itemView.findViewById(R.id.advance);
            cardview = itemView.findViewById(R.id.cardview);
            showstatus = itemView.findViewById(R.id.showstatus);

        }
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2 + 1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        if (temp != null) {
            temp.setText(new StringBuilder().append(String.format("%02d", day)).append("-")
                    .append(String.format("%02d", month)).append("-").append(String.format("%04d", year)));
        }
    }


    private boolean isEmpty(EditText et) {
        if (et.getText().toString().isEmpty()) {
            et.setError("Enter Value");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean isEmpty(TextView et) {
        if (et.getText().toString().equalsIgnoreCase("Valid From")
                || et.getText().toString().equalsIgnoreCase("Valid To")
                || et.getText().toString().equalsIgnoreCase("Shopping Date")) {
            et.setError("Enter Value");
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkDate(TextView et, TextView et1) {

        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(et.getText().toString());
            Date enddate = sdf.parse(et1.getText().toString());

            if (strDate.getTime() > enddate.getTime()) {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

}
