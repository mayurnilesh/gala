package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.StorageReference;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.utility.ChatMessage;
import com.nearme.smartbuy.utility.StorageUtil;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import kotlin.jvm.internal.Intrinsics;

public final class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    @NotNull
    private final List chatMessages;
    @NotNull
    private final String uid;
    Context mContext;

    @NotNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_chat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.chatMessages.size();
    }

    @Override
    public void onBindViewHolder(@NotNull ChatAdapter.ViewHolder holder, int position) {
        Intrinsics.checkParameterIsNotNull(holder, "holder");
        ChatMessage chatMessage = (ChatMessage) this.chatMessages.get(position);

        if (Intrinsics.areEqual(chatMessage.getUser(), this.uid)) {
            holder.chatTextSent.setText((CharSequence) chatMessage.getText());
            holder.chatTextReceived.setVisibility(View.INVISIBLE);
            holder.imageView_message_image_left.setVisibility(View.GONE);
            holder.chatImageRecived.setVisibility(View.VISIBLE);
            String data = chatMessage.getImage();
            if (data != null) {
                StorageReference ref = StorageUtil.INSTANCE.pathToReference(data);
                ref.getDownloadUrl()
                        .addOnSuccessListener(uri -> {
                            Glide.with(mContext)
                                    .load(uri)
                                    .placeholder(R.drawable.ic_account_circle_black_48dp)
                                    .into(holder.chatImageRecived);
                        });
                holder.chatImageRecived.setVisibility(View.VISIBLE);
                holder.chatTextSent.setVisibility(View.GONE);
            } else {
                holder.chatImageRecived.setVisibility(View.GONE);
                holder.chatTextSent.setVisibility(View.VISIBLE);
            }
        } else {
            holder.chatTextReceived.setText((CharSequence) chatMessage.getText());
            holder.chatTextSent.setVisibility(View.INVISIBLE);
            holder.imageView_message_image_left.setVisibility(View.VISIBLE);
            holder.chatImageRecived.setVisibility(View.GONE);
            String data = chatMessage.getImage();
            if (data != null) {
                StorageReference ref = StorageUtil.INSTANCE.pathToReference(data);
                ref.getDownloadUrl()
                        .addOnSuccessListener(uri -> {
                            Glide.with(mContext)
                                    .load(uri)
                                    .placeholder(R.drawable.ic_account_circle_black_48dp)
                                    .into(holder.imageView_message_image_left);
                        });
                holder.imageView_message_image_left.setVisibility(View.VISIBLE);
                holder.chatTextReceived.setVisibility(View.GONE);
            }
            else{
                holder.imageView_message_image_left.setVisibility(View.GONE);
                holder.chatTextReceived.setVisibility(View.VISIBLE);
            }
        }

    }


    @NotNull
    public final List getChatMessages() {
        return this.chatMessages;
    }

    @NotNull
    public final String getUid() {
        return this.uid;
    }

    public ChatAdapter(@NotNull List chatMessages, @NotNull String uid) {
        super();
        this.chatMessages = chatMessages;
        this.uid = uid;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public int holderID;
        private TextView chatTextSent;
        private TextView chatTextReceived;
        private ImageView chatImageRecived;
        private ImageView imageView_message_image_left;

        public ViewHolder(View view) {
            super(view);
            chatTextSent = view.findViewById(R.id.textview_chat_sent);
            chatTextReceived = view.findViewById(R.id.textview_chat_received);
            chatImageRecived = view.findViewById(R.id.imageView_message_image);
            imageView_message_image_left = view.findViewById(R.id.imageView_message_image_left);

        }


    }
}
