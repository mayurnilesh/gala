package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.NegotiationData;
import com.squareup.picasso.Picasso;

import java.util.List;


public class NegotiationDataAdapter extends RecyclerView.Adapter<NegotiationDataAdapter.Holder> {
    List<NegotiationData> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public NegotiationDataAdapter(List<NegotiationData> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_negotiation, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.contact.setText(homeArrayList.get(position).offercode);
        holder.off_desc.setText(homeArrayList.get(position).itemdesc);
        holder.minsale.setText(String.valueOf(homeArrayList.get(position).minamount));
        holder.maxsale.setText(String.valueOf(homeArrayList.get(position).maxamount));
        holder.disc.setText(String.valueOf(homeArrayList.get(position).DiscountExpectation));
        holder.desc.setText(homeArrayList.get(position).description);
        holder.date.setText(homeArrayList.get(position).ShoppingProbableDates);
        Picasso.get().load(homeArrayList.get(position).imgUrl).into(holder.negotiatedimg);

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.gen_white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }


    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView contact;
        TextView minsale;
        TextView maxsale;
        TextView disc;
        TextView desc;
        TextView date;
        TextView off_desc;
        CardView cardview;
        SimpleDraweeView negotiatedimg;


        public Holder(View itemView) {
            super(itemView);

            contact = itemView.findViewById(R.id.contact);
            off_desc = itemView.findViewById(R.id.off_desc);
            minsale = itemView.findViewById(R.id.minsale);
            maxsale = itemView.findViewById(R.id.maxsale);
            disc = itemView.findViewById(R.id.disc);
            desc = itemView.findViewById(R.id.desc);
            date = itemView.findViewById(R.id.date);
            cardview = itemView.findViewById(R.id.cardview);
            negotiatedimg = itemView.findViewById(R.id.negotiatedimage);


        }
    }
}
