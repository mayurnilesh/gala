package com.nearme.smartbuy.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.model.SlotItem;
import com.nearme.smartbuy.startup.MerchantSlotActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.Holder> {
    ArrayList<SlotItem> slotArrayLIst;
    Context mContext;
    LayoutInflater layoutInflater;
    int lastPosition = -1;
    MerchantSlotActivity.SlotCallback slotCallback;

    public SlotAdapter(ArrayList<SlotItem> slotArrayLIst, MerchantSlotActivity.SlotCallback slotCallback) {
        this.slotArrayLIst = slotArrayLIst;
        this.slotCallback = slotCallback;
    }

    @NotNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_slot_item, parent, false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull Holder holder, final int position) {
        SlotItem slotItem = slotArrayLIst.get(position);
        holder.tvTime.setText(slotItem.getFromTime() + " - " + slotItem.getToTime());
        holder.tvSlot.setText(slotItem.getMaxToken() + "/" + slotItem.getCurToken() + " Peoples");
        holder.reg_slot.setOnClickListener(v -> slotCallback.callBack(slotArrayLIst.get(position)));
    }

    @Override
    public int getItemCount() {
        return slotArrayLIst.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        TextView tvTime, tvSlot,reg_slot;

        public Holder(View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvSlot = itemView.findViewById(R.id.tvSlot);
            reg_slot = itemView.findViewById(R.id.reg_slot);
        }
    }
}
