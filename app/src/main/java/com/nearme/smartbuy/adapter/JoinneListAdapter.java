package com.nearme.smartbuy.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearme.smartbuy.R;

import java.util.List;


public class JoinneListAdapter extends RecyclerView.Adapter<JoinneListAdapter.Holder> {
    List<String> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public JoinneListAdapter(List<String> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_joinee_list, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.name.setText(homeArrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView name;

        public Holder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);


        }
    }
}
