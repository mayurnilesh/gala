package com.nearme.smartbuy.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.model.StatusData;

import java.util.ArrayList;


public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.Holder> {
    ArrayList<StatusData> statusDataArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public StatusAdapter(ArrayList<StatusData> statusDataArrayList) {
        this.statusDataArrayList = statusDataArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.status_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {


//        Uri uri = Uri.parse(homeArrayList.get(position).getMerchanturl());
//
//        holder.merchantname.setText(homeArrayList.get(position).getMerchantname());
//        holder.token.setText("Your Token : " + homeArrayList.get(position).getToken());
//        holder.position.setText("Your position : " + homeArrayList.get(position).getPosition());
//        Glide.with(mContext)
//                .load(uri)
//                .into(holder.iv);

    }

    @Override
    public int getItemCount() {
        return statusDataArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

       // CardView cardview;
//        ImageView iv;
//        TextView merchantname, token,position;

        public Holder(View itemView) {
            super(itemView);
//            cardview = itemView.findViewById(R.id.cardview);
//            iv = itemView.findViewById(R.id.iv);
//            merchantname = itemView.findViewById(R.id.merchantname);
//            token = itemView.findViewById(R.id.token);
//            position = itemView.findViewById(R.id.position);

        }
    }
}
