package com.nearme.smartbuy.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nearme.smartbuy.fragments.IntroFragment1;
import com.nearme.smartbuy.fragments.IntroFragment2;
import com.nearme.smartbuy.fragments.IntroFragment3;
import com.nearme.smartbuy.fragments.IntroFragment4;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final Fragment[] childFragments;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        childFragments = new Fragment[] {
                new IntroFragment1(), //0
                new IntroFragment2(), //1
                new IntroFragment3() ,//2
                new IntroFragment4() ,//2
        };
    }

    @Override
    public Fragment getItem(int position) {
        return childFragments[position];
    }

    @Override
    public int getCount() {
        return childFragments.length; //3 items
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = getItem(position).getClass().getName();
        return title.subSequence(title.lastIndexOf(".") + 1, title.length());
    }
}
