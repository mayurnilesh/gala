package com.nearme.smartbuy.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.model.UserSlot;
import com.nearme.smartbuy.startup.MerchantSlotActivity;

import java.util.ArrayList;

public class UserSlotAdapter extends RecyclerView.Adapter<UserSlotAdapter.Holder> {
    ArrayList<UserSlot> merchantArrayList;
    Context mContext;

    public UserSlotAdapter(ArrayList<UserSlot> merchantArrayList) {
        this.merchantArrayList = merchantArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_user_slot, parent, false);
        return new Holder(view);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        holder.peoples.setText(merchantArrayList.get(position).tokensRequested + " peoples");
        holder.merchantName.setText(merchantArrayList.get(position).MerchantName);
        holder.timeSlot.setText(merchantArrayList.get(position).selectedSlotStartHash + " to " +
                merchantArrayList.get(position).selectedSlotEndHash);

    }

    @Override
    public int getItemCount() {
        return merchantArrayList.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        TextView peoples, merchantName, timeSlot;

        public Holder(View itemView) {
            super(itemView);
            peoples = itemView.findViewById(R.id.peoples);
            timeSlot = itemView.findViewById(R.id.timeSlot);
            merchantName = itemView.findViewById(R.id.merchantName);
        }
    }

}
