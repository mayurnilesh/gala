package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.jobs.JobsSubCategoryFragment;
import com.nearme.smartbuy.rest.supportedjobs;

import java.util.List;


public class JobsCategoryAdapter extends RecyclerView.Adapter<JobsCategoryAdapter.Holder> {
    List<supportedjobs> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    View view;

    public JobsCategoryAdapter(List<supportedjobs> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.row_jobs_category, parent, false);


        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        holder.catname.setText(homeArrayList.get(position).Jobtype);
        Glide.with(mContext)
                .load(homeArrayList.get(position).jobimgurl)
                .into(holder.iv);

        if (((position + 1)%8) % 8 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#eae6ff"));
        } else if (((position + 1)%8) % 7 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#e6fffb"));
        } else if (((position + 1)%8) % 6 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f4ffe6"));
        } else if (((position + 1)%8) % 5 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#fff2f2"));
        } else if (((position + 1)%8) % 4 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#fffbf2"));
        } else if (((position + 1)%8) % 3 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f5fff2"));
        } else if (((position + 1)%8) % 2 == 0) {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f2fffe"));
        }   else {
            holder.cardView.setCardBackgroundColor(Color.parseColor("#fff2f7"));
        }

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView catname;
        ImageView iv;
        CardView cardView;

        public Holder(View itemView) {
            super(itemView);

            catname = itemView.findViewById(R.id.catname);
            cardView = itemView.findViewById(R.id.cardmain);
            iv = itemView.findViewById(R.id.iv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.content_main, new JobsSubCategoryFragment(homeArrayList.get(getAdapterPosition()).subjobs)).addToBackStack("");
                    ft.commit();
                }
            });

        }
    }
}
