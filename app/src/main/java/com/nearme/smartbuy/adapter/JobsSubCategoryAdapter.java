package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.jobs.JobsListFragment;
import com.nearme.smartbuy.rest.supportedsubjobs;

import java.util.List;


public class JobsSubCategoryAdapter extends RecyclerView.Adapter<JobsSubCategoryAdapter.Holder> {
    List<supportedsubjobs> subcatList;

    Context mContext;
    LayoutInflater layoutInflater;

    public JobsSubCategoryAdapter(List<supportedsubjobs> subcatList) {
        this.subcatList = subcatList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_jobs_subcategory, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.subcatname.setText(subcatList.get(position).subjobname);
    }

    @Override
    public int getItemCount() {
        return subcatList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView subcatname;

        public Holder(View itemView) {
            super(itemView);

            subcatname = itemView.findViewById(R.id.subcatname);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.content_main, new JobsListFragment()).addToBackStack("");
                    ft.commit();

                }
            });

        }
    }
}
