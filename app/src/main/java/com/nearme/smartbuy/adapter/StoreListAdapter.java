package com.nearme.smartbuy.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.negotiationresponsesreceived;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.startup.MerchantSlotActivity;

import java.util.ArrayList;
import java.util.List;

public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.Holder> {
    ArrayList<Merchant> merchantArrayList;
    Context mContext;

    public StoreListAdapter(ArrayList<Merchant> merchantArrayList) {
        this.merchantArrayList = merchantArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_register_slot_list, parent, false);
        return new Holder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, MerchantSlotActivity.class)
                        .putExtra("mid", merchantArrayList.get(position).merchantId)
                );
            }
        });
        holder.merchantName.setText(merchantArrayList.get(position).merchantName);
        holder.merchantPhone.setText(merchantArrayList.get(position).merchantPhn);
        holder.merchanKm.setText(String.format("%.2f km", merchantArrayList.get(position).distance));
    }

    @Override
    public int getItemCount() {
        return merchantArrayList.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        TextView merchantName, merchantPhone, merchanKm;

        public Holder(View itemView) {
            super(itemView);
            merchanKm = itemView.findViewById(R.id.merchanKm);
            merchantPhone = itemView.findViewById(R.id.merchantPhone);
            merchantName = itemView.findViewById(R.id.merchantName);
        }
    }

}
