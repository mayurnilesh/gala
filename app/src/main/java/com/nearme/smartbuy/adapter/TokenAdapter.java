package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.net.Uri;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.model.Tokenmodel;

import java.util.List;


public class TokenAdapter extends RecyclerView.Adapter<TokenAdapter.Holder> {
    List<Tokenmodel> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    onItemClick itemClick;

    public TokenAdapter(List<Tokenmodel> homeArrayList,onItemClick itemClick) {
        this.homeArrayList = homeArrayList;
        this.itemClick = itemClick;
    }

    public interface onItemClick {
        public void onClick(Tokenmodel tokenmodel, String type);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.token_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }

        Uri uri = Uri.parse(homeArrayList.get(position).getMerchanturl());

        holder.merchantname.setText(homeArrayList.get(position).getMerchantname());
        holder.token.setText("Your Token : " + homeArrayList.get(position).getToken());
        holder.position.setText("Your position : " + homeArrayList.get(position).getPosition());
        Glide.with(mContext)
                .load(uri)
                .into(holder.iv);

        holder.deregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onClick(homeArrayList.get(position),"deregister");
            }
        });

        if(homeArrayList.get(position).getPosition().equalsIgnoreCase("1")){
            holder.regenrate.setVisibility(View.GONE);
        }else{
            holder.regenrate.setVisibility(View.VISIBLE);
        }

        holder.regenrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onClick(homeArrayList.get(position),"regenerate");
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        CardView cardview;
        ImageView iv;
        TextView merchantname, token, position;
        TextView deregister,regenrate ;

        public Holder(View itemView) {
            super(itemView);
            cardview = itemView.findViewById(R.id.cardview);
            iv = itemView.findViewById(R.id.iv);
            merchantname = itemView.findViewById(R.id.merchantname);
            token = itemView.findViewById(R.id.token);
            position = itemView.findViewById(R.id.position);
            deregister = itemView.findViewById(R.id.deregister);
            regenrate = itemView.findViewById(R.id.regeneratetoken);

        }
    }
}
