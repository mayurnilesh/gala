package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.Categories;
import com.nearme.smartbuy.fragments.ImageListFragment;
import com.nearme.smartbuy.fragments.list_item_base;
import com.nearme.smartbuy.model.BottomSheetItem;
import com.nearme.smartbuy.product.ItemDetailsActivity;

import java.util.List;

public class BottomSheetAdapter extends RecyclerView.Adapter<BottomSheetAdapter.ViewHolder> {

    public List<Categories> catValues;
    public Context ctx;

    public BottomSheetAdapter(List<Categories> items, Context context) {
        catValues = items;
        ctx = context;
    }

    @NonNull
    @Override
    public BottomSheetAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_sheet_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BottomSheetAdapter.ViewHolder holder, final int position) {
        holder.categoryText.setText(catValues.get(position).catname);

        if (catValues.get(position).isSelected) {
            holder.mLayoutItem.setBackground(ctx.getResources().getDrawable(R.drawable.btn_bg_orange));
            holder.categoryText.setTextColor(ctx.getResources().getColor(R.color.white));
        } else {
            holder.mLayoutItem.setBackground(ctx.getResources().getDrawable(R.drawable.btn_bg_frame_orange));
            holder.categoryText.setTextColor(ctx.getResources().getColor(R.color.light_orange));
        }

        holder.mLayoutItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catValues.get(position).isSelected = !catValues.get(position).isSelected;
                notifyItemChanged(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return catValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout mLayoutItem;
        public TextView categoryText;


        public ViewHolder(View view) {
            super(view);
            categoryText = view.findViewById(R.id.categoryText);
            mLayoutItem = view.findViewById(R.id.mainLayout);
        }
    }
}
