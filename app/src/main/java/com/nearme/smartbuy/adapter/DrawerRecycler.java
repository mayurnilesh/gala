package com.nearme.smartbuy.adapter;


import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.model.DrawerMainModel;

import java.util.ArrayList;

public class DrawerRecycler extends RecyclerView.Adapter<DrawerRecycler.ViewHolder> {

    private ArrayList<DrawerMainModel> myItems;
    private ItemListener myListener;

    public DrawerRecycler(ArrayList<DrawerMainModel> items, ItemListener listener) {
        myItems = items;
        myListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_item_view, parent, false));
    }

    @Override
    public int getItemCount() {
        return myItems.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemName.setText(myItems.get(position).name);
        holder.imgSymbol.setImageDrawable(myItems.get(position).resurceId);
        if (myItems.get(position).isDropDown) {
            holder.dropDown.setVisibility(View.VISIBLE);
            holder.llSubItem.removeAllViews();
            for (int n = 0; n < myItems.get(position).subitems.size(); n++) {
                View subItemView = LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.drawer_sub_item_view, holder.llSubItem, false);
                ((TextView) subItemView.findViewById(R.id.subItemName)).setText(myItems.get(position).subitems.get(n).name);
                subItemView.setTag(R.id.subItemName, myItems.get(position).subitems.get(n).name);
                subItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (myListener != null) {
                            myListener.onItemClick(view.getTag(R.id.subItemName).toString());
                        }
                    }
                });
                holder.llSubItem.addView(subItemView);
            }
        } else {
            holder.dropDown.setVisibility(View.GONE);
            holder.llSubItem.setVisibility(View.GONE);
        }




        holder.headerView.setTag(String.valueOf(position));
        holder.headerView.setTag(R.id.subItemName,holder.llSubItem);
        holder.headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout holder = (LinearLayout) view.getTag(R.id.subItemName);
                DrawerMainModel model = myItems.get(Integer.parseInt(view.getTag().toString()));
                if (model.isDropDown) {
                    if (holder.getVisibility() == View.VISIBLE) {
                        holder.setVisibility(View.GONE);
                        return;
                    }
                    holder.setVisibility(View.VISIBLE);
                } else {
                    if (myListener != null) {
                        myListener.onItemClick(model.name);
                    }
                }

            }
        });


    }


    public interface ItemListener {
        void onItemClick(String item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgSymbol;
        private ImageView dropDown;
        private TextView itemName;
        private LinearLayout llSubItem;
        private RelativeLayout headerView;


        public ViewHolder(View itemView) {
            super(itemView);
            imgSymbol = itemView.findViewById(R.id.imgSymbol);
            itemName = itemView.findViewById(R.id.itemName);
            dropDown = itemView.findViewById(R.id.dropDown);
            llSubItem = itemView.findViewById(R.id.llSubItem);
            headerView = itemView.findViewById(R.id.headerView);
        }

    }


}

