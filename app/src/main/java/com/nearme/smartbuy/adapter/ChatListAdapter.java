package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nearme.smartbuy.R;
//import com.nearme.smartbuy.fragments.ChatFragment;
import com.nearme.smartbuy.model.ChatInfo;
import com.nearme.smartbuy.startup.ChatActivity;

import java.util.ArrayList;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.AppInfoHolder> {
    Context mContext;
    ArrayList<ChatInfo> data;

    public ChatListAdapter(Context context, ArrayList<ChatInfo> data) {
        this.mContext = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AppInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_chat_listview, parent, false);

        return new AppInfoHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull AppInfoHolder holder, int position) {
        holder.txtId.setText(data.get(position).txtId);

        holder.txtTitle.setText(data.get(position).txtTitle);
        //holder.txtId.setVisibility(View.INVISIBLE);

        Glide.with(mContext).load(data.get(position).imgIcon)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.imgIcon);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("name", data.get(position).txtTitle);//
                intent.putExtra("add", false);//add
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class AppInfoHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtId;

        public AppInfoHolder(@NonNull View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.listview_image);
            txtId = itemView.findViewById(R.id.listview_item_short_description);
            txtTitle = itemView.findViewById(R.id.listview_item_title);
        }
    }
}

