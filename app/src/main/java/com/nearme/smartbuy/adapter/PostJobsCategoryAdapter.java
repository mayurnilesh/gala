package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.jobs.PostJobsSubCategoryFragment;
import com.nearme.smartbuy.rest.supportedjobs;

import java.util.List;


public class PostJobsCategoryAdapter extends RecyclerView.Adapter<PostJobsCategoryAdapter.Holder> {
    List<supportedjobs> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public PostJobsCategoryAdapter(List<supportedjobs> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_jobs_category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.catname.setText(homeArrayList.get(position).Jobtype);
        Glide.with(mContext)
                .load(homeArrayList.get(position).jobimgurl)
                .into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView catname;
        ImageView iv;

        public Holder(View itemView) {
            super(itemView);

            catname = itemView.findViewById(R.id.catname);
            iv = itemView.findViewById(R.id.iv);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.content_main, new PostJobsSubCategoryFragment(homeArrayList.get(getAdapterPosition()).subjobs)).addToBackStack("");
                    ft.commit();
                }
            });

        }
    }
}
