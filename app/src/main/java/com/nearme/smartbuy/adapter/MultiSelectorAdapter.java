package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.Categories;
import com.nearme.smartbuy.fragments.OfferListFragment;

import java.util.List;

public class MultiSelectorAdapter extends ArrayAdapter<Categories> {
    private LayoutInflater mInflater;
    private List<Categories> listState;
    public Spinner mySpinner = null;
    public String oneSpace = " ";
    public int tikMark = 0X2714;
    public int crossMark = 0X2715;
    public int tikMarkAroundBox = 0X2611;
    public int crossMarkAroundBox = 0X274E;
    public String dash = "-";
    OfferListFragment offerFragment;

    public MultiSelectorAdapter(OfferListFragment offerFragment, Context context, int resource, List<Categories> objects, Spinner mySpinner) {
        super(context, resource, objects);
        this.listState = objects;
        this.mySpinner = mySpinner;
        this.offerFragment = offerFragment;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        String text = "";
        final ViewHolder holder;
        holder = new ViewHolder();
        mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.spinnerrow, null, false);
        holder.mTextView = convertView.findViewById(R.id.tvSpinnerItem);
        convertView.setTag(holder);

        if (listState.get(position).isSelected) {
            text = "  " + (char) tikMark + " " + listState.get(position).catname;
        } else {
            text = "  " + dash + " " + listState.get(position).catname;
        }

        holder.mTextView.setText(text);
        holder.mTextView.setTag(position);
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * if you want open spinner after click on text for first time we have to open spinner programmatically
                 */
                mySpinner.performClick();
                int getPosition = (Integer) v.getTag();
                listState.get(getPosition).isSelected = !listState.get(getPosition).isSelected;
                notifyDataSetChanged();
                offerFragment.setCategory();
            }
        });
        return convertView;
    }

    /**
     * view holder
     */
    private class ViewHolder {
        private TextView mTextView;
    }
}