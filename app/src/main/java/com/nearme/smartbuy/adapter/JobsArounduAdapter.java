package com.nearme.smartbuy.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearme.smartbuy.R;
import com.nearme.smartbuy.db.jobsreceived;

import java.util.List;


public class JobsArounduAdapter extends RecyclerView.Adapter<JobsArounduAdapter.Holder> {
    List<jobsreceived> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public JobsArounduAdapter(List<jobsreceived> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.jobs_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.empname.setText(homeArrayList.get(position).getEmployername());
        holder.location.setText(homeArrayList.get(position).getEmployerlocationurl());
        holder.desc.setText(homeArrayList.get(position).getJobDescription());
        holder.landmark.setText(homeArrayList.get(position).getLocationLandmark());
        holder.post.setText(homeArrayList.get(position).getOfferingpost());
        holder.education.setText(homeArrayList.get(position).getEducationQualification());
        holder.experience.setText(homeArrayList.get(position).getExperienceReq());
        holder.sex.setText(homeArrayList.get(position).getSex());
        holder.age.setText(homeArrayList.get(position).getAgeLimitation());
        holder.contact.setText(homeArrayList.get(position).getContact());
        holder.email.setText(homeArrayList.get(position).getEmailId());
        holder.date.setText(homeArrayList.get(position).getInterviewDate());
        holder.timing.setText(homeArrayList.get(position).getShiftTimings());
        holder.salary.setText(homeArrayList.get(position).getSalary());

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }

        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(homeArrayList.get(position).getEmployerlocationurl()));
                    mContext.startActivity(browserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView empname;
        TextView location;
        TextView desc;
        TextView landmark;
        TextView post;
        TextView education;
        TextView experience;
        TextView sex;
        TextView age;
        TextView contact;
        TextView email;
        TextView date;
        TextView timing;
        TextView salary;
        CardView cardview;


        public Holder(View itemView) {
            super(itemView);

//            title = itemView.findViewById(R.id.title);
//            rl = itemView.findViewById(R.id.rlmain);

            empname = itemView.findViewById(R.id.empname);
            location = itemView.findViewById(R.id.location);
            desc = itemView.findViewById(R.id.desc);
            landmark = itemView.findViewById(R.id.landmark);
            post = itemView.findViewById(R.id.post);
            education = itemView.findViewById(R.id.education);
            experience = itemView.findViewById(R.id.experience);
            sex = itemView.findViewById(R.id.sex);
            age = itemView.findViewById(R.id.age);
            contact = itemView.findViewById(R.id.contact);
            email = itemView.findViewById(R.id.email);
            date = itemView.findViewById(R.id.date);
            timing = itemView.findViewById(R.id.timing);
            salary = itemView.findViewById(R.id.salary);
            cardview = itemView.findViewById(R.id.cardview);

        }
    }
}
