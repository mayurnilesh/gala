package com.nearme.smartbuy.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.nearme.smartbuy.R;
import com.nearme.smartbuy.fragments.GroupOffersStatusFragment;
import com.nearme.smartbuy.rest.Statuspayload;

import java.util.List;

import static com.nearme.smartbuy.fragments.GroupOffersStatusFragment.status_contactNumber;


public class OfferstatusAdapter extends RecyclerView.Adapter<OfferstatusAdapter.Holder> {
    List<Statuspayload> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    GroupOffersStatusFragment.StatusInterface statusInterface;

    public OfferstatusAdapter(List<Statuspayload> homeArrayList, GroupOffersStatusFragment.StatusInterface statusInterface) {
        this.homeArrayList = homeArrayList;
        this.statusInterface = statusInterface;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.offerstatus_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.contact.setText(homeArrayList.get(position).contactOn);
        if (homeArrayList.get(position).flat == 1) {
            holder.discount.setText("Flat : " + String.valueOf(homeArrayList.get(position).value));
        } else {
            holder.discount.setText("Discount : " + String.valueOf(homeArrayList.get(position).value) + "%");
        }
        holder.validfrom.setText(homeArrayList.get(position).validfrom);
        holder.validto.setText(homeArrayList.get(position).validto);
        holder.offerdesc.setText(homeArrayList.get(position).offerdesc);
        holder.shoppingdate.setText(homeArrayList.get(position).shoppingdate);
        holder.value.setText(String.valueOf(homeArrayList.get(position).value));
        Glide.with(mContext)
                .load(homeArrayList.get(position).imgurl)
                .into(holder.offerImage);

        SpannableString content = new SpannableString("Joined Count " + homeArrayList.get(position).joinedcount.toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.joinnedcount.setText(content);

        if (homeArrayList.get(position).registeredcontactnumber.equalsIgnoreCase(status_contactNumber)
                || homeArrayList.get(position).isalreadyJoined == 1) {
            holder.joinnow.setVisibility(View.GONE);
        } else {
            holder.joinnow.setVisibility(View.VISIBLE);
        }

        holder.share.setOnClickListener(v -> {
            if (homeArrayList.get(position).flat == 1)
                statusInterface.onStatusClick("I got flat discount " + homeArrayList.get(position).value);
            else
                statusInterface.onStatusClick("I got discount " + homeArrayList.get(position).value + "%");
        });
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView contact, discount, validfrom, validto, offerdesc, shoppingdate, value, joinnedcount, joinnow, share;
        ImageButton offerImage;

        public Holder(View itemView) {
            super(itemView);

            contact = itemView.findViewById(R.id.contact);
            offerImage = itemView.findViewById(R.id.image1);
            discount = itemView.findViewById(R.id.discount);
            validfrom = itemView.findViewById(R.id.validfrom);
            validto = itemView.findViewById(R.id.validto);
            offerdesc = itemView.findViewById(R.id.offerdesc);
            shoppingdate = itemView.findViewById(R.id.shoppingdate);
            value = itemView.findViewById(R.id.value);
            joinnedcount = itemView.findViewById(R.id.joincount);
            joinnow = itemView.findViewById(R.id.joinnow);
            share = itemView.findViewById(R.id.share);

        }
    }
}
