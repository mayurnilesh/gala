package com.nearme.smartbuy.peoplearound;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nearme.smartbuy.utility.ServiceUtils;
import com.squareup.picasso.Target;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by gopi.komanduri on 12/09/18.
 */

public class peoplearoundservice  extends Service implements GeoQueryEventListener {

    private static GeoQuery geoQuery;
    private static GeoFire geoFire;
    static Target picassoImgTarget = null;

    public static ConcurrentHashMap<String, String> enteredKeys = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, String> userDetailsMap = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, String> thumbnailImagePath = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, File> thumbnailRealPath = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, String> enteredIds = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, Bitmap> decodedPics = new ConcurrentHashMap<>();
    public static HashMap<String,Integer> isInRange = new HashMap<>();

    public static int radius = 1;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                invokeGeoQuery();
                //Do something after 20 seconds
                handler.postDelayed(this, 10000);
            }
        }, 50000);  //the time is in miliseconds
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;

    }

    @Override
    public void onDestroy() {
        Intent intent = new Intent("com.nearme.gopikomanduri.around.SensorRestarterBroadcastReceiver");
        intent.putExtra("yourvalue", "torestore");
        sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void invokeGeoQuery()
    {
        DatabaseReference ref =  FirebaseDatabase.getInstance().getReference("UserLocations");
        geoFire = new GeoFire(ref);
        if(ServiceUtils.currentLoc != null) {
            geoQuery = geoFire.queryAtLocation(new GeoLocation(ServiceUtils.currentLoc.getLatitude(), ServiceUtils.currentLoc.getLongitude()), radius);
            this.geoQuery.addGeoQueryEventListener(this);
        }
    }

//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//
//
//
//
//        Log.d("SocketService", "In OnTabRemoved");
//
//        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
//
//        PendingIntent restartServicePendingIntent = PendingIntent.getService(
//                getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
//        AlarmManager alarmService = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarmService.set(ELAPSED_REALTIME, elapsedRealtime() + 1000,
//                restartServicePendingIntent);
//
//        super.onTaskRemoved(rootIntent);
//
//
//    }













    @Override
    public void onKeyEntered(final String key, GeoLocation location) {

        Log.i("GopiGeofire","Number is inside and key is "+key);
        final String enteredKey = key;

        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        Query query =  mDatabaseReference.orderByChild("user").equalTo(key);


        DatabaseReference dbref =  FirebaseDatabase.getInstance().getReference().child("user/" + key);
        dbref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot emailVal =  dataSnapshot.child("contact");
                //   Log.i("In contact", emailVal);

                String val = emailVal.getValue().toString();
                if(enteredKeys.get(val) == null) {
                    enteredKeys.put(val, key);

                    String thumbnailUrl = dataSnapshot.child("avata").getValue().toString();
                    //   String md5Name = md5.uniqueValue(thumbnailUrl);
                    thumbnailImagePath.put(val, thumbnailUrl);

                    String name = dataSnapshot.child("name").getValue().toString();
                    //if(userDetailsMap.contains(emailVal.getValue().toString()) == false)
                    //   {
                    userDetailsMap.put(val, name);
                    //   }
//                    if (GroupFragment.currentInstance != null) {
//                        GroupFragment.currentInstance.onRefresh();
//                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



//        DatabaseReference dbref1 =  FirebaseDatabase.getInstance().getReference().child("user/" + key);
//
//
//        dbref1.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                DataSnapshot userDetails = dataSnapshot.child("avata");
//                Iterable<DataSnapshot> userdetailsArray = userDetails.getChildren();
//
//
//         //       Log.i("In contact", emailVal);
//            //    enteredKeys.put(emailVal,key);
//                if(GroupFragment.currentInstance != null )
//                {
//                    GroupFragment.currentInstance.onRefresh();
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }

    @Override
    public void onKeyExited(String key) {
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(DatabaseError error) {

    }
}
