package com.nearme.smartbuy.location;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nearme.smartbuy.db.maxadidreceived;
import com.nearme.smartbuy.db.mybizannouncements;
import com.nearme.smartbuy.db.negotiationrequests;
import com.nearme.smartbuy.db.negotiationresponsesreceived;
import com.nearme.smartbuy.db.userdetails;
import com.nearme.smartbuy.events.updateUi;
import com.nearme.smartbuy.fragments.list_item_base;
import com.nearme.smartbuy.geohashutil.BoundingHashes;
import com.nearme.smartbuy.geohashutil.GeoHash;
import com.nearme.smartbuy.geohashutil.latlngcls;
import com.nearme.smartbuy.rest.AdPayLoadResponse;
import com.nearme.smartbuy.rest.ApiClient;
import com.nearme.smartbuy.rest.ApiInterface;
import com.nearme.smartbuy.rest.LastReceivedAdStruct;
import com.nearme.smartbuy.rest.NegotationPayLoadResponse;
import com.nearme.smartbuy.startup.MainActivity;
import com.nearme.smartbuy.utility.ConnectionDetector;
import com.nearme.smartbuy.utility.ServiceUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.AlarmManager.ELAPSED_REALTIME;
import static android.os.SystemClock.elapsedRealtime;

public class MyLocationListener extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GeoQueryEventListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {


    private static GeoQuery geoQuery;
    private static GeoFire geoFire;

    private static boolean isCalled = false;


    protected static final String TAG = "MainActivity";

    //Any random number you can take
    public static final int REQUEST_PERMISSION_LOCATION = 1000;

    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000 * 60 * 5;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS;

    /**
     * Provides the entry point to Google Play services.
     */
    protected static GoogleApiClient mGoogleApiClient = null;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;

    /**
     * Represents a geographical location.
     */
    public static Location mCurrentLocation;


    public static ConcurrentHashMap<String, String> enteredKeys = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, String> userDetailsMap = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, String> thumbnailImagePath = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, File> thumbnailRealPath = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, String> enteredIds = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, Bitmap> decodedPics = new ConcurrentHashMap<>();
    public static HashMap<String, Integer> isInRange = new HashMap<>();

    public static int radius = 1;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;


    int RQS_GooglePlayServices = 0;

    public static List<list_item_base> ads = new ArrayList<>();
    public List<list_item_base> adsTemp = new ArrayList<>();

    public static HashMap<String, Integer> maxIdsReceived = new HashMap<>();
    public static HashMap<String, Integer> maxJobIdsReceived = new HashMap<>();


    protected void connectGoogleApiClient() {

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);

        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                if (mGoogleApiClient.isConnected()) {
                    createLocationRequest();
                    buildLocationSettingsRequest();
                    checkLocationSettings();
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
            }
        });

        if (resultCode == ConnectionResult.SUCCESS) {
            mGoogleApiClient.connect();
        } else {
            if (MainActivity.mMainActivity != null)
                googleAPI.getErrorDialog(MainActivity.mMainActivity, resultCode, RQS_GooglePlayServices);
        }


    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if (isCalled)
            return;
        isCalled = true;

        if (ConnectionDetector.isConnectingToInternet(MyLocationListener.this)) {
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (statusOfGPS) {
               // invokeAdRequest(location);
                invokeFetchNegotiationResponses();
            } else
                Toast.makeText(this, "Gps Disabled", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    private void invokeFetchNegotiationResponses() {

        String qry = "select * , min(negotiationid) from negotiationrequests  group by geohash";
        List<Integer> maxresponseids = new ArrayList<>();
        List<String> geohashs = new ArrayList<>();


        try {
            Iterator<negotiationrequests> ids = negotiationrequests.
                    findWithQueryAsIterator
                            (negotiationrequests.class, qry, null);

            while (ids.hasNext()) {

                negotiationrequests temp = ids.next();
                maxresponseids.add(temp.negotiationid);
                geohashs.add(temp.geohash);
            }
        } catch (Exception ex) {
            int y = 40;
            ++y;
        }
        Log.d(TAG, "invokeFetchNegotiationResponses: " + maxresponseids.size());
        Log.d(TAG, "invokeFetchNegotiationResponses: " + geohashs.toString());
        if (maxresponseids.size() != geohashs.size())
            return;

        String jsonIds = new Gson().toJson(maxresponseids);
        String jsongeohashs = new Gson().toJson(geohashs);
        List<userdetails> temp = userdetails.listAll(userdetails.class);
        String phoneNumber = "";
        if (temp.size() > 0)
            phoneNumber = temp.get(0).ContactNumber;


        Log.d(TAG, "invokeFetchNegotiationResponses: " + phoneNumber + " " + jsongeohashs + " " + jsonIds);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<NegotationPayLoadResponse[]> call = apiService.fetchnegotiationresponse(phoneNumber, jsongeohashs, jsonIds);
        call.enqueue(new Callback<NegotationPayLoadResponse[]>() {
            @Override
            public void onResponse(Call<NegotationPayLoadResponse[]> call, Response<NegotationPayLoadResponse[]> response) {
                Type type = new TypeToken<List<NegotationPayLoadResponse>>() {
                }.getType();

                NegotationPayLoadResponse[] npr = response.body();
                if (npr == null)
                    return;
                Log.d(TAG, "negoonResponse: " + npr.length);
                for (int i = 0; i < npr.length; i++) {
                    NegotationPayLoadResponse obj = npr[i];
                    negotiationresponsesreceived object = new negotiationresponsesreceived(obj.idnegotiationresponse,
                            obj.advanceNeeded, obj.negotiationresponse, obj.customercontact, obj.merchantid,
                            obj.geohash, obj.minamount, obj.maxamount, obj.DiscountExpectation,
                            obj.ShoppingProbableDates, obj.description, obj.negotiationresponse, obj.notificationid);
                    object.setCanPostToStatus(obj.canPostToStatus);
                    try {
                        object.save();
                        negotiationrequests.executeQuery("delete from negotiationrequests " +
                                "where notificationid=" + obj.notificationid + " AND geohash='" + obj.geohash + "'");
                    } catch (Exception ex) {
                        int z = 0;
                        ++z;
                    }
                }

            }

            @Override
            public void onFailure(Call<NegotationPayLoadResponse[]> call, Throwable t) {

                int z = 50;
                ++z;
            }
        });


    }

    public void invokeAdRequest(Location location) {
        String[] params = new String[2];

        mCurrentLocation = location;
        double latitude = mCurrentLocation.getLatitude();
        double longitude = mCurrentLocation.getLongitude();

        final String geoHash = GeoHash.fromCoordinates(latitude, longitude, 5).toString();
        List<LastReceivedAdStruct> lastReceivedAdStructList = new ArrayList<>();

        List<latlngcls> boundedLatLngs = BoundingHashes.create_geohash(latitude, longitude, 2000, 5, false, 1, 12);

        HashMap<String, LastReceivedAdStruct> uniquesHash = new HashMap<>();
//        for (int i = 0; i < boundedLatLngs.size(); i++) {
//            latlngcls temp = boundedLatLngs.get(i);
//            LastReceivedAdStruct obj = new LastReceivedAdStruct();
//            obj.geoHash = GeoHash.fromCoordinates(temp.x, temp.y, 6).toString();
//            if (uniquesHash.containsKey(obj.geoHash))
//                continue;
//            try {
//                obj.lastReceivedAdId = 0;
//
//                List<maxadidreceived> ids = maxadidreceived.find(maxadidreceived.class, "geo_hash = ?", new String[]{obj.geoHash}, null, "notificationid DESC", null);
//
//                if (ids.size() > 0)
//                    obj.lastReceivedAdId = ids.get(0).notificationid;
//
//            } catch (Exception ex) {
//                if (ex.getMessage().contains(" no such table")) {
//                    new maxadidreceived("test", 1).save();
//                }
//                obj.lastReceivedAdId = 0;
//            }
//            uniquesHash.put(obj.geoHash, obj);
//            lastReceivedAdStructList.add(obj);
//
//        }
//        Gson gson = new Gson();
//        String jsonConvertedlastReceivedAdStructList = gson.toJson(lastReceivedAdStructList);
//
//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//
//        Call<AdPayLoadResponse[]> call = apiService.getText(jsonConvertedlastReceivedAdStructList);
//        call.enqueue(new Callback<AdPayLoadResponse[]>() {
//            @Override
//            public void onResponse(Call<AdPayLoadResponse[]> call, Response<AdPayLoadResponse[]> response) {
//
//                AdPayLoadResponse[] receivedAds = response.body();
//
//                if (receivedAds != null && receivedAds.length > 0) {
//                    for (int i = 0; i < receivedAds.length; i++) {
//
//                        mybizannouncements obj = new mybizannouncements(receivedAds[i].Id, receivedAds[i].geo, receivedAds[i].lat, receivedAds[i].lng, receivedAds[i].merchantid,
//                                receivedAds[i].cat, receivedAds[i].tilldate, receivedAds[i].tillmonth,
//                                receivedAds[i].tillyear, receivedAds[i].fromdate, receivedAds[i].frommonth
//                                , receivedAds[i].fromyear, receivedAds[i].offercode,
//                                receivedAds[i].itemdesc, receivedAds[i].imgUrl, receivedAds[i].shopname,
//                                receivedAds[i].shopDp, receivedAds[i].negotiate, receivedAds[i].minBusiness);
//
//                        obj.save();
//
//                        try {
//                            Integer maxId = null;
//
//                            if ((maxId = maxIdsReceived.get(receivedAds[i].geo)) == null) {
//                                maxadidreceived adObj = (maxadidreceived.find(maxadidreceived.class, "geo_hash = ?", receivedAds[i].geo)).get(0);
//                                adObj.notificationid = receivedAds[i].Id;
//                                maxIdsReceived.put(receivedAds[i].geo, maxId);
//                                adObj.save();
//                            } else if ((maxId = maxIdsReceived.get(receivedAds[i].geo)) != null && maxId < receivedAds[i].Id) {
//                                maxIdsReceived.put(receivedAds[i].geo, maxId);
//                                maxadidreceived adObj = (maxadidreceived.find(maxadidreceived.class, "geo_hash = ?", receivedAds[i].geo)).get(0);
//                                adObj.notificationid = receivedAds[i].Id;
//                                adObj.save();
//                            }
//
//                        } catch (Exception ex) {
//                            maxIdsReceived.put(receivedAds[i].geo, receivedAds[i].Id);
//                            maxadidreceived adObj = new maxadidreceived(receivedAds[i].geo, receivedAds[i].Id);
//                            adObj.save();
//                        }
//
//                        String imageurl = receivedAds[i].imgUrl;
//                        String validTill = receivedAds[i].tilldate + "/" + receivedAds[i].tillmonth + "/" + receivedAds[i].tillyear;
//                        String offer = receivedAds[i].offercode;
//                        String shopid = receivedAds[i].merchantid;
////          mGoogleApiClient  shopdetails shop = shopdetails.findById(shopdetails.class,shopid);
//                        Double lat = receivedAds[i].lat;
//                        Double lng = receivedAds[i].lng;
//                        String shopname = receivedAds[i].shopname;
////            String mapurl = "https://www.google.com/maps/?q="+shop.lat+","+shop.lng;
//                        String mapurl = "https://www.google.com/maps/?q=" + lat + "," + lng;
//                        String offerDesc = receivedAds[i].itemdesc;
//                        int notificationId = receivedAds[i].Id;
//                        Integer negotiation = receivedAds[i].negotiate;
//                        Integer minBusiness = receivedAds[i].minBusiness;
//
//                        list_item_base itemsToAddIntoList = new list_item_base(imageurl, validTill,
//                                shopname, offer, mapurl,
//                                offerDesc, notificationId, negotiation, minBusiness);
//                        itemsToAddIntoList.setLat(lat);
//                        itemsToAddIntoList.setLng(lng);
//
//                        adsTemp.add(itemsToAddIntoList);
//                        //ads.add(itemsToAddIntoList);
//                    }
//
//                    List<list_item_base> temp = new ArrayList<>();
//
//                    temp.addAll(adsTemp);
//                    temp.addAll(ads);
//                    ads.clear();
//                    ads.addAll(temp);
//                    adsTemp.clear();
//                    temp.clear();
//
//                    String adsReceived = response.body().toString();
//                    Log.d(TAG, "onResponse: " + adsReceived);
//
//                }
//                isCalled = false;
//            }
//
//            @Override
//            public void onFailure(Call<AdPayLoadResponse[]> call, Throwable t) {
//                // Log error here since request failed
//                Log.e(TAG, t.toString());
//            }
//        });
//

        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        params[0] = "OnLocationChanged Invoked";
        params[1] = " at " + mLastUpdateTime;
        ServiceUtils.updateCurrentLocation(mCurrentLocation);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(this, MainActivity.class));
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");

                //Toast.makeText(FusedLocationWithSettingsDialog.this, "Location is already on.", Toast.LENGTH_SHORT).show();
                if (mGoogleApiClient.isConnected() == true) {
                    startLocationUpdates();
                }
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildGoogleApiClient();
        connectGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationSettings();
        //  return super.onStartCommand(intent, flags, startId);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                invokeGeoQuery();
                //Do something after 20 seconds
                handler.postDelayed(this, 10000);
            }
        }, 50000);  //the time is in miliseconds
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Intent intent = new Intent("com.nearme.gopikomanduri.around.SensorRestarterBroadcastReceiver");
        intent.putExtra("yourvalue", "torestore");
        sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {


        Log.d("SocketService", "In OnTabRemoved");

        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(
                getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmService.set(ELAPSED_REALTIME, elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);


    }

    //step 1
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
    }

    //step 2
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //step 3
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    //step 4
    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {

//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_LOCATION);
//        } else {
        goAndDetectLocation();
        //  }

    }

    public void goAndDetectLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
                //     setButtonsEnabledState();
            }
        });
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
                //   setButtonsEnabledState();
            }
        });
    }

    @Override
    public void onKeyEntered(final String key, GeoLocation location) {

        Log.i("GopiGeofire", "Number is inside and key is " + key);
        final String enteredKey = key;

        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = mDatabaseReference.orderByChild("user").equalTo(key);


        DatabaseReference dbref = FirebaseDatabase.getInstance().getReference().child("user/" + key);
        dbref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot emailVal = dataSnapshot.child("contact");
                //   Log.i("In contact", emailVal);

                if (emailVal != null && emailVal.getValue() != null) {
                    String val = emailVal.getValue().toString();
                    if (enteredKeys.get(val) == null) {
                        enteredKeys.put(val, key);

                        String thumbnailUrl = dataSnapshot.child("avata").getValue().toString();
                        //   String md5Name = md5.uniqueValue(thumbnailUrl);
                        thumbnailImagePath.put(val, thumbnailUrl);

                        String name = dataSnapshot.child("name").getValue().toString();
                        //if(userDetailsMap.contains(emailVal.getValue().toString()) == false)
                        //   {
                        userDetailsMap.put(val, name);
                        //   }
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onKeyExited(String key) {

    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(DatabaseError error) {

    }

    public void invokeGeoQuery() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("UserLocations");
        geoFire = new GeoFire(ref);
        if (ServiceUtils.currentLoc != null) {
            double radius = 1.0;
            geoQuery = geoFire.queryAtLocation(new GeoLocation(ServiceUtils.currentLoc.getLatitude(), ServiceUtils.currentLoc.getLongitude()), radius);
            this.geoQuery.addGeoQueryEventListener(this);
        }
    }
}
