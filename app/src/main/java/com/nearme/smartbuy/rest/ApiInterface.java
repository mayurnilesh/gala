package com.nearme.smartbuy.rest;

import com.nearme.smartbuy.db.NegotiationResponse;
import com.nearme.smartbuy.model.Merchant;
import com.nearme.smartbuy.model.Merchantslot;
import com.nearme.smartbuy.model.UserSlot;
import com.nearme.smartbuy.utility.PointsClass;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiInterface {
    @POST("ads")
    Call<AdPayLoadResponse[]> getText(@Body List<LastReceivedAdStruct> lastReceivedAdStructList);


    @POST("transaction")
    Call<PointsClass> updateTransaction(@Query("merchant") String merchant, @Query("customer") String customer,
                                        @Query("offer") String offer);

    @POST("latestversionconsumer")
    Call<versionclass> getVersion();

    @POST("pushjob")
    Call<String> getPushJob(@Query("job") String merchant);

    @POST("getsupportedjobs")
    Call<supportedjobs[]> getsupportedjobs();

    @POST("getjobsaround")
    Call<JobPayLoadResponse[]> getjobsaround(@Query("geohash") String geohash);


    @POST("pushstatus")
    Call<String> PushStatus(@Query("status") String statuspayload);

    @POST("StartNegotiate")
    Call<String> StartNegotiate(@Query("negotiate") String negotiate);

    @POST("fetchnegotiationresponse")
    Call<NegotationPayLoadResponse[]> fetchnegotiationresponse(@Query("customercontact") String customercontact, @Query("geohashlist") String geohashlist,
                                                               @Query("lastidlist") String lastidlist);

    @POST("RegUser")
    Call<String> RegUser(@Query("consumerpayload") String consumerpayload);

    @POST("joinad")
    Call<String[]> JoinStatus(@Query("joincountpayload") String joincountpayload);

    @POST("LoginUser")
    Call<consumerpayload> LoginUser(@Query("consumerpayload") String consumerpayload, @Query("consumerFirebasepayload") String consumerFirebasepayload);

    @POST("pushstatus")
    Call<String> pushstatus(@Query("status") String dataReceived);

    @POST("fetchstatuses")
    Call<Statuspayload[]> fetchstatuses(@Query("geohash") String geohash, @Query("lastreceivedstatusid") String lastreceivedstatusid, @Query("userID") String userID);

    @POST("respondtostatus")
    Call<String[]> respondtostatus(@Query("geohash") String geohash, @Query("contact") String contact, @Query("statusid") String statusid);

    @POST("fetchalljoinees")
    Call<String[]> fetchalljoinees(@Query("statusid") String statusid, @Query("geohash") String geohash);

    @POST("getmerchanttokendetails")
    Call<tokenregistrationpayload> getmerchanttokendetails(@Query("merchantid") String statusid);

    @POST("registerfortoken")
    Call<tokenstatus> registerfortoken(@Query("merchantid") String merchantid, @Query("consumercontact") String consumercontact, @Query("consumerFireID") String firebaseToken);

    @POST("renewtoken")
    Call<String> regenrateToken(@Query("merchantid") String merchantid, @Query("token") String token);


    @POST("getforspecificcontact")
    Call<contactspecificinfo[]> getforspecificcontact(@Query("contact") String contact, @Query("lastid") String lastid);

    @POST("gettokenstatus")
    Call<String> gettokenstatus(@Query("merchantid") String merchantid, @Query("token") String consumercontact, @Query("contact") String contact, @Query("existingStatus") String existingStatus);

    @POST("deregister")
    Call<String> deregister(@Query("merchantid") String merchantid, @Query("contact") String contact, @Query("existingtoken") String existingtoken, @Query("firebaseID") String firebaseID);

    @POST("fetchconsumernegotiations")
    Call<NegotiationResponse[]> fetchConsumerNegotiations(@Query("contact") String contact, @Query("lastid") String lastid);

    @POST("getalltokenstatus")
    Call<tokenstatuspayload[]> getalltokenstatus(@Query("tokenstatuspayload") String tokenstatuspayload);

    @POST("getMerchantsAround")
    Call<Merchant[]> getAroundMerchant(@Query("geohashes") String geohashes);

    @POST("getMerchantSlots")
    Call<Merchantslot[]> getSlots(@Query("MerchantID") String MerchantID, @Query("FromTime") String FromTime, @Query("toTime") String toTime);

    @POST("registerTokenforSlot")
    Call<String> registerSlots(@Query("merchantid") String merchantid, @Query("consumercontact") String consumercontact
            , @Query("epochID") String epochID, @Query("EpochStarttime") String EpochStarttime, @Query("tokensrequested") String tokensrequested);

    @POST("getUserSlots")
    Call<UserSlot[]> getRegisteredSlots(@Query("UserContact") String UserContact, @Query("FromTime") String FromTime, @Query("toTime") String toTime);

    @POST("fetchacategories")
    Call<CategoriesPayLoad[]> getCategories();

    @POST("getmerchantdetails")
    Call<MerchantPayload> getMerchantDetails(@Query("merchantcontact") String merchantcontact);

    @POST("RegMerchant")
    Call<MerchantPayload> RegMerchant(@Query("merchant") String merchantdetails);

    @POST("pushad")
    Call<AdPayLoad> pushAd(@Query("ad") String lastReceivedList);

}