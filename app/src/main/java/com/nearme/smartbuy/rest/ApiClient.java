package com.nearme.smartbuy.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

 //   public static final String BASE_URL = "http://35.196.161.59:5557/";
    public static final String BASE_URL = "http://13.59.119.212:5556/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;//
    }
}
