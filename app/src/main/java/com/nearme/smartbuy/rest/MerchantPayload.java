package com.nearme.smartbuy.rest;

import com.google.gson.annotations.SerializedName;

public class MerchantPayload {
    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantPhn() {
        return merchantPhn;
    }

    public void setMerchantPhn(String merchantPhn) {
        this.merchantPhn = merchantPhn;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public String getShopNo() {
        return shopNo;
    }

    public void setShopNo(String shopNo) {
        this.shopNo = shopNo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getGeoHash() {
        return geoHash;
    }

    public void setGeoHash(String geoHash) {
        this.geoHash = geoHash;
    }

    public String getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(String registeredOn) {
        this.registeredOn = registeredOn;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    @SerializedName("merchantName")
    public String merchantName;

    @SerializedName("merchantPhn")
    public String merchantPhn;

    @SerializedName("merchantId")
    public String merchantId;

    @SerializedName("locality")
    public String locality;

    @SerializedName("landMark")
    public String landMark;

    @SerializedName("shopNo")
    public String shopNo;

    @SerializedName("latitude")
    public Double latitude;

    @SerializedName("longitude")
    public Double longitude;

    @SerializedName("geoHash")
    public String geoHash;

    @SerializedName("registeredOn")
    public String registeredOn;

    @SerializedName("isActive")
    public String isActive;

    @SerializedName("imgurl")
    public String imgurl;

    @SerializedName("merchantIdProof")
    public String merchantIdProof;

    @SerializedName("merchantIdProofUri")
    public String merchantIdProofUri;

    @SerializedName("area")
    public String area;

    @SerializedName("state")
    public String state;

    @SerializedName("country")
    public String country;

    @SerializedName("city")
    public String city;

    @SerializedName("password")
    public String password;

    @SerializedName("role")
    public Integer role;

    public MerchantPayload(String merchantName, String merchantPhn, String merchantId, String locality, String landMark, String shopNo, Double latitude,
                           Double longitude, String geoHash, String registeredOn, String isActive, String imgurl, String area, String state, String country,
                           String city, String password, Integer role, String merchantIdProof, String merchantIdProofUri) {
        this.merchantName = merchantName;
        this.merchantPhn = merchantPhn;
        this.merchantId = merchantId;
        this.locality = locality;
        this.landMark = landMark;
        this.shopNo = shopNo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.geoHash = geoHash;
        this.merchantIdProof=merchantIdProof;
        this.merchantIdProofUri=merchantIdProofUri;
        this.registeredOn = registeredOn;
        this.isActive = isActive;
        this.imgurl = imgurl;
        this.area = area;
        this.state = state;
        this.country = country;
        this.city = city;
        this.role=role;
        this.password=password;
    }
}
