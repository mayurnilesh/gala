package com.nearme.smartbuy.rest;

import com.google.gson.annotations.SerializedName;

public class ChatListPayLoad {
    public Integer getCatid() {
        return catid;
    }

    public void setCatid(Integer catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getCatimg() {
        return catimg;
    }

    public void setCatimg(String catimg) {
        this.catimg = catimg;
    }

    @SerializedName("catid")
    public Integer catid;

    @SerializedName("catname")
    public String catname;

    @SerializedName("catimg")
    public String catimg;
}