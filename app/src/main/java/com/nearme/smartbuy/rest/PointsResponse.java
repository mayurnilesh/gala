package com.nearme.smartbuy.rest;

/**
 * Created by gopi.komanduri on 09/08/18.
 */

public class PointsResponse {
    public int addedPoints;
    public int totalPoints;
}
