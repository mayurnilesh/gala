package com.nearme.smartbuy.rest;

public class contactspecificinfo {

    String imgurl;
    Integer type;
    String merchantname;
    String date;
    String merchantreceiptid;
    Integer Id;
    String billamount;

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMerchantreceiptid() {
        return merchantreceiptid;
    }

    public void setMerchantreceiptid(String merchantreceiptid) {
        this.merchantreceiptid = merchantreceiptid;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getBillamount() {
        return billamount;
    }

    public void setBillamount(String billamount) {
        this.billamount = billamount;
    }
}
