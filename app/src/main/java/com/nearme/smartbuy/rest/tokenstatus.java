package com.nearme.smartbuy.rest;

public class tokenstatus {
    public Integer token;
    public Integer youareat;

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public Integer getYouareat() {
        return youareat;
    }

    public void setYouareat(Integer youareat) {
        this.youareat = youareat;
    }
}
