package com.nearme.smartbuy.rest;

public class AdPayLoad {
    public String geo;
    public Double lat;
    public Double lng;
    public String merchantid;
    public String cat;
    public String tilldate;
    public String tillmonth;
    public String tillyear;
    public String fromdate;
    public String frommonth;
    public String fromyear;
    public String offercode;
    public String itemdesc;
    public String imgUrl;
    public String shopname;
    public String shopDp;
    public Integer negotiate;
    public Integer minBusiness;
    public String mindiscount;
    public String maxdiscount;
    public String discdesc;
    public String contatNumber;


    public String getGeo() {
        return geo;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public String getCat() {
        return cat;
    }

    public String getTilldate() {
        return tilldate;
    }

    public String getTillmonth() {
        return tillmonth;
    }

    public String getTillyear() {
        return tillyear;
    }

    public String getFromdate() {
        return fromdate;
    }

    public String getFrommonth() {
        return frommonth;
    }

    public String getFromyear() {
        return fromyear;
    }

    public String getOffercode() {
        return offercode;
    }

    public String getItemdesc() {
        return itemdesc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getShopname() {
        return shopname;
    }

    public String getShopDp() {
        return shopDp;
    }

    public Integer getNegotiate() {
        return negotiate;
    }

    public Integer getMinBusiness() {
        return minBusiness;
    }
}