package com.nearme.smartbuy.rest;

/**
 * Created by gopi.komanduri on 09/10/18.
 */

public class tokenregistrationpayload  {

    public String  merchantname;
    public String merchanturl;
    public Integer currentMaxToken;
    public Integer currentRunningToken;

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getMerchanturl() {
        return merchanturl;
    }

    public void setMerchanturl(String merchanturl) {
        this.merchanturl = merchanturl;
    }

    public Integer getCurrentMaxToken() {
        return currentMaxToken;
    }

    public void setCurrentMaxToken(Integer currentMaxToken) {
        this.currentMaxToken = currentMaxToken;
    }

    public Integer getCurrentRunningToken() {
        return currentRunningToken;
    }

    public void setCurrentRunningToken(Integer currentRunningToken) {
        this.currentRunningToken = currentRunningToken;
    }
}
