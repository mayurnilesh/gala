package com.nearme.smartbuy.rest;

/**
 * Created by gopi.komanduri on 07/10/18.
 */

public class NegotationPayLoad {

    /*

    idnegotations int(11) AI PK
customercontact varchar(15)
merchantid varchar(128)
geohash varchar(6)
minamount int(4)
maxamount int(11)
DiscountExpectation int(3)
ShoppingProbableDates varchar(45)
description varchar(512)
delivered int(1)
adnotification int(11)
     */
    public String customercontact;
    public String merchantid;
    public String geohash;
    public Integer minamount;
    public Integer maxamount;
    public Integer DiscountExpectation;
    public String ShoppingProbableDates;
    public String description;
    public Integer delieverd;
    public Integer response;
    public Integer notificationid;
}