package com.nearme.smartbuy.rest;

import com.google.gson.annotations.SerializedName;

public class AdRequest {
    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public Integer getTilldate() {
        return tilldate;
    }

    public void setTilldate(Integer tilldate) {
        this.tilldate = tilldate;
    }

    public Integer getTillmonth() {
        return tillmonth;
    }

    public void setTillmonth(Integer tillmonth) {
        this.tillmonth = tillmonth;
    }

    public Integer getTillyear() {
        return tillyear;
    }

    public void setTillyear(Integer tillyear) {
        this.tillyear = tillyear;
    }

    public Integer getFromdate() {
        return fromdate;
    }

    public void setFromdate(Integer fromdate) {
        this.fromdate = fromdate;
    }

    public Integer getFrommonth() {
        return frommonth;
    }

    public void setFrommonth(Integer frommonth) {
        this.frommonth = frommonth;
    }

    public Integer getFromyear() {
        return fromyear;
    }

    public void setFromyear(Integer fromyear) {
        this.fromyear = fromyear;
    }

    public String getOffercode() {
        return offercode;
    }

    public void setOffercode(String offercode) {
        this.offercode = offercode;
    }

    public String getItemdesc() {
        return itemdesc;
    }

    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getNegotiate() {
        return negotiate;
    }

    public void setNegotiate(Integer negotiate) {
        this.negotiate = negotiate;
    }

    public Integer getMinBusiness() {
        return minBusiness;
    }

    public void setMinBusiness(Integer minBusiness) {
        this.minBusiness = minBusiness;
    }

    @SerializedName("geo")
    public   String geo;

    @SerializedName("lat")
    public String lat;

    @SerializedName("lng")
    public String lng;

    @SerializedName("merchantid")
    public  String merchantid;

    @SerializedName("cat")
    public String cat;

    @SerializedName("tilldate")
    public  Integer tilldate;

    @SerializedName("tillmonth")
    public Integer tillmonth;

    @SerializedName("tillyear")
    public Integer tillyear;

    @SerializedName("fromdate")
    public  Integer fromdate;

    @SerializedName("frommonth")
    public  Integer frommonth;

    @SerializedName("fromyear")
    public  Integer fromyear;

    @SerializedName("offercode")
    public  String offercode;

    @SerializedName("itemdesc")
    public  String itemdesc;

    @SerializedName("imgUrl")
    public  String imgUrl;

    @SerializedName("negotiate")
    public Integer negotiate;

    @SerializedName("minBusiness")
    public Integer minBusiness;

    @SerializedName("mindiscount")
    public String mindiscount;
    @SerializedName("maxdiscount")
    public String maxdiscount;
    @SerializedName("discdesc")
    public String discdesc;

    public AdRequest(String geo, String lat, String lng,
                     String merchantid, String cat,
                     Integer tilldate, Integer tillmonth, Integer tillyear,
                     Integer fromdate, Integer frommonth, Integer fromyear,
                     String offercode, String itemdesc, String imgUrl,
                     Integer negotiate, Integer minBusiness, String mindiscount, String maxdiscount, String discdesc) {
        this.geo = geo;
        this.lat = lat;
        this.lng = lng;
        this.merchantid = merchantid;
        this.cat = cat;
        this.tilldate = tilldate;
        this.tillmonth = tillmonth;
        this.tillyear = tillyear;
        this.fromdate = fromdate;
        this.frommonth = frommonth;
        this.fromyear = fromyear;
        this.offercode = offercode;
        this.itemdesc = itemdesc;
        this.imgUrl = imgUrl;
        this.negotiate = negotiate;
        this.minBusiness = minBusiness;
        this.mindiscount=mindiscount;
        this.maxdiscount=maxdiscount;
        this.discdesc=discdesc;
    }
}