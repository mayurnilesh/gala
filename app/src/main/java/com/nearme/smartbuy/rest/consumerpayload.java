package com.nearme.smartbuy.rest;

import com.nearme.smartbuy.firebase.consumerFirebasepayload;

/**
 * Created by gopi.komanduri on 18/10/18.
 */

public class consumerpayload {

        /*

        idconsumers int(11) AI PK
    consumername varchar(128)
    contact varchar(15)
    DOB date
    dpurl varchar(256)
    registeredon varchar(45)
    status int(11)
         */
        public String idconsumers;
        public String consumername;
        public String contact;
        public String dob;
        public String dpurl;
        public Integer sex;
        public consumerFirebasepayload FirebaseDetails;
}
