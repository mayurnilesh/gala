package com.nearme.smartbuy.rest;

/**
 * Created by gopi.komanduri on 09/10/18.
 */

public class NegotationPayLoadResponse extends NegotationPayLoad{

    public Integer idnegotiationresponse;
    public Integer advanceNeeded;
    public Integer negotiationresponse;
    public Integer canPostToStatus;
}
