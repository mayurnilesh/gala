/*
Copyright (c) AppDynamics, Inc., and its affiliates
2018
All Rights Reserved
*/

#ifndef APPD_APP_AGENT_ANALYTICS_COLLECTOR_H
#define APPD_APP_AGENT_ANALYTICS_COLLECTOR_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
using boost::asio::ip::tcp;
using boost::property_tree::ptree; using boost::property_tree::read_json; using boost::property_tree::write_json;

class AnalyticsCollector
{
public:
  AnalyticsCollector();
  void recordTransaction();
  void reportData();
  ~AnalyticsCollector();
private:
  boost::shared_ptr< boost::asio::io_service > m_io_service;
};


#endif
