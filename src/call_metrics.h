#ifndef __call_metrics
#define __call_metrics

/**
 * Keeps track of general metrics associated with a particular call site.
 */

class CallMetrics : boost::noncopyable {
    public:
        CallMetrics() : m_callCount(0)
                      , m_totalTimeInMicros(0)
                      , m_errorCount(0)
                      , m_minCallTime(-1)
                      , m_maxCallTime(-1)
        { }

        /**
         * Returns total accumulated time, in microseconds.
         */
        long getTotalTime() { return m_totalTimeInMicros; }

        /**
         * Returns total accumulated calls.
         */
        long getNumCalls() { return m_callCount; }

        /**
         * Returns total accumulated errors.
         */
        long getNumErrors() { return m_errorCount; }

        /**
         * Returns time of shortest call.
         */
        long getMinCallTime() { return m_minCallTime; }

        /**
         * Returns time of longest call.
         */
        long getMaxCallTime() { return m_maxCallTime; }

        /**
         * Accumulates given time period into the total.
         *
         * @param time Time period to accumulate (in microseconds).
         */
        inline void reportTime(long time) {
            m_totalTimeInMicros += time;
            if (time < m_minCallTime || m_minCallTime == -1)
                m_minCallTime = time;
            if (time > m_maxCallTime || m_maxCallTime == -1)
                m_maxCallTime = time;
        }

        /**
         * Accumulates number of calls into the total.
         *
         * @param count Number of calls to accumulate.
         */
        inline void reportCalls(long count) { m_callCount += count; }

        /**
         * Accumulates number of errors into the total.
         *
         * @param count Number of errors to accumulate.
         */
        inline void reportErrors(long count) { m_errorCount += count; }

    private:
        long m_callCount;
        long m_totalTimeInMicros;
        long m_errorCount;
        long m_minCallTime;
        long m_maxCallTime;
};

#endif // __call_metrics
