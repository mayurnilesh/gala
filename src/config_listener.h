/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __CONFIG_LISTENER_H
#define __CONFIG_LISTENER_H

namespace appdynamics
{
    namespace pb
    {
        class ConfigResponse;
    }
}

struct _zend_agent_globals;

/**
    Interface implemented by classes that wish to be notified when new
    configuration data is received from the proxy.
 */
class IConfigListener
{
    public:
        /**
            Called when ever new configuration data is received from the proxy.
            @param configResponse The configuration data.
         */
        virtual void configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                   const struct _zend_agent_globals* agentGlobals) = 0;
};


#endif
