/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#include "agent.h"
#include "agent_api.h"
#include "correlation.h"
#include "entrypoint.h"
#include "transactions.h"
#include "zval_helper.h"
#include "PHPAgentProtobufs.pb.h"
#include "db/sqlutil.h"

extern "C"
{
#include "Zend/zend_exceptions.h"
}

zend_class_entry* exit_call_ce = NULL;

static const char* const DISALLOWED_CHARS_IN_TX_NAME = "{}[]|&;";

static const size_t DISALLOWED_CHARS_IN_TX_NAME_COUNT =
    sizeof(DISALLOWED_CHARS_IN_TX_NAME) / sizeof(DISALLOWED_CHARS_IN_TX_NAME[0]);



#if PHP_VERSION_ID >= 70000
zend_object_handlers exit_call_handlers;
static php_exit_call_object* exit_call_new(zend_class_entry *ce)
{
    php_exit_call_object* intern;
    intern = (php_exit_call_object*) ecalloc(1, sizeof(php_exit_call_object) + zend_object_properties_size(ce));
    zend_object_std_init(&intern->std, ce); // also does zend_objects_store_put()
    object_properties_init(&intern->std, ce);

    /* *** setting the object handlers (includes dtor, free_storage, clone handlers) *** */
    // intern->std.handlers is declared as const zend_object_handlers *handlers,
    // so we need to point it to the global variable exit_call_handlers.
    intern->std.handlers = &exit_call_handlers;
    memcpy(&exit_call_handlers, zend_get_std_object_handlers(), sizeof(exit_call_handlers));
    // set the offset (default offset obtained from zend_get_std_object_handlers() is always 0)
    exit_call_handlers.offset = XtOffsetOf(php_exit_call_object, std);
    exit_call_handlers.free_obj = (zend_object_free_obj_t) php_exit_call_free_storage;

    return intern;
}
zend_object* php_exit_call_create(zend_class_entry *ce)
{
    php_exit_call_object* intern = exit_call_new(ce);
    return &intern->std;
}
#else
static php_exit_call_object* exit_call_new(zend_class_entry *ce TSRMLS_DC)
{
    php_exit_call_object *currentExitCall;

    currentExitCall = (php_exit_call_object *)emalloc(sizeof(php_exit_call_object));
    memset(currentExitCall, 0, sizeof(php_exit_call_object));
    zend_object_std_init(&currentExitCall->std, ce TSRMLS_CC);

# if PHP_VERSION_ID < 50400
    zval *tmp;
    zend_hash_copy(currentExitCall->std.properties, &ce->default_properties, (copy_ctor_func_t) zval_add_ref, (void *) &tmp, sizeof(zval *));
# else
    object_properties_init(&(currentExitCall->std), ce);
# endif

    return currentExitCall;
}
static zend_object_value exit_call_store(php_exit_call_object* currentExitCall TSRMLS_DC)
{
    zend_object_value retval;
    retval.handle = zend_objects_store_put(currentExitCall,
                                           (zend_objects_store_dtor_t)zend_objects_destroy_object,
                                           (zend_objects_free_object_storage_t)php_exit_call_free_storage,
                                           NULL TSRMLS_CC);
    retval.handlers = zend_get_std_object_handlers();

    return retval;
}

zend_object_value php_exit_call_create(zend_class_entry *ce TSRMLS_DC)
{
    php_exit_call_object* intern = exit_call_new(ce TSRMLS_CC);
    return exit_call_store(intern TSRMLS_CC);
}
#endif

#if PHP_VERSION_ID >= 70000
void php_exit_call_free_storage(zend_object* obj)
{
    php_exit_call_object* exitCall = php_exit_call_fetch_object(obj);
#else
void php_exit_call_free_storage(php_exit_call_object* exitCall TSRMLS_DC)
{
#endif
    zend_object_std_dtor(&exitCall->std TSRMLS_CC);
    if (!exitCall->inner->isEnded()) {
        LOG4CXX_DEBUG(AG(G)->log_agent, "Destroying ADExitCall call that was not ended properly");
    }
    delete exitCall->inner;
    efree(exitCall);
}

static bool checkTxNameIsValid(const std::string& txName)
{
    return txName.find_first_of(DISALLOWED_CHARS_IN_TX_NAME) == std::string::npos;
}

static bool APIstartTransaction(const char* transactionName,
                                const int entryPointArg,
                                bool isContinuingTransaction = false,
                                const std::string& correlationHeaderString = "")
{
    DisableSignalsScope noSignals;

    if (AG(is_disabled) || !AG(did_lazy_init)) {
        zend_error(E_WARNING, "agent not initialized, ignoring appdynamics_start_transaction API call.");
        return false;
    }

    AG(G)->configChannel->requestUpdate();
    AG(G)->configChannel->checkForUpdate(&agent_globals, AG(first_config_timeout));
    if (!AG(G)->configChannel->isInitialized()) {
        LOG4CXX_DEBUG(AG(G)->log_agent, "agent not initialized, ignoring appdynamics_start_transaction API call.");
        return false;
    }

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();

    boost::shared_ptr<RequestContext> requestContext = AG(G)->request_context;
    if (!requestContext) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "agent request context is invalid.");
        LOG4CXX_DEBUG(AG(G)->log_agent, "aborting appdynamics transaction API call, agent request context is invalid.");
        return false;
    }

    if (!checkTxNameIsValid(transactionName)) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "transaction name contains one of the following invalid characters: %s",
                   DISALLOWED_CHARS_IN_TX_NAME);
        LOG4CXX_DEBUG(AG(G)->log_agent, "aborting appdynamics transaction API call, transaction name contains invalid characters: " << transactionName);
        return false;
    }

    // Don't proceed (aka "freeze the name") if the correlation has already
    // been performed in some exit call.
    if (requestContext->isCorrelationSent()) {
        LOG4CXX_DEBUG(AG(G)->log_agent, "correlation already sent, ignoring API call for transaction: " << transactionName);
        // TODO: evaluate proper bool value
        return false;
    }

    if (!appdynamics::pb::Agent::EntryPointType_IsValid(entryPointArg) ||
        entryPointArg > static_cast<int>(appdynamics::pb::Agent::EntryPointType::PHP_WEB_SERVICE)) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "entrypoint type specified as second argument is invalid.");
        LOG4CXX_DEBUG(AG(G)->log_agent, "aborting appdynamics transaction API call, "
                                        "entrypoint type specified as second argument is invalid: " << entryPointArg);
        return false;
    }
    appdynamics::pb::Agent::EntryPointType entryPointType(static_cast<appdynamics::pb::Agent::EntryPointType>(entryPointArg));

    if (txMonitor->isTransactionExcluded(transactionName, entryPointType)) {
        zend_error(E_WARNING,
                   "Not reporting AppDynamics transaction, "
                   "transaction exclude rule matched.");
        LOG4CXX_INFO(AG(G)->log_agent, "not reporting AppDynamics transaction, "
                                       "transaction exclude rule matched.");
        return false;
    }

    const AEntryPointDelegate* const delegate =
        txMonitor->getEntryPointDelegate(entryPointType);

    // If delegate not found, bail.
    if (!delegate) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "cannot find delegate for the entry point type.");
        return false;
    }

    requestContext->clearRequestGUID();
    requestContext->clearErrorStatus();

    // Don't proceed if we already sent response headers with EUM information.
    // The EUM information in the response headers contains the current BT ID.  If
    // allow the code below to proceed, then we will have sent one BT ID to the
    // javascript agent and different BT ID to the controller.
    const EUM::Context& eumContext = requestContext->eumContext();
    if (eumContext.headersSent()) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "already sent EUM HTTP response headers.");
        LOG4CXX_DEBUG(AG(G)->log_agent, "already sent EUM HTTP response headers can't change BT ["  \
                                << txMonitor->getTransactionContext()->getTransactionName()
                                << "], ignoring API call for transaction: " << transactionName);
        return false;
    }

    bool haveExistingTransaction = txMonitor->getTransactionContext() != NULL;

    txMonitor->getTransactionReporter()->setBTInfoRepsonseTimeout(AG(bt_info_response_timeout));
    txMonitor->getSnapshotManager()->setCallGraphEnabled(!AG(G)->dit_flags.disableCallGraph);

    txMonitor->getSnapshotManager()->startSnapshot(&(AG(G)->exec_env->getAgentGlobals().callGraphCollectionState), false);

    // Start the transaction timing now (instead of from the start of the request).
    TimePoint const startTime(requestContext->getTimer(), requestContext->getConfigChannel()->getTimestampSkew());
    if (isContinuingTransaction) {
        TransactionCorrelator::processContinuingTransaction(correlationHeaderString,
                                                            entryPointType,
                                                            startTime,
                                                            AG(G)->log_agent);
    }
    else {
        boost::shared_ptr<ATransactionAcceptor> acceptor(boost::static_pointer_cast<ATransactionAcceptor>(
                delegate->getTransactionAcceptor()));

        // Always treat transaction as discovered.
        boost::shared_ptr<AgentTransaction> agentTx(acceptor->getTransaction(transactionName, NULL));

        txMonitor->addTransaction(agentTx, entryPointType, startTime);
    }

    TransactionContext* const txContext = txMonitor->getTransactionContext();
    if (!txContext) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "failed to create transaction.");
        LOG4CXX_DEBUG(AG(G)->log_agent, "aborting appdynamics transaction API call, "
                                        "failed to create transaction.");
        return false;
    }
    // Clear any errors collected up to this point.
    txContext->getErrorRegistry()->clear();

    txMonitor->getTransactionReporter()->reportTransaction(txContext, AG(G)->request_context, haveExistingTransaction);
    txContext->setOrigin(TransactionContext::API);

    return true;
}

static bool APIendTransaction()
{
    DisableSignalsScope noSignals;

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* txContext = txMonitor->getTransactionContext();

    if (!txContext) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "transaction context invalid.");
        return false;
    }

    if (txContext->getOrigin() != TransactionContext::API) {
        zend_error(E_WARNING,
                   "Aborting appdynamics transaction API call, "
                   "cannot end transaction with API unless also started with API.");
        return false;
    }

    AG(G)->callGraphCollectionState.onTransactionEnd(AG(G)->timer.getTimestamp());

    txMonitor->onRequestEnd(AG(G)->exec_env.get(), AG(G)->request_context, false);

    return true;
}

PHP_FUNCTION(appdynamics_start_transaction)
{
    char* transactionName = NULL;
#if PHP_VERSION_ID >= 70000
    size_t transactionNameLen = 0;
    zend_long entryPointType = -1;
#else
    int transactionNameLen = 0;
    int entryPointType = -1;
#endif

#if PHP_VERSION_ID >= 70200
    ZEND_PARSE_PARAMETERS_START(2, 2)
    Z_PARAM_STRING(transactionName,transactionNameLen)
    Z_PARAM_LONG(entryPointType)
    ZEND_PARSE_PARAMETERS_END();
#else
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sl",
                                &transactionName,
                                &transactionNameLen,
                                &entryPointType
                              ) == FAILURE) {
        return;
    }
#endif
    RETURN_BOOL(APIstartTransaction(transactionName, entryPointType));
}

PHP_FUNCTION(appdynamics_continue_transaction)
{
    char* correlationHeaderString = NULL;
#if PHP_VERSION_ID >= 70000
    size_t correlationHeaderStringLen = 0;
#else
    int correlationHeaderStringLen = 0;
#endif
    int entryPointType;
    if (AG(is_cli_sapi)) {
        entryPointType = static_cast<int>(appdynamics::pb::Agent::EntryPointType::PHP_CLI);
    } else {
        entryPointType = static_cast<int>(appdynamics::pb::Agent::EntryPointType::PHP_WEB);
    }

#if PHP_VERSION_ID >= 70200
    zend_long entryPointLongType=entryPointType;
    ZEND_PARSE_PARAMETERS_START(1, 2)
    Z_PARAM_STRING(correlationHeaderString,correlationHeaderStringLen)
    Z_PARAM_OPTIONAL
    Z_PARAM_LONG(entryPointLongType)
    ZEND_PARSE_PARAMETERS_END();
    entryPointType=entryPointLongType;
#else
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s|l",
                                &correlationHeaderString,
                                &correlationHeaderStringLen,
                                &entryPointType
                              ) == FAILURE) {
        return;
    }
#endif

    RETURN_BOOL(APIstartTransaction("", entryPointType, true, correlationHeaderString));
}

PHP_FUNCTION(appdynamics_end_transaction)
{
    RETURN_BOOL(APIendTransaction());
}

#if 0
PHP_FUNCTION(appdynamics__get_handle_registry_size)
{
    LOG4CXX_INFO(AG(G)->log_agent, "handle registry size: " << AG(G)->exec_env->getHandleRegistry().size());
    RETURN_LONG(AG(G)->exec_env->getHandleRegistry().size());
}
#endif

PHP_FUNCTION(appdynamics_begin_exit_call)
{
#if PHP_VERSION_ID >= 70000
    zend_long type;
    size_t labelSize = 0;
#else
    int type;
    int labelSize = 0;
#endif
    zval* properties = NULL;
    zend_bool exclusive = 1;

#if PHP_VERSION_ID >= 70200
    char* label = NULL;
    ZEND_PARSE_PARAMETERS_START(3, 4)
    Z_PARAM_LONG(type)
    Z_PARAM_STRING(label,labelSize)
    Z_PARAM_ARRAY(properties) 
    Z_PARAM_OPTIONAL
    Z_PARAM_BOOL(exclusive)
    ZEND_PARSE_PARAMETERS_END();
#else
    const char* label = NULL;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "lsa|b",
                              &type, &label, &labelSize, &properties, &exclusive) == FAILURE)
        return;
#endif
    if (AG(is_disabled) || !AG(did_lazy_init)) {
        zend_error(E_WARNING, "Not starting exit call, "
                              "agent is disabled or not initialized.");
        return;
    }

    // TODO: check for empty label

    if (!appdynamics::pb::Agent::ExitPointType_IsValid(type)) {
        zend_error(E_WARNING, "Invalid exit call type");
        return;
    }
    appdynamics::pb::Agent::ExitPointType const exitType =
        static_cast<appdynamics::pb::Agent::ExitPointType>(type);
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    const AExitPointDelegate* const delegate = txMonitor->getExitPointDelegate(exitType);
    if (!delegate) {
        LOG4CXX_DEBUG(AG(G)->log_agent, "Not starting exit call, no delegate for specified exit point type "
                << appdynamics::pb::Agent::ExitPointType_Name(exitType));
        zend_error(E_WARNING, "Unsupported exit call type (missing delegate)");
        return;
    }

    if (delegate->isDetectionDisabled()) {
        LOG4CXX_DEBUG(AG(G)->log_agent, "Not starting exit call, exit call type "
                                        << appdynamics::pb::Agent::ExitPointType_Name(exitType)
                                        << "is disabled.");
        return;
    }

    ZValPointerArray propertiesArray(ZValPointerAny::share(properties).cast<ZValPointerArray>());
    if (propertiesArray.size() == 0) {
        zend_error(E_WARNING, "Identifying properties array is empty");
        return;
    }

    // check for valid context (txContext, etc)

    TransactionContext* const txContext = txMonitor->getTransactionContext();
    if (txContext == NULL) {
        if (AG(G)->configChannel->isInitialized()) {
            LOG4CXX_DEBUG(AG(G)->log_agent, "Not starting exit call outside business transaction context");
        }
        return;
    }

    if (exclusive && txContext->getCurrentExitCall()) {
        LOG4CXX_INFO(AG(G)->log_agent, "Not starting exit call because one is already in progress");
        return;
    }

    txContext->incrementExitCallCounter();
    APICurrentExitCall* const currentExitCall =
            new APICurrentExitCall(txContext->getExitCallCounter(), exclusive, AG(G)->log_agent);

    // "resolve" the exit call
    // no property validation is done
    ExitCallInfo exitCallInfo(exitType, true);
    exitCallInfo.setDisplayName(label);
    ZValPointerArray::const_iterator const propertiesEnd = propertiesArray.end();
    for (ZValPointerArray::const_iterator it = propertiesArray.begin(); it != propertiesEnd; ++it) {
        if ((*it).keyIsString()) {
            ZValPointerString propertyValue((*it).getValue().cast<ZValPointerString>());
            if (propertyValue)
                exitCallInfo.addIdentifyingProperty((*it).getKeyAsString(), propertyValue.getStringValue());
        }
    }

    currentExitCall->setExitCallInfo(exitCallInfo);

    ExitCallRegistry* const exitCallRegistry =
        txMonitor->getTransactionRegistry()->getExitCallRegistry();
    exitCallRegistry->resolveExitCall(currentExitCall);

    php_exit_call_object* const exitCallObject = exit_call_new(exit_call_ce TSRMLS_CC);
    exitCallObject->inner = currentExitCall;

#if PHP_VERSION_ID >= 70000
    // exit_call_new() already did what exit_call_store() does under PHP 5.
    RETVAL_OBJ(&exitCallObject->std);
#else
    return_value->value.obj = exit_call_store(exitCallObject TSRMLS_CC);
    return_value->type = IS_OBJECT;
#endif

    if (exclusive)
        txContext->setCurrentExitCall(currentExitCall);

    LOG4CXX_DEBUG(AG(G)->log_agent, "started API exit call of type " << appdynamics::pb::Agent::ExitPointType_Name(exitType));

    currentExitCall->startExitCall();
}

PHP_FUNCTION(appdynamics_end_exit_call)
{
    zval* exitCallWrapper = NULL;
    zval* exception = NULL;
    
#if PHP_VERSION_ID >= 70200
    ZEND_PARSE_PARAMETERS_START(1,2)
    Z_PARAM_OBJECT_OF_CLASS(exitCallWrapper,exit_call_ce)
    Z_PARAM_OPTIONAL
    Z_PARAM_OBJECT_OF_CLASS_EX(exception,zend_exception_get_default(),1,0)
    ZEND_PARSE_PARAMETERS_END();
#else
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "O|O!",
                              &exitCallWrapper, exit_call_ce,
                              &exception, zend_exception_get_default()) == FAILURE)
        return;
#endif

    php_exit_call_object* exitCallObject = static_cast<php_exit_call_object*>(getInternalObjectFromZval(exitCallWrapper TSRMLS_CC));
    if (!exitCallObject) {
        zend_error(E_WARNING, "Invalid ADExitCall object");
        return;
    }

    TransactionContext* const txContext =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
    if (txContext == NULL) {
        if (AG(G)->configChannel->isInitialized()) {
            LOG4CXX_DEBUG(AG(G)->log_agent, "Not ending exit call outside business transaction context");
        }
        return;
    }

    APICurrentExitCall* currentExitCall = exitCallObject->inner;

    if (currentExitCall->isEnded())
        return;

    if (currentExitCall->isExclusive() && currentExitCall != txContext->getCurrentExitCall()) {
        LOG4CXX_INFO(AG(G)->log_agent, "Not ending exit call because it is not the current one for the business transaction");
        return;
    }

    currentExitCall->endExitCall();

    boost::shared_ptr<AErrorObject> errorObject;
    if (exception) {
        currentExitCall->incrementErrorCount(1);
        ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorObject = errorMonitor->reportException(AG(G)->exec_env.get(), exception);
    }
    txContext->reportExitCall(currentExitCall);

    LOG4CXX_DEBUG(AG(G)->log_agent, "ended API exit call of type "
                  << appdynamics::pb::Agent::ExitPointType_Name(currentExitCall->getExitCallInfo()->getExitPointType()));

    if (!txContext->snapshotEnabled())
        return;

    const boost::shared_ptr<SnapshotManager>& snapshotManager =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getSnapshotManager();

    uint64_t timeTakenInMS = currentExitCall->getElapsedTimeInUs() / 1000;

    Snapshot* snapshot = snapshotManager->getSnapshot();
    BOOST_ASSERT(snapshot != NULL); // we should never get here if there is no Snapshot.

    if (!snapshot->isCallGraphEnabled())
        return;

    uint64_t minDurationMS = errorObject ? 0 : snapshotManager->getMinExitCallTime();

    if (timeTakenInMS < minDurationMS)
        return;

    std::string exitCallSequence(TransactionCorrelator::getExitCallSequenceString(txContext,
                                                                                  currentExitCall->getSequenceNumber()));

    boost::shared_ptr<SnapshotExitCall> sec(
            boost::make_shared<SnapshotExitCall>(currentExitCall->getBackendID(),
                                                 timeTakenInMS,
                                                 exitCallSequence));

    // Get SQL Query details if there are any and add them to the SnapshotExitCall
    const std::string* sqlQuery = currentExitCall->getSqlQuery();
    if (sqlQuery) {
        sec->setDetailString(*sqlQuery);
        sec->addProperty("Query Type", getSQLType(*sqlQuery));
        ZValPointerArray paramsArray(currentExitCall->getBoundParamsArray().get().cast<ZValPointerArray>());
        if (paramsArray.size() == 0) {
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       *sqlQuery,
                                       timeTakenInMS);
            sec->addProperty("Statement Type", kSqlString);
        } else {
            appdynamics::pb::SnapshotExitCall* dbCall(snapshot->getDBCalls().addWithBoundParams(txContext,
                                                                                                currentExitCall,
                                                                                                *sqlQuery,
                                                                                                timeTakenInMS));
            if (dbCall) {
                appdynamics::pb::BoundParameters* const boundParamsProtobuf =
                    dbCall->mutable_boundparameters();
                boundParamsProtobuf->set_type(appdynamics::pb::BoundParameters::POSITIONAL);
                for (auto it = paramsArray.begin(); it != paramsArray.end(); ++it) {
                    if ((*it).getValue().getType() == ZValPointer::ZVal_String)
                        boundParamsProtobuf->add_posparameters((*it).getValue().cast<ZValPointerString>().getStringValue());
                }
                sec->setBoundParams(&(dbCall->boundparameters()));
                sec->addProperty("Statement Type", kSqlPreparedStatement);
            }
        }
    }

    if (errorObject)
        sec->setErrorDetails(errorObject->getSummary());

    AG(G)->callGraphCollectionState.addSnapshotExitCall(sec);

    if (currentExitCall->isExclusive() && txContext->getCurrentExitCall() == currentExitCall)
        txContext->setCurrentExitCall(NULL);

}

PHP_METHOD(ADExitCall, __construct)
{
    // dummy
}

PHP_METHOD(ADExitCall, getCorrelationHeader)
{
#if PHP_VERSION_ID >= 70200
    ZEND_PARSE_PARAMETERS_START(0, 0)
    ZEND_PARSE_PARAMETERS_END();
#else
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE)
        return;
#endif
    php_exit_call_object* exitCallObject = static_cast<php_exit_call_object*>(getInternalObjectFromZval(getThis() TSRMLS_CC));
    if (!exitCallObject) {
        zend_error(E_WARNING, "Invalid ADExitCall object");
        return;
    }

    TransactionContext* const txContext =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
    if (txContext == NULL) {
        zend_error(E_WARNING, "No business transaction context found");
        return;
    }
    const boost::shared_ptr<RequestContext>& requestContext = txContext->getRequestContext();
    TransactionReporter* txReporter =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionReporter();

    if (!txReporter->receiveTransactionInfo(requestContext, false)) {
        // If we timed out, we expect the response field of the request context to still be null.
        BOOST_ASSERT(!(requestContext->getBTInfoResponse()));
    }

    APICurrentExitCall* currentExitCall = exitCallObject->inner;
    if (!currentExitCall->hasComponentID()) {
        currentExitCall->disablePropagation();
    }

    std::string header(TransactionCorrelator::getCorrelationHeader(txContext,
                                                                   currentExitCall,
                                                                   AG(G)->log_agent));
#if PHP_VERSION_ID >= 70000
    RETURN_STRINGL(const_cast<char*>(header.c_str()), header.size());
#else
    RETURN_STRINGL(const_cast<char*>(header.c_str()), header.size(), 1);
#endif
}

PHP_METHOD(ADExitCall, setSQLQueryInfo)
{
#if PHP_VERSION_ID >= 70000
    size_t querysize = 0;
#else
    int querysize = 0;
#endif
    zval* bparray = NULL;
    
#if PHP_VERSION_ID >= 70200
    char* query = NULL;
    ZEND_PARSE_PARAMETERS_START(1,2)
    Z_PARAM_STRING(query,querysize)
    Z_PARAM_OPTIONAL
    Z_PARAM_ARRAY(bparray)
    ZEND_PARSE_PARAMETERS_END();
#else
    const char* query = NULL;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s|a",
                              &query, &querysize, &bparray) == FAILURE)
        return;
#endif
    php_exit_call_object* exitCallObject = static_cast<php_exit_call_object*>(getInternalObjectFromZval(getThis() TSRMLS_CC));
    if (!exitCallObject) {
        zend_error(E_WARNING, "Invalid ADExitCall object");
        return;
    }

    APICurrentExitCall* currentExitCall = exitCallObject->inner;
    if (currentExitCall->getExitCallInfo()->getExitPointType() != appdynamics::pb::Agent::ExitPointType::EXIT_DB){
        zend_error(E_WARNING, "Trying to add DB Query to Exit Call that is not of type AD_EXIT_DB");
        return;
    }

    if (bparray)
        currentExitCall->setBoundParamsArray(ZValPointerAny::share(bparray));

    if (querysize)
        currentExitCall->setSqlQuery(query);
}
