#include "util.h"
#include <sstream>
#include <boost/foreach.hpp>
extern "C" {
#include "php.h"
}

// Generate loggable representation of a string map:
//  { key1 = "value1", key2 = "value2", key3 = "value3" }
std::string stringMapToPrintableString(const StringMap& map)
{
    std::stringstream ss;
    bool first = true;

    ss << "{ ";
    BOOST_FOREACH(auto entry, map) {
        if (!first)
            ss << ", ";
        ss << entry.first << "=\"" << entry.second << "\"";
        first = false;
    }
    ss << " }" << std::endl;
    return ss.str();
}


/**
 * Takes an input of the form /a/b/c/d/foo.php and returns
 * a pointer to one-level directory and basefile name
 * (d/foo.php) in the same string.
 */
const char *agent_get_base_filename(const char *filename) {
    const char *ptr;
    int   found = 0;

    if (!filename)
        return "";

    /* reverse search for "/" and return a ptr to the next char */
    for (ptr = filename + strlen(filename) - 1; ptr >= filename; ptr--) {
        if (*ptr == '/') {
            found++;
        }
        if (found == 2) {
            return ptr + 1;
        }
    }

    /* no "/" char found, so return the whole string */
    return filename;
}

char *agent_sprintf(int min_size, const char* fmt, ...)
{
    char   *new_str;
    int     size = min_size;
    va_list args;

    if (size < 1) {
        size = 1;
    }

    new_str = (char *) emalloc(size);

    for (;;) {
        int n;

        va_start(args, fmt);
        n = vsnprintf(new_str, size, fmt, args);
        va_end(args);

        if (n > -1 && n < size) {
            break;
        }
        if (n < 0) {
            size *= 2;
        } else {
            size = n + 1;
        }
        new_str = (char *) erealloc(new_str, size);
    }

    return new_str;
}

