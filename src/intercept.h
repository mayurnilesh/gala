/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __INTERCEPT_H
#define __INTERCEPT_H

#include <boost/function_types/result_type.hpp>
#include <boost/functional/hash.hpp>
#include <boost/intrusive/set.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>
#include <memory>
#include <set>

extern "C" {
#include <php.h>
#include "Zend/zend_compile.h"
#include "Zend/zend_list.h"
}

#include "common.h"
#include "handle_registry.h"
#include "agent_logger.h"
#include "config_channel.h"

/*
 * In case we are building against PHP version < 5.3.6.
 */
#ifndef DEBUG_BACKTRACE_IGNORE_ARGS
#define DEBUG_BACKTRACE_IGNORE_ARGS    (1<<1)
#endif

class CXXGlobals;
class ZValPointerAny;
struct _zend_fcall_info_cache;
typedef _zend_fcall_info_cache zend_fcall_info_cache;

struct _zend_fcall_info;
typedef _zend_fcall_info zend_fcall_info;

template <class T>
class EMallocAllocator;

class AMethodInterceptor;
class HTTPPayload;
class CompiledScriptInterceptor;
class InterceptionEngine;
class ConfigChannel;

namespace CallGraph
{
    class ExecStackFrame;
}

/**
    Method name for constructor's of PHP classes.
 */
extern const char PHP_CONSTRUCT_METHOD[];

/**
    Method name for the root php file.
 */
extern const char ROOT_SYMBOL[];

/**
    Helper function that uses the zend_str_tolower_copy to lower
    case the contents of a std::string.
    @param s String to lower case with zend_str_tolower_copy.
    @return Lower case version of the specified std::string.
 */
inline std::string zend_str_tolower(const std::string& s)
{
    size_t l = s.length();
    char* buffer = reinterpret_cast<char*>(alloca(l + 1));
    zend_str_tolower_copy(buffer, s.c_str(), l);
    return std::string(buffer, l);
}

/*
 * For now, map class/func name directly to a ordered set of
 * AMethodInterceptors. In the future, we may need to wrap those in
 * InterceptionInfo, to keep track of matched rules and more.
 */

/* {{{ CallableInfo */
/*
 * NOTE Potentially optimize by using const char*, since that's how class/func names
 * are stored in the engine.
 */
class CallableInfo {
    public:
        friend struct boost::hash<CallableInfo>;
        friend struct boost::hash< std::pair<CallableInfo, uint32_t> >;
        CallableInfo(const std::string& func) : func_name(func) { }
        CallableInfo(const std::string& klass, const std::string& func) : class_name(klass), func_name(func) { }
        CallableInfo(const char* func) {
            BOOST_ASSERT(func);
            func_name = func;
        }
        CallableInfo(const char* klass, const char* func) {
            BOOST_ASSERT(func);
            func_name = func;
            if (klass) {
                class_name = klass;
            }
        }
        inline const std::string& getClass() const { return class_name; }
        inline const std::string& getFunc() const { return func_name; }

    private:
        std::string class_name;
        std::string func_name;
};

inline bool operator==(const CallableInfo& lhs, const CallableInfo& rhs)
{
    if (lhs.getClass() != rhs.getClass()) {
        return false;
    }
    if (lhs.getFunc() != rhs.getFunc()) {
        return false;
    }
    return true;
}

inline bool operator<(const CallableInfo& lhs, const CallableInfo& rhs)
{
    if (lhs.getClass() != rhs.getClass()) {
        return lhs.getClass() < rhs.getClass();
    } else {
        return lhs.getFunc() < rhs.getFunc();
    }
}

namespace boost {
    /* Hash functor for CallableInfo */
    template<> struct hash<CallableInfo> {
        size_t operator()(const CallableInfo& c) const {
            size_t seed = 0;
            boost::hash_combine(seed, c.class_name);
            boost::hash_combine(seed, c.func_name);
            return seed;
        }
    };

    /* Hash functor for pair<CallableInfo, uint32_t> */
    template<> struct hash< std::pair<CallableInfo, uint32_t> > {
        size_t operator()(const std::pair<CallableInfo, uint32_t>& p) const {
            size_t seed = 0;
            boost::hash_combine(seed, p.first.class_name);
            boost::hash_combine(seed, p.first.func_name);
            boost::hash_combine(seed, p.second);
            return seed;
        }
    };
}

std::ostream& operator<<(std::ostream& out, const CallableInfo& callable);
/* }}} */

/* {{{ Execution environment classes */

class PHPExecEnvironment : boost::noncopyable
{
    public:
        enum AccessLevel
        {
            PUBLIC = ZEND_ACC_PUBLIC,
            PROTECTED = ZEND_ACC_PROTECTED,
            PRIVATE = ZEND_ACC_PRIVATE
        };

        /**
            Constructor saves current exception state from php execution state,
            then clears the exception state in the php execution state.
            Destructor clears exception state in the php execution state, and restores
            the exception state saved in the constructor.
         */
        class ExceptionState : boost::noncopyable
        {
        public:
            ExceptionState(const void* threadSafetyContext);
            ~ExceptionState();

        private:
            const void* const m_threadSafetyContext;
            typedef boost::mpl::if_c< PHP_VERSION_ID >= 70000, zend_object*, zval*>::type t_ExceptionType;
            t_ExceptionType m_exception;
            t_ExceptionType m_prevException;
            zend_op* m_savedOpline;
            zend_op* m_savedBeforeExceptionOpLine;
        };

        /**
            Constructor updates arg count and pushes arguments onto php vm stack
            from current php execution state. Destructor removes these arguments
            and resets the arg count.

            ************************** IMPORTANT *******************************
            * The constructor is to be invoked ONLY in onCallableBegin, and    *
            * the destructor in onCallableEnd.                                 *
            ********************************************************************
         */
#if PHP_VERSION_ID >= 70300
#error "Check how stack works for PHP >= 7.3, ArgStackState may not work."
/******************************************
 5.2 stack:
 ______
 |_____| <- 'EG(argument_stack)->top'
 |_NULL|
 |__3__| <- this is the argcount
 |__a__| <- 3rd argument
 |__b__| <- 2nd argument
 |__c__| <- 1st argument

*******************************************/
#elif PHP_VERSION_ID >= 50300
        class ArgStackState : boost::noncopyable
        {
        public:
            ArgStackState(const std::vector<ZValPointerAny>& args,
                          zend_execute_data* executeData);
            ~ArgStackState();
        private:
            const std::vector<ZValPointerAny> m_args;
            // TODO: now that constness is gone, I wonder if it's really
            // necessary to extend the stack frame the way we do it in the
            // constructor of ArgStackState. We even had to get zend_execute.h
            // into our fixed headers and declare a method that was only in the
            // scope of zend_execute.cpp (zend_vm_stack_extend_call_frame()).
            zend_execute_data* m_executeData;
        };
#endif
        ~PHPExecEnvironment() { }

        static inline boost::shared_ptr<PHPExecEnvironment> create(bool hookResourceDestructors, bool hookObjectDestructors TSRMLS_DC)
        {
            return boost::make_shared<PHPExecEnvironment>(hookResourceDestructors, hookObjectDestructors TSRMLS_CC);
        }

        inline void setExecuteDataOnCallableBegin(zend_execute_data* edata,
                                                  zend_fcall_info* fcallInfo,
                                                  CallGraph::ExecStackFrame* execStackFrame,
                                                  zend_op_array* op,
                                                  bool internal)
        {
            setExecuteData(false, edata, fcallInfo, execStackFrame, op, internal);
        }

        inline void setExecuteDataOnCallableEnd(zend_execute_data* edata,
                                                zend_fcall_info* fcallInfo,
                                                CallGraph::ExecStackFrame* execStackFrame,
                                                zend_op_array* op,
                                                bool internal)
        {
            setExecuteData(true, edata, fcallInfo, execStackFrame, op, internal);
        }

        inline void setExecuteDataOnCallableEnd(zend_execute_data* edata,
                                                zend_fcall_info* fcallInfo,
                                                CallGraph::ExecStackFrame* execStackFrame,
                                                zend_op_array* op,
                                                bool internal,
                                                zval* returnValue)
        {
            setExecuteData(true, edata, fcallInfo, execStackFrame, op, internal);
            m_cachedReturnValue = returnValue;
            m_returnValueComputed = true;
        }

        inline void clearExecuteData()
        {
            setExecuteData(false, NULL, NULL, NULL, NULL, false);
        }

        zval* getInvokedObject(unsigned numCallFramesUp) const;
        zval* getInvokedObject() const;
        zval* getReturnValue(bool isDataCollectorConfigured = false) const;
        zval* getExceptionObject() const;
        bool doesExceptionObjectExist() const;

        /**
            @return NULL if the current php function is not a method of a class,
            otherwise the name of the class containing the current php method.
         */
        const char* getClassName() const;

        /**
            @return The name of the current php function.
         */
        const char* getFunctionName() const;

        /**
            Generates a name for the current php function, method, script, etc.
            If the current callable is a method, then the string will be of the form:
            Class::Method.
            @return String containing the name of the current php callable.
         */
        std::string getCallableName() const;

        /**
            Method for accessing the values of parameters passed to the current php
            callable.
            @param t_ZValPointerClass The pointer type to wrap the
            zval for the parameter with.  Usually one of the classes in
            zval_helper.h, but may also be zval*.
            @param runtimeParamCountCheck If true, the function will return a null
            pointer value if the paramIndex is greater than or equal to the number of
            parameters passed to the current php callable otherwise no check against
            the number of parameters is performed in a release builds.
            @param paramIndex Zero-based index into the list of parameters passed to the
            current php callable.
         */
        template <class t_ZValPointerClass=ZValPointerAny, bool runtimeParamCountCheck=false>
        t_ZValPointerClass getParam(unsigned paramIndex) const;

        /**
            @return The number of parameters passed to the current php callable.
         */
        zend_uintptr_t getParamCount() const;

        /**
            Helper method for checking if the current php callable returned false.
         */
        inline bool isReturnValueFalse() const
        {
            zval* returnValue = getReturnValue();
#if PHP_VERSION_ID >= 70000
            if (Z_TYPE_P(returnValue) != IS_TRUE && Z_TYPE_P(returnValue) != IS_FALSE)
#else
            if (Z_TYPE_P(returnValue) != IS_BOOL)
#endif
                return false;
            return !(Z_BVAL_P(returnValue));
        }

        /**
            Helper method for checking if the current php callable returned null.
         */
        inline bool isReturnValueNull() const
        {
            zval* returnValue = getReturnValue();
            if (Z_TYPE_P(returnValue) == IS_NULL)
                return true;
            return false;
        }

        /**
            Helper method for checking if the current php callable returned null or false.
         */
        inline bool isReturnValueNullOrFalse() const
        {
            return isReturnValueFalse() || isReturnValueNull();
        }

        /**
            Helper function for checking if the current php callable returned true.
         */
        inline bool isReturnValueTrue() const
        {
            zval* returnValue = getReturnValue();
#if PHP_VERSION_ID >= 70000
            if (Z_TYPE_P(returnValue) != IS_TRUE && Z_TYPE_P(returnValue) != IS_FALSE)
#else
            if (Z_TYPE_P(returnValue) != IS_BOOL)
#endif
                return false;
            return Z_BVAL_P(returnValue);
        }

        inline HandleRegistry& getHandleRegistry() const { return m_handleRegistry; }

        /**
            Lookup the zend_function* for a function or class method.
            @param callable Name of the function or class method to lookup.  Callers
            should normalize ( lower case ) the class and function name before
            calling this method.
            @return The zend_function* for the specified callable or NULL if
            the specified callable does not yet exist.
         */
        zend_function* lookupCallable(const CallableInfo& callable) const;

        /**
            Lookup the zend_function* for a callable zval.
            @param callableValue zval containing a callable.
            @return The zend_function* for the specified callable or NULL if
            the specified callable does not yet exist.
         */
        zend_function* lookupCallable(const ZValPointerAny& callableValue) const;

        /**
            Look up the zend_class_entry* for a class.  This method will *not*
            cause any classes to be loaded.
            @param className Name of the class to look up.  Callers
            should normalize ( lower case ) the class name before calling this method.
            @param classNameLen The length of the class name in bytes *not* including
            the terminating null.
            @return The zend_class_entry for the specified class or NULL if the
            specified class does not yet exist.
         */
        zend_class_entry* lookupClass(const char* className, size_t classNameLen) const;


        /**
            Look up the zend_function* for a class method.
            @param object The object to use for lookup
            @param methodName Name of the method to look up. Callers
            should normalize ( lower case ) the class name before calling this method.
            @param methodNameLen The length of the method name in bytes *not* including
            the terminating null.
            @return The zend_function* for the specified method or NULL if the
            specified method does not yet exist.
         */
        zend_function* lookupMethod(const ZValPointerObject& object,
                                    const char* methodName,
                                    size_t methodNameLen) const;

        /**
            Look up the zend_function* in the global function table.
            @param funcName Name of the function to look up. Callers
            should normalize ( lower case ) the function name before calling this method.
            @param funcNameLen The length of the function name in bytes *not* including
            the terminating null.
            @return The zend_function* for the specified function or NULL if the
            specified function does not yet exist.
         */
        zend_function* lookupFunction(const char* funcName, size_t funcNameLen) const;

        /**
            Lookup the value of an auto global.
            @param autoGlobalName Name of the auto global to look up.
            @param autoGlobalNameLen Length of the auto global name in bytes *not* including
            the terminating null character.
            @param value Pointer to a ZValPointerAny that will be set to the value of the
            auto global if it exists.  If the auto global does not exist, this parameter
            is ignored.  This parameter may be null.
            @return true if the specified auto global exists, false otherwise.
         */
        bool getAutoGlobalValue(const char* autoGlobalName,
                                size_t autoGlobalNameLen,
                                ZValPointerAny* value) const;

        /**
            Lookup the value of a constant long.
            @param constantName Name of the constant to lookup.
            @param constantNameLen Length of the name of the constant in bytes, *not* including
            the terminating null character.
            @param value Pointer to a long that will be set to the value of the constant.
            @return true if the specified constant exists and its value is a long, false
            otherwise.

         */
        bool getConstantAsLong(const char* constantName,
                               size_t constantNameLen,
                               long* value) const;

        /**
            Lookup the value of a constant.
            @param constantName Name of the constant to lookup.
            @param constantNameLen Length of the name of the constant in bytes, *not* including
            the terminating null character.
            @param value Pointer to a zval that will be set to the value of the constant.
            If this function is successful, the zval is completely overwritten.  The zval
            should be initialized to an IS_NULL zval before calling this function.  If this
            function returns true then the caller of this function owns a reference
            to the zval returned via this parameter.
            @return true if the specified constant exists, false otherwise.
            otherwise.
         */
        bool getConstant(const char* constantName,
                         size_t constantNameLen,
                         zval* value) const;

        /**
            Compute a name for a given callable zval.
            @param callableValue ZValPointerAny that refers to a value that is callable.
            @param name Out parameter that receives the name of the callable.
            @return true if the specified zval was a callable and a name could be computed,
            false otherwise.
         */
        bool getCallableName(const ZValPointerAny& callableValue, std::string* name) const;

        /**
            Get's a reference to the agent's "global" state.  If the agent
            state really was completely global, we could just use a global variable.
            However when the php engine is compiled in "thread safe" mode most if
            not all "global" state should be thread local.
         */
        CXXGlobals& getAgentGlobals() const;

        boost::shared_ptr<IConfigChannel>& getConfigChannel() const;

        /**
         * Returns a shared pointer to a payload object that encapsulates data
         * from the current HTTP request, such as headers, cookies, request
         * params, etc.
         */
        const boost::shared_ptr<HTTPPayload>& getHTTPPayload() const;

        /**
            @return the default exception class entry.
         */
        zend_class_entry* getDefaultExceptionClassEntry() const;

        /**
            Get the class entry for a specified zval that refers to an object.
            @param zval Value that references an object
            @return the zend_class_entry* of the class of the object
            referenced by the specified zval or null if the specified zval
            does not refer to a zval.
         */
        zend_class_entry* getObjectValueClassEntry(zval* object) const;

        /**
            Determines if an ancestor descendant relationship exists between
            two specified zend_class_entry's.
            @param ancestorClass The class to treat as the ancestor class.
            @param derivedClass The class to treat as the derived class.
            @return true if the specified ancestor class is an ancestor
            of the specified derived class.
         */
        bool isAncestorClass(zend_class_entry* ancestorClass,
                             zend_class_entry* derivedClass) const;

        /**
            Get's the value of a property named property from an object.
            Note: It's not recommended to use this method directly. Instead, use
            ZValPointerObject's property look-up methods.
            @param object zval referring to the object containing the property whose
                          value should be returned.
            @param accessLevel Access level of the property to lookup.
            @param name Name of the property to lookup.
            @param nameLenInBytes Length of the property name not including the terminating
                                  null.
            @param scope Scope for the property look-up (only makes sense for private properties)
            @return A zval for the value of the specified property on the specified object,
            null if object is not really an object or if it does not have the specified
            property.
         */
        zval* getPropertyByName(zval* object,
                                AccessLevel accessLevel,
                                const char* name,
                                size_t nameLenInBytes,
                                const zend_class_entry* scope) const;

        /**
            Add a named property to an object with value 'value'.
            Note: It's not recommended to use this method directly. Instead, use
            ZValPointerObject's property look-up methods.
            @param object zval referring to the object containing the property whose
                          value should be returned.
            @param accessLevel Access level of the property to lookup.
            @param name Name of the property to add.
            @param nameLenInBytes Length of the property name not including the terminating
                                  null.
            @param scope Scope for the property look-up (only makes sense for private properties)
            @param value a zval to be set as the value of the property
            @return bool, true if write succeeds else false
            Note: If property alread exists write will fail
         */
        bool  writePropertyByName(zval* object,
                                  AccessLevel accessLevel,
                                  const char* name,
                                  size_t nameLenInBytes,
                                  const zend_class_entry* scope,
                                  const ZValPointerAny& value) const;

        zval* readPropertyByName(zval* object,
                                 const char* name,
                                 size_t nameLenInBytes) const;

        /**
            Gets the name of the php class for a php object.
            @param object A object zval.
            @return the name of the class which specified object is an instance of or
            the empty string if the specified value is not an object.
         */
        std::string getClassName(zval* object) const;

        /*************************************************************
         | "There be dragons." - Dave Roth.                          |
         |                                                           |
         | BIG SCARY COMMENT: THIS THING CAN RUN USER CODE.          |
         |                                                           |
         | PROCEED WITH CAUTION.                                     |
         |                                                           |
         | Only do this if the application is going to convert this  |
         | zval to a string anyway.                                  |
         |                                                           |
         | Other ZValPointers of other types, convertToString() may  |
         | invalidate the zval type (e.g. Long, Object, anything     |
         | that's not Any or String.                                 |
         *************************************************************/
        ZValPointerString convertToString(zval* value) const;

        /**
         * Captures and returns the current stack trace.
         * Note: uses PHP's zend_fetch_debug_backtrace(), so the arguments are
         * the same as to that function.
         */
        ZValPointerAny getStackTrace(int skipTopTraces, int options) const;

        ZValPointerAny getSymbolFromSymbolTable(const std::string& symbolName) const;

        /**
            Call a php callable, passing parameters if specified.
            @param returnValue Pointer to a ZValPointerAny that will be filled
            in with a reference to a zval used to hold the return value of the callable.
            If this function returns false, the parameter was ignored.
            @param callable A zval that refers to a php value that can be called as a
            function.
            @param params A vector of parameters to pass to the callable, or NULL.
            @return true if the function was successfully called, false otherwise.
         */
        bool callCallable(ZValPointerAny* returnValue,
                          const ZValPointerAny& callable,
                          const std::vector<ZValPointerAny>* params) const;

        bool callInstanceMethod(ZValPointerAny* returnValue,
                                zval* object,
                                const char* methodName,
                                size_t methodNameLen,
                                const std::vector<ZValPointerAny>* params) const;
        /**
            Saves the current exception state in the returned object and
            clears the exception state.
            @return A ExceptionState object whose destructor will restore
            the exception state to what it was immediately before this method was called.
         */
        unique_ptr<ExceptionState>::type saveExceptionState() const;

#if PHP_VERSION_ID >= 50300
        /**
            Saves the argument stack state in the returned object.
            @return A ExceptionState object whose destructor will restore
            the argument stack state to what it was immediately before this
            method was called.
         */
        unique_ptr<ArgStackState>::type pushArgs(
                const std::vector<ZValPointerAny>& args) const;
#endif

        /**
            Emit a php error message.
            @param docRef Document reference string
            @param type Type of error to emit, eg: E_ERROR or E_WARNING
            @param message Error message to emit.
         */
        void errorDocRef(const char* docRef, int type, const std::string& message) const;

        /**
            Calls a callable with the specified arguments followed by the
            TSRM parameter.  Use this method if the php function you need to
            call needs the TSRM parameter, but it does not make sense to add a first
            class method to PHPExecEnvironment instead.
         */
        template <typename t_callable, typename...t_Args>
        typename boost::function_types::result_type<t_callable>::type callWithTSRMContext(const t_callable& callable, t_Args... args) const;

        /**
            Accessor for retrieving the interception engine.
         */
        InterceptionEngine* getInterceptionEngine() const;

        /**
            Accessor for retrieving the execute data.
        */
        zend_execute_data* getExecuteData() const;

#if PHP_VERSION_ID < 70000
        zend_object_store_bucket* getObjectStoreBucket(const zend_object_handle& handle) const;
#endif

        bool isCLISAPI() const;

        inline PHPExecEnvironment(bool hookResourceDestructors, bool hookObjectDestructors TSRMLS_DC)
            : m_executeData(NULL)
            , m_fcallInfo(NULL)
            , m_execStackFrame(NULL)
            , m_opArray(NULL)
            , m_isInternal(false)
            , m_isSoapServerHandleFunctionCalled(false)
            , m_isCallableEnd(false)
            , m_returnValueComputed(false)
            , m_cachedReturnValue(NULL)
            , m_logger(getLogger(std::string(LogContext::AGENT) + ".PHPExecEnvironment"))
            , m_handleRegistry(hookResourceDestructors, hookObjectDestructors)
        {
#if PHP_VERSION_ID >= 70000
            m_threadSafetyContext = NULL;
#else
            TSRMLS_SET_CTX(m_threadSafetyContext);
#endif
        }
        void setSaopServerHandleFunctionStarted(){
			m_isSoapServerHandleFunctionCalled=true;
        }
        void setSoapServerHandleFunctionCompleted(){
			m_isSoapServerHandleFunctionCalled=false;
        }
        bool isSoapServerHandleFuncInExecution(void){
			return m_isSoapServerHandleFunctionCalled ;
        }

    private:
        inline void setExecuteData(bool isCallableEnd,
                                   zend_execute_data* edata,
                                   zend_fcall_info* fcallInfo,
                                   CallGraph::ExecStackFrame* execStackFrame,
                                   zend_op_array* op,
                                   bool internal)
        {
            m_isCallableEnd = isCallableEnd;
            m_executeData = edata;
            m_fcallInfo = fcallInfo;
            m_execStackFrame = execStackFrame;
            m_opArray = op;
            m_isInternal = internal;
            m_returnValueComputed = false;
            m_cachedReturnValue = NULL;
        }

        bool populateFunctionCallInfoCache(zend_fcall_info_cache* functionCallInfoCache,
                                           const ZValPointerAny& callableValue) const;

        bool callCallableViaCache(ZValPointerAny* returnValue,
                                  zend_fcall_info_cache& callInfoCache,
                                  const std::vector<ZValPointerAny>* params) const;

        zval* getParamImpl(unsigned paramIndex) const;

        const void* m_threadSafetyContext;

        zend_execute_data* m_executeData;
        zend_fcall_info* m_fcallInfo;
        CallGraph::ExecStackFrame* m_execStackFrame;
        zend_op_array* m_opArray;
        bool m_isInternal;
        bool m_isSoapServerHandleFunctionCalled ;

        bool m_isCallableEnd : 1;
        mutable bool m_returnValueComputed : 1;

        mutable zval* m_cachedReturnValue;

        mutable boost::shared_ptr<HTTPPayload> m_httpPayload;


        AgentLogger m_logger;

        mutable HandleRegistry m_handleRegistry;

#if PHP_VERSION_ID >= 70000
        mutable unique_ptr<zval>::type m_exceptionZval;
#endif
};

template <>
inline zval* PHPExecEnvironment::getParam<zval*, true>(unsigned paramIndex) const
{
    if (getParamCount() <= paramIndex)
        return NULL;
    return getParamImpl(paramIndex);
}

template <>
inline zval* PHPExecEnvironment::getParam<zval*, false>(unsigned paramIndex) const
{
    BOOST_ASSERT(getParamCount() > paramIndex);
    return getParamImpl(paramIndex);
}

inline unique_ptr<PHPExecEnvironment::ExceptionState>::type PHPExecEnvironment::saveExceptionState() const
{
    return unique_ptr<ExceptionState>::type(new ExceptionState(m_threadSafetyContext));
}

#if PHP_VERSION_ID >= 50300
inline unique_ptr<PHPExecEnvironment::ArgStackState>::type
PHPExecEnvironment::pushArgs(const std::vector<ZValPointerAny>& args) const
{
    return unique_ptr<ArgStackState>::type(
                new ArgStackState(args, m_executeData)
                );
}
#endif

template <typename t_callable, typename...t_Args>
inline typename boost::function_types::result_type<t_callable>::type PHPExecEnvironment::callWithTSRMContext(const t_callable& callable, t_Args... args) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return callable(args... TSRMLS_CC);
}

/* }}} */

/* {{{ InterceptorRegistry */
class InterceptorRegistry
{
    public:
        enum ID
        {
            CompiledScriptInterceptor_ID,
            CallUserFuncArrayInterceptor_ID,

            HTTPEntryPointInterceptor_ID,
            CLIEntryPointInterceptor_ID,

            Symfony1EntryPointInterceptor_ID,
            Symfony2GetArgumentsInterceptor_ID,
            Symfony2HandlerInterceptor_ID,
            Zend1EntryPointInterceptor_ID,
            Zend1RedirectInterceptor_ID,
            Zend1HttpClientRequestInterceptor_ID,
            Zend2ActionEntryPointInterceptor_ID,
            Zend2RestfulEntryPointInterceptor_ID,
            Zend2RestfulHTTPMethodInterceptor_ID,
            Zend2HttpClientRequestInterceptor_ID,
            Zend2HttpDoRequestInterceptor_ID,
            Drupal7MenuExecuteActiveHandlerInterceptor_ID,
            Drupal7CallUserFuncArrayInterceptor_ID,
            Drupal7DrupalPageGetCacheInterceptor_ID,
            Drupal7CacheGetInterceptor_ID,
            Drupal7DrupalPageSetCacheInterceptor_ID,
            Drupal7CacheSetInterceptor_ID,
            Drupal7HTTPRequestInterceptor_ID,
            Drupal7StreamSocketClientInterceptor_ID,
            Drupal7DBTableExistsInterceptor_ID,
            Drupal8PageCacheSetInterceptor_ID,
            Drupal8PageCacheGetInterceptor_ID,
            Wordpress3ApplyFiltersInterceptor_ID,
            CodeIgniterSetRoutingInterceptor_ID,
            CodeIgniterHandlerInterceptor_ID,
            FuelPHPRequestExecuteInterceptor_ID,
            CakePHPControllerInvokeActionInterceptor_ID,

            AMQPConnectionConnectInterceptor_ID,
            AMQPConnectionDisconnectInterceptor_ID,
            AMQPChannelConstructorInterceptor_ID,
            AMQPExchangeConstructorInterceptor_ID,
            AMQPExchangeConfigInterceptor_ID,
            AMQPExchangeWriteInterceptor_ID,

            FOpenExitPointInterceptor_ID,
            FileGetContentsInterceptor_ID,

            CurlInitInterceptor_ID,
            CurlSetoptInterceptor_ID,
            CurlSetoptArrayInterceptor_ID,
            CurlExecInterceptor_ID,
            CurlCloseInterceptor_ID,
            CurlResetInterceptor_ID,
            CurlMultiAddHandleInterceptor_ID,
            CurlMultiRemoveHandleInterceptor_ID,
            CurlMultiExecInterceptor_ID,
            CurlMultiSelectInterceptor_ID,
            CurlMultiInfoReadInterceptor_ID,
            CurlCopyHandleInterceptor_ID,

            PDOConstructorInterceptor_ID,
            PDOCommitInterceptor_ID,
            PDORollbackInterceptor_ID,
            PDOPrepareInterceptor_ID,
            PDOQueryInterceptor_ID,
            PDOStatementMethodExitPointInterceptor_ID,
            PDOExecInterceptor_ID,
            PDOStatementExecInterceptor_ID,

            MySQLConnectInterceptor_ID,
            MySQLCloseInterceptor_ID,
            MySQLSelectDBInterceptor_ID,
            MySQLQueryInterceptor_ID,

            MemcachedConstructorInterceptor_ID,
            MemcachedResetServerListInterceptor_ID,
            MemcachedReadInterceptor_ID,
            MemcachedWriteInterceptor_ID,

            MemcacheServerConnectionMethodInterceptor_ID,
            MemcacheCloseMethodInterceptor_ID,
            MemcacheReadMethodInterceptor_ID,
            MemcacheWriteMethodInterceptor_ID,

            MemcacheServerConnectionProcedureInterceptor_ID,
            MemcacheAddServerProcedureInterceptor_ID,
            MemcacheCloseProcedureInterceptor_ID,
            MemcacheReadProcedureInterceptor_ID,
            MemcacheWriteProcedureInterceptor_ID,

            MySQLIConnectInterceptor_ID,
            MySQLIConstructorInterceptor_ID,
            MySQLIRealConnectInterceptor_ID,
            MySQLIRealConnectMethodInterceptor_ID,
            MySQLICloseInterceptor_ID,
            MySQLICloseMethodInterceptor_ID,
            MySQLISelectDBInterceptor_ID,
            MySQLISelectDBMethodInterceptor_ID,
            MySQLIQueryInterceptor_ID,
            MySQLIQueryMethodInterceptor_ID,
            MySQLICommitInterceptor_ID,
            MySQLICommitMethodInterceptor_ID,
            MySQLIStatementInitInterceptor_ID,
            MySQLIStatementInitMethodInterceptor_ID,
            MySQLIPrepareInterceptor_ID,
            MySQLIPrepareMethodInterceptor_ID,
            MySQLIStatementPrepareInterceptor_ID,
            MySQLIStatementPrepareMethodInterceptor_ID,
            MySQLIStatementExecuteInterceptor_ID,
            MySQLIStatementExecuteMethodInterceptor_ID,
            MySQLIStatementBindParamInterceptor_ID,
            MySQLIStatementBindParamMethodInterceptor_ID,

            OCIBindInterceptor_ID,
            OCICloseInterceptor_ID,
            OCIConnectInterceptor_ID,
            OCIExecuteInterceptor_ID,
            OCIFreeStatementInterceptor_ID,
            OCIParseInterceptor_ID,

            PostgreSQLInterceptor_ID,
            PostgreSQLConnectInterceptor_ID,
            PostgreSQLCloseInterceptor_ID,
            PostgreSQLGetResultInterceptor_ID,
            PostgreSQLQueryInterceptor_ID,
            PostgreSQLExecuteInterceptor_ID,
            PostgreSQLPrepareInterceptor_ID,
            PostgreSQLSendPrepareInterceptor_ID,
            PostgreSQLSendExecuteInterceptor_ID,
            PostgreSQLSendQueryInterceptor_ID,
            PostgreSQLSendQueryParamsInterceptor_ID,
            PostgreSQLSelectInterceptor_ID,
            PostgreSQLCopyToInterceptor_ID,
            PostgreSQLCopyFromInterceptor_ID,
            PostgreSQLInsertInterceptor_ID,
            PostgreSQLUpdateInterceptor_ID,
            PostgreSQLDeleteInterceptor_ID,
            PostgreSQLCancelQueryInterceptor_ID,

            PredisConstructorInterceptor_ID,
            PredisCallTrafficInterceptor_ID,
            PredisExecuteCommandTrafficInterceptor_ID,
            PredisBatchTrafficInterceptor_ID,
            PredisCloseInterceptor_ID,

            EUM_MainScriptInterceptor_ID,

            NativeSOAPHandleMethodInterceptor_ID,
            NativeSOAPCallUserFuncInterceptor_ID,
            NuSOAPInvokeMethodInterceptor_ID,
            NuSOAPCallUserFuncArrayInterceptor_ID,
            NuSOAPClientSendInterceptor_ID,
            NuSOAPHTTPTransportSendInterceptor_ID,
            SOAPClientCallInterceptor_ID,
            SOAPClientDoRequestInterceptor_ID,

            InfoPointInterceptor_ID,
            MethodDataInterceptor_ID,

            BasicTestInterceptor_ID,
            BasicTestInterceptor2_ID,
            FullTestInterceptor_ID,

            HttpDataMainScriptInterceptor_ID,

            INTERCEPTOR_COUNT
        };

        static void init();
        static void shutdown();

    private:
        static AgentLogger logger;
};
/* }}} */

/* {{{ AMethodInterceptor */
class AMethodInterceptor : boost::noncopyable
{
    public:
        virtual ~AMethodInterceptor();
        inline const std::string& getClass() const { return m_className; }
        virtual int getPriority() const { return 0; }

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv) = 0;

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state) = 0;

        virtual bool isActive(const PHPExecEnvironment* phpExecEnv) const = 0;

        inline InterceptorRegistry::ID getInterceptorID() const { return m_interceptorID; }

        virtual bool needParamsInOnMethodBegin() const = 0;
        virtual bool needReturnValueInOnMethodEnd() const = 0;
        virtual bool shouldCallOnMethodEnd() const = 0;
    protected:
        inline AMethodInterceptor(const char* const className,
                                  InterceptorRegistry::ID interceptorID)
            : m_className(className)
            , m_interceptorID(interceptorID)
            {
            }

        virtual bool reportErrorIfNeeded(const PHPExecEnvironment* execEnv);
    private:
        std::string const m_className;
        InterceptorRegistry::ID const m_interceptorID;
};


/* }}} */

/* {{{ InterceptorEngine */

class InterceptorSet;

void intrusive_ptr_add_ref(const InterceptorSet*);
void intrusive_ptr_release(const InterceptorSet*);

struct UnResolvedInterceptorsTag {};
struct ResolvedToOpArrayInterceptorsTag {};

typedef boost::intrusive::set_base_hook<  boost::intrusive::tag<UnResolvedInterceptorsTag>
                                        , boost::intrusive::link_mode<boost::intrusive::normal_link> > t_UnResolvedInterceptorsHook;

typedef boost::intrusive::set_base_hook<  boost::intrusive::tag<ResolvedToOpArrayInterceptorsTag>
                                        , boost::intrusive::link_mode<boost::intrusive::normal_link> > t_ResolvedToOpArrayInterceptorsHook;


class InterceptorSet    : public t_UnResolvedInterceptorsHook
                        , public t_ResolvedToOpArrayInterceptorsHook
                        , boost::noncopyable
{
    friend class InterceptionEngine;
    friend void intrusive_ptr_add_ref(const InterceptorSet*);
    friend void intrusive_ptr_release(const InterceptorSet*);
    private:
        struct InterceptorPriorityCmp
        {
            inline bool operator()(const AMethodInterceptor* m1, const AMethodInterceptor* m2) const
            {
                if (m1->getPriority() == m2->getPriority())
                    return m1 < m2;
                return m1->getPriority() < m2->getPriority();
            }
        };
        typedef std::set<AMethodInterceptor*, InterceptorPriorityCmp> t_Set;

    public:

        typedef t_Set::const_iterator const_iterator;

        inline const_iterator begin() const { return m_set.begin(); }
        inline const_iterator end() const { return m_set.end(); }
        inline size_t size() const { return m_set.size(); }
        inline const CallableInfo* getCallableInfo() const { return m_callableInfo; }
        inline bool resolveOnce() const { return m_resolveOnce; }

        inline bool operator<(const InterceptorSet& other) const
        {
            if (m_callableInfo && other.m_callableInfo) {
                return *m_callableInfo < *other.m_callableInfo;
            } else {
                return m_callableInfo < other.m_callableInfo;
            }
        }

        static boost::intrusive_ptr<InterceptorSet> create()
        {
            return boost::intrusive_ptr<InterceptorSet>(new InterceptorSet(), true);
        }

        static boost::intrusive_ptr<InterceptorSet> create(const CallableInfo& callableInfo, bool resolveOnce)
        {
            return boost::intrusive_ptr<InterceptorSet>(new InterceptorSet(callableInfo, resolveOnce), true);
        }

    private:
        InterceptorSet(const CallableInfo& callableInfo, bool resolveOnce)
            : m_refCount(0)
            , m_callableInfo(NULL)
            , m_resolveOnce(resolveOnce)
        {
            setCallableInfo(callableInfo);
        }

        InterceptorSet()
            : m_refCount(0)
            , m_callableInfo(NULL)
            , m_resolveOnce(false)
        {
        }

        ~InterceptorSet()
        {
            if (m_callableInfo)
                m_callableInfo->~CallableInfo();
            m_callableInfo = NULL;
        }

        inline std::pair<t_Set::iterator, bool> insert(AMethodInterceptor* interceptor, bool resolveOnce)
        {
            m_resolveOnce = m_resolveOnce && resolveOnce;
            return m_set.insert(interceptor);
        }

        template <typename t_Iterator>
        inline void insert(t_Iterator first, t_Iterator last, bool resolveOnce)
        {
            m_set.insert(first, last);
            m_resolveOnce = m_resolveOnce && resolveOnce;
        }

        inline void setCallableInfo(const CallableInfo& callableInfo)
        {
            if (!m_callableInfo)
                m_callableInfo = new (&(m_callableInfoStorage.bytes[0])) CallableInfo(callableInfo);
            else
                *m_callableInfo = callableInfo;
        }

        size_t erase(AMethodInterceptor* interceptor) { return m_set.erase(interceptor); }

        mutable unsigned long m_refCount;

        union
        {
            void* alignment;
            uint8_t bytes[sizeof(CallableInfo)];
        } m_callableInfoStorage;
        CallableInfo* m_callableInfo;

        t_Set m_set;
        bool m_resolveOnce;
};

inline void intrusive_ptr_add_ref(const InterceptorSet* set)
{
    ++(set->m_refCount);
}

inline void intrusive_ptr_release(const InterceptorSet* set)
{
    BOOST_ASSERT(set->m_refCount > 0);
    --set->m_refCount;
    if (set->m_refCount == 0)
        delete const_cast<InterceptorSet*>(set);
}

class InterceptionEngine
{
    friend class CompiledScriptInterceptor;
    public:
        /**
            Constructor.
            @param zendFunctionOpArrayReservedOffset The index into the
            reserved member array in the zend_op_array structure the InterceptEngine
            should use to store InterceptorSet's.
         */
        InterceptionEngine(int zendFunctionOpArrayReservedOffset);

        ~InterceptionEngine();

        void addInterceptorForCallable(const CallableInfo& callable,
                                       AMethodInterceptor* const interceptor,
                                       bool resolveOnce = false);

        /**
            Removes the specified interceptor class from set of interceptors
            that will be invoked when the specified callable starts/ends execution.
            <p>
            If the specified interceptor class is not currently in the set of interceptors
            associated with the specified callable this method does nothing.
            <p>
            If the specified interceptor class is not "registered" this method logs
            an error, but otherwise does nothing.
            @param callable Description of php callable
            @param interceptor_class Name of the interceptor class to remove.
         */
        void removeInterceptorForCallable(const CallableInfo& callable, AMethodInterceptor* const interceptor);

        /**
            Adds an interceptor to a specific zend_op_array.  The interceptor will
            only be invoked during the current request.
            @param opArray zend_op_array to add an interceptor to.
            @param interceptor_class Name of the interceptor class to add to
            the interceptor set for the specified zend_op_array.
            @return true if the interceptor was successfully added, false otherwise.
         */
        bool addInterceptorForOpArray(zend_op_array* opArray, AMethodInterceptor* const interceptor);

        /**
            Removes all interceptors from the specified op_array for the rest of
            the current request.  Use this method with care, it is kind of a hack.
            @param opArray zend_op_array to remove all interceptors from.
         */
        void removeAllInterceptorsFromOpArray(zend_op_array* opArray);

        /**
            Notifies the InterceptEngine that a new PHP file has been compiled.
            @param opArray zend_op_array for the newly compiled PHP file.
         */
        void onFileCompiled(zend_op_array* opArray);

        /**
            Notifies the InterceptEngine that a new request has started.
            @param env The PHPExecEnvironment that has been created for the
            new request.
         */
        void onRequestBegin(boost::shared_ptr<PHPExecEnvironment> env);

        /**
            Notifies the InterceptEngine that the current request is ending.
         */
        void onRequestEnd();

        /**
            Notifies the InterceptEngine that a PHP function is
            starting to execute.
            @param fileName Out parameter that will be set to the file name of the
            call site of the function that is about to start execution.  The caller
            of this method must *not* free the returned string.  The php engine will
            free the string at the end of the current request.
            @param lineno Out parameter that will be set to the line number of the
            call site of the function that is about to start execution.
            @param interceptorSet Out parameter that will be set to NULL or to the
            InterceptorSet for the function that is about to be called.
            @param edata The zend_execute_data* passed to the execute call back by
            the php engine.  This will be null when this method is called for the
            start of the execution of the first php script for a request ( the root
            of the call stack ).
            @param op The zend_op_array* for the function that is about to start
            execution.  This will be null if the function that is about to start
            execution is an internal php function.
            @param internal true indicates that the function that is about to start
            execution is an internal php function, false otherwise.
         */
        void onExecuteBegin(const InterceptorSet** interceptorSet,
                            zend_execute_data* edata,
                            zend_op_array* op,
                            bool internal);


        boost::intrusive_ptr<InterceptorSet> findExistingInterceptorSetForFunction(const zend_function* f);

        boost::intrusive_ptr<InterceptorSet> getStartFunctionInterceptorSet() { return m_startFunctionInterceptorSet; }

    private:
        boost::intrusive_ptr<InterceptorSet> findExistingInterceptorSetForOpArray(const zend_op_array* opArray);
        boost::intrusive_ptr<InterceptorSet> findExistingInterceptorSetForInternalFunction(const zend_internal_function* internalFunction);

        bool addInterceptorSetToOpArray(zend_op_array* opArray, const boost::intrusive_ptr<InterceptorSet>& interceptorSet);
        bool addInterceptorSetToFunction(zend_function* f, const boost::intrusive_ptr<InterceptorSet>& interceptors);
        void instrumentZendFunctionIfPossible(const CallableInfo& callable, zend_function* target, const boost::intrusive_ptr<InterceptorSet>& interceptors);

        void updateZendFunctions();
        bool addInterceptorSetToOpArray(zend_op_array* opArray,
                                        const boost::shared_ptr<InterceptorSet>& interceptorSet);
        void resetRequestInterceptorSets();
        void resetResolvedInterceptorSets();

        AgentLogger m_logger;

        boost::unordered_map<CallableInfo, boost::intrusive_ptr<InterceptorSet>> m_callableToInterceptorSet;
        std::vector<boost::intrusive_ptr<InterceptorSet>> m_internalFunctionsInterceptorSets;

        typedef boost::intrusive::set<  InterceptorSet
                                      , boost::intrusive::base_hook<t_UnResolvedInterceptorsHook> > t_UnResolvedInterceptors;
        t_UnResolvedInterceptors m_unresolvedInterceptors;

        typedef boost::intrusive::set<  InterceptorSet
                                      , boost::intrusive::base_hook<t_ResolvedToOpArrayInterceptorsHook> > t_ResolvedToOpArrayInterceptors;
        t_ResolvedToOpArrayInterceptors m_resolvedToOpArrayInterceptors;

        // Vector of pairs, where each pair is an interceptor and an interceptor set that
        // refers to that interceptor.  This vector is used to un-register certain interceptors
        // at the end of each PHP request.
        typedef std::pair<AMethodInterceptor*, boost::intrusive_ptr<InterceptorSet> > t_RequestUnRegisterInfo;
        std::vector<t_RequestUnRegisterInfo> m_requestInterceptorSets;

        boost::intrusive_ptr<InterceptorSet> m_startFunctionInterceptorSet;

        boost::shared_ptr<PHPExecEnvironment> m_execEnv;

        int const m_zendFunctionOpArrayReservedOffset;
        boost::shared_ptr<CompiledScriptInterceptor> const m_compiledScriptInterceptor;
        boost::intrusive_ptr<InterceptorSet> const m_compiledScriptInterceptorSet;
        bool m_anyOpArraysInstrumented;
};
/* }}} */

class MethodInterceptorDelegate
{
    public:
        static void setDisabled(bool disabled) { s_disabled = disabled; }
        static bool isDisabled() { return s_disabled; }

        static void* safeOnCallableBegin(AMethodInterceptor* interceptor,
                                         const PHPExecEnvironment* phpExecEnv);
        static void safeOnCallableEnd(AMethodInterceptor* interceptor,
                                      const PHPExecEnvironment* phpExecEnv,
                                      void *state);

    private:
        static bool s_disabled;
};

#endif

// vim: set fdm=marker:
