/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __errors_h
#define __errors_h

#include "common.h"
#include "util.h"
#include "PHPAgentProtobufs.pb.h"
#include "string_match.h"
#include "zval_helper.h"
#include "safe_module_shutdown.h"
#include "services.h"

#include <vector>
#include <boost/functional/hash.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>

namespace appdynamics
{
    namespace pb
    {
        class ErrorConfig;
        class StackTrace;
    }
}

class ErrorMonitor;
class ErrorRegistry;
class RequestContext;
class StringMatch;

// Function pointer for an error handler
typedef void (*t_errorHandler)(int type, const char *error_filename, const uint32_t error_lineno, const char *format, va_list args);

// Function pointer for an exception handler
typedef void (*t_exceptionHandler)(zval *exception TSRMLS_DC);

enum ErrorType
{
    ERROR_MESSAGE,
    ERROR_EXCEPTION,
};

/* {{{ class AErrorObject */

/*
 * Abstract base class for error objects.
 */
class AErrorObject : public boost::noncopyable
{
    public:
        virtual ~AErrorObject() {}
        /*
         * @return The type of the error object.
         */
        ErrorType getType() const { return m_type; }

        /*
         * @return The error summary.
         */
        virtual std::string getSummary() = 0;

        /*
         * Computes and saves the hash code of the object to be used in custom
         * hash function.
         */
        virtual void computeHashCode() = 0;

        /*
         * Custom comparator function for error objects of the same type.
         * @param that the error object to compare against. Guaranteed to be of
         * the same type as this object.
         */
        virtual bool equalTo(const AErrorObject* that) const;

        inline size_t getHashCode() const { return m_hashCode; }

    protected:
        AErrorObject(ErrorType type) : m_type(type) {}

        size_t m_hashCode;
        ErrorType const m_type;
};

namespace boost {
    // Hash functor for boost::shared_ptr<AErrorObject>
    template<> struct hash<boost::shared_ptr<AErrorObject>> {
        size_t operator()(const boost::shared_ptr<AErrorObject>& e) const {
            size_t seed = 0;
            AErrorObject* object = e.get();
            boost::hash_combine(seed, static_cast<unsigned>(object->getType()));
            boost::hash_combine(seed, object->getHashCode());
            return seed;
        }
    };
}

namespace std {
    template<> struct equal_to<boost::shared_ptr<AErrorObject>> {
        bool operator()(const boost::shared_ptr<AErrorObject>& a, const boost::shared_ptr<AErrorObject>& b) const {
            if (a->getType() != b->getType())
                return false;
            return a.get()->equalTo(b.get());
        }
    };
}
/* }}} */

/* {{{ class MessageErrorObject */

/*
 * Error class representing standard PHP errors (messages).
 */
class MessageErrorObject : public AErrorObject
{
    public:
        /*
         * @param phpErrorType PHP-specific error type
         * @param message Error message
         * @param filename filename where the error occurred
         * @param lineNumber line number where the error occurred
         */
        MessageErrorObject(int phpErrorType, const char* message,
                           const char* filename, unsigned lineNumber);


        /*
         * NOTE: If return-by-copy turns out to be too slow, we can switch to
         * return-by-ref and do lazy generation of strings in error objects.
         */
        std::string getName();
        std::string getDetail();

        virtual std::string getSummary();
        virtual void computeHashCode();
        virtual bool equalTo(const AErrorObject* that) const;

        inline appdynamics::pb::PHPErrorThreshold getLevel() const { return m_level; }

    private:
        std::string m_message;
        const char* m_filename;
        unsigned m_lineNumber;
        appdynamics::pb::PHPErrorThreshold m_level;
        const char* m_phpErrorName;
};
/* }}} */

/* {{{ class AExceptionBase */

/**
 * A base class for exception-like objects.
 */
class AExceptionBase : public AErrorObject
{
    public:
        virtual std::string getClass() const = 0;
        virtual std::string getMessage() const = 0;
        virtual std::string getOriginFilename() const = 0;
        virtual unsigned getOriginLineno() const = 0;

        virtual std::string getSummary();

        /*
         * Returns a zval array representing the stack trace.
         */
        virtual const appdynamics::pb::StackTrace& getStackTrace() const = 0;

        virtual boost::shared_ptr<AExceptionBase> getExceptionCause() const = 0;

        virtual void copyStackTrace(const ZValPointerArray& exceptionTrace, appdynamics::pb::StackTrace* trace);

    protected:
        AExceptionBase() : AErrorObject(ERROR_EXCEPTION) { }
};
/* }}} */

/* {{{ class ExceptionObject */

class ExceptionObject : public AExceptionBase
{
    template <class T, class Arg1, class... Args>
    friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);

    public:
        static inline boost::shared_ptr<ExceptionObject> create(const PHPExecEnvironment* phpExecEnv,
                                                                zval* exception);
        static inline boost::shared_ptr<ExceptionObject> create(const PHPExecEnvironment* phpExecEnv,
                                                                const ZValPointerObject& object);

        virtual void computeHashCode();
        virtual bool equalTo(const AErrorObject* that) const;
        virtual boost::shared_ptr<AExceptionBase> getExceptionCause() const;

        inline std::string getClass() const
        {
            return m_className;
        }

        inline std::string getMessage() const
        {
            return m_message;
        }

        inline std::string getOriginFilename() const
        {
            return m_originFilename;
        }

        inline unsigned getOriginLineno() const
        {
            return m_originLineno;
        }

        inline const appdynamics::pb::StackTrace& getStackTrace() const
        {
            return m_trace;
        }

    private:
        const PHPExecEnvironment* const m_execEnv;
        boost::shared_ptr<ExceptionObject> m_exceptionCause;
        std::string m_className;
        std::string m_message;
        std::string m_originFilename;
        unsigned m_originLineno;

        mutable appdynamics::pb::StackTrace m_trace;

        ExceptionObject(const PHPExecEnvironment* phpExecEnv,
                        const ZValPointerObject& exceptionObject);
        ExceptionObject(const PHPExecEnvironment* phpExecEnv,
                        const ZValPointerObject& exceptionObject,
                        const boost::shared_ptr<ExceptionObject>& exceptionCause);

        void copyFromPHPException(const ZValPointerObject& exceptionObject);
};

/* }}} */

/* {{{ class SyntheticExceptionObject */

/**
 * This class represents errors caught via error callback during an exit call.
 */
class SyntheticExceptionObject : public AExceptionBase
{
    template <class T, class Arg1, class... Args>
    friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);

    public:
        static inline boost::shared_ptr<SyntheticExceptionObject> create(const PHPExecEnvironment* phpExecEnv,
                                                                   const std::string& errorName,
                                                                   const std::string& errorMessage)
        {
            return boost::make_shared<SyntheticExceptionObject>(phpExecEnv, errorName, errorMessage);
        }

        virtual void computeHashCode();
        virtual bool equalTo(const AErrorObject* that) const;

        inline const appdynamics::pb::StackTrace& getStackTrace() const
        {
            return m_trace;
        }

        inline boost::shared_ptr<AExceptionBase> getExceptionCause() const
        {
            // no causes in this fake exception object
            return boost::shared_ptr<ExceptionObject>();
        }

        inline std::string getClass() const
        {
            return m_fullErrorName;
        }

        inline std::string getMessage() const
        {
            return m_errorMessage;
        }

        inline std::string getOriginFilename() const
        {
            return m_originFilename;
        }

        inline unsigned getOriginLineno() const
        {
            return m_originLineno;
        }

    private:
        const PHPExecEnvironment* const m_execEnv;
        std::string m_fullErrorName;
        std::string m_errorMessage;
        std::string m_originFilename;
        unsigned m_originLineno;
        mutable appdynamics::pb::StackTrace m_trace;

        SyntheticExceptionObject(const PHPExecEnvironment* phpExecEnv, const std::string& backendName, const std::string& errorMessage);
};
/* }}} */

/* {{{ class ExceptionCauseMap */

/*
 * This class represents one level of an exception cause chain configuration.
 * Each level has cause class names as keys and the nested (next level)
 * ExceptionCauseMap as the value. For instance, if the configured cause chains
 * are as follows:
 *
 *   A : B : C
 *   A : B : D
 *
 *  then the exception cause maps (starting from the outermost) would look like
 *  this:
 *
 *  map(
 *      B : map(
 *               C : map( )
 *               D : map( )
 *             )
 *     )
 *
 * In case one chain is a prefix of another, i.e. A : B and A : B : C, the
 * isChainTerminator flag is used to indicate that the B map, while containing
 * entries, also terminates at least one of the chains.
 */

class ExceptionCauseMap {
    public:
        ExceptionCauseMap() : m_isChainTerminator(true)
        {
        }

        inline bool contains(const std::string& className)
        {
            return (m_causeMap.find(className) != m_causeMap.end());
        }

        inline boost::shared_ptr<ExceptionCauseMap> get(const std::string& className)
        {
            auto it = m_causeMap.find(className);
            if (it != m_causeMap.end())
                return it->second;
            else
                return boost::shared_ptr<ExceptionCauseMap>();
        }

        inline void put(const std::string& className, const boost::shared_ptr<ExceptionCauseMap>& causeMap)
        {
            m_causeMap[className] = causeMap;
        }

        inline void clear()
        {
            m_causeMap.clear();
        }

        inline size_t size()
        {
            return m_causeMap.size();
        }

        inline bool isChainTerminator() const
        {
            return m_isChainTerminator;
        }

        inline void setIsChainTerminator(bool value)
        {
            m_isChainTerminator = value;
        }

    private:
        boost::unordered_map<std::string, boost::shared_ptr<ExceptionCauseMap>> m_causeMap;
        bool m_isChainTerminator;
};

/* }}} */

/* {{{ class ErrorMonitor */

/*
 * Provides error monitoring functionality.
 * <p>
 * This class is the main interface to error monitoring and collection. It
 * overrides the existing PHP error and exception handlers with custom ones and
 * captures the errors during execution.
 */
class ErrorMonitor : public IMonitorStateListener, public IBailoutHandler, boost::noncopyable
{
    public:
        /**
            An object that can be instantiated on the stack
            to suppress reporting of errors that were reported by the
            reportError method below to the proxy.
            <p>
            Suppression starts when the constructor returns and ends
            when the destructor returns.
            <p>
            Suppression scopes may be nested.
         */
        class SuppressionScope : boost::noncopyable
        {
        public:
            SuppressionScope(ErrorMonitor* monitor, bool suppressDelegation = false);
            ~SuppressionScope();
        private:
            ErrorMonitor* const m_monitor;
            bool const m_delegationWasSuppressed;
        };


        inline ErrorMonitor()
            : m_logger(getLogger(std::string(LogContext::ERROR) + ".ErrorMonitor"))
            , m_registryLogger(getLogger(std::string(LogContext::ERROR) + ".ErrorRegistry"))
            , m_enabled(false)
            , m_detectErrorMessages(true)
            , m_markTransactionAsErrorOnErrorMessages(true)
            , m_messageLevelThreshold(appdynamics::pb::WARNING)
            , m_exceptionErrorExpected(false)
            , m_errorSuppressionDepth(0)
            , m_delegationSuppressed(false)
            , m_recursionDepth(0)
            , m_hadFatalError(false)
            , m_exceptionCauseMap(boost::make_shared<ExceptionCauseMap>())
        {
        }

        virtual ~ErrorMonitor() {};

        inline t_errorHandler getDefaultErrorHandler() const { return m_defaultErrorHandler; }
        inline void expectExceptionError() { m_exceptionErrorExpected = true; }
        inline bool isExceptionErrorExpected() const { return m_exceptionErrorExpected; }
        inline uint8_t getErrorsSuppressionDepth() const { return m_errorSuppressionDepth; }
        inline bool isDelegationSuppressed() const { return m_delegationSuppressed; }

        inline bool shouldReportExceptions() const
        {
            return (m_enabled && getErrorsSuppressionDepth() == 0);
        }

        inline void addToErrorSuppressionDepth(int8_t delta)
        {
            m_errorSuppressionDepth += delta;
        }

        /**
         * Returns true if the error should be reported, based on ErrorMonitor
         * config and the error type.
         * @param type The type of the error
         */
        bool shouldReportError(int type) const;

        /**
         * Reports a standard PHP error.
         * @param phpExecEnv PHP execution environment.
         * @param phpErrorType PHP-specific error type
         * @param message Error message
         * @param filename filename where the error occurred
         * @param lineNumber line number where the error occurred
         */
        boost::shared_ptr<AErrorObject> reportError(const PHPExecEnvironment* phpExecEnv,
                                                    int phpErrorType,
                                                    const char* message,
                                                    const char* filename,
                                                    unsigned lineNumber);

        /**
         * Reports an uncaught PHP exception.
         * @param object zval for the current exception object.
         */
        boost::shared_ptr<AErrorObject> reportException(const PHPExecEnvironment* phpExecEnv, zval* exceptionValue);

        /**
         * Reports an exception that was not thrown by the user or extension
         * code, but instead based on a library error, timeout, etc.
         *
         * @param phpExecEnv execution environment pointer
         * @param errorName identifies the type of the error
         * @param errorMessage the error message to report
         */
        boost::shared_ptr<AErrorObject> reportSyntheticException(const PHPExecEnvironment* phpExecEnv, const std::string& backendName, const std::string& errorMessage);

        /*
         * Installs custom error handler.
         */
        static void installErrorHandler(const AgentLogger& logger);

        /**
         * Restores previous error handler.
         */
        static void uninstallErrorHandler(const AgentLogger& logger);

        /**
         * Sets up this error monitor for the request that is
         * just starting.
         * @param errorRegistry ErrorRegistry into which reported errors
         * should be registered.
         */
        void onRequestBegin(const boost::shared_ptr<RequestContext>& requestContext);

        /*
         * Resets this error monitor for the next request.
         */
        void onTransactionEnd();

        /*
         * Deletes the request context.
         */
        void onRequestEnd();

        /*
         * Parses incoming error configuration.
         */
        void configure(const appdynamics::pb::ErrorConfig& errorConfig);

        void onMonitorDisable();
        void onMonitorEnable();

        virtual void afterBailout();

        uint8_t enterErrorCallback() { return ++m_recursionDepth; }
        void leaveErrorCallback() { BOOST_ASSERT(m_recursionDepth > 0); --m_recursionDepth; }

        const AgentLogger& getRegistryLogger() const { return m_registryLogger; }

        const AgentLogger& getMonitorLogger() const { return m_logger; }
    private:
        class IgnoreExceptionConfig {
            public:
                static const char* WILDCARD_PATTERN;
                bool messageMatchingConfigured;
                bool causeMatchingConfigured;

                IgnoreExceptionConfig()
                    : messageMatchingConfigured(false)
                    , causeMatchingConfigured(false)
                    , m_isWildcard(false)
                { }

                void update(const google::protobuf::RepeatedPtrField<std::string>& exceptionClassNames,
                            const boost::shared_ptr<StringMatch>& messagePattern,
                            const boost::shared_ptr<ExceptionCauseMap>& causeMap);
                bool matchExceptionMessage(const AgentLogger& logger,
                                           const PHPExecEnvironment* phpExecEnv,
                                           const boost::shared_ptr<AExceptionBase>& exception) const;
                bool matchCauseChain(const boost::shared_ptr<AExceptionBase>& exception,
                                     const boost::shared_ptr<ExceptionCauseMap>& currentCauseMap) const;

            private:
                bool m_isWildcard;
                std::vector<boost::shared_ptr<StringMatch>> m_messagePatterns;

        };

        void recordException(const PHPExecEnvironment* phpExecEnv,
                             const boost::shared_ptr<AExceptionBase>& exceptionObject);
        bool isMessageIgnored(const PHPExecEnvironment* phpExecEnv,
                              const std::string& message) const;
        bool isExceptionIgnored(const PHPExecEnvironment* phpExecEnv,
                                const boost::shared_ptr<AExceptionBase>& exception) const;

        AgentLogger m_logger;
        AgentLogger m_registryLogger;
        boost::shared_ptr<RequestContext> m_requestContext;
        static t_errorHandler m_defaultErrorHandler;
        bool m_enabled;
        bool m_detectErrorMessages;
        bool m_markTransactionAsErrorOnErrorMessages;
        appdynamics::pb::PHPErrorThreshold m_messageLevelThreshold;
        bool m_exceptionErrorExpected;
        uint8_t m_errorSuppressionDepth;
        bool m_delegationSuppressed;
        uint8_t m_recursionDepth;
        bool m_hadFatalError;

        std::vector<boost::shared_ptr<StringMatch>> m_ignoredMessagePatterns;
        boost::unordered_map<std::string, boost::shared_ptr<IgnoreExceptionConfig>> m_ignoredExceptions;
        const boost::shared_ptr<ExceptionCauseMap> m_exceptionCauseMap;
};

inline ErrorMonitor::SuppressionScope::SuppressionScope(ErrorMonitor* monitor,
                                                        bool suppressDelegation)
    : m_monitor(monitor)
    , m_delegationWasSuppressed(m_monitor->isDelegationSuppressed())
{
    m_monitor->addToErrorSuppressionDepth(1);
    m_monitor->m_delegationSuppressed = suppressDelegation;
}

inline ErrorMonitor::SuppressionScope::~SuppressionScope()
{
    m_monitor->addToErrorSuppressionDepth(-1);
    m_monitor->m_delegationSuppressed = m_delegationWasSuppressed;
}

/* }}} */

bool getStackTraceProtobufID(const AExceptionBase* exception,
                             boost::unordered_map<size_t, unsigned>* stackIDMap,
                             google::protobuf::RepeatedPtrField<appdynamics::pb::StackTrace>* stackTraces,
                             unsigned* stackTraceID);

/* {{{ class ErrorRegistry */

/*
 * Class to keep track of the errors and their counts.
 * <p>
 * Holds all the error objects with counts as well as a list of application
 * (non-trasaction) errors. There should be only a single instance of this
 * class.
 */
class ErrorRegistry : boost::noncopyable
{
    public:
        inline ErrorRegistry(const ErrorMonitor* monitor)
            : m_logger(monitor->getRegistryLogger())
            , m_errorCapacityReached(false)
        {
        }

        inline const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& getErrors() const
        {
            return m_errors;
        }

        inline const std::vector<boost::shared_ptr<AErrorObject>>& getErrorsInReportedOrder() const
        {
            return m_errorsInReportedOrder;
        }

        inline void clear()
        {
            m_errorCapacityReached = false;
            m_errors.clear();
            m_errorsInReportedOrder.clear();
        }

        /*
         * Register the given error.
         * @param errorObject The error to register
         * @param markTransactionAsError Whether the transaction should be marked
         * as an error one because of this error
         */
        void report(boost::shared_ptr<AErrorObject> errorObject);


    private:
        static const int ERROR_CAPACITY = 1000;

        AgentLogger const m_logger;

        // A map to keep track of unique error counts
        boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned> m_errors;
        std::vector<boost::shared_ptr<AErrorObject>> m_errorsInReportedOrder;
        bool m_errorCapacityReached;
};
/* }}} */

#endif // __errors_h

// vim: set fdm=marker:
