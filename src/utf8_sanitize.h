#ifndef __utf8_sanitize_h
#define __utf8_sanitize_h

#include <string>

namespace {
    // UTF-8 for 0xFFFD.
    static const char s_invalidUTF8Replacement[] = { '\xEF', '\xBF', '\xBD' };
    size_t s_invalidUTF8ReplacementLength = sizeof(s_invalidUTF8Replacement);
}

/**
    Ensure that the byte bytes sequence in the specified string
    is valid UTF-8.
 */
static inline std::string sanitizeUTF8(const std::string& input)
{
    std::string result;
    result.reserve(input.length());

    std::string::const_iterator charStart = input.begin();
    std::string::const_iterator const end = input.end();

    // Ack!!! goto's....  This code is more concise
    // with the non-linear controll flow than without.
    //
    // Using the goto's enabled this code to have single place
    // the replacement character is added to the output.
    //
    // Once inside the body of the loop below, we will end
    // up either:
    //
    // a. using a continue to get to the top of the loop again.
    //    This only happens when the UTF-8 byte sequence is considered
    //    valid.
    //
    // b. goto'ing appendReplacement after setting charStart to
    //    the next byte potentially starts a valid UTF-8 sequence.

    while (charStart != end) {
        {
            std::string::const_iterator i1 = charStart;
            int8_t const b1 = *i1;
            // Handle 1 byte characters....
            if ((b1 & '\x80') == 0) {
                result.append(1, b1);
                ++charStart;
                continue;
            }

            // If not a 1 byte character then we need to
            // read the 2nd byte.
            std::string::const_iterator i2 = i1 + 1;
            if (i2 == end) {
                // No 2nd byte, underflow.
                // Add the replacement character and bail.
                charStart = end;
                goto appendReplacement;
            }

            if ((b1 & '\xC0') == '\x80') {
                charStart = i2;
                goto appendReplacement;
            }

            int8_t const b2 = *i2;
            if ((b1 & '\xE0') == '\xC0') {

                // Less than 8 bits, why use 2 bytes when 1 would work.
                bool const notEnoughBits = (b1 & '\x1E') == 0;
                if (notEnoughBits || ((b2 & '\xC0') != '\x80')) {
                    charStart = i2;
                    goto appendReplacement;
                }

                // Valid 2 byte character.
                result.append(1, b1);
                result.append(1, b2);
                charStart += 2;
                continue;
            }

            // If not 1 or 2 byte character, need to read 3rd byte.
            std::string::const_iterator i3 = i2 + 1;
            if (i3 == end) {
                // No 3rd byte, underflow.
                charStart = end;
                goto appendReplacement;
            }
            int8_t const b3 = *i3;

            if ((b1 & '\xF0') == '\xE0') {
                //  Less than 12 bits, why use 3 bytes 2 two would work
                bool const notEnoughBits = (b1 == '\xE0') && ((b2 & '\xE0') == '\x80');

                if (notEnoughBits || ((b2 & '\xC0') != '\x80')) {
                    charStart = i2;
                    goto appendReplacement;
                }

                if ((b3 & '\xC0') != '\x80') {
                    // First 2 bytes looked valid, so skip them and
                    // try to re-synchronize on 3rd byte.
                    charStart = i3;
                    goto appendReplacement;
                }

                // Valid 3 byte character.
                result.append(1, b1);
                result.append(1, b2);
                result.append(1, b3);
                charStart += 3;
                continue;
            }

            // If not 1, 2, or 3 byte character, need to read 4th byte.
            std::string::const_iterator i4 = i3 + 1;
            if (i4 == end) {
                // No 4th byte, underflow.
                // Add the replacement character and bail.
                charStart = end;
                goto appendReplacement;
            }
            int8_t const b4 = *i4;

            if ((b1 & '\xF8') == '\xF0') {

                // Unicode code points that require
                // 4 bytes are in the range:
                // 0x10000 <= code point <= 0x10FFFF.
                // This implies:
                // 1.  first byte's low 3 bits must not be > 4.  Otherwise
                //     the encoded code point is > 0x10FFFF.
                // 2.  If first byte's data bits are 4, then the
                //     second byte's high 2 data bits must be 0.  Otherwise
                //     the encoded code point is > 0x10FFFF.
                // 3.  If first byte's data bits are 0, then the
                //     second byte's high 2 data bits must not be 0.  Otherwise
                //     the encoded code point is < 0x10000 and should have
                //     been encoded with 3 bytes instead of 4.
                bool const codePointOutOfRange =
                  ((b1 & '\x07') > '\x04') ||                       // 1.
                  ((b1 == '\xF4') && ((b2 & '\xF0') != '\x80')) ||  // 2.
                  ((b1 == '\xF0') && ((b2 & '\x30') == '\x00'));    // 3.
                if (codePointOutOfRange || ((b2 & '\xC0') != '\x80')) {
                    charStart = i2;
                    goto appendReplacement;
                }

                if ((b3 & '\xC0') != '\x80') {
                    // First 2 bytes looked valid, so skip them and
                    // try to re-synchronize on 3rd byte.
                    charStart = i3;
                    goto appendReplacement;
                }

                if ((b4 & '\xC0') != '\x80') {
                    // First 2 bytes looked valid, so skip them and
                    // try to re-synchronize on 4th byte.
                    charStart = i4;
                    goto appendReplacement;
                }

                //Valid 4 byte character.
                result.append(1, b1);
                result.append(1, b2);
                result.append(1, b3);
                result.append(1, b4);
                charStart += 4;
                continue;
            }

            // All remaining cases are invalid UTF-8 sequences, so
            // append the replament char.

            // 1111111X  is not a valid start byte, try
            // to re-synchronize on the next byte.
            if ((b1 & '\xFE') == '\xFE') {
                charStart = i2;
                goto appendReplacement;
            }


            // Only two cases left:
            // 111110XX  -> 5 byte sequence, skip over up to 4 continuation bytes.
            // 1111110X  -> 6 byte sequence, skip over up to 5 continuation bytes.
            {
                size_t const continationsToSkip = ((b2 & '\xFC') == '\xF8') ? 4 : 5;
                charStart = i2;
                for (unsigned i = 0; i < continationsToSkip; ++i) {
                    if (charStart == end) {
                        // Underflow, just bail.  We already added the replacement char above.
                        goto appendReplacement;
                    }
                    if ((*charStart & '\xC0') != '\x80') {
                        // Current byte is not a continuation byte so, try to re-synchronize
                        // here.
                        goto appendReplacement;
                    }
                    ++charStart;
                }
                goto appendReplacement;
            }
        }

        appendReplacement:
            result.append(&(s_invalidUTF8Replacement[0]),
                          s_invalidUTF8ReplacementLength);
            continue;
    }

    return result;

}




#endif
