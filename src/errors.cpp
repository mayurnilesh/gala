/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "common.h"
#include "agent.h"
#include "errors.h"

#include "current_exit_call.h"
#include "transactions.h"
#include "zend_exceptions.h"
#include "request_context.h"
#include <boost/functional/hash.hpp>
#include <boost/format.hpp>
#include <sstream>

static const int MAX_MESSAGE_SIZE = 1024;

static const int OPTIMUM_FRAMES = 5;

static const uint8_t MAX_CALLBACK_DEPTH = 10;

const char* ErrorMonitor::IgnoreExceptionConfig::WILDCARD_PATTERN = "*";

t_errorHandler ErrorMonitor::m_defaultErrorHandler = NULL;

/* {{{ agent_error_cb() */

template <size_t bufferSize>
inline void getErrorMessageText(char* buffer, const char* format, va_list args)
{
    va_list argsCopy;
    va_copy(argsCopy, args);
    vslprintf(buffer, bufferSize - 1, format, argsCopy);
    va_end(argsCopy);
    buffer[bufferSize - 1] = 0;
}

/**
    Called from agent_error_cb to report errors.  Factoring the code
    this way makes it easier to understand how the recursion limit is enforced.

    This function returns a flag indicating whether or not the agent_error_cb should
    delegate to the next callback in the chain.  This should not be necessary as we
    should always delegate to the next callback in the chain.  However, I don't want
    to try to fix that bug right now.

    @return true if caller should delegate to next error callback, false otherwise.
 */
static inline void doErrorReporting(ErrorMonitor* errorMonitor,
                                    int type,
                                    const char *error_filename,
                                    const uint32_t error_lineno,
                                    const char *format,
                                    va_list args)
{
    /*
     * A timeout will cause a fatal error to be reported. Check if this is the
     * case and report a synthesized exception for TimeoutError instead.
     */
    if (type == E_ERROR && (PG(connection_status) & PHP_CONNECTION_TIMEOUT)) {
        try {
            std::string message(boost::str(boost::format("Maximum execution time of %1% seconds exceeded") % EG(timeout_seconds)));
            const PHPExecEnvironment* phpExecEnv = AG(G)->exec_env.get();
            errorMonitor->reportSyntheticException(phpExecEnv, "Timeout", message);
        }
        catch (...) {
            LOG4CXX_ERROR(errorMonitor->getMonitorLogger(), "uncaught exception while reporting timeout.");
        }
        return;
    }

    // Proceed normally
    if (!errorMonitor->shouldReportError(type)) {
        LOG4CXX_TRACE(errorMonitor->getMonitorLogger(), "Not reporting error: "
                                                        << "type: " << type);
        return;
    }
#if PHP_VERSION_ID >= 50300
    /*
     * Check error_handling mode first to see whether we need to suppress some
     * errors or await them coming back to us as exceptions.
     */
    if (EG(error_handling) != EH_NORMAL) {
        switch (type) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
            case E_PARSE:
                /* fatal errors are real errors and cannot be skipped or made
                 * into exceptions */
                break;
            case E_STRICT:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                /* for the sake of BC to old damaged code */
                return;
            case E_NOTICE:
            case E_USER_NOTICE:
                /* notices are not errors and are not treated as such like E_WARNINGS */
                break;
            default:
                /* if EH_THROW, the default handler will turn it into exception,
                 * so bail out early and process it when it comes back to us
                 */
                if (EG(error_handling) == EH_THROW && !EG(exception)) {
                    return;
                }
        }
    }
#endif

    char message[MAX_MESSAGE_SIZE];
    getErrorMessageText<sizeof(message)>(message, format, args);

    try {
        /*
         * If we are not expecting an uncaught exception error or if we're expecting
         * it, but this is not the one, report the error.
         */
        if (!errorMonitor->isExceptionErrorExpected()
                || strncmp(message, "Uncaught ", sizeof("Uncaught ")-1)) {

            /*
             * If we have an active exit call in progress, just save the error
             * message, because the exit call interceptor will use a different error
             * reporting mechanism.
             */
            TransactionContext* txContext =
                AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
            if (txContext && txContext->getCurrentExitCall()) {
                CurrentExitCall* exitCall = txContext->getCurrentExitCall();
                exitCall->setErrorMessage(message);
            } else {
                const PHPExecEnvironment* phpExecEnv = AG(G)->exec_env.get();
                errorMonitor->reportError(phpExecEnv, type, message, error_filename, error_lineno);
            }
        }
    }
    catch (...) {
        LOG4CXX_ERROR(errorMonitor->getMonitorLogger(), "uncaught exception while reporting error");
        LOG4CXX_ERROR(errorMonitor->getMonitorLogger(), "type: " << type);
        LOG4CXX_ERROR(errorMonitor->getMonitorLogger(), "error_lineno: " << error_lineno);
        LOG4CXX_ERROR(errorMonitor->getMonitorLogger(), "error_filename: " << (error_filename ? error_filename : ""));
        LOG4CXX_ERROR(errorMonitor->getMonitorLogger(), "message: " << message);
    }
}

/*
 * Custom error handler. Captures the error information and proxies to the
 * previous error handler.
 */
static void agent_error_cb(const int type,
                           const char* const errorFilename,
                           const uint32_t errorLineNumber,
                           const char* const format,
                           va_list args)
{
    TSRMLS_FETCH();

    {
        const char* const safeFilename = errorFilename ? errorFilename : "";
        const char* const safeFormat = format ? format : "";
        LOG4CXX_TRACE(AG(G)->log_agent,    "In error callback:"
                                        << " type:"     << type
                                        << " file:"     << safeFilename
                                        << " line:"     << errorLineNumber
                                        << " format:"   << safeFormat);
    }
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    t_errorHandler defaultErrorHandler = errorMonitor->getDefaultErrorHandler();

    if (!AG(G)->inExecution) {
        // Avoid reporting errors in RINIT/RSHUTDOWN and the like.
        // Just invoke the default error handler.
        LOG4CXX_ERROR(AG(G)->log_agent, "Unexpected error outside of execution");
        if (!AG(G)->log_agent->isTraceEnabled()) {
            char message[MAX_MESSAGE_SIZE];
            getErrorMessageText<sizeof(message)>(message, format, args);
            const char* const safeFilename = errorFilename ? errorFilename : "";
            LOG4CXX_ERROR(AG(G)->log_agent,
                             " type:"     << type
                          << " file:"     << safeFilename
                          << " line:"     << errorLineNumber
                          << " message:"  << message);
        }
        (defaultErrorHandler)(type, errorFilename, errorLineNumber, format, args);
        return;
    }

    // SoapClient wraps all the methods with zend_try/zend_catch, which
    // messes with the order of the longjmps. If this error callback is
    // invoked during the execution of one of SoapClient methods and the
    // error fits the specific parameters, then delegate to the SOAP error
    // handler directly. The exception it generates should come back to us
    // anyway.
    const zend_execute_data* execData = AG(G)->exec_env->getExecuteData();
#if PHP_VERSION_ID >= 70000
    if (execData && execData->func) {
#else
    if (execData && execData->object) {
#endif
        zend_class_entry* scope = APPD_EXECDATA_ZEND_FUNC(execData)->common.scope;
        if (scope == AG(G)->soapClientClassEntry) {
            if (EG(exception) == NULL ||
#if PHP_VERSION_ID >= 70000
                !instanceof_function((EG(exception)->ce),
                                     const_cast<zend_class_entry*>(AG(G)->soapFaultClassEntry))) {
#else
                Z_TYPE_P(EG(exception)) != IS_OBJECT ||
                !instanceof_function(Z_OBJCE_P(EG(exception)),
                                     const_cast<zend_class_entry*>(AG(G)->soapFaultClassEntry) TSRMLS_CC)) {
#endif
                // Invoke the default error handler
                (defaultErrorHandler)(type, errorFilename, errorLineNumber, format, args);
                return;
            }
        }
    }

    uint8_t currentDepth = errorMonitor->enterErrorCallback();
    if (currentDepth > MAX_CALLBACK_DEPTH) {
        LOG4CXX_ERROR(AG(G)->log_agent,    "Maximum error call back recursion limit("
                                        << static_cast<uint32_t>(MAX_CALLBACK_DEPTH)
                                        << ") reached:");

        char message[MAX_MESSAGE_SIZE];
        getErrorMessageText<sizeof(message)>(message, format, args);
        const char* const safeFilename = errorFilename ? errorFilename : "";
        LOG4CXX_ERROR(AG(G)->log_agent,    "type:"      << type
                                        << " file:"     << safeFilename
                                        << " line:"     << errorLineNumber
                                        << " message:"  << message);
        errorMonitor->leaveErrorCallback();
        return;
    }

    doErrorReporting(errorMonitor, type, errorFilename, errorLineNumber, format, args);
    bool doDelegate = (!errorMonitor->isDelegationSuppressed());

    if (!doDelegate) {
        errorMonitor->leaveErrorCallback();
        return;
    }
    // Delegate to the real error handler inside of a zend_try such that if
    // the real error handler does a zend_bailout, we'll catch it here and
    // call CallGraph::GraphCollectionState::onFatalError.  This will produce more
    // complete call graphs than the alternative of calling the real error handler
    // outside of a zend_try and letting the CallGraph::GraphCollectionState::afterBailout
    // clean up the call graph after the longjmp happened.  The difference is that
    // onFatalError can create call graph nodes for any exec stack frame whose call graph node
    // creation is still deferred.  After the longjmp happens we can't create any more
    // call graph nodes, we can only set the execution time on any call graph nodes that
    // have already been created.
    zend_try {
        // Invoke the default error handler
        (defaultErrorHandler)(type, errorFilename, errorLineNumber, format, args);
    }
    zend_catch {
        LOG4CXX_TRACE(AG(G)->log_agent, "default error handler did longjmp!");
        AG(G)->callGraphCollectionState.onFatalError();
        errorMonitor->leaveErrorCallback();
        // Re-throw...
        LONGJMP(*EG(bailout), FAILURE);
    }
    zend_end_try();

    errorMonitor->leaveErrorCallback();
}
/* }}} */

static size_t getStackTraceHashCode(const appdynamics::pb::StackTrace& trace)
{
    size_t stackTraceHashCode = 0;
    int stackDepth = OPTIMUM_FRAMES;
    int i = 0;

    for (int i = 0; i < trace.elements_size() && i < stackDepth; ++i){
        const appdynamics::pb::StackTraceElement& element = trace.elements(i);

        if (!element.klass().empty())
            boost::hash_combine(stackTraceHashCode, element.klass());
        if (!element.method().empty())
            boost::hash_combine(stackTraceHashCode, element.method());
        if (!element.filename().empty())
            boost::hash_combine(stackTraceHashCode, element.filename());
        if (element.linenumber())
            boost::hash_combine(stackTraceHashCode, element.linenumber());
    }

    return stackTraceHashCode;
}

void AExceptionBase::copyStackTrace(const ZValPointerArray& exceptionTrace, appdynamics::pb::StackTrace* trace)
{
    for (ZValPointerArray::const_iterator it = exceptionTrace.begin(); it != exceptionTrace.end(); ++it)
    {
        ZValPointerArray entry((*it).getValue().cast<ZValPointerArray>());
        ZValPointerString function(entry.findEntryByName<ZValPointerString>("function", strlen("function")));
        ZValPointerString klass(entry.findEntryByName<ZValPointerString>("class", strlen("class")));
        ZValPointerString file(entry.findEntryByName<ZValPointerString>("file", strlen("file")));
        ZValPointerLong line(entry.findEntryByName<ZValPointerLong>("line", strlen("line")));

        appdynamics::pb::StackTraceElement* element = trace->add_elements();
        element->set_linenumber(line.getLongValue());
        element->set_filename(file.getStringValue());
        element->set_klass(klass.getStringValue());
        element->set_method(function.getStringValue());
    }

    /*
     * add a fake stack frame representing the top level of execution.
     * TODO: remove this once the proxy can deal with an empty stacktrace.
     */
    appdynamics::pb::StackTraceElement* element = trace->add_elements();
    element->set_linenumber(getOriginLineno());
    element->set_filename(getOriginFilename());
    element->set_method(ROOT_SYMBOL);
}

/*
 * TODO: potentially look at "args" to determine the filename if "function" is
 * an include/require statement.
 */
bool getStackTraceProtobufID(const AExceptionBase* exception,
                             boost::unordered_map<size_t, unsigned>* stackIDMap,
                             google::protobuf::RepeatedPtrField<appdynamics::pb::StackTrace>* stackTraces,
                             unsigned* stackTraceID)
{
    const appdynamics::pb::StackTrace& trace = exception->getStackTrace();

    if (!trace.elements_size())
        return false;

    BOOST_ASSERT(stackTraceID);

    size_t stackHashCode = getStackTraceHashCode(trace);
    auto it = stackIDMap->find(stackHashCode);
    if (it != stackIDMap->end()) {
        *stackTraceID = it->second;
        return true;
    }

    unsigned newStackTraceID = stackTraces->size();
    stackTraces->Add()->CopyFrom(trace);

    stackIDMap->insert(std::make_pair(stackHashCode, newStackTraceID));
    *stackTraceID = newStackTraceID;
    return true;
}

static inline boost::shared_ptr<StringMatch> parseStringMatchCondition(const appdynamics::pb::Common::StringMatchCondition& condition)
{
    boost::shared_ptr<StringMatch> match = boost::make_shared<StringMatch>(condition);

    return match;
}

std::string AExceptionBase::getSummary()
{
    return getClass() + ": " + getMessage();
}

/* {{{ AErrorObject methods */

bool AErrorObject::equalTo(const AErrorObject* that) const
{
    if (that->m_type != m_type)
        return false;
    if (that->getHashCode() != getHashCode())
        return false;
    return true;
}

/* }}} */

/* {{{ MessageErrorObject methods */

MessageErrorObject::MessageErrorObject(int phpErrorType, const char* message,
                                       const char* filename, unsigned lineNumber)
    : AErrorObject(ERROR_MESSAGE)
    , m_message(message)
    , m_filename(filename)
    , m_lineNumber(lineNumber)
{
    switch (phpErrorType) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
            m_phpErrorName = "Fatal error";
            m_level = appdynamics::pb::ERROR;
            break;
        case E_RECOVERABLE_ERROR:
            m_phpErrorName = "Catchable fatal error";
            m_level = appdynamics::pb::ERROR;
            break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            m_phpErrorName = "Warning";
            m_level = appdynamics::pb::WARNING;
            break;
        case E_PARSE:
            m_phpErrorName = "Parse error";
            m_level = appdynamics::pb::ERROR;
            break;
        case E_NOTICE:
        case E_USER_NOTICE:
            m_phpErrorName = "Notice";
            m_level = appdynamics::pb::NOTICE;
            break;
        case E_STRICT:
            m_phpErrorName = "Strict Standards";
            m_level = appdynamics::pb::NOTICE;
            break;
#if PHP_VERSION_ID >= 50300
        case E_DEPRECATED:
        case E_USER_DEPRECATED:
            m_phpErrorName = "Deprecated";
            m_level = appdynamics::pb::NOTICE;
            break;
#endif
        default:
            m_phpErrorName = "Unknown error";
            m_level = appdynamics::pb::ERROR;
            break;
    }

    computeHashCode();
}

void MessageErrorObject::computeHashCode()
{
    size_t seed = 0;
    boost::hash_combine(seed, static_cast<unsigned>(m_level));
    boost::hash_combine(seed, m_message);
    if (m_filename) {
        boost::hash_combine(seed, m_filename);
        boost::hash_combine(seed, m_lineNumber);
    }
    m_hashCode = seed;
}

bool MessageErrorObject::equalTo(const AErrorObject* that) const
{
    if (!AErrorObject::equalTo(that))
        return false;

    const MessageErrorObject* thatObject = static_cast<const MessageErrorObject*>(that);

    if (m_level != thatObject->m_level)
        return false;

    if (m_message != thatObject->m_message)
        return false;

    if (m_filename != thatObject->m_filename)
        return false;

    if (m_lineNumber != thatObject->m_lineNumber)
        return false;

    return true;
}

std::string MessageErrorObject::getSummary()
{
    // Could potentially be optimized to cache the result
    return getName() + ": " + getDetail();
}

std::string MessageErrorObject::getName()
{
    switch (m_level) {
        case appdynamics::pb::NOTICE:
            return "PHP Notice";
        case appdynamics::pb::WARNING:
            return "PHP Warning";
        case appdynamics::pb::ERROR:
            return "PHP Error";
        default:
            BOOST_ASSERT_MSG(false, "Invalid error level");
            return std::string();
    }
}

std::string MessageErrorObject::getDetail()
{
    // If string streams become a bottleneck, we'll switch to snprintf().
    std::stringstream ss;

    ss << m_phpErrorName << ": " << m_message;
    if (m_filename) {
        ss << " in " << m_filename << " on line " << m_lineNumber;
    }
    return ss.str();
}

/* }}} */

/* {{{ ExceptionObject methods */

ExceptionObject::ExceptionObject(const PHPExecEnvironment* phpExecEnv,
                                 const ZValPointerObject& exceptionObject)
    : AExceptionBase()
    , m_execEnv(phpExecEnv)
    , m_exceptionCause(boost::shared_ptr<ExceptionObject>())
{
    copyFromPHPException(exceptionObject);
    computeHashCode();
}

ExceptionObject::ExceptionObject(const PHPExecEnvironment* phpExecEnv,
                                 const ZValPointerObject& exceptionObject,
                                 const boost::shared_ptr<ExceptionObject>& exceptionCause)
    : AExceptionBase()
    , m_execEnv(phpExecEnv)
    , m_exceptionCause(exceptionCause)
{
    copyFromPHPException(exceptionObject);
    computeHashCode();
}

void ExceptionObject::copyFromPHPException(const ZValPointerObject& exceptionObject)
{
    m_className = exceptionObject.getClassName(m_execEnv);

    ZValPointerString message(exceptionObject.findProtectedPropertyByName<ZValPointerString>(
                                                    m_execEnv, "message", sizeof("message")-1));
    ZValPointerString file(exceptionObject.findProtectedPropertyByName<ZValPointerString>(m_execEnv,
                "file", sizeof("file")-1));
    ZValPointerLong line(exceptionObject.findProtectedPropertyByName<ZValPointerLong>(m_execEnv,
                "line", sizeof("line")-1));

    if (message) {
        m_message = message.getStringValue();
    }
    if (file) {
        m_originLineno = line.getLongValue();
        m_originFilename = file.getStringValue();
    }

    ZValPointerArray exceptionTrace = exceptionObject.findPrivatePropertyByName<ZValPointerArray>(m_execEnv,
            "trace", strlen("trace"), m_execEnv->getDefaultExceptionClassEntry());

    if (!exceptionTrace) {
        LOG4CXX_DEBUG(m_execEnv->getAgentGlobals().log_agent,
                      "Unable to get stack trace from exception " << exceptionObject.getClassName(m_execEnv));
        return;
    }

    copyStackTrace(exceptionTrace, &m_trace);
}

boost::shared_ptr<ExceptionObject> ExceptionObject::create(const PHPExecEnvironment* phpExecEnv,
                                                           const ZValPointerObject& object)
{
    if (object) {
        if (object.isInstanceOf(phpExecEnv, phpExecEnv->getDefaultExceptionClassEntry())) {

            ZValPointerObject
                previous(object.findPrivatePropertyByName<ZValPointerObject>(phpExecEnv,
                            "previous", sizeof("previous") - 1, phpExecEnv->getDefaultExceptionClassEntry()));
            if (previous) {
                boost::shared_ptr<ExceptionObject> exceptionCause = create(phpExecEnv, previous);
                return boost::make_shared<ExceptionObject>(phpExecEnv,
                                                           object,
                                                           exceptionCause);
            } else {
                return boost::make_shared<ExceptionObject>(phpExecEnv,
                                                           object);
            }

        }
    }
    return boost::shared_ptr<ExceptionObject>();
}

boost::shared_ptr<ExceptionObject> ExceptionObject::create(const PHPExecEnvironment* phpExecEnv,
                                                           zval* exception)
{
    if (exception) {
        ZValPointerObject exceptionObject(ZValPointerAny::share(exception).cast<ZValPointerObject>());
        return create(phpExecEnv, exceptionObject);
    }
    return boost::shared_ptr<ExceptionObject>();
}

boost::shared_ptr<AExceptionBase> ExceptionObject::getExceptionCause() const
{
    return m_exceptionCause;
}

bool ExceptionObject::equalTo(const AErrorObject* that) const
{
    if (!AErrorObject::equalTo(that))
        return false;

    return true;
}

void ExceptionObject::computeHashCode()
{
    size_t seed = 0;
    boost::hash_combine(seed, getClass());
    boost::shared_ptr<AExceptionBase> cause = getExceptionCause();
    while (cause) {
        boost::hash_combine(seed, cause->getClass());
        cause = cause->getExceptionCause();
    }
    m_hashCode = seed;
}

/* }}} */

/* {{{ SyntheticExceptionObject methods */

SyntheticExceptionObject::SyntheticExceptionObject(const PHPExecEnvironment* phpExecEnv,
                                                   const std::string& errorName,
                                                   const std::string& errorMessage)
    : AExceptionBase()
    , m_execEnv(phpExecEnv)
    , m_fullErrorName(errorName + "Error")
    , m_errorMessage(errorMessage)
{
    m_originFilename = zend_get_executed_filename();
    m_originLineno = zend_get_executed_lineno();

    ZValPointerArray exceptionTrace = phpExecEnv->getStackTrace(0, DEBUG_BACKTRACE_IGNORE_ARGS).cast<ZValPointerArray>();
    copyStackTrace(exceptionTrace, &m_trace);
    computeHashCode();
}

void SyntheticExceptionObject::computeHashCode()
{
    size_t seed = 0;
    boost::hash_combine(seed, m_fullErrorName);
    boost::hash_combine(seed, m_errorMessage);
    m_hashCode = seed;
}

bool SyntheticExceptionObject::equalTo(const AErrorObject* that) const
{
    if (!AErrorObject::equalTo(that))
        return false;

    const SyntheticExceptionObject* thatObject = static_cast<const SyntheticExceptionObject*>(that);

    if (m_fullErrorName != thatObject->m_fullErrorName)
        return false;

    if (m_errorMessage != thatObject->m_errorMessage)
        return false;

    return true;
}

/* }}} */

/* {{{ ErrorRegistry methods */

void ErrorRegistry::report(boost::shared_ptr<AErrorObject> errorObject)
{
    if (m_errors.size() >= ERROR_CAPACITY) {
        if (!m_errorCapacityReached) {
            m_errorCapacityReached = true;
            LOG4CXX_WARN(m_logger, "Error registry capacity reached (" << ERROR_CAPACITY << "), dropping future errors in this request");
        }
        return;
    }

    // Increment count, or set it to 1 if new error
    auto it = m_errors.find(errorObject);
    if (it != m_errors.end()) {
        (*it).second++;
    } else {
        m_errors.insert(std::make_pair(errorObject, 1));
        m_errorsInReportedOrder.push_back(errorObject);
    }
}

/* }}} */

/* {{{ ErrorMonitor methods */

bool ErrorMonitor::shouldReportError(int type) const
{
    if (m_enabled && m_detectErrorMessages && getErrorsSuppressionDepth() == 0) {
        switch (type) {
            case E_STRICT:
#if PHP_VERSION_ID >= 50300
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
#endif
                /* we don't want to report deprecated and strict messages */
                return false;
            default:
                return true;
        }

    } else
        return false;
}

static inline bool isFatalError(int phpErrorType)
{
    switch (phpErrorType) {
    case E_ERROR:
    case E_PARSE:
    case E_CORE_ERROR:
    case E_COMPILE_ERROR:
    case E_USER_ERROR:
    case E_RECOVERABLE_ERROR:
        return true;
    }

    return false;
}

boost::shared_ptr<AErrorObject> ErrorMonitor::reportError(const PHPExecEnvironment* phpExecEnv,
                                                          int phpErrorType,
                                                          const char* message,
                                                          const char* filename,
                                                          unsigned lineNumber)
{
    if (!m_enabled)
        return boost::shared_ptr<AErrorObject>();

    if (isFatalError(phpErrorType))
        m_hadFatalError = true;

    boost::shared_ptr<MessageErrorObject> error =
        boost::make_shared<MessageErrorObject>(phpErrorType, message, filename, lineNumber);

    // Skip if message level is below the configured error message threshold
    if (error->getLevel() < m_messageLevelThreshold) {
        return boost::shared_ptr<AErrorObject>();
    }

    // Skip ignored messages
    if (isMessageIgnored(phpExecEnv, message)) {
        return boost::shared_ptr<AErrorObject>();
    }

    m_requestContext->reportError(error,
                                  m_markTransactionAsErrorOnErrorMessages);
    return error;
}

void ErrorMonitor::recordException(const PHPExecEnvironment* phpExecEnv,
                                   const boost::shared_ptr<AExceptionBase>& exceptionObject)
{
    bool markTransactionAsError = true;

    /*
     * Do not mark transaction as error if the exception is ignored and the
     * transaction is not marked as error already, just record it.
     */
    if (!m_requestContext->isErrorRequest()) {
        markTransactionAsError = !isExceptionIgnored(phpExecEnv, exceptionObject);
    }
    m_requestContext->reportError(exceptionObject,
                                  markTransactionAsError);
}

boost::shared_ptr<AErrorObject> ErrorMonitor::reportException(const PHPExecEnvironment* phpExecEnv,
                                                              zval* exceptionValue)
{
    if (!shouldReportExceptions())
        return boost::shared_ptr<AErrorObject>();

    BOOST_ASSERT(exceptionValue != NULL);
    boost::shared_ptr<ExceptionObject> exceptionObject(ExceptionObject::create(phpExecEnv, exceptionValue));
    // exceptionObject is only valid when the exceptionValue zend object
    // is an instance of the Zend default exception
    if (exceptionObject)
        recordException(phpExecEnv, boost::static_pointer_cast<AExceptionBase>(exceptionObject));
    return exceptionObject;
}

boost::shared_ptr<AErrorObject>
ErrorMonitor::reportSyntheticException(const PHPExecEnvironment* phpExecEnv,
                                       const std::string& errorName,
                                       const std::string& errorMessage)
{
    if (!m_enabled)
        return boost::shared_ptr<AErrorObject>();

    boost::shared_ptr<SyntheticExceptionObject> syntheticException(SyntheticExceptionObject::create(phpExecEnv, errorName, errorMessage));
    recordException(phpExecEnv, boost::static_pointer_cast<AExceptionBase>(syntheticException));
    return syntheticException;
}

void ErrorMonitor::installErrorHandler(const AgentLogger& logger)
{
    if (zend_error_cb != agent_error_cb) {
        LOG4CXX_TRACE(logger,     "Installing agent error callback "
                                <<  std::hex << reinterpret_cast<uintptr_t>(agent_error_cb)
                                <<  " in place of "
                                <<  std::hex << reinterpret_cast<uintptr_t>(zend_error_cb));
        m_defaultErrorHandler = zend_error_cb;
        zend_error_cb = agent_error_cb;
    }
    else {
        LOG4CXX_TRACE(logger, "Agent error callback is already installed!!!");
    }
}

void ErrorMonitor::uninstallErrorHandler(const AgentLogger& logger)
{
    if (zend_error_cb == agent_error_cb) {
        LOG4CXX_TRACE(logger,     "Uninstalling agent error callback "
                                <<  std::hex << reinterpret_cast<uintptr_t>(agent_error_cb)
                                <<  " restoring "
                                <<  std::hex << reinterpret_cast<uintptr_t>(m_defaultErrorHandler));
        zend_error_cb = m_defaultErrorHandler;
        m_defaultErrorHandler = NULL;
    }
    else {
        LOG4CXX_TRACE(logger, "Agent error callback is not currently installed!!!");
    }
}

void ErrorMonitor::onMonitorDisable()
{
    m_enabled = false;
}

void ErrorMonitor::onMonitorEnable()
{
    m_enabled = true;
}

void ErrorMonitor::afterBailout()
{
    /*
     * Clear the suppression depth so that errors are reported in the
     * onExit handlers.
     */
    m_errorSuppressionDepth = 0;

    if (!m_requestContext)
        return;

    // If we think that a fatal error caused the bailout, then
    // tell the eum context that the entry point had an uncaught exception,
    // because a bailout is like an uncaught exception.
    if (m_hadFatalError)
        m_requestContext->eumContext().onUncaughtEntryPointException();
}

void ErrorMonitor::onRequestBegin(const boost::shared_ptr<RequestContext>& requestContext)
{
    // TODO: this assertion no longer valid if restarting transaction.
    //BOOST_ASSERT(!m_requestContext);
    BOOST_ASSERT(!m_exceptionErrorExpected);
    BOOST_ASSERT(m_errorSuppressionDepth == 0);
    BOOST_ASSERT(!m_delegationSuppressed);
    m_requestContext = requestContext;
    m_hadFatalError = false;
}

void ErrorMonitor::onTransactionEnd()
{
    BOOST_ASSERT(m_requestContext);
    m_exceptionErrorExpected = false;
    m_errorSuppressionDepth = 0;
    m_delegationSuppressed = false;
}

void ErrorMonitor::onRequestEnd()
{
    BOOST_ASSERT(m_requestContext);
    m_requestContext.reset();
}

void ErrorMonitor::configure(const appdynamics::pb::ErrorConfig& errorConfig)
{
    m_enabled = true;

    if (errorConfig.has_errordetection()) {
        m_detectErrorMessages = errorConfig.errordetection().detecterrors();
        m_messageLevelThreshold = errorConfig.errordetection().phperrorthreshold();
        if (m_detectErrorMessages) {
            m_markTransactionAsErrorOnErrorMessages = errorConfig.errordetection().marktransactionaserror();
        } else {
            m_markTransactionAsErrorOnErrorMessages = false;
        }
    }

    if (errorConfig.ignoredmessages_size()) {
        m_ignoredMessagePatterns.clear();
        m_ignoredMessagePatterns.reserve(errorConfig.ignoredmessages_size());

        BOOST_FOREACH(const auto& ignoredMessage, errorConfig.ignoredmessages()) {
            boost::shared_ptr<StringMatch> match = parseStringMatchCondition(ignoredMessage.matchcondition());
            m_ignoredMessagePatterns.push_back(match);
        }
    }

    if (errorConfig.ignoredexceptions_size()) {
        m_ignoredExceptions.clear();

        BOOST_FOREACH(const auto& ignoredException, errorConfig.ignoredexceptions()) {
            if (ignoredException.classnames_size() < 1 || !ignoredException.has_matchcondition()) {
                continue;
            }

            // take just the first (leaf) exception class name for now
            std::string exceptionClass = ignoredException.classnames(0);
            boost::shared_ptr<IgnoreExceptionConfig> exceptionConfig;
            auto it = m_ignoredExceptions.find(exceptionClass);
            if (it == m_ignoredExceptions.end()) {
                exceptionConfig = boost::make_shared<IgnoreExceptionConfig>();
                m_ignoredExceptions.insert(make_pair(exceptionClass, exceptionConfig));
            } else {
                exceptionConfig = it->second;
            }

            boost::shared_ptr<StringMatch> match = parseStringMatchCondition(ignoredException.matchcondition());
            exceptionConfig->update(ignoredException.classnames(), match, m_exceptionCauseMap);
        }
    }

}

bool ErrorMonitor::isMessageIgnored(const PHPExecEnvironment* phpExecEnv,
                                    const std::string& message) const
{
    BOOST_FOREACH(const auto& pattern, m_ignoredMessagePatterns) {
        if (pattern->matchString(m_logger, phpExecEnv, message))
            return true;
    }
    return false;
}

bool ErrorMonitor::isExceptionIgnored(const PHPExecEnvironment* phpExecEnv,
                                      const boost::shared_ptr<AExceptionBase>& exception) const
{
    if (m_ignoredExceptions.empty())
        return false; // do not ignore

    boost::unordered_map<std::string, boost::shared_ptr<IgnoreExceptionConfig>>::const_iterator it;

    it = m_ignoredExceptions.find(IgnoreExceptionConfig::WILDCARD_PATTERN);
    if (it != m_ignoredExceptions.end()) {
        if (it->second->matchExceptionMessage(m_logger, phpExecEnv, exception))
            return true; // ignore
    }

    std::string exceptionClass = exception->getClass();
    it = m_ignoredExceptions.find(exceptionClass);
    if (it == m_ignoredExceptions.end()) {
        /*
         * This exception has no ignore config.
         */
        return false; // do not ignore
    }

    const boost::shared_ptr<IgnoreExceptionConfig> ignoreExceptionConfig = it->second;

    /*
     *  If neither exception cause classes nor message patterns have been configured
     *  then ignore the exception.
     */
    if (!ignoreExceptionConfig->messageMatchingConfigured && !ignoreExceptionConfig->causeMatchingConfigured)
        return true; // ignore

    if (ignoreExceptionConfig->causeMatchingConfigured
            && ignoreExceptionConfig->messageMatchingConfigured
            && ignoreExceptionConfig->matchCauseChain(exception, m_exceptionCauseMap)
            && ignoreExceptionConfig->matchExceptionMessage(m_logger, phpExecEnv, exception))
        return true; // ignore

    /*
     *  If any exception message patterns have been configured, then check whether
     *  any of them match the exception message.
     */
    if (ignoreExceptionConfig->messageMatchingConfigured
            && ignoreExceptionConfig->matchExceptionMessage(m_logger, phpExecEnv, exception))
        return true; // ignore

    /*
     * If one or more cause classes have been configured, then check whether the
     * the exception's causes match any of them.
     */
    if (ignoreExceptionConfig->causeMatchingConfigured
            && ignoreExceptionConfig->matchCauseChain(exception, m_exceptionCauseMap))
        return true; // ignore


    return false; // do not ignore (fallback)
}

void ErrorMonitor::IgnoreExceptionConfig::update(const google::protobuf::RepeatedPtrField<std::string>& exceptionClassNames,
                                                 const boost::shared_ptr<StringMatch>& messagePattern,
                                                 const boost::shared_ptr<ExceptionCauseMap>& causeMap)
{
    m_isWildcard = !exceptionClassNames.Get(0).compare(IgnoreExceptionConfig::WILDCARD_PATTERN);
    if (!messagePattern->isEmptyPattern()) {
        m_messagePatterns.push_back(messagePattern);
        messageMatchingConfigured = true;
    }

    auto it = exceptionClassNames.begin();

    // skip the initial (leaf) class name
    ++it;

    if (it == exceptionClassNames.end())
        return;

    causeMatchingConfigured = true;
    boost::shared_ptr<ExceptionCauseMap> currentCauseMap(causeMap);

    do {
        std::string causeClassName(*it);

        /*
         * If this cause is not in the map yet, add it with causeClassName as
         * its key and an empty map as its value.
         */
        if (!currentCauseMap->contains(causeClassName)) {
            currentCauseMap->put(causeClassName, boost::make_shared<ExceptionCauseMap>());
            currentCauseMap->setIsChainTerminator(false);
        }

        /*
         * If there are more tokens i.e. subsequent causes, then those must be
         * inserted into the map pointed to by the preceding cause, so reassign
         * currentCauseMap to the new map.
         */
        currentCauseMap = currentCauseMap->get(causeClassName);
    } while (++it != exceptionClassNames.end());
}

bool ErrorMonitor::IgnoreExceptionConfig::matchCauseChain(const boost::shared_ptr<AExceptionBase>& exception,
                                                          const boost::shared_ptr<ExceptionCauseMap>& currentCauseMap) const
{
    if (m_isWildcard)
        return true;

    if (!exception)
        return false;

    boost::shared_ptr<AExceptionBase> cause = exception->getExceptionCause();

    // If there are no causes, return immediately
    if (!cause)
        return false;

    /*
     * Try to get the entry for the cause class in the current map.
     */
    boost::shared_ptr<ExceptionCauseMap> nextCauseMap(currentCauseMap->get(cause->getClass()));
    if (nextCauseMap) {
        /*
         * Found a configured cause.
         * If this is the last cause configured then we've reached the end
         * and matched successfully.
         */
        if (nextCauseMap->size() == 0)
            return true;
        /* If the current exception does not have a cause and its cause map
         * terminates at least one chain, then we've reached the end and
         * matched successfully.
         */
        else if (!cause->getExceptionCause() && nextCauseMap->isChainTerminator())
            return true;
        /*
         * Call ourselves recursively with the next cause in the chain and
         * next cause map.
         */
        else
            return matchCauseChain(cause, nextCauseMap);
    } else {
        /*
         * If the current cause is not configured then perhaps the next cause is
         * configured, so call the method again with the same map but with the
         * next cause.
         */
        return matchCauseChain(cause, currentCauseMap);
    }
}

bool ErrorMonitor::IgnoreExceptionConfig::matchExceptionMessage(const AgentLogger& logger,
                                                                const PHPExecEnvironment* phpExecEnv,
                                                                const boost::shared_ptr<AExceptionBase>& exception) const
{
    if (!exception)
        return false;

    boost::shared_ptr<AExceptionBase> currentException(exception);

    while (currentException) {
        std::string message = currentException->getMessage();

        /*
         *  Iterate through the list and compare with each pattern
         *  until a match is found or the end of the list is reached.
         */
        BOOST_FOREACH(const auto& pattern, m_messagePatterns) {
            if (pattern->matchString(logger, phpExecEnv, message))
                return true;
        }

        /*
         *  If this is the wildcard, then there are no causes,
         *  so we can return false here since no match was found.
         */
        if (m_isWildcard)
            return false;

        /*
         *  Otherwise, look for the message patterns in the next cause in the chain.
         */
        currentException = currentException->getExceptionCause();
    }

    return false;
}

/* }}} */

// vim: set fdm=marker:
