/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */

#include "pdo_exitpoint.h"
#include "current_exit_call.h"
#include "backend_resolver.h"
#include "zval_helper.h"
#include "db_exitpoint.h"
#include "sqlutil.h"
#include "agent.h"
#include "oracle_connection_string.h"
#include "transactions.h"

#include <memory>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/foreach.hpp>

extern "C" {
#include "ext/pdo/php_pdo_driver.h"
#include "php_reentrancy.h"
#include <unistd.h>
}

#include "emalloc_allocator.h"
#include "PHPAgentProtobufs.pb.h"

static const char MESSAGE_PROPERTY_NAME[] = "message";
static size_t MESSAGE_PROPERTY_NAME_LEN = sizeof(MESSAGE_PROPERTY_NAME) - 1;

/* {{{ class PDOPropertyGenerator  */
class PDOPropertyGenerator : public IBackendPropertyGenerator
{
    public:
        bool getProperties(const PHPExecEnvironment* execEnv,
                           const CurrentExitCall* exitCall,
                           StringMap& properties) const;
};

const AHandleInfo::TypeTag PDOHandleInfo::typeTag;

namespace
{

    enum ParamIndices
    {
        kHostParam = 0,
        kPortParam,
        kDBParam,
        //------------
        kParamCount
    };

    struct PDODriverInfo
    {
        const char* const name;
        const char* const a4dbName;
        const pdo_data_src_parser connectionParams[kParamCount];

        inline bool haveParam(ParamIndices param) const
        {
            return connectionParams[param].optname[0] != '\0';
        }
    };

    #define PDO_DRIVER_INFO_TABLE(XX) \
        XX(mysql,      MYSQL_VENDOR      , "host"      , "localhost"   , "port"    , "3306"    , "dbname"      , NULL) \
        XX(pgsql,      POSTGRESQL_VENDOR , "host"      , "localhost"   , "port"    , "5432"    , "dbname"      , NULL) \
        XX(cubrid,     NULL              , "host"      , "localhost"   , "port"    , "3306"    , "dbname"      , NULL) \
        XX(dblib,      SYBASE_VENDOR     , "host"      , "127.0.0.1"   , ""        , NULL      , "dbname"      , NULL) \
        XX(firebird,   NULL              , ""          , NULL          , ""        , NULL      , "dbname"      , NULL) \
        XX(ibm,        DB2_VENDOR        , "HOSTNAME"  , NULL          , "PORT"    , NULL      , "DATABASE"    , NULL) \
        XX(informix,   NULL              , "host"      , NULL          , "service" , NULL      , "database"    , NULL)

    #define PDO_DRIVER_INFO_ENTRY(name,                                 \
                                  a4dbName,                             \
                                  hostParamName,                        \
                                  defaultHostName,                      \
                                  portParamName,                        \
                                  defaultPortNumber,                    \
                                  dbParamName,                          \
                                  defaultDBName)                        \
        { #name,                                                        \
          a4dbName,                                                     \
          { { hostParamName , defaultHostName   , 0 },                  \
            { portParamName , defaultPortNumber , 0 },                  \
            { dbParamName   , defaultDBName     , 0 } } },

    PDODriverInfo const driverInfo[] = {
        PDO_DRIVER_INFO_TABLE(PDO_DRIVER_INFO_ENTRY)
    };

    const size_t nDrivers = sizeof(driverInfo) / sizeof(driverInfo[0]);

    const PDODriverInfo* getDriverInfo(const char* driverName)
    {
        // simple linear search...
        for (unsigned i = 0; i < nDrivers; ++i)
        {
            if (strcmp(driverName, driverInfo[i].name) == 0)
                return &driverInfo[i];
        }
        return NULL;
    }
}

bool PDOPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                         const CurrentExitCall* exitCall,
                                         StringMap& properties) const
{
    const DelayedFunctions::FunctionTable& delayedFunctions =
        execEnv->getAgentGlobals().delayed_functions;

    const auto & phpPDOParseDataSource =
        delayedFunctions.get_php_pdo_parse_data_source();

    if (!(phpPDOParseDataSource.isValid()))
        return false;

    if (execEnv->getParamCount() < 1)
        return false;
    ZValPointerString param0String(execEnv->getParam<ZValPointerString>(0));
    if (!param0String)
        return false;

    zval* zobject = execEnv->getInvokedObject();
    ZValPointerAny object(ZValPointerAny::share(zobject));
    if (!object || object.getType() != ZValPointer::ZVal_Object)
        return false;

#if PHP_VERSION_ID >= 70000
    // we'll leave the ZvalPointerAny creation above to keep the refcount right.
    pdo_dbh_t *dbh = Z_PDO_DBH_P(zobject);
#else
    pdo_dbh_t *dbh = object.cast<ZValPointerObject>().getInternalObject<pdo_dbh_t>(execEnv);
#endif


    // We did not find a driver!  We need to get out of here!
    pdo_driver_t* const driver = dbh->driver;
    if (!driver)
        return false;

    std::string driverName(dbh->driver->driver_name, dbh->driver->driver_name_len);

    std::string databaseName;
    std::string host;
    std::string port;
    std::string a4dbName;
    std::string version;

    const PDODriverInfo* driverInfo =
        getDriverInfo(driverName.c_str());

    if (driverInfo) {
        pdo_data_src_parser connectionParams[kParamCount];
        std::memcpy(&connectionParams,
                    &(driverInfo->connectionParams),
                    sizeof(connectionParams));

        phpPDOParseDataSource(dbh->data_source,
                              dbh->data_source_len,
                              connectionParams,
                              kParamCount);

        if ((driverInfo->haveParam(kDBParam)) && (connectionParams[kDBParam].optval))
            databaseName = connectionParams[kDBParam].optval;

        if ((driverInfo->haveParam(kHostParam)) && (connectionParams[kHostParam].optval))
            host = connectionParams[kHostParam].optval;

        if ((driverInfo->haveParam(kPortParam)) && (connectionParams[kPortParam].optval))
            port = connectionParams[kPortParam].optval;

        if (driverInfo->a4dbName)
            a4dbName = driverInfo->a4dbName;

        // Clean up
        for (int i = 0; i < kParamCount; i++) {
            if (connectionParams[i].freeme)
                efree(connectionParams[i].optval);
        }
    }

    else if ( "oci" == driverName ) {
        // call phpPDOParseDataSource
        pdo_data_src_parser dbNameParam = { "dbname" , NULL, 0 } ;

        phpPDOParseDataSource(dbh->data_source,
                              dbh->data_source_len,
                              &dbNameParam,
                              1);

        if (dbNameParam.optval) {
            // Parse connection string.

            a4dbName = "ORACLE";
            std::string connectionString = dbNameParam.optval;

            oci::OCINameCache& nameCache = execEnv->getAgentGlobals().ociNameCache;

            const oci::OCINameCache::Entry& ociNameEntry =
                nameCache.getPDOEntry(connectionString);
            host = ociNameEntry.host();
            port = ociNameEntry.port();
            databaseName = ociNameEntry.database();
        }
    }

    std::string dsnString(dbh->data_source, dbh->data_source_len);
    boost::algorithm::trim_right_if(dsnString,
                                    boost::algorithm::is_space() || boost::algorithm::is_from_range(';', ';'));

    if ( "oci" == driverName ) {
        boost::erase_all(dsnString, " ");
        boost::erase_all(dsnString, "\n");
        boost::erase_all(dsnString, "\t");
    }

    if (dsnString.size() > MAX_URL_LEN_IN_IDENTIFYING_PROPERTY)
        dsnString.resize(MAX_URL_LEN_IN_IDENTIFYING_PROPERTY);
    properties[enum2str(DBExitPoint_URL)] = dsnString;

    DBExitPointDelegate* const delegate =
        static_cast<DBExitPointDelegate*>(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
        );
    if (dbh->methods->get_attribute && delegate->isDBVersionNeeded()) {
        ZValPointerAny attrVal;
        if (dbh->methods->get_attribute(dbh, PDO_ATTR_SERVER_VERSION, attrVal.get() TSRMLS_CC) == 1) {
            ZValPointerString attrStringVal(attrVal.cast<ZValPointerString>());
            if (attrStringVal)
                properties[enum2str(DBExitPoint_VERSION)] = attrStringVal.getStringValue();
        }
    }

    if (!databaseName.empty())
        properties[enum2str(DBExitPoint_DATABASE)] = databaseName;
    if (!host.empty())
        properties[enum2str(DBExitPoint_HOST)] = host;
    if (!port.empty())
        properties[enum2str(DBExitPoint_PORT)] = port;
    if (!a4dbName.empty())
        properties[enum2str(DBExitPoint_VENDOR)] = a4dbName;

    return true;
}

/* }}} */

/* {{{ PDOConstructorInterceptor methods */
static PDOPropertyGenerator pdoPropertyGenerator;

PDOConstructorInterceptor::PDOConstructorInterceptor()
    : t_Base("PDOConstructorInterceptor",
             InterceptorRegistry::PDOConstructorInterceptor_ID,
             appdynamics::pb::Agent::EXIT_DB)
{
}

const appdynamics::pb::SnapshotExitCall* PDOConstructorInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                               TransactionContext* txContext,
                                                                                               Snapshot* snapshot,
                                                                                               const CurrentExitCall* currentExitCall,
                                                                                               uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    const std::string& sqlStatement = std::string("Get DB Connection");
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

bool PDOConstructorInterceptor::getHandle(ZValPointerAny* handlePointer,
                                          const PHPExecEnvironment* execEnv) const
{

    ZValPointerAny handle(ZValPointerAny::share(execEnv->getInvokedObject()));

    if (!handle)
        return false;

    if (handle.getType() != ZValPointer::ZVal_Object)
        return false;

    *handlePointer = handle;
    return true;
}

bool PDOConstructorInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                               const ExitCallInfo* currentExitCallInfo,
                                               const PHPExecEnvironment* execEnv) const
{
    if (currentExitCallInfo) {
        *handleInfo = PDOHandleInfo::create(*currentExitCallInfo);
    } else {
        ExitCallInfo exitCallInfo(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(getExitPointType())
                      ->makeIdentifyingProperties(execEnv, NULL, pdoPropertyGenerator)
        );
        if (!exitCallInfo.isValid())
            return false;
        *handleInfo = PDOHandleInfo::create(exitCallInfo);
    }

    return true;
}

/* }}} */

class PDOCurrentExitCall : public HandleBasedCurrentExitCall
{
public:
    PDOCurrentExitCall(uint64_t sequenceNumber,
                       const PHPExecEnvironment* execEnv,
                       pdo_dbh_t* pdoHandle,
                       const boost::shared_ptr<AExitPointHandleInfo>& handleInfo);

    PDOCurrentExitCall(uint64_t sequenceNumber,
                       const PHPExecEnvironment* execEnv,
                       pdo_dbh_t* pdoHandle);
    virtual ~PDOCurrentExitCall();

private:
    void init();

    const PHPExecEnvironment* const m_execEnv;
    pdo_dbh_t* const m_pdoHandle;
    pdo_error_mode m_savedErrorMode;
    unique_ptr<PHPExecEnvironment::ExceptionState>::type m_savedExceptionState;
};

inline void PDOCurrentExitCall::init()
{
    m_savedErrorMode = m_pdoHandle->error_mode;
    if (m_savedErrorMode != PDO_ERRMODE_EXCEPTION)
    {
        m_savedExceptionState = m_execEnv->saveExceptionState();
        m_pdoHandle->error_mode = PDO_ERRMODE_EXCEPTION;
    }
}

PDOCurrentExitCall::PDOCurrentExitCall(uint64_t sequenceNumber,
                                       const PHPExecEnvironment* execEnv,
                                       pdo_dbh_t* pdoHandle,
                                       const boost::shared_ptr<AExitPointHandleInfo>& handleInfo)
    : HandleBasedCurrentExitCall(sequenceNumber, handleInfo)
    , m_execEnv(execEnv)
    , m_pdoHandle(pdoHandle)
{
    init();
}

PDOCurrentExitCall::PDOCurrentExitCall(uint64_t sequenceNumber,
                                       const PHPExecEnvironment* execEnv,
                                       pdo_dbh_t* pdoHandle)
    : HandleBasedCurrentExitCall(sequenceNumber)
    , m_execEnv(execEnv)
    , m_pdoHandle(pdoHandle)
{
    init();
}

PDOCurrentExitCall::~PDOCurrentExitCall()
{
    // Restore the PDO handle's error_mode to the saved value.
    m_pdoHandle->error_mode = m_savedErrorMode;

    // If the original error mode was PDO_ERRMODE_EXCEPTION
    // we did not save the exception state in the constructor
    // so there is nothing to do here.
    //
    // If the original error mode was PDO_ERRMODE_SILENT, then
    // we still need to emit a warning, because that's what PDO
    // does for implementation errors.

#if PHP_VERSION_ID < 70300
    if (m_savedErrorMode == PDO_ERRMODE_EXCEPTION)
        return;
#else
#error The treatment of PDO_ERRMODE_SILENT in PHP >= 7.3 might have changed. Please check and update this code.
#endif

    // The original error mode was PDO_ERRMODE_WARNING or PDO_ERRMODE_SILENT,
    // so we need to dig the message out of the exception object
    // if there is one and pass the message to the php logging system.
    zval* exceptionRawZVal = m_execEnv->getExceptionObject();
    if (!exceptionRawZVal)
        return;

    ZValPointerAny exception(ZValPointerAny::share(exceptionRawZVal));
    ZValPointerObject exceptionObject(exception.cast<ZValPointerObject>());
    if (!exceptionObject)
        return;

    ZValPointerString messageValue(exceptionObject.findProtectedPropertyByName<ZValPointerString>(m_execEnv,
                                                                                                  MESSAGE_PROPERTY_NAME,
                                                                                                  MESSAGE_PROPERTY_NAME_LEN));

    if (!messageValue)
        return;

    const std::string message(messageValue.getStringValue());
    m_execEnv->errorDocRef(NULL, E_WARNING, message);
}

/* {{{ APDOMethodInterceptor methods */

HandleBasedCurrentExitCall* APDOMethodInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                         uint64_t sequenceNumber,
                                                                         const ZValPointerAny& handle,
                                                                         const boost::shared_ptr<PDOHandleInfo>& handleInfo) const
{
    ZValPointerObject pdoObject(handle.cast<ZValPointerObject>());
    if (!pdoObject)
        return NULL;

#if PHP_VERSION_ID >= 70000
    pdo_dbh_t* pdoHandle = php_pdo_dbh_fetch_inner(pdoObject.getZendObject());
#else
    pdo_dbh_t* pdoHandle = pdoObject.getInternalObject<pdo_dbh_t>(execEnv);
#endif

    if (!pdoHandle)
        return NULL;
    return new PDOCurrentExitCall(sequenceNumber, execEnv, pdoHandle, handleInfo);
}

bool APDOMethodInterceptor::getHandle(ZValPointerAny* handlePointer,
                                      const PHPExecEnvironment* execEnv) const
{
    ZValPointerAny handle(ZValPointerAny::share(execEnv->getInvokedObject()));

    if (!handle)
        return false;

    if (handle.getType() != ZValPointer::ZVal_Object)
        return false;

    *handlePointer = handle;
    return true;
}

/* }}} */

/* {{{ PDOCommitInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PDOCommitInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                          TransactionContext* txContext,
                                                                                          Snapshot* snapshot,
                                                                                          const CurrentExitCall* currentExitCall,
                                                                                          uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    const std::string& sqlStatement = "DB Transaction Commit";
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PDORollbackInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PDORollbackInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                            TransactionContext* txContext,
                                                                                            Snapshot* snapshot,
                                                                                            const CurrentExitCall* currentExitCall,
                                                                                            uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    const std::string& sqlStatement = "DB Transaction Rollback";
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

/* }}} */

void* PDOPrepareInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void PDOPrepareInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                          void* state)
{
    if (execEnv->doesExceptionObjectExist() || execEnv->isReturnValueFalse())
        return;

    ZValPointerObject statementObject(ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerObject>());
    if (!statementObject)
        return;

    ZValPointerObject pdoObject(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());

    boost::shared_ptr<PDOHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<PDOHandleInfo>(pdoObject));

    // If this pdo object is not being tracked, then bail out.
    if (!handleInfo)
        return;

    execEnv->getHandleRegistry().registerHandle(statementObject, handleInfo);
}

/* {{{ PDOQueryInterceptor methods */
PDOQueryInterceptor::PDOQueryInterceptor()
    : t_Base("PDOQueryInterceptor",
             InterceptorRegistry::PDOQueryInterceptor_ID,
             appdynamics::pb::Agent::EXIT_DB)
{
}

CurrentExitCall* PDOQueryInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                            uint64_t sequenceNumber) const
{
    ZValPointerObject pdoObject(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());
    if (!pdoObject)
        return NULL;

#if PHP_VERSION_ID >= 70000
    pdo_dbh_t* pdoHandle = php_pdo_dbh_fetch_inner(pdoObject.getZendObject());
#else
    pdo_dbh_t* pdoHandle = pdoObject.getInternalObject<pdo_dbh_t>(execEnv);
#endif

    if (!pdoHandle)
        return NULL;
    return new PDOCurrentExitCall(sequenceNumber, execEnv, pdoHandle);
}

const appdynamics::pb::SnapshotExitCall* PDOQueryInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                                         TransactionContext* txContext,
                                                                                         Snapshot* snapshot,
                                                                                         const CurrentExitCall* currentExitCall,
                                                                                         uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    if (execEnv->getParamCount() < 1)
        return NULL;
    ZValPointerString param0String(execEnv->getParam<ZValPointerString>(0));
    if (!param0String)
        return NULL;
    std::string sqlStatement(param0String.getStringValue());
    boost::algorithm::trim(sqlStatement);
    if (sqlStatement.empty())
        return NULL;

    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

bool PDOQueryInterceptor::getHandle(ZValPointerAny* handlePointer,
                                    const PHPExecEnvironment* execEnv) const
{
    if (execEnv->doesExceptionObjectExist() || execEnv->isReturnValueFalse())
    {
        // In case of an error, the statement handle will be NULL, but we'll
        // return the PDO handle instead, which should enable the rest of the
        // error reporting mechanisms to work. Hack, but will do for now.
        ZValPointerAny pdoHandle(ZValPointerAny::share(execEnv->getInvokedObject()));

        if (!pdoHandle)
            return false;

        if (pdoHandle.getType() != ZValPointer::ZVal_Object)
            return false;

        *handlePointer = pdoHandle;
        return true;
    }

    ZValPointerAny handle(ZValPointerAny::share(execEnv->getReturnValue()));

    if (!handle)
        return false;

    if (handle.getType() != ZValPointer::ZVal_Object)
        return false;

    *handlePointer = handle;
    return true;
}

bool PDOQueryInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                         const ExitCallInfo* currentExitCallInfo,
                                         const PHPExecEnvironment* execEnv) const
{
    ZValPointerObject pdoObject(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());

    *handleInfo = execEnv->getHandleRegistry().getHandleInfo<PDOHandleInfo>(pdoObject);

    // If this pdo object is not being tracked, then bail out.
    if (!(*handleInfo))
        return false;
    return true;
}

/* }}} */

/* {{{ PDOExecInterceptor methods */

PDOExecInterceptor::PDOExecInterceptor()
    : t_Base("PDOExecInterceptor",
             InterceptorRegistry::PDOExecInterceptor_ID,
             appdynamics::pb::Agent::EXIT_DB)
{
}

HandleBasedCurrentExitCall* PDOExecInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                      uint64_t sequenceNumber,
                                                                      const ZValPointerAny& handle,
                                                                      const boost::shared_ptr<PDOHandleInfo>& handleInfo) const
{
    ZValPointerObject pdoObject(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());
    if (!pdoObject)
        return NULL;

#if PHP_VERSION_ID >= 70000
    pdo_dbh_t* pdoHandle = php_pdo_dbh_fetch_inner(pdoObject.getZendObject());
#else
    pdo_dbh_t* pdoHandle = pdoObject.getInternalObject<pdo_dbh_t>(execEnv);
#endif

    if (!pdoHandle)
        return NULL;
    return new PDOCurrentExitCall(sequenceNumber, execEnv, pdoHandle, handleInfo);
}

bool PDOExecInterceptor::getHandle(ZValPointerAny* handlePointer,
                                   const PHPExecEnvironment* execEnv) const
{
    ZValPointerAny handle(ZValPointerAny::share(execEnv->getInvokedObject()));

    if (!handle)
        return false;

    if (handle.getType() != ZValPointer::ZVal_Object)
        return false;

    *handlePointer = handle;
    return true;
}

const appdynamics::pb::SnapshotExitCall* PDOExecInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                                        TransactionContext* txContext,
                                                                                        Snapshot* snapshot,
                                                                                        const CurrentExitCall* currentExitCall,
                                                                                        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    if (execEnv->getParamCount() < 1)
        return NULL;
    ZValPointerString param0String(execEnv->getParam<ZValPointerString>(0));
    if (!param0String)
        return NULL;
    std::string sqlStatement(param0String.getStringValue());
    boost::algorithm::trim(sqlStatement);
    if (sqlStatement.empty())
        return NULL;

    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PDOStatementExecInterceptor methods */

PDOStatementExecInterceptor::PDOStatementExecInterceptor()
    : t_Base("PDOStatementExecInterceptor",
                           InterceptorRegistry::PDOStatementExecInterceptor_ID)
{
}

HandleBasedCurrentExitCall* PDOStatementExecInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                               uint64_t sequenceNumber,
                                                                               const ZValPointerAny& handle,
                                                                               const boost::shared_ptr<PDOHandleInfo>& handleInfo) const
{

    ZValPointerObject pdoStatementObject(handle.cast<ZValPointerObject>());
    if (!pdoStatementObject)
        return NULL;

#if PHP_VERSION_ID >= 70000
    pdo_stmt_t* pdoStatementHandle = php_pdo_stmt_fetch_object(pdoStatementObject.getZendObject());
#else
    pdo_stmt_t* pdoStatementHandle = pdoStatementObject.getInternalObject<pdo_stmt_t>(execEnv);
#endif

    if (!pdoStatementHandle)
        return NULL;

    pdo_dbh_t* pdoHandle = pdoStatementHandle->dbh;
    if (!pdoHandle)
        return NULL;

    return new PDOCurrentExitCall(sequenceNumber, execEnv, pdoHandle, handleInfo);
}

const appdynamics::pb::SnapshotExitCall* PDOStatementExecInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                                                 TransactionContext* txContext,
                                                                                                 Snapshot* snapshot,
                                                                                                 const CurrentExitCall* currentExitCall,
                                                                                                 uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    std::string sqlStatement;
    GetSQL_Result const getSQLResult = getSQLStatement(execEnv, &sqlStatement);
    if (getSQLResult == GetSQL_FAILED)
        return NULL;

    boost::algorithm::trim(sqlStatement);
    if (sqlStatement.empty())
        return NULL;

    if (getSQLResult == GetSQL_NOBIND)
        return snapshot->getDBCalls().add(txContext,
                                          currentExitCall,
                                          sqlStatement,
                                          timeTakenMS);
    appdynamics::pb::SnapshotExitCall* const dbCall =
        snapshot->getDBCalls().addWithBoundParams(txContext,
                                                  currentExitCall,
                                                  sqlStatement,
                                                  timeTakenMS);
    // dbCall will be null if we hit the limit of unique sql calls.
    if (!dbCall)
        return NULL;
    appdynamics::pb::BoundParameters* const boundParamsProtobuf =
            dbCall->mutable_boundparameters();
    boundParamsProtobuf->set_type(appdynamics::pb::BoundParameters::SUBSTITUTED);
    return dbCall;
}

PDOStatementExecInterceptor::GetSQL_Result
PDOStatementExecInterceptor::getSQLStatement(const PHPExecEnvironment* phpExecEnv,
                                             std::string* sqlStatement) const
{
    const zval* object = phpExecEnv->getInvokedObject();
    ZValPointerObject statement(ZValPointerAny::share(const_cast<zval*>(object)).cast<ZValPointerObject>());
    if (!statement)
        return GetSQL_FAILED;

#if PHP_VERSION_ID >= 70000
    // we'll leave the ZvalPointerAny creation above to keep the refcount right.
    pdo_stmt_t* stmt = Z_PDO_STMT_P(object);
#else
    pdo_stmt_t* stmt = statement.getInternalObject<pdo_stmt_t>(phpExecEnv);
#endif

    if (!stmt)
        return GetSQL_FAILED;

    BOOST_ASSERT(sqlStatement);

    const boost::shared_ptr<SnapshotManager>& snapshotSvc =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getSnapshotManager();

    const DelayedFunctions::FunctionTable& delayedFunctions =
        phpExecEnv->getAgentGlobals().delayed_functions;
    const auto & pdoParseParams =
        delayedFunctions.get_pdo_parse_params();
    if (!(pdoParseParams.isValid()))
        return GetSQL_FAILED;

    // Save exception state in case pdo_parse_params() throws an implementation
    // error. We'll eat it here, but there will be another copy of it from
    // PDOStatement->execute() or similar calling pdo_parse_params().

    pdo_error_mode savedErrorMode;
    unique_ptr<PHPExecEnvironment::ExceptionState>::type savedExceptionState;
    pdo_dbh_t* pdoHandle = stmt->dbh;
    BOOST_ASSERT(pdoHandle);

    savedErrorMode = pdoHandle->error_mode;
    savedExceptionState = phpExecEnv->saveExceptionState();
    pdoHandle->error_mode = PDO_ERRMODE_EXCEPTION;

    char* fullQuery = NULL;
#if PHP_VERSION_ID >= 70000
    size_t fullQueryLen = 0;
#else
    int fullQueryLen = 0;
#endif

    int ret = phpExecEnv->callWithTSRMContext(pdoParseParams,
                                              stmt,
                                              stmt->query_string,
                                              stmt->query_stringlen,
                                              &fullQuery,
                                              &fullQueryLen);

    pdoHandle->error_mode = savedErrorMode;

    if (ret == 0) {
        // no changes were made, fullQuery not allocated, return query string
        sqlStatement->assign(stmt->query_string, stmt->query_stringlen);
        return GetSQL_NOBIND;
    } else if (ret == 1) {
        // query was expanded, return fullQuery
        sqlStatement->assign(fullQuery, fullQueryLen);
        efree(fullQuery);
        return GetSQL_SUBSTITUTED;
    } else {
        // some error occurred
        return GetSQL_FAILED;
    }
}

/* }}} */

// vim: set fdm=marker:
