/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */

#ifndef __mysql_exitpoint_h
#define __mysql_exitpoint_h

#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "exitcall_detail.h"
#include "sqlutil.h"

typedef struct _php_mysql_conn php_mysql_conn;

/**
 * Handle info class that holds data associated with a particular MySQL
 * connection, including host, port, and database.
 * Instances of this class can be created only via the static create() method.
 */
class MySQLHandleInfo : public AExitPointHandleInfo
{
        friend class HandleRegistry;
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<MySQLHandleInfo> create(const ExitCallInfo& exitCallInfo,
                                                                const PHPExecEnvironment* execEnv)
        {
            return boost::make_shared<MySQLHandleInfo>(exitCallInfo, execEnv);
        }

        const std::string& getDatabase() const { return m_database; }
        void setDatabase(const std::string& database);

        const StringMap& getProperties() const { return m_properties; }
        void setProperties(const StringMap& properties) { m_properties = properties; }

    protected:
        inline MySQLHandleInfo(const ExitCallInfo& exitCallInfo,
                               const PHPExecEnvironment* execEnv)
            : AExitPointHandleInfo(exitCallInfo, &typeTag)
            , m_execEnv(execEnv) {}

    private:
        const PHPExecEnvironment* const m_execEnv;
        std::string m_hostAndPort;
        std::string m_database;

        // Original properties created by the MySQL property generator
        StringMap m_properties;
};

/*
 * This interceptor is not an exit call one (for now), because complete
 * resolution cannot be made until mysql_select_db() is called (if DB is part of
 * the discovery config). Instead, it does partial resolution and stores it in
 * the instances of MySQLHandleInfo.
 */
class MySQLConnectInterceptor : public ExitCallDetailHelper<HandleInitExitPointInterceptor<MySQLConnectInterceptor>,
                                                            MySQLConnectInterceptor,
                                                            appdynamics::pb::Agent::EXIT_DB>
{
        friend class HandleInitExitPointInterceptor<MySQLConnectInterceptor>;
        // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
        // getStatementType.
        friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<MySQLConnectInterceptor>,
                                          MySQLConnectInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<HandleInitExitPointInterceptor<MySQLConnectInterceptor>,
                                     MySQLConnectInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;
    public:
        MySQLConnectInterceptor();
        virtual ~MySQLConnectInterceptor() {}

        virtual bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const;

        virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                    const ExitCallInfo* currentExitCallInfo,
                                    const PHPExecEnvironment* execEnv) const;

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* phpExecEnv);
    private:
        /**
            Called by base class template to create SnapshotExitCall and ensure that
            any newly created SnapshotExitCall is added to the specified Snapshot.

            Returns NULL if a SnapshotExitCall could not be created.  The total number
            of SnapshotExitCall's is limited by a setting from the SnapshotManager.
        */
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                            TransactionContext* txContext,
                                                                            Snapshot*,
                                                                            const CurrentExitCall*,
                                                                            uint64_t timeTakenMS) const;
        /**
            Called by base class template to get the sql type string for the specified
            query.
         */
        inline std::string getSQLType(const std::string&) const { return ::kSqlConnect; }

        /**
            Called by class template to get the type of SQL statement this interceptor
            discovers.
         */
        inline const char* getStatementType() const { return NULL; }
};

class MySQLCloseInterceptor : public AMethodInterceptor {
    public:
        MySQLCloseInterceptor()
            : AMethodInterceptor("MySQLCloseInterceptor",
                                 InterceptorRegistry::MySQLCloseInterceptor_ID)
        {
        }

        virtual ~MySQLCloseInterceptor() {}

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        int getInterceptorID() const;
        inline bool needParamsInOnMethodBegin() const { return true; }
        inline bool shouldCallOnMethodEnd() const { return true; }
        bool needReturnValueInOnMethodEnd() const { return true; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
};


class MySQLSelectDBInterceptor : public AMethodInterceptor {
    public:
        MySQLSelectDBInterceptor();
        virtual ~MySQLSelectDBInterceptor() { }

    protected:
        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        bool needParamsInOnMethodBegin() const { return true; }
        bool shouldCallOnMethodEnd() const { return true; }
        bool needReturnValueInOnMethodEnd() const { return false; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
};

class MySQLQueryInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<MySQLQueryInterceptor,
                                                                                           MySQLHandleInfo>,
                                                          MySQLQueryInterceptor,
                                                          appdynamics::pb::Agent::EXIT_DB>
{
    typedef ExitCallDetailHelper<AHandleBasedExitPointInterceptor<MySQLQueryInterceptor,
                                                                  MySQLHandleInfo>,
                                 MySQLQueryInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<MySQLQueryInterceptor,
                                                                       MySQLHandleInfo>,
                                      MySQLQueryInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    public:
        MySQLQueryInterceptor();
        virtual ~MySQLQueryInterceptor() { }

    protected:
        virtual bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const;

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* phpExecEnv);
    private:
        /**
            Called by base class template to create SnapshotExitCall and ensure that
            any newly created SnapshotExitCall is added to the specified Snapshot.

            Returns NULL if a SnapshotExitCall could not be created.  The total number
            of SnapshotExitCall's is limited by a setting from the SnapshotManager.

            If the sql string passed to mysql_query can not be extracted
            from the parameters passed mysql_query, this method will return NULL.
         */
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                            TransactionContext* txContext,
                                                                            Snapshot*,
                                                                            const CurrentExitCall*,
                                                                            uint64_t timeTakenMS) const;
        /**
            Called by base class template to get the sql type string for the specified
            query.
         */
        inline std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }

        /**
            Called by class template to get the type of SQL statement this interceptor
            discovers.
         */
        inline const char* getStatementType() const { return ::kSqlString; }
};

#endif /* __mysql_exitpoint_h */
