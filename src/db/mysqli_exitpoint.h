/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __mysqli_exitpoint_h
#define __mysqli_exitpoint_h

#include "handle_tracking_exitpoint.h"

#include "emalloc_allocator.h"
#include "exitcall_detail.h"
#include "method_selector.h"
#include "sqlutil.h"
#include "safe_module_shutdown.h"

namespace mysqli
{
    /**
     * Handle info class that holds data associated with a particular MySQL
     * connection, including host, port, and database.
     * Instances of this class can be created only via the static create() method.
     */
    class MySQLIHandleInfo : public AExitPointHandleInfo
    {
            friend class ::HandleRegistry;
            static const TypeTag typeTag;
        public:
            template <class T, class Arg1, class... Args>
                friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
            static boost::shared_ptr<MySQLIHandleInfo> create(const PHPExecEnvironment* execEnv,
                                                              bool firstParamIsLinkHandle,
                                                              const ZValPointerAny& thisZVal);

            const std::string& getHost() const { return m_host; }
            unsigned long getPort() const { return m_port; }
            const std::string& getDatabase() const { return m_database; }
            void setDatabase(const std::string& database);

        protected:
            MySQLIHandleInfo(const ExitCallInfo& exitCallInfo,
                             const std::string& hostName,
                             unsigned long portNumber,
                             const std::string& databaseName,
                             const PHPExecEnvironment* execEnv,
                             const StringMap& properties);

        private:
            std::string m_host;
            unsigned long m_port;
            std::string m_database;
            const PHPExecEnvironment* const m_execEnv;
            StringMap m_properties;
    };

    class StatementHandleInfo : public AExitPointHandleInfo
    {
        friend class ::HandleRegistry;
        static const TypeTag typeTag;
        public:
            template <class T, class Arg1, class... Args>
                friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);

            const std::string& getQuery() const { return m_query; }
            void setQuery(const std::string& query) { m_query = query; }
            std::vector<SafeModuleShutdownWrapper<ZValPointerAny>>& getBoundParams() { return m_boundParams; }

        protected:
            StatementHandleInfo(const ExitCallInfo& exitCallInfo,
                                const std::string& query);
            StatementHandleInfo(const ExitCallInfo& exitCallInfo);

        private:
            std::string m_query;
            std::vector<SafeModuleShutdownWrapper<ZValPointerAny>> m_boundParams;
    };

    /**
        Interceptor class for mysqli_connect and mysqli::__construct.
     */
    class AConnectInterceptor : public ExitCallDetailHelper<HandleInitExitPointInterceptor<AConnectInterceptor>,
                                                            AConnectInterceptor,
                                                            appdynamics::pb::Agent::EXIT_DB>
                              , protected AMethodInvocationSelector
    {
        friend class HandleInitExitPointInterceptor<AConnectInterceptor>;
        // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
        // getStatementType.
        friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<AConnectInterceptor>,
                                          AConnectInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<HandleInitExitPointInterceptor<AConnectInterceptor>,
                                     AConnectInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;
        protected:
            AConnectInterceptor(const char* interceptorName,
                                InterceptorRegistry::ID interceptorID,
                                AMethodInvocationSelector::CallableType type);
            virtual ~AConnectInterceptor() { }

            virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                           uint64_t sequenceNumber) const;

            virtual bool getHandle(ZValPointerAny* handlePointer,
                                   const PHPExecEnvironment* execEnv) const;

            virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                        const ExitCallInfo* currentExitCallInfo,
                                        const PHPExecEnvironment* execEnv) const;
        private:
            /**
                Called by base class template to create SnapshotExitCall and ensure that
                any newly created SnapshotExitCall is added to the specified Snapshot.

                Returns NULL if a SnapshotExitCall could not be created.  The total number
                of SnapshotExitCall's is limited by a setting from the SnapshotManager.
            */
            const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                TransactionContext*,
                                                                                Snapshot*,
                                                                                const CurrentExitCall*,
                                                                                uint64_t timeTakenMS) const;
            /**
                Called by base class template to get the sql type string for the specified
                query.
             */
            inline std::string getSQLType(const std::string&) const { return ::kSqlConnect; }

            /**
                Called by class template to get the type of SQL statement this interceptor
                discovers.
             */
            inline const char* getStatementType() const { return NULL; }
    };

    class ConnectInterceptor : public AConnectInterceptor
    {
        public:
            ConnectInterceptor();
    };

    class ConstructorInterceptor : public AConnectInterceptor
    {
        public:
            ConstructorInterceptor();
    };

    class RealConnectInterceptor : public AConnectInterceptor
    {
        public:
            RealConnectInterceptor();
    };

    class RealConnectMethodInterceptor : public AConnectInterceptor
    {
        public:
            RealConnectMethodInterceptor();
    };

    /**
        Interceptor base class used to update handle information.  Provides
        an empty implementation of onCallableBegin and ensures that onCallableEnd
        is called and receives parameters and the return value.
     */
    class AHandleInfoMaintenanceInterceptor : public AMethodInterceptor
                                            , protected AMethodInvocationSelector
    {
        protected:
            AHandleInfoMaintenanceInterceptor(const char* interceptorName,
                                              InterceptorRegistry::ID interceptorID,
                                              AMethodInvocationSelector::CallableType type);
        public:
            virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

            bool needParamsInOnMethodBegin() const { return true; }
            bool shouldCallOnMethodEnd() const { return true; }
            bool needReturnValueInOnMethodEnd() const { return true; }

            inline bool isActive(const PHPExecEnvironment* execEnv) const
            {
                return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
            }
    };

    /**
        Interceptor for mysqli_close and mysqli::close.
     */
    class ACloseInterceptor : public AHandleInfoMaintenanceInterceptor
    {
        protected:
            ACloseInterceptor(const char* interceptorName,
                              InterceptorRegistry::ID interceptorID,
                              AMethodInvocationSelector::CallableType type);
        public:
            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
    };

    class CloseInterceptor : public ACloseInterceptor
    {
        public:
            CloseInterceptor();
    };

    class CloseMethodInterceptor : public ACloseInterceptor
    {
        public:
            CloseMethodInterceptor();
    };

    /**
        Interceptor for mysqli_select_db and mysqli::select_db.
     */
    class ASelectDBInterceptor : public AHandleInfoMaintenanceInterceptor
    {
        protected:
            ASelectDBInterceptor(const char* interceptorName,
                                 InterceptorRegistry::ID interceptorID,
                                 AMethodInvocationSelector::CallableType type);
        public:
            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
    };

    class SelectDBInterceptor : public ASelectDBInterceptor
    {
        public:
            SelectDBInterceptor();
    };

    class SelectDBMethodInterceptor : public ASelectDBInterceptor
    {
        public:
            SelectDBMethodInterceptor();
    };

    /**
        Exit point interceptor for mysqli_query, mysqli::query,
        mysqli_real_query, and mysqli::real_query.
     */
    class AQueryInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<AQueryInterceptor,
                                                                                           MySQLIHandleInfo>,
                                                          AQueryInterceptor,
                                                          appdynamics::pb::Agent::EXIT_DB>
                            , protected AMethodInvocationSelector
    {
            // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
            // getStatementType.
            friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<AQueryInterceptor,
                                                                               MySQLIHandleInfo>,
                                              AQueryInterceptor,
                                              appdynamics::pb::Agent::EXIT_DB>;
            typedef ExitCallDetailHelper<AHandleBasedExitPointInterceptor<AQueryInterceptor,
                                                                          MySQLIHandleInfo>,
                                         AQueryInterceptor,
                                         appdynamics::pb::Agent::EXIT_DB> t_Base;
        public:
            AQueryInterceptor(const char* interceptorName,
                              InterceptorRegistry::ID interceptorID,
                              AMethodInvocationSelector::CallableType type);

            virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                      uint64_t sequenceNumber,
                                                                      const ZValPointerAny& handle,
                                                                      const boost::shared_ptr<MySQLIHandleInfo>& handleInfo) const;

            virtual bool getHandle(ZValPointerAny* handlePointer,
                                   const PHPExecEnvironment* execEnv) const;
        private:
            /**
                Called by base class template to create SnapshotExitCall and ensure that
                any newly created SnapshotExitCall is added to the specified Snapshot.

                Returns NULL if a SnapshotExitCall could not be created.  The total number
                of SnapshotExitCall's is limited by a setting from the SnapshotManager.

                If the sql string passed to mysqli_query, mysqli_real_query,
                mysqli::query, or mysqli::real_query can not be extracted
                from the parameters passed to the query function/method, this method
                will return NULL.
            */
            const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                TransactionContext*,
                                                                                Snapshot*,
                                                                                const CurrentExitCall*,
                                                                                uint64_t timeTakenMS) const;
            /**
                Called by base class template to get the sql type string for the specified
                query.
             */
            inline std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }

            /**
                Called by class template to get the type of SQL statement this interceptor
                discovers.
             */
            inline const char* getStatementType() const { return ::kSqlString; }
    };

    class QueryInterceptor : public AQueryInterceptor
    {
        public:
            QueryInterceptor();
    };

    class QueryMethodInterceptor : public AQueryInterceptor
    {
        public:
            QueryMethodInterceptor();
    };

    /**
        Exit point interceptor for mysqli_commit and mysqli::commit.
     */
    class ACommitInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<ACommitInterceptor,
                                                                                            MySQLIHandleInfo>,
                                                           ACommitInterceptor,
                                                           appdynamics::pb::Agent::EXIT_DB>
                             , protected AMethodInvocationSelector
    {
            // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
            // getStatementType.
            friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<ACommitInterceptor,
                                                                               MySQLIHandleInfo>,
                                              ACommitInterceptor,
                                              appdynamics::pb::Agent::EXIT_DB>;
            typedef ExitCallDetailHelper<AHandleBasedExitPointInterceptor<ACommitInterceptor,
                                                                          MySQLIHandleInfo>,
                                         ACommitInterceptor,
                                         appdynamics::pb::Agent::EXIT_DB> t_Base;
        public:
            ACommitInterceptor(const char* interceptorName,
                               InterceptorRegistry::ID interceptorID,
                               AMethodInvocationSelector::CallableType type);

            virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                      uint64_t sequenceNumber,
                                                                      const ZValPointerAny& handle,
                                                                      const boost::shared_ptr<MySQLIHandleInfo>& handleInfo) const;

            virtual bool getHandle(ZValPointerAny* handlePointer,
                                   const PHPExecEnvironment* execEnv) const;
        private:
             /**
                Called by base class template to create SnapshotExitCall and ensure that
                any newly created SnapshotExitCall is added to the specified Snapshot.

                Returns NULL if a SnapshotExitCall could not be created.  The total number
                of SnapshotExitCall's is limited by a setting from the SnapshotManager.
            */
            const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                TransactionContext*,
                                                                                Snapshot*,
                                                                                const CurrentExitCall*,
                                                                                uint64_t timeTakenMS) const;
            /**
                Called by base class template to get the sql type string for the specified
                query.
             */
            inline std::string getSQLType(const std::string& sql) const { return ::kSqlCommit; }

            /**
                Called by class template to get the type of SQL statement this interceptor
                discovers.
             */
            inline const char* getStatementType() const { return NULL; }
    };

    class CommitInterceptor : public ACommitInterceptor
    {
        public:
            CommitInterceptor();
    };

    class CommitMethodInterceptor : public ACommitInterceptor
    {
        public:
            CommitMethodInterceptor();
    };

    /**
        Interceptor for mysqli_stmt_init and mysqli::stmt_init.
     */
    class AStatementInitInterceptor : public AHandleInfoMaintenanceInterceptor
    {
        protected:
            AStatementInitInterceptor(const char* interceptorName,
                                      InterceptorRegistry::ID interceptorID,
                                      AMethodInvocationSelector::CallableType type);
        public:
            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
    };

    class StatementInitInterceptor : public AStatementInitInterceptor
    {
        public:
            StatementInitInterceptor();
    };

    class StatementInitMethodInterceptor : public AStatementInitInterceptor
    {
        public:
            StatementInitMethodInterceptor();
    };

    /**
        Interceptor for mysqli_prepare and mysqli::prepare.
     */
    class APrepareInterceptor : public AHandleInfoMaintenanceInterceptor
    {
        protected:
            APrepareInterceptor(const char* interceptorName,
                                InterceptorRegistry::ID interceptorID,
                                AMethodInvocationSelector::CallableType type);
        public:
            virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
    };

    class PrepareInterceptor : public APrepareInterceptor
    {
        public:
            PrepareInterceptor();
    };

    class PrepareMethodInterceptor : public APrepareInterceptor
    {
        public:
            PrepareMethodInterceptor();
    };

    /**
        Interceptor for mysqli_stmt_prepare and mysqli_stmt::prepare.
     */
    class AStatementPrepareInterceptor : public AHandleInfoMaintenanceInterceptor
    {
        protected:
            AStatementPrepareInterceptor(const char* interceptorName,
                                         InterceptorRegistry::ID interceptorID,
                                         AMethodInvocationSelector::CallableType type);
        public:
            virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
    };

    class StatementPrepareInterceptor : public AStatementPrepareInterceptor
    {
        public:
            StatementPrepareInterceptor();
    };

    class StatementPrepareMethodInterceptor : public AStatementPrepareInterceptor
    {
        public:
            StatementPrepareMethodInterceptor();
    };

    /**
        Interceptor for mysqli_stmt_execute and mysqli_stmt::execute.
     */
    class AStatementExecuteInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<AStatementExecuteInterceptor,
                                                                                                      StatementHandleInfo>,
                                                                     AStatementExecuteInterceptor,
                                                                     appdynamics::pb::Agent::EXIT_DB>
                                       , protected AMethodInvocationSelector
    {
            // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
            // getStatementType.
            friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<AStatementExecuteInterceptor,
                                                                               StatementHandleInfo>,
                                              AStatementExecuteInterceptor,
                                              appdynamics::pb::Agent::EXIT_DB>;
            typedef ExitCallDetailHelper<AHandleBasedExitPointInterceptor<AStatementExecuteInterceptor,
                                                                          StatementHandleInfo>,
                                         AStatementExecuteInterceptor,
                                         appdynamics::pb::Agent::EXIT_DB> t_Base;
        public:
            AStatementExecuteInterceptor(const char* interceptorName,
                                         InterceptorRegistry::ID interceptorID,
                                         AMethodInvocationSelector::CallableType type);

            virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                      uint64_t sequenceNumber,
                                                                      const ZValPointerAny& handle,
                                                                      const boost::shared_ptr<StatementHandleInfo>& handleInfo) const;

            virtual bool getHandle(ZValPointerAny* handlePointer,
                                   const PHPExecEnvironment* execEnv) const;
        private:
             /**
                Called by base class template to create SnapshotExitCall and ensure that
                any newly created SnapshotExitCall is added to the specified Snapshot.

                Returns NULL if a SnapshotExitCall could not be created.  The total number
                of SnapshotExitCall's is limited by a setting from the SnapshotManager.

                If the sql string for the prepared statement is not available,
                this method will return NULL.
            */
            const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                TransactionContext* txContext,
                                                                                Snapshot*,
                                                                                const CurrentExitCall*,
                                                                                uint64_t timeTakenMS) const;
            /**
                Called by base class template to get the sql type string for the specified
                query.
             */
            inline std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }

            /**
                Called by class template to get the type of SQL statement this interceptor
                discovers.
             */
            inline const char* getStatementType() const { return ::kSqlPreparedStatement; }

    };

    class StatementExecuteInterceptor : public AStatementExecuteInterceptor
    {
        public:
            StatementExecuteInterceptor();
    };

    class StatementExecuteMethodInterceptor : public AStatementExecuteInterceptor
    {
        public:
            StatementExecuteMethodInterceptor();
    };

    /**
        Interceptor for mysqli_stmt_bind_param and mysqli_stmt::bind_param.
     */
    class AStatementBindParamInterceptor : public AHandleInfoMaintenanceInterceptor
    {
        protected:
            AStatementBindParamInterceptor(const char* interceptorName,
                                           InterceptorRegistry::ID interceptorID,
                                           AMethodInvocationSelector::CallableType type);
        public:
            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);

    };

    class StatementBindParamInterceptor : public AStatementBindParamInterceptor
    {
        public:
            StatementBindParamInterceptor();
    };

    class StatementBindParamMethodInterceptor : public AStatementBindParamInterceptor
    {
        public:
            StatementBindParamMethodInterceptor();
    };
}

#endif
