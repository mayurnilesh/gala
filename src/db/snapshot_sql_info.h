/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __snapshot_sql_info_h
#define __snapshot_sql_info_h

#include <string>
#include <boost/functional/hash.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>

#include "common.h"
#include "backend_identifier.h"
#include "snapshot_exitcall.h"

namespace appdynamics
{
    namespace pb
    {
        class SnapshotExitCall;
        class Snapshot;
    }
}

class Snapshot;
class SnapshotManager;
class CurrentExitCall;
class TransactionContext;

namespace Private
{

    /**
        Value class for keys in the map in the SnapshotDBCalls.  Each key
        in the map is a combination of a backend identifier and a sql string.

        Since the BackendIdentifiers are interned for the lifetime of a php
        program the pointer to the backend identifier is hashed and compared.
     */
    class SnapshotDBCalls_Key
    {
        friend struct boost::hash<SnapshotDBCalls_Key>;
    public:
        SnapshotDBCalls_Key(const BackendIdentifierPtr& backendID,
            const std::string& sqlString);
        SnapshotDBCalls_Key(const SnapshotDBCalls_Key& other);

        SnapshotDBCalls_Key& operator=(const SnapshotDBCalls_Key& other);
        bool operator==(const SnapshotDBCalls_Key& other) const;
    private:
        BackendIdentifierPtr m_backendID;
        std::string m_sqlString;
    };

    inline SnapshotDBCalls_Key::SnapshotDBCalls_Key(const BackendIdentifierPtr& backendID,
                                                    const std::string& sqlString)
        : m_backendID(backendID)
        , m_sqlString(sqlString)
    {
    }

    inline SnapshotDBCalls_Key::SnapshotDBCalls_Key(const SnapshotDBCalls_Key& other)
        : m_backendID(other.m_backendID)
        , m_sqlString(other.m_sqlString)
    {
    }

    inline SnapshotDBCalls_Key& SnapshotDBCalls_Key::operator=(const SnapshotDBCalls_Key& other)
    {
        m_backendID = other.m_backendID;
        m_sqlString = other.m_sqlString;
        return *this;
    }

    inline bool SnapshotDBCalls_Key::operator==(const SnapshotDBCalls_Key& other) const
    {
        if (m_backendID != other.m_backendID)
            return false;
        if (m_sqlString != other.m_sqlString)
            return false;
        return true;
    }
}

namespace boost
{
    /**
        Specialization of std::hash for Private::SnapshotDBCalls_Key.
     */
    template<> struct hash<Private::SnapshotDBCalls_Key>
    {
        inline size_t operator()(const Private::SnapshotDBCalls_Key& k) const
        {
            size_t seed = 0;
            // Since backend identifiers are interned for the lifetime of a php
            // program, we can hash a pointer here instead of all the identifying
            // properties.
            boost::hash_combine(seed, k.m_backendID.get());
            boost::hash_combine(seed, k.m_sqlString);
            return seed;
        }
    };
}

/**
    Class to hold all the database calls in a snapshot.

    This can can only be instantiated by the Snapshot class.
 */
class SnapshotDBCalls
{
    // Allow snapshot to construct instances of this class.
    friend class Snapshot;
    typedef Private::SnapshotDBCalls_Key Key;
public:
    /**
        Add a new SnapshotExitCall to the list of database calls and allow the
        caller to add bound parameter information to the new SnapshotExitCall.
        @return A new SnapshotExitCall or null if the maximum number of SnapshotExitCall's
        per snapshot has been reached.
     */
    appdynamics::pb::SnapshotExitCall* addWithBoundParams(TransactionContext* txContext,
                                                          const CurrentExitCall* exitCall,
                                                          const std::string& sqlString,
                                                          const long timeTakenMS);

    /**
        Update the aggregated metrics for the specified backendID/SQL pair.
        Creates a new SnapshotExitCall if needed.

        @return the SnapshotExitCall containing the metrics that were incremented or
        null if the maximum number of SnapshotExitCall's per Snapshot has been reached and
        the specified backendID/SQL pair has not yet been added to the Snapshot.
     */
    appdynamics::pb::SnapshotExitCall* add(TransactionContext* txContext,
                                           const CurrentExitCall* exitCall,
                                           const std::string& sqlString,
                                           const long timeTakenMS);

    /**
        Destructively transfer SQL call information to the specified Snapshot protobuf.
     */
    void mergeIntoSnapshot(appdynamics::pb::Snapshot* snapshot);
private:
    SnapshotDBCalls(const SnapshotManager* snapshotManager);
    ~SnapshotDBCalls();

    const SnapshotManager* const m_snapshotManager;
    boost::unordered_map<Key, appdynamics::pb::SnapshotExitCall*> m_normalizedCalls;
    std::vector<appdynamics::pb::SnapshotExitCall*> m_calls;

};








#endif
