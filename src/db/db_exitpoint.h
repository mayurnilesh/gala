/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __db_exitpoint_h
#define __db_exitpoint_h

#include <boost/utility.hpp>
#include "enumfactory.h"
#include "exitpoint.h"
#include "PHPAgentProtobufs.pb.h"

struct _zend_agent_globals;
class InterceptionEngine;

#define DB_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL(XX) \
    XX(DBExitPoint_HOST,"HOST",) \
    XX(DBExitPoint_PORT,"PORT",) \
    XX(DBExitPoint_DATABASE,"DATABASE",) \
    XX(DBExitPoint_URL,"URL",) \
    XX(DBExitPoint_VENDOR,"VENDOR",) \
    XX(DBExitPoint_VERSION,"VERSION",)

DECLARE_ENUM(DBExitPointIndentifyingPropertyNames, DB_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

static const char* const DB2_VENDOR = "DB2";
static const char* const MYSQL_VENDOR = "MYSQL";
static const char* const ORACLE_VENDOR = "ORACLE";
static const char* const POSTGRESQL_VENDOR = "POSTGRESQL";
static const char* const SQLSERVER_VENDOR = "SQLSERVER";
static const char* const SYBASE_VENDOR = "SYBASE";

// True global
extern int mysqlLinkType;
extern int mysqlPersistentLinkType;
extern int pgsqlLinkType;
extern int pgsqlPersistentLinkType;
extern int pgsqlResultType;

/**
    Exit point delegate that sets up the interceptors to
    detect database backends.
 */
class DBExitPointDelegate : public AExitPointDelegate
{
public:
    DBExitPointDelegate(InterceptionEngine* interceptEngine,
                        TransactionMonitor* txMonitor);

    appdynamics::pb::Agent::ExitPointType getExitPointType() const
    {
        return appdynamics::pb::Agent::EXIT_DB;
    }

    void configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                   const struct _zend_agent_globals* agentGlobals);

    inline bool isDBVersionNeeded() const
    {
        return m_needDBVersion;
    }

protected:
    void applyRules(const struct _zend_agent_globals* agentGlobals);

    bool processMatchRule(const PHPExecEnvironment* execEnv,
                          const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                          const StringMap& properties) const;

    void processNamingRule(const PHPExecEnvironment* execEnv,
                           const appdynamics::pb::Agent::BackendRule& backendRule,
                           const StringMap& properties,
                           StringMap& identifyingProperties) const;

private:
    bool m_needDBVersion;

    static bool s_didApplyRules;

    bool checkIfDBVersionNeeded() const;
};

#endif
