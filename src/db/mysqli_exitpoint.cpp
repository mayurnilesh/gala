/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "mysqli_exitpoint.h"
#include "db_exitpoint.h"
#include "sqlutil.h"
#include "transactions.h"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <algorithm>

#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION

#if PHP_VERSION_ID >= 50300
#include <ext/mysqli/php_mysqli_structs.h>
#else
#ifdef MYSQL_UNIX_ADDR
#undef MYSQL_UNIX_ADDR
#endif
#include <ext/mysqli/php_mysqli.h>
#endif

#include <stdio.h>

namespace mysqli
{
    static const char SERVER_INFO_PROPERTY_NAME[] = "server_info";
    static size_t SERVER_INFO_PROPERTY_NAME_LEN = sizeof(SERVER_INFO_PROPERTY_NAME) - 1;

    const AHandleInfo::TypeTag MySQLIHandleInfo::typeTag;

    boost::shared_ptr<MySQLIHandleInfo> MySQLIHandleInfo::create(const PHPExecEnvironment* execEnv,
                                                                 bool firstParamIsLinkHandle,
                                                                 const ZValPointerAny& thisZVal)
    {
        StringMap properties;
        unsigned paramIndex = 0;
        unsigned const paramCount = execEnv->getParamCount();

        // Skip over the link parameter if it is the first param
        if ((firstParamIsLinkHandle) && (paramIndex != paramCount))
            ++paramIndex;

        std::string hostName;
        if (paramIndex != paramCount) {
            ZValPointerString hostNameStringZVal(execEnv->getParam<ZValPointerString>(paramIndex));
            ++paramIndex;
            // We had a hostname argument, but it was not a string!
            if (!hostNameStringZVal)
                return boost::shared_ptr<MySQLIHandleInfo>();
            hostName = hostNameStringZVal.getStringValue();
            // Check for persistent connection
            if ((hostName.size() > 2) && (boost::algorithm::istarts_with(hostName, "p:")))
                hostName.erase(0, 2); // strip off "p:"
        }

        if (hostName.empty())
        {
            char* defaultHost = INI_STR("mysqli.default_host");
            if (!defaultHost)
                defaultHost = "localhost";
            hostName = defaultHost;
        }

        // Skip past user name parameter
        if (paramIndex != paramCount)
            ++paramIndex;

        // Skip past password parameter
        if (paramIndex != paramCount)
            ++paramIndex;

        std::string databaseName;
        if (paramIndex != paramCount) {
            ZValPointerString dbNameStringZVal(execEnv->getParam<ZValPointerString>(paramIndex));
            ++paramIndex;
            // We had a database name argument, but it was not a string!
            if (!dbNameStringZVal)
                return boost::shared_ptr<MySQLIHandleInfo>();
            databaseName = dbNameStringZVal.getStringValue();
        }

        unsigned long portNumber = 0;
        if (paramIndex != paramCount) {
            ZValPointerAny portNumberZVal(execEnv->getParam(paramIndex));
            ++paramIndex;
            long portNumberSigned;
            // We had a port number argument but we could not convert
            // it to a long.
            if (!portNumberZVal.convertInternalFunctionParameterToLong(&portNumberSigned,
                false))
                return boost::shared_ptr<MySQLIHandleInfo>();
            portNumber = static_cast<unsigned long>(portNumberSigned);
        }

        if (!portNumber) {
            portNumber = static_cast<unsigned long>(INI_INT("mysqli.default_port"));
            if (portNumber == 0)
                portNumber = 3306;
        }
        std::string portNumberString(boost::lexical_cast<std::string>(portNumber));

        ZValPointerObject mysqliObject(thisZVal.cast<ZValPointerObject>());
        if (mysqliObject) {
            ZValPointerAny version;
            if (mysqliObject.readPropertyByName(execEnv, SERVER_INFO_PROPERTY_NAME,
                                                SERVER_INFO_PROPERTY_NAME_LEN, &version)) {
                properties[enum2str(DBExitPoint_VERSION)] = version.cast<ZValPointerString>().getStringValue();
            }
        }

        properties[enum2str(DBExitPoint_HOST)] = hostName;
        properties[enum2str(DBExitPoint_PORT)] = portNumberString;
        properties[enum2str(DBExitPoint_VENDOR)] = MYSQL_VENDOR;
        if (!databaseName.empty())
            properties[enum2str(DBExitPoint_DATABASE)] = databaseName;

        ExitCallInfo exitCallInfo(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
                      ->makeIdentifyingProperties(execEnv, NULL, properties)
        );

        BOOST_ASSERT(exitCallInfo.isValid());
        return boost::make_shared<MySQLIHandleInfo>(exitCallInfo,
                                                    hostName,
                                                    portNumber,
                                                    databaseName,
                                                    execEnv,
                                                    properties);
    }

    MySQLIHandleInfo::MySQLIHandleInfo(const ExitCallInfo& exitCallInfo,
                                       const std::string& hostName,
                                       unsigned long portNumber,
                                       const std::string& databaseName,
                                       const PHPExecEnvironment* execEnv,
                                       const StringMap& properties)
        : AExitPointHandleInfo(exitCallInfo, &typeTag)
        , m_host(hostName)
        , m_port(portNumber)
        , m_database(databaseName)
        , m_execEnv(execEnv)
        , m_properties(properties)
    {
    }

    void MySQLIHandleInfo::setDatabase(const std::string& databaseName)
    {
        // Caller needs to check for empty database names
        // since the caller should also be checking that the underlying
        // php call was successful.
        BOOST_ASSERT(!databaseName.empty());

        m_database = databaseName;
        m_properties[enum2str(DBExitPoint_DATABASE)] = m_database;

        // Generate new ExitCallInfo, because adding just a single database name
        // property may have resulted in a different config rule matching.
        ExitCallInfo exitCallInfo(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
                      ->makeIdentifyingProperties(m_execEnv, NULL, m_properties)
        );
        setExitCallInfo(exitCallInfo);
    }

    const AHandleInfo::TypeTag StatementHandleInfo::typeTag;

    StatementHandleInfo::StatementHandleInfo(const ExitCallInfo& exitCallInfo,
                                             const std::string& query)
        : AExitPointHandleInfo(exitCallInfo, &typeTag)
        , m_query(query)
    {
    }

    StatementHandleInfo::StatementHandleInfo(const ExitCallInfo& exitCallInfo)
        : AExitPointHandleInfo(exitCallInfo, &typeTag)
    {
    }

    namespace
    {
        static const char MESSAGE_PROPERTY_NAME[] = "message";
        static size_t MESSAGE_PROPERTY_NAME_LEN = sizeof(MESSAGE_PROPERTY_NAME) - 1;

        static const char SQLSTATE_PROPERTY_NAME[] = "sqlstate";
        static size_t SQLSTATE_PROPERTY_NAME_LEN = sizeof(SQLSTATE_PROPERTY_NAME) - 1;

        static const char CODE_PROPERTY_NAME[] = "code";
        static size_t CODE_PROPERTY_NAME_LEN = sizeof(CODE_PROPERTY_NAME) - 1;

        class ReportModePusherPopper
        {
        protected:
            ReportModePusherPopper(const PHPExecEnvironment* execEnv);
            virtual ~ReportModePusherPopper();

        private:
            const PHPExecEnvironment* const m_execEnv;
#if PHP_VERSION_ID >= 50300
            long m_savedReportMode;
            zend_long* m_reportModePtr;
#else
            int m_savedReportMode;
            int* m_reportModePtr;
#endif
            long const m_MYSQLI_REPORT_ERROR;
            long const m_MYSQLI_REPORT_STRICT;
            unique_ptr<PHPExecEnvironment::ExceptionState>::type m_savedExceptionState;
        };

        class MySQLICurrentExitCall : public HandleBasedCurrentExitCall
                                    , private ReportModePusherPopper
        {
        public:
            MySQLICurrentExitCall(uint64_t sequenceNumber,
                                  const PHPExecEnvironment* execEnv,
                                  const boost::shared_ptr<AExitPointHandleInfo>& handleInfo);

            MySQLICurrentExitCall(uint64_t sequenceNumber,
                                  const PHPExecEnvironment* execEnv);
            virtual ~MySQLICurrentExitCall();
        };

        class PrepareInterceptorState : private ReportModePusherPopper
        {
        public:
            PrepareInterceptorState(const PHPExecEnvironment* execEnv,
                                    const std::string& queryString);
            virtual ~PrepareInterceptorState();

            const std::string& getQueryString() const { return m_queryString; }

        private:
            std::string const m_queryString;
        };

        inline long getMYSQLI_ASYNC(const PHPExecEnvironment* execEnv)
        {
            static bool initialized = false;
            static long value = 0;
            if (initialized)
                return value;
            static const char MYSQLI_ASYNC_NAME[] = "MYSQLI_ASYNC";
            execEnv->getConstantAsLong(MYSQLI_ASYNC_NAME, strlen(MYSQLI_ASYNC_NAME), &value);
            initialized = true;
            return value;
        }

        inline long getMYSQLI_REPORT_ERROR(const PHPExecEnvironment* execEnv)
        {
            static bool initialized = false;
            static long value = 0;
            if (initialized)
                return value;
            static const char MYSQLI_REPORT_ERROR_NAME[] = "MYSQLI_REPORT_ERROR";
            execEnv->getConstantAsLong(MYSQLI_REPORT_ERROR_NAME, strlen(MYSQLI_REPORT_ERROR_NAME), &value);
            initialized = true;
            return value;
        }

        inline long getMYSQLI_REPORT_STRICT(const PHPExecEnvironment* execEnv)
        {
            static bool initialized = false;
            static long value = 0;
            if (initialized)
                return value;
            static const char MYSQLI_REPORT_STRICT_NAME[] = "MYSQLI_REPORT_STRICT";
            execEnv->getConstantAsLong(MYSQLI_REPORT_STRICT_NAME, strlen(MYSQLI_REPORT_STRICT_NAME), &value);
            initialized = true;
            return value;
        }

        inline ReportModePusherPopper::ReportModePusherPopper(const PHPExecEnvironment* execEnv)
            : m_execEnv(execEnv)
            , m_reportModePtr(NULL)
            , m_MYSQLI_REPORT_ERROR(getMYSQLI_REPORT_ERROR(execEnv))
            , m_MYSQLI_REPORT_STRICT(getMYSQLI_REPORT_STRICT(execEnv))
        {

            const auto& mysqliGlobals =
                m_execEnv->getAgentGlobals().delayed_ext_globals.get_mysqli();
            // If mysqli is not loaded, or we can't find
            // its globals, just bail out of here.
            if (!mysqliGlobals)
                return;

            if (!m_MYSQLI_REPORT_ERROR)
                return;

            if (!m_MYSQLI_REPORT_STRICT)
                return;

            long const reportMask = m_MYSQLI_REPORT_ERROR | m_MYSQLI_REPORT_STRICT;

            m_reportModePtr = &(mysqliGlobals->report_mode);

            m_savedReportMode = *m_reportModePtr;
            if ((m_savedReportMode & reportMask) != reportMask)
            {
                m_savedExceptionState = m_execEnv->saveExceptionState();
                *m_reportModePtr = m_savedReportMode | reportMask;
            }
        }

        ReportModePusherPopper::~ReportModePusherPopper()
        {
            // m_reportModePtr will be null, if initialization
            // of the this object failed.
            if (!m_reportModePtr)
                return;

            // Restore the mysqli report_mode to the saved value.
            *m_reportModePtr = m_savedReportMode;

            long const reportMask = m_MYSQLI_REPORT_ERROR | m_MYSQLI_REPORT_STRICT;

            long const maskedSavedReportMode = m_savedReportMode & reportMask;

            // If the original report mode was
            // MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT
            // we did not save the exception state in the constructor
            // so there is nothing to do here.
            //
            // If the original report mode did not have
            // MYSQLI_REPORT_ERROR set, then
            // we don't need to do anything other than restore the report
            // mode, which we did above.
            if ((maskedSavedReportMode == reportMask) ||
                (!(maskedSavedReportMode & m_MYSQLI_REPORT_ERROR)))
                return;

            // The original report mode had MYSQLI_REPORT_ERROR set, so
            // we need to construct a message from the exception object
            // and pass the message to the php logging system.
            zval* exceptionRawZVal = m_execEnv->getExceptionObject();
            if (!exceptionRawZVal)
                return;

            ZValPointerAny exception(ZValPointerAny::share(exceptionRawZVal));
            ZValPointerObject exceptionObject(exception.cast<ZValPointerObject>());
            if (!exceptionObject)
                return;

            ZValPointerString messageValue(exceptionObject.findProtectedPropertyByName<ZValPointerString>(m_execEnv,
                                                                                                          MESSAGE_PROPERTY_NAME,
                                                                                                          MESSAGE_PROPERTY_NAME_LEN));

            if (!messageValue)
                return;
            std::string messageFieldString(messageValue.getStringValue());

            ZValPointerString sqlStateValue(exceptionObject.findProtectedPropertyByName<ZValPointerString>(m_execEnv,
                                                                                                           SQLSTATE_PROPERTY_NAME,
                                                                                                           SQLSTATE_PROPERTY_NAME_LEN));
            if (!sqlStateValue)
                return;
            std::string sqlStateString(sqlStateValue.getStringValue());
            if (sqlStateString == "00000")
                sqlStateString = "(null)";

            ZValPointerLong codeValue(exceptionObject.findProtectedPropertyByName<ZValPointerLong>(m_execEnv,
                                                                                                   CODE_PROPERTY_NAME,
                                                                                                   CODE_PROPERTY_NAME_LEN));
            if (!codeValue)
                return;
            std::string codeString(boost::lexical_cast<std::string>(codeValue.getLongValue()));

            std::string message;
            message.reserve(messageFieldString.size()
                          + sqlStateString.size()
                          + codeString.size()
                          + 5);
            message += '(';
            message += sqlStateString;
            message += '/';
            message += codeString;
            message += "): ";
            message += messageFieldString;

            m_execEnv->errorDocRef(NULL, E_WARNING, message);
        }

        MySQLICurrentExitCall::MySQLICurrentExitCall(uint64_t sequenceNumber,
                                                     const PHPExecEnvironment* execEnv,
                                                     const boost::shared_ptr<AExitPointHandleInfo>& handleInfo)
            : HandleBasedCurrentExitCall(sequenceNumber, handleInfo)
            , ReportModePusherPopper(execEnv)
        {
        }

        MySQLICurrentExitCall::MySQLICurrentExitCall(uint64_t sequenceNumber,
                                                     const PHPExecEnvironment* execEnv)
            : HandleBasedCurrentExitCall(sequenceNumber)
            , ReportModePusherPopper(execEnv)
        {
        }

        MySQLICurrentExitCall::~MySQLICurrentExitCall()
        {
        }

        PrepareInterceptorState::PrepareInterceptorState(const PHPExecEnvironment* execEnv,
                                                         const std::string& queryString)
            : ReportModePusherPopper(execEnv)
            , m_queryString(queryString)
        {
        }

        PrepareInterceptorState::~PrepareInterceptorState()
        {
        }
    }

    AConnectInterceptor::AConnectInterceptor(const char* interceptorName,
                                             InterceptorRegistry::ID interceptorID,
                                             AMethodInvocationSelector::CallableType type)
        : t_Base(interceptorName, interceptorID, appdynamics::pb::Agent::EXIT_DB)
        , AMethodInvocationSelector(type)
    {
    }

    CurrentExitCall* AConnectInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                uint64_t sequenceNumber) const
    {
        return new MySQLICurrentExitCall(sequenceNumber, execEnv);
    }

    const appdynamics::pb::SnapshotExitCall* AConnectInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                             TransactionContext* txContext,
                                                                                             Snapshot* snapshot,
                                                                                             const CurrentExitCall* currentExitCall,
                                                                                             uint64_t timeTakenMS) const
    {
        BOOST_ASSERT(currentExitCall != NULL);
        const boost::shared_ptr<AExitPointHandleInfo>& handleInfo =
            static_cast<const HandleBasedCurrentExitCall*>(currentExitCall)->getHandleInfo();
        const boost::shared_ptr<MySQLIHandleInfo>& mysqliHandleInfo(boost::static_pointer_cast<MySQLIHandleInfo>(handleInfo));
        const std::string& host(mysqliHandleInfo->getHost());
        unsigned long port(mysqliHandleInfo->getPort());
        const std::string& sqlStatement =
            std::string("Get DB Connection: ") + host + ":" + boost::lexical_cast<std::string>(port);
        return snapshot->getDBCalls().add(txContext,
                                          currentExitCall,
                                          sqlStatement,
                                          timeTakenMS);
    }

    bool AConnectInterceptor::getHandle(ZValPointerAny* handlePointer,
                                        const PHPExecEnvironment* execEnv) const
    {
        ZValPointerAny thisVal(getThisZVal(execEnv));
        if (!thisVal)
            return false;
        if (thisVal.getType() != ZValPointer::ZVal_Object)
            return false;
        *handlePointer = thisVal;
        return true;
    }

    bool AConnectInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                             const ExitCallInfo* currentExitCallInfo,
                                             const PHPExecEnvironment* execEnv) const
    {
        // We resolve the backend in onCallableEnd, so we never
        // expect to have an exit call info in the CurrentExitCall.
        BOOST_ASSERT(!currentExitCallInfo);
        boost::shared_ptr<MySQLIHandleInfo> mysqliHandleInfo(MySQLIHandleInfo::create(execEnv,
                                                                                      firstParamIsThis(),
                                                                                      getThisZVal(execEnv)));
        if (!mysqliHandleInfo)
            return false;
        *handleInfo = mysqliHandleInfo;
        return true;
    }

    ConnectInterceptor::ConnectInterceptor()
        : AConnectInterceptor("mysqli::ConnectInterceptor",
                              InterceptorRegistry::MySQLIConnectInterceptor_ID,
                              AMethodInvocationSelector::kInitProcedure)
    {
    }

    ConstructorInterceptor::ConstructorInterceptor()
        : AConnectInterceptor("mysqli::ConstructorInterceptor",
                              InterceptorRegistry::MySQLIConstructorInterceptor_ID,
                              AMethodInvocationSelector::kConstructor)
    {
    }

    RealConnectInterceptor::RealConnectInterceptor()
        : AConnectInterceptor("mysqli::RealConnectInterceptor",
                              InterceptorRegistry::MySQLIRealConnectInterceptor_ID,
                              AMethodInvocationSelector::kProcedure)
    {
    }

    RealConnectMethodInterceptor::RealConnectMethodInterceptor()
        : AConnectInterceptor("mysqli::RealConnectMethodInterceptor",
                              InterceptorRegistry::MySQLIRealConnectMethodInterceptor_ID,
                              AMethodInvocationSelector::kMethod)
    {
    }

    void* AHandleInfoMaintenanceInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        return NULL;
    }

    AHandleInfoMaintenanceInterceptor::AHandleInfoMaintenanceInterceptor(const char* interceptorName,
                                                                         InterceptorRegistry::ID interceptorID,
                                                                         AMethodInvocationSelector::CallableType type)
        : AMethodInterceptor(interceptorName, interceptorID)
        , AMethodInvocationSelector(type)
    {
    }

    ACloseInterceptor::ACloseInterceptor(const char* interceptorName,
                                         InterceptorRegistry::ID interceptorID,
                                         AMethodInvocationSelector::CallableType type)
        : AHandleInfoMaintenanceInterceptor(interceptorName, interceptorID, type)
    {
    }

    void ACloseInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                          void* state)
    {
        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        phpExecEnv->getHandleRegistry().unregisterHandle(handle);
    }

    CloseInterceptor::CloseInterceptor()
        : ACloseInterceptor("mysqli::CloseInterceptor",
                            InterceptorRegistry::MySQLICloseInterceptor_ID,
                            AMethodInvocationSelector::kProcedure)
    {
    }

    CloseMethodInterceptor::CloseMethodInterceptor()
        : ACloseInterceptor("mysqli::CloseMethodInterceptor",
                            InterceptorRegistry::MySQLICloseMethodInterceptor_ID,
                            AMethodInvocationSelector::kMethod)
    {
    }

    ASelectDBInterceptor::ASelectDBInterceptor(const char* interceptorName,
                                               InterceptorRegistry::ID interceptorID,
                                               AMethodInvocationSelector::CallableType type)
        : AHandleInfoMaintenanceInterceptor(interceptorName, interceptorID, type)
    {
    }

    void ASelectDBInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                             void* state)
    {
        if (phpExecEnv->doesExceptionObjectExist())
            return;

        if (!phpExecEnv->isReturnValueTrue())
            return;

        if (getParamCount(phpExecEnv) < 1)
            return;

        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        ZValPointerAny databaseNameZVal(getNthParameter(phpExecEnv, 0));
        if (!databaseNameZVal)
            return;

        ZValPointerString databaseNameZValString(databaseNameZVal.cast<ZValPointerString>());
        if (!databaseNameZValString)
            return;

        boost::shared_ptr<MySQLIHandleInfo> handleInfo(phpExecEnv->getHandleRegistry().getHandleInfo<MySQLIHandleInfo>(handle));
        if (!handleInfo)
            return;
        handleInfo->setDatabase(databaseNameZValString.getStringValue());
    }

    SelectDBInterceptor::SelectDBInterceptor()
        : ASelectDBInterceptor("mysqli::SelectDBInterceptor",
                               InterceptorRegistry::MySQLISelectDBInterceptor_ID,
                               AMethodInvocationSelector::kProcedure)
    {
    }

    SelectDBMethodInterceptor::SelectDBMethodInterceptor()
        : ASelectDBInterceptor("mysqli::SelectDBMethodInterceptor",
                               InterceptorRegistry::MySQLISelectDBMethodInterceptor_ID,
                               AMethodInvocationSelector::kMethod)
    {
    }

    AQueryInterceptor::AQueryInterceptor(const char* interceptorName,
                                         InterceptorRegistry::ID interceptorID,
                                         AMethodInvocationSelector::CallableType type)
        : t_Base(interceptorName, interceptorID, appdynamics::pb::Agent::EXIT_DB)
        , AMethodInvocationSelector(type)
    {
    }

    HandleBasedCurrentExitCall* AQueryInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                         uint64_t sequenceNumber,
                                                                         const ZValPointerAny& handle,
                                                                         const boost::shared_ptr<MySQLIHandleInfo>& handleInfo) const
    {
        return new MySQLICurrentExitCall(sequenceNumber, execEnv, handleInfo);
    }

    const appdynamics::pb::SnapshotExitCall* AQueryInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                                           TransactionContext* txContext,
                                                                                           Snapshot* snapshot,
                                                                                           const CurrentExitCall* exitCall,
                                                                                           uint64_t timeTakenMS) const
    {
        size_t nParams = getParamCount(execEnv);

        // If there are no parameters we don't
        // expect to get in here because AQueryInterceptor::getHandle
        // should have failed to get a handle, which should keep us out of here.
        // We'll still check just to be sure, though it is kind of gratuitous
        // defensive programming.
        BOOST_ASSERT(nParams > 0);
        if (nParams < 1)
            return NULL;

        ZValPointerString param0String(getNthParameter(execEnv, 0).cast<ZValPointerString>());
        if (!param0String)
            return NULL;
        std::string sqlStatement(param0String.getStringValue());
        boost::algorithm::trim(sqlStatement);
        if (sqlStatement.empty())
            return NULL;

        return snapshot->getDBCalls().add(txContext,
                                          exitCall,
                                          sqlStatement,
                                          timeTakenMS);
    }

    bool AQueryInterceptor::getHandle(ZValPointerAny* handlePointer,
                                      const PHPExecEnvironment* execEnv) const
    {
        size_t nParams = getParamCount(execEnv);
        if (nParams < 1)
            return false;

        if (nParams == 2) {
            ZValPointerAny resultModeZVal(getNthParameter(execEnv, 1));
            long resultMode;
            if (!resultModeZVal.convertInternalFunctionParameterToLong(&resultMode, false))
                return false;
            // We don't support async mode right now, so bailout of here
            // if the mode is async.
            if (resultMode & getMYSQLI_ASYNC(execEnv))
                return false;
        }

        ZValPointerAny thisZVal(getThisZVal(execEnv));
        if (!thisZVal)
            return false;
        *handlePointer = thisZVal;
        return true;
    }

    QueryInterceptor::QueryInterceptor()
        : AQueryInterceptor("mysqli::QueryInterceptor",
                            InterceptorRegistry::MySQLIQueryInterceptor_ID,
                            AMethodInvocationSelector::kProcedure)
    {
    }

    QueryMethodInterceptor::QueryMethodInterceptor()
        : AQueryInterceptor("mysqli::QueryMethodInterceptor",
                            InterceptorRegistry::MySQLIQueryMethodInterceptor_ID,
                            AMethodInvocationSelector::kMethod)
    {
    }

    ACommitInterceptor::ACommitInterceptor(const char* interceptorName,
                                           InterceptorRegistry::ID interceptorID,
                                           AMethodInvocationSelector::CallableType type)
        : t_Base(interceptorName, interceptorID, appdynamics::pb::Agent::EXIT_DB)
        , AMethodInvocationSelector(type)
    {
    }

    HandleBasedCurrentExitCall* ACommitInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                          uint64_t sequenceNumber,
                                                                          const ZValPointerAny& handle,
                                                                          const boost::shared_ptr<MySQLIHandleInfo>& handleInfo) const
    {
        return new MySQLICurrentExitCall(sequenceNumber, execEnv, handleInfo);
    }

    const appdynamics::pb::SnapshotExitCall* ACommitInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                            TransactionContext* txContext,
                                                                                            Snapshot* snapshot,
                                                                                            const CurrentExitCall* currentExitCall,
                                                                                            uint64_t timeTakenMS) const
    {
        BOOST_ASSERT(currentExitCall != NULL);
        const std::string& sqlStatement = "DB Transaction Commit";
        return snapshot->getDBCalls().add(txContext,
                                          currentExitCall,
                                          sqlStatement,
                                          timeTakenMS);
    }

    bool ACommitInterceptor::getHandle(ZValPointerAny* handlePointer,
                                       const PHPExecEnvironment* execEnv) const
    {
        ZValPointerAny thisZVal(getThisZVal(execEnv));
        if (!thisZVal)
            return false;
        *handlePointer = thisZVal;
        return true;
    }

    CommitInterceptor::CommitInterceptor()
        : ACommitInterceptor("mysqli::CommitInterceptor",
                             InterceptorRegistry::MySQLICommitInterceptor_ID,
                             AMethodInvocationSelector::kProcedure)
    {
    }

    CommitMethodInterceptor::CommitMethodInterceptor()
        : ACommitInterceptor("mysqli::CommitMethodInterceptor",
                             InterceptorRegistry::MySQLICommitMethodInterceptor_ID,
                             AMethodInvocationSelector::kMethod)
    {
    }

    AStatementInitInterceptor::AStatementInitInterceptor(const char* interceptorName,
                                                         InterceptorRegistry::ID interceptorID,
                                                         AMethodInvocationSelector::CallableType type)
        : AHandleInfoMaintenanceInterceptor(interceptorName, interceptorID, type)
    {
    }

    void AStatementInitInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                  void* state)
    {
        if (phpExecEnv->doesExceptionObjectExist())
            return;

        ZValPointerAny statementZVal(ZValPointerAny::share(phpExecEnv->getReturnValue()));
        ZValPointerObject statementObject(statementZVal.cast<ZValPointerObject>());
        if (!statementObject)
            return;

        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        HandleRegistry& handleRegistry = phpExecEnv->getHandleRegistry();
        boost::shared_ptr<MySQLIHandleInfo> handleInfo(handleRegistry.getHandleInfo<MySQLIHandleInfo>(handle));
        if (!handleInfo)
            return;

        boost::shared_ptr<StatementHandleInfo> statementHandleInfo(boost::make_shared<StatementHandleInfo>(handleInfo->getExitCallInfo()));

        handleRegistry.registerHandle(statementObject, statementHandleInfo);
    }

    StatementInitInterceptor::StatementInitInterceptor()
        : AStatementInitInterceptor("mysqli::StatementInitInterceptor",
                                    InterceptorRegistry::MySQLIStatementInitInterceptor_ID,
                                    AMethodInvocationSelector::kProcedure)
    {
    }

    StatementInitMethodInterceptor::StatementInitMethodInterceptor()
        : AStatementInitInterceptor("mysqli::StatementInitMethodInterceptor",
                                    InterceptorRegistry::MySQLIStatementInitMethodInterceptor_ID,
                                    AMethodInvocationSelector::kMethod)
    {
    }

    APrepareInterceptor::APrepareInterceptor(const char* interceptorName,
                                             InterceptorRegistry::ID interceptorID,
                                             AMethodInvocationSelector::CallableType type)
        : AHandleInfoMaintenanceInterceptor(interceptorName, interceptorID, type)
    {
    }

    void* APrepareInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        unsigned paramCount = getParamCount(phpExecEnv);
        if (paramCount < 1)
            return NULL;

        ZValPointerString queryZVal(getNthParameter(phpExecEnv, 0).cast<ZValPointerString>());
        if (!queryZVal)
            return NULL;

        std::string const query(queryZVal.getStringValue());

        return new PrepareInterceptorState(phpExecEnv, query);
    }

    void APrepareInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                            void* state)
    {
        if (!state)
            return;

        unique_ptr<PrepareInterceptorState>::type interceptorState(reinterpret_cast<PrepareInterceptorState*>(state));

        zval* const exceptionObject = phpExecEnv->getExceptionObject();
        if (exceptionObject) {
            ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
            errorMonitor->reportException(phpExecEnv, exceptionObject);
            return;
        }

        // If the prepare call failed, then bail out of here
        // We don't really ever expect the return value to be false
        // at this point because we change the mysqli reporting mode
        // to throw exceptions temporarily during this call, but
        // we'll be on the safe side and bail out of here if
        // the method/function returned false.
        if (phpExecEnv->isReturnValueFalse())
            return;

        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        zval* statementZVal = phpExecEnv->getReturnValue();
        if (!statementZVal)
            return;

        ZValPointerObject statementObject(ZValPointerAny::share(statementZVal).cast<ZValPointerObject>());
        if (!statementObject)
            return;

        HandleRegistry& handleRegistry = phpExecEnv->getHandleRegistry();
        boost::shared_ptr<MySQLIHandleInfo> handleInfo(handleRegistry.getHandleInfo<MySQLIHandleInfo>(handle));
        if (!handleInfo)
            return;

        boost::shared_ptr<StatementHandleInfo> statementHandleInfo(boost::make_shared<StatementHandleInfo>(handleInfo->getExitCallInfo(),
                                                                                                           interceptorState->getQueryString()));

        handleRegistry.registerHandle(statementObject, statementHandleInfo);
    }

    PrepareInterceptor::PrepareInterceptor()
        : APrepareInterceptor("mysqli::PrepareInterceptor",
                              InterceptorRegistry::MySQLIPrepareInterceptor_ID,
                              AMethodInvocationSelector::kProcedure)
    {
    }

    PrepareMethodInterceptor::PrepareMethodInterceptor()
        : APrepareInterceptor("mysqli::PrepareMethodInterceptor",
                              InterceptorRegistry::MySQLIPrepareMethodInterceptor_ID,
                              AMethodInvocationSelector::kMethod)
    {
    }


    AStatementPrepareInterceptor::AStatementPrepareInterceptor(const char* interceptorName,
                                                               InterceptorRegistry::ID interceptorID,
                                                               AMethodInvocationSelector::CallableType type)
        : AHandleInfoMaintenanceInterceptor(interceptorName,
                                            interceptorID,
                                            type)
    {
    }

    void* AStatementPrepareInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        unsigned paramCount = getParamCount(phpExecEnv);
        if (paramCount < 1)
            return NULL;

        ZValPointerString queryZVal(getNthParameter(phpExecEnv, 0).cast<ZValPointerString>());
        if (!queryZVal)
            return NULL;

        std::string const query(queryZVal.getStringValue());

        return new PrepareInterceptorState(phpExecEnv, query);
    }

    void AStatementPrepareInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                     void* state)
    {
        if (!state)
            return;

        unique_ptr<PrepareInterceptorState>::type interceptorState(reinterpret_cast<PrepareInterceptorState*>(state));

        zval* const exceptionObject = phpExecEnv->getExceptionObject();
        if (exceptionObject) {
            ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
            errorMonitor->reportException(phpExecEnv, exceptionObject);
            return;
        }

        // If the prepare call failed, then bail out of here
        // We don't really ever expect the return value to be false
        // at this point because we change the mysqli reporting mode
        // to throw exceptions temporarily during this call, but
        // we'll be on the safe side and bail out of here if
        // the method/function returned false.
        if (phpExecEnv->isReturnValueFalse())
            return;

        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        boost::shared_ptr<StatementHandleInfo> handleInfo(phpExecEnv->getHandleRegistry().getHandleInfo<StatementHandleInfo>(handle));
        if (!handleInfo)
            return;

        handleInfo->setQuery(interceptorState->getQueryString());
    }

    StatementPrepareInterceptor::StatementPrepareInterceptor()
        : AStatementPrepareInterceptor("mysqli::StatementPrepareInterceptor",
                                       InterceptorRegistry::MySQLIStatementPrepareInterceptor_ID,
                                       AMethodInvocationSelector::kProcedure)
    {
    }

    StatementPrepareMethodInterceptor::StatementPrepareMethodInterceptor()
        : AStatementPrepareInterceptor("mysqli::StatementPrepareMethodInterceptor",
                                       InterceptorRegistry::MySQLIStatementPrepareMethodInterceptor_ID,
                                       AMethodInvocationSelector::kMethod)
    {
    }

    AStatementExecuteInterceptor::AStatementExecuteInterceptor(const char* interceptorName,
                                                               InterceptorRegistry::ID interceptorID,
                                                               AMethodInvocationSelector::CallableType type)
        : t_Base(interceptorName, interceptorID, appdynamics::pb::Agent::EXIT_DB)
        , AMethodInvocationSelector(type)
    {
    }

    HandleBasedCurrentExitCall* AStatementExecuteInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                                    uint64_t sequenceNumber,
                                                                                    const ZValPointerAny& handle,
                                                                                    const boost::shared_ptr<StatementHandleInfo>& handleInfo) const
    {
        return new MySQLICurrentExitCall(sequenceNumber, execEnv, handleInfo);
    }

    const appdynamics::pb::SnapshotExitCall* AStatementExecuteInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                                                      TransactionContext* txContext,
                                                                                                      Snapshot* snapshot,
                                                                                                      const CurrentExitCall* exitCall,
                                                                                                      uint64_t timeTakenMS) const
    {
        BOOST_ASSERT(exitCall != NULL);

        ZValPointerAny thisZVal(getThisZVal(execEnv));
        if (!thisZVal)
            return NULL;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return NULL;

        HandleRegistry& handleRegistry = execEnv->getHandleRegistry();
        boost::shared_ptr<StatementHandleInfo> handleInfo(handleRegistry.getHandleInfo<StatementHandleInfo>(handle));
        if (!handleInfo)
            return NULL;

        std::string queryString = handleInfo->getQuery();
        boost::algorithm::trim(queryString);
        if (queryString.empty())
            return NULL;
        std::vector<SafeModuleShutdownWrapper<ZValPointerAny>>& boundParams = handleInfo->getBoundParams();
        if ((boundParams.empty()) || (!snapshot->captureSQLPositionalBoundParameters()))
            return snapshot->getDBCalls().add(txContext,
                                              exitCall,
                                              queryString,
                                              timeTakenMS);
        appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().addWithBoundParams(txContext,
                                                      exitCall,
                                                      queryString,
                                                      timeTakenMS);
        // dbCall will be null if we hit the limit of unique sql calls.
        if (!dbCall)
            return NULL;
        appdynamics::pb::BoundParameters* const boundParamsProtobuf =
            dbCall->mutable_boundparameters();
        boundParamsProtobuf->set_type(appdynamics::pb::BoundParameters::POSITIONAL);
        BOOST_FOREACH(const ZValPointerAny& param, boundParams) {
            getSQLParameterStringFromZVal(boundParamsProtobuf->add_posparameters(), param, execEnv);
        }
        return dbCall;
    }

    bool AStatementExecuteInterceptor::getHandle(ZValPointerAny* handlePointer,
                                                 const PHPExecEnvironment* execEnv) const
    {
        ZValPointerAny thisZVal(getThisZVal(execEnv));
        if (!thisZVal)
            return false;
        *handlePointer = thisZVal;
        return true;
    }

    StatementExecuteInterceptor::StatementExecuteInterceptor()
        : AStatementExecuteInterceptor("mysqli::StatementExecuteInterceptor",
                                       InterceptorRegistry::MySQLIStatementExecuteInterceptor_ID,
                                       AMethodInvocationSelector::kProcedure)
    {
    }

    StatementExecuteMethodInterceptor::StatementExecuteMethodInterceptor()
        : AStatementExecuteInterceptor("mysqli::StatementExecuteMethodInterceptor",
                                       InterceptorRegistry::MySQLIStatementExecuteMethodInterceptor_ID,
                                       AMethodInvocationSelector::kMethod)
    {
    }

    AStatementBindParamInterceptor::AStatementBindParamInterceptor(const char* interceptorName,
                                                                   InterceptorRegistry::ID interceptorID,
                                                                   AMethodInvocationSelector::CallableType type)
        : AHandleInfoMaintenanceInterceptor(interceptorName, interceptorID, type)
    {
    }

    void AStatementBindParamInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                       void* state)
    {
        // bindParam() succeeds only if the return value is actually boolean and true
        if (!(phpExecEnv->isReturnValueTrue()))
            return;

        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        HandleRegistry& handleRegistry = phpExecEnv->getHandleRegistry();
        boost::shared_ptr<StatementHandleInfo> handleInfo(handleRegistry.getHandleInfo<StatementHandleInfo>(handle));
        if (!handleInfo)
            return;

        size_t const paramCount = getParamCount(phpExecEnv);
        if (paramCount < 2)
            return;
        size_t const numBound = paramCount - 1;

        std::vector<SafeModuleShutdownWrapper<ZValPointerAny>>& boundParams = handleInfo->getBoundParams();
        boundParams.clear();
        boundParams.reserve(numBound);
        for (unsigned i = 0; i < numBound; ++i)
            boundParams.push_back(getNthParameter(phpExecEnv, i + 1));
    }

    StatementBindParamInterceptor::StatementBindParamInterceptor()
        : AStatementBindParamInterceptor("mysqli::StatementBindParamInterceptor",
                                       InterceptorRegistry::MySQLIStatementBindParamInterceptor_ID,
                                       AMethodInvocationSelector::kProcedure)
    {
    }

    StatementBindParamMethodInterceptor::StatementBindParamMethodInterceptor()
        : AStatementBindParamInterceptor("mysqli::StatementBindParamMethodInterceptor",
                                       InterceptorRegistry::MySQLIStatementBindParamMethodInterceptor_ID,
                                       AMethodInvocationSelector::kMethod)
    {
    }
}
