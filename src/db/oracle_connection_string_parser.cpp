/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */
#include <generated/db/oci/oci.cc>
#include <boost/algorithm/string/predicate.hpp>

namespace oci
{
    std::size_t CaseInsensitiveHash::operator() (const std::string& a) const
    {
        std::size_t hash = 0;
        std::locale locale;

        for(auto i = a.begin(); i != a.end(); ++i) {
            boost::hash_combine(hash, std::toupper(*i, locale));
        }

        return hash;
    }

    bool CaseInsensitiveEquals::operator() (const std::string& a, const std::string& b) const
    {
        return boost::algorithm::iequals(a, b, std::locale());
    }


    ParamNode::ParamNode(const std::string& name,
                         const boost::shared_ptr<DictionaryNode> value)
        : ASTNode(PARAM)
        , m_name(name)
        , m_value(value)
    {
    }

    DictionaryEntryNode::DictionaryEntryNode(const std::string& key,
                                             const boost::shared_ptr<ASTNode>& value)
        : ASTNode(DICTIONARY_ENTRY)
        , m_key(key)
        , m_value(value)
    {

    }

    void DictionaryNode::addEntry(const boost::shared_ptr<DictionaryEntryNode>& entry)
    {
        m_entries.push_back(entry);
        m_map.emplace(entry->key(), entry);
    }

    void ParamListNode::addParam(const boost::shared_ptr<ParamNode>& param)
    {
        m_params.push_back(param);
        m_map.emplace(param->name(), param);
    }

    StringNode::StringNode(const Token& token)
        : ASTNode(STRING)
        , m_value(token.text())
    {

    }

    void parser::error (const location_type& loc, const std::string& msg)
    {

    }
}
