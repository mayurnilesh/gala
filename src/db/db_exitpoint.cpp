/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#include "db_exitpoint.h"
#include "intercept.h"
#include "pdo_exitpoint.h"
#include "mysql_exitpoint.h"
#include "mysqli_exitpoint.h"
#include "oci8_exitpoint.h"
#include "pgsql_exitpoint.h"
#include <boost/assign/list_inserter.hpp>

DEFINE_ENUM(DBExitPointIndentifyingPropertyNames, DB_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

/* {{{ ext/pdo symbols */
static const char PDO_SYMBOL[] = "PDO";

static const char PDO_COMMIT_METHOD[] = "commit";
static const char PDO_ROLLBACK_METHOD[] = "rollback";
static const char PDO_PREPARE_METHOD[] = "prepare";
static const char PDO_QUERY_METHOD[] = "query";
static const char PDO_EXEC_METHOD[] = "exec";

static const char PDO_STATEMENT_SYMBOL[] = "PDOStatement";

static const char PDO_STATEMENT_EXEC_METHOD[] = "execute";

/* }}} */

/* {{{ ext/mysql symbols */
static const char MYSQL_CONNECT_SYMBOL[] = "mysql_connect";
static const char MYSQL_PCONNECT_SYMBOL[] = "mysql_pconnect";
static const char MYSQL_CLOSE_SYMBOL[] = "mysql_close";
static const char MYSQL_SELECT_DB_SYMBOL[] = "mysql_select_db";
static const char MYSQL_QUERY_SYMBOL[] = "mysql_query";
static const char MYSQL_UNBUFFERED_QUERY_SYMBOL[] = "mysql_unbuffered_query";
/* }}} */

/* {{{ ext/mysqli symbols */
namespace mysqli
{

static const char CONNECT_SYMBOL[] = "mysqli_connect";
static const char REAL_CONNECT_SYMBOL[] = "mysqli_real_connect";
static const char CLOSE_SYMBOL[] = "mysqli_close";
static const char SELECT_DB_SYMBOL[] = "mysqli_select_db";
static const char QUERY_SYMBOL[] = "mysqli_query";
static const char REAL_QUERY_SYMBOL[] = "mysqli_real_query";
static const char COMMIT_SYMBOL[] = "mysqli_commit";
static const char STATEMENT_INIT_SYMBOL[] = "mysqli_stmt_init";
static const char PREPARE_SYMBOL[] = "mysqli_prepare";
static const char STATEMENT_PREPARE_SYMBOL[] = "mysqli_stmt_prepare";
static const char STATEMENT_EXECUTE_SYMBOL[] = "mysqli_stmt_execute";
static const char STATEMENT_BIND_PARAM_SYMBOL[] = "mysqli_stmt_bind_param";
static const char EXECUTE_SYMBOL[] = "mysqli_execute";


static const char LINK_SYMBOL[] = "mysqli";
static const char REAL_CONNECT_METHOD[] = "real_connect";
static const char CLOSE_METHOD[] = "close";
static const char SELECT_DB_METHOD[] = "select_db";
static const char QUERY_METHOD[] = "query";
static const char REAL_QUERY_METHOD[] = "real_query";
static const char COMMIT_METHOD[] = "commit";
static const char STATEMENT_INIT_METHOD[] = "stmt_init";
static const char PREPARE_METHOD[] = "prepare";

static const char STATEMENT_SYMBOL[] = "mysqli_stmt";
static const char EXECUTE_METHOD[] = "execute";
static const char BIND_PARAM_METHOD[] = "bind_param";


}
/* }}} */

/* {{{ ext/oci8 symbols */
namespace oci
{

static const char CONNECT_SYMBOL[] = "oci_connect";
static const char NEW_CONNECT_SYMBOL[] = "oci_new_connect";
static const char PCONNECT_SYMBOL[] = "oci_pconnect";
static const char PARSE_SYMBOL[] = "oci_parse";
static const char BIND_BY_NAME_SYMBOL[] = "oci_bind_by_name";
static const char EXECUTE_SYMBOL[] = "oci_execute";
static const char FREE_STATEMENT_SYMBOL[] = "oci_free_statement";
static const char CLOSE_SYMBOL[] = "oci_close";

}
/* }}} */

/* {{{ ext/pgsql symbols */
static const char PGSQL_CONNECT_SYMBOL[] = "pg_connect";
static const char PGSQL_PCONNECT_SYMBOL[] = "pg_pconnect";
static const char PGSQL_CLOSE_SYMBOL[] = "pg_close";
static const char PGSQL_QUERY_SYMBOL[] = "pg_query";
static const char PGSQL_QUERY_PARAMS_SYMBOL[] = "pg_query_params";
static const char PGSQL_SEND_EXECUTE_SYMBOL[] = "pg_send_execute";
static const char PGSQL_SEND_PREPARE_SYMBOL[] = "pg_send_prepare";
static const char PGSQL_SEND_QUERY_PARAMS_SYMBOL[] = "pg_send_query_params";
static const char PGSQL_SEND_QUERY_SYMBOL[] = "pg_send_query";
static const char PGSQL_CANCEL_QUERY_SYMBOL[] = "pg_cancel_query";
static const char PGSQL_GET_RESULT_SYMBOL[] = "pg_get_result";
static const char PGSQL_PREPARE_SYMBOL[] = "pg_prepare";
static const char PGSQL_EXECUTE_SYMBOL[] = "pg_execute";
static const char PGSQL_SELECT_SYMBOL[] = "pg_select";
static const char PGSQL_COPY_TO_SYMBOL[] = "pg_copy_to";
static const char PGSQL_COPY_FROM_SYMBOL[] = "pg_copy_from";
static const char PGSQL_DELETE_SYMBOL[] = "pg_delete";
static const char PGSQL_INSERT_SYMBOL[] = "pg_insert";
static const char PGSQL_UPDATE_SYMBOL[] = "pg_update";
/* }}} */

int mysqlLinkType = 0;
int mysqlPersistentLinkType = 0;
int pgsqlLinkType = 0;
int pgsqlPersistentLinkType = 0;
int pgsqlResultType = 0;

PDOConstructorInterceptor pdoConstructorInterceptor;
PDOCommitInterceptor pdoCommitInterceptor;
PDORollbackInterceptor pdoRollbackInterceptor;
PDOPrepareInterceptor pdoPrepareInterceptor;
PDOQueryInterceptor pdoQueryInterceptor;
PDOExecInterceptor pdoExecInterceptor;
PDOStatementExecInterceptor pdoStatementExecInterceptor;
MySQLConnectInterceptor mysqlConnectInterceptor;
MySQLCloseInterceptor mysqlCloseInterceptor;
MySQLSelectDBInterceptor mysqlSelectDBInterceptor;
MySQLQueryInterceptor mysqlQueryInterceptor;
mysqli::ConnectInterceptor mysqliConnectInterceptor;
mysqli::ConstructorInterceptor mysqliConstructorInterceptor;
mysqli::RealConnectInterceptor mysqliRealConnectInterceptor;
mysqli::RealConnectMethodInterceptor mysqliRealConnectMethodInterceptor;
mysqli::CloseInterceptor mysqliCloseInterceptor;
mysqli::CloseMethodInterceptor mysqliCloseMethodInterceptor;
mysqli::SelectDBInterceptor mysqliSelectDBInterceptor;
mysqli::SelectDBMethodInterceptor mysqliSelectDBMethodInterceptor;
mysqli::QueryInterceptor mysqliQueryInterceptor;
mysqli::QueryMethodInterceptor mysqliQueryMethodInterceptor;
mysqli::CommitInterceptor mysqliCommitInterceptor;
mysqli::CommitMethodInterceptor mysqliCommitMethodInterceptor;
mysqli::StatementInitInterceptor mysqliStatementInitInterceptor;
mysqli::StatementInitMethodInterceptor mysqliStatementInitMethodInterceptor;
mysqli::PrepareInterceptor mysqliPrepareInterceptor;
mysqli::PrepareMethodInterceptor mysqliPrepareMethodInterceptor;
mysqli::StatementPrepareInterceptor mysqliStatementPrepareInterceptor;
mysqli::StatementPrepareMethodInterceptor mysqliStatementPrepareMethodInterceptor;
mysqli::StatementExecuteInterceptor mysqliStatementExecuteInterceptor;
mysqli::StatementExecuteMethodInterceptor mysqliStatementExecuteMethodInterceptor;
mysqli::StatementBindParamInterceptor mysqliStatementBindParamInterceptor;
mysqli::StatementBindParamMethodInterceptor mysqliStatementBindParamMethodInterceptor;
oci::ConnectInterceptor ociConnectInterceptor;
oci::ParseInterceptor ociParseInterceptor;
oci::BindInterceptor ociBindInterceptor;
oci::ExecuteInterceptor ociExecuteInterceptor;
oci::FreeStatementInterceptor ociFreeStatementInterceptor;
oci::CloseInterceptor ociCloseInterceptor;
PostgreSQLConnectInterceptor pgsqlConnectInterceptor;
PostgreSQLCloseInterceptor pgsqlCloseInterceptor;
PostgreSQLGetResultInterceptor pgsqlGetResultInterceptor;
PostgreSQLQueryInterceptor pgsqlQueryInterceptor;
PostgreSQLPrepareInterceptor pgsqlPrepareInterceptor;
PostgreSQLExecuteInterceptor pgsqlExecuteInterceptor;
PostgreSQLSendPrepareInterceptor pgsqlSendPrepareInterceptor;
PostgreSQLSendExecuteInterceptor pgsqlSendExecuteInterceptor;
PostgreSQLSendQueryInterceptor pgsqlSendQueryInterceptor;
PostgreSQLSendQueryParamsInterceptor pgsqlSendQueryParamsInterceptor;
PostgreSQLSelectInterceptor pgsqlSelectInterceptor;
PostgreSQLInsertInterceptor pgsqlInsertInterceptor;
PostgreSQLUpdateInterceptor pgsqlUpdateInterceptor;
PostgreSQLDeleteInterceptor pgsqlDeleteInterceptor;
PostgreSQLCancelQueryInterceptor pgsqlCancelQueryInterceptor;
PostgreSQLCopyToInterceptor pgsqlCopyToInterceptor;
PostgreSQLCopyFromInterceptor pgsqlCopyFromInterceptor;

bool DBExitPointDelegate::s_didApplyRules = false;

DBExitPointDelegate::DBExitPointDelegate(InterceptionEngine* interceptEngine,
                                         TransactionMonitor* txMonitor)
    : AExitPointDelegate(interceptEngine, txMonitor)
    , m_needDBVersion(false)
{
    mysqlLinkType = zend_fetch_list_dtor_id("mysql link");
    mysqlPersistentLinkType = zend_fetch_list_dtor_id("mysql link persistent");
    pgsqlLinkType = zend_fetch_list_dtor_id("pgsql link");
    pgsqlPersistentLinkType = zend_fetch_list_dtor_id("pgsql link persistent");
    pgsqlResultType = zend_fetch_list_dtor_id("pgsql result");

    boost::assign::push_back(m_displayNameSchema)
        (CONFIG_RULE_PROP_NAME,          (const char*) NULL)
        (enum2str(DBExitPoint_HOST),     (const char*) NULL)
        (enum2str(DBExitPoint_PORT),                    ":")
        (enum2str(DBExitPoint_DATABASE), (const char*) NULL)
        (enum2str(DBExitPoint_VENDOR),   (const char*) NULL)
        (enum2str(DBExitPoint_VERSION),  (const char*) NULL)
        (enum2str(DBExitPoint_URL),      (const char*) NULL);
}

void DBExitPointDelegate::configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                                    const struct _zend_agent_globals* agentGlobals)
{
    boost::shared_ptr<t_BackendRulesList> dbBackendRules(boost::make_shared<t_BackendRulesList>());
    dbBackendRules->CopyFrom(backendConfig.dbbackendconfig());
    AExitPointDelegate::configure(dbBackendRules, agentGlobals);
    m_needDBVersion = checkIfDBVersionNeeded();
}

void DBExitPointDelegate::applyRules(const _zend_agent_globals* agentGlobals)
{
    if (s_didApplyRules)
        return;

    s_didApplyRules = true;
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_SYMBOL, PHP_CONSTRUCT_METHOD), &pdoConstructorInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_SYMBOL, PDO_COMMIT_METHOD), &pdoCommitInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_SYMBOL, PDO_ROLLBACK_METHOD), &pdoRollbackInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_SYMBOL, PDO_PREPARE_METHOD), &pdoPrepareInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_SYMBOL, PDO_QUERY_METHOD), &pdoQueryInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_SYMBOL, PDO_EXEC_METHOD), &pdoExecInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(PDO_STATEMENT_SYMBOL, PDO_STATEMENT_EXEC_METHOD), &pdoStatementExecInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(MYSQL_CONNECT_SYMBOL), &mysqlConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MYSQL_PCONNECT_SYMBOL), &mysqlConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MYSQL_CLOSE_SYMBOL), &mysqlCloseInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MYSQL_SELECT_DB_SYMBOL), &mysqlSelectDBInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MYSQL_QUERY_SYMBOL), &mysqlQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MYSQL_UNBUFFERED_QUERY_SYMBOL), &mysqlQueryInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::CONNECT_SYMBOL), &mysqliConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, PHP_CONSTRUCT_METHOD), &mysqliConstructorInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::LINK_SYMBOL), &mysqliConstructorInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::REAL_CONNECT_SYMBOL), &mysqliRealConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::REAL_CONNECT_METHOD), &mysqliRealConnectMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::CLOSE_SYMBOL), &mysqliCloseInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::CLOSE_METHOD), &mysqliCloseMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::SELECT_DB_SYMBOL), &mysqliSelectDBInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::SELECT_DB_METHOD), &mysqliSelectDBMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::QUERY_SYMBOL), &mysqliQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::QUERY_METHOD), &mysqliQueryMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::REAL_QUERY_SYMBOL), &mysqliQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::REAL_QUERY_METHOD), &mysqliQueryMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::COMMIT_SYMBOL), &mysqliCommitInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::COMMIT_METHOD), &mysqliCommitMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_INIT_SYMBOL), &mysqliStatementInitInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::STATEMENT_INIT_METHOD), &mysqliStatementInitMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::PREPARE_SYMBOL), &mysqliPrepareInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::LINK_SYMBOL, mysqli::PREPARE_METHOD), &mysqliPrepareMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_PREPARE_SYMBOL), &mysqliStatementPrepareInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_SYMBOL, mysqli::PREPARE_METHOD), &mysqliStatementPrepareMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_EXECUTE_SYMBOL), &mysqliStatementExecuteInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::EXECUTE_SYMBOL), &mysqliStatementExecuteInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_SYMBOL, mysqli::EXECUTE_METHOD), &mysqliStatementExecuteMethodInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_BIND_PARAM_SYMBOL), &mysqliStatementBindParamInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(mysqli::STATEMENT_SYMBOL, mysqli::BIND_PARAM_METHOD), &mysqliStatementBindParamMethodInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::CONNECT_SYMBOL), &ociConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::NEW_CONNECT_SYMBOL), &ociConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::PCONNECT_SYMBOL), &ociConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::PARSE_SYMBOL), &ociParseInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::BIND_BY_NAME_SYMBOL), &ociBindInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::EXECUTE_SYMBOL), &ociExecuteInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::FREE_STATEMENT_SYMBOL), &ociFreeStatementInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(oci::CLOSE_SYMBOL), &ociCloseInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_CONNECT_SYMBOL), &pgsqlConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_PCONNECT_SYMBOL), &pgsqlConnectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_GET_RESULT_SYMBOL), &pgsqlGetResultInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_CLOSE_SYMBOL), &pgsqlCloseInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_QUERY_SYMBOL), &pgsqlQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_QUERY_PARAMS_SYMBOL), &pgsqlQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_PREPARE_SYMBOL), &pgsqlPrepareInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_EXECUTE_SYMBOL), &pgsqlExecuteInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_SEND_PREPARE_SYMBOL), &pgsqlSendPrepareInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_SEND_EXECUTE_SYMBOL), &pgsqlSendExecuteInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_SEND_QUERY_SYMBOL), &pgsqlSendQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_SEND_QUERY_PARAMS_SYMBOL), &pgsqlSendQueryParamsInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_SELECT_SYMBOL), &pgsqlSelectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_INSERT_SYMBOL), &pgsqlInsertInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_UPDATE_SYMBOL), &pgsqlUpdateInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_DELETE_SYMBOL), &pgsqlDeleteInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_CANCEL_QUERY_SYMBOL), &pgsqlCancelQueryInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_COPY_TO_SYMBOL), &pgsqlCopyToInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(PGSQL_COPY_FROM_SYMBOL), &pgsqlCopyFromInterceptor, true);
}

bool DBExitPointDelegate::processMatchRule(const PHPExecEnvironment* execEnv,
                                           const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                                           const StringMap& properties) const
{
    BOOST_ASSERT(matchRule.has_dbmatchrule());

    const appdynamics::pb::Agent::BackendMatchRule_DB& dbMatchRule =
        matchRule.dbmatchrule();
    auto notFound = properties.end();

    if (dbMatchRule.has_host()) {
        auto it = properties.find(enum2str(DBExitPoint_HOST));
        if (it == notFound)
            return false;
        StringMatch condition(dbMatchRule.host());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (dbMatchRule.has_port()) {
        auto it = properties.find(enum2str(DBExitPoint_PORT));
        if (it == notFound)
            return false;
        StringMatch condition(dbMatchRule.port());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (dbMatchRule.has_url()) {
        auto it = properties.find(enum2str(DBExitPoint_URL));
        if (it == notFound)
            return false;
        StringMatch condition(dbMatchRule.url());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (dbMatchRule.has_database()) {
        auto it = properties.find(enum2str(DBExitPoint_DATABASE));
        if (it == notFound)
            return false;
        StringMatch condition(dbMatchRule.database());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (dbMatchRule.has_vendor()) {
        auto it = properties.find(enum2str(DBExitPoint_VENDOR));
        if (it == notFound)
            return false;
        StringMatch condition(dbMatchRule.vendor());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (dbMatchRule.has_version()) {
        auto it = properties.find(enum2str(DBExitPoint_VERSION));
        if (it == notFound)
            return false;
        StringMatch condition(dbMatchRule.version());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    return true;
}

void DBExitPointDelegate::processNamingRule(const PHPExecEnvironment* execEnv,
                                            const appdynamics::pb::Agent::BackendRule& backendRule,
                                            const StringMap& properties,
                                            StringMap& identifyingProperties) const
{
    BOOST_ASSERT(backendRule.namingrule().has_dbnamingrule());
    const appdynamics::pb::Agent::BackendNamingRule_DB& dbNamingRule =
        backendRule.namingrule().dbnamingrule();
    Expression::Context context(execEnv, &properties);

    if (dbNamingRule.has_host()) {
        evaluateProperty(enum2str(DBExitPoint_HOST),
                         dbNamingRule.host(),
                         context,
                         identifyingProperties);
    }

    if (dbNamingRule.has_port()) {
        evaluateProperty(enum2str(DBExitPoint_PORT),
                         dbNamingRule.port(),
                         context,
                         identifyingProperties);
    }

    if (dbNamingRule.has_url()) {
        evaluateProperty(enum2str(DBExitPoint_URL),
                         dbNamingRule.url(),
                         context,
                         identifyingProperties);
    }

    if (dbNamingRule.has_database()) {
        evaluateProperty(enum2str(DBExitPoint_DATABASE),
                         dbNamingRule.database(),
                         context,
                         identifyingProperties);
    }

    if (dbNamingRule.has_vendor()) {
        evaluateProperty(enum2str(DBExitPoint_VENDOR),
                         dbNamingRule.vendor(),
                         context,
                         identifyingProperties);
    }

    if (dbNamingRule.has_version()) {
        evaluateProperty(enum2str(DBExitPoint_VERSION),
                         dbNamingRule.version(),
                         context,
                         identifyingProperties);
    }
}

bool DBExitPointDelegate::checkIfDBVersionNeeded() const
{
    auto end = m_backendRules->end();
    for (auto it = m_backendRules->begin(); it != end; ++it) {
        if (it->has_matchrule() &&
            it->matchrule().dbmatchrule().has_version()) {
           return true;
        }
        if (it->namingrule().dbnamingrule().has_version()) {
            return true;
        }
    }
    return false;
}

// vim: set fdm=marker:
