/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#include "oracle_connection_string.h"
#include "oracle_connection_string_parser.h"
#include "oracle_connection_string_scanner.h"
#include <boost/assert.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/make_shared.hpp>

#include <fcntl.h>
#include <sys/mman.h>

#if defined(__APPLE__)
#include <mach-o/dyld.h>
#else
#include <link.h>
#endif

namespace oci
{
    // Used to create entries in the OCINameCache that never expire.
    static bool returnTrue()
    {
        return true;
    }

    OCINameCache::Entry::Entry(const std::string& host,
                               const std::string& port,
                               const std::string& database,
                               const boost::function<bool ()>& isValid)
        : m_host(host)
        , m_port(port)
        , m_database(database)
        , m_isValid(isValid)
    {

    }

    OCINameCache::OCINameCache()
        : m_logger(::getLogger(std::string(LogContext::EXIT_DB) + ".OCINameCache"))
        , m_etcTNS("/etc/tnsnames.ora")
        , m_returnTrue(&returnTrue)
        , m_didComputeHome(false)
        , m_didComputeTNSAdmin(false)
        , m_didComputeOracleHome(false)
        , m_didComputeFileList(false)
    {
    }

    void OCINameCache::onRequestBegin()
    {
        for (auto i = m_files.begin(); i != m_files.end(); ++i)
            i->onRequestBegin();
    }

    OCINameCache::TNSFile::TNSFile(boost::filesystem::path const& name)
        : m_name(name)
        , m_lastModTimeKnown(false)
    {

    }

    OCINameCache::TNSFile::TNSFile(oci::OCINameCache::TNSFile const& other)
        : m_name(other.m_name)
        , m_paramList(other.m_paramList)
        , m_lastModTime(other.m_lastModTime)
        , m_lastParseTime(other.m_lastParseTime)
        , m_lastModTimeKnown(other.m_lastModTimeKnown)
    {
    }

    OCINameCache::TNSFile& OCINameCache::TNSFile::operator=(const OCINameCache::TNSFile& other)
    {
        m_name = other.m_name;
        m_paramList = other.m_paramList;
        m_lastModTime = other.m_lastModTime;
        m_lastParseTime = other.m_lastParseTime;
        m_lastModTimeKnown = other.m_lastModTimeKnown;
        return *this;
    }

    bool OCINameCache::TNSFile::isUpToDate() const
    {
        // If we have stat'd the file in the current request,
        // don't stat the file again.
        if (!m_lastModTimeKnown) {
            struct stat st;
            std::string const native(m_name.native());
            int statRet = stat(native.c_str(), &st);
            if (statRet == 0) {
                m_lastModTimeKnown = true;
                m_lastModTime = st.st_mtime;
            }
        }

        // If we failed to stat the file then let's expire
        // the cache entry.
        if (!m_lastModTimeKnown)
            return false;

        // Is the last modification time at the time of
        // parsing the file as new or newer than the last modification
        // time now.
        return m_lastParseTime >= m_lastModTime;
    }

    const boost::shared_ptr<ParamListNode>& OCINameCache::TNSFile::paramList(const AgentLogger& logger) const
    {
        // If we have already parsed the file and the file has not changed
        // then just return the existing parse tree.
        if (m_paramList && isUpToDate())
            return m_paramList;

        // The file changed, free the existing parse tree.
        m_paramList.reset();

        std::string const native(m_name.native());
        int fd = open(native.c_str(), O_RDONLY);
        if (fd == -1) {
            LOG4CXX_DEBUG(logger, "Unable to open: " << native);
            return m_paramList;
        }
        struct stat st;
        int statRet = fstat(fd, &st);
        if (statRet != 0) {
            close(fd);
            LOG4CXX_ERROR(logger, "fstat failed: " << errno);
            return m_paramList;
        }

        void* contents = mmap(0, st.st_size, PROT_READ, MAP_SHARED, fd, 0);

        // We can close the file after calling mmap.  Read the mmap docs
        // if you don't believe me.
        close(fd);

        if (contents == MAP_FAILED) {
            LOG4CXX_ERROR(logger, "mmap failed: " << errno);
            return m_paramList;
        }

        m_lastParseTime = st.st_mtime;
        m_lastModTime = st.st_mtime;
        m_lastModTimeKnown = true;

        const uint8_t* bytes = reinterpret_cast<const uint8_t*>(contents);
        oci::Scanner scanner(bytes, st.st_size);
        m_paramList = boost::make_shared<oci::ParamListNode>();
        LOG4CXX_TRACE(logger, "parsing: "
                              << std::string(reinterpret_cast<const char*>(bytes),
                                             std::min<off_t>(1024, st.st_size))
                              << "... from "
                              << native);
        oci::parser parser(&scanner, m_paramList);
        int parseRet = parser.parse();
        if (parseRet != 0) {
            LOG4CXX_DEBUG(logger, "parser returned: "
                                      << parseRet
                                      << " while parsing: "
                                      << std::string(reinterpret_cast<const char*>(bytes),
                                                     std::min<off_t>(1024, st.st_size))
                                      << "... from "
                                      << native);
        }

        munmap(contents, st.st_size);
        return m_paramList;
    }

    const boost::filesystem::path& OCINameCache::getHomeTNS()
    {
        if (m_didComputeHome)
            return m_homeTNS;
        m_didComputeHome = true;
        const char* envValue = getenv("HOME");
        if (envValue) {
            m_homeTNS = boost::filesystem::path(envValue) / ".tnsnames.ora";
        }
        return m_homeTNS;
    }

    const boost::filesystem::path& OCINameCache::getTNSAdminTNS()
    {
        if (m_didComputeTNSAdmin)
            return m_tnsAdminTNS;
        m_didComputeTNSAdmin = true;
        const char* envValue = getenv("TNS_ADMIN");
        if (envValue) {
            m_tnsAdminTNS = boost::filesystem::path(envValue) / "tnsnames.ora";
        }
        return m_tnsAdminTNS;
    }

    static const char OCI_LIBRARY_NAME_PREFIX[] = "libclntsh." SHLIB_SUFFIX_NAME;
    static const size_t OCI_LIBRARY_NAME_PREFIX_LEN = sizeof(OCI_LIBRARY_NAME_PREFIX) - 1;

    static int matchOCILib(const char* sharedLibName)
    {
        // Skip shared libraries whose name is null or empty.
        if (!sharedLibName)
            return 0;
        if (sharedLibName[0] == '\0')
            return 0;

        // Compute the basename of the shared library.
        boost::filesystem::path sharedLibPath(sharedLibName);
        std::string sharedLibBaseName(sharedLibPath.filename().native());

        // If the basename is too short, skip the library.
        if (sharedLibBaseName.length() < OCI_LIBRARY_NAME_PREFIX_LEN)
            return 0;

        // If the basename does not start with libclntsh.so, skip the library.
        if (sharedLibBaseName.compare(0, OCI_LIBRARY_NAME_PREFIX_LEN, OCI_LIBRARY_NAME_PREFIX) != 0)
            return 0;

        return 1;
    }

#if !defined(__APPLE__)
    static int findOCILibCallback(dl_phdr_info* hdr,
                                  size_t s,
                                  void* data)
    {
        // Look for a loaded shared library whose basename
        // starts with libclntsh.so.

        const char* const sharedLibName = hdr->dlpi_name;
        if (!matchOCILib(sharedLibName))
            return 0;

        // compute the location of the tnsnames.ora file relative to the location of the
        // shared library.
        boost::filesystem::path sharedLibPath(sharedLibName);
        boost::filesystem::path* out = reinterpret_cast<boost::filesystem::path*>(data);
        *out = boost::filesystem::path(sharedLibPath.parent_path()) / "network/admin/tnsnames.ora";
        return 1;
    }
#endif

    const boost::filesystem::path& OCINameCache::getOracleHomeTNS()
    {
        if (m_didComputeOracleHome)
            return m_oracleHomeTNS;
        m_didComputeOracleHome = true;

        const char* envValue = getenv("ORACLE_HOME");
        if (envValue) {
            m_oracleHomeTNS = boost::filesystem::path(envValue) / "network/admin/tnsnames.ora";
            return m_oracleHomeTNS;
        }

        // Enumerate all the loaded shared libraries looking for
        // one whose base name starts with libclntsh.so.  If such
        // a library is found construct the path to tnsnames.ora
        // relative to the location of that library.
#if !defined(__APPLE__)
        dl_iterate_phdr(findOCILibCallback, &m_oracleHomeTNS);
#else
        uint32_t i, count = _dyld_image_count();
        for (i = 0; i < count; i++) {
            const char* sharedLibName = _dyld_get_image_name(i);
            if (matchOCILib(sharedLibName)) {
                boost::filesystem::path sharedLibPath(sharedLibName);
                m_oracleHomeTNS = sharedLibPath.parent_path() / "network/admin/tnsnames.ora";
                break;
            }
        }
#endif
        return m_oracleHomeTNS;
    }

    const std::vector<OCINameCache::TNSFile>& OCINameCache::files()
    {
        if (m_didComputeFileList)
            return m_files;
        m_didComputeFileList = true;
        const boost::filesystem::path& homeTNS = getHomeTNS();
        addIfReadable(&m_files, homeTNS);
        const boost::filesystem::path& tnsAdminTNS = getTNSAdminTNS();
        addIfReadable(&m_files, tnsAdminTNS);
        const boost::filesystem::path& etcTNS = m_etcTNS;
        addIfReadable(&m_files, etcTNS);
        const boost::filesystem::path& oracleHomeTNS = getOracleHomeTNS();
        addIfReadable(&m_files, oracleHomeTNS);
        return m_files;
    }

    bool OCINameCache::canRead(const boost::filesystem::path& path)
    {
        std::string pathStr(path.native());
        // Successfully opening a file for reading is the best
        // way to determine if a file is readable.
        int fd = open(pathStr.c_str(), O_RDONLY);
        if (fd == -1)
            return false;
        close(fd);
        return true;
    }

    void OCINameCache::addIfReadable(std::vector<OCINameCache::TNSFile>* dest,
                                     const boost::filesystem::path& path)
    {
        if (!canRead(path))
            return;
        dest->push_back(path);
    }

    template <bool requireLeadingSlashes>
    OCINameCache::t_CacheMapIterator OCINameCache::parseShortForm(const std::string& connectionString)
    {
        std::string::const_iterator end = connectionString.end();
        std::string::const_iterator hostBegin = connectionString.begin();

        LOG4CXX_TRACE(m_logger, "Parsing "
                                << connectionString
                                << " ( leading '//' "
                                << (requireLeadingSlashes ? "" : "not ")
                                <<  "required )");

        size_t nSlashes = requireLeadingSlashes ? 2 : 0;

        // Consume the leading slashes if needed.
        while ((hostBegin != end) && (nSlashes > 0) && (*hostBegin == '/')) {
            hostBegin++;
            nSlashes--;
        }

        if (hostBegin == end)
            return m_map.end();

        if (nSlashes != 0)
            return m_map.end();

        // Consume the host name.
        std::string::const_iterator hostEnd = hostBegin;
        while ((hostEnd != end) && (*hostEnd != ':') && (*hostEnd != '/')) {
            hostEnd++;
        }

        std::string const host(hostBegin, hostEnd);
        std::string port("1521");
        std::string const emptyString;
        if (hostEnd == end) {
            return m_map.emplace(connectionString, Entry(host, port, emptyString, m_returnTrue)).first;
        }

        std::string::const_iterator dbStart = hostEnd + 1;
        if (*hostEnd == ':') {
            // Consume the port number
            std::string::const_iterator portStart = hostEnd + 1;
            if (portStart == end)
                return m_map.emplace(connectionString, Entry(host, port, emptyString, m_returnTrue)).first;
            std::string::const_iterator portEnd = portStart;
            while ((portEnd != end) && (*portEnd != '/')) {
                portEnd++;
            }
            port = std::string(portStart, portEnd);
            if (portEnd == end)
                return m_map.emplace(connectionString, Entry(host, port, emptyString, m_returnTrue)).first;
            dbStart = portEnd + 1;
        }

        if (dbStart == end)
            return m_map.emplace(connectionString, Entry(host, port, emptyString, m_returnTrue)).first;

        std::string const databaseName(dbStart, end);
        return m_map.emplace(connectionString, Entry(host, port, databaseName, m_returnTrue)).first;
    }

    /**
        Functor used to expire OCINameCache entries parsed from tnsnames.ora
        files.

        When a TNS entry is requested from the OCINameCache, we search
        a list of files for the definition of that entry.  To determine if the
        cache entry should be expired we need to stat the files in the search
        path until we get to the file that defined the TNS entry.
     */
    class ValidateTNSEntry
    {
    public:
        /**
            Constructor.
            @param nameCache containing OCINameCache.
            @param fileListEntry An interator pointing into the list of tnsnames.ora
            files that should be parsed when looking for a tnsnames.ora entry.  The TNSFile
            pointed at by this iterator and all the TNSFiles that precede it in the path
            vector will be checked for expiration when this functor is called.
         */
        ValidateTNSEntry(OCINameCache* nameCache,
                         std::vector<OCINameCache::TNSFile>::const_iterator fileListEntry)
            : m_nameCache(nameCache)
            , m_fileListEntry(fileListEntry)
        {

        }

        ValidateTNSEntry(const ValidateTNSEntry& other)
            : m_nameCache(other.m_nameCache)
            , m_fileListEntry(other.m_fileListEntry)
        {

        }

        ValidateTNSEntry& operator=(const ValidateTNSEntry& other)
        {
            m_nameCache = other.m_nameCache;
            m_fileListEntry = other.m_fileListEntry;
            return *this;
        }

        bool operator()() const
        {
            const auto& files = m_nameCache->files();
            // Walk forward throw the tnsnames.ora path.
            for (auto i = files.begin(); i != files.end(); ++i) {
                // If the current file is not up to date, then expire
                // the cache entry.
                if (!i->isUpToDate())
                    return false;

                // If we just check the TNSFile from which this entry was
                // parsed, then we don't need to expire the entry.
                if (i == m_fileListEntry)
                    return true;
            }
            if (m_fileListEntry != files.end()) {
                // Crap on a stick!  The iterator we were constructed with
                // no longer points into the TNSFile vector!
                LOG4CXX_ERROR(m_nameCache->m_logger, "file iterator does not point into the file list!");
                // Never found the file entry we were constructed with.
            }
            return false;
        }
    private:
        OCINameCache* m_nameCache;
        std::vector<OCINameCache::TNSFile>::const_iterator m_fileListEntry;
    };

    OCINameCache::t_CacheMapIterator OCINameCache::parseTNS(const std::string& connectionString)
    {
        const auto& fileList = files();
        LOG4CXX_TRACE(m_logger, "Looking for parameter named:" << connectionString);
        // Walk through all the TNSFile objects, until we find one that defines
        // a parameter that corresponds to our connection string.
        for (auto i = fileList.begin(); i != fileList.end(); ++i) {
            // Get the parameter list for the TNSFile parsing the file if needed.
            const boost::shared_ptr<ParamListNode>& paramList =
                i->paramList(m_logger);
            // File failed to parse, just skip it.
            if (!paramList) {
                LOG4CXX_TRACE(m_logger, "Unable to get parameter list: " << i->name().native())
                continue;
            }

            // File parsed, no extract the information we need
            // from the parse tree.
            const auto& paramListMap = paramList->map();

            auto const paramEntry = paramListMap.find(connectionString);
            if (paramEntry == paramListMap.end()) {
                LOG4CXX_TRACE(m_logger, connectionString << " not found in " << i->name().native());
                continue;
            }

            const auto& dictNode = paramEntry->second->value();
            ValidateTNSEntry validate(this, i);
            const auto newEntry = emplaceEntry(connectionString,
                                               dictNode,
                                               validate);
            if (newEntry == m_map.end()) {
                LOG4CXX_TRACE(m_logger, "Malformed parameter " << paramEntry->second->name() << " in " << i->name().native());
                continue;
            }

            return newEntry;
        }

        ValidateTNSEntry validate(this, fileList.end());
        std::string const emptyString;
        return m_map.emplace(connectionString, Entry(emptyString, emptyString, emptyString, validate)).first;
    }

    OCINameCache::t_CacheMapIterator OCINameCache::parseLongForm(const std::string& connectionString)
    {
        LOG4CXX_TRACE(m_logger, "parsing: " << connectionString);
        const uint8_t* bytes = reinterpret_cast<const uint8_t*>(connectionString.c_str());
        oci::Scanner scanner(bytes, connectionString.length());
        auto paramList(boost::make_shared<oci::ParamListNode>());
        oci::parser parser(&scanner, paramList);
        int parseRet = parser.parse();
        if (parseRet != 0) {
            LOG4CXX_DEBUG(m_logger, "parser returned: " << parseRet << " while parsing: " << connectionString);
        }

        std::string const emptyString;

        const auto& params = paramList->params();
        if (params.size() != 1) {
            LOG4CXX_DEBUG(m_logger, "Long form: param list size " << paramList->params().size());
            return m_map.emplace(connectionString, Entry(emptyString, emptyString, emptyString, m_returnTrue)).first;
        }

        const auto& firstParam = params[0];
        if (!firstParam->name().empty()) {
            LOG4CXX_DEBUG(m_logger, "Long form: first param name " << firstParam->name());
            return m_map.emplace(connectionString, Entry(emptyString, emptyString, emptyString, m_returnTrue)).first;
        }

        auto const newEntry =
            emplaceEntry(connectionString, firstParam->value(), m_returnTrue);
        if (newEntry != m_map.end())
            return newEntry;

        return m_map.emplace(connectionString, Entry(emptyString, emptyString, emptyString, m_returnTrue)).first;
    }

    template <bool shortFormRequiresLeadingSlashes>
    OCINameCache::t_CacheMapIterator OCINameCache::parseOracleConnectionString(const std::string& connectionString)
    {
        std::string emptyString;
        if (connectionString.empty()) {
            return m_map.emplace(connectionString, Entry(emptyString, emptyString, emptyString, m_returnTrue)).first;
        }

        // Case: long-form connection string
        // e.g. (DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=cs-database-1.corp.appdynamics.com)
        // (PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=orcl.corp.appdynamics.com)))
        const char firstChar = connectionString[0];
        if (firstChar == '(') {
            auto newEntry = parseLongForm(connectionString);
            if (newEntry != m_map.end())
                return newEntry;
        }

        // Case: short-form Oracle Easy Connect string.
        // e.g. //cs-database-1.corp.appdynamics.com:1521/orcle.corp.appdynamics.com
        else if (firstChar == '/') {
            auto newEntry = parseShortForm<true>(connectionString);
            if (newEntry != m_map.end())
                return newEntry;
        }

        // Case: short-form Oracle Easy Connect string, without leading slashes
        // e.g. cs-database-1.corp.appdynamics.com:1521/orcle.corp.appdynamics.com
        if (!shortFormRequiresLeadingSlashes && (connectionString.find("/") != std::string::npos)) {
            auto newEntry = parseShortForm<false>(connectionString);
            if (newEntry != m_map.end())
                return newEntry;
        }
        else {
            // DSN specified in tnsnames.ora
            auto newEntry = parseTNS(connectionString);
            if (newEntry != m_map.end())
                return newEntry;
        }

        return m_map.emplace(connectionString, Entry(emptyString, emptyString, emptyString, m_returnTrue)).first;
    }

    OCINameCache::t_CacheMapIterator OCINameCache::emplaceEntry(const std::string& connectionString,
                                                                const std::string& host,
                                                                const std::string& port,
                                                                const std::string& database,
                                                                const boost::function<bool ()>& validate)
    {
        auto newEntry = m_map.emplace(connectionString, Entry(host, port, database, validate));
        BOOST_ASSERT(newEntry.second);
        return newEntry.first;
    }

    /**
        Validates an ADDRESS dictionary entry and returns the
        dictionary in the value of the ADDRESS dictionary entry.

        For example if the specified dictionary entry node is for:
        (ADDRESS = (PROTOCOL = TCP)(HOST = <hostname>)(HOST = <port>))

        Then this function will return the DictionaryNode for:
        (PROTOCOL = TCP)(Host = <hostname>)(Port = <port>)

        A valid ADDRESS dictionary:
          1.  has a PROTOCOL entry whose value is TCP.
          2.  Has a HOST entry whose value is a string.
          3.  Either
              a.  Has no PORT entry
              b.  Has a PORT entry whose value is a string.
     */
    boost::shared_ptr<DictionaryNode> castToAddressDictionary(const AgentLogger& logger,
                                                              const boost::shared_ptr<DictionaryEntryNode>& entry)
    {
        if (entry->key() != "ADDRESS")
            return boost::shared_ptr<DictionaryNode>();
        const auto& v = entry->value();
        const DictionaryNode* dict = v->asDict();
        if (!dict) {
            LOG4CXX_DEBUG(logger, "ADDRESS value is not a dictionary!");
            return boost::shared_ptr<DictionaryNode>();
        }

        const auto& map = dict->map();
        auto protocolEntry = map.find("PROTOCOL");
        if (protocolEntry == map.end()) {
            LOG4CXX_DEBUG(logger, "Found ADDRESS without a PROTOCOL!");
            return boost::shared_ptr<DictionaryNode>();
        }

        const StringNode* protocol = protocolEntry->second->value()->asString();
        if (!protocol) {
            LOG4CXX_DEBUG(logger, "PROTOCOL value is not a string!");
            return boost::shared_ptr<DictionaryNode>();
        }

        if (!boost::algorithm::iequals(protocol->value(), "TCP")) {
            LOG4CXX_TRACE(logger, "Found PROTOCOL whose value is " << protocol->value());
            return boost::shared_ptr<DictionaryNode>();
        }

        auto hostEntry = map.find("HOST");
        if (hostEntry == map.end()) {
            LOG4CXX_DEBUG(logger, "Found ADDRESS without a HOST!");
            return boost::shared_ptr<DictionaryNode>();
        }

        const StringNode* host = hostEntry->second->value()->asString();
        if (!host) {
            LOG4CXX_DEBUG(logger, "HOST value is not a string!");
            return boost::shared_ptr<DictionaryNode>();
        }

        auto portEntry = map.find("PORT");
        if (portEntry == map.end()) {
            LOG4CXX_TRACE(logger, "Found ADDRESS without a PORT!");
            return boost::static_pointer_cast<DictionaryNode>(v);
        }

        const StringNode* port = portEntry->second->value()->asString();
        if (!port) {
            LOG4CXX_DEBUG(logger, "PORT value is not a string!");
            return boost::shared_ptr<DictionaryNode>();
        }

        return boost::static_pointer_cast<DictionaryNode>(v);
    }

    /**
        Given the dictionary node for DESCRIPTION, find the first ADDRESS entry under the
        dictionary node and return its value as a DictionaryNode.

        For example if the specified dictionary node is:

        (ADDRESS_LIST=
          (ADDRESS = (PROTOCOL = TCP)(Host = <hostname>)(Port = <port>))
        )
        (ADDRESS = (PROTOCOL = TCP)(Host = <other host>)(Port = <other port>))
        (CONNECT_DATA = (SERVICE_NAME = <sid>))

        Then this function will return the DictionaryNode for:
        (PROTOCOL = TCP)(Host = <hostname>)(Port = <port>)

        If the specified dictionary node is:
        (ADDRESS = (PROTOCOL = TCP)(Host = <hostname>)(Port = <port>))
        (ADDRESS = (PROTOCOL = TCP)(Host = <other host>)(Port = <other port>))
        (CONNECT_DATA = (SERVICE_NAME = <sid>))

        The this function will return the DictionaryNode for:
        (PROTOCOL = TCP)(Host = <hostname>)(Port = <port>)


     */
    boost::shared_ptr<DictionaryNode> searchForFirstAddressDict(const AgentLogger& logger,
                                                                const DictionaryNode* descriptionDict)
    {
        const auto& descriptionEntries = descriptionDict->entries();
        for (auto i = descriptionEntries.begin(); i != descriptionEntries.end(); ++i) {
            {
                boost::shared_ptr<DictionaryNode> addrDict(castToAddressDictionary(logger, *i));
                if (addrDict)
                    return addrDict;
            }
            if ((*i)->key() != "ADDRESS_LIST")
                continue;
            const DictionaryNode* addrListDict = (*i)->value()->asDict();
            if (!addrListDict) {
                LOG4CXX_DEBUG(logger, "ADDRESS_LIST value is not a dictionary!");
                continue;
            }

            const auto& addrListEntries = addrListDict->entries();
            for (auto j = addrListEntries.begin(); j != addrListEntries.end(); ++j) {
                boost::shared_ptr<DictionaryNode> addrDict(castToAddressDictionary(logger, *j));
                if (addrDict)
                    return addrDict;
            }
        }
        return boost::shared_ptr<DictionaryNode>();
    }

    OCINameCache::t_CacheMapIterator OCINameCache::emplaceEntry(const std::string& connectionString,
                                                                const boost::shared_ptr<DictionaryNode>& dict,
                                                                const boost::function<bool ()>& validate)
    {
        const DictionaryNode* descriptionsDict = dict.get();
        const auto& map = dict->map();
        // Dig the dictionary containing the DESCRIPTION entry out
        // of the DESCRIPTION_LIST entry if needed.
        auto descriptionListEntry = map.find("DESCRIPTION_LIST");
        if (descriptionListEntry != map.end()) {
            LOG4CXX_TRACE(m_logger, "Found DESCRIPTION_LIST");
            const DictionaryNode* subDict =
                descriptionListEntry->second->value()->asDict();
            if (!subDict) {
                LOG4CXX_DEBUG(m_logger, "DESCRIPTION_LIST value is not a dictionary!");
                return m_map.end();
            }
            descriptionsDict = subDict;
        }

        // Dig the first ADDRESS with a TCP protocol out of the DESCRIPTION.
        const auto& descriptionsMap = descriptionsDict->map();
        auto descriptionEntry = descriptionsMap.find("DESCRIPTION");
        if (descriptionEntry == descriptionsMap.end()) {
            LOG4CXX_TRACE(m_logger, "DESCRIPTION not found!");
            return m_map.end();
        }

        const DictionaryNode* descriptionDict = descriptionEntry->second->value()->asDict();
        if (!descriptionDict) {
            LOG4CXX_DEBUG(m_logger, "DESCRIPTION value is not a dictionary!");
            return m_map.end();
        }

        auto const addressDict(searchForFirstAddressDict(m_logger,
                                                         descriptionDict));
        if (!addressDict) {
            LOG4CXX_DEBUG(m_logger, "DESCRIPTION does not contain a valid ADDRESS!");
            return m_map.end();
        }

        const auto& descriptionMap = descriptionDict->map();
        auto const connectDataEntry = descriptionMap.find("CONNECT_DATA");
        if (connectDataEntry == descriptionMap.end()) {
            LOG4CXX_DEBUG(m_logger, "Found DESCRIPTION without a CONNECT_DATA!");
            return m_map.end();
        }

        // Get the database name out of the SERVICE_NAME entry in the CONNECT_DATA.
        const DictionaryNode* connectDataDict =
            connectDataEntry->second->value()->asDict();
        if (!connectDataDict) {
            LOG4CXX_DEBUG(m_logger, "CONNECT_DATA value is not a dictionary!");
            return m_map.end();
        }

        const auto& connectDataMap = connectDataDict->map();
        auto const serviceNameEntry = connectDataMap.find("SERVICE_NAME");
        std::string serviceName;
        if (serviceNameEntry != connectDataMap.end()) {
            const StringNode* serviceNameNode = serviceNameEntry->second->value()->asString();
            if (!serviceNameNode) {
                LOG4CXX_DEBUG(m_logger, "SERVICE_NAME value is not a string!");
                return m_map.end();
            }
            serviceName = serviceNameNode->value();
        }
        else {
            LOG4CXX_DEBUG(m_logger, "Found CONNECT_DATA without a SERVICE_NAME!");
        }

        const auto& addressMap = addressDict->map();
        const std::string& hostName = addressMap.find("HOST")->second->value()->asString()->value();
        std::string port("1521");
        auto const portEntry = addressMap.find("PORT");
        if (portEntry != addressMap.end()) {
            port = portEntry->second->value()->asString()->value();
        }

        return emplaceEntry(connectionString, hostName, port, serviceName, validate);
    }

    template <bool shortFormRequiresLeadingSlashes>
    OCINameCache::t_CacheMapIterator OCINameCache::getEntry(const std::string& connectionString)
    {
        auto const existingEntry = m_map.find(connectionString);
        if (existingEntry != m_map.end()) {
            if (existingEntry->second.isValid())
                return existingEntry;
            m_map.erase(existingEntry);
        }
        OCINameCache::t_CacheMapIterator const entry =
           parseOracleConnectionString<shortFormRequiresLeadingSlashes>(connectionString);
        LOG4CXX_DEBUG(m_logger, connectionString
                                << " -> "
                                << "host:\""
                                << entry->second.host()
                                << "\" port:\""
                                << entry->second.port()
                                << "\" db:\""
                                << entry->second.database()
                                << "\"");
        return entry;
    }

    const OCINameCache::Entry& OCINameCache::getOCIEntry(const std::string& connectionString)
    {
        return getEntry<false>(connectionString)->second;
    }

    const OCINameCache::Entry& OCINameCache::getPDOEntry(const std::string& connectionString)
    {
        return getEntry<true>(connectionString)->second;
    }
}
