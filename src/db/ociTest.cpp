/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

#include <iostream>

#include <db/oracle_connection_string_scanner.h>
#include <boost/make_shared.hpp>

// This is a test program used for testing the tnsnames.ora lexer and parser.


#if (defined(BOOST_ENABLE_ASSERT_HANDLER) && BOOST_ENABLE_ASSERT_HANDLER)

// forward declaration from assert.h
#if defined(__APPLE__)
extern "C" void __assert_rtn(const char *function,
                             const char *file,
                             int line,
                             const char *expr) __attribute__((__noreturn__));
#else
extern "C" void __assert_fail (__const char *__assertion,
                               __const char *__file,
                               unsigned int __line,
                               __const char *__function) __THROW __attribute__ ((__noreturn__));
#endif

namespace boost
{
    void assertion_failed(char const * expr,
                          char const * function,
                          char const * file,
                          long line)
    {
#if defined(__APPLE__)
        __assert_rtn(function, file, line, expr);
#else
        __assert_fail(expr, file, line, function);
#endif
    }

    void assertion_failed_msg(char const * expr,
                              char const * msg,
                              char const * function,
                              char const * file,
                              long line)
    {
        size_t exprLen = strlen(expr);
        size_t msgLen = strlen(msg);
        static const size_t seperatorLen = sizeof(" - ") - 1;
        size_t bufferLen = exprLen + msgLen + seperatorLen + 1;
        char* buffer = reinterpret_cast<char*>(alloca(bufferLen));
        char* dest = buffer;
        memcpy(dest, msg, msgLen);
        dest += msgLen;
        memcpy(dest, " - ", seperatorLen);
        dest += seperatorLen;
        memcpy(dest, expr, exprLen);
        buffer[bufferLen - 1] = '\0';
        assertion_failed(buffer, function, file, line);
    }
}

#endif

const char* tokenTypeToString(oci::TokenType type)
{
    switch (type)
    {
    case oci::parser::token::END:
        return "END";
    case oci::parser::token::PARAM:
        return "PARAM";
    case oci::parser::token::LPAREN:
        return "LPAREN";
    case oci::parser::token::RPAREN:
        return "RPAREN";
    case oci::parser::token::EQUALS:
        return "EQUALS";
    case oci::parser::token::ERROR:
        return "ERROR";
    case oci::parser::token::WORD:
        return "WORD";
    case oci::parser::token::SINGLE_QUOTED_STRING:
        return "SINGLE_QUOTED_STRING";
    case oci::parser::token::DOUBLE_QUOTED_STRING:
        return "DOUBLE_QUOTED_STRING";
    default:
        return "invalid token!!!";
    }
}

void print(std::ostream& ostr,
           const std::string& indent,
           const boost::shared_ptr<oci::DictionaryNode>& dict);

inline void print(std::ostream& ostr,
                  const std::string& indent,
                  const boost::shared_ptr<oci::DictionaryEntryNode>& entry)
{
    ostr << indent << "(" << entry->key() << " = ";
    const auto& value = entry->value();
    switch (value->type()) {
    case oci::ASTNode::STRING:
        ostr << boost::static_pointer_cast<oci::StringNode>(value)->value();
        ostr << ")" << std::endl;
        break;
    case oci::ASTNode::DICTIONARY:
        ostr << std::endl;
        print(ostr, indent + "  ", boost::static_pointer_cast<oci::DictionaryNode>(value));
        ostr << indent << ")" << std::endl;
        break;
    default:
        ostr << "Invalid tree shape!!! " << entry->type() << " " << value->type() ;
    }
}

inline void print(std::ostream& ostr,
                  const std::string& indent,
                  const boost::shared_ptr<oci::DictionaryNode>& dict)
{
    const auto& entries = dict->entries();
    for (auto i = entries.begin(); i != entries.end(); ++i) {
        print(ostr, indent, *i);
    }
}

inline void print(std::ostream& ostr,
                  const std::string& indent,
                  const boost::shared_ptr<oci::ParamNode>& param)
{
    ostr << param->name() << " = " << std::endl;
    print(ostr, indent + "  ", param->value());
}

inline void print(std::ostream& ostr,
                  const std::string& indent,
                  const boost::shared_ptr<oci::ParamListNode>& paramList)
{
    const auto& params = paramList->params();
    for (auto i = params.begin(); i != params.end(); ++i) {
        print(ostr, indent, *i);
        ostr << std::endl;
    }
}

int main(int argc, const char* argv[])
{
    if (argc < 3) {
        std::cerr << "Missing argument(s)!" << std::endl;
        return -1;
    }

    std::string command(argv[1]);

    int fd = open(argv[2], O_RDONLY);
    if (fd == -1) {
        std::cerr << "Failed to open \"" << argv[1] << "\"" << std::endl;
        return -1;
    }
    struct stat s;
    if (fstat(fd, &s) != 0) {
        close(fd);
        std::cerr << "fstat failed!" << std::endl;
    }

    void* contents = mmap(0, s.st_size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    if (contents == MAP_FAILED) {
        std::cerr << "mmap failed!" << std::endl;
        return -1;
    }

    const uint8_t* bytes = reinterpret_cast<const uint8_t*>(contents);
    oci::Scanner scanner(bytes, s.st_size);

    if (command == "lex") {
        oci::Token t(scanner.nextToken());

        while (t.type() != oci::parser::token::END) {
            std::cout << tokenTypeToString(t.type()) << ":" << t.text() << " " << t.location() << std::endl;
            t = scanner.nextToken();
        }
    }
    else if (command == "parse") {
        boost::shared_ptr<oci::ParamListNode> paramList(boost::make_shared<oci::ParamListNode>());
        oci::parser parser(&scanner, paramList);
        int parseRet = parser.parse();
        std::cout << "parseRet: " << parseRet << std::endl;
        print(std::cout, "", paramList);
    }

    munmap(contents, s.st_size);
    return 0;
}
