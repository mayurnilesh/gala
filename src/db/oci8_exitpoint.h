/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */
#ifndef __oci8_exitpoint_h
#define __oci8_exitpoint_h

#include "handle_tracking_exitpoint.h"
#include "exitcall_detail.h"
#include "sqlutil.h"
#include "PHPAgentProtobufs.pb.h"

namespace oci
{
    /**
        Interceptor for oci_connect and oci_pconnect.  Registers a handle info
        object with the resource returned by oci_connect.  The associated
        handle info object contains the host/port/service name for the
        oci connection resource.
     */
    class ConnectInterceptor : public ExitCallDetailHelper<HandleInitExitPointInterceptor<ConnectInterceptor>,
                                                           ConnectInterceptor,
                                                           appdynamics::pb::Agent::EXIT_DB>
    {
        friend class HandleInitExitPointInterceptor<ConnectInterceptor>;
        // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
        // getStatementType.
        friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<ConnectInterceptor>,
                                          ConnectInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<HandleInitExitPointInterceptor<ConnectInterceptor>,
                                     ConnectInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;
    public:
        ConnectInterceptor();

        virtual ~ConnectInterceptor();

        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;

        virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                    const ExitCallInfo* currentExitCallInfo,
                                    const PHPExecEnvironment* execEnv) const;
    protected:
        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* phpExecEnv);

    private:
        /**
            Called by base class template to create SnapshotExitCall and ensure that
            any newly created SnapshotExitCall is added to the specified Snapshot.

            Returns NULL if a SnapshotExitCall could not be created.  The total number
            of SnapshotExitCall's is limited by a setting from the SnapshotManager.
        */
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                            TransactionContext*,
                                                                            Snapshot*,
                                                                            const CurrentExitCall*,
                                                                            uint64_t timeTakenMS) const;
        /**
            Called by base class template to get the sql type string for the specified
            query.
         */
        inline std::string getSQLType(const std::string&) const { return ::kSqlConnect; }

        /**
            Called by class template to get the type of SQL statement this interceptor
            discovers.
         */
        inline const char* getStatementType() const { return NULL; }
    };

    /**
        Interceptor for oci_parse.  Associates a handle info object with the
        resource returned by oci_parse.  The associated handle info object
        contains the PL/SQL string for the oci statement resource.

     */
    class ParseInterceptor : public AMethodInterceptor
    {
    public:
        ParseInterceptor();
        virtual ~ParseInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
    };

    /**
        Interceptor for oci_bind_by_name.  Updates the handle info object
        associated with the oci statement resource passed to oci_bind_by_name
        with binding information so that the interceptor on oci_execute can
        adding binding information to snapshot exit calls.
     */
    class BindInterceptor : public AMethodInterceptor
    {
    public:
        BindInterceptor();
        virtual ~BindInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
    };

    class StatementHandleInfo;

    /**
        Interceptor for oci_execute.
     */
    class ExecuteInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<ExecuteInterceptor,
                                                                                            StatementHandleInfo>,
                                                           ExecuteInterceptor,
                                                           appdynamics::pb::Agent::EXIT_DB>
    {
        // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
        // getStatementType.
        friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<ExecuteInterceptor,
                                                                           StatementHandleInfo>,
                                          ExecuteInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<AHandleBasedExitPointInterceptor<ExecuteInterceptor,
                                                                      StatementHandleInfo>,
                                     ExecuteInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        ExecuteInterceptor();
        virtual ~ExecuteInterceptor();

    protected:
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;
        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* phpExecEnv);
    private:
         /**
            Called by base class template to create SnapshotExitCall and ensure that
            any newly created SnapshotExitCall is added to the specified Snapshot.

            Returns NULL if a SnapshotExitCall could not be created.  The total number
            of SnapshotExitCall's is limited by a setting from the SnapshotManager.

            If the sql string for the prepared statement is not available,
            this method will return NULL.
        */
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                            TransactionContext* txContext,
                                                                            Snapshot*,
                                                                            const CurrentExitCall*,
                                                                            uint64_t timeTakenMS) const;
        /**
            Called by base class template to get the sql type string for the specified
            query.
         */
        std::string getSQLType(const std::string& sql) const;

        /**
            Called by class template to get the type of SQL statement this interceptor
            discovers.
         */
        inline const char* getStatementType() const { return ::kSqlPreparedStatement; }
    };

    /**
        Interceptor for oci_free_statement.  Clears any handle information associated
        with the oci statement resource passed to oci_free_statement.
     */
    class FreeStatementInterceptor : public HandleCleanupInterceptor<FreeStatementInterceptor>
    {
        friend class HandleCleanupInterceptor<FreeStatementInterceptor>;
    public:
        FreeStatementInterceptor();
        virtual ~FreeStatementInterceptor();

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
    private:
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;
    };

    /**
        Interceptor for oci_close.  Clears any handle information associated
        with the oci connection resource passed to oci_close.
     */
    class CloseInterceptor : public HandleCleanupInterceptor<CloseInterceptor>
    {
        friend class HandleCleanupInterceptor<CloseInterceptor>;
    public:
        CloseInterceptor();
        virtual ~CloseInterceptor();

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
    private:
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;
    };
}


#endif
