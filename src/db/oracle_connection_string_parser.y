/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

%{
#include <db/oracle_connection_string_scanner.h>
#include <db/oracle_connection_string_parser.h>
#include <boost/make_shared.hpp>

namespace oci {

template <class t_Derived>
inline boost::shared_ptr<t_Derived> cast(const boost::shared_ptr<ASTNode>& ptr)
{
    return boost::static_pointer_cast<t_Derived>(ptr);
}


}

%}

%skeleton "lalr1.cc"

%parse-param   {Scanner *scanner}
%parse-param   {boost::shared_ptr<ParamListNode> paramList}
%lex-param   {Scanner *scanner}
%locations

%type <node> start
%type <node> paramList
%type <node> param
%type <node> value
%type <node> dictionary
%type <node> dictionaryEntry
%type <token> keyword
%type <node> literalValue

%token END
%token <token> PARAM
%token LPAREN
%token RPAREN
%token EQUALS
%token ERROR

%token <token> WORD
%token <token> SINGLE_QUOTED_STRING
%token <token> DOUBLE_QUOTED_STRING



%%

start:
    paramList {}
  | dictionaryEntry
    {
        auto implicitDictionary = boost::make_shared<DictionaryNode>();
        implicitDictionary->addEntry(cast<DictionaryEntryNode>($1));
        auto implicitParamNode = boost::make_shared<ParamNode>(std::string(),
                                                               implicitDictionary);
        paramList->addParam(implicitParamNode);
        $$ = paramList;
    }
;

paramList:
    paramList param
    {
        cast<ParamListNode>($1)->addParam(cast<ParamNode>($2));
        $$ = $1;
    }
  | { $$ = paramList; }
;

param:
  PARAM EQUALS dictionary
  {
      $$ = boost::make_shared<ParamNode>($1.text(),
                                         cast<DictionaryNode>($3));
  }
;

value:
    literalValue
  | dictionary
;

dictionary:
    dictionary dictionaryEntry
    {
        cast<DictionaryNode>($1)->addEntry(cast<DictionaryEntryNode>($2));
        $$ = $1;
    }
  | dictionaryEntry
    {
        auto dict = boost::make_shared<DictionaryNode>();
        dict->addEntry(cast<DictionaryEntryNode>($1));
        $$ = dict;
    }
;

dictionaryEntry:
  LPAREN keyword EQUALS value RPAREN
  {
      $$ = boost::make_shared<DictionaryEntryNode>($2.text(), $4);
  }
;

keyword:
    PARAM
  | WORD
  | SINGLE_QUOTED_STRING
  | DOUBLE_QUOTED_STRING
;

literalValue:
    PARAM { $$ = boost::make_shared<StringNode>($1); }
  | WORD { $$ = boost::make_shared<StringNode>($1); }
  | SINGLE_QUOTED_STRING { $$ = boost::make_shared<StringNode>($1); }
  | DOUBLE_QUOTED_STRING { $$ = boost::make_shared<StringNode>($1); }
  ;
