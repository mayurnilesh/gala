/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __sqlutil_h
#define __sqlutil_h

#include <string>

extern const std::string kSqlCommit;
extern const std::string kSqlConnect;
extern const std::string kSqlDDL;
extern const std::string kSqlDelete;
extern const std::string kSqlInsert;
extern const std::string kSqlQuery;
extern const std::string kSqlRollback;
extern const std::string kSqlUpdate;

extern const char* const kSqlPreparedStatement;
extern const char* const kSqlString;

const std::string& getSQLType(const std::string& sql);

class PHPExecEnvironment;
class ZValPointerAny;

void getSQLParameterStringFromZVal(std::string* positionalParameter,
                                   const ZValPointerAny& param,
                                   const PHPExecEnvironment* execEnv);

#endif /* __sqlutil_h */
