/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#include <generated/db/oci/oci_scanner.cpp>

namespace oci {

    Scanner::Scanner(const uint8_t* bufferStart,
                     size_t bufferLength)
        : m_currentTokenStart(bufferStart)
        , m_cursor(bufferStart)
        , m_lineStart(bufferStart)
        , m_lineNumber(1)
        , YYLIMIT(bufferStart + bufferLength)
        , YYMARKER(bufferStart)
        , m_eofBuffer(NULL)
    {

    }

    Scanner::~Scanner()
    {
        if (m_eofBuffer)
            free(m_eofBuffer);
    }

    const uint8_t* Scanner::fill(const uint8_t* cursor)
    {
        // This should only be called once we are nearing
        // the end of the file.
        size_t nBytesNeededForBacktrack = YYLIMIT - m_currentTokenStart;
        size_t newBufferSize = nBytesNeededForBacktrack + 1;
        uint8_t* newBuffer = reinterpret_cast<uint8_t*>(calloc(newBufferSize, 1));
        memcpy(newBuffer, m_currentTokenStart, nBytesNeededForBacktrack);
        cursor = &newBuffer[cursor - m_currentTokenStart];
        YYMARKER = &newBuffer[YYMARKER - m_currentTokenStart];
        YYLIMIT = newBuffer + newBufferSize;
        m_currentTokenStart = newBuffer;

        if (m_eofBuffer)
            free(m_eofBuffer);
        m_eofBuffer = newBuffer;
        return cursor;
    }
}
