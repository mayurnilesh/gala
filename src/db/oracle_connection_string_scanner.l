/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */
#include "db/oracle_connection_string_scanner.h"
#include <string.h>

namespace oci
{
    typedef uint8_t YYCTYPE;

    inline Token Scanner::returnToken(const uint8_t* cursor, TokenType tokenType)
    {
        m_cursor = cursor;
        oci::location location;
        location.begin = m_tokenStartPosition;
        location.end.line = m_lineNumber;
        location.end.column = (cursor - m_lineStart) + 1;
        return Token(this, tokenType, location);
    }

    #define YYFILL(n) { YYCURSOR = fill(YYCURSOR); }

    Token Scanner::nextToken()
    {
        const uint8_t* YYCURSOR = this->m_cursor;

    restart:
        m_currentTokenStart = YYCURSOR;
        m_tokenStartPosition.line = m_lineNumber;
        m_tokenStartPosition.column = (m_currentTokenStart - m_lineStart) + 1;

        /*!re2c
          any     = [\001-\377];
          ESC     = [\\][\001-\377];
          [a-zA-Z_][a-zA-Z0-9_\.]* { return returnToken(YYCURSOR, parser::token::PARAM); }

          '(' { return returnToken(YYCURSOR, parser::token::LPAREN); }
          ')' { return returnToken(YYCURSOR, parser::token::RPAREN); }
          '=' { return returnToken(YYCURSOR, parser::token::EQUALS); }
          ([A-Za-z0-9<>/\.:;\-_$+*&!%?@]|ESC)+ { return returnToken(YYCURSOR, parser::token::WORD); }
          (['] (ESC|(any\[\n\\']))* [']) { return returnToken(YYCURSOR, parser::token::SINGLE_QUOTED_STRING); }
          (["] (ESC|(any\[\n\\"]))* ["]) { return returnToken(YYCURSOR, parser::token::DOUBLE_QUOTED_STRING); }
          ([\000]) { return returnToken(YYCURSOR, parser::token::END); }
          ([#] (any\[\n])*) { goto restart; }
          ([ \t\v\r]+) { goto restart; }
          ([\n]) { m_lineStart = YYCURSOR; m_lineNumber++; goto restart; }
          (any\[ \t\v\r\n#]) { return returnToken(YYCURSOR, parser::token::ERROR); }
        */
        return Token();
    }

}
