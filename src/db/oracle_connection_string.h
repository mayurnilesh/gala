/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#ifndef __oracle_connection_string_h
#define __oracle_connection_string_h

#include <string>
#include <vector>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>
#include <boost/filesystem.hpp>
#include <boost/function.hpp>
#include "agent_logger.h"

namespace oci
{

class ParamListNode;
class DictionaryNode;


/**
    Calculates and caches host, port, and
    database name for oracle connection strings.

    Includes support for parsing tnsnames.ora.
 */
class OCINameCache : boost::noncopyable
{
    friend class ValidateTNSEntry;
public:
    class Entry
    {
    public:
        Entry(const std::string& host,
              const std::string& port,
              const std::string& database,
              const boost::function<bool ()>& isValid);
        inline const std::string& host() const { return m_host; }
        inline const std::string& port() const { return m_port; }
        inline const std::string& database() const { return m_database; }
        inline bool isValid() const { return m_isValid(); }
    private:
        std::string m_host;
        std::string m_port;
        std::string m_database;
        boost::function<bool ()> const m_isValid;
    };

    OCINameCache();

    const Entry& getOCIEntry(const std::string& connectionString);
    const Entry& getPDOEntry(const std::string& connectionString);


    void onRequestBegin();

private:

    typedef boost::unordered_map<std::string, Entry> t_CacheMap;
    typedef t_CacheMap::iterator t_CacheMapIterator;

    class TNSFile
    {
    public:
        TNSFile(const boost::filesystem::path& name);
        TNSFile(const TNSFile& other);
        TNSFile& operator=(const TNSFile& other);
        const boost::shared_ptr<ParamListNode>& paramList(const AgentLogger&) const;
        void onRequestBegin() { m_lastModTimeKnown = false; }
        bool isUpToDate() const;
        const boost::filesystem::path& name() const { return m_name; }

  private:
        boost::filesystem::path m_name;
        mutable boost::shared_ptr<ParamListNode> m_paramList;
        mutable time_t m_lastModTime;
        mutable time_t m_lastParseTime;
        mutable bool m_lastModTimeKnown;
    };

    const boost::filesystem::path& getHomeTNS();
    const boost::filesystem::path& getTNSAdminTNS();
    const boost::filesystem::path& getOracleHomeTNS();

    const std::vector<TNSFile>& files();

    bool canRead(const boost::filesystem::path& path);

    void addIfReadable(std::vector<TNSFile>* dest,
                       const boost::filesystem::path& path);

    template <bool shortFormRequiresLeadingSlashes>
    t_CacheMapIterator getEntry(const std::string& connectionString);

    template <bool shortFormRequiresLeadingSlashes>
    t_CacheMapIterator parseOracleConnectionString(const std::string& connectionString);

    t_CacheMapIterator parseLongForm(const std::string& connectionString);

    template <bool requireLeadingSlashes>
    t_CacheMapIterator parseShortForm(const std::string& connectionString);

    t_CacheMapIterator parseTNS(const std::string& connectionString);

    t_CacheMapIterator emplaceEntry(const std::string& connectionString,
                                    const std::string& host,
                                    const std::string& port,
                                    const std::string& database,
                                    const boost::function<bool ()>& validate);

    t_CacheMapIterator emplaceEntry(const std::string& connectionString,
                                    const boost::shared_ptr<DictionaryNode>& dict,
                                    const boost::function<bool ()>&);



    AgentLogger const m_logger;
    boost::filesystem::path m_homeTNS;
    boost::filesystem::path m_tnsAdminTNS;
    boost::filesystem::path const m_etcTNS;
    boost::filesystem::path m_oracleHomeTNS;
    std::vector<TNSFile> m_files;
    boost::unordered_map<std::string, Entry> m_map;
    boost::function<bool ()> m_returnTrue;

    bool m_didComputeHome : 1;
    bool m_didComputeTNSAdmin : 1;
    bool m_didComputeOracleHome : 1;
    bool m_didComputeFileList : 1;

};
}

#endif
