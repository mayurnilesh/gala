/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */
#include "oci8_exitpoint.h"
#include "sqlutil.h"
#include "oracle_connection_string.h"
#include "db_exitpoint.h"
#include "resource_type.h"
#include "transactions.h"

namespace oci
{
    // GCC requires these constants to be
    // externed so that they can be used as
    // template arguments.
    extern const char connectionResourceTypeName[];
    extern const char pconnectionResourceTypeName[];
    extern const char statementResourceTypeName[];

    const char connectionResourceTypeName[] = "oci8 connection";
    const char pconnectionResourceTypeName[] = "oci8 persistent connection";
    const char statementResourceTypeName[] = "oci8 statement";

    static const char OCI_SERVER_VERSION_FUNC[] = "oci_server_version";

    /**
        Handle info for oci connection resources.  Stores host/port/service
        for oci connections.
     */
    class ConnectionHandleInfo : public AExitPointHandleInfo
    {
        friend class ::HandleRegistry;
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static boost::shared_ptr<ConnectionHandleInfo> create(const PHPExecEnvironment* const execEnv);

        const std::string& getHost() const { return m_host; }
        const std::string& getPort() const { return m_port; }
        const std::string& getDatabase() const { return m_database; }

    private:
        ConnectionHandleInfo(const ExitCallInfo& exitCallInfo,
                             const std::string& host,
                             const std::string& port,
                             const std::string& database);

        std::string const m_host;
        std::string const m_port;
        std::string const m_database;
    };

    const AHandleInfo::TypeTag ConnectionHandleInfo::typeTag;

    /**
        Handle info for oci statement resources.  Stores SQL strings
        and binding information for oci statement resources.
     */
    class StatementHandleInfo : public AExitPointHandleInfo
    {
        friend class ::HandleRegistry;
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static boost::shared_ptr<StatementHandleInfo> create(const PHPExecEnvironment* const execEnv);

        void setBinding(const std::string& name, const ZValPointerAny& val, bool isArray);
        inline const std::string& getSQL() const { return m_sql; }

        class Binding
        {
        public:
            Binding();
            Binding(const ZValPointerAny& val, bool isArray);
            Binding(const Binding& other);

            Binding& operator=(const Binding& other);

            inline const ZValPointerAny& getValue() const { return m_val; }
        private:
            SafeModuleShutdownWrapper<ZValPointerAny> m_val;
            bool m_isArray;
        };


        typedef boost::unordered_map<std::string, Binding> t_BindingsMap;
        typedef t_BindingsMap::const_iterator t_BindingsIterator;

        t_BindingsIterator bindingsBegin() const { return m_bindings.begin(); }
        t_BindingsIterator bindingsEnd() const { return m_bindings.end(); }

    private:
        StatementHandleInfo(const ExitCallInfo& exitCallInfo, const std::string& sql);

        std::string const m_sql;
        boost::unordered_map<std::string, Binding> m_bindings;
    };

    const AHandleInfo::TypeTag StatementHandleInfo::typeTag;

    boost::shared_ptr<ConnectionHandleInfo> ConnectionHandleInfo::create(const PHPExecEnvironment* const execEnv)
    {
        StringMap properties;

        ZValPointerString connectionStringZVal(execEnv->getParam<ZValPointerString, true>(2));
        if (!connectionStringZVal)
            return boost::shared_ptr<ConnectionHandleInfo>();

        OCINameCache& nameCache = execEnv->getAgentGlobals().ociNameCache;

        const OCINameCache::Entry& ociNameEntry = nameCache.getOCIEntry(connectionStringZVal.getStringValue());
        const std::string& hostName = ociNameEntry.host();
        const std::string& port = ociNameEntry.port();
        const std::string& databaseName = ociNameEntry.database();
        if (hostName.empty())
            return boost::shared_ptr<ConnectionHandleInfo>();
        if (port.empty())
            return boost::shared_ptr<ConnectionHandleInfo>();

        properties[enum2str(DBExitPoint_HOST)] = hostName;
        properties[enum2str(DBExitPoint_PORT)] = port;
        properties[enum2str(DBExitPoint_VENDOR)] = ORACLE_VENDOR;
        if (!databaseName.empty())
            properties[enum2str(DBExitPoint_DATABASE)] = databaseName;

        std::string url(connectionStringZVal.getStringValue());
        if (url.size() > MAX_URL_LEN_IN_IDENTIFYING_PROPERTY)
            url.resize(MAX_URL_LEN_IN_IDENTIFYING_PROPERTY);
        properties[enum2str(DBExitPoint_URL)] = url;

        DBExitPointDelegate* const delegate =
            static_cast<DBExitPointDelegate*>(
                AG(kernel)->getAgentContext()
                          ->getTransactionMonitor()
                          ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
            );
        if (delegate->isDBVersionNeeded()) {
            ZValPointerResource connectionHandle(
                ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>()
            );

            if (connectionHandle) {
                ZValPointerAny returnValue;
                ZValPointerString ociServerVersionFunc(ZValPointerString::copy(OCI_SERVER_VERSION_FUNC));
                std::vector<ZValPointerAny> callableParams;
                callableParams.push_back(connectionHandle);
                if (execEnv->callCallable(&returnValue, ociServerVersionFunc, &callableParams)) {
                    ZValPointerString version(returnValue.cast<ZValPointerString>());
                    if (version)
                        properties[enum2str(DBExitPoint_VERSION)] = version.getStringValue();
                }
            }
        }

        ExitCallInfo exitCallInfo(
            delegate->makeIdentifyingProperties(execEnv, NULL, properties)
        );

        BOOST_ASSERT(exitCallInfo.isValid());
        return boost::make_shared<ConnectionHandleInfo>(exitCallInfo, hostName, port, databaseName);
    }

    ConnectionHandleInfo::ConnectionHandleInfo(const ExitCallInfo& exitCallInfo,
                                               const std::string& host,
                                               const std::string& port,
                                               const std::string& database)
       : AExitPointHandleInfo(exitCallInfo, &typeTag)
       , m_host(host)
       , m_port(port)
       , m_database(database)
    {
    }

    StatementHandleInfo::Binding::Binding()
        : m_isArray(false)
    {
    }

    StatementHandleInfo::Binding::Binding(const ZValPointerAny& val, bool isArray)

        : m_val(val)
        , m_isArray(isArray)
    {

    }

    StatementHandleInfo::Binding::Binding(const StatementHandleInfo::Binding& other)
        : m_val(other.m_val)
        , m_isArray(other.m_isArray)
    {

    }

    StatementHandleInfo::Binding& StatementHandleInfo::Binding::operator=(const StatementHandleInfo::Binding& other)
    {
        m_val = other.m_val;
        m_isArray = other.m_isArray;
        return *this;
    }



    boost::shared_ptr<StatementHandleInfo> StatementHandleInfo::create(const PHPExecEnvironment* const execEnv)
    {
        ZValPointerResource const connection(execEnv->getParam<ZValPointerResource, true>(0));
        if (!connection)
            return boost::shared_ptr<StatementHandleInfo>();
        ZValPointerString const sqlVal(execEnv->getParam<ZValPointerString, true>(1));
        if (!sqlVal)
            return boost::shared_ptr<StatementHandleInfo>();

        std::string const sql(sqlVal.getStringValue());
        if (sql.empty())
            return boost::shared_ptr<StatementHandleInfo>();

        HandleRegistry& handleRegistry = execEnv->getHandleRegistry();
        boost::shared_ptr<ConnectionHandleInfo> connectionHandleInfo(handleRegistry.getHandleInfo<ConnectionHandleInfo>(connection));
        if (!connectionHandleInfo)
            return boost::shared_ptr<StatementHandleInfo>();

        return boost::make_shared<StatementHandleInfo>(connectionHandleInfo->getExitCallInfo(), sql);
    }

    void StatementHandleInfo::setBinding(const std::string& name,
                                         const ZValPointerAny& val,
                                         bool isArray)
    {
        Binding const binding(val, isArray);
        m_bindings[name] = binding;
    }

    StatementHandleInfo::StatementHandleInfo(const ExitCallInfo& exitCallInfo,
                                             const std::string& sql)
        : AExitPointHandleInfo(exitCallInfo, &typeTag)
        , m_sql(sql)
    {

    }


    static const char messageKey[] = "message";
    static const char codeKey[] = "code";

    std::string getErrorMessage(const PHPExecEnvironment* execEnv,
                                const ZValPointerAny& ociResource)
    {
        ZValPointerString callable(ZValPointerString::copy("oci_error"));
        ZValPointerAny returnValue;
        std::vector<ZValPointerAny> callableParams;
        if (ociResource.cast<ZValPointerResource>())
            callableParams.push_back(ociResource);

        execEnv->callCallable(&returnValue, callable, &callableParams);

        ZValPointerArray errorInfo(returnValue.cast<ZValPointerArray>());
        if (!errorInfo)
            return std::string();

        ZValPointerString messageVal(errorInfo.findEntryByName<ZValPointerString>(messageKey, strlen(messageKey)));
        if (messageVal)
            return messageVal.getStringValue();

        ZValPointerLong errorCode(errorInfo.findEntryByName<ZValPointerLong>(codeKey, strlen(codeKey)));
        if (!errorCode)
            return std::string();

        return std::string("ORA-") + boost::lexical_cast<std::string>(errorCode.getLongValue());

    }

    //virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
    //                                                     const PHPExecEnvironment* phpExecEnv);

    void reportOCIError(const PHPExecEnvironment* phpExecEnv,
                        ErrorMonitor* errorMonitor,
                        const ZValPointerAny& ociResource)
    {

        std::string errorMessage(getErrorMessage(phpExecEnv, ociResource));

        if (errorMessage.empty())
            return;

        errorMonitor->reportSyntheticException(phpExecEnv, "OCI8", errorMessage);
    }


    ConnectInterceptor::ConnectInterceptor()
        : t_Base("oci::ConnectInterceptor",
                 InterceptorRegistry::OCIConnectInterceptor_ID,
                 appdynamics::pb::Agent::EXIT_DB)
    {

    }

    ConnectInterceptor::~ConnectInterceptor()
    {

    }

    bool ConnectInterceptor::getHandle(ZValPointerAny* handlePointer,
                                       const PHPExecEnvironment* execEnv) const
    {
        ZValPointerResource connectionHandle(ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>());
        if (!connectionHandle)
            return false;

        // Ignore return values that are not oci connection or oci
        // persistent connection resources.
        int const connectionResourceType =
            getResourceTypeId<connectionResourceTypeName>();
        int const pconnectionResourceType =
            getResourceTypeId<pconnectionResourceTypeName>();
        if (!connectionHandle.getInternalResource<void>(connectionResourceType, pconnectionResourceType))
            return false;
        *handlePointer = connectionHandle;
        return true;
    }

    bool ConnectInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                            const ExitCallInfo* currentExitCallInfo,
                                            const PHPExecEnvironment* execEnv) const
    {
        // We resolve the backend in onCallableEnd, so we never
        // expect to have an exit call info in the CurrentExitCall.
        BOOST_ASSERT(!currentExitCallInfo);
        boost::shared_ptr<ConnectionHandleInfo> connectionHandleInfo(ConnectionHandleInfo::create(execEnv));
        if (!connectionHandleInfo)
            return false;
        *handleInfo = connectionHandleInfo;
        return true;
    }

    boost::shared_ptr<AErrorObject> ConnectInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                                                     const PHPExecEnvironment* phpExecEnv)
    {
        return detectLoggedErrorsDuringExitCall(currentExitCall, "OCI8", phpExecEnv);
    }

    const appdynamics::pb::SnapshotExitCall* ConnectInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                            TransactionContext* txContext,
                                                                                            Snapshot* snapshot,
                                                                                            const CurrentExitCall* currentExitCall,
                                                                                            uint64_t timeTakenMS) const
    {
        BOOST_ASSERT(currentExitCall != NULL);
        boost::shared_ptr<AExitPointHandleInfo> handleInfo(static_cast<const HandleBasedCurrentExitCall*>(currentExitCall)->getHandleInfo());
        boost::shared_ptr<ConnectionHandleInfo> connectionHandleInfo(boost::static_pointer_cast<ConnectionHandleInfo>(handleInfo));
        const std::string& host(connectionHandleInfo->getHost());
        const std::string& port(connectionHandleInfo->getPort());
        const std::string& sqlStatement =
            std::string("Get DB Connection: ") + host + ":" + port;
        return snapshot->getDBCalls().add(txContext,
                                            currentExitCall,
                                            sqlStatement,
                                            timeTakenMS);
    }

    ParseInterceptor::ParseInterceptor()
        : AMethodInterceptor("oci::ParseInterceptor",
                             InterceptorRegistry::OCIParseInterceptor_ID)
    {
    }

    ParseInterceptor::~ParseInterceptor()
    {

    }

    static unsigned parseInterceptorDummyState = 0;

    void* ParseInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        ErrorMonitor* const errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        // We'll report a synthetic exception if the parse fails, so
        // don't report error messages relating to the parse.
        errorMonitor->addToErrorSuppressionDepth(1);
        return &parseInterceptorDummyState;
    }

    void ParseInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                         void* state)
    {
        if (state != &parseInterceptorDummyState)
            return;

        ErrorMonitor* const errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->addToErrorSuppressionDepth(-1);

        ZValPointerResource connection(execEnv->getParam<ZValPointerResource, true>(0));
        if (!connection)
            return;

        if (execEnv->isReturnValueFalse()) {
            reportOCIError(execEnv, errorMonitor, connection);
            return;
        }

        ZValPointerResource statement(ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>());
        if (!statement)
            return;

        int const statementResourceType =
            getResourceTypeId<statementResourceTypeName>();
        if (!statement.getInternalResource<void>(statementResourceType))
            return;

        HandleRegistry& handleRegistry = execEnv->getHandleRegistry();
        boost::shared_ptr<ConnectionHandleInfo> connectionHandleInfo(handleRegistry.getHandleInfo<ConnectionHandleInfo>(connection));
        if (!connectionHandleInfo)
            return;

        boost::shared_ptr<StatementHandleInfo> statementHandleInfo(StatementHandleInfo::create(execEnv));
        handleRegistry.registerHandle(statement, statementHandleInfo);
    }

    BindInterceptor::BindInterceptor()
        : AMethodInterceptor("oci::BindInterceptor", InterceptorRegistry::OCIBindInterceptor_ID)
    {

    }

    BindInterceptor::~BindInterceptor()
    {

    }

    static unsigned bindInterceptorDummyState = 0;

    void* BindInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        return &bindInterceptorDummyState;
    }

    void BindInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                        void* state)
    {
        if (state != &bindInterceptorDummyState)
            return;

        ZValPointerResource statement(execEnv->getParam<ZValPointerResource, true>(0));
        if (!statement)
            return;

        const HandleRegistry& handleRegistry = execEnv->getHandleRegistry();
        boost::shared_ptr<StatementHandleInfo> statementHandleInfo(handleRegistry.getHandleInfo<StatementHandleInfo>(statement));
        if (!statementHandleInfo)
            return;

        ZValPointerString name(execEnv->getParam<ZValPointerString, true>(1));
        if (name.isNullOrEmpty())
            return;

        ZValPointerAny variable(execEnv->getParam<ZValPointerAny, true>(2));
        statementHandleInfo->setBinding(name.getStringValue(), variable, false);
    }

    ExecuteInterceptor::ExecuteInterceptor()
        : t_Base("oci::ExecuteInterceptor", InterceptorRegistry::OCIExecuteInterceptor_ID, appdynamics::pb::Agent::EXIT_DB)
    {
    }

    ExecuteInterceptor::~ExecuteInterceptor()
    {

    }

    bool ExecuteInterceptor::getHandle(ZValPointerAny* handlePointer,
                                       const PHPExecEnvironment* execEnv) const
    {
        ZValPointerResource statement(execEnv->getParam<ZValPointerResource, true>(0));
        if (!statement)
            return false;
        *handlePointer = statement;
        return true;
    }

    boost::shared_ptr<AErrorObject> ExecuteInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                                                     const PHPExecEnvironment* phpExecEnv)
    {
        return detectLoggedErrorsDuringExitCall(currentExitCall, "OCI8", phpExecEnv);
    }

    static inline ZValPointerString toString(const ZValPointerAny& val,
                                             const PHPExecEnvironment* execEnv)
    {
        BOOST_ASSERT(val.getType() != ZValPointer::ZVal_Object);
        if (val.getType() == ZValPointer::ZVal_String)
            return val.cast<ZValPointerString>();
        return val.convertToStringAsCopy(execEnv);
    }

    const appdynamics::pb::SnapshotExitCall* ExecuteInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv, TransactionContext* txContext, Snapshot* snapshot,
                                                                                            const CurrentExitCall* currentExitCall,
                                                                                            uint64_t timeTakenMS) const
    {
        const HandleBasedCurrentExitCall* const handleBaseExitCall =
            static_cast<const HandleBasedCurrentExitCall*>(currentExitCall);

        boost::shared_ptr<StatementHandleInfo> statementHandleInfo(boost::static_pointer_cast<StatementHandleInfo>(handleBaseExitCall->getHandleInfo()));
        BOOST_ASSERT(statementHandleInfo);

        std::string sql(statementHandleInfo->getSQL());

        StatementHandleInfo::t_BindingsIterator bindingsBegin(statementHandleInfo->bindingsBegin());
        StatementHandleInfo::t_BindingsIterator bindingsEnd(statementHandleInfo->bindingsEnd());

        size_t bindingsCount = 0;
        for (StatementHandleInfo::t_BindingsIterator i = bindingsBegin; i != bindingsEnd; ++i) {
            const ZValPointerAny& val = i->second.getValue();

            if (val.getType() == ZValPointer::ZVal_Object)
                continue;
            if (val.getType() == ZValPointer::ZVal_Array)
                continue;

            ++bindingsCount;
        }

        if ((bindingsCount == 0) || (!snapshot->captureSQLPositionalBoundParameters()))
            return snapshot->getDBCalls().add(txContext,
                                                currentExitCall,
                                                sql,
                                                timeTakenMS);

        appdynamics::pb::SnapshotExitCall* const snapshotDBCall =
          snapshot->getDBCalls().addWithBoundParams(txContext,
                                                      currentExitCall,
                                                      sql,
                                                      timeTakenMS);
        // snapshotDBCall will be null if we hit the limit of unique sql calls.
        if (!snapshotDBCall)
            return NULL;
        appdynamics::pb::BoundParameters* const boundParamsProtobuf =
            snapshotDBCall->mutable_boundparameters();
        boundParamsProtobuf->set_type(appdynamics::pb::BoundParameters::OCI_NAMED);

        for (StatementHandleInfo::t_BindingsIterator i = bindingsBegin; i != bindingsEnd; ++i) {
            const std::string& key = i->first;
            const ZValPointerAny& val = i->second.getValue();

            if (val.getType() == ZValPointer::ZVal_Object)
                continue;
            if (val.getType() == ZValPointer::ZVal_Array)
                continue;

            const ZValPointerString valAsString(toString(val, execEnv));

            appdynamics::pb::BoundParameters_OCINamedParameter* const binding =
                boundParamsProtobuf->add_ocinamedparameters();
            binding->set_name(key);
            binding->set_value(valAsString.getStringValue());
        }

        return snapshotDBCall;
    }

    std::string ExecuteInterceptor::getSQLType(const std::string& sql) const
    {
        return ::getSQLType(sql);
    }

    FreeStatementInterceptor::FreeStatementInterceptor()
        : HandleCleanupInterceptor<FreeStatementInterceptor>("oci::FreeStatementInterceptor", InterceptorRegistry::OCIFreeStatementInterceptor_ID)
    {

    }

    FreeStatementInterceptor::~FreeStatementInterceptor()
    {

    }

    bool FreeStatementInterceptor::getHandle(ZValPointerAny* handlePointer,
                                             const PHPExecEnvironment* execEnv) const
    {
        ZValPointerResource statement(execEnv->getParam<ZValPointerResource, true>(0));
        if (!statement)
            return false;
        *handlePointer = statement;
        return true;
    }

    CloseInterceptor::CloseInterceptor()
        : HandleCleanupInterceptor<CloseInterceptor>("oci::CloseInterceptor", InterceptorRegistry::OCICloseInterceptor_ID)
    {

    }

    CloseInterceptor::~CloseInterceptor()
    {

    }

    bool CloseInterceptor::getHandle(ZValPointerAny* handlePointer,
                                             const PHPExecEnvironment* execEnv) const
    {
        ZValPointerResource statement(execEnv->getParam<ZValPointerResource, true>(0));
        if (!statement)
            return false;
        *handlePointer = statement;
        return true;
    }

}
