/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __pdo_exitpoint_h
#define __pdo_exitpoint_h

#include "handle_tracking_exitpoint.h"
#include "exitcall_detail.h"
#include "sqlutil.h"

class PDOHandleInfo : public AExitPointHandleInfo {
    friend class HandleRegistry;
    static const TypeTag typeTag;
    template <class T, class Arg1, class... Args>
        friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
public:
    static inline boost::shared_ptr<PDOHandleInfo> create(const appdynamics::pb::Agent::ExitPointType& type) { return boost::make_shared<PDOHandleInfo>(type); }
    static inline boost::shared_ptr<PDOHandleInfo> create(const ExitCallInfo& exitCallInfo) { return boost::make_shared<PDOHandleInfo>(exitCallInfo); }
private:
    inline PDOHandleInfo(const ExitCallInfo& exitCallInfo)
        : AExitPointHandleInfo(exitCallInfo, &typeTag)
    {
    }
    inline PDOHandleInfo(const appdynamics::pb::Agent::ExitPointType& type)
        : AExitPointHandleInfo(type, &typeTag)
    {
    }
};

/* {{{ class PDOConstructorInterceptor */

/**
    Interceptor for the PDO ( php data objects ) constructor that
    registers the PDO object in the HandleRegistry.  When there is no
    active registered business transaction, the PDO handle is still registered but
    no metrics for the backend are recorded by this interceptor.
    <p>
    Subsequent interceptors for PDO methods use the HandleRegistry entry this interceptor
    creates to identify the back-end associated with the PDO object.
 */
class PDOConstructorInterceptor : public ExitCallDetailHelper<HandleInitExitPointInterceptor<PDOConstructorInterceptor>,
                                                              PDOConstructorInterceptor,
                                                              appdynamics::pb::Agent::EXIT_DB>
{
    friend class HandleInitExitPointInterceptor<PDOConstructorInterceptor>;
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<PDOConstructorInterceptor>,
                                      PDOConstructorInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    typedef ExitCallDetailHelper<HandleInitExitPointInterceptor<PDOConstructorInterceptor>,
                                 PDOConstructorInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
public:
    PDOConstructorInterceptor();
    virtual ~PDOConstructorInterceptor() { }
protected:

    virtual bool getHandle(ZValPointerAny* handlePointer,
                           const PHPExecEnvironment* execEnv) const;

    virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                const ExitCallInfo* currentExitCallInfo,
                                const PHPExecEnvironment* execEnv) const;
private:
    /**
        Called by base class template to create SnapshotExitCall and ensure that
        any newly created SnapshotExitCall is added to the specified Snapshot.

        Returns NULL if a SnapshotExitCall could not be created.  The total number
        of SnapshotExitCall's is limited by a setting from the SnapshotManager.
    */
    const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                        TransactionContext*,
                                                                        Snapshot*,
                                                                        const CurrentExitCall*,
                                                                        uint64_t timeTakenMS) const;
    /**
        Called by base class template to get the sql type string for the specified
        query.
     */
    inline std::string getSQLType(const std::string&) const { return ::kSqlConnect; }

    /**
        Called by class template to get the type of SQL statement this interceptor
        discovers.
     */
    inline const char* getStatementType() const { return NULL; }
};

/* }}} */

/* {{{ class APDOMethodInterceptor */

class APDOMethodInterceptor : public AHandleBasedExitPointInterceptor<APDOMethodInterceptor, PDOHandleInfo>
{
protected:
    APDOMethodInterceptor(const char* interceptorName,
                          InterceptorRegistry::ID id)
        : AHandleBasedExitPointInterceptor<APDOMethodInterceptor,
                                           PDOHandleInfo>(interceptorName,
                                                          id,
                                                          appdynamics::pb::Agent::EXIT_DB) {}
    virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                              uint64_t sequenceNumber,
                                                              const ZValPointerAny& handle,
                                                              const boost::shared_ptr<PDOHandleInfo>& handleInfo) const;

    virtual bool getHandle(ZValPointerAny* handlePointer,
                           const PHPExecEnvironment* execEnv) const;
};

/* }}} */

/* {{{ class PDOCommitInterceptor */

/**
    Interceptor for PDO::commit.
 */
class PDOCommitInterceptor : public ExitCallDetailHelper<APDOMethodInterceptor,
                                                         PDOCommitInterceptor,
                                                         appdynamics::pb::Agent::EXIT_DB>
{
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<APDOMethodInterceptor,
                                      PDOCommitInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    typedef ExitCallDetailHelper<APDOMethodInterceptor,
                                 PDOCommitInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
public:
    PDOCommitInterceptor()
        : t_Base("PDOCommitInterceptor", InterceptorRegistry::PDOCommitInterceptor_ID) {}
    virtual ~PDOCommitInterceptor() { }

private:
    /**
        Called by base class template to create SnapshotExitCall and ensure that
        any newly created SnapshotExitCall is added to the specified Snapshot.

        Returns NULL if a SnapshotExitCall could not be created.  The total number
        of SnapshotExitCall's is limited by a setting from the SnapshotManager.
    */
    const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                        TransactionContext*,
                                                                        Snapshot*,
                                                                        const CurrentExitCall*,
                                                                        uint64_t timeTakenMS) const;
    /**
        Called by base class template to get the sql type string for the specified
        query.
     */
    inline std::string getSQLType(const std::string&) const { return ::kSqlCommit; }

    /**
        Called by class template to get the type of SQL statement this interceptor
        discovers.
     */
    inline const char* getStatementType() const { return NULL; }
};

/* }}} */

/* {{{ class PDORollbackInterceptor */

/**
    Interceptor for PDO::commit.
 */
class PDORollbackInterceptor : public ExitCallDetailHelper<APDOMethodInterceptor,
                                                           PDORollbackInterceptor,
                                                           appdynamics::pb::Agent::EXIT_DB>
{
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<APDOMethodInterceptor,
                                      PDORollbackInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    typedef ExitCallDetailHelper<APDOMethodInterceptor,
                                 PDORollbackInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
public:
    PDORollbackInterceptor()
        : t_Base("PDORollbackInterceptor", InterceptorRegistry::PDORollbackInterceptor_ID) {}
    virtual ~PDORollbackInterceptor() { }

private:
    /**
        Called by base class template to create SnapshotExitCall and ensure that
        any newly created SnapshotExitCall is added to the specified Snapshot.

        Returns NULL if a SnapshotExitCall could not be created.  The total number
        of SnapshotExitCall's is limited by a setting from the SnapshotManager.
    */
    const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                        TransactionContext*,
                                                                        Snapshot*,
                                                                        const CurrentExitCall*,
                                                                        uint64_t timeTakenMS) const;
    /**
        Called by base class template to get the sql type string for the specified
        query.
     */
    inline std::string getSQLType(const std::string&) const { return ::kSqlRollback; }

    /**
        Called by class template to get the type of SQL statement this interceptor
        discovers.
     */
    inline const char* getStatementType() const { return NULL; }
};

/* }}} */

/* {{{ class PDOPrepareInterceptor */

/**
 * Interceptor for PDO::prepare method that creates an entry in the
 * HandleRegistry for the PDOStatement object returned by this method.
 * The handleInfo saved in the registry is the same one as for the main PDO
 * object.
 * <p> Subsequent interceptors for PDOStatements methods use the HandleRegistry
 * entry this interceptor creates to identify the back-end associated with the
 * PDO object.
 */

class PDOPrepareInterceptor : public AMethodInterceptor {
    public:
        PDOPrepareInterceptor()
            : AMethodInterceptor("PDOPrepareInterceptor",
                                 InterceptorRegistry::PDOPrepareInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return false; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
};

/* }}} */

/* {{{ class PDOQueryInterceptor */

/**
    Interceptor for the PDO::query method that creates an entry
    in the HandleRegistry for the PDOStatement objects returned by this method.
    When there is no active registered business transaction, the PDO handle is still
    registered but no metrics for the backend are recorded by this interceptor.
    <p>
    Subsequent interceptors for PDOStatements methods use the HandleRegistry entry this
    interceptor creates to identify the back-end associated with the PDO object.
 */
class PDOQueryInterceptor : public ExitCallDetailHelper<HandleInitExitPointInterceptor<PDOQueryInterceptor>,
                                                        PDOQueryInterceptor,
                                                        appdynamics::pb::Agent::EXIT_DB>
{
    friend class HandleInitExitPointInterceptor<PDOQueryInterceptor>;
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<PDOQueryInterceptor>,
                                      PDOQueryInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    typedef ExitCallDetailHelper<HandleInitExitPointInterceptor<PDOQueryInterceptor>,
                                 PDOQueryInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
public:
    PDOQueryInterceptor();
    virtual ~PDOQueryInterceptor() { }
protected:
    virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                   uint64_t sequenceNumber) const;

    virtual bool getHandle(ZValPointerAny* handlePointer,
                           const PHPExecEnvironment* execEnv) const;

    virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                const ExitCallInfo* currentExitCallInfo,
                                const PHPExecEnvironment* execEnv) const;

private:
    /**
        Called by base class template to create SnapshotExitCall and ensure that
        any newly created SnapshotExitCall is added to the specified Snapshot.

        Returns NULL if a SnapshotExitCall could not be created.  The total number
        of SnapshotExitCall's is limited by a setting from the SnapshotManager.

        If the sql string passed to PDO::query can not be extracted
        from the parameters passed to the PDO::query, this method
        will return NULL.
    */
    const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                        TransactionContext*,
                                                                        Snapshot*,
                                                                        const CurrentExitCall*,
                                                                        uint64_t timeTakenMS) const;
    /**
        Called by base class template to get the sql type string for the specified
        query.
     */
    inline std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }

    /**
        Called by class template to get the type of SQL statement this interceptor
        discovers.
     */
    inline const char* getStatementType() const { return ::kSqlString; }
};

/* }}} */

/* {{{ class PDOExecInterceptor */

/**
  Interceptor for the PDO::exec method that executes a query and returns the number
  of rows in the result. Resolves the backend using an entry in the HandleRegistry.
 */
class PDOExecInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<PDOExecInterceptor,
                                                                                        PDOHandleInfo>,
                                                       PDOExecInterceptor,
                                                       appdynamics::pb::Agent::EXIT_DB>
{
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<PDOExecInterceptor,
                                                                       PDOHandleInfo>,
                                      PDOExecInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    typedef ExitCallDetailHelper<AHandleBasedExitPointInterceptor<PDOExecInterceptor,
                                                                  PDOHandleInfo>,
                                 PDOExecInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
public:
    PDOExecInterceptor();
    virtual ~PDOExecInterceptor() { }
protected:
    virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                              uint64_t sequenceNumber,
                                                              const ZValPointerAny& handle,
                                                              const boost::shared_ptr<PDOHandleInfo>& handleInfo) const;

    virtual bool getHandle(ZValPointerAny* handlePointer,
                           const PHPExecEnvironment* execEnv) const;

private:
    /**
        Called by base class template to create SnapshotExitCall and ensure that
        any newly created SnapshotExitCall is added to the specified Snapshot.

        Returns NULL if a SnapshotExitCall could not be created.  The total number
        of SnapshotExitCall's is limited by a setting from the SnapshotManager.

        If the sql string passed to PDO::exec can not be extracted
        from the parameters passed to the PDO::exec, this method
        will return NULL.
    */
    const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                        TransactionContext*,
                                                                        Snapshot*,
                                                                        const CurrentExitCall*,
                                                                        uint64_t timeTakenMS) const;
    /**
        Called by base class template to get the sql type string for the specified
        query.
     */
    inline std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }

    /**
        Called by class template to get the type of SQL statement this interceptor
        discovers.
     */
    inline const char* getStatementType() const { return ::kSqlString; }
};

/* }}} */

/* {{{ class PDOStatementExecInterceptor */

/**
    Interceptor for PDOStatement::execute() method. Captures SQL statement.
 */
class PDOStatementExecInterceptor : public ExitCallDetailHelper<APDOMethodInterceptor,
                                                                PDOStatementExecInterceptor,
                                                                appdynamics::pb::Agent::EXIT_DB>
{
    // Give base class template access to createAndAddSnapshotDBCall, getSQLType, and
    // getStatementType.
    friend class ExitCallDetailHelper<APDOMethodInterceptor,
                                      PDOStatementExecInterceptor,
                                      appdynamics::pb::Agent::EXIT_DB>;
    typedef ExitCallDetailHelper<APDOMethodInterceptor,
                                 PDOStatementExecInterceptor,
                                 appdynamics::pb::Agent::EXIT_DB> t_Base;
public:
    PDOStatementExecInterceptor();
    virtual ~PDOStatementExecInterceptor() { }

protected:
    virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                              uint64_t sequenceNumber,
                                                              const ZValPointerAny& handle,
                                                              const boost::shared_ptr<PDOHandleInfo>& handleInfo) const;
private:
    /**
        Called by base class template to create SnapshotExitCall and ensure that
        any newly created SnapshotExitCall is added to the specified Snapshot.

        Returns NULL if a SnapshotExitCall could not be created.  The total number
        of SnapshotExitCall's is limited by a setting from the SnapshotManager.

        If the sql string for the prepared statement is not available,
        this method will return NULL.
    */
    const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                        TransactionContext*,
                                                                        Snapshot*,
                                                                        const CurrentExitCall*,
                                                                        uint64_t timeTakenMS) const;
    /**
        Called by base class template to get the sql type string for the specified
        query.
     */
    inline std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }

    /**
        Called by class template to get the type of SQL statement this interceptor
        discovers.
     */
    inline const char* getStatementType() const { return ::kSqlPreparedStatement; }

    enum GetSQL_Result
    {
        GetSQL_FAILED,
        GetSQL_NOBIND,
        GetSQL_SUBSTITUTED
    };

    GetSQL_Result getSQLStatement(const PHPExecEnvironment* phpExecEnv, std::string* sqlStatement) const;
};

/* }}} */

#endif

// vim: set fdm=marker:
