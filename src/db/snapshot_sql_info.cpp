/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "snapshot_sql_info.h"
#include "PHPAgentProtobufs.pb.h"
#include "snapshot.h"
#include "current_exit_call.h"
#include "correlation.h"

appdynamics::pb::SnapshotExitCall* SnapshotDBCalls::addWithBoundParams(TransactionContext* txContext,
                                                                       const CurrentExitCall* exitCall,
                                                                       const std::string& sqlString,
                                                                       const long timeTakenMS)
{
    if (!exitCall->hasBackendID())
        return NULL;
    if (m_calls.size() >= m_snapshotManager->maxUniqueSQLCalls())
        return NULL;
    appdynamics::pb::SnapshotExitCall* dbCall = new appdynamics::pb::SnapshotExitCall();
    m_calls.push_back(dbCall);
    const BackendIdentifierPtr backendID(exitCall->getBackendID());
    backendID->fillInProtobufIdentifier(dbCall->mutable_backendidentifier());
    dbCall->set_detailstring(sqlString);
    dbCall->set_count(1);
    dbCall->set_timetaken(timeTakenMS);

    appdynamics::pb::Common::NameValuePair* mintime = dbCall->add_properties();
    mintime->set_name("minTimeMS");
    mintime->set_value(boost::lexical_cast<std::string>(timeTakenMS));

    appdynamics::pb::Common::NameValuePair* maxtime = dbCall->add_properties();
    maxtime->set_name("maxTimeMS");
    maxtime->set_value(boost::lexical_cast<std::string>(timeTakenMS));

    std::string const exitCallSequenceInfo(TransactionCorrelator::getExitCallSequenceString(txContext, exitCall->getSequenceNumber()));
    dbCall->set_sequenceinfo(exitCallSequenceInfo);

    return dbCall;
}

appdynamics::pb::SnapshotExitCall* SnapshotDBCalls::add(TransactionContext* txContext,
                                                        const CurrentExitCall* exitCall,
                                                        const std::string& sqlString,
                                                        const long timeTakenMS)
{
    if (!exitCall->hasBackendID())
        return NULL;
    const BackendIdentifierPtr backendID(exitCall->getBackendID());
    Key k(backendID, sqlString);
    auto i = m_normalizedCalls.find(k);
    if (i != m_normalizedCalls.end()) {
        // Update existing entry.
        appdynamics::pb::SnapshotExitCall* const existingDBCall = i->second;
        existingDBCall->set_count(existingDBCall->count() + 1);
        existingDBCall->set_timetaken(existingDBCall->timetaken() + timeTakenMS);
        // TODO This is crazy and needs to be fixed (add min/max fields to
        // protobuf)
        existingDBCall->mutable_properties(0)->set_value(
            boost::lexical_cast<std::string>(std::min<long>(boost::lexical_cast<long>(existingDBCall->mutable_properties(0)->value()), timeTakenMS)));
        existingDBCall->mutable_properties(1)->set_value(
            boost::lexical_cast<std::string>(std::max<long>(boost::lexical_cast<long>(existingDBCall->mutable_properties(1)->value()), timeTakenMS)));

        return existingDBCall;
    }

    if (m_calls.size() >= m_snapshotManager->maxUniqueSQLCalls())
        return NULL;

    appdynamics::pb::SnapshotExitCall* newDBCall = new appdynamics::pb::SnapshotExitCall();
    m_calls.push_back(newDBCall);
    backendID->fillInProtobufIdentifier(newDBCall->mutable_backendidentifier());
    newDBCall->set_detailstring(sqlString);
    newDBCall->set_count(1);
    newDBCall->set_timetaken(timeTakenMS);

    appdynamics::pb::Common::NameValuePair* mintime = newDBCall->add_properties();
    mintime->set_name(std::string("minTimeMS"));
    mintime->set_value(boost::lexical_cast<std::string>(timeTakenMS));

    appdynamics::pb::Common::NameValuePair* maxtime = newDBCall->add_properties();
    maxtime->set_name(std::string("maxTimeMS"));
    maxtime->set_value(boost::lexical_cast<std::string>(timeTakenMS));

    std::string const exitCallSequenceInfo(TransactionCorrelator::getExitCallSequenceString(txContext, exitCall->getSequenceNumber()));
    newDBCall->set_sequenceinfo(exitCallSequenceInfo);

    m_normalizedCalls[k] = newDBCall;
    return newDBCall;
}

void SnapshotDBCalls::mergeIntoSnapshot(appdynamics::pb::Snapshot* snapshot)
{
    auto const end = m_calls.end();
    google::protobuf::RepeatedPtrField<appdynamics::pb::SnapshotExitCall>* protoDBCalls =
        snapshot->mutable_exitcalls();
    for (auto i = m_calls.begin(); i != end; ++i) {
        protoDBCalls->AddAllocated(*i);
        *i = NULL;
    }
    m_calls.clear();
    m_normalizedCalls.clear();
}

SnapshotDBCalls::SnapshotDBCalls(const SnapshotManager* snapshotManager)
    : m_snapshotManager(snapshotManager)
{
    BOOST_ASSERT(m_snapshotManager != NULL);
}

SnapshotDBCalls::~SnapshotDBCalls()
{
    auto const end = m_calls.end();
    for (auto i = m_calls.begin(); i != end; ++i)
        delete *i;
}
