#include "db_exitpoint.h"
#include "mysql_exitpoint.h"
#include "sqlutil.h"
#include "emalloc_allocator.h"
#include "transactions.h"

#include <boost/algorithm/string.hpp>

#if PHP_VERSION_ID >= 50500
    // PHP 5.5 makes these headers public, so we just include them.
    #include <ext/mysqlnd/mysqlnd.h>
#elif PHP_VERSION_ID >= 50300
    // In PHP 5.4 and earlier, mysqlnd.h defines this macro which changes the
    // size of one of the structs in mysqlnd_structs.h. To avoid copying all
    // mysqlnd headers into our tree, we just define it ourselves.
    #define MYSQLND_STRING_TO_INT_CONVERSION
    #include <ext/mysqlnd/mysqlnd_enum_n_def.h>
    #include <ext/mysqlnd/mysqlnd_structs.h>
#endif

struct _php_mysql_conn {
    void *conn;
};


const AHandleInfo::TypeTag MySQLHandleInfo::typeTag;

class MySQLPropertyGenerator : public IBackendPropertyGenerator
{
    public:
        virtual bool getProperties(const PHPExecEnvironment* execEnv,
                                   const CurrentExitCall* exitCall,
                                   StringMap& properties) const;

    private:
        const char* getMySQLServerVersion(const PHPExecEnvironment* execEnv) const;
};
static MySQLPropertyGenerator mysqlPropertyGenerator;

static boost::shared_ptr<AErrorObject> detectMySQLError(const php_mysql_conn* mysqlConn,
                                                        CurrentExitCall* currentExitCall,
                                                        const PHPExecEnvironment* phpExecEnv)
{
    if (!mysqlConn)
        return boost::shared_ptr<AErrorObject>();

    const char* errorMessage = NULL;

    if (phpExecEnv->getAgentGlobals().doesMysqlUseMysqlnd) {
#if PHP_VERSION_ID >= 50300
        MYSQLND* mysqlndConn = reinterpret_cast<MYSQLND*>(mysqlConn->conn);
#if PHP_VERSION_ID < 50400
        if (phpExecEnv->callWithTSRMContext(mysqlndConn->m->get_error_no, mysqlndConn) != 0) {
            errorMessage = phpExecEnv->callWithTSRMContext(mysqlndConn->m->get_error_str, mysqlndConn);
        }
#else
        MYSQLND_CONN_DATA* mysqlndConnData = mysqlndConn->data;
        if (phpExecEnv->callWithTSRMContext(mysqlndConnData->m->get_error_no, mysqlndConnData) != 0) {
            errorMessage = phpExecEnv->callWithTSRMContext(mysqlndConnData->m->get_error_str, mysqlndConnData);
        }
#endif
#endif
    } else {
        // The extension was built with libmysql
        const DelayedFunctions::FunctionTable& delayedFunctions =
            phpExecEnv->getAgentGlobals().delayed_functions;

        const auto & mysqlErrno = delayedFunctions.get_mysql_errno();
        const auto & mysqlError = delayedFunctions.get_mysql_error();
        if (!mysqlErrno.isValid() || !mysqlError.isValid())
            return boost::shared_ptr<AErrorObject>();

#if PHP_VERSION_ID >= 50300
        MYSQL* conn = reinterpret_cast<MYSQL*>(mysqlConn->conn);
#else
        // In PHP 5.2, php_mysql_conn embeds the MYSQL struct as the first
        // field, instead of having a pointer to it.
        MYSQL* conn = reinterpret_cast<MYSQL*>(const_cast<php_mysql_conn*>(mysqlConn));
#endif

        if (mysqlErrno(conn) != 0) {
            errorMessage = mysqlError(conn);
        }
    }

    if (!errorMessage)
        return boost::shared_ptr<AErrorObject>();

    if (currentExitCall)
        currentExitCall->incrementErrorCount(1);
    ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return errorMonitor->reportSyntheticException(phpExecEnv, "MySQL", errorMessage);
}

bool MySQLPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                           const CurrentExitCall* exitCall,
                                           StringMap& properties) const
{
    zend_uintptr_t paramCount = execEnv->getParamCount();
    std::string host("localhost");
    std::string port("3306");
    std::string hostAndPort;

    if (paramCount == 0) {
        char* defaultHost = INI_STR("mysql.default_host");
        if (!defaultHost)
            defaultHost = "localhost:3306";
        hostAndPort = defaultHost;
    } else {
        ZValPointerString param0String(execEnv->getParam<ZValPointerString>(0));
        if (!param0String)
            return false;
        hostAndPort = param0String.getStringValue();
    }

    if (hostAndPort.empty())
        // TODO handle socket case??
        return false;

    size_t colonPos = hostAndPort.rfind(":");
    if (colonPos == std::string::npos) {
        host = hostAndPort;
        port = boost::lexical_cast<std::string>(INI_INT("mysql.default_port"));
    } else {
        if (colonPos != 0)
            host = hostAndPort.substr(0, colonPos);
        if (colonPos != hostAndPort.size()-1)
            port = hostAndPort.substr(colonPos+1);
    }

    DBExitPointDelegate* const delegate =
        static_cast<DBExitPointDelegate*>(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
        );
    if (delegate->isDBVersionNeeded()) {
        const char* version = getMySQLServerVersion(execEnv);
        if (version)
            properties[enum2str(DBExitPoint_VERSION)] = version;
    }

    properties[enum2str(DBExitPoint_HOST)] = host;
    properties[enum2str(DBExitPoint_PORT)] = port;
    properties[enum2str(DBExitPoint_VENDOR)] = MYSQL_VENDOR;

    return true;
}

const char*
MySQLPropertyGenerator::getMySQLServerVersion(const PHPExecEnvironment* execEnv) const
{
    ZValPointerResource mysqlHandle(ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>());
    if (!mysqlHandle)
        return NULL;

    php_mysql_conn* mysqlConn = mysqlHandle.getInternalResource<php_mysql_conn>(mysqlLinkType, mysqlPersistentLinkType);
    if (!mysqlConn)
        return NULL;

    const char* version = NULL;

    if (execEnv->getAgentGlobals().doesMysqlUseMysqlnd) {
#if PHP_VERSION_ID >= 50300
        MYSQLND* mysqlndConn = reinterpret_cast<MYSQLND*>(mysqlConn->conn);
#if PHP_VERSION_ID < 50400
        version = execEnv->callWithTSRMContext(mysqlndConn->m->get_server_information, mysqlndConn);
#else
        MYSQLND_CONN_DATA* mysqlndConnData = mysqlndConn->data;
        version = execEnv->callWithTSRMContext(mysqlndConnData->m->get_server_information, mysqlndConnData);
#endif
#endif
    } else {
        const DelayedFunctions::FunctionTable& delayedFunctions =
            execEnv->getAgentGlobals().delayed_functions;

        const auto & mysqlGetServerInfo = delayedFunctions.get_mysql_get_server_info();
        if (!mysqlGetServerInfo.isValid())
            return NULL;

#if PHP_VERSION_ID >= 50300
        MYSQL* conn = reinterpret_cast<MYSQL*>(mysqlConn->conn);
#else
        // In PHP 5.2, php_mysql_conn embeds the MYSQL struct as the first
        // field, instead of having a pointer to it.
        MYSQL* conn = reinterpret_cast<MYSQL*>(const_cast<php_mysql_conn*>(mysqlConn));
#endif

        version = mysqlGetServerInfo(conn);
    }

    return version;
}

void MySQLHandleInfo::setDatabase(const std::string& database)
{
    m_database = database;
    m_properties[enum2str(DBExitPoint_DATABASE)] = m_database;

    // Generate new ExitCallInfo, because adding just a single database name
    // property may have resulted in a different config rule matching.
    ExitCallInfo exitCallInfo(
        AG(kernel)->getAgentContext()
                  ->getTransactionMonitor()
                  ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
                  ->makeIdentifyingProperties(m_execEnv, NULL, m_properties)
    );
    setExitCallInfo(exitCallInfo);
}

/* {{{ MySQLConnectInterceptor methods */

MySQLConnectInterceptor::MySQLConnectInterceptor()
    : t_Base("MySQLConnectInterceptor",
             InterceptorRegistry::MySQLConnectInterceptor_ID,
             appdynamics::pb::Agent::EXIT_DB)
{
}

bool MySQLConnectInterceptor::getHandle(ZValPointerAny* handlePointer,
                                        const PHPExecEnvironment* execEnv) const
{
    zval* mysqlHandle = execEnv->getReturnValue();

    ZValPointerAny handle(ZValPointerAny::share(mysqlHandle));

    if (!handle || handle.getType() != ZValPointer::ZVal_Resource)
        return false;

    // Save the default link handle so we have access to it later, and keep a
    // reference to it so that it doesn't get freed until we are done with it.
    execEnv->getAgentGlobals().mysqlDefaultLinkHandle = mysqlHandle;
    Z_ADDREF_P(mysqlHandle);

    *handlePointer = handle;
    return true;
}

bool MySQLConnectInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                             const ExitCallInfo* currentExitCallInfo,
                                             const PHPExecEnvironment* execEnv) const
{
    boost::shared_ptr<MySQLHandleInfo> mysqlHandleInfo;

    if (currentExitCallInfo) {
        mysqlHandleInfo = MySQLHandleInfo::create(*currentExitCallInfo, execEnv);
    } else {
        /*
         * Do partial resolution (host/port/socket only).
         */
        StringMap properties;
        if (!mysqlPropertyGenerator.getProperties(execEnv, NULL, properties))
            return false;

        ExitCallInfo exitCallInfo(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(getExitPointType())
                      ->makeIdentifyingProperties(execEnv, NULL, properties));
        if (!exitCallInfo.isValid())
            return false;
        mysqlHandleInfo = MySQLHandleInfo::create(exitCallInfo, execEnv);

        // Save the generated properties in case we need to re-resolve on DB selection
        mysqlHandleInfo->setProperties(properties);
    }

    *handleInfo = mysqlHandleInfo;

    return true;
}

const appdynamics::pb::SnapshotExitCall* MySQLConnectInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                                             TransactionContext* txContext,
                                                                                             Snapshot* snapshot,
                                                                                             const CurrentExitCall* currentExitCall,
                                                                                             uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);
    const boost::shared_ptr<AExitPointHandleInfo>& handleInfo(static_cast<const HandleBasedCurrentExitCall*>(currentExitCall)->getHandleInfo());
    const StringMap& properties(boost::static_pointer_cast<MySQLHandleInfo>(handleInfo)->getProperties());
    const std::string host(properties.find(enum2str(DBExitPoint_HOST))->second);
    const std::string port(properties.find(enum2str(DBExitPoint_PORT))->second);
    const std::string& sqlStatement = std::string("Get DB Connection: ") + host + ":" + port;
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

boost::shared_ptr<AErrorObject> MySQLConnectInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                                                      const PHPExecEnvironment* phpExecEnv)
{
    boost::shared_ptr<AErrorObject> errorObject(AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, "MySQL", phpExecEnv));

    ZValPointerResource mysqlHandle(ZValPointerAny::share(phpExecEnv->getReturnValue()).cast<ZValPointerResource>());
    if (!mysqlHandle)
        return errorObject;

    php_mysql_conn* mysqlConn = mysqlHandle.getInternalResource<php_mysql_conn>(mysqlLinkType, mysqlPersistentLinkType);
    if (!mysqlConn)
        return errorObject;

    return detectMySQLError(mysqlConn, currentExitCall, phpExecEnv);
}

/* }}} */

/* {{{ MySQLCloseInterceptor methods */

void* MySQLCloseInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    zval* const mysqlHandle = (phpExecEnv->getParamCount() < 1)
            ? phpExecEnv->getAgentGlobals().mysqlDefaultLinkHandle
            : phpExecEnv->getParam<zval*>(0);
    if (!mysqlHandle)
        return NULL;

    ZValPointerResource handle(ZValPointerAny::share(mysqlHandle).cast<ZValPointerResource>());
    if (!handle)
        return NULL;

    phpExecEnv->getHandleRegistry().unregisterHandle(handle);
    if (phpExecEnv->getAgentGlobals().mysqlDefaultLinkHandle == mysqlHandle) {
        APPD_ZVAL_PTR_DTOR(&phpExecEnv->getAgentGlobals().mysqlDefaultLinkHandle);
        phpExecEnv->getAgentGlobals().mysqlDefaultLinkHandle = NULL;
    }
    return NULL;
}

void MySQLCloseInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv, void* state)
{
}

/* }}} */

/* {{{ MySQLSelectDBInterceptor methods */

MySQLSelectDBInterceptor::MySQLSelectDBInterceptor()
    : AMethodInterceptor("MySQLSelectDBInterceptor",
                         InterceptorRegistry::MySQLSelectDBInterceptor_ID)
{
}

void* MySQLSelectDBInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    return NULL;
}

void MySQLSelectDBInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                             void* state)
{
    if (phpExecEnv->doesExceptionObjectExist())
        return;

    if (!phpExecEnv->isReturnValueTrue())
        return;

    zend_uintptr_t paramCount = phpExecEnv->getParamCount();
    if (paramCount < 1)
        return;
    zval* const mysqlHandle = (paramCount < 2)
        ? phpExecEnv->getAgentGlobals().mysqlDefaultLinkHandle
        : phpExecEnv->getParam<zval*>(1);
    if (!mysqlHandle)
        return;

    ZValPointerAny handle(ZValPointerAny::share(mysqlHandle));
    if (!handle)
        return;

    ZValPointerString databaseNameZValString(phpExecEnv->getParam<ZValPointerString>(0));
    if (!databaseNameZValString)
        return;

    boost::shared_ptr<MySQLHandleInfo> handleInfo(phpExecEnv->getHandleRegistry().getHandleInfo<MySQLHandleInfo>(handle));
    if (!handleInfo)
        return;
    handleInfo->setDatabase(databaseNameZValString.getStringValue());
}

/* }}} */

/* {{{ MySQLQueryInterceptor methods */

MySQLQueryInterceptor::MySQLQueryInterceptor()
    : t_Base("MySQLQueryInterceptor",
             InterceptorRegistry::MySQLQueryInterceptor_ID,
             appdynamics::pb::Agent::EXIT_DB)
{
}

bool MySQLQueryInterceptor::getHandle(ZValPointerAny* handlePointer,
                                      const PHPExecEnvironment* execEnv) const
{
    zend_uintptr_t paramCount = execEnv->getParamCount();
    if (paramCount < 1)
        return false;
    zval* const mysqlHandle = (paramCount < 2)
        ? execEnv->getAgentGlobals().mysqlDefaultLinkHandle
        : execEnv->getParam<zval*>(1);
    if (!mysqlHandle)
        return false;

    ZValPointerAny handle(ZValPointerAny::share(mysqlHandle));
    if (!handle || handle.getType() != ZValPointer::ZVal_Resource)
        return false;

    *handlePointer = handle;
    return true;
}

const appdynamics::pb::SnapshotExitCall* MySQLQueryInterceptor::createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                                           TransactionContext* txContext,
                                                                                           Snapshot* snapshot,
                                                                                           const CurrentExitCall* exitCall,
                                                                                           uint64_t timeTakenMS) const
{
    BOOST_ASSERT(exitCall != NULL);

    ZValPointerString param0String(execEnv->getParam<ZValPointerString, true>(0));

    if (!param0String)
        return NULL;
    std::string sqlStatement(param0String.getStringValue());
    boost::algorithm::trim(sqlStatement);

    if (sqlStatement.empty())
        return NULL;

    return snapshot->getDBCalls().add(txContext,
                                      exitCall,
                                      sqlStatement,
                                      timeTakenMS);
}

boost::shared_ptr<AErrorObject>
MySQLQueryInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                    const PHPExecEnvironment* phpExecEnv)
{
    boost::shared_ptr<AErrorObject> errorObject(AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, "MySQL", phpExecEnv));

    ZValPointerAny mysqlHandle;
    if (!getHandle(&mysqlHandle, phpExecEnv))
        return errorObject;

    php_mysql_conn* mysqlConn = mysqlHandle.cast<ZValPointerResource>()
                                           .getInternalResource<php_mysql_conn>(mysqlLinkType, mysqlPersistentLinkType);
    if (!mysqlConn)
        return errorObject;

    return detectMySQLError(mysqlConn, currentExitCall, phpExecEnv);
}

/* }}} */

// vim: set fdm=marker:
