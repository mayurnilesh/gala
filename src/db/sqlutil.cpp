/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */

#include "sqlutil.h"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/range/iterator_range.hpp>

#include "agent.h"
#include "common.h"
#include "zval_helper.h"

const std::string kSqlCommit = "Commit";
const std::string kSqlConnect = "Connect";
const std::string kSqlDDL = "DDL";
const std::string kSqlDelete = "Delete";
const std::string kSqlInsert = "Insert";
const std::string kSqlQuery = "Query";
const std::string kSqlRollback = "Rollback";
const std::string kSqlSelect = "Select";
const std::string kSqlUpdate = "Update";

const char* const kSqlPreparedStatement = "Prepared Statement";
const char* const kSqlString = "String";

static const std::string kSqlAlter = "alter";
static const std::string kSqlCreate = "create";
static const std::string kSqlDrop = "drop";

static inline bool hasPrefixIgnoringCase(const std::string& sql, const std::string& prefix)
{
    return boost::istarts_with(boost::make_iterator_range(sql),
                               boost::make_iterator_range(prefix));
}

const std::string& getSQLType(const std::string& sql)
{
    if (hasPrefixIgnoringCase(sql, kSqlInsert))
        return kSqlInsert;

    if (hasPrefixIgnoringCase(sql, kSqlUpdate))
        return kSqlUpdate;

    if (hasPrefixIgnoringCase(sql, kSqlDelete))
        return kSqlDelete;

    if (hasPrefixIgnoringCase(sql, kSqlSelect))
        return kSqlQuery;

    if (hasPrefixIgnoringCase(sql, kSqlCreate) ||
        hasPrefixIgnoringCase(sql, kSqlDrop) ||
        hasPrefixIgnoringCase(sql, kSqlAlter))
        return kSqlDDL;

    return kSqlQuery;
}

void getSQLParameterStringFromZVal(std::string* positionalParameter,
                                   const ZValPointerAny& param,
                                   const PHPExecEnvironment* execEnv)
{
    switch (param.getType()) {
        case ZValPointer::ZVal_Object: {
            const ZValPointerObject object = param.cast<ZValPointerObject>();
            *positionalParameter = boost::str(
                    boost::format("object(%1%)#%2%)") % object.getClassName(execEnv) % object.getInternalHandle());
            break;
        }

        case ZValPointer::ZVal_String:
            *positionalParameter = param.cast<ZValPointerString>().getStringValue();
            break;

        default: {
            const zval* const paramZVal = param.get();
            BOOST_ASSERT(paramZVal);

            const ZValPointerString paramAsString = param.convertToStringAsCopy(execEnv);
            *positionalParameter = paramAsString.getStringValue();
            break;
        }
    }
}
