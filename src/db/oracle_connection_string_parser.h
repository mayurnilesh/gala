/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#ifndef __oracle_connection_string_parser_h
#define __oracle_connection_string_parser_h

#include <string>
#include <vector>
#include <stdint.h>
#include <boost/shared_ptr.hpp>
#include <boost/utility.hpp>
#include <boost/unordered_map.hpp>

namespace oci
{
    class DictionaryNode;
    class StringNode;

    /**
        Dictionary keys in tnsnames.ora are not case sensitive.
     */
    class CaseInsensitiveHash : public std::unary_function<std::string, std::size_t>
    {
    public:
        std::size_t operator() (const std::string&) const;
    };

    class CaseInsensitiveEquals : public std::binary_function<std::string, std::string, bool>
    {
    public:
        bool operator() (const std::string&, const std::string&) const;
    };

    /**
        Base class for all tnsname.ora syntax tree nodes.
     */
    class ASTNode : boost::noncopyable
    {
    public:
        enum Type
        {
            PARAM_LIST,
            PARAM,
            DICTIONARY,
            DICTIONARY_ENTRY,
            STRING
        };

        ASTNode(Type t) : m_type(t) {}
        virtual ~ASTNode() {}

        inline Type type() const { return m_type; }

        inline const DictionaryNode* asDict() const;
        inline const StringNode* asString() const;

    private:
        Type const m_type;
    };

    /**
        tnsnames.ora syntax tree node for a dictionary entry.

        Dictionary entries are of the form:
        (Key=<value>)

        <value> may be a indentifier, quoted string, or a dictionary.
     */
    class DictionaryEntryNode : public ASTNode
    {
    public:
        DictionaryEntryNode(const std::string& key,
                            const boost::shared_ptr<ASTNode>& value);
        inline const std::string& key() const { return m_key; }
        inline const boost::shared_ptr<ASTNode>& value() { return m_value; }
    private:
        std::string const m_key;
        boost::shared_ptr<ASTNode> const m_value;
    };

    /**
        tnsnames.ora syntax tree node for a dictionary.
        A dictionary is a sequence of dictionary entries.

        Multiple entries in the dictionary may have the same value, so
        a multimap is used as an index for the dictionary.

        Example:
        (Key1=<value>)
        (Key2=<value>)
        (Key1=<value>)

     */
    class DictionaryNode : public ASTNode
    {
    public:
        typedef boost::unordered_multimap<std::string,
                                          boost::shared_ptr<DictionaryEntryNode>,
                                          CaseInsensitiveHash,
                                          CaseInsensitiveEquals> t_Map;

        DictionaryNode() : ASTNode(DICTIONARY) {}
        void addEntry(const boost::shared_ptr<DictionaryEntryNode>& entry);
        inline const std::vector< boost::shared_ptr<DictionaryEntryNode> >& entries() const { return m_entries; }
        inline const t_Map& map() const { return m_map; }
    private:
        t_Map m_map;
        std::vector< boost::shared_ptr<DictionaryEntryNode> > m_entries;
    };

    /**
        tnsnames.ora syntax tree node for a parameter.  A parameter has a name
        and a value.  The value of a parameter must be a dictionary.

        Example:
        paramName = (Key=<value>)
     */
    class ParamNode : public ASTNode
    {
    public:
        ParamNode(const std::string& name,
                  const boost::shared_ptr<DictionaryNode> value);
        inline const std::string& name() const { return m_name; }
        inline const boost::shared_ptr<DictionaryNode> value() const { return m_value; }
    private:
        std::string const m_name;
        boost::shared_ptr<DictionaryNode> const m_value;
    };

    /**
        tnsnames.ora syntax tree node for an either tnsnames.ora file.  The
        entire syntax tree is a sequence of parameters.  A multimap is used to
        index the parameters.

        Example:
        connection1 =
          (DESCRIPTION = (ENABLE = BROKEN)
            (LOAD_BALANCE = ON)
            (FAILOVER = ON)
            (ADDRESS = (PROTOCOL = TCP)(HOST = xxxx)(PORT = xxxx))
            (ADDRESS = (PROTOCOL = TCP)(HOST = xxxx)(PORT = xxxx))
            (ADDRESS = (PROTOCOL = TCP)(HOST = xxxx)(PORT = xxxx))
            (CONNECT_DATA = (SERVICE_NAME = xxx))
          )


        connection2 =
          (DESCRIPTION = (ENABLE = BROKEN)
            (FAILOVER = ON)
            (ADDRESS = (PROTOCOL = TCP)(HOST = xxxx)(PORT = xxxx))
            (ADDRESS = (PROTOCOL = TCP)(HOST = xxxx)(PORT = xxxx))
            (CONNECT_DATA = (SERVICE_NAME = xxx))
          )

        connection3 =
          (DESCRIPTION =
            (ADDRESS = (PROTOCOL = TCP)(HOST = xxxx)(PORT = xxxx))
            (CONNECT_DATA = (SERVICE_NAME = xxx))
          )
     */
    class ParamListNode : public ASTNode
    {
    public:
        typedef boost::unordered_multimap<std::string,
                                          boost::shared_ptr<ParamNode>,
                                          CaseInsensitiveHash,
                                          CaseInsensitiveEquals> t_Map;

        ParamListNode() : ASTNode(PARAM_LIST) {}
        void addParam(const boost::shared_ptr<ParamNode>& );
        const std::vector< boost::shared_ptr<ParamNode> >& params() const { return m_params; }
        const t_Map& map() const { return m_map; }
    private:
        t_Map m_map;
        std::vector< boost::shared_ptr<ParamNode> > m_params;
    };

    class Token;

    /**
        tnsnames.ora syntax tree node for a string value.  This can
        be any of:
          A string in single quotes
          A string in double quotes
          A bare word.
     */
    class StringNode : public ASTNode
    {
    public:
        StringNode(const Token& token);
        inline const std::string& value() const { return m_value; }
    private:
        std::string const m_value;
    };

    inline const DictionaryNode* ASTNode::asDict() const
    {
        if (type() != DICTIONARY)
            return NULL;
        return static_cast<const DictionaryNode*>(this);
    }

    inline const StringNode* ASTNode::asString() const
    {
        if (type() != STRING)
            return NULL;
        return static_cast<const StringNode*>(this);
    }

    // forward declare these for generated oci.hh.
    class YYSTYPE;
    class Scanner;



}

#include <generated/db/oci/oci.hh>

#endif
