/*
   Copyright 2015 AppDynamics.
   All rights reserved.
 */

#include "db_exitpoint.h"
#include "pgsql_exitpoint.h"
#include "sqlutil.h"
#include "emalloc_allocator.h"
#include "transactions.h"

#include <boost/algorithm/string.hpp>

const AHandleInfo::TypeTag PostgreSQLHandleInfo::typeTag;

static const char kPostgreSQLBackendName[] = "PostgreSQL";

template <class t_ZValPointerClass>
static t_ZValPointerClass getNthParameter(int parameterIndex,
                                          const ZValPointerAny& handlePointer,
                                          const PHPExecEnvironment* execEnv)
{
    if (execEnv->getParam<zval*>(0) == handlePointer.get())
        parameterIndex++;
    if (parameterIndex >= execEnv->getParamCount())
        return ZValPointerAny().cast<t_ZValPointerClass>();
    return execEnv->getParam<t_ZValPointerClass>(parameterIndex);
}

static long getLongParameter(int parameterIndex,
                             const PHPExecEnvironment* execEnv,
                             long defaultValue = 0)
{
    if (parameterIndex >= execEnv->getParamCount())
        return defaultValue;
    return execEnv->getParam<ZValPointerLong>(parameterIndex).getLongValue();
}

static void markExitCallAsync(appdynamics::pb::SnapshotExitCall* const exitCall)
{
    if (exitCall) {
        appdynamics::pb::Common::NameValuePair* mintime = exitCall->add_properties();
        mintime->set_name("async");
        mintime->set_value(boost::lexical_cast<std::string>(true));
    }
}


class PostgreSQLSendQueryAsyncCommand : public PostgreSQLAsyncCommand {
    public:
        PostgreSQLSendQueryAsyncCommand(const std::string& sqlString)
            : PostgreSQLAsyncCommand(sqlString) {}
};

class PostgreSQLSendQueryParamsAsyncCommand : public PostgreSQLAsyncCommand {
    private:
        const std::vector<std::string> m_params;

    public:
        PostgreSQLSendQueryParamsAsyncCommand(const std::string& sqlString,
                                              const std::vector<std::string>& params)
            : PostgreSQLAsyncCommand(sqlString)
            , m_params(params) {}

        virtual appdynamics::pb::SnapshotExitCall*
                createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                           TransactionContext* txContext,
                                           Snapshot* snapshot,
                                           const CurrentExitCall* currentExitCall,
                                           uint64_t timeTakenMS) const
        {
            appdynamics::pb::SnapshotExitCall* const dbCall =
                    snapshot->getDBCalls().addWithBoundParams(txContext,
                                                              currentExitCall,
                                                              getSqlString(),
                                                              timeTakenMS);
            if (dbCall) {
                appdynamics::pb::BoundParameters* const boundParams = dbCall->mutable_boundparameters();
                boundParams->set_type(appdynamics::pb::BoundParameters::POSITIONAL);
                for (size_t i = 0; i < m_params.size(); ++i)
                    boundParams->add_posparameters(m_params[i]);
            }
            return dbCall;
        }
};

class PostgreSQLSendPrepareAsyncCommand : public PostgreSQLAsyncCommand {
    private:
        const std::string m_stmtName;

    public:
        PostgreSQLSendPrepareAsyncCommand(const std::string& stmtName, const std::string& sqlString)
            : PostgreSQLAsyncCommand(sqlString)
            , m_stmtName(stmtName) {}

        virtual void onSuccess(PostgreSQLHandleInfo* handleInfo)
        {
            handleInfo->getPreparedStmts()[m_stmtName] = getSqlString();
        }
};

class PostgreSQLInsertAsyncCommand : public PostgreSQLAsyncCommand {
    public:
        PostgreSQLInsertAsyncCommand(const std::string& sqlString)
            : PostgreSQLAsyncCommand(sqlString) {}
};

class PostgreSQLSendExecuteAsyncCommand : public PostgreSQLAsyncCommand {
    public:
        PostgreSQLSendExecuteAsyncCommand(const std::string& sqlString)
            : PostgreSQLAsyncCommand(sqlString) {}
};

static boost::shared_ptr<PostgreSQLHandleInfo> getHandleInfo(const CurrentExitCall* currentExitCall)
{
    const boost::shared_ptr<AExitPointHandleInfo>& handleInfo =
        static_cast<const HandleBasedCurrentExitCall*>(currentExitCall)->getHandleInfo();
    return boost::static_pointer_cast<PostgreSQLHandleInfo>(handleInfo);
}

class PostgreSQLPropertyGenerator : public IBackendPropertyGenerator
{
    public:
        virtual bool getProperties(const PHPExecEnvironment* execEnv,
                                   const CurrentExitCall* currentExitCall,
                                   StringMap& properties) const;
        bool isAsync(const PHPExecEnvironment* execEnv);
        bool isPersistent(const PHPExecEnvironment* execEnv);

    private:
        boost::optional<std::string> getPostgreSQLServerVersion(const PHPExecEnvironment* execEnv) const;
};
static PostgreSQLPropertyGenerator pgsqlPropertyGenerator;

static boost::shared_ptr<AErrorObject> detectPGconnError(PGconn* pgConn,
                                                         CurrentExitCall* currentExitCall,
                                                         const PHPExecEnvironment* execEnv)
{
    if (!pgConn)
        return boost::shared_ptr<AErrorObject>();

    const auto& PQerrorMessage = execEnv->getAgentGlobals().delayed_functions.get_PQerrorMessage();
    const char* const errorMessage = PQerrorMessage.isValid() ? PQerrorMessage(pgConn) : NULL;
    if (!errorMessage || !*errorMessage)
        return boost::shared_ptr<AErrorObject>();

    if (currentExitCall)
        currentExitCall->incrementErrorCount(1);
    ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return errorMonitor->reportSyntheticException(execEnv, kPostgreSQLBackendName, errorMessage);
}

static boost::shared_ptr<AErrorObject> detectPGresultError(PGresult* pgResult,
                                                           CurrentExitCall* currentExitCall,
                                                           const PHPExecEnvironment* execEnv)
{
    if (!pgResult)
        return boost::shared_ptr<AErrorObject>();

    const auto& PQresultErrorMessage = execEnv->getAgentGlobals().delayed_functions.get_PQresultErrorMessage();
    const char* const errorMessage = PQresultErrorMessage.isValid() ? PQresultErrorMessage(pgResult) : NULL;
    if (!errorMessage || !*errorMessage)
        return boost::shared_ptr<AErrorObject>();

    if (currentExitCall)
        currentExitCall->incrementErrorCount(1);
    ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return errorMonitor->reportSyntheticException(execEnv, kPostgreSQLBackendName, errorMessage);
}

static const char *param_suffixes[] = {
    " = ",
    " =",
    "= ",
    "=",
};
#define NUM_PARAM_SUFFIXES (sizeof(param_suffixes) / sizeof(param_suffixes[0]))

// TODO: replace this with a proper connection string parser
static boost::optional<std::string> getConnectionStringParameter(
        const std::string& connString,
        const std::string& paramName,
        const boost::optional<std::string>& defaultValue = boost::none)
{
    for (size_t i = 0; i < NUM_PARAM_SUFFIXES; ++i) {
        const std::string needle = paramName + param_suffixes[i];
        const size_t find = connString.find(needle);
        if (find == std::string::npos)
            continue;
        if (find > 0 && !isspace(connString[find-1])) // Should we look again?
            continue;
        const size_t value_start = find + needle.size();
        if (value_start >= connString.size())
            continue;
        if (isspace(connString[value_start]))
            continue;
        const size_t value_end = connString.find(' ', value_start + 1);
        return connString.substr(value_start, value_end == std::string::npos ? value_end : (value_end - value_start));
    }
    return defaultValue;
}

static const std::string DEFAULT_HOST("localhost");

bool PostgreSQLPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                                const CurrentExitCall *currentExitCall,
                                                StringMap& properties) const
{
    if (execEnv->getParamCount() >= 3 && execEnv->getParamCount() <= 5) {
        // Deprecated forms:
        //  pg_connect ( host, port, dbname )
        //  pg_connect ( host, port, options, dbname )
        //  pg_connect ( host, port, options, tty, dbname )
        ZValPointerString hostParam = execEnv->getParam<ZValPointerString>(0);
        ZValPointerString portParam = execEnv->getParam<ZValPointerString>(1);
        ZValPointerString dbnameParam = execEnv->getParam<ZValPointerString>(execEnv->getParamCount() - 1);
        if (!hostParam || !portParam || !dbnameParam)
            return false;
        properties[enum2str(DBExitPoint_HOST)] = hostParam.getStringValue();
        properties[enum2str(DBExitPoint_PORT)] = portParam.getStringValue();
        properties[enum2str(DBExitPoint_DATABASE)] = dbnameParam.getStringValue();
    } else {
        // resource pg_connect ( string $connection_string [, int $connect_type ] )
        // resource pg_pconnect ( string $connection_string [, int $connect_type ] )
        ZValPointerString connectionStringParam = execEnv->getParam<ZValPointerString>(0);
        if (!connectionStringParam)
            return false;
        const std::string connectionString = connectionStringParam.getStringValue();

        properties[enum2str(DBExitPoint_HOST)] = *getConnectionStringParameter(connectionString, "host", DEFAULT_HOST);
        // We cannot assume the default port (5432) because there may be local configuration, e.g. pg_service.conf
        if (boost::optional<std::string> port = getConnectionStringParameter(connectionString, "port"))
            properties[enum2str(DBExitPoint_PORT)] = *port;
        if (boost::optional<std::string> dbname = getConnectionStringParameter(connectionString, "dbname"))
            properties[enum2str(DBExitPoint_DATABASE)] = *dbname;
    }
    properties[enum2str(DBExitPoint_VENDOR)] = POSTGRESQL_VENDOR;

    DBExitPointDelegate* const delegate = static_cast<DBExitPointDelegate*>(
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB));
    if (delegate->isDBVersionNeeded()) {
        if (boost::optional<std::string> version = getPostgreSQLServerVersion(execEnv))
            properties[enum2str(DBExitPoint_VERSION)] = *version;
    }

    return true;
}

bool PostgreSQLPropertyGenerator::isAsync(const PHPExecEnvironment* execEnv)
{
    // resource pg_connect ( string $connection_string [, int $connect_type ] )
    // resource pg_pconnect ( string $connection_string [, int $connect_type ] )
    if (execEnv->getParamCount() < 2)
        return false;
    ZValPointerLong connectType = execEnv->getParam<ZValPointerLong>(1);
    if (!connectType)
        return false; // Deprecated forms of pg_connect will get here.
    return (connectType.getLongValue() & PGSQL_CONNECT_ASYNC) != 0;
}

bool PostgreSQLPropertyGenerator::isPersistent(const PHPExecEnvironment* execEnv)
{
    // resource pg_connect ( string $connection_string [, int $connect_type ] )
    // resource pg_pconnect ( string $connection_string [, int $connect_type ] )
    if (strcmp(execEnv->getFunctionName(), "pg_pconnect"))
        return false;
    if (execEnv->getParamCount() < 2)
        return true;
    ZValPointerLong connectType = execEnv->getParam<ZValPointerLong>(1);
    if (!connectType)
        return true;
    return (connectType.getLongValue() & PGSQL_CONNECT_FORCE_NEW) == 0;
}

boost::optional<std::string>
PostgreSQLPropertyGenerator::getPostgreSQLServerVersion(const PHPExecEnvironment* execEnv) const
{
    ZValPointerResource pgsqlHandle = ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>();
    if (!pgsqlHandle)
        return boost::none;

    PGconn* const pgConn = pgsqlHandle.getInternalResource<PGconn>(pgsqlLinkType, pgsqlPersistentLinkType);
    if (!pgConn)
        return boost::none;

    const auto& PQserverVersion = execEnv->getAgentGlobals().delayed_functions.get_PQserverVersion();
    if (!PQserverVersion.isValid())
        return boost::none;

    int serverVersion = PQserverVersion(pgConn);
    if (!serverVersion)
        return boost::none;

    // Version information is returned as an integer, e.g. version 8.1.2 would return 80102
    std::ostringstream versionStream;
    versionStream << (serverVersion/10000) << '.' << ((serverVersion % 10000) / 100) << '.' << (serverVersion % 100);
    return versionStream.str();
}

boost::shared_ptr<PostgreSQLHandleInfo> PostgreSQLHandleInfo::create(const PHPExecEnvironment* execEnv)
{
    StringMap properties;
    pgsqlPropertyGenerator.getProperties(execEnv, NULL, properties);
    ExitCallInfo exitCallInfo =
        AG(kernel)->getAgentContext()
                  ->getTransactionMonitor()
                  ->getExitPointDelegate(appdynamics::pb::Agent::EXIT_DB)
                  ->makeIdentifyingProperties(execEnv, NULL, properties);
    bool isPersistent = pgsqlPropertyGenerator.isPersistent(execEnv);
    return boost::make_shared<PostgreSQLHandleInfo>(exitCallInfo, execEnv, properties, isPersistent);
}

/* {{{ PostgreSQLConnectInterceptor methods */

bool PostgreSQLConnectInterceptor::getHandle(ZValPointerAny* handlePointer,
                                             const PHPExecEnvironment* execEnv) const
{
    // pg_connect() will return FALSE on failure
    ZValPointerAny handle = ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>();
    if (!handle)
        return false;

    // Save the handle so we can access it in methods where the connection parameter is optional.
    execEnv->getAgentGlobals().pgsqlDefaultLinkHandle = handle.get();
    Z_ADDREF_P(execEnv->getAgentGlobals().pgsqlDefaultLinkHandle);
    *handlePointer = handle;
    return true;
}

bool PostgreSQLConnectInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                                  const ExitCallInfo* currentExitCallInfo,
                                                  const PHPExecEnvironment* execEnv) const
{
    // We resolve the backend in onCallableEnd, so we never
    // expect to have an exit call info in the CurrentExitCall.
    BOOST_ASSERT(!currentExitCallInfo);
    boost::shared_ptr<PostgreSQLHandleInfo> pgHandleInfo = PostgreSQLHandleInfo::create(execEnv);
    if (!pgHandleInfo)
        return false;
    *handleInfo = pgHandleInfo;
    return true;
}

const appdynamics::pb::SnapshotExitCall* PostgreSQLConnectInterceptor::createAndAddSnapshotDBCall(
    const PHPExecEnvironment* execEnv,
    TransactionContext* txContext,
    Snapshot* snapshot,
    const CurrentExitCall* currentExitCall,
    uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    const boost::shared_ptr<AExitPointHandleInfo>& handleInfo =
        static_cast<const HandleBasedCurrentExitCall*>(currentExitCall)->getHandleInfo();
    const StringMap& properties = boost::static_pointer_cast<PostgreSQLHandleInfo>(handleInfo)->getProperties();
    std::string sqlStatement = std::string("Get DB Connection: ") + properties.find(enum2str(DBExitPoint_HOST))->second;
    if (properties.count(enum2str(DBExitPoint_PORT)))
        sqlStatement += ":" + properties.find(enum2str(DBExitPoint_PORT))->second;
    if (properties.count(enum2str(DBExitPoint_DATABASE)))
        sqlStatement += "/" + properties.find(enum2str(DBExitPoint_DATABASE))->second;

    appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       sqlStatement,
                                       timeTakenMS);
    if (dbCall) {
        if (pgsqlPropertyGenerator.isAsync(execEnv))
            markExitCallAsync(dbCall);
    }
    return dbCall;
}

boost::shared_ptr<AErrorObject> PostgreSQLConnectInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                                                           const PHPExecEnvironment* execEnv)
{
    boost::shared_ptr<AErrorObject> errorObject =
            AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, kPostgreSQLBackendName, execEnv);
    // TODO: fetch handle info and call detectPGconnError?
    return errorObject;
}

/* }}} */

/* {{{ PostgreSQLCloseInterceptor methods */

bool PostgreSQLCloseInterceptor::getHandle(ZValPointerAny* handlePointer,
                                           const PHPExecEnvironment* execEnv) const
{
    // bool pg_close ([ resource $connection ] )
    zval* const pgsqlHandle = (execEnv->getParamCount() < 1)
            ? execEnv->getAgentGlobals().pgsqlDefaultLinkHandle
            : execEnv->getParam<zval*>(0);
    if (!pgsqlHandle)
        return false;
    ZValPointerAny handle = ZValPointerAny::share(pgsqlHandle).cast<ZValPointerResource>();
    if (!handle)
        return false;
    *handlePointer = handle;
    return true;
}

// TODO: ensure that errors are detected and reported
void PostgreSQLCloseInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
{
    // bool pg_close ([ resource $connection ] )
    zval* const pgsqlHandle = (execEnv->getParamCount() < 1)
            ? execEnv->getAgentGlobals().pgsqlDefaultLinkHandle
            : execEnv->getParam<zval*>(0);
    if (!pgsqlHandle)
        return;
    if (execEnv->getAgentGlobals().pgsqlDefaultLinkHandle == pgsqlHandle) {
        APPD_ZVAL_PTR_DTOR(&execEnv->getAgentGlobals().pgsqlDefaultLinkHandle);
        execEnv->getAgentGlobals().pgsqlDefaultLinkHandle = NULL;
    }
}

/* }}} */

/* {{{ APostgreSQLMethodInterceptor methods */

bool APostgreSQLMethodInterceptor::getHandle(ZValPointerAny* handlePointer,
                                             const PHPExecEnvironment* execEnv) const
{
    zval* pgsqlHandle;
    if (execEnv->getParamCount() < 1) {
        pgsqlHandle = execEnv->getAgentGlobals().pgsqlDefaultLinkHandle;
    } else {
        pgsqlHandle = execEnv->getParam<zval*>(0);
        if (pgsqlHandle && Z_TYPE_P(pgsqlHandle) != IS_RESOURCE)
            pgsqlHandle = execEnv->getAgentGlobals().pgsqlDefaultLinkHandle;
    }
    if (!pgsqlHandle)
        return false;

    *handlePointer = ZValPointerAny::share(pgsqlHandle).cast<ZValPointerResource>();
    if (!*handlePointer)
        return false;
    return true;
}

boost::shared_ptr<AErrorObject> APostgreSQLMethodInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                                                           const PHPExecEnvironment* execEnv)
{
    BOOST_ASSERT(currentExitCall);
    boost::shared_ptr<AErrorObject> errorObject =
            AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, kPostgreSQLBackendName, execEnv);
    if (errorObject)
        return errorObject;

    const char *errorMessage = NULL;
    ZValPointerAny pgsqlHandle;
    if (execEnv->isReturnValueFalse() && getHandle(&pgsqlHandle, execEnv)) {
        ZValPointerResource resource = pgsqlHandle.cast<ZValPointerResource>();
        PGconn* const pgConn =
                resource ? resource.getInternalResource<PGconn>(pgsqlLinkType, pgsqlPersistentLinkType) : NULL;
        const auto& PQerrorMessage = execEnv->getAgentGlobals().delayed_functions.get_PQerrorMessage();
        errorMessage = (pgConn && PQerrorMessage.isValid()) ? PQerrorMessage(pgConn) : NULL;
    }
    if (!errorMessage)
        return boost::shared_ptr<AErrorObject>();
    LOG4CXX_INFO(execEnv->getAgentGlobals().log_agent,
            "Detected error for " << getClass() << ": " << (errorMessage && *errorMessage ? errorMessage : "<none>"));

    currentExitCall->incrementErrorCount(1);
    ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return errorMonitor->reportSyntheticException(execEnv, kPostgreSQLBackendName, errorMessage);
}

/* }}} */

/* {{{ PostgreSQLQueryInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLQueryInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // resource pg_query ([ resource $connection ], string $query )
    // resource pg_query_params ([ resource $connection ], string $query , array $params )
    ZValPointerAny handle;
    if (!getHandle(&handle, execEnv))
        return NULL;
    ZValPointerString query = getNthParameter<ZValPointerString>(0, handle, execEnv);
    ZValPointerArray params = getNthParameter<ZValPointerArray>(1, handle, execEnv);
    if (!query)
        return NULL;

    const char* const functionName = execEnv->getFunctionName();
    if (!strcmp(functionName, "pg_query") ||
        (!params || params.size() == 0) || !snapshot->captureSQLPositionalBoundParameters())
        return snapshot->getDBCalls().add(txContext,
                                          currentExitCall,
                                          query.getStringValue(),
                                          timeTakenMS);

    BOOST_ASSERT(!strcmp(functionName, "pg_query_params"));
    if (!params)
        return NULL;
    appdynamics::pb::SnapshotExitCall* const dbCall =
        snapshot->getDBCalls().addWithBoundParams(txContext,
                                                  currentExitCall,
                                                  query.getStringValue(),
                                                  timeTakenMS);
    if (dbCall) {
        appdynamics::pb::BoundParameters* const boundParams = dbCall->mutable_boundparameters();
        boundParams->set_type(appdynamics::pb::BoundParameters::POSITIONAL);
        ZValPointerAny param;
        for (long index = 0; params.findEntryByLong(index, &param); ++index) {
            getSQLParameterStringFromZVal(boundParams->add_posparameters(), param, execEnv);
        }
        LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "pg_query_params:\n" << dbCall->DebugString());
    }
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLPrepareInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLPrepareInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // resource pg_prepare ([ resource $connection ], string $stmtname , string $query )
    ZValPointerAny handle;
    if (!getHandle(&handle, execEnv))
        return NULL;
    ZValPointerString name = getNthParameter<ZValPointerString>(0, handle, execEnv);
    ZValPointerString query = getNthParameter<ZValPointerString>(1, handle, execEnv);
    if (!name || !query)
        return NULL;
    const std::string stmtName = name.getStringValue();
    const std::string sqlStatement = query.getStringValue();

    if (!execEnv->isReturnValueFalse()) {
        getHandleInfo(currentExitCall)->getPreparedStmts()[stmtName] = sqlStatement;
    }
    appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       "Prepare statement: " + sqlStatement, // FIXME
                                       timeTakenMS);
    LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "Prepare exit call:\n" + dbCall->DebugString());
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLExecuteInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLExecuteInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // resource pg_execute ([ resource $connection ], string $stmtname , array $params )
    ZValPointerAny handle;
    if (!getHandle(&handle, execEnv))
        return NULL;
    ZValPointerString name = getNthParameter<ZValPointerString>(0, handle, execEnv);
    ZValPointerArray params = getNthParameter<ZValPointerArray>(1, handle, execEnv);
    if (!name || !params)
        return NULL;

    const StringMap& preparedStmts = getHandleInfo(currentExitCall)->getPreparedStmts();
    StringMap::const_iterator find = preparedStmts.find(name.getStringValue());
    const std::string preparedSql = (find == preparedStmts.end()) ? "" : find->second;

    if (params.size() == 0 || !snapshot->captureSQLPositionalBoundParameters())
        return snapshot->getDBCalls().add(txContext,
                                          currentExitCall,
                                          preparedSql,
                                          timeTakenMS);

    appdynamics::pb::SnapshotExitCall* const dbCall =
        snapshot->getDBCalls().addWithBoundParams(txContext,
                                                  currentExitCall,
                                                  preparedSql,
                                                  timeTakenMS);
    if (dbCall) {
        appdynamics::pb::BoundParameters* const boundParams = dbCall->mutable_boundparameters();
        boundParams->set_type(appdynamics::pb::BoundParameters::POSITIONAL);
        ZValPointerAny param;
        for (long index = 0; params.findEntryByLong(index, &param); ++index) {
            getSQLParameterStringFromZVal(boundParams->add_posparameters(), param, execEnv);
        }
        LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "pg_execute bound params:\n" << dbCall->DebugString());
    }
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLSelectInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLSelectInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // mixed pg_select ( resource $connection , string $table_name , array $assoc_array [, int $options = PGSQL_DML_EXEC ] )
    ZValPointerResource handle = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString table = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray array = execEnv->getParam<ZValPointerArray>(2);
    if (!handle || !table || !array)
        return NULL;
    const ulong options = getLongParameter(3, execEnv, PGSQL_DML_EXEC);
    if (!(options & PGSQL_DML_EXEC))
        return NULL;
    /* pg_select() disregards PGSQL_DML_ASYNC entirely */

    const std::string sqlString = boost::str(boost::format("SELECT FROM %1% where %2%")
            % table.getStringValue() % StringHelpers::prettyPrint(array));
    LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "Select SQL: " << sqlString);
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlString,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PostgreSQLGetResultInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLGetResultInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // resource pg_get_result ([ resource $connection ] )
    boost::shared_ptr<PostgreSQLHandleInfo> handleInfo = getHandleInfo(currentExitCall);
    unique_ptr<PostgreSQLAsyncCommand>::type& asyncCommand = handleInfo->getAsyncCommand();
    if (!asyncCommand) {
        LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "pg_get_result called without pending command!");
        return snapshot->getDBCalls().add(txContext,
                                          currentExitCall,
                                          "pg_get_result", // FIXME
                                          timeTakenMS);
    }
    asyncCommand->incrementGetResultCount();
    if (execEnv->isReturnValueFalse()) // TODO: ensure that an error is reported in this case.
        asyncCommand->onFailure(handleInfo.get());
    else
        asyncCommand->onSuccess(handleInfo.get());

    appdynamics::pb::SnapshotExitCall* snapshotExitCall =
            asyncCommand->createAndAddSnapshotDBCall(execEnv,
                                                     txContext,
                                                     snapshot,
                                                     currentExitCall,
                                                     timeTakenMS);
     LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent,
            "Get result snapshot:\n" << (snapshotExitCall ? snapshotExitCall->DebugString() : "<none>"));
     return snapshotExitCall;
}

boost::shared_ptr<AErrorObject> PostgreSQLGetResultInterceptor::detectErrors(
        CurrentExitCall* currentExitCall,
        const PHPExecEnvironment* execEnv)
{
    ZValPointerResource handle = ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerResource>();
    if (!handle)
        return boost::shared_ptr<AErrorObject>();

    pgsql_result_handle* const result_handle =
            handle.getInternalResource<pgsql_result_handle>(pgsqlResultType);
    if (!result_handle || !result_handle->result)
        return boost::shared_ptr<AErrorObject>();

    return detectPGresultError(result_handle->result, currentExitCall, execEnv);
}

/* }}} */

/* {{{ PostgreSQLInsertInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLInsertInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // mixed pg_insert(resource $connection, string $table_name, array $assoc_array [, int $options = PGSQL_DML_EXEC])
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString table = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray data = execEnv->getParam<ZValPointerArray>(2);
    if (!connection || !table || !data)
        return NULL;
    const ulong options = getLongParameter(3, execEnv, PGSQL_DML_EXEC);
    if (!(options & (PGSQL_DML_EXEC | PGSQL_DML_ASYNC)))
        return NULL;

    const std::string sqlString = boost::str(boost::format("INSERT INTO %1% using values %2%")
            % table.getStringValue() % StringHelpers::prettyPrint(data));
    // LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "Insert SQL: " << sqlString);

    appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       sqlString,
                                       timeTakenMS);
    if (options & PGSQL_DML_ASYNC) {
        markExitCallAsync(dbCall);
        if (!execEnv->isReturnValueFalse())
            getHandleInfo(currentExitCall)->getAsyncCommand().reset(new PostgreSQLInsertAsyncCommand(sqlString));
    }
    // Command will get executed twice if both PGSQL_DML_ASYNC and PGSQL_DML_EXEC are set!
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLUpdateInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLUpdateInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // mixed pg_update(resource $connection, string $table_name, array $data, array $condition [, int $options = PGSQL_DML_EXEC])
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString table = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray data = execEnv->getParam<ZValPointerArray>(2);
    ZValPointerArray condition = execEnv->getParam<ZValPointerArray>(3);
    if (!connection || !table || !data || !condition)
        return NULL;

    const ulong options = getLongParameter(4, execEnv, PGSQL_DML_EXEC);
    if (!(options & PGSQL_DML_EXEC))
        return NULL;
    /* pg_update() does not support PGSQL_DML_ASYNC and will generate an error */

    const std::string sqlString = boost::str(boost::format("UPDATE %1% set %2% where %3%")
            % table.getStringValue() % StringHelpers::prettyPrint(data) % StringHelpers::prettyPrint(condition));
    LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "Update SQL: " << sqlString);

    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlString,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PostgreSQLDeleteInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLDeleteInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // mixed pg_delete ( resource $connection , string $table_name , array $assoc_array [, int $options = PGSQL_DML_EXEC ] )
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString table = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray data = execEnv->getParam<ZValPointerArray>(2);
    if (!connection || !table || !data)
        return NULL;
    const ulong options = getLongParameter(3, execEnv, PGSQL_DML_EXEC);
    if (!(options & PGSQL_DML_EXEC))
        return NULL;
    /* pg_delete() does not support PGSQL_DML_ASYNC and will generate an error */

    const std::string sqlString = boost::str(
        boost::format("DELETE FROM %1% WHERE %2%") % table.getStringValue() % StringHelpers::prettyPrint(data));
    LOG4CXX_DEBUG(execEnv->getAgentGlobals().log_agent, "Delete SQL: " << sqlString);

    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlString,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PostgreSQLCancelQueryInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLCancelQueryInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // bool pg_cancel_query ( resource $connection )
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    if (!connection)
        return NULL;

    std::string sqlString = "Cancel query";

    boost::shared_ptr<PostgreSQLHandleInfo> handleInfo = getHandleInfo(currentExitCall);
    unique_ptr<PostgreSQLAsyncCommand>::type& asyncCmd = handleInfo->getAsyncCommand();
    if (asyncCmd) {
        sqlString += ": " + asyncCmd->getSqlString();
        asyncCmd->onCancel(handleInfo.get());
        asyncCmd.reset();
    } else {
        LOG4CXX_WARN(execEnv->getAgentGlobals().log_agent, "pg_cancel_query called without pending async command!");
    }
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlString,
                                      timeTakenMS);
}

/* {{{ PostgreSQLCopyToInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLCopyToInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString table = execEnv->getParam<ZValPointerString>(1);
    if (!connection || !table)
        return NULL;

    const std::string sqlString = boost::str(
        boost::format("COPY %1% TO array") % table.getStringValue());
    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlString,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PostgreSQLCopyFromInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLCopyFromInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString table = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray array = execEnv->getParam<ZValPointerArray>(2);
    if (!connection || !table || !array)
        return NULL;

    const std::string sqlString = boost::str(
            boost::format("COPY FROM %1%-element array to %2%") % array.size() % table.getStringValue());

    return snapshot->getDBCalls().add(txContext,
                                      currentExitCall,
                                      sqlString,
                                      timeTakenMS);
}

/* }}} */

/* {{{ PostgreSQLSendPrepareInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLSendPrepareInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // bool pg_send_prepare ( resource $connection , string $stmtname , string $query )
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString stmtName = execEnv->getParam<ZValPointerString>(1);
    ZValPointerString query = execEnv->getParam<ZValPointerString>(2);
    if (!connection || !stmtName || !query)
        return NULL;

    if (!execEnv->isReturnValueFalse()) {
        boost::shared_ptr<PostgreSQLHandleInfo> handleInfo = getHandleInfo(currentExitCall);
        handleInfo->getAsyncCommand().reset(
                new PostgreSQLSendPrepareAsyncCommand(stmtName.getStringValue(), query.getStringValue()));
    }

    appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       "Prepare async: " + query.getStringValue(),
                                       timeTakenMS);
    markExitCallAsync(dbCall);
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLSendExecuteInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLSendExecuteInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // bool pg_send_execute ( resource $connection , string $stmtname , array $params )
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString stmtName = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray params = execEnv->getParam<ZValPointerArray>(2);
    if (!connection || !stmtName || !params)
        return NULL;

    const StringMap& preparedStmts = getHandleInfo(currentExitCall)->getPreparedStmts();
    StringMap::const_iterator find = preparedStmts.find(stmtName.getStringValue());
    const std::string preparedSql = (find == preparedStmts.end()) ? "" : find->second;

    if (!execEnv->isReturnValueFalse()) {
        boost::shared_ptr<PostgreSQLHandleInfo> handleInfo = getHandleInfo(currentExitCall);
        handleInfo->getAsyncCommand().reset(new PostgreSQLSendExecuteAsyncCommand(preparedSql));
    }

    appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       preparedSql,
                                       timeTakenMS);
    markExitCallAsync(dbCall);
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLSendQueryInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLSendQueryInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    // bool pg_send_query ( resource $connection , string $query )
    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString query = execEnv->getParam<ZValPointerString>(1);
    if (!connection || !query)
        return NULL;

    if (!execEnv->isReturnValueFalse()) {
        getHandleInfo(currentExitCall)->getAsyncCommand().reset(
                new PostgreSQLSendQueryAsyncCommand(query.getStringValue()));
    }

    appdynamics::pb::SnapshotExitCall* const dbCall =
            snapshot->getDBCalls().add(txContext,
                                       currentExitCall,
                                       query.getStringValue(),
                                       timeTakenMS);
    markExitCallAsync(dbCall);
    return dbCall;
}

/* }}} */

/* {{{ PostgreSQLSendQueryParamsInterceptor methods */

const appdynamics::pb::SnapshotExitCall* PostgreSQLSendQueryParamsInterceptor::createAndAddSnapshotDBCall(
        const PHPExecEnvironment* execEnv,
        TransactionContext* txContext,
        Snapshot* snapshot,
        const CurrentExitCall* currentExitCall,
        uint64_t timeTakenMS) const
{
    BOOST_ASSERT(currentExitCall != NULL);

    LOG4CXX_INFO(execEnv->getAgentGlobals().log_agent, "Intercepted pg_send_query_params!");

    ZValPointerResource connection = execEnv->getParam<ZValPointerResource>(0);
    ZValPointerString query = execEnv->getParam<ZValPointerString>(1);
    ZValPointerArray params = execEnv->getParam<ZValPointerArray>(2);
    if (!connection || !query) // TODO: check for params?
        return NULL;

    std::vector<std::string> paramStrings;
    ZValPointerAny param;
    for (long index = 0; params.findEntryByLong(index, &param); ++index) {
        std::string paramString;
        getSQLParameterStringFromZVal(&paramString, param, execEnv);
        paramStrings.push_back(paramString);
    }

    unique_ptr<PostgreSQLAsyncCommand>::type command(new PostgreSQLSendQueryParamsAsyncCommand(
            query.getStringValue(), paramStrings));
    appdynamics::pb::SnapshotExitCall* const dbCall =
            command->createAndAddSnapshotDBCall(execEnv,
                                                txContext,
                                                snapshot,
                                                currentExitCall,
                                                timeTakenMS);
    if (!execEnv->isReturnValueFalse()) {
        getHandleInfo(currentExitCall)->getAsyncCommand().swap(command);
        LOG4CXX_INFO(execEnv->getAgentGlobals().log_agent, "Storing pg_send_query_params query:\n" << query.getStringValue());
    }
    return dbCall;
}

/* }}} */

// vim: set fdm=marker:
