/*
   Copyright 2015 AppDynamics.
   All rights reserved.
 */

#ifndef __pgsql_exitpoint_h
#define __pgsql_exitpoint_h

#include <string>
#include <utility>

#include "common.h"
#include "exitcall_detail.h"
#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "method_selector.h"
#include "sqlutil.h"

class PostgreSQLAsyncCommand {
    private:
        const std::string m_sqlString;
        // TODO: move get result count into handleinfo so that
        // we can track get result counts even without a pending command?
        int m_getResultCount;

    public:
        PostgreSQLAsyncCommand(const std::string& sqlString) : m_sqlString(sqlString), m_getResultCount(0)  {}
        virtual ~PostgreSQLAsyncCommand() {}

        const std::string& getSqlString() const { return m_sqlString; }
        int getGetResultCount() const { return m_getResultCount; }
        void incrementGetResultCount() { m_getResultCount++; }

        virtual appdynamics::pb::SnapshotExitCall*
                createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                           TransactionContext* txContext,
                                           Snapshot* snapshot,
                                           const CurrentExitCall* currentExitCall,
                                           uint64_t timeTakenMS) const {
            return snapshot->getDBCalls().add(txContext,
                                              currentExitCall,
                                              getSqlString(),
                                              timeTakenMS);
        }
        virtual void onSuccess(class PostgreSQLHandleInfo* handleInfo) {}
        virtual void onFailure(class PostgreSQLHandleInfo* handleInfo) {}
        virtual void onCancel(class PostgreSQLHandleInfo* handleInfo) {}
};

/**
 * Handle info class that holds data associated with a particular PostgreSQL
 * connection, including host, port, and database.
 * Instances of this class can only be created via the static create() method.
 */
class PostgreSQLHandleInfo : public AExitPointHandleInfo
{
    private:
        friend class HandleRegistry;
        static const TypeTag typeTag;

    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<PostgreSQLHandleInfo> create(const PHPExecEnvironment* execEnv);

        const StringMap& getProperties() const { return m_properties; }

        bool isPersistent() const { return m_isPersistent; }

        StringMap& getPreparedStmts() { return m_preparedStmts; }
        const StringMap& getPreparedStmts() const { return m_preparedStmts; }

        unique_ptr<PostgreSQLAsyncCommand>::type& getAsyncCommand() { return m_asyncCommand; }

    protected:
        PostgreSQLHandleInfo(const ExitCallInfo& exitCallInfo,
                             const PHPExecEnvironment* execEnv,
                             const StringMap& properties,
                             const bool& isPersistent)
            : AExitPointHandleInfo(exitCallInfo, &typeTag)
            , m_execEnv(execEnv)
            , m_properties(properties)
            , m_isPersistent(isPersistent) {}

    private:
        const PHPExecEnvironment* const m_execEnv;
        // Properties created by the PostgreSQL property generator.
        StringMap m_properties;
        // True if the connection is persistent
        bool m_isPersistent;
        // Map of prepared statements names to SQL (populated by pg_prepare interceptor).
        StringMap m_preparedStmts;
        // Pending async command.
        // Can be set by pg_send_prepare(), pg_send_query(), pg_send_query_params(), or pg_send_execute().
        // It can be cleared by pg_cancel_query().
        unique_ptr<PostgreSQLAsyncCommand>::type m_asyncCommand;
};

class PostgreSQLConnectInterceptor : public ExitCallDetailHelper<HandleInitExitPointInterceptor<PostgreSQLConnectInterceptor>,
                                                                 PostgreSQLConnectInterceptor,
                                                                 appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class HandleInitExitPointInterceptor<PostgreSQLConnectInterceptor>;
        friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<PostgreSQLConnectInterceptor>,
                                          PostgreSQLConnectInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<HandleInitExitPointInterceptor<PostgreSQLConnectInterceptor>,
                                     PostgreSQLConnectInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLConnectInterceptor()
            : t_Base("PostgreSQLConnectInterceptor",
                     InterceptorRegistry::PostgreSQLConnectInterceptor_ID,
                     appdynamics::pb::Agent::EXIT_DB) {}
        virtual ~PostgreSQLConnectInterceptor() {}

        virtual bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const;

        virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                    const ExitCallInfo* currentExitCallInfo,
                                    const PHPExecEnvironment* execEnv) const;

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* execEnv);
    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                            TransactionContext* txContext,
                                                                            Snapshot*,
                                                                            const CurrentExitCall*,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string&) const { return ::kSqlConnect; }
        const char* getStatementType() const { return NULL; }
};

class PostgreSQLCloseInterceptor : public HandleCleanupInterceptor<PostgreSQLCloseInterceptor>
{
    private:
        friend class HandleCleanupInterceptor<PostgreSQLCloseInterceptor>;
        typedef HandleCleanupInterceptor<PostgreSQLCloseInterceptor> t_Base;

    public:
        PostgreSQLCloseInterceptor()
            : t_Base("PostgreSQLCloseInterceptor",
                     InterceptorRegistry::PostgreSQLCloseInterceptor_ID) {}
        virtual ~PostgreSQLCloseInterceptor() {}

        virtual bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const;

        virtual void onCallableEnd(const PHPExecEnvironment* execEnv, void* state);

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_DB);
        }
};

class APostgreSQLMethodInterceptor : public AHandleBasedExitPointInterceptor<APostgreSQLMethodInterceptor,
                                                                             PostgreSQLHandleInfo>
{
    protected:
        APostgreSQLMethodInterceptor(const char* interceptorName,
                                     InterceptorRegistry::ID id)
            : AHandleBasedExitPointInterceptor<APostgreSQLMethodInterceptor,
                                               PostgreSQLHandleInfo>(interceptorName,
                                                                     id,
                                                                     appdynamics::pb::Agent::EXIT_DB) {}
        virtual ~APostgreSQLMethodInterceptor() {}

        virtual bool getHandle(ZValPointerAny* handlePointer, const PHPExecEnvironment* execEnv) const;

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* execEnv);

        static boost::shared_ptr<PostgreSQLHandleInfo> getHandleInfo(const CurrentExitCall* exitCall)
        {
            const boost::shared_ptr<AExitPointHandleInfo>& handleInfo =
                static_cast<const HandleBasedCurrentExitCall*>(exitCall)->getHandleInfo();
            return boost::static_pointer_cast<PostgreSQLHandleInfo>(handleInfo);
        }
};

class PostgreSQLQueryInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                               PostgreSQLQueryInterceptor,
                                                               appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLQueryInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLQueryInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLQueryInterceptor()
            : t_Base("PostgreSQLQueryInterceptor",
                     InterceptorRegistry::PostgreSQLQueryInterceptor_ID) {}
        virtual ~PostgreSQLQueryInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlString; } // FIXME is this correct for pg_query_params()?
};

class PostgreSQLPrepareInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                 PostgreSQLPrepareInterceptor,
                                                                 appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLPrepareInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLPrepareInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLPrepareInterceptor()
            : t_Base("PostgreSQLPrepareInterceptor",
                     InterceptorRegistry::PostgreSQLPrepareInterceptor_ID) {}
        virtual ~PostgreSQLPrepareInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlString; } // FIXME
};

class PostgreSQLExecuteInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                 PostgreSQLExecuteInterceptor,
                                                                 appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLExecuteInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLExecuteInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLExecuteInterceptor()
            : t_Base("PostgreSQLExecuteInterceptor",
                     InterceptorRegistry::PostgreSQLExecuteInterceptor_ID) {}
        virtual ~PostgreSQLExecuteInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlString; }
};

class PostgreSQLSelectInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                PostgreSQLSelectInterceptor,
                                                                appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLSelectInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLSelectInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLSelectInterceptor()
            : t_Base("PostgreSQLSelectInterceptor",
                     InterceptorRegistry::PostgreSQLSelectInterceptor_ID) {}
        virtual ~PostgreSQLSelectInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlQuery; }
        const char* getStatementType() const { return NULL; }
};

class PostgreSQLGetResultInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                   PostgreSQLGetResultInterceptor,
                                                                   appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLGetResultInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLGetResultInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLGetResultInterceptor()
            : t_Base("PostgreSQLGetResultInterceptor",
                     InterceptorRegistry::PostgreSQLGetResultInterceptor_ID) {}
        virtual ~PostgreSQLGetResultInterceptor() {}

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* execEnv);
    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlQuery; } // FIXME
        const char* getStatementType() const { return NULL; }
};

class PostgreSQLInsertInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                PostgreSQLInsertInterceptor,
                                                                appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLInsertInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLInsertInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLInsertInterceptor()
            : t_Base("PostgreSQLInsertInterceptor",
                     InterceptorRegistry::PostgreSQLInsertInterceptor_ID) {}
        virtual ~PostgreSQLInsertInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlInsert; }
        const char* getStatementType() const { return ::kSqlString; }
};

class PostgreSQLUpdateInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                PostgreSQLUpdateInterceptor,
                                                                appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLUpdateInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLUpdateInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLUpdateInterceptor()
            : t_Base("PostgreSQLUpdateInterceptor",
                     InterceptorRegistry::PostgreSQLUpdateInterceptor_ID) {}
        virtual ~PostgreSQLUpdateInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlUpdate; }
        const char* getStatementType() const { return ::kSqlString; }
};

class PostgreSQLDeleteInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                PostgreSQLDeleteInterceptor,
                                                                appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLDeleteInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLDeleteInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLDeleteInterceptor()
            : t_Base("PostgreSQLDeleteInterceptor",
                     InterceptorRegistry::PostgreSQLDeleteInterceptor_ID) {}
        virtual ~PostgreSQLDeleteInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlDelete; }
        const char* getStatementType() const { return ::kSqlString; }
};

class PostgreSQLCancelQueryInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                     PostgreSQLCancelQueryInterceptor,
                                                                     appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLCancelQueryInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLCancelQueryInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLCancelQueryInterceptor()
            : t_Base("PostgreSQLCancelQueryInterceptor",
                     InterceptorRegistry::PostgreSQLCancelQueryInterceptor_ID) {}
        virtual ~PostgreSQLCancelQueryInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return "Cancel"; } // FIXME
        const char* getStatementType() const { return NULL; }
};

// class PostgreSQLCancelQueryInterceptor : public AMethodInterceptor
// {
//     public:
//         PostgreSQLCancelQueryInterceptor()
//             : AMethodInterceptor("PostgreSQLCancelQueryInterceptor",
//                                  InterceptorRegistry::PostgreSQLCancelQueryInterceptor_ID) {}
//         virtual ~PostgreSQLCancelQueryInterceptor() {}
//
//         void* onCallableBegin(const PHPExecEnvironment* execEnv);
//         void onCallableEnd(const PHPExecEnvironment* execEnv, void* state);
//
//         bool needParamsInOnMethodBegin() const { return true; }
//         bool shouldCallOnMethodEnd() const { return true; }
//         bool needReturnValueInOnMethodEnd() const { return true; }
// };
//
class PostgreSQLCopyToInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                PostgreSQLCopyToInterceptor,
                                                                appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLCopyToInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLCopyToInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLCopyToInterceptor()
            : t_Base("PostgreSQLCopyToInterceptor",
                     InterceptorRegistry::PostgreSQLCopyToInterceptor_ID) {}
        virtual ~PostgreSQLCopyToInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlQuery; }
        const char* getStatementType() const { return NULL; }
};

class PostgreSQLCopyFromInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                  PostgreSQLCopyFromInterceptor,
                                                                  appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLCopyFromInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLCopyFromInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLCopyFromInterceptor()
            : t_Base("PostgreSQLCopyFromInterceptor",
                     InterceptorRegistry::PostgreSQLCopyFromInterceptor_ID) {}
        virtual ~PostgreSQLCopyFromInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::kSqlInsert; }
        const char* getStatementType() const { return NULL; }
};

class PostgreSQLSendPrepareInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                     PostgreSQLSendPrepareInterceptor,
                                                                     appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLSendPrepareInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLSendPrepareInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLSendPrepareInterceptor()
            : t_Base("PostgreSQLSendPrepareInterceptor",
                     InterceptorRegistry::PostgreSQLSendPrepareInterceptor_ID) {}
        virtual ~PostgreSQLSendPrepareInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlPreparedStatement; }
};

class PostgreSQLSendExecuteInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                     PostgreSQLSendExecuteInterceptor,
                                                                     appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLSendExecuteInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLSendExecuteInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLSendExecuteInterceptor()
            : t_Base("PostgreSQLSendExecuteInterceptor",
                     InterceptorRegistry::PostgreSQLSendExecuteInterceptor_ID) {}
        virtual ~PostgreSQLSendExecuteInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlPreparedStatement; }
};

class PostgreSQLSendQueryInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                   PostgreSQLSendQueryInterceptor,
                                                                   appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLSendQueryInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLSendQueryInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLSendQueryInterceptor()
            : t_Base("PostgreSQLSendQueryInterceptor",
                     InterceptorRegistry::PostgreSQLSendQueryInterceptor_ID) {}
        virtual ~PostgreSQLSendQueryInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlString; }
};

class PostgreSQLSendQueryParamsInterceptor : public ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                                                         PostgreSQLSendQueryParamsInterceptor,
                                                                         appdynamics::pb::Agent::EXIT_DB>
{
    private:
        friend class ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                          PostgreSQLSendQueryParamsInterceptor,
                                          appdynamics::pb::Agent::EXIT_DB>;
        typedef ExitCallDetailHelper<APostgreSQLMethodInterceptor,
                                     PostgreSQLSendQueryParamsInterceptor,
                                     appdynamics::pb::Agent::EXIT_DB> t_Base;

    public:
        PostgreSQLSendQueryParamsInterceptor()
            : t_Base("PostgreSQLSendQueryParamsInterceptor",
                     InterceptorRegistry::PostgreSQLSendQueryParamsInterceptor_ID) {}
        virtual ~PostgreSQLSendQueryParamsInterceptor() {}

    private:
        const appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment* execEnv,
                                                                            TransactionContext* txContext,
                                                                            Snapshot* snapshot,
                                                                            const CurrentExitCall* currentExitCall,
                                                                            uint64_t timeTakenMS) const;
        std::string getSQLType(const std::string& sql) const { return ::getSQLType(sql); }
        const char* getStatementType() const { return ::kSqlString; }
};

#endif /* __pgsql_exitpoint_h */
