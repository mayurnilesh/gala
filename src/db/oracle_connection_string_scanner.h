/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */
#ifndef __oracle_connection_string_scanner_h
#define __oracle_connection_string_scanner_h

#include <boost/utility.hpp>
#include <stdint.h>
#include <string>

#include "oracle_connection_string_parser.h"

namespace oci
{
    typedef parser::token_type TokenType;

    /**
        Value class to hold tokens returned by the tnsnames.ora scanner.
     */
    class Token
    {
        friend class Scanner;
    public:
        Token();
        Token(const Token&);
        Token& operator=(const Token&);

        std::string text() const;
        inline TokenType type() const { return m_tokenType; }
        inline const oci::location& location() const { return m_location; }

    private:
        Token(const Scanner* scanner, TokenType tokenType, const oci::location& location);

        const Scanner* m_scanner;

        TokenType m_tokenType;
        const uint8_t* m_start;
        size_t m_tokenLength;

        oci::location m_location;
    };

    /**
        Lexigraphical scanner for tokenizing tnsnames.ora files.
     */
    class Scanner : boost::noncopyable
    {
        friend class Token;
    public:
        Scanner(const uint8_t* bufferStart, size_t bufferLength);
        ~Scanner();

        Token nextToken();
    private:
        const uint8_t* fill(const uint8_t* cursor);
        Token returnToken(const uint8_t* cursor, TokenType tokenType);

        const uint8_t* m_currentTokenStart;
        const uint8_t* m_cursor;

        const uint8_t* m_lineStart;
        size_t m_lineNumber;

        oci::position m_tokenStartPosition;

        const uint8_t* YYLIMIT;
        const uint8_t* YYMARKER;
        uint8_t* m_eofBuffer;

    };

    class YYSTYPE
    {
    public:
        boost::shared_ptr<ASTNode> node;
        Token token;
    };

    inline Token::Token()
        : m_scanner(NULL)
        , m_tokenType(parser::token::END)
        , m_start(NULL)
        , m_tokenLength(0)
    {
    }

    inline Token::Token(const Token& other)
        : m_scanner(other.m_scanner)
        , m_tokenType(other.m_tokenType)
        , m_start(other.m_start)
        , m_tokenLength(other.m_tokenLength)
        , m_location(other.m_location)
    {
    }

    inline Token::Token(const Scanner* scanner,
                        TokenType type,
                        const oci::location& location)
        : m_scanner(scanner)
        , m_tokenType(type)
        , m_start(scanner->m_currentTokenStart)
        , m_tokenLength(scanner->m_cursor - scanner->m_currentTokenStart)
        , m_location(location)
    {
    }

    inline Token& Token::operator=(const Token& other)
    {
        m_scanner = other.m_scanner;
        m_tokenType = other.m_tokenType;
        m_start = other.m_start;
        m_tokenLength = other.m_tokenLength;
        m_location = other.m_location;
        return *this;
    }

    inline std::string Token::text() const
    {
        std::string result;
        result.reserve(m_tokenLength);
        bool stripFirstAndLast =
            m_tokenLength >= 2 &&
            ((m_tokenType == parser::token::SINGLE_QUOTED_STRING) ||
            (m_tokenType == parser::token::DOUBLE_QUOTED_STRING));
        const uint8_t* i = m_start + (stripFirstAndLast ? 1 : 0);
        const uint8_t* const end = m_start + m_tokenLength - (stripFirstAndLast ? 1 : 0);
        while (i != end) {
            if (*i == '\\') {
                ++i;
                if (i == end)
                    break;
            }
            result.push_back(*i);
            ++i;
        }
        return result;
    }

    inline int ocilex(YYSTYPE* value, oci::location* pos, Scanner* scanner)
    {
        value->token = scanner->nextToken();
        *pos = value->token.location();
        if (value->token.type() == parser::token::END)
            return 0;
        return value->token.type();
    }
}

#endif
