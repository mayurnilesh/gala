/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */
#ifndef __timer_cgt_thread_h
#define __timer_cgt_thread_h
#include <signal.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <boost/atomic.hpp>

#include "common.h"
#include "deferred_instance.h"

#if defined(__APPLE__)
#include <mach/mach.h>
#include <mach/mach_time.h>
#endif

/**
    Class to provide time stamps that are accurate to about 1ms.
 */
class Timer
{
    public:
        Timer();
        ~Timer();

        void init(bool useSharedMemory, uint64_t nanoSecondsToSleep);
        /*
         * Callback for beginning of request.
         */
        void onRequestBegin();

        /**
         * Callback for end of request.
         */
        void onRequestEnd();

        /*
         * Returns the current timestamp, in microseconds
         */
        uint64_t getTimestamp() const;

        /*
         * Returns the elapsed time since the provided timestamp. Both values
         * are in microseconds.
         */
        uint64_t getElapsedTime(uint64_t startTime) const;

        /*
         * Return the elapsed time given the start and end timestamps. Both
         * values are in microseconds.
         */
        uint64_t getElapsedTime(uint64_t startTime, uint64_t endTime) const;

        void beforeFork();
        void afterForkParent();
        void afterForkChild();

        /*
         * Returns current system time, in milliseconds.
         */
        static uint64_t getCurrentTime();

    private:
        static const uint32_t s_MinMicroSecondTimeStamp = 1;
        static const uint32_t s_MaxMicroSecondTimeStamp = 0xFFFFFFFF;
        static const uint32_t s_DefaultNanoSecondsToSleep = 500000;

        enum State
        {
            /**
                A client of this class has requested that the timer thread start by
                calling onRequestBegin, but the time thread has not yet started
                updating m_currentMicroSecondTime.
                <p>
                Legal transitions: STARTING to RUNNING, STARTING to PAUSING,
                STARTING to STOPPING.
             */
            STARTING,

            /**
                The timer thread is in the loop that continuously updates
                m_currentMicroSecondTime.
                <p>
                Legal transitions: RUNNING to PAUSING, RUNNING to STOPPING
             */
            RUNNING,

            /**
                A client of this class has requested that the timer thread pause by
                blocking on a condition variable by calling onRequestEnd.
                <p>
                When the state variable is PAUSING, the timer thread might be in the
                early stages of starting up or in the loop that continuously
                updates m_currentMicroSecondTime.
                <p>
                Legal transitions: PAUSING to PAUSED, PAUSING to STARTING,
                PAUSING to STOPPING
             */
            PAUSING,
            /**
                The timer thread is blocked on a condition variable waiting to be
                signaled by another thread that calls onRequestBegin on onRequestEnd.
                <p>
                Legal transitions: PAUSED to STARTING, PAUSED to STOPPING
             */
            PAUSED,
            /**
                The destructor of this class has been invoked and the timer thread
                needs to be stopped.
                <p>
                Legal transitions: STOPPING to STOPPED
             */
            STOPPING,
            /**
                The timer thread has ended or is about to end.
             */
            STOPPED
        };

        class CurrentTime
        {
        public:
            CurrentTime(bool useSharedMemory);

            uint64_t get() const;
            void set(uint64_t currentTime);
        private:
            boost::atomic<uint64_t>* m_currentTime;
            void* m_sharedPage;
            DeferredInstance<boost::atomic<uint64_t> > m_nonSharedCurrentTime;
        };

        uint64_t stamp();

        bool startThread();
        void pauseThread();
        void stopThread();

        /**
            Updates the number of nano seconds to sleep between updates
            to the "current" time.  The new value is not used until the timer thread
            transitions to the running state.  If the timer is already in the running
            state the new value is not used until the timer leaves the running state
            and re-enters the running state.
            @param nanoSecondsToSleep the number of nano seconds to sleep between updates
            to the "current" time.
         */
        void setNanoSecondsToSleep(uint64_t nanoSecondsToSleep);

        static void* threadProc(void* pThis);
        void threadMethod();

        pthread_t m_thread;
        pthread_mutex_t m_mutex;
        pthread_cond_t m_condition;

        uint64_t m_lastStamp;

        /*
            State variable.  Possible states are listed in the above enum.
            This state variable is never written when the mutex is not held, however
            the background timer thread does read the state variable while the mutex
            is not held while the timer thread is in the loop that continuously updates
            m_currentMicroSecondTime.
         */
        boost::atomic<State>  m_state;
        DeferredInstance<CurrentTime> m_currentTime;


        /**
            The number of nanoseconds to sleep between updates to the current
            time.
         */
        uint64_t m_nanoSecondsToSleep;

        bool m_useSharedMemory;
        pid_t m_owningProcess;
};

inline void ALWAYS_INLINE_IN_RELEASE Timer::init(bool useSharedMemory, uint64_t nanoSecondsToSleep)
{
    m_useSharedMemory = useSharedMemory;
    m_currentTime.construct(useSharedMemory);
    setNanoSecondsToSleep(nanoSecondsToSleep);
    if (m_useSharedMemory)
        startThread();
}

inline Timer::Timer()
    : m_lastStamp(0)
    , m_state(STOPPED)
    , m_nanoSecondsToSleep(s_DefaultNanoSecondsToSleep)
    , m_owningProcess(0)

{
    pthread_mutex_init(&m_mutex, NULL);
    pthread_cond_init(&m_condition, NULL);
}

inline Timer::~Timer()
{
    stopThread();
    pthread_mutex_destroy(&m_mutex);
    pthread_cond_destroy(&m_condition);
}

inline bool Timer::startThread()
{
    pthread_mutex_lock(&m_mutex);
    // startThead can't be called when we are trying to stop the thread.
    // We do not allow startThread while in the STOPPING state
    // because stopThread blocks until the thread can be joined.  If this method
    // whacked the state back to STARTING, stopThread may never
    // return!
    BOOST_ASSERT(m_state.load(boost::memory_order_relaxed) != STOPPING);
    switch (m_state.load(boost::memory_order_relaxed))
    {
    case STARTING:
        // Already starting, nothing to do!
        break;
    case RUNNING:
        // Already running, nothing to do!
        break;

    case STOPPED:
        {
            int createRetVal = pthread_create(&m_thread, NULL, &threadProc, this);
            if (createRetVal) {
                // pthread_create failed!!!
                // TODO Need to get a logger in here.
                m_state.store(STOPPED, boost::memory_order_relaxed);
                return false;
            }
            m_owningProcess = getpid();
            // Thread we just created will block on the mutex, until
            // we release it at the end of this function.
            m_state.store(STARTING, boost::memory_order_relaxed);

            // By setting m_currentTime to stamp() here
            // we avoid the need to wait until the background
            // thread wakes up and starts running again.
            //
            // Any time stamps returned by this Timer implementation
            // will report no time has passed until the thread gets around
            // to waking up, which should be fine.
            m_currentTime.get()->set(stamp());
        }
        break;
    case PAUSING:
        // Since we hold the mutex here, we know that the thread is not yet
        // in the paused state, it is probably still taking time stamps.
        // We want to support transitioning from PAUSING directly to
        // STARTING so that nobody has to wait for the state to finish
        // the PAUSING to PAUSED transition.  The good news is that
        // this code can just assume we are already in the PAUSED state.
        m_state.store(STARTING, boost::memory_order_relaxed);

        // By setting m_currentTime to stamp() here
        // we avoid the need to wait until the background
        // thread wakes up and starts running again.
        //
        // Any time stamps returned by this Timer implementation
        // will report no time has passed until the thread gets around
        // to waking up, which should be fine.
        m_currentTime.get()->set(stamp());
        break;
    case PAUSED:
        m_state.store(STARTING, boost::memory_order_relaxed);

        // By setting m_currentTime to stamp() here
        // we avoid the need to wait until the background
        // thread wakes up and starts running again.
        //
        // Any time stamps returned by this Timer implementation
        // will report no time has passed until the thread gets around
        // to waking up, which should be fine.
        m_currentTime.get()->set(stamp());

        pthread_cond_signal(&m_condition);
        break;
    default:
        BOOST_ASSERT_MSG(false, "Invalid timer state");
        break;
    }
    BOOST_ASSERT(m_state.load(boost::memory_order_relaxed) == STARTING);
    pthread_mutex_unlock(&m_mutex);
    return true;
}

inline void Timer::pauseThread()
{
    pthread_mutex_lock(&m_mutex);
    // pauseThread can't be called when we are trying to stop the thread.
    // We do not allow pauseThread while in the STOPPING state
    // because stopThread blocks until the thread can be joined.  If this method
    // whacked the state to PAUSING, stopThread may never return!
    BOOST_ASSERT(m_state.load(boost::memory_order_relaxed) != STOPPING);
    switch (m_state.load(boost::memory_order_relaxed))
    {
    case PAUSING:
        // Already pausing, nothing to do!
        break;
    case PAUSED:
        // Already PAUSED, nothing to do!
        break;
    case STOPPED:
        break;
    case STARTING:
        // Since we hold the mutex here, we know that the thread is not yet
        // in the running state.
        // We want to support transitioning from STARTING directly to
        // PAUSING so that nobody has to wait for the state to finish
        // the STARTING to RUNNING transition.  The good news is that
        // this code can just assume we are already in the RUNNING state
        // and fall through to that case below.
    case RUNNING:
        m_state.store(PAUSING, boost::memory_order_relaxed);
        break;
    default:
        BOOST_ASSERT_MSG(false, "Invalid timer state");
        break;
    }
    pthread_mutex_unlock(&m_mutex);
}

inline void Timer::stopThread()
{
    pthread_mutex_lock(&m_mutex);
    if (m_owningProcess != getpid()) {
        pthread_mutex_unlock(&m_mutex);
        return;
    }

    switch (m_state.load(boost::memory_order_relaxed))
    {
    case STOPPED:
        break;
    case PAUSED:
        // To make the thread leave the paused state, we need to whack the state
        // variable to STOPPING, signal the condition, and release the mutex.
        // We can signal the condition first because we still have the mutex.
        // After signalling the condition the PAUSED to STOPPING transition
        // is the same as the RUNNING to STOPPING transition, so we'll fall through
        // to the RUNNING case.
        pthread_cond_signal(&m_condition);
    case PAUSING:
        // Since we hold the mutex here, we know that the thread is not yet
        // in the paused state, it is probably still taking time stamps.
        // We want to support transitioning from PAUSING directly to
        // STOPPING so that nobody has to wait for the state to finish
        // the PAUSING to PAUSED transition.  The good news is that
        // this code can just whack the state to stopping by falling
        // through.
    case RUNNING:
    case STARTING:
        // To make the thread leave the running state, we need to what the state
        // variable to STOPPING.
        m_state.store(STOPPING, boost::memory_order_relaxed);

    case STOPPING:
        // Release the mutex so the thread
        // can proceed, wait for the thread to end,
        // and grab the mutex.  We grab the mutex
        // here to balance the unlock call at the end of this method.
        pthread_mutex_unlock(&m_mutex);
        pthread_join(m_thread, NULL);
        pthread_mutex_lock(&m_mutex);
    }

    pthread_mutex_unlock(&m_mutex);
}

inline void Timer::setNanoSecondsToSleep(uint64_t nanoSecondsToSleep)
{
    pthread_mutex_lock(&m_mutex);
    m_nanoSecondsToSleep = nanoSecondsToSleep;
    pthread_mutex_unlock(&m_mutex);
}

inline void* Timer::threadProc(void* pOpaqueThis)
{
    // Disable all signals in this thread
    sigset_t signal_set;
    int rc = sigfillset (&signal_set);
    BOOST_ASSERT(rc == 0);
    rc = pthread_sigmask (SIG_BLOCK, &signal_set, NULL);
    BOOST_ASSERT(rc == 0); (void)rc;
    Timer* pThis = reinterpret_cast<Timer*>(pOpaqueThis);
    pThis->threadMethod();
    return NULL;
}

inline void ALWAYS_INLINE_IN_RELEASE Timer::threadMethod()
{
    pthread_mutex_lock(&m_mutex);
    while (m_state.load(boost::memory_order_relaxed) != STOPPING) {
        BOOST_ASSERT(m_state.load(boost::memory_order_relaxed) != STOPPED);
        BOOST_ASSERT(m_state.load(boost::memory_order_relaxed) != PAUSED);
        switch (m_state.load(boost::memory_order_relaxed)) {
            case STARTING:
                {
                    time_t secondsToSleep = m_nanoSecondsToSleep / static_cast<uint64_t>(1000000000);
                    long nanosToSleep = m_nanoSecondsToSleep % static_cast<uint64_t>(1000000000);
                    const timespec sleepTimeSpec = { secondsToSleep, nanosToSleep };
                    m_state.store(RUNNING, boost::memory_order_relaxed);
                    pthread_mutex_unlock(&m_mutex);
                    while (m_state.load(boost::memory_order_relaxed) == RUNNING) {
                        uint64_t const currentTime = stamp();
                        m_currentTime.get()->set(currentTime);
                        nanosleep(&sleepTimeSpec, NULL);
                    }
                    pthread_mutex_lock(&m_mutex);
                }
                break;
            case PAUSING:
                m_state.store(PAUSED, boost::memory_order_relaxed);
                while (m_state.load(boost::memory_order_relaxed) == PAUSED) {
                    pthread_cond_wait(&m_condition, &m_mutex);
                }
                break;
        }
    }

    m_state.store(STOPPED, boost::memory_order_relaxed);
    pthread_mutex_unlock(&m_mutex);
}

/**
 * Get the timestamp via clock_gettime() syscall.
 *
 * @return 64 bit unsigned integer
 */
inline uint64_t ALWAYS_INLINE_IN_RELEASE Timer::stamp()
{
#if defined(__APPLE__)
    uint64_t const newStamp = mach_absolute_time() / 1000ULL;
#else
    struct timespec tv;
    clock_gettime(CLOCK_MONOTONIC, &tv);
    uint64_t const newStamp =
        (static_cast<uint64_t>(tv.tv_sec) * 1000000ULL) + (static_cast<uint64_t>(tv.tv_nsec) / 1000ULL);
#endif
    if (newStamp < m_lastStamp)
        return m_lastStamp;
    m_lastStamp = newStamp;
    return newStamp;
}

inline uint64_t ALWAYS_INLINE_IN_RELEASE Timer::getCurrentTime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (uint64_t)tv.tv_sec * 1000L + tv.tv_usec / 1000L;
}

inline void ALWAYS_INLINE_IN_RELEASE Timer::onRequestBegin()
{
    if (m_useSharedMemory)
        return;
    startThread();
}

inline void ALWAYS_INLINE_IN_RELEASE Timer::onRequestEnd()
{
    if (m_useSharedMemory)
        return;
    pauseThread();
}

inline uint64_t ALWAYS_INLINE_IN_RELEASE Timer::getTimestamp() const
{
    uint64_t const result = m_currentTime.get()->get();

    // ** WE HAVE CODE THAT ASSUMES THIS METHOD
    // NEVER RETURNS 0.
    BOOST_ASSERT(result != 0);
    return result;
}

inline uint64_t ALWAYS_INLINE_IN_RELEASE Timer::getElapsedTime(uint64_t startTime) const
{
    uint64_t const endStamp = getTimestamp();
    // thread that updates m_currentMicroSecondTime, should never
    // let m_currentMicroSecondTime go backwards.
    BOOST_ASSERT(startTime <= endStamp);

    uint64_t elapsedTime = endStamp - startTime;
    return elapsedTime;
}

inline uint64_t ALWAYS_INLINE_IN_RELEASE Timer::getElapsedTime(uint64_t startTime, uint64_t endTime) const
{
    // thread that updates m_currentMicroSecondTime, should never
    // let m_currentMicroSecondTime go backwards.
    BOOST_ASSERT(startTime <= endTime);

    return (endTime - startTime);
}

inline void Timer::beforeFork()
{
    pthread_mutex_lock(&m_mutex);
}

inline void Timer::afterForkParent()
{
    pthread_mutex_unlock(&m_mutex);
}

inline void Timer::afterForkChild()
{
    m_state.store(STOPPED, boost::memory_order_relaxed);
    pthread_mutex_unlock(&m_mutex);
}

inline Timer::CurrentTime::CurrentTime(bool useSharedMemory)
{
    if (!useSharedMemory) {
        m_nonSharedCurrentTime.construct(0);
        m_currentTime = m_nonSharedCurrentTime.get();
        return;
    }

    m_sharedPage = mmap(NULL,
                        sizeof(boost::atomic<uint64_t>),
                        PROT_READ | PROT_WRITE,
                        MAP_ANONYMOUS | MAP_SHARED,
                        -1,
                        0);
    BOOST_ASSERT(m_sharedPage);
    m_currentTime = new (m_sharedPage) boost::atomic<uint64_t>(0);
}

inline uint64_t ALWAYS_INLINE_IN_RELEASE Timer::CurrentTime::get() const
{
    return m_currentTime->load(boost::memory_order_relaxed);
}

inline void ALWAYS_INLINE_IN_RELEASE Timer::CurrentTime::set(uint64_t currentTime)
{
    m_currentTime->store(currentTime, boost::memory_order_relaxed);
}

#endif /* __timer_cgt_thread_h */
