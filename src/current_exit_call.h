/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __current_exit_call_h
#define __current_exit_call_h

#include <boost/utility.hpp>
#include "services.h"
#include "timer.h"
#include "exitpoint.h"

/**
    Base class used by exit point interceptors
    to track the elapsed time of an exit call.
 */
class CurrentExitCall : boost::noncopyable
{
public:
    CurrentExitCall(const uint64_t sequenceNumber);
    virtual ~CurrentExitCall();

    /**
        @return the number of microseconds that have elapsed
        since this object was created.
     */
    uint64_t getElapsedTimeInUs() const { return m_elapsedTimeInUs; }

    /**
        @return the backend identifier, if any that has been computed
        for this exit call.
     */
    const BackendIdentifierPtr& getBackendID() const;

    /**
        @return true if a valid backend id has been computed
        for this exit call, false otherwise.
     */
    bool hasBackendID() const;

    /**
     * @return true if the backend ID describes a registered backend
     */
    bool hasRegisteredBackendID() const;

    /**
        Sets the backend id that has been computed for this exit call.
        <p>
        This method should be called a backend resolver.
        @param backendCallID The identifier for this exit call, must not be null.
     */
    void setBackendID(const BackendIdentifierPtr& backendID);

    /**
     * @return the component ID for this exit call
     */
    const std::string& getComponentID() const;

    /**
     * Sets the component ID (resolved or unresolved) in the string form.
     * @param componentID the component ID for this exit call
     */
    void setComponentID(const std::string& componentID);

    /**
     * @return true if this exit call is associated with a component
     */
    bool hasComponentID() const;

    /**
     * @return true if the downstream transaction context propagation should
     * be disabled for the exit call.
     */
    bool isPropagationDisabled() const;

    /**
     * Disables the downstream transaction context propagation for this exit
     * call.
     */
    void disablePropagation();

    void setCorrelationEnabledByConfig(bool value);

    bool isCorrelationEnabledByConfig() const;

    /**
     * Return the backend category for this exit call.
     */
    const std::string& getCategory() const;

    /**
     * Sets the backend category for this exit call.
     *
     * @param category The category to set (may be empty)
     */
    void setCategory(const std::string& category);

    /**
        @return A const pointer to the ExitCallInfo member variable in
        this class.
     */
    const ExitCallInfo* getExitCallInfo() const;

    /**
     * @param exitCallInfo An ExitCallInfo structure to construct this
     * instance's member variable from.
     */
    void setExitCallInfo(const ExitCallInfo& exitCallInfo);

    /**
     * Marks the end of the exit call. This computes the elapsed time, which
     * may be based on something other than the recorded start time and
     * current time.
     */
    virtual uint64_t endExitCall();

    /**
     * Increments the error count of the exit call by a specified amount.
     * @param nErrors Amount to increase the error count by.
     */
    void incrementErrorCount(unsigned nErrors);

    /**
     * Get the error count of the exit call.
     * @return The error count.
     */
    unsigned getErrorCount() const;

    /**
     * Return the last error message reported during the exit call.
     * @return The error message.
     */
    const std::string* getErrorMessage() const;

    /**
     * Save the error message reported during the exit call.
     * @param message The error message.
     */
    void setErrorMessage(const char* message);

    /**
     * Get the exit call sequence number for this exit call.  This number
     * uniquely identifies this exit call in the current BT.  This number
     * is sent to continuing tiers via the correlation header and to
     * the controller in snapshot exit calls.
     * @return the exit call sequence number for this exit call.
     */
    uint64_t getSequenceNumber() const;

    inline void addSnapshotDebugProperty(const std::string& name, const std::string& value);

    inline const std::vector<boost::shared_ptr<NameValuePair>>& getSnapshotDebugProperties() const;

protected:
    uint64_t m_startTimestamp;
    uint64_t m_elapsedTimeInUs;

private:
    BackendIdentifierPtr m_backendID;
    std::string m_componentID;
    ExitCallInfo* m_exitCallInfo;
    unsigned m_errorCount;
    std::string m_category;
    unique_ptr<std::string>::type m_errorMessage;
    uint64_t const m_sequenceNumber;
    bool m_correlationEnabledByConfig;
    bool m_propagationDisabled;

    // This is meant for interceptors that wish to stuff detailed debug
    // information into SnapshotExitCall properties.
    std::vector<boost::shared_ptr<NameValuePair>> m_snapshotDebugProperties;
};

inline CurrentExitCall::CurrentExitCall(uint64_t sequenceNumber)
    : m_startTimestamp(AgentKernel::getTimer()->getTimestamp())
    , m_elapsedTimeInUs(0)
    , m_exitCallInfo(NULL)
    , m_errorCount(0)
    , m_sequenceNumber(sequenceNumber)
    , m_correlationEnabledByConfig(true)
    , m_propagationDisabled(false)
{
}

inline uint64_t CurrentExitCall::endExitCall()
{
    const Timer* timer = AgentKernel::getTimer();
    m_elapsedTimeInUs = timer->getElapsedTime(m_startTimestamp);
    return m_elapsedTimeInUs;
}

inline const BackendIdentifierPtr& CurrentExitCall::getBackendID() const
{
    return m_backendID;
}

inline bool CurrentExitCall::hasBackendID() const
{
    return m_backendID ? true : false;
}

inline void CurrentExitCall::setBackendID(const BackendIdentifierPtr& backendID)
{
    BOOST_ASSERT(backendID != NULL);
    m_backendID = backendID;
}

inline bool CurrentExitCall::hasRegisteredBackendID() const
{
    return (m_backendID && m_backendID->type() == appdynamics::pb::BackendIdentifier::REGISTERED);
}

inline bool CurrentExitCall::hasComponentID() const
{
    return !m_componentID.empty();
}

inline const std::string& CurrentExitCall::getComponentID() const
{
    return m_componentID;
}

inline void CurrentExitCall::setComponentID(const std::string& componentID)
{
    m_componentID = componentID;
}

inline bool CurrentExitCall::isPropagationDisabled() const
{
    return m_propagationDisabled;
}

inline void CurrentExitCall::disablePropagation()
{
    m_propagationDisabled = true;
}

inline bool CurrentExitCall::isCorrelationEnabledByConfig() const
{
    return m_correlationEnabledByConfig;
}

inline void CurrentExitCall::setCorrelationEnabledByConfig(bool value)
{
    m_correlationEnabledByConfig = value;
}

inline const ExitCallInfo* CurrentExitCall::getExitCallInfo() const
{
    return m_exitCallInfo;
}

inline void CurrentExitCall::setExitCallInfo(const ExitCallInfo& exitCallInfo)
{
    delete m_exitCallInfo;
    m_exitCallInfo = new ExitCallInfo(exitCallInfo);
    const appdynamics::pb::Agent::BackendRule* rule = m_exitCallInfo->getMatchedConfigRule();
    if (rule)
        setCorrelationEnabledByConfig(rule->correlationenabled());
}

inline void CurrentExitCall::incrementErrorCount(unsigned nErrors)
{
    m_errorCount += nErrors;
}

inline unsigned CurrentExitCall::getErrorCount() const
{
    return m_errorCount;
}

inline const std::string& CurrentExitCall::getCategory() const
{
    return m_category;
}

inline void CurrentExitCall::setCategory(const std::string& category)
{
    m_category = category;
}

inline const std::string* CurrentExitCall::getErrorMessage() const
{
    return m_errorMessage.get();
}

inline void CurrentExitCall::setErrorMessage(const char* message)
{
    m_errorMessage.reset(new std::string(message));
}

inline uint64_t CurrentExitCall::getSequenceNumber() const
{
    return m_sequenceNumber;
}

inline void CurrentExitCall::addSnapshotDebugProperty(const std::string& name,
                                                      const std::string& value)
{
    m_snapshotDebugProperties.push_back(boost::make_shared<NameValuePair>(name, value));
}

inline const std::vector<boost::shared_ptr<NameValuePair>>&
CurrentExitCall::getSnapshotDebugProperties() const
{
    return m_snapshotDebugProperties;
}

#endif
