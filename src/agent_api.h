/*
   Copyright 2014 AppDynamics.
   All rights reserved.
 */

#ifndef __agent_api_h
#define __agent_api_h

extern "C"
{
#include "php.h"
}

#include "current_exit_call.h"

// + register ADExitCall class (private constructor)
// + register exit type constants
// - add start/end exit call functions
// - find out disallowed chars in display name & property names/values

class APICurrentExitCall : public CurrentExitCall
{
    public:
        APICurrentExitCall(uint64_t sequenceNumber, bool exclusive, AgentLogger& logger)
            : CurrentExitCall(sequenceNumber)
            , m_logger(logger)
            , m_ended(false)
            , m_exclusive(exclusive)
        {
        }

        virtual ~APICurrentExitCall()
        {
            if (!isEnded()) {
                LOG4CXX_DEBUG(m_logger, "API exit call destroyed without being ended, not reporting");
            }
        }

        inline void startExitCall()
        {
            m_startTimestamp = AgentKernel::getTimer()->getTimestamp();
        }

        inline uint64_t endExitCall()
        {
            m_ended = true;
            return CurrentExitCall::endExitCall();
        }

        inline bool isEnded() const { return m_ended; }

        inline bool isExclusive() const { return m_exclusive; }

        inline std::string* getSqlQuery() const { return m_sqlQuery.get(); }

        inline void setSqlQuery(const char* sqlQuery) { m_sqlQuery.reset(new std::string(sqlQuery)); }

        inline SafeModuleShutdownWrapper<ZValPointerAny> getBoundParamsArray() const { return m_boundParamsArray; }

        inline void setBoundParamsArray(const SafeModuleShutdownWrapper<ZValPointerAny>& boundParamsArray) 
        {
            m_boundParamsArray = boundParamsArray;
        }

    private:
        AgentLogger& m_logger;
        bool m_ended;
        bool m_exclusive;
        unique_ptr<std::string>::type m_sqlQuery;
        SafeModuleShutdownWrapper<ZValPointerAny> m_boundParamsArray;
};

/**
 * in PHP 7, the _zend_object struct utilizes a struct hack
 * and therefore needs to be placed in the END of a custom
 * PHP object like php_exit_call_object. We kept the original order for PHP 5.x
 */
#if PHP_VERSION_ID >= 70000
typedef struct {
    APICurrentExitCall* inner;
    zend_object std;
} php_exit_call_object;
#else
typedef struct {
    zend_object std;
    APICurrentExitCall* inner;
} php_exit_call_object;
#endif

extern zend_class_entry* exit_call_ce;

#if PHP_VERSION_ID >= 70000
zend_object* php_exit_call_create(zend_class_entry *ce);
void php_exit_call_free_storage(zend_object* obj);
static inline php_exit_call_object* php_exit_call_fetch_object(zend_object* obj)
{
    return (php_exit_call_object *)((char*)(obj) - XtOffsetOf(php_exit_call_object, std));
}
#else
zend_object_value php_exit_call_create(zend_class_entry *ce TSRMLS_DC);
void php_exit_call_free_storage(php_exit_call_object* exitCall TSRMLS_DC);
#endif


PHP_METHOD(ADExitCall, __construct);
PHP_METHOD(ADExitCall, getCorrelationHeader);
PHP_METHOD(ADExitCall, setSQLQueryInfo);
PHP_FUNCTION(appdynamics_begin_exit_call);
PHP_FUNCTION(appdynamics_end_exit_call);

#endif // __agent_api_h
