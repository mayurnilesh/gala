/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __EXITPOINT_H
#define __EXITPOINT_H

#include <boost/algorithm/string/replace.hpp>
#include <boost/foreach.hpp>
#include <boost/functional/hash.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include "common.h"
#include "util.h"
#include "intercept.h"
#include "errors.h"
#include "snapshot_exitcall.h"
#include "backend_resolver.h"
#include "expressions.h"

class CurrentExitCall;
class ExitCallInfo;
class BackendResolver;
class TransactionContext;
class TransactionMonitor;
class SnapshotManager;
class TransactionReporter;

namespace appdynamics
{
    namespace pb
    {
        class BackendIdentifier;
        class BackendInfo;
        class ExitCallInfo;
    }
}

static const char CONFIG_RULE_PROP_NAME[] = "Configuration Name";

extern const size_t MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY;
extern const size_t MAX_URL_LEN_IN_IDENTIFYING_PROPERTY;

#define BOUND_PARAMETERS_TYPES_DECL(XX) \
    XX(BOUND_PARAMS_SUBSTITUTED,"substituted",=0) \
    XX(BOUND_PARAMS_POSITIONAL,"positional",=1) \
    XX(BOUND_PARAMS_NAMED,"named",=2) \

DECLARE_ENUM(BoundParametersType, BOUND_PARAMETERS_TYPES_DECL)

class AExitPointDelegate : boost::noncopyable
{
    public:
        AExitPointDelegate(InterceptionEngine* interceptEngine,
                           TransactionMonitor* txMonitor);
        virtual ~AExitPointDelegate() { }

        virtual void configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                               const struct _zend_agent_globals* agentGlobals) = 0;

        virtual appdynamics::pb::Agent::ExitPointType getExitPointType() const = 0;

        virtual ExitCallInfo makeIdentifyingProperties(const PHPExecEnvironment* execEnv,
                                                       const CurrentExitCall* exitCall,
                                                       const StringMap& properties) const;

        ExitCallInfo makeIdentifyingProperties(const PHPExecEnvironment* execEnv,
                                               const CurrentExitCall* exitCall,
                                               const IBackendPropertyGenerator& generator) const;

        virtual std::string generateDisplayName(const ExitCallInfo& exitCallInfo);

        inline bool isDetectionDisabled() const
        {
            return m_detectionDisabled;
        }

    protected:
        typedef google::protobuf::RepeatedPtrField<appdynamics::pb::Agent::BackendRule> t_BackendRulesList;
        typedef std::vector<std::pair<std::string, const char *>> t_DisplayNameOptionList;

        virtual void configure(const boost::shared_ptr<t_BackendRulesList>& backendRules,
                               const struct _zend_agent_globals* agentGlobals);

        virtual inline const char* getDefaultDisplayNameDelimiter() const
        {
            return " - ";
        }

        /**
            Pure virtual callback that exit point
            delegates should implement to register their
            interceptors. This is called always and individual
            interceptors should check the config to see if they should be applied.
         */
        virtual void applyRules(const struct _zend_agent_globals* agentGlobals) = 0;

        virtual bool processMatchRule(const PHPExecEnvironment* execEnv,
                                      const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                                      const StringMap& properties) const = 0;

        virtual void processNamingRule(const PHPExecEnvironment* execEnv,
                                       const appdynamics::pb::Agent::BackendRule& backendRule,
                                       const StringMap& properties,
                                       StringMap& identifyingProperties) const = 0;

        void computeDisplayName(const StringMap& properties,
                                const t_DisplayNameOptionList& displayNameSchema,
                                const appdynamics::pb::Agent::BackendRule& matchedRule,
                                std::string* displayName);

        void evaluateProperty(const char* propertyName,
                              const appdynamics::pb::Instrumentation::ExpressionNode& propertyNode,
                              const Expression::Context& context,
                              StringMap& identifyingProperties) const;

        TransactionMonitor* const m_txMonitor;
        InterceptionEngine* const m_interceptEngine;
        bool m_detectionDisabled;
        boost::shared_ptr<t_BackendRulesList> m_backendRules;
        t_DisplayNameOptionList m_displayNameSchema;
        AgentLogger m_logger;

    private:
        appdynamics::pb::Agent::BackendRule*
            getMatchingBackendRule(const PHPExecEnvironment* execEnv,
                                   const StringMap& properties) const;
};


/* {{{ class AExitCallInterceptor */
/**
    Abstract base class of all interceptors that intercept
    exit calls.  This class provides functionality to
    time an exit call and report that timing to the TransactionDataHolder.  This
    class also will report newly discovered exit points to
    the ExitCallRegistry.
    <p>
    Sub-classes of this class should not override the onCallableBegin, onCallableEnd,
    needParamsInOnMethodBegin, needReturnValueInOnMethodEnd, and shouldCallOnMethodEnd
    methods of this class.
 */
class AExitCallInterceptor : public AMethodInterceptor
{
public:

    virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

    virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                void* state);

    virtual bool needParamsInOnMethodBegin() const { return true; }
    virtual bool needReturnValueInOnMethodEnd() const { return true; }
    virtual bool shouldCallOnMethodEnd() const { return true; }

    virtual inline bool isActive(const PHPExecEnvironment* execEnv) const
    {
        return isExitCallToBeDetected();
    }

protected:
    virtual bool isExitCallToBeDetected() const;

    /**
        Pure-virtual method implemented by sub-classes to create the CurrentExitCall
        object.  The CurrentExitCall object is passed to methods of the
        BackendResolver.
        <p>
        CurrentExitCall's constructor ( which is invoked by this method ) records a
        timestamp that is used as the start time of the exit call.
        @param execEnv PHPExecEnvironment for the currently executing php program.
        @param sequenceNumber Exit call sequence number.  Sent to continuing tiers
        via the correlation header and to the controller in snapshot exit calls.
        @return A new CurrentExitCall instance.
     */
    virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                   uint64_t sequenceNumber) const = 0;

    /**
        Resolves backend identifying properties and display name and returns
        ExitCallInfo as the result.
        The backend is resolved using information parsed from the specified php function
        invocation information or potentially copied from an existing exit call.
        <p>
        This method is called from AExitCallInterceptor's onCallableBegin or
        onCallableEnd.
        @param exitCall The current exit call, which may already have ExitCallInfo.
        @param execEnv PHPExecEnvironment for the currently executing php program.
        @return ExitCallInfo object that identifies the backend if its
        isValid flag is true or an invalid backend if it's false.
     */
    virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                          const PHPExecEnvironment* execEnv) const = 0;

    /**
        @return true if makeExitCallInfo() should be called
        on onCallableBegin, false otherwise.
     */
    virtual bool resolveBackendOnCallableBegin() const = 0;

    /**
        @return true if makeExitCallInfo() should be called
        on onCallableEnd, false otherwise.  Default implementation returns false.
     */
    virtual bool resolveBackendOnCallableEnd() const;

    /**
        @return true if this exit call should have its metrics reported
        in onCallableEnd, false otherwise. Default implementation returns true.
     */
    virtual bool reportMetrics() const;

    /**
     * Fills in the detail and propeties of the SnapshotExitCall, for
     * display in the snapshot summary.
     */
    virtual void addExitCallDetailForSnapshot(const PHPExecEnvironment* phpExecEnv,
                                              TransactionContext* txContext,
                                              const CurrentExitCall* exitCall,
                                              const boost::shared_ptr<AErrorObject>& errorObject,
                                              const boost::shared_ptr<SnapshotManager>& snapshotManager,
                                              uint64_t timeTakenInMS) const = 0;

    virtual void setupCorrelation(TransactionContext* context,
                                  CurrentExitCall* exitCall,
                                  const PHPExecEnvironment* phpExecEnv);

    /**
     * Return the type of the exit point this interceptor handles.
     */
    appdynamics::pb::Agent::ExitPointType getExitPointType() const { return m_exitPointType; }

    /**
        Protected constructor invoked by sub-classes that burns in the
        interceptor ID and interceptor class name into const member variables.
     */
    AExitCallInterceptor(const char* className,
                         InterceptorRegistry::ID id,
                         appdynamics::pb::Agent::ExitPointType type);

    void reportTimeTaken(TransactionContext* txContext, CurrentExitCall* currentExitCall);

    void populateSnapshot(TransactionContext* context,
                          CurrentExitCall* exitCall,
                          const boost::shared_ptr<AErrorObject>& errorObject,
                          const PHPExecEnvironment* execEnv);

    /*
     * Cleans up the exit call data. By default clears the exit cal from the
     * transaction context and deletes the exit call.
     * @param state the state passed into onCallableEnd()
     */
    virtual void cleanupExitCall(void* state) const;


    /**
     * This method determines if there have been any errors or exceptions
     * during the exit call execution.
     * <p>
     * Default implementation considers any exit call that throws an exception
     * an error. Interceptors that need to perform more deliberate error
     * detection should override it.
     * <p>
     * @return An error object to be included in the SnapshotExitCall.
     */
    virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                         const PHPExecEnvironment* phpExecEnv);

    /**
     * Updates the exit call's error count and calls ErrorMonitor::reportException
     * if the interceptor detects there was an error during the exit call.
     * <p>
     * @return An error object to be included in the SnapshotExitCall.
     */
    static boost::shared_ptr<AErrorObject> detectExceptions(CurrentExitCall* currentExitCall,
                                                            const PHPExecEnvironment* phpExecEnv);

    /**
     * Updates the exit call's error count and calls ErrorMonitor::reportSyntheticException
     * if the exit call had any warnings/errors logged during its execution.
     * <p>
     * @return An error object to be included in the SnapshotExitCall.
     */
    static boost::shared_ptr<AErrorObject> detectLoggedErrorsDuringExitCall(CurrentExitCall* currentExitCall,
                                                                            const char* backendName,
                                                                            const PHPExecEnvironment* phpExecEnv);

    boost::shared_ptr<SnapshotExitCall> createSnapshotExitCall(TransactionContext* txContext,
                                                               const CurrentExitCall* exitCall,
                                                               const boost::shared_ptr<AErrorObject>& errorObject,
                                                               uint64_t timeTakenInMS) const;

    void reportSnapshotExitCall(const boost::shared_ptr<SnapshotExitCall>& sec,
                                const PHPExecEnvironment* execEnv) const;

    template <class t_ExitPointDelegate=AExitPointDelegate>
    boost::shared_ptr<t_ExitPointDelegate> getExitPointDelegate() const;

private:
    appdynamics::pb::Agent::ExitPointType m_exitPointType;
};

inline AExitCallInterceptor::AExitCallInterceptor(const char* className,
                                                  InterceptorRegistry::ID id,
                                                  appdynamics::pb::Agent::ExitPointType type)
    : AMethodInterceptor(className, id)
    , m_exitPointType(type)
{
}

/* }}} */

/* {{{ class ExitCallInfo */

/**
 * ExitCallInfo represents a backend identified via exit point type and
 * a number of properties, specific to the exit point type. The same
 * structure is used in communication between extension and proxy, for
 * backend registration. The m_isValid member is false if the class
 * represents unknown or undefined backend.
 *
 * This is a value class.
 */
class ExitCallInfo {
    public:
        ExitCallInfo()
            : m_isValid(false)
            , m_isApiCall(false)
            , m_matchedConfigRule(NULL)
        {
        }

        ExitCallInfo(appdynamics::pb::Agent::ExitPointType type, bool isApiCall = false)
            : m_type(type)
            , m_isValid(true)
            , m_isApiCall(isApiCall)
            , m_matchedConfigRule(NULL)
        {
        }

        ExitCallInfo(appdynamics::pb::Agent::ExitPointType type, const StringMap& properties, bool isApiCall = false)
            : m_type(type)
            , m_isValid(true)
            , m_isApiCall(isApiCall)
            , m_matchedConfigRule(NULL)
        {
            // Sanitize the generated properties because controller doesn't like
            // semicolons for some reason.
            BOOST_FOREACH(const auto& it, properties) {
                m_properties[it.first] = boost::replace_all_copy(it.second, ";", "_");
            }
        }

        ExitCallInfo(const appdynamics::pb::ExitCallInfo& protobuf);

        ~ExitCallInfo() {}

        appdynamics::pb::Agent::ExitPointType getExitPointType() const { return m_type; }
        inline const StringMap& getProperties() const { return m_properties; }

        void addIdentifyingProperty(const std::string& propertyName, const std::string& propertyValue);

        const std::string& getDisplayName() const { return m_displayName; }

        void setDisplayName(const std::string& name)
        {
            m_displayName = name;
            if (m_displayName.length() > 100)
                m_displayName.resize(100);
        }

        const appdynamics::pb::Agent::BackendRule* getMatchedConfigRule() const
        {
            return m_matchedConfigRule;
        }

        void setMatchedConfigRule(appdynamics::pb::Agent::BackendRule* backendRule)
        {
            m_matchedConfigRule = backendRule;
        }

        bool isValid() const { return m_isValid; }
        bool isApiCall() const { return m_isApiCall; }

        bool operator==(const ExitCallInfo& other) const;
        bool operator!=(const ExitCallInfo& other) const;

    private:
        appdynamics::pb::Agent::ExitPointType m_type;
        std::string m_displayName;
        StringMap m_properties;
        bool m_isValid;
        bool m_isApiCall;
        appdynamics::pb::Agent::BackendRule* m_matchedConfigRule;
};

inline void ExitCallInfo::addIdentifyingProperty(const std::string& propertyName, const std::string& propertyValue)
{
    if (propertyValue.empty())
        return;
    std::string& valueInMap = m_properties[propertyName] = propertyValue;
    replace(valueInMap.begin(), valueInMap.end(), ';', '_');
}

inline bool ExitCallInfo::operator==(const ExitCallInfo& other) const
{
    if (m_type != other.m_type)
        return false;

    if (m_properties.size() > 0) {
        if (m_properties.size() != other.m_properties.size()) {
            return false;
        }

        BOOST_FOREACH(auto& e, m_properties) {
            auto temp = other.m_properties.find(e.first);
            if (temp == other.m_properties.end())
                return false;
            if (e.second != temp->second)
                return false;
        }
    } else if (other.m_properties.size() > 0)
        return false;

    return true;
}

inline bool ExitCallInfo::operator!=(const ExitCallInfo& other) const
{
    return !(this->operator==(other));
}

/* Hash functor for ExitCallInfo */
namespace boost {
    template<> struct hash<ExitCallInfo> {
        size_t operator()(const ExitCallInfo& c) const {
            size_t seed = 0;
            boost::hash_combine(seed, static_cast<unsigned>(c.getExitPointType()));
            BOOST_FOREACH(auto& entry, c.getProperties()) {
                boost::hash_combine(seed, entry.first);
                boost::hash_combine(seed, entry.second);
            }
            return seed;
        }
    };
}

std::ostream& operator<<(std::ostream& out, const ExitCallInfo& e);

/* }}} */

/* {{{ class ExitCallRegistry */

/**
 * A registry containing newly discovered backends and backends that are
 * already registered with the proxy/controller. Has a method to report
 * unregistered backends to proxy.
 *
 * This should only be a field in TransactionRegistry.
 */
class ExitCallRegistry : public IMonitorStateListener, boost::noncopyable {
    private:
        // default max backend count is 300
        // max named backends listed in each overflow backend is 50.
        // There are two overflow backends ( one for databases, one for everything else ).
        // Double the max backends to deal with the fact that the registered backends
        // we last received from the proxy might be stale.
        static const size_t defaultMaxBackendsCount = (300 + (50 * 2)) * 2;

    public:
        ExitCallRegistry()
            : m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".ExitCallRegistry"))
            , m_maxBackendsCount(defaultMaxBackendsCount)
        {
        }

        void updateBackendInfo(const appdynamics::pb::BackendInfo& backendInformation);

        /**
            @return Total number of backends encountered.
         */
        inline size_t getTotalNumberOfBackends() const
        {
            return m_backends.size();
        }

        /**
            Gets the backend identifier for the specified exit call info.
            @param info ExitCallInfo that identifies a backend.
            @return The protobuf that should be used to report
            activity on the backend that corresponds to the specified
            ExitCallInfo.
         */
        BackendIdentifierPtr getBackendIdentifier(const ExitCallInfo& info);

        /**
         * Sets this exit call's backend ID based on its ExitCallInfo. If the
         * backend is not registered, reports it as a newly discovered backend.
         * @param currentExitCall Exit call to resolve and register.
         */
        void resolveExitCall(CurrentExitCall* currentExitCall);

        /**
         * Fill in the component ID of the exit call for both resolved and
         * unresolved component cases.
         * @param exitCall the exit call to resolve
         */
        void resolveComponentID(CurrentExitCall* exitCall);

        /**
         * Return the component ID corresponding to the given backend ID.
         * If the component is not found, return 0.
         * @param backendID The foreign backend ID to look up
         * @return The component ID for the foreign backend ID
         */
        int64_t getComponentForForeignBackendID(int64_t backendID) const;

        /**
         * If a backend is currently resolved to a tier other than a tier
         * for the current php application, this method should be called
         * to send a message to the proxy saying that the specified backend
         * should be re-resolved to the tier for the current php application.
         * @param transactionReporter TransactionReporter to use to report
         * self re-resolution messages to the proxy.
         * @param backendID The backend ID to re-resolve to the tier of the
         * current PHP application.
         */
        void reResolveBackendToSelf(TransactionReporter* transactionReporter,
                                    int64_t backendID);

        void onMonitorDisable();
        void onMonitorEnable();

        /**
            Drops information about registered backends/exit calls.  This
            method is called from TransactionRegistry::reset which is itself
            called when the proxy has indicated that all the IDs for business
            transactions and backends are now invalid.
         */
        void reset();

    private:
        AgentLogger m_logger;
        boost::unordered_map<ExitCallInfo, BackendIdentifierPtr> m_backends;
        boost::unordered_map<int64_t, std::string> m_unresolvedBackendCache;
        boost::unordered_map<int64_t, int64_t> m_resolvedBackends;
        boost::unordered_set<int64_t> m_selfReResolvedBackends;
        size_t m_maxBackendsCount;

        // private to prevent creation on heap
        void* operator new(size_t);

        /**
         * @param backendID the registered backend identifier
         * @return the unresolved component ID string based on the backend identifier
         */
        std::string generateUnresolvedCallInfo(int64_t backendID);

};

/* }}} */

/* {{{ struct ExitCallMetricsKey */

/**
 * A value class that serves as the look-up for the exit call metrics.
 *
 * For majority of backends, the category will be empty. Those backends that
 * wish to split the metrics into separate categories, need to fill it in.
 *
 * The default copy constructor and assignment operator will work fine, so no
 * explicit ones are defined.
 *
 */
struct ExitCallMetricsKey {
    public:
        ExitCallMetricsKey(const BackendIdentifierPtr& backendIdentifier)
            : m_backendID(backendIdentifier) { }
        ExitCallMetricsKey(const BackendIdentifierPtr& backendIdentifier, const std::string& category)
            : m_backendID(backendIdentifier)
            , m_category(category) { }

        inline const BackendIdentifierPtr& getBackendID() const { return m_backendID; }
        inline const std::string& getCategory() const { return m_category; }

    private:
        BackendIdentifierPtr const m_backendID;
        std::string const m_category;
};

inline bool operator==(const ExitCallMetricsKey& lhs, const ExitCallMetricsKey& rhs)
{
    if (lhs.getBackendID() != rhs.getBackendID()) {
        return false;
    }
    if (lhs.getCategory() != rhs.getCategory()) {
        return false;
    }
    return true;
}

namespace boost {
    /* Hash functor for ExitCallMetricsKey */
    template<> struct hash<ExitCallMetricsKey> {
        size_t operator()(const ExitCallMetricsKey& k) const {
            size_t seed = 0;
            boost::hash_combine(seed, k.getBackendID());
            boost::hash_combine(seed, k.getCategory());
            return seed;
        }
    };
}

/* }}} */

#endif /* __EXITPOINT_H */

// vim: set fdm=marker:
