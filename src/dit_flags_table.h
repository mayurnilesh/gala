/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#ifndef __dit_flags_table_h
#define __dit_flags_table_h

/**
    Defines a table of flags the "do interesting things".

    ALWAYS add to the end of this table!
 */
#define DIT_FLAGS_TABLE(XX)             \
    XX(disableCallGraph)                \
    XX(disableInterceptorExecution)     \
    XX(disableExecuteCallback)          \
    XX(disableInterceptorInstallation)  \
    XX(disableConfigUpdates)            \
    XX(disableEUM)

#endif
