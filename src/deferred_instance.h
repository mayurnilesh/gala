/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __deferred_instance_h
#define __deferred_instance_h

#include <boost/assert.hpp>
#include <boost/utility.hpp>
#include "always_inline.h"

/**
    Template that allows the storage for an object to be embedded in
    an other object, but allows the call to the embedded object's constructor
    to be deferred to a time after the containing object's constructor
    has finished.

    This template is a substitute for the pattern of having a class with
    a default constructor and an init method that actually initializes the class.
 */
template <class T>
class DeferredInstance : boost::noncopyable
{
public:
    DeferredInstance();
    ~DeferredInstance();

    template <class... Args>
    void construct(Args &&...);

    const T* get() const;
    T* get();

private:
    union {
        void* alignment;
        uint8_t m_bytes[sizeof(T)];
    } m_storage;
    bool m_didConstruct;
};

template <class T>
inline DeferredInstance<T>::DeferredInstance()
    : m_didConstruct(false)
{
}

template <class T>
inline DeferredInstance<T>::~DeferredInstance()
{
    if (m_didConstruct)
        get()->~T();
}

template <class T>
template <class... Args>
inline void ALWAYS_INLINE_IN_RELEASE DeferredInstance<T>::construct(Args &&... args)
{
    BOOST_ASSERT(!m_didConstruct);
    new (&(m_storage.m_bytes[0])) T(args...);
    m_didConstruct = true;
}

template <class T>
inline const T* ALWAYS_INLINE_IN_RELEASE DeferredInstance<T>::get() const
{
    BOOST_ASSERT(m_didConstruct);
    return reinterpret_cast<const T*>(&(m_storage.m_bytes[0]));
}

template <class T>
inline T* ALWAYS_INLINE_IN_RELEASE DeferredInstance<T>::get()
{
    BOOST_ASSERT(m_didConstruct);
    return reinterpret_cast<T*>(&(m_storage.m_bytes[0]));
}

#endif
