/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef _backend_resolver_h
#define _backend_resolver_h

#include "util.h"
#include <vector>
#include <boost/utility.hpp>
#include <zend.h>

class CurrentExitCall;
class ExitCallInfo;
class PHPExecEnvironment;

template <class T>
class EMallocAllocator;

/**
    Abstract base class for all backend property generators. A backend
    property generator is able to parse the arguments to a php function into
    information that is used to identify a backend.
    <p>
    Generally sub-classes of this class should not need to be defined in
    header files. The class declaration and definition should be able to be
    contained entirely in the .cpp files for the AExitCallInterceptor sub-
    classes methods.
 */
class IBackendPropertyGenerator : boost::noncopyable
{
public:
    /**
        Sub-classes should overload this method to parse information from the php
        function invocation into the properties map.
        <p>
        @param execEnv PHPExecEnvironment for the currently executing php program.  This
        can be used to access the return value of the function that is currently returning
        the "this" value of the method that is being invoked, the parameters of the
        method that is being invoked, the class name of the method that is being invoked,
        and the name of the method being invoked.
        @return true if properties could be determined, false otherwise.
     */
    virtual bool getProperties(const PHPExecEnvironment* execEnv,
                               const CurrentExitCall* exitCall,
                               StringMap& properties) const = 0;
};

#endif
