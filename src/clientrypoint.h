/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#ifndef __clientrypoint_h
#define __clientrypoint_h


#include "entrypoint.h"

namespace CLI
{
    class EntryPointInterceptor : public AEntryPointInterceptor
    {
        public:
            EntryPointInterceptor();

            virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
            appdynamics::pb::Agent::EntryPointType getEntryPointType() const;
        protected:
            void detectErrors(const PHPExecEnvironment* phpExecEnv, void* state);
    };

    class EntryPointDelegate : public AEntryPointDelegate
    {
        public:
            EntryPointDelegate(InterceptionEngine* interceptEngine,
                               TransactionMonitor* txMonitor);
            virtual ~EntryPointDelegate();

        protected:
            virtual void configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                   const struct _zend_agent_globals* agentGlobals);
            virtual void initAcceptor();
            virtual void applyRules(const struct _zend_agent_globals* agentGlobals);
        private:
            static bool s_didApplyRules;
    };
}

#endif // __clientrypoint_h
