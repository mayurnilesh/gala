/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/


#ifndef __AGENT_LOGGER_H_
#define __AGENT_LOGGER_H_

#include <string>
#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include "SAPI.h"

# if PHP_VERSION_ID >= 70100
#  define SAPI_LOG_LEVEL , -1
# else
#  define SAPI_LOG_LEVEL
# endif

typedef log4cxx::LoggerPtr AgentLogger;

extern const int NullLoggerPtr;

void preForkInitLogging();
void initLogging(const std::string& configFilePath);
bool getLoggingDirectory(boost::filesystem::path* path);
AgentLogger getLogger(const std::string& name);

/* this is cloned from log4cxx locationinfo.h */
#if defined(_MSC_VER)
    #if _MSC_VER >= 1300
        #define __LOG4CXX_FUNC__ __FUNCSIG__
    #endif
#else
    #if defined(__GNUC__)
        #define __LOG4CXX_FUNC__ __PRETTY_FUNCTION__
    #endif
#endif
#if !defined(__LOG4CXX_FUNC__)
    #define __LOG4CXX_FUNC__ ""
#endif
/* */

struct LogContext
{
    static const char* AGENT;
    static const char* TX_SERVICE;
    static const char* CONFIG;
    static const char* ERROR;
    static const char* REPORT;
    static const char* SNAPSHOT;
    static const char* DATAGATHERER;
    static const char* INSTRUMENT;
    static const char* CORRELATION;
    static const char* EXIT_HTTP;
    static const char* EXIT_CACHING;
    static const char* EXIT_DB;
    static const char* INFOPOINT;
};

/**
 * Helper class that is used to increase the logging level to DEBUG and then
 * reset it back to normal..It is controlled from the RequestContext.
 */
class RequestDebugLoggingManager
{
    public:
        inline static void enable()
        {
            log4cxx::spi::LoggerRepositoryPtr repository(log4cxx::LogManager::getLoggerRepository());
            m_previousLogLevel = repository->getThreshold();
            if (!m_previousLogLevel->equals(log4cxx::Level::getDebug()))
                repository->setThreshold(log4cxx::Level::getDebug());
        }

        inline static void reset()
        {
            if (m_previousLogLevel)
                log4cxx::LogManager::getLoggerRepository()->setThreshold(m_previousLogLevel);
        }

    private:
        RequestDebugLoggingManager() {}
        static log4cxx::LevelPtr m_previousLogLevel;
};

template<class charT, class Traits>
inline void logStartupError(const boost::basic_format<charT,Traits>& f)
{
    if (sapi_module.log_message) {
        sapi_module.log_message(const_cast<char*>(f.str().c_str()) SAPI_LOG_LEVEL);
    } else {
        std::cerr << f << std::endl;
    }
}

inline void logStartupError(const char* message)
{
    if (sapi_module.log_message) {
        sapi_module.log_message(const_cast<char*>(message) SAPI_LOG_LEVEL);
    } else {
        std::cerr << message << std::endl;
    }
}

#endif /* __AGENT_LOGGER_H_ */
