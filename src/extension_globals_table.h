/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __delayed_extension_globals_table_h
#define __delayed_extension_globals_table_h

// We need this for our preprocessor magic to work, because
// the PHP session globals struct doesn't follow the
// _zend_<extname>_globals naming scheme.
#define _zend_session_globals _php_ps_globals

#define DELAYED_EXTENSION_GLOBALS_TABLE(XX)             \
    XX(mysqli)                                          \
    XX(session)

#endif
