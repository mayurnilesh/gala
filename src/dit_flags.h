/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#ifndef __dit_flags_h
#define __dit_flags_h

#include <string>
#include "dit_flags_table.h"
#include "agent_logger.h"

#define DEFINE_DIT_FLAG(f)                \
    bool f : 1;

#define DEFINE_DIT_FLAG_INITIALIZER(f)    \
    f(false),

#define DEFINE_DIT_FLAG_OR_IS_SET(f)      \
    || f

/**
    A struct containing a flag for each "framework" supported by the agent.
    If the flag is set then the interceptors needed to instrument that "framework"
    are registered with the interception engine.  The table of supported "framework"'s
    is in frameworks_table.h.
 */
struct DITFlags
{
    DITFlags() : DIT_FLAGS_TABLE(DEFINE_DIT_FLAG_INITIALIZER) m_unused(false) {}

    bool anySet() const { return false DIT_FLAGS_TABLE(DEFINE_DIT_FLAG_OR_IS_SET); }

    DIT_FLAGS_TABLE(DEFINE_DIT_FLAG)
    bool m_unused : 1;
};

#undef DEFINE_DIT_FLAG
#undef DEFINE_DIT_FLAG_INITIALIZER


#define DEFINE_DIT_ENUM_ENTRY(f)  \
    DITFlag_##f,

/**
    An enum that has an entry for each supported framework and an extra entry
    that can be used to determine the number of supported frameworks.
 */
enum DITFlag
{
    DIT_FLAGS_TABLE(DEFINE_DIT_ENUM_ENTRY)
    DITFlag_LAST
};

#undef DEFINE_DIT_ENUM_ENTRY

/**
    Parses binary number into a DITFlags structure.
 */
extern void parseDITFlags(const std::string& ditString,
                          DITFlags* ditFlags);

/**
    Generates a binary number for the specified DITFlags structure.
 */
extern std::string ditFlagsToString(const DITFlags& ditFlags);

extern void logDITFlags(const AgentLogger& logger, const DITFlags& ditFlags);

#endif
