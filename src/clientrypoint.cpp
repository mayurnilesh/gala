/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#include "clientrypoint.h"
#include "agent.h"
#include <boost/filesystem.hpp>

namespace CLI
{
    namespace
    {

        EntryPointInterceptor entryPointInterceptor;

        static const char SERVER_VAR_NAME[] = "_SERVER";
        static const char SCRIPT_VAR_NAME[] = "SCRIPT_NAME";

        static const size_t nPathSegments = 3;

        class Payload : public IMatchPointPayload
        {
            public:
                Payload(const PHPExecEnvironment* execEnv)
                    : m_execEnv(execEnv)
                {
                    ZValPointerAny serverArray;
                    m_execEnv->getAutoGlobalValue(SERVER_VAR_NAME, strlen(SERVER_VAR_NAME), &serverArray);
                    if (serverArray && serverArray.getType() == ZValPointer::ZVal_Array) {
                        ZValPointerString scriptVarNameVal(serverArray.cast<ZValPointerArray>().findEntryByName<ZValPointerString>(SCRIPT_VAR_NAME, strlen(SCRIPT_VAR_NAME)));
                        if (scriptVarNameVal) {
                            if (scriptVarNameVal.getStringValue() == "-") {
                                m_scriptName = m_scriptNameCanonical = "[no script]";
                            } else {
                                boost::system::error_code error;
                                boost::filesystem::path scriptFile(boost::filesystem::canonical(scriptVarNameVal.getStringValue(), error));
                                if (error.value() == boost::system::errc::success) {
                                    // Truncate the path to contain only the last N segments
                                    unsigned currentSegment = 0;
                                    auto current = scriptFile.end();

                                    while (currentSegment < nPathSegments && current != scriptFile.begin()) {
                                        --current;
                                        ++currentSegment;
                                    }
                                    boost::filesystem::path partial;
                                    for ( ; current != scriptFile.end(); ++current)
                                        partial /= *current;
                                    m_scriptName = partial.string();
                                    m_scriptNameCanonical = scriptFile.string();
                                } else {
                                    m_scriptName = m_scriptNameCanonical = scriptVarNameVal.getStringValue();
                                }
                            }
                        }
                    }
                }

                const std::string& getScriptName() { return m_scriptName; }

                const std::string& getScriptNameCanonical() { return m_scriptNameCanonical; }

                const PHPExecEnvironment* getExecEnvironment() const { return m_execEnv; }

            private:
                const PHPExecEnvironment* const m_execEnv;
                std::string m_scriptName;
                std::string m_scriptNameCanonical;
        };

        class TransactionAcceptor : public ATransactionAcceptor
        {
            public:
                TransactionAcceptor(const boost::shared_ptr<MatchPointConfig>& config,
                                    TransactionMonitor* monitor)
                    : ATransactionAcceptor(config, monitor)
                {
                }

                virtual bool acceptTransaction(const PHPExecEnvironment* phpExecEnv,
                                               const boost::shared_ptr<IMatchPointPayload>& payload)
                {
                    if (!ATransactionAcceptor::acceptTransaction(phpExecEnv, payload))
                        return false;
                    boost::shared_ptr<Payload> cliPaylod(boost::static_pointer_cast<Payload>(payload));
                    BOOST_ASSERT(cliPaylod != NULL);

                    getTransactionMonitor()->getSnapshotManager()->setURL(cliPaylod->getScriptNameCanonical());
                    return true;
                }

                virtual bool matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& iPayload,
                                                  const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition)
                {
                    const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
                    appdynamics::pb::Agent::EntryPointMatchCondition::CLIMatchRule matchRule = matchCondition.cli();
                    const boost::shared_ptr<Payload> payload = boost::static_pointer_cast<Payload>(iPayload);

                    bool matches = true;
                    if (matchRule.has_scriptname())
                        matches = matches && configPtr->matchAndLogError(payload->getExecEnvironment(),
                                                                         &(matchRule.scriptname()),
                                                                         payload->getScriptNameCanonical(),
                                                                         getLogger());
                    return matches;
                }

                virtual bool customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                                    const boost::shared_ptr<IMatchPointPayload>& iPayload)
                {
                    *matchedTransaction = boost::shared_ptr<AgentTransaction>();
                    const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
                    const appdynamics::pb::Agent::MatchPointConfig* config = configPtr->get();

                    const auto& customDefinitions = config->customdefinitions();

                    int maxPriority = std::numeric_limits<int>::min();
                    const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* prioritizedMatchRule = NULL;

                    for (auto it = customDefinitions.begin(); it != customDefinitions.end(); ++it) {

                        if (!it->condition().has_cli())
                            continue;

                        LOG4CXX_TRACE(getLogger(), "Attempting to match custom match rule: " << it->btname());
                        bool matches = matchTransactionRule(iPayload, it->condition());

                        if (!matches)
                            continue;

                        LOG4CXX_DEBUG(getLogger(), "Potential match rule found: " << it->btname() << " Priority: " << it->priority() << " Current maximum matching priority: " << maxPriority);

                        if (it->priority() > maxPriority) {
                            prioritizedMatchRule = &(*it);
                            maxPriority = it->priority();
                        }
                    }

                    if (!prioritizedMatchRule)
                        return false;
                    *matchedTransaction = getTransaction(prioritizedMatchRule->btname(), prioritizedMatchRule);
                    return true;
                }

                virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() { return appdynamics::pb::Agent::PHP_CLI; }

                virtual std::string getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& iPayload)
                {
                    boost::shared_ptr<Payload> payload(boost::static_pointer_cast<Payload>(iPayload));
                    return payload->getScriptName();
                }
        };

    }

    EntryPointInterceptor::EntryPointInterceptor()
        : AEntryPointInterceptor("CLI::EntryPointInterceptor",
                                 InterceptorRegistry::CLIEntryPointInterceptor_ID)
    {
    }

    appdynamics::pb::Agent::EntryPointType EntryPointInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_CLI;
    }

    boost::shared_ptr<IMatchPointPayload> EntryPointInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        return boost::make_shared<Payload>(execEnv);
    }

    void EntryPointInterceptor::detectErrors(const PHPExecEnvironment* phpExecEnv, void* state)
    {
    }

    bool EntryPointDelegate::s_didApplyRules = false;

    EntryPointDelegate::EntryPointDelegate(InterceptionEngine* interceptEngine,
                                           TransactionMonitor* txMonitor)
        : AEntryPointDelegate(interceptEngine, txMonitor)
    {
    }

    EntryPointDelegate::~EntryPointDelegate()
    {
    }

    void EntryPointDelegate::configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                       const struct _zend_agent_globals* agentGlobals)
    {
        const appdynamics::pb::Agent::MatchPointConfig* configProto = &(txConfig->cli());
        boost::shared_ptr<MatchPointConfig> config(boost::make_shared<MatchPointConfig>(txConfig,
                                                                                        configProto));
        AEntryPointDelegate::configure(config, agentGlobals);
    }

    void EntryPointDelegate::initAcceptor()
    {
        m_txAcceptor = boost::make_shared<TransactionAcceptor>(m_matchPointConfig, m_txMonitor);
    }

    void EntryPointDelegate::applyRules(const struct _zend_agent_globals* agentGlobals)
    {
        if (s_didApplyRules)
            return;
        s_didApplyRules = true;
        m_interceptEngine->addInterceptorForCallable(CallableInfo(ROOT_SYMBOL), &entryPointInterceptor);
    }

}
