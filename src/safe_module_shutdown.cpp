/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */
#include "safe_module_shutdown.h"
#include <boost/assert.hpp>

bool SafeModuleShutdown::s_inModuleShutdown = false;


SafeModuleShutdown::SafeModuleShutdown()
{
    BOOST_ASSERT(!s_inModuleShutdown);
    s_inModuleShutdown = true;
}

SafeModuleShutdown::~SafeModuleShutdown()
{
    BOOST_ASSERT(s_inModuleShutdown);
    s_inModuleShutdown = false;
}
