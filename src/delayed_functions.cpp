/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/

#include "delayed_functions.h"

namespace DelayedFunctions
{
const char FunctionTable::nullName[] = "";

#define DELAYED_FUNCTION_NAME_STRING_DEFINITION(function, returnType, args)     \
const char FunctionTable::delayed___##function##___name[] = #function;

#define DELAYED_FUNCTION_NAME_STRING_DEFINITION_EX(function,                    \
                                                   functionName1,               \
                                                   functionName2,               \
                                                   returnType,                  \
                                                   args)                        \
const char FunctionTable::delayed___##function##___name_1[] = functionName1;    \
const char FunctionTable::delayed___##function##___name_2[] = functionName2;

DELAYED_FUNCTIONS_TABLE(DELAYED_FUNCTION_NAME_STRING_DEFINITION,                \
                        DELAYED_FUNCTION_NAME_STRING_DEFINITION_EX)
#undef DELAYED_FUNCTION_NAME_STRING_DEFINITION
}
