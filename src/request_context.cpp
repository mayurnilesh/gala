/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#include "request_context.h"
#include "always_inline.h"
#include "call_metrics.h"
#include "correlation.h"
#include "infopoints.h"
#include "services.h"
#include "timer.h"
#include "transactions.h"
#include "Instrumentation.pb.h"
#include <boost/lexical_cast.hpp>

void TimePoint::reset(const Timer* timer, uint64_t timestampSkew) {
    m_timestamp = timer->getTimestamp();
    m_wallTime = timer->getCurrentTime();
    m_skewAdjustedWallTime = m_wallTime + timestampSkew;
}

boost::shared_ptr<RequestContext> RequestContext::create(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                                                         ErrorMonitor* errorMonitor,
                                                         const boost::shared_ptr<const EUM::Config>& eumConfig)
{
    return boost::make_shared<RequestContext>(execEnv,
                                              errorMonitor,
                                              eumConfig);
}

RequestContext::RequestContext(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                               ErrorMonitor* errorMonitor,
                               const boost::shared_ptr<const EUM::Config>& eumConfig)
    : m_timer(&(execEnv->getAgentGlobals().timer))
    , m_configChannel(execEnv->getConfigChannel())
    , m_startTime(m_timer, m_configChannel->getTimestampSkew())
    , m_requestGUIDComputed(false)
    , m_errorRegistry(errorMonitor)
    , m_markedAsError(false)
    , m_frameworkDetected(false)
    , m_correlationSent(false)
    , m_btDetectionDisabled(false)
    , m_proxyMessageID(0)
    , m_eumContext(execEnv, eumConfig)
{
}

void RequestContext::clear()
{
    m_startTime.reset(m_timer, m_configChannel->getTimestampSkew());
    m_requestGUIDComputed = false;
    m_markedAsError = false;
    m_frameworkDetected = false;
    m_correlationSent = false;
    m_btDetectionDisabled = false;
    m_proxyMessageID = 0;
    m_btInfoResponse.reset();

    m_requestGUIDComputed = false;

    m_registeredExitCallMetrics.clear();
    m_infoPointMetrics.clear();

    m_errorRegistry.clear();
}

RequestContext::~RequestContext()
{
    RequestDebugLoggingManager::reset();
}

void RequestContext::reportError(const boost::shared_ptr<AErrorObject>& errorObject,
                                 bool markTransactionAsError)
{
    m_markedAsError = m_markedAsError || markTransactionAsError;
    m_errorRegistry.report(errorObject);
}

void RequestContext::reportExitCallData(const BackendIdentifierPtr& backendID,
                                        const std::string& category,
                                        long timeTaken,
                                        long callCount,
                                        unsigned errorCount)
{
    t_CallMetricsPtr metrics(getRegisteredExitCallMetrics(ExitCallMetricsKey(backendID,
                                                                             category)));
    reportExitCallData(metrics, timeTaken, callCount, errorCount);
}

boost::shared_ptr<CallMetrics>
ALWAYS_INLINE_IN_RELEASE RequestContext::getRegisteredExitCallMetrics(const ExitCallMetricsKey& metricsKey)
{
    boost::shared_ptr<CallMetrics> metrics;
    auto it = m_registeredExitCallMetrics.find(metricsKey);

    if (it == m_registeredExitCallMetrics.end()) {
        metrics = boost::make_shared<CallMetrics>();
        m_registeredExitCallMetrics[metricsKey] = metrics;
    } else {
        metrics = it->second;
    }
    return metrics;
}

boost::shared_ptr<InfoPoint::Metrics>
ALWAYS_INLINE_IN_RELEASE RequestContext::getInfoPointMetrics(uint32_t infoPointID)
{
    boost::shared_ptr<InfoPoint::Metrics> metrics;
    auto it = m_infoPointMetrics.find(infoPointID);

    if (it == m_infoPointMetrics.end()) {
        metrics = boost::make_shared<InfoPoint::Metrics>();
        m_infoPointMetrics[infoPointID] = metrics;
    } else {
        metrics = it->second;
    }
    return metrics;
}

void RequestContext::reportInfoPointCall(uint32_t infoPointID,
                                         int64_t timeTakenUS,
                                         unsigned errorCount)
{
    t_InfoPointMetricsPtr metrics(getInfoPointMetrics(infoPointID));

    metrics->getCallMetrics()->reportCalls(1);
    metrics->getCallMetrics()->reportTime(timeTakenUS);
    if (errorCount != 0) {
        metrics->getCallMetrics()->reportErrors(errorCount);
    }
}

void RequestContext::reportInfoPointCustomMetric(uint32_t infoPointID,
                                                 const std::string& metricName,
                                                 appdynamics::pb::Instrumentation::CustomMetricDefinition::Rollup rollup,
                                                 double metricValue)
{
    t_InfoPointMetricsPtr metrics(getInfoPointMetrics(infoPointID));
    boost::shared_ptr<appdynamics::pb::InformationPointMetrics::Custom> custom(metrics->getCustomMetrics(metricName, rollup));
    custom->set_value(custom->value() + metricValue);
    custom->set_numofcalls(custom->numofcalls() + 1);
    if (metricValue < custom->minvalue())
        custom->set_minvalue(metricValue);
    if (metricValue > custom->maxvalue())
        custom->set_maxvalue(metricValue);
}

const appdynamics::pb::SnapshotInfo::Trigger*
    RequestContext::evaluateSnapshotPolicy(uint64_t const requestElapsedTimeMS,
                                           bool includeContinuing,
                                           const TransactionContext* transactionContext)
{
    if (includeContinuing &&
        transactionContext->isContinuingTransaction() &&
        transactionContext->getCorrelationHeader()->isSnapshotEnabled()) {

        if (!m_btInfoResponse || m_btInfoResponse->sendsnapshotifcontinuing()) {
            return &appdynamics::pb::SnapshotInfo::CONTINUING;
        }
    }

    if (!m_btInfoResponse)
        return NULL;

    appdynamics::pb::SnapshotInfo::Trigger snapshotTrigger;

    if (m_btInfoResponse->issnapshotrequired())
        return &appdynamics::pb::SnapshotInfo::REQUIRED;

    if (m_btInfoResponse->sendsnapshotiferror() && isErrorRequest())
        return &appdynamics::pb::SnapshotInfo::ERROR;

    if (m_btInfoResponse->has_currentslowthreshold() &&
        m_btInfoResponse->currentslowthreshold() < requestElapsedTimeMS)
        return &appdynamics::pb::SnapshotInfo::SLOW;

    return NULL;
}

bool RequestContext::getAverageResponseTimeForLastMinute(int64_t* dest)
{
    BOOST_ASSERT(dest);
    if (!m_btInfoResponse)
        return false;
    if (!m_btInfoResponse->has_averageresponsetimeforlastminute())
        return false;
    *dest = m_btInfoResponse->averageresponsetimeforlastminute();
    return true;
}

void RequestContext::reportExitCallData(const t_CallMetricsPtr& metrics,
                                        long timeTaken,
                                        long callCount,
                                        unsigned errorCount)
{
    metrics->reportCalls(callCount);
    metrics->reportTime(timeTaken);
    if (errorCount != 0) {
        metrics->reportErrors(errorCount);
    }
}


std::string RequestContext::computeRequestGUID() const
{
    return AG(kernel)->getAgentContext()->getTransactionMonitor()->getGUIDMaker().getAndIncrement();
}
