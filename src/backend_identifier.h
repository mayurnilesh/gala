/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __backend_identifier_h
#define __backend_identifier_h

#include "PHPAgentProtobufs.pb.h"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

class ExitCallInfo;
class AExitPointDelegate;

class ABackendIdentifier
{
public:
    virtual ~ABackendIdentifier() { }
    virtual void fillInProtobufIdentifier(appdynamics::pb::BackendIdentifier* identifier) const = 0;
    virtual void fillInDisplayName(std::string* displayName) const = 0;
    virtual appdynamics::pb::BackendIdentifier::Type type() const = 0;
};

class RegisteredBackendIdentifier : public ABackendIdentifier
{
public:
    RegisteredBackendIdentifier(const appdynamics::pb::RegisteredBackendInfo& backendInfo);

    void fillInProtobufIdentifier(appdynamics::pb::BackendIdentifier* identifier) const;
    void fillInDisplayName(std::string* displayName) const;
    inline appdynamics::pb::BackendIdentifier::Type type() const
    {
        return appdynamics::pb::BackendIdentifier::REGISTERED;
    }
    const appdynamics::pb::RegisteredBackend& getRegisteredBackend() const
    {
        return m_backend;
    }

private:
    appdynamics::pb::RegisteredBackend m_backend;
    std::string m_displayName;
};

class UnRegisteredBackendIdentifier : public ABackendIdentifier
{
public:
    UnRegisteredBackendIdentifier(const ExitCallInfo& exitCallInfo);

    void fillInProtobufIdentifier(appdynamics::pb::BackendIdentifier* identifier) const;
    void fillInDisplayName(std::string* displayName) const;
    inline appdynamics::pb::BackendIdentifier::Type type() const
    {
        return appdynamics::pb::BackendIdentifier::UNREGISTERED;
    }

private:
    boost::shared_ptr<ExitCallInfo> m_exitCallInfo;
    mutable bool m_didGenerateDisplayName;
    mutable std::string m_displayName;
    AExitPointDelegate* m_delegate;
};

typedef boost::shared_ptr<ABackendIdentifier> BackendIdentifierPtr;

#endif
