#ifndef __timer_h
#define __timer_h

/*
 * Each of the header files selectively included here define a Timer class with
 * the following public methods:
 *  - init()
 *  - getTimestamp()
 *  - getElapsedTime(startTime)
 *  - getCurrentTime()
 *  - onRequestBegin()
 *
 *  Each header has a different underlying implementation of the timing
 *  mechanism.
 *  - timer_tsc uses rdtsc CPU instruction
 *  - timer_gtod uses gettimeofday() syscall
 *  - timer_cgt uses clock_gettime(CLOCK_MONOTONIC) syscall
 */

#if defined(TIMER_USE_TSC)
#include "timer_tsc.h"
#elif defined(TIMER_USE_GTOD)
#include "timer_gtod.h"
#elif defined(TIMER_USE_CGT)
#include "timer_cgt.h"
#elif defined(TIMER_USE_CGT_THREAD)
#include "timer_cgt_thread.h"
#else
#error "TIMER_USE_* setting not specified"
#endif

#endif /* __timer_h */
