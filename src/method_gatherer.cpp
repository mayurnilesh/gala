/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#include <boost/make_shared.hpp>
#include <boost/foreach.hpp>

#include "agent.h"
#include "method_gatherer.h"
#include "expressions.h"
#include "transactions.h"
#include "utf8_sanitize.h"

namespace MethodData
{
    AgentLogger Interceptor::m_logger = NullLoggerPtr;

    ConfigManager::ConfigManager(InterceptionEngine* interceptionEngine,
                                 const boost::shared_ptr<IConfigChannel>& configChannel)
        : m_interceptionEngine(interceptionEngine)
        , m_logger(getLogger(std::string(LogContext::INFOPOINT) + ".MethodData::ConfigManager"))
    {
        configChannel->addListener(this);
    }

    ConfigManager::~ConfigManager()
    {
    }

    void ConfigManager::configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                      const struct _zend_agent_globals* agentGlobals)
    {
        if (configResponse.datagatherers_size() > 0) {
            setDataGatherers(configResponse.datagatherers());
        }

        if (configResponse.has_datagathererbtconfig()) {
            setDataGathererBTConfig(configResponse.datagathererbtconfig());
        }

        clearProbes();

        BOOST_FOREACH(auto& entry, m_dataGatherers) {
            const Instrumentation::MethodDataGathererConfig* gathererConfig = &(entry.second->methoddatagathererconfig());

            boost::shared_ptr<InstrumentationSite> instrumentationSite
                = InstrumentationSite::create(gathererConfig->probe().phpdefinition(), m_logger);
            if (!instrumentationSite) {
                LOG4CXX_WARN(m_logger, "Could not resolve callable from instrumentation probe config: " << gathererConfig->probe().phpdefinition().DebugString());
                continue;
            }

            const ConfigManager* pThis = this;
            boost::shared_ptr<Interceptor> interceptor(
                boost::make_shared<Interceptor>(pThis, gathererConfig));

            m_interceptionEngine->addInterceptorForCallable(instrumentationSite->getCallableInfo(), interceptor.get());
            LOG4CXX_TRACE(m_logger, "adding method data gatherer interceptor to callable " << instrumentationSite->getCallableInfo());
            m_probes.push_back(std::make_pair(instrumentationSite, interceptor));
        }
    }

    void ConfigManager::setDataGatherers(const google::protobuf::RepeatedPtrField<appdynamics::pb::DataGatherer>& dataGatherers)
    {
        m_dataGatherers.clear();
        BOOST_FOREACH(const auto& it, dataGatherers)
        {
            // Skip non-method data gatherers
            if (it.type() != appdynamics::pb::DataGatherer::METHOD)
                continue;

            // Skip non-PHP gatherers
            if (!it.methoddatagathererconfig().probe().has_phpdefinition())
                continue;

            boost::shared_ptr<appdynamics::pb::DataGatherer> gatherer(boost::make_shared<appdynamics::pb::DataGatherer>());
            gatherer->CopyFrom(it);
            m_dataGatherers[it.gathererid()] = gatherer;
        }
    }

    void ConfigManager::setDataGathererBTConfig(const appdynamics::pb::DataGathererBTConfig& dataGathererBTConfig)
    {
        m_btGatherersSet.clear();
        BOOST_FOREACH(const auto& it, dataGathererBTConfig.btconfig())
        {
            int64_t const btID = it.btid();
            BOOST_FOREACH(const auto gathererID, it.gathererids())
            {
                // Skip non-relevant gatherers
                if (m_dataGatherers.find(gathererID) == m_dataGatherers.end())
                    continue;
                    m_btGatherersSet.emplace(btID, gathererID);
            }
        }
    }

    void ConfigManager::activateProbes(bool active)
    {
        BOOST_FOREACH(auto& it, m_probes)
        {
            it.second->setActive(active);
        }
    }

    void ConfigManager::clearProbes()
    {
        BOOST_FOREACH(const auto& it, m_probes)
        {
            LOG4CXX_TRACE(m_logger, "removing method data gatherer interceptor from callable " << it.first->getCallableInfo());
            m_interceptionEngine->removeInterceptorForCallable(it.first->getCallableInfo(), it.second.get());
        }
        m_probes.clear();
    }

    bool ConfigManager::isGathererEnabledForBT(int64_t gathererID) const
    {
        TransactionContext* context =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
        bool const isEnabled = m_btGatherersSet.count(BTGatherer(context->getTransaction()->getID(), gathererID));
        return isEnabled;
    }

    Interceptor::Interceptor(const ConfigManager* configManager,
                             const Instrumentation::MethodDataGathererConfig* gathererConfig)
        : AMethodInterceptor("MethodData::Interceptor",
                             InterceptorRegistry::MethodDataInterceptor_ID)
        , m_configManager(configManager)
        , m_gathererConfig(gathererConfig)
    {
        BOOST_ASSERT(m_gathererConfig);
        if (m_logger == NullLoggerPtr) {
            m_logger = getLogger(std::string(LogContext::INSTRUMENT) + ".MethodData::Interceptor");
        }
    }

    void* Interceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
    {
        return NULL;
    }

    void Interceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
    {
        boost::shared_ptr<TransactionMonitor> txMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor();
        TransactionContext* context = txMonitor->getTransactionContext();
        if (!context)
            return;

        if (txMonitor->getSnapshotManager()->isEnabled()) {
            if (m_configManager->isGathererEnabledForBT(m_gathererConfig->id())) {
                context->gatherDataForMethod(execEnv, *m_gathererConfig);
            }
        }
    }

    void Gatherer::gatherData(const Instrumentation::MethodDataGathererConfig& gathererConfig)
    {
        LOG4CXX_DEBUG(m_logger, "method data gatherer ID " << gathererConfig.id() << " condition expression evaluation start");

        if (gathererConfig.probe().phpdefinition().has_methodmatch()) {
            if (!InstrumentationUtil::matchCallableEnvironment(
                        gathererConfig.probe().phpdefinition().methodmatch(), m_execEnv)) {
                LOG4CXX_DEBUG(m_logger, "method data gatherer ID " << gathererConfig.id() << " condition expression did not match");
                return;
            }

            LOG4CXX_DEBUG(m_logger, "method data gatherer ID " <<  gathererConfig.id() << " condition expression matched");
        }

        BOOST_FOREACH(auto dataToCollect, gathererConfig.methoddatatocollect())
        {
            if (!extractData(dataToCollect))
                // reached the data collector limit
                break;
        }
    }

    bool Gatherer::extractData(const Instrumentation::MethodDataToCollect& dataToCollect)
    {
        LOG4CXX_TRACE(m_logger, "getting data for collector [" << dataToCollect.name() << "]");
        ZValPointerAny data(Expression::evaluate(dataToCollect.data(), Expression::Context(m_execEnv)));

        // find the data point holder for this collector
        auto it = m_methodData.find(dataToCollect.name());
        if (it == m_methodData.end()) {
            if (m_methodData.size() >= MAX_DATA_COLLECTORS) {
                m_dataCollectorLimitReached = true;
                return false;
            }

            // create the new data point holder
            it = m_methodData.insert(t_methodData(dataToCollect.name(),
                                                  boost::make_shared<std::vector<ZValPointerAny>>())).first;
        }

        if (it->second->size() == MAX_DATA_COLLECTOR_POINTS) {
            std::stringstream ss;
            ss << "Max data points per collector limit [" << MAX_DATA_COLLECTOR_POINTS << "] reached";
            it->second->push_back(ZValPointerString::copy(ss.str()));
        }

        if (it->second->size() > MAX_DATA_COLLECTOR_POINTS) {
            // Even though this data collector has exceeded its occurrence
            // limit, return true so that others may run
            return true;
        }

        // append the collected data
        it->second->push_back(data);

        return true;
    }

    void Gatherer::fillInGatheredData(google::protobuf::RepeatedPtrField<appdynamics::pb::Common::NameValuePair>* nvList)
    {
        if (m_methodData.size() == 0)
            return;

        // Iterate through data collectors in insertion order, adding them to the
        // provided name-value pair container.
        auto m_methodDataEnd = m_methodData.get<1>().end();
        for (auto it = m_methodData.get<1>().begin(); it != m_methodDataEnd; ++it) {
            appdynamics::pb::Common::NameValuePair* nv = nvList->Add();
            nv->set_name(it->first);
            std::string* value = nv->mutable_value();
            dataPointsToString(*(it->second.get()), value);
        }
        if (m_dataCollectorLimitReached) {
            appdynamics::pb::Common::NameValuePair* nv = nvList->Add();
            nv->set_name("Warning");
            nv->set_value("Max number of data collectors reached");
        }
    }

    void Gatherer::dataPointsToString(const std::vector<ZValPointerAny>& dataPoints,
                                      std::string* output)
    {
        std::stringstream ss;

        bool first = true;
        bool limitReached = false;
        BOOST_FOREACH(auto& dataPoint, dataPoints) {
            ss << (first ? "" : ", ");
            first = false;

            std::string value(StringHelpers::prettyPrint(dataPoint));

            if (value.size() > MAX_COLLECTOR_OUTPUT_LENGTH)
                value = value.substr(0, MAX_COLLECTOR_OUTPUT_LENGTH) + "...";

            ss << value;

            if (ss.tellp() > MAX_COLLECTOR_OUTPUT_LENGTH)
                limitReached = true;
        }

        if (limitReached)
            ss << ", \"Data point length limit [" << MAX_COLLECTOR_OUTPUT_LENGTH << "] reached\"";

        output->assign(sanitizeUTF8(ss.str()));
    }
}
