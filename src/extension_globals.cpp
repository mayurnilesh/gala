#include "extension_globals.h"

#include <boost/preprocessor/stringize.hpp>

// Create the DELAYED_EXT_GLOBALS_CONSTRUCTOR_CALL macro to:
//   * Invoke the constructor of the GlobalPtr value for
//     the delayed extension global.
#define DELAYED_EXT_GLOBALS_CONSTRUCTOR_CALL(extName)               \
    , m_##extName(TSRMLS_C)

namespace DelayedExtensionGlobals
{

#define DELAYED_EXT_GLOBALS_NAME_STRING_DEFINITION(extName)         \
const char Globals::delayed___##extName##___name[] =                \
    BOOST_PP_STRINGIZE(extName);


DELAYED_EXTENSION_GLOBALS_TABLE(DELAYED_EXT_GLOBALS_NAME_STRING_DEFINITION);
#undef DELAYED_EXT_GLOBALS_NAME_STRING_DEFINITION

    Globals::Globals(TSRMLS_D)
        : boost::noncopyable()
          DELAYED_EXTENSION_GLOBALS_TABLE(DELAYED_EXT_GLOBALS_CONSTRUCTOR_CALL)
    {
    }
    #undef DELAYED_EXT_GLOBALS_CONSTRUCTOR_CALL


}
