/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __always_inline_h
#define __always_inline_h

// If optimizations are enabled there are some functions we want to force inline.
// The ALWAYS_INLINE_IN_RELEASE will evaluate to the correct attribute, when
// optimizations are enabled.

#if defined(__OPTIMIZE__)
#define ALWAYS_INLINE_IN_RELEASE __attribute__ ((always_inline))
#else
#define ALWAYS_INLINE_IN_RELEASE
#endif

#endif
