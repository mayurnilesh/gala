/*
    Copyright 2013 AppDynamics
    All rights reserved.
*/

#include "name_value_pair.h"
#include <ostream>

void NameValuePair::copyMapToVector(std::vector<NameValuePair>* v,
                                    const std::map<std::string, std::string>& nameValueMap)
{
    if (nameValueMap.empty())
        return;
    auto const end = nameValueMap.end();
    for (auto it = nameValueMap.begin(); it != end; ++it)
        v->push_back(NameValuePair(it->first, it->second));
}

/*
    For logging/debugging....
 */
std::ostream& operator<<(std::ostream& out, const NameValuePair& p)
{
    out << "(" << p.getName() << ", " << p.getValue() << ")";
    return out;
}
