/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#include "expressions.h"
#include "instrumentation.h"
#include "zval_helper.h"

boost::shared_ptr<InstrumentationSite> InstrumentationSite::create(
        const appdynamics::pb::Instrumentation::InstrumentationProbe_PHP& config,
        const AgentLogger& logger)
{
    std::string className;
    std::string methodName;

    if (config.has_methodmatch()) {
    // PHP Agent only supports method match with classname EQUALS for now.
        methodName = config.methodmatch().methodnamecondition().matchstrings(0);
    }

    if (config.has_classmatch()) {
        if (config.classmatch().type() != appdynamics::pb::Instrumentation::ClassMatch::MATCHES_CLASS) {
            LOG4CXX_INFO(logger, "unsupported ClassMatch type: " << appdynamics::pb::Instrumentation::ClassMatch_Type_Name(config.classmatch().type()));
            return boost::shared_ptr<InstrumentationSite>();
        }

        // PHP Agent only supports class match with classname EQUALS for now.
        className = config.classmatch().classnamecondition().matchstrings(0);
    }

    if (!className.empty() && !methodName.empty()) {
        return boost::make_shared<InstrumentationSite>(CallableInfo(className, methodName));
    } else if (!methodName.empty()) {
        return boost::make_shared<InstrumentationSite>(CallableInfo(methodName));
    }

    return boost::shared_ptr<InstrumentationSite>();
}

bool InstrumentationUtil::matchCallableEnvironment(const appdynamics::pb::Instrumentation::MethodMatch& methodMatch,
                                                   const PHPExecEnvironment* execEnv)
{
    if (methodMatch.has_matchcondition()) {
        ZValPointerAny result(Expression::evaluate(methodMatch.matchcondition(), Expression::Context(execEnv)));
        return Expression::getBooleanValue(result);
    } else {
        // no match condition means that all callable invocations match
        return true;
    }
}

double InstrumentationUtil::getMetricFromCallable(const appdynamics::pb::Instrumentation::CustomMetricDefinition& metricDef,
                                                  const PHPExecEnvironment* execEnv)
{
    ZValPointerAny result(Expression::evaluate(metricDef.data(), Expression::Context(execEnv)));
    return Expression::getDoubleValue(result);
}

