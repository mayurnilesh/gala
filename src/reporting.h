/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __REPORTING_H
#define __REPORTING_H

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

#include "common.h"
#include "agent_logger.h"

class AgentTransaction;
class TransactionContext;
class IReportingTransport;
class IBTInfoTransport;
class Snapshot;
class ExitCallInfo;
class PHPExecEnvironment;
class TransactionMonitor;

namespace appdynamics
{
    namespace pb
    {
        class BTDetails;
        class BTInfoRequest;
        class BTInfoResponse;
        class Snapshot;
    }
}

class RequestContext;

class TransactionReporter : boost::noncopyable
{
    public:
        TransactionReporter(boost::shared_ptr<IReportingTransport> reportingTransport,
                            boost::shared_ptr<IBTInfoTransport> btInfoTransport);

        bool reportTransaction(const TransactionContext* transactionContext,
                               boost::shared_ptr<RequestContext>& requestContext,
                               bool resetTransaction = false);
        bool receiveTransactionInfo(const boost::shared_ptr<RequestContext>& requestContext, bool waitAgain);
        bool reportTransactionDetails(const PHPExecEnvironment* execEnv,
                                      const TransactionContext* transactionContext,
                                      uint64_t requestEndTimeStamp,
                                      const boost::shared_ptr<RequestContext>& requestContext,
                                      Snapshot* snapshot);
        void reportSelfReResolution(int64_t backendID);
        inline bool didLastBTInfoResponseTimeOut() { return m_didLastBTInfoResponseTimeOut; }
        inline void resetBTInfoResponseTimeOutFlag() { m_didLastBTInfoResponseTimeOut = false; }

        inline void setBTInfoRepsonseTimeout(long timeoutInMS) { m_btInfoResponseTimeoutInMS = timeoutInMS; }

    private:
        boost::shared_ptr<IReportingTransport> m_reportingTransport;
        boost::shared_ptr<IBTInfoTransport> m_btInfoTransport;
        bool m_didLastBTInfoResponseTimeOut;
        AgentLogger m_logger;
        long m_btInfoResponseTimeoutInMS;

        void fillInSnapshot(const boost::shared_ptr<RequestContext>& requestContext,
                            const TransactionContext* context,
                            uint64_t requestElapsedTimeMS,
                            Snapshot* snapshot,
                            appdynamics::pb::Snapshot* pbSnapshot);
        void fillInErrors(const TransactionContext* context,
                          appdynamics::pb::BTDetails& btDetails);
};

#endif /* __REPORTING_H */

// vim: set fdm=marker
