/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __TRANSACTIONS_H
#define __TRANSACTIONS_H

/*
 * TransactionEntryPointType enum
 *
 * ATransactionEntryPointInterceptor implements ITransactionEntryPointInterceptor
 *  - determines if tx is to be identified
 *  - generates payload
 *  - gets tx acceptor from the entry point delegate
 *  - calls tx acceptor to accept and identify tx
 *  - inits data gathering, etc.
 *
 * AEntryPointDelegate
 *  - configures itself from the ConfigAccess
 *  - contains tx acceptor and tx match point config
 *  - returns naming scheme parser and match rule parser
 *  - (applies transform rules in Java)
 *
 * ATransactionAcceptor implements ITransactionAcceptor
 *  - actually identifies and names tx
 *  - registers tx
 *
 * TransactionMonitoringService
 *  - inits and configures delegates
 *  - provides tx registry
 *  - provides tx monitoring (which provides IConfigChannel)
 *  - creates TransactionConfigChannel and registers it for config changes
 *
 * IServiceConfigListener
 *  - allows classes to be notified when new config is received
 *
 * TransactionConfigChannel implements IServiceConfigListener
 *  - reconfigures delegates on new config
 *  - updates excluded tx in TMS
 *  - etc
 *
 * ConfigAcccess
 *  - provides access to parts of config response object
 *
 */

#include <boost/lexical_cast.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/utility.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <string>

#include "agent_logger.h"
#include "backend_identifier.h"
#include "common.h"
#include "errors.h"
#include "exitpoint.h"
#include "match_criteria.h"
#include "name_value_pair.h"
#include "naming.h"
#include "payload.h"
#include "request_context.h"
#include "services.h"
#include "txinfo.h"
#include "util.h"

/* {{{ Forward declarations  */

class AEntryPointDelegate;
class IEntryPointInterceptor;
class TransactionContext;
class TransactionMonitor;
class SnapshotManager;
class RequestContext;
class InterceptionEngine;
class CurrentExitCall;
class IConfigChannel;
class CorrelationHeader;
class CorrelationHeaderBuilder;
class CallMetrics;

typedef std::map<appdynamics::pb::Agent::EntryPointType, boost::shared_ptr<AEntryPointDelegate>> t_EntryPointDelegateMap;
typedef std::map<appdynamics::pb::Agent::ExitPointType, boost::shared_ptr<AExitPointDelegate>> t_ExitPointDelegateMap;

void convertPBNameValuePairsToMap(const google::protobuf::RepeatedPtrField<appdynamics::pb::Common::NameValuePair>& nameValues,
                                  StringMap& nvMap);

namespace CallGraph
{
    class GraphCollectionState;
}

namespace MethodData
{
    class Gatherer;
}

namespace appdynamics
{
    namespace pb
    {
        class BTIdentifier;
        class BackendIdentifier;
    }

    namespace Instrumentation
    {
        class MethodDataGathererConfig;
    }
}

/* }}} */

// Hash functor for pair<string, appdynamics::pb::EntryPointType>
namespace boost {

    template<> struct hash<std::pair<std::string, appdynamics::pb::Agent::EntryPointType>> {
        size_t operator()(const std::pair<std::string, appdynamics::pb::Agent::EntryPointType>& p) const {
            size_t result = 0;
            boost::hash_combine(result, p.first);
            boost::hash_combine(result, static_cast<unsigned>(p.second));
            return result;
        }
    };
}

/* {{{ struct FastGUIDMaker */
struct FastGUIDMaker
{
    inline FastGUIDMaker()
        : m_counter(0)
    {
    }

    inline std::string getAndIncrement()
    {
        // On the first request or when the counter overflows, generate a new UUID:
        if (m_counter == 0) {
            // Its very important we generate the first UUID lazily.  This
            // class in instantiated in the root apache/fpm worker.  If we generate
            // the UUID in the constructor of this class then all the works will share
            // the same UUID prefix!!!!  By generating the first UUID lazily, the UUID
            // prefix will not be generated until a apache/fpm worker gets an request.
            boost::uuids::uuid uuid = boost::uuids::random_generator()();
            m_uuidPrefix = boost::uuids::to_string(uuid);
        }

        return m_uuidPrefix + boost::lexical_cast<std::string>(m_counter++);
    }

    private:
        std::string m_uuidPrefix;
        /* 16 bit unsigned counter will range between 0 and 65535.
         * Every time it overflows back to 0, we generate a new UUID.
         */
        uint16_t m_counter;
};
/* }}} */

/* {{{ class AgentTransaction */
class AgentTransaction : boost::noncopyable
{
    template <class T, class Arg1, class... Args>
    friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
public:
    static const int64_t NULL_BT_ID = 0;

    /**
        Creates an AgentTransaction instance for a discovered and as of yet unregistered
        business transaction.  An unregistered business transaction does not have
        a business traction id yet.
        @return A new AgentTransaction instance.
     */
    static inline boost::shared_ptr<AgentTransaction>
        createDiscoveredUnRegisteredTransaction(const std::string& name,
                                                const std::string& entryPointType,
                                                const boost::shared_ptr<MatchPointConfig>& matchPointConfig,
                                                int64_t componentID)
    {
        BOOST_ASSERT(matchPointConfig);
        boost::shared_ptr<MatchCriteriaLocal>
            criteria(boost::make_shared<MatchCriteriaLocal>(matchPointConfig));

        // make_shared takes a reference, so we need to use
        // a local variable to pass NULL_BT_ID to make_shared.
        // If we don't do this then we'll need to provide a definition
        // in a .cpp file to prevent linker errors.  In the release build
        // this madness should all get boiled away.
        const int64_t nullBTID = NULL_BT_ID;

        // Work-around for GCC 4.4 bug
        bool isContinuing = false;

        return boost::make_shared<AgentTransaction>(name, entryPointType, componentID, criteria, nullBTID, isContinuing);
    }

    static inline boost::shared_ptr<AgentTransaction>
        createMatchedUnRegisteredTransaction(const std::string& name,
                                             const std::string& entryPointType,
                                             const boost::shared_ptr<MatchPointConfig>& matchPointConfig,
                                             const appdynamics::pb::Agent::MatchPointConfig::CustomMatch* customMatch,
                                             int64_t componentID)
    {
        BOOST_ASSERT(matchPointConfig);
        boost::shared_ptr<MatchCriteriaLocal>
            criteria(boost::make_shared<MatchCriteriaLocal>(matchPointConfig, customMatch));

        // make_shared takes a reference, so we need to use
        // a local variable to pass NULL_BT_ID to make_shared.
        // If we don't do this then we'll need to provide a definition
        // in a .cpp file to prevent linker errors.  In the release build
        // this madness should all get boiled away.
        const int64_t nullBTID = NULL_BT_ID;

        // Work-around for GCC 4.4 bug
        bool isContinuing = false;

        return boost::make_shared<AgentTransaction>(name, entryPointType, componentID, criteria, nullBTID, isContinuing);
    }

    /**
        Creates an AgentTransaction instance for a registered
        business transaction.  A registered business transaction has a business transaction
        id which can be used to refer to the business transaction in correction headers
        and collected metrics.
        @return A new AgentTransaction instance.
     */
    static inline boost::shared_ptr<AgentTransaction>
        createRegisteredTransaction(const std::string& name,
                                    const std::string& entryPointType,
                                    const boost::shared_ptr<MatchPointConfig>& matchPointConfig,
                                    const appdynamics::pb::Agent::MatchPointConfig::CustomMatch* customMatch,
                                    int64_t componentID,
                                    int64_t btID)
    {
        BOOST_ASSERT(matchPointConfig);
        boost::shared_ptr<MatchCriteriaLocal>
            criteria(boost::make_shared<MatchCriteriaLocal>(matchPointConfig, customMatch));
        //
        // Work-around for GCC 4.4 bug
        bool isContinuing = false;

        return boost::make_shared<AgentTransaction>(name, entryPointType, componentID, criteria, btID, isContinuing);
    }


    static inline boost::shared_ptr<AgentTransaction>
        createContinuingRegisteredTransaction(int64_t btID)
    {
        BOOST_ASSERT(btID != NULL_BT_ID);

        // Work-around for GCC 4.4 bug
        bool isContinuing = true;

        return boost::make_shared<AgentTransaction>(btID, isContinuing);
    }

    static inline boost::shared_ptr<AgentTransaction>
        createContinuingUnRegisteredTransaction(const std::string& name,
                                                const std::string& entryPointType,
                                                int64_t originatingComponentID,
                                                const boost::shared_ptr<MatchCriteria>& criteria)
    {
        const int64_t nullBTID = NULL_BT_ID;
        //
        // Work-around for GCC 4.4 bug
        bool isContinuing = true;

        return boost::make_shared<AgentTransaction>(name, entryPointType, originatingComponentID, criteria, nullBTID, isContinuing);
    }

    inline ~AgentTransaction()
    {
    }

    int64_t getID() const { return m_txID; }
    const std::string& getEntryPointType() const { return m_txInfo.getEntryPointType(); }
    const std::string& getName() const { return m_txInfo.getName(); }
    int64_t getComponentID() const { return m_txInfo.getComponentID(); }
    const boost::shared_ptr<MatchCriteria>& getMatchCriteria() const { return m_criteria; }

    inline const TxInfo& getTxInfo() const { return m_txInfo; }

    const std::string getDisplayName() const;

    /**
        Determines if this business transaction has been registered.
        @return true if this business transaction has been registered and it has
        a business transaction id, false otherwise.
     */
    bool isRegistered() const { return m_txID != NULL_BT_ID; }

    /**
        @return true if this business transaction was automatically discovered,
        false otherwise.
     */
    bool isAutoDiscovered() const;

    /**
        @return true if this business transaction was named with a custom match rule,
        false otherwise.
     */
    bool isCustom() const;

    void fillInBTIdentifier(appdynamics::pb::BTIdentifier* btIdentifier);

private:
    AgentTransaction(const std::string& name,
                     const std::string& entryPointType,
                     int64_t componentID,
                     const boost::shared_ptr<MatchCriteria>& criteria,
                     int64_t txID,
                     bool continuing)
        : m_txID(txID)
        , m_txInfo(name,
                   entryPointType,
                   componentID)
        , m_criteria(criteria)
        , m_continuing(continuing)
    {
    }

    AgentTransaction(int64_t txID, bool continuing)
        : m_txID(txID)
        , m_continuing(continuing)
    {
    }

    int64_t const m_txID;
    TxInfo const m_txInfo;
    boost::shared_ptr<MatchCriteria> const m_criteria;
    bool m_continuing;
};
/* }}} */

/* {{{ class TransactionContext */

class TransactionContext : boost::noncopyable
{
    public:
        enum Origin {
            RINIT, API
        };

        TransactionContext(const boost::shared_ptr<RequestContext>& requestContext,
                           const boost::shared_ptr<AgentTransaction>& tx,
                           appdynamics::pb::Agent::EntryPointType type,
                           const TimePoint& startTime);

        virtual ~TransactionContext();

        const boost::shared_ptr<AgentTransaction>& getTransaction() const { return m_agentTX; }
        appdynamics::pb::Agent::EntryPointType getEntryPointType() const { return m_entryPointType; }

        inline Origin getOrigin() const { return m_origin; }
        inline void setOrigin(Origin value) { m_origin = value; }

        CurrentExitCall* getCurrentExitCall() const { return m_currentExitCall; }
        inline void setCurrentExitCall(CurrentExitCall* exit_call)
        {
            m_currentExitCall = exit_call;
        }

        bool snapshotEnabled() const { return m_snapshotEnabled; }
        void setSnapshotEnabled(bool enabled) { m_snapshotEnabled = enabled; }

        inline bool isDownstreamDetectionDisabled() const { return m_downstreamDetectionDisabled; }
        inline void setDownstreamDetectionDisabled(bool value) { m_downstreamDetectionDisabled = value; }

        inline bool isContinuingTransaction() const { return m_continuingTransaction; }
        inline void setContinuingTransaction(bool value) { m_continuingTransaction = value; }

        inline const boost::shared_ptr<CorrelationHeader>& getCorrelationHeader() const
        {
            return m_correlationHeader;
        }
        inline void setCorrelationHeader(const boost::shared_ptr<CorrelationHeader>& correlationHeader)
        {
            m_correlationHeader = correlationHeader;
        }
        void setTransactionPayload(const boost::shared_ptr<IMatchPointPayload>& payload)
        {
            m_txPayload = payload;
        }
        boost::shared_ptr<IMatchPointPayload>& getTransactionPayload()
        {
            return m_txPayload;
        }

        inline const boost::shared_ptr<CorrelationHeaderBuilder>& getCorrelationHeaderBuilder() const
        {
            return m_correlationHeaderBuilder;
        }
        inline void setCorrelationHeaderBuilder(const boost::shared_ptr<CorrelationHeaderBuilder>& correlationHeaderBuilder)
        {
            m_correlationHeaderBuilder = correlationHeaderBuilder;
        }

        const std::string getTransactionName();
        void clearCurrentExitCall();

        /**
            Checks to see if there is a current agent transaction and if there is, is
            that transaction registered with the controller yet.
            @return true if the current agent transaction is registered with the
            controller, false otherwise.
         */
        bool isCurrentTransactionRegistered() const;

        bool isErrorTransaction() const;

        ErrorRegistry* getErrorRegistry() const;

        /**
         * Reports elapsed time for an exit call against the given backend
         * and category.
         * Increments the call count by 1 and error count, if there is an
         * error.
         *
         * @param backendID Identifier of the backend.
         * @param category The category to assign the metrics to (can be
         * empty)
         * @param exitType Exit point type (here to help proxy avoid an
         * extra look-up)
         * @param timeTaken Elapsed time, in microseconds.
         * @param callCount Number of calls to count during the elapsed time
         * (can be 0, to accumulate time without incrementing call count)
         * @param hasError Whether the exit call encountered an error
         * condition.
         * @param errorCount The number of errors that occurred during the exit call.
         */
        void reportExitCallData(const BackendIdentifierPtr& backendID,
                                const std::string& category,
                                long timeTaken,
                                long callCount,
                                unsigned errorCount);

        void reportExitCall(const CurrentExitCall* currentExitCall);

        /**
         * Returns exit call metrics map. The key is the registered backend
         * ID and the value is the object maintaining exit call metrics.
         */
        const boost::unordered_map<ExitCallMetricsKey, boost::shared_ptr<CallMetrics>>& getRegisteredExitCallMetrics() const;

        inline void setInitiatingInterceptor(const IEntryPointInterceptor* interceptor)
        {
            m_initiatingInterceptor = interceptor;
        }
        inline const IEntryPointInterceptor* getInitiatingInterceptor() const
        {
            return m_initiatingInterceptor;
        }

        inline void incrementExitCallCounter() { ++m_exitCallCounter; }
        inline uint64_t getExitCallCounter() { return m_exitCallCounter; }

        inline const boost::shared_ptr<RequestContext>& getRequestContext() const { return m_requestContext; }

        void gatherDataForMethod(const PHPExecEnvironment* execEnv,
                                 const appdynamics::pb::Instrumentation::MethodDataGathererConfig& gathererConfig);
        void mergeMethodDataIntoSnapshot(appdynamics::pb::Snapshot* pbSnapshot) const;

        /**
         * Returns true if the entrypoint type is web AND the transaction origin is NOT from the API.
         */
        inline bool isWeb() const
        {
            return m_entryPointType == appdynamics::pb::Agent::PHP_WEB
                && m_origin != TransactionContext::API;
        }

        /**
            @return the time stamp for the time this transaction context was created.
         */
        inline const TimePoint& getStartTime() const { return m_startTime; }

    private:
        boost::shared_ptr<RequestContext> const m_requestContext;
        boost::shared_ptr<AgentTransaction> const m_agentTX;
        appdynamics::pb::Agent::EntryPointType const m_entryPointType;
        Origin m_origin;
        CurrentExitCall* m_currentExitCall;
        bool m_snapshotEnabled;
        bool m_downstreamDetectionDisabled;
        const IEntryPointInterceptor* m_initiatingInterceptor;
        uint64_t m_exitCallCounter;
        bool m_continuingTransaction;
        boost::shared_ptr<CorrelationHeader> m_correlationHeader;
        boost::shared_ptr<CorrelationHeaderBuilder> m_correlationHeaderBuilder;
        boost::shared_ptr<MethodData::Gatherer> m_methodDataGatherer;
        boost::shared_ptr<IMatchPointPayload> m_txPayload;
        const TimePoint m_startTime;
};

inline bool TransactionContext::isCurrentTransactionRegistered() const
{
    if (!m_agentTX)
        return false;
    return m_agentTX->isRegistered();
}

/* }}} */

/* {{{ Transaction acceptor classes
 *
 * Given payload data, identifies and names the transaction.
 * Additionally, registers it with the TransactionMonitoringService.
 */
class ITransactionAcceptor : boost::noncopyable {
    public:
        virtual ~ITransactionAcceptor();
        virtual bool acceptTransaction(const PHPExecEnvironment* phpExecEnv, const boost::shared_ptr<IMatchPointPayload>& payload) = 0;
};

class ATransactionAcceptor : public ITransactionAcceptor {
    public:
        ATransactionAcceptor(const boost::shared_ptr<MatchPointConfig>& config,
                             TransactionMonitor* monitor)
            : m_logger(::getLogger(std::string(LogContext::TX_SERVICE) + ".ATransactionAcceptor"))
            , m_matchPointConfig(config)
            , m_txMonitor(monitor)
        {
        }

        virtual ~ATransactionAcceptor();

        virtual bool acceptTransaction(const PHPExecEnvironment* phpExecEnv, const boost::shared_ptr<IMatchPointPayload>& payload);

        boost::shared_ptr<AgentTransaction> getTransaction(const std::string& transactionName,
                                                           const appdynamics::pb::Agent::MatchPointConfig::CustomMatch* customMatch);

    protected:

        virtual inline boost::shared_ptr<CorrelationHeader>
            checkForContinuingTransaction(const boost::shared_ptr<IMatchPointPayload>& iPayload)
        {
            return boost::shared_ptr<CorrelationHeader>();
        }

        virtual bool isDiscoveryEnabled();
        virtual boost::shared_ptr<AgentTransaction>
            discoverTransaction(const boost::shared_ptr<IMatchPointPayload>& payload);
        // Iff the payload matches a rule, returns true.
        // Iff the payload matched a rule that was not excluded, *matchedTransaction != NULL.
        virtual bool
            customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                   const boost::shared_ptr<IMatchPointPayload>& payload);
        virtual bool matchTransactionRules(const boost::shared_ptr<IMatchPointPayload>& payload,
                                           const google::protobuf::RepeatedPtrField<appdynamics::pb::Agent::EntryPointMatchCondition>& matchConditions);
        virtual bool matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& payload,
                                          const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition);
        virtual std::string getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>&) = 0;

        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() = 0;

        const boost::shared_ptr<MatchPointConfig>& getConfig() { return m_matchPointConfig; }

        const AgentLogger& getLogger() const { return m_logger; }

        TransactionMonitor* getTransactionMonitor() const { return m_txMonitor; }
    private:
        boost::shared_ptr<AgentTransaction> matchOrDiscoverTransaction(const boost::shared_ptr<IMatchPointPayload>& payload, bool const isCorrHeaderValid);

        AgentLogger m_logger;
        boost::shared_ptr<MatchPointConfig> m_matchPointConfig;
        TransactionMonitor* const m_txMonitor;

};

/* }}} */

/* {{{ Transaction logger */

class TxLogger : boost::noncopyable {
    public:
        TxLogger() : tx_index(0), dropped_index(0)
        {
            logger = getLogger(std::string(LogContext::TX_SERVICE) + ".logger");
            LOG4CXX_TRACE(logger, "starting BT logging");
        }
        ~TxLogger();

        void logIdentifiedTx(const std::string& txName,
                             appdynamics::pb::Agent::EntryPointType entryPointType,
                             bool isAutoDiscovered);
        void logDroppedTx(const std::string& txName,
                          appdynamics::pb::Agent::EntryPointType entryPointType,
                          bool isAutoDiscovered);
        void logDroppedTxStats() const;

        void clearDiscoveredTransactions(const google::protobuf::RepeatedPtrField<appdynamics::pb::RegisteredBT>& registered);
        void reset();

    private:
        static const int MAX_DROPPED_INDEX = 200;

        AgentLogger logger;

        int tx_index;
        int dropped_index;
        boost::unordered_set<std::string> discovered;
        boost::unordered_map<std::pair<std::string, appdynamics::pb::Agent::EntryPointType>, int> dropped;
};

/* }}} */

/* {{{ class TransactionRegistry */
class TransactionRegistry : public IMonitorStateListener, boost::noncopyable {
    public:
        static const unsigned MAX_TRANSACTIONS = 100;

        TransactionRegistry(const boost::shared_ptr<IConfigChannel>& configChannel)
            : m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".TransactionRegistry"))
            , m_configChannel(configChannel)
        {
        }
        virtual ~TransactionRegistry();

        ExitCallRegistry* getExitCallRegistry() { return &exitcall_registry; }

        // adds new AT to the registry
        void addUnRegisteredTransaction(const boost::shared_ptr<AgentTransaction>& discoveredTransaction);

        /**
            @return Total number of unique business transactions encountered.
         */
        inline size_t getTotalNumberOfTransactions() const
        {
            return m_transactions.size();
        }

        void setRegisteredTransactions(const google::protobuf::RepeatedPtrField<appdynamics::pb::RegisteredBT>& registered);

        /**
            Drops all registered business transactions and all registered
            backend/exit class.  This is called from
            TransactionConfigChannel::configChanged when the proxy has indicated that
            all any previous business transaction id's and backend id's it has sent
            us are now invalid.
         */
        void reset();

        void onMonitorDisable();
        void onMonitorEnable();

        inline const boost::unordered_map<TxInfo, int64_t>& getTransactions() const
        {
            return m_transactions;
        }

    private:
        AgentLogger m_logger;
        boost::unordered_map<TxInfo, int64_t> m_transactions;
        ExitCallRegistry exitcall_registry;
        boost::shared_ptr<IConfigChannel> m_configChannel;
};

/* }}} */

/* {{{ class TransactionMonitor */
class TransactionMonitor : public IConfigListener, public IBailoutHandler, boost::noncopyable {
    public:
        static const unsigned MAX_METRIC_NAME_LENGTH = 750;

        TransactionMonitor(AgentContext* agentContext);
        ~TransactionMonitor();

        bool isEnabled() const { return m_enabled; }
        void enable();
        void disable();
        void reset();
        void disableBTDetection();

        void configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                           const struct _zend_agent_globals* agentGlobals);

        void onRequestBegin(const PHPExecEnvironment* phpExecEnv, const boost::shared_ptr<RequestContext>& requestContext);
        void onRequestEnd(const PHPExecEnvironment* phpExecEnv, const boost::shared_ptr<RequestContext>& requestContext,
                bool isRequestEnd);
        virtual void afterBailout();

        TransactionRegistry* getTransactionRegistry() { return &tx_registry; }
        boost::shared_ptr<IConfigChannel> getConfigChannel() { return m_configChannel; }
        const boost::shared_ptr<IConfigChannel>& getConfigChannel() const { return m_configChannel; }
        TransactionContext* getTransactionContext() { return m_transactionContext; }
        TransactionReporter* getTransactionReporter() { return tx_reporter.get(); }
        ErrorMonitor* getErrorMonitor() { return &m_errorMonitor; }
        TxLogger* getTxLogger() { return &tx_logger; }
        boost::shared_ptr<SnapshotManager> getSnapshotManager() { return m_snapshotManager; }
        const boost::shared_ptr<SnapshotManager>& getSnapshotManager() const { return m_snapshotManager; }

        void abortTransaction();

        // creates a TransactionContext or overwrites the existing one, and returns it
        TransactionContext* addTransaction(const boost::shared_ptr<AgentTransaction>& agentTx,
                                           appdynamics::pb::Agent::EntryPointType entryPointType,
                                           const TimePoint& startTime);
        TransactionContext* addContinuingTransaction(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                                     appdynamics::pb::Agent::EntryPointType interceptingEntryPointType,
                                                     const TimePoint& startTime);

        bool isTransactionExcluded(const std::string& name, appdynamics::pb::Agent::EntryPointType entryPointType);
        void setExcludedTransactions(const boost::unordered_set<std::pair<std::string, appdynamics::pb::Agent::EntryPointType > >& excluded) {
            excluded_txs = excluded;
        }

        /**
            @return the maximum number of backends we should track.
         */
        size_t getMaxDiscoveredBackends() const { return 50; }

        inline FastGUIDMaker& getGUIDMaker() { return m_guidMaker; }

        inline boost::shared_ptr<RequestContext>& getRequestContext() { return m_requestContext; }

        AEntryPointDelegate* getEntryPointDelegate(appdynamics::pb::Agent::EntryPointType type);
        AExitPointDelegate*  getExitPointDelegate(appdynamics::pb::Agent::ExitPointType type);

    private:
        AgentLogger m_logger;

        boost::shared_ptr<IConfigChannel> m_configChannel;
        boost::shared_ptr<SnapshotManager> m_snapshotManager;
        TransactionRegistry tx_registry;
        ErrorMonitor m_errorMonitor;
        TxLogger tx_logger;
        unique_ptr<TransactionReporter>::type tx_reporter;
        TransactionContext* m_transactionContext;
        boost::shared_ptr<RequestContext> m_requestContext;

        boost::unordered_set<std::pair<std::string, appdynamics::pb::Agent::EntryPointType>> excluded_txs;

        bool m_enabled;
        FastGUIDMaker m_guidMaker;

        t_EntryPointDelegateMap m_entryPointDelegates;
        t_ExitPointDelegateMap m_exitPointDelegates;

        void createEntryPointDelegates(InterceptionEngine* interceptEngine);
        void createExitPointDelegates(InterceptionEngine* interceptEngine);
};
/* }}} */

#endif /* __TRANSACTIONS_H */

// vim: set fdm=marker:
