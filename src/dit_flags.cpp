/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#include "dit_flags.h"
#include "agent_logger.h"
#include <boost/static_assert.hpp>
#include <boost/assert.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <bitset>

static inline void parseBitSet(const std::string& ditString,
                               std::bitset<DITFlag_LAST>* bitSet)
{
    BOOST_STATIC_ASSERT(DITFlag_LAST != 0);

    unsigned nFlags = ditString.length();
    if (nFlags == 0)
        return;
    if (nFlags > DITFlag_LAST)
        return;

    unsigned count = 0;
    auto i = ditString.end();
    auto const begin = ditString.begin();
    do {
        --i;
        const char c = *i;
        if (c == '0')
            bitSet->reset(count);
        else if (c == '1')
            bitSet->set(count, 1);
        else {
            bitSet->reset();
            return;
        }
        ++count;
    } while (i != begin);
}


#define DIT_SET_FLAG(f)                 \
    if (bitSet.test(DITFlag_##f))       \
        ditFlags->f = true;

void parseDITFlags(const std::string& ditString,
                   DITFlags* ditFlags)
{
    BOOST_ASSERT(ditFlags != NULL);
    std::bitset<DITFlag_LAST> bitSet;
    parseBitSet(ditString, &bitSet);

    DIT_FLAGS_TABLE(DIT_SET_FLAG);
}

#undef DIT_SET_FLAG

#define DIT_SET_FLAG(f)                               \
    bitSet.set(DITFlag_##f, ditFlags.f);

std::string ditFlagsToString(const DITFlags& ditFlags)
{
    std::bitset<DITFlag_LAST> bitSet;
    DIT_FLAGS_TABLE(DIT_SET_FLAG);

    unsigned i = 0;
    while ((i < DITFlag_LAST) && (!bitSet.test((DITFlag_LAST - 1) - i)))
        ++i;
    std::string result;
    result.reserve(DITFlag_LAST - i);
    while (i < DITFlag_LAST) {
        result.append(1, bitSet.test((DITFlag_LAST - 1) - i) ? '1' : '0');
        ++i;
    }
    if (result.empty())
        result.append(1, '0');
    return result;
}

#undef DIT_SET_FLAG

#define DIT_LOG_FLAG(f)                             \
    << (ditFlags.f ? " " : "")                      \
    << (ditFlags.f ? BOOST_PP_STRINGIZE(f) : "")

void logDITFlags(const AgentLogger& logger, const DITFlags& ditFlags)
{
    if (ditFlags.anySet())
        LOG4CXX_INFO(logger, "DIT Flags:" DIT_FLAGS_TABLE(DIT_LOG_FLAG));
}

#undef DIT_LOG_FLAG
