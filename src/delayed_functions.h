/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __delayed_functions_h
#define __delayed_functions_h

#include <boost/utility.hpp>
#include <boost/function_types/result_type.hpp>

#include "delayed_functions_table.h"

namespace DelayedFunctions
{
    /**
        Helper template used by FunctionTable below.  This
        template changes a class with an operator() that will lazily lookup
        a symbol with dlsym and then call that function.

        @param t_Function Function pointer type of the function whose resolution
        is being delayed till runtime.
        @param s_Function The function whose resolution is being delayed till runtime.
        This template never references this template argument.  It is specified here
        to ensure users of this template have properly specified the function pointer
        type in the previous template argument.
        @param functionName The name of the function whose resolution is being delayed
        till runtime as a null terminated string.
     */
    template <typename t_Function,
              t_Function s_Function,
              const char* functionName1,
              const char* functionName2>
    class Function : boost::noncopyable
    {
    private:
        typedef typename boost::function_types::result_type<t_Function>::type t_ReturnType;
    public:
        Function()
            : m_functionPointer(NULL)
            , m_initialized(false)
        {
        }

        /**
            @return true if the target function has been successfully resolved,
            false otherwise.  The operator() below will crash if this
            method return false.
         */
        bool isValid() const
        {
            if (m_functionPointer)
                return true;
            return init();
        }

        /**
            Call's the resolved function.
         */
        template <typename...t_Args>
        inline t_ReturnType operator()(t_Args... args) const
        {
            init();
            return m_functionPointer(args...);
        }


    private:
        /**
            Resolves the function.
         */
        inline bool init() const
        {
            if (m_initialized)
                return m_functionPointer != NULL;
            m_initialized = true;
            // Using a C style cast to function pointer types.
            m_functionPointer = (t_Function) dlsym(RTLD_DEFAULT, functionName1);
            if (m_functionPointer)
                return true;
            if (!functionName2[0])
                return false;
            m_functionPointer = (t_Function) dlsym(RTLD_DEFAULT, functionName2);
            return m_functionPointer != NULL;
        }

        mutable t_Function m_functionPointer;
        mutable bool m_initialized;
    };

    /**
        Table of functions we want to call, but can not directly link against
        on CentOS/RHEL linux distributions.
        <p>
        To add a function to the table see delayed_functions_table.h.
        <p>
        There is a get_XXX() that returns a const reference to
        a callable for each function.
     */
    class FunctionTable : boost::noncopyable
    {
    private:

        static const char nullName[];

// Set up the DELAYED_FUNCTION to:
//   * forward declare a static array of bytes for the function name
//   * declare a typedef for the delayed function's type.
//   * Declare a member variable to hold the Function<> value
//     for the delayed function.
#define DELAYED_FUNCTION_STORAGE_DECL(function, returnType, args)   \
    static const char delayed___##function##___name[];              \
    typedef returnType (*t_delayed___##function)args;               \
    Function<t_delayed___##function,                                \
             function,                                              \
             delayed___##function##___name,                         \
             nullName> const m_##function;
#define DELAYED_FUNCTION_STORAGE_DECL_EX(function,                  \
                                         functionName1,             \
                                         functionName2,             \
                                         returnType, args)          \
    static const char delayed___##function##___name_1[];            \
    static const char delayed___##function##___name_2[];            \
    typedef returnType (*t_delayed___##function)args;               \
    Function<t_delayed___##function,                                \
             function,                                              \
             delayed___##function##___name_1,                       \
             delayed___##function##___name_2> const m_##function;

    DELAYED_FUNCTIONS_TABLE(DELAYED_FUNCTION_STORAGE_DECL,          \
                            DELAYED_FUNCTION_STORAGE_DECL_EX)
#undef DELAYED_FUNCTION_STORAGE_DECL
#undef DELAYED_FUNCTION_STORAGE_DECL_EX

    public:

// Set up the DELAYED_FUNCTION to:
//   * declare and define an inline getter to get the
//     Function<> value for the delayed function.
#define DELAYED_FUNCTION_ACCESSOR(function, returnType, args)                       \
    inline const Function<t_delayed___##function,                                   \
                          function,                                                 \
                          delayed___##function##___name,                            \
                          nullName>& get_##function() const                         \
    {                                                                               \
        return m_##function;                                                        \
    }
#define DELAYED_FUNCTION_ACCESSOR_EX(function,                                      \
                                     functionName1,                                 \
                                     functionName2,                                 \
                                     returnType, args)                              \
    inline const Function<t_delayed___##function,                                   \
             function,                                                              \
             delayed___##function##___name_1,                                       \
             delayed___##function##___name_2>& get_##function() const               \
    {                                                                               \
        return m_##function;                                                        \
    }
    DELAYED_FUNCTIONS_TABLE(DELAYED_FUNCTION_ACCESSOR,                              \
                            DELAYED_FUNCTION_ACCESSOR_EX)
#undef DELAYED_FUNCTION_ACCESSOR
#undef DELAYED_FUNCTION_ACCESSOR_EX

    };
}

namespace boost
{
    namespace function_types
    {
        template <typename t_Function, t_Function s_Function, const char* functionName1, const char* functionName2>
        struct result_type<DelayedFunctions::Function<t_Function, s_Function, functionName1, functionName2> >
        {
            typedef typename boost::function_types::result_type<t_Function>::type type;
        };
    }
}

#endif
