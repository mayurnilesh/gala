#ifndef __timer_cgt_h
#define __timer_cgt_h

extern "C" {

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

}

#include "common.h"

/**
    Class to provide microsecond time stamps.
 */
class Timer {
    public:
        inline int init() { m_lastStamp = 0; return SUCCESS; }

        /*
         * Callback for beginning of request.
         */
        void onRequestBegin(uint64_t nanoSecondsToSleep);

        /**
         * Callback for end of request.
         */
        void onRequestEnd();

        /*
         * Returns the current timestamp, in microseconds
         */
        uint64_t getTimestamp() const;

        /*
         * Returns the elapsed time since the provided timestamp. Both values
         * are in microseconds.
         */
        uint64_t getElapsedTime(uint64_t startTime) const;

        /*
         * Return the elapsed time given the start and end timestamps. Both
         * values are in microseconds.
         */
        uint64_t getElapsedTime(uint64_t startTime, uint64_t endTime) const;

        /*
         * Return the elapsed time given the start and end timestamps. Both
         * values are in microseconds.
         */
        uint64_t getElapsedTime(uint64_t startTime, uint64_t endTime) const;

        /*
         * Returns current system time, in milliseconds.
         */
        static uint64_t getCurrentTime();

    private:
        uint64_t stamp() const;

        mutable uint64_t m_lastStamp;
};

/**
 * Get the timestamp via gettimeofday() syscall.
 *
 * @return 64 bit unsigned integer
 */
inline uint64_t Timer::stamp() const {
    struct timespec tv;
    clock_gettime(CLOCK_MONOTONIC, &tv);
    uint64_t const newStamp =
        (static_cast<uint64_t>(tv.tv_sec) * 1000000ULL) + (static_cast<uint64_t>(tv.tv_nsec) / 1000ULL);
    if (newStamp < m_lastStamp)
        return m_lastStamp;
    m_lastStamp = newStamp;
    return newStamp;
}

inline uint64_t Timer::getCurrentTime()
{
    struct timespec tv;
    clock_gettime(CLOCK_REALTIME, &tv);
    return (uint64_t)tv.tv_sec * 1000L + tv.tv_nsec / 1000000L;
}

inline void Timer::onRequestBegin(uint64_t nanoSecondsToSleep)
{
}

inline void Timer::onRequestEnd()
{
}

inline uint64_t Timer::getTimestamp() const
{
    return stamp();
}

inline uint64_t Timer::getElapsedTime(uint64_t startTime) const
{
    uint64_t const endStamp = stamp();
    // Result of NTP adjustment or something similar
    if (endStamp < startTime)
        return 0;

    uint64_t elapsedTime = endStamp - startTime;
    return elapsedTime;
}

inline uint64_t Timer::getElapsedTime(uint64_t startTime, uint64_t endTime) const
{
    return (endTime - startTime);
}

#endif /* __timer_gtod_h */
