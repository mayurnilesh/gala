/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __SNAPSHOT_H
#define __SNAPSHOT_H

#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include "services.h"
#include "agent_logger.h"
#include "configs.h"
#include "callgraph/call_graph.h"
#include "db/snapshot_sql_info.h"

class ZValPointerArray;
class Snapshot;

namespace CallGraph
{
    class GraphCollectionState;
}

class HTTPDataGatherer : boost::noncopyable
{
    public:
        template <class T> friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared();
        static inline boost::shared_ptr<HTTPDataGatherer> create()
        {
            return boost::make_shared<HTTPDataGatherer>();
        }

        void setDataGatherers(const google::protobuf::RepeatedPtrField<appdynamics::pb::DataGatherer>& dataGatherers);
        void setDataGathererBTConfig(const appdynamics::pb::DataGathererBTConfig& dataGathererBTConfig);

        void process(const PHPExecEnvironment* execEnv,
                     const TransactionContext* context);

        inline bool enabled() const { return m_enabled; };
        void setEnabled(bool enabled);

    private:
        AgentLogger m_logger;
        boost::unordered_map<int64_t, boost::shared_ptr<appdynamics::pb::DataGatherer>> m_dataGatherers;
        boost::unordered_multimap<int64_t, int64_t> m_btToGatherersMap;

        inline HTTPDataGatherer()
            : m_enabled(true)
        {
            m_logger = getLogger(std::string(LogContext::DATAGATHERER) + ".HTTP");
        };

        void gatherHTTPRequestParameters(const ZValPointerArray& paramsArray,
                                        const google::protobuf::RepeatedPtrField<appdynamics::pb::HTTPRequestParameter>& paramsToGather,
                                        StringMap& target);
        static void gatherAllHTTPRequestParameters(const ZValPointerArray& paramsArray,
                                                StringMap& target);
        bool m_enabled;
        static bool s_didAddInterceptor;
};

class HTTPData
{
    public:
        template <class T> friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared();
        static inline boost::shared_ptr<HTTPData> create()
        { return boost::make_shared<HTTPData>(); };

        inline void setHttpParams(StringMap& value) { m_httpParams = value; }
        inline StringMap getHttpParams() const { return m_httpParams; }

        inline void setCookies(StringMap& value) { m_cookies = value; }
        inline StringMap getCookies() const { return m_cookies; }

        inline void setHeaders(StringMap& value) { m_headers = value; }
        inline StringMap getHeaders() const { return m_headers; }

        inline void setSessionEntries(StringMap& value) { m_sessionEntries = value; }
        inline StringMap getSessionEntries() const { return m_sessionEntries; }

        inline void setSessionId(std::string& value) { m_sessionId = value; }
        inline std::string getSessionId() const { return m_sessionId; }

        inline void setRequestMethod(const char* requestMethod) { m_requestMethod = requestMethod; }
        inline const std::string& getRequestMethod() const { return m_requestMethod; }

        inline void setResponseCode(unsigned short responseCode)
        { m_responseCode = boost::lexical_cast<std::string>(responseCode); }
        inline const std::string& getResponseCode() const { return m_responseCode; }

    private:
        inline HTTPData() { };
        StringMap m_httpParams;
        StringMap m_cookies;
        StringMap m_headers;
        StringMap m_sessionEntries;
        std::string m_sessionId;
        std::string m_requestMethod;
        std::string m_responseCode;
};


/* {{{ class SnapshotManager */

class SnapshotManager : public IConfigListener, boost::noncopyable
{
    public:
        static const int MIN_EXIT_CALL_DURATION = 15;     // milliseconds
        static const int MIN_DB_EXIT_CALL_DURATION = 10;  // milliseconds
        static const uint32_t MAX_UNIQUE_SQL_CALLS = 500;
        static const int MIN_CALLGRAPH_NODE_DURATION = 10; // milliseconds

        SnapshotManager(const AgentContext* agentContext);
        ~SnapshotManager();

        void disable();
        void enable();
        bool isEnabled() { return m_enabled; }

        void setURL(const std::string& url);

        int getMinExitCallTime() const { return m_minExitCallTime; }
        int getMinSQLExecTime() const { return m_minSQLExecTime; }
        inline int getMinCallGraphNodeDuration() const { return m_minCallGraphNodeDuration; }
        bool skipInternals() const { return m_skipInternals; }
        bool captureRawSQL() const { return m_captureRawSQL; }
        uint32_t maxUniqueSQLCalls() const { return m_maxUniqSQLCalls; }

        bool isCallGraphEnabled() { return m_callgraphEnabled; }
        void setCallGraphEnabled(bool callgraphEnabled)
        {
            m_callgraphEnabled = callgraphEnabled && m_enabled;
        }

        Snapshot* startSnapshot(CallGraph::GraphCollectionState* callGraphCollectionState, bool purgeCallGraph = true);
        Snapshot* finishSnapshot(const CallGraph::GraphCollectionState* callGraphCollectionState);
        void resetSnapshot(CallGraph::GraphCollectionState* callGraphCollectionState);
        Snapshot* getSnapshot() const { return snapshot; }

        const boost::shared_ptr<HTTPDataGatherer> getHTTPDataGatherer() const { return m_httpDataGatherer; }

        virtual void configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                   const _zend_agent_globals* agentGlobals);

    private:
        static const std::string name;
        AgentLogger logger;

        bool m_enabled;
        bool m_callgraphEnabled;
        int m_minExitCallTime;
        int m_minSQLExecTime;
        int m_minCallGraphNodeDuration;
        bool m_skipInternals;
        bool m_captureRawSQL;
        uint32_t m_maxUniqSQLCalls;
        boost::shared_ptr<HTTPDataGatherer> m_httpDataGatherer;

        Snapshot* snapshot;
};

/* }}} */

/* {{{ class Snapshot */

class Snapshot
{
    friend class SnapshotManager;
    public:
        Snapshot(SnapshotManager* snapshotManager)
            : m_logger(::getLogger(std::string(LogContext::SNAPSHOT) + ".Snapshot"))
            , m_snapshotManager(snapshotManager)
            , m_dbCalls(snapshotManager)
            , m_httpData(HTTPData::create())
        {
        }

        ~Snapshot()
        {
        }

        inline CallGraph::Graph& getCallGraph() { return m_callGraph; }

        inline const CallGraph::Graph& getCallGraph() const { return m_callGraph; }

        inline const AgentLogger& getLogger() const { return m_logger; }

        inline bool isCallGraphEnabled() const { return m_snapshotManager->isCallGraphEnabled(); }

        inline const std::string& getURL() const { return m_url; }

        inline bool captureSQLPositionalBoundParameters() const { return m_snapshotManager->captureRawSQL(); }

        inline SnapshotDBCalls& getDBCalls() { return m_dbCalls; }

        inline const SnapshotDBCalls& getDBCalls() const { return m_dbCalls; }

        inline const boost::shared_ptr<HTTPData> getHttpData() const { return m_httpData; }

        void mergeHttpDataIntoSnapshot(appdynamics::pb::Snapshot* pbSnapshot);

    private:
        inline void setURL(const std::string& url) { m_url = url; }

        CallGraph::Graph m_callGraph;
        AgentLogger m_logger;
        SnapshotManager* m_snapshotManager;
        std::string m_url;
        SnapshotDBCalls m_dbCalls;
        const boost::shared_ptr<HTTPData> m_httpData;
};

/* }}} */

#endif /* __SNAPSHOT_H */

// vim: set fdm=marker:
