/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __delayed_functions_table_h
#define __delayed_functions_table_h

// Table of functions that we want to call, but can't link directly against
// on CentOS/RHEL linux distros.
//

#include <php.h>
#include <Zend/zend_compile.h>
#include <ext/pdo/php_pdo_driver.h>

extern "C" {
#include <ext/pcre/php_pcre.h>

#include <curl/curl.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#if LIBXML_VERSION >= 20902
typedef const xmlDoc* xmlDocPtr_maybeConst;
#else
typedef xmlDocPtr xmlDocPtr_maybeConst;
#endif

typedef struct st_pgconn PGconn;
typedef struct st_pgresult PGresult;
typedef struct {
    PGconn *conn;
    PGresult *result;
} pgsql_result_handle;
int PQserverVersion(const PGconn*);
char* PQdb(const PGconn*);
char* PQhost(const PGconn*);
char* PQport(const PGconn*);
char* PQerrorMessage(const PGconn*);
char* PQresultErrorMessage(const PGresult*);

#define PGSQL_CONNECT_FORCE_NEW     (1<<1)
#define PGSQL_CONNECT_ASYNC         (1<<2)
#define PGSQL_DML_EXEC              (1<<9)     /* Execute query */
#define PGSQL_DML_ASYNC             (1<<10)    /* Do async query */

// Declare our own relevant structs and functions to avoid conflicts between
// libmysql and mysqlnd headers
typedef struct st_mysql MYSQL;
unsigned int mysql_errno(MYSQL*);
const char* mysql_error(MYSQL*);
const char* mysql_get_server_info(MYSQL*);
}

#if PHP_VERSION_ID >= 70000
#define DELAYED_FUNCTIONS_TABLE(XX, YY)                 \
    XX(php_pdo_parse_data_source,                       \
       int,                                             \
       (const char *data_source,                        \
        zend_ulong data_source_len,                     \
        struct pdo_data_src_parser *parsed,             \
        int nparams))                                   \
                                                        \
    XX(pdo_parse_params,                                \
       int,                                             \
       (pdo_stmt_t* stmt,                               \
        char* inquery,                                  \
        size_t inquery_len,                             \
        char** outquery,                                \
        size_t* outquery_len))                          \
                                                        \
    XX(curl_multi_info_read,                            \
       CURLMsg*,                                        \
       (CURLM *multi_handle,                            \
        int *msgs_in_queue))                            \
                                                        \
    XX(pcre_get_compiled_regex,                         \
       pcre*,                                           \
       (zend_string *regex,                             \
        pcre_extra **extra,                             \
        int *preg_options))                             \
                                                        \
    YY(pcre_exec,                                       \
       "php_pcre_exec",                                 \
       "pcre_exec",                                     \
       int,                                             \
       (const pcre *,                                   \
        const pcre_extra *,                             \
        const char *,                                   \
        int,                                            \
        int,                                            \
        int,                                            \
        int *,                                          \
        int))                                           \
                                                        \
    YY(pcre_fullinfo,                                   \
       "php_pcre_fullinfo",                             \
       "pcre_fullinfo",                                 \
       int,                                             \
       (const pcre *,                                   \
        const pcre_extra *,                             \
        int,                                            \
        void *))                                        \
                                                        \
    YY(pcre_get_substring_list,                         \
       "php_pcre_get_substring_list",                   \
       "pcre_get_substring_list",                       \
       int,                                             \
       (const char *,                                   \
        int *,                                          \
        int,                                            \
        const char ***))                                \
                                                        \
     XX(mysql_errno,                                    \
       unsigned int,                                    \
       (MYSQL *))                                       \
                                                        \
    XX(mysql_error,                                     \
       const char*,                                     \
       (MYSQL *))                                       \
                                                        \
    XX(mysql_get_server_info,                           \
       const char*,                                     \
       (MYSQL *))                                       \
                                                        \
    XX(PQserverVersion,                                 \
       int,                                             \
       (const PGconn *conn))                            \
                                                        \
    XX(PQdb,                                            \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQhost,                                          \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQport,                                          \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQerrorMessage,                                  \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQresultErrorMessage,                            \
       char *,                                          \
       (const PGresult *result))                        \
                                                        \
    XX(xmlReadMemory,                                   \
       xmlDocPtr,                                       \
       (const char *,                                   \
        int,                                            \
        const char *,                                   \
        const char *,                                   \
        int))                                           \
                                                        \
    XX(xmlDocGetRootElement,                            \
       xmlNodePtr,                                      \
       (xmlDocPtr_maybeConst))                          \
                                                        \
    XX(xmlNewChild,                                     \
       xmlNodePtr,                                      \
       (xmlNodePtr,                                     \
        xmlNsPtr,                                       \
        const xmlChar*,                                 \
        const xmlChar*))                                \
                                                        \
    XX(xmlEncodeSpecialChars,                           \
       xmlChar*,                                        \
       (xmlDocPtr_maybeConst,                           \
        const xmlChar*))                                \
                                                        \
    XX(xmlNewNs,                                        \
       xmlNsPtr,                                        \
       (xmlNodePtr,                                     \
        const xmlChar*,                                 \
        const xmlChar*))                                \
                                                        \
    XX(xmlDocDumpMemory,                                \
       void,                                            \
       (xmlDocPtr,                                      \
        xmlChar**,                                      \
        int *))                                         \
                                                        \
    XX(xmlFreeDoc,                                      \
       void,                                            \
       (xmlDocPtr))                                     \
                                                        \
    XX(xmlStrcasecmp,                                   \
       int,                                             \
       (const xmlChar*,                                 \
        const xmlChar*))                                \
                                                        \
    XX(xmlAddPrevSibling,                               \
       xmlNodePtr,                                      \
       (xmlNodePtr,                                     \
        xmlNodePtr))

#else

    #define DELAYED_FUNCTIONS_TABLE(XX, YY)             \
    XX(php_pdo_parse_data_source,                       \
       int,                                             \
       (const char *data_source,                        \
        unsigned long data_source_len,                  \
        struct pdo_data_src_parser *parsed,             \
        int nparams))                                   \
                                                        \
    XX(pdo_parse_params,                                \
       int,                                             \
       (pdo_stmt_t* stmt,                               \
        char* inquery,                                  \
        int inquery_len,                                \
        char** outquery,                                \
        int* outquery_len TSRMLS_DC))                   \
                                                        \
    XX(curl_multi_info_read,                            \
       CURLMsg*,                                        \
       (CURLM *multi_handle,                            \
        int *msgs_in_queue))                            \
                                                        \
    XX(pcre_get_compiled_regex,                         \
       pcre*,                                           \
       (char *regex,                                    \
        pcre_extra **extra,                             \
        int *preg_options TSRMLS_DC))                   \
                                                        \
    YY(pcre_exec,                                       \
       "php_pcre_exec",                                 \
       "pcre_exec",                                     \
       int,                                             \
       (const pcre *,                                   \
        const pcre_extra *,                             \
        const char *,                                   \
        int,                                            \
        int,                                            \
        int,                                            \
        int *,                                          \
        int))                                           \
                                                        \
    YY(pcre_fullinfo,                                   \
       "php_pcre_fullinfo",                             \
       "pcre_fullinfo",                                 \
       int,                                             \
       (const pcre *,                                   \
        const pcre_extra *,                             \
        int,                                            \
        void *))                                        \
                                                        \
    YY(pcre_get_substring_list,                         \
       "php_pcre_get_substring_list",                   \
       "pcre_get_substring_list",                       \
       int,                                             \
       (const char *,                                   \
        int *,                                          \
        int,                                            \
        const char ***))                                \
                                                        \
    XX(mysql_errno,                                     \
       unsigned int,                                    \
       (MYSQL *))                                       \
                                                        \
    XX(mysql_error,                                     \
       const char*,                                     \
       (MYSQL *))                                       \
                                                        \
    XX(mysql_get_server_info,                           \
       const char*,                                     \
       (MYSQL *))                                       \
                                                        \
    XX(PQserverVersion,                                 \
       int,                                             \
       (const PGconn *conn))                            \
                                                        \
    XX(PQdb,                                            \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQhost,                                          \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQport,                                          \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQerrorMessage,                                  \
       char *,                                          \
       (const PGconn *conn))                            \
                                                        \
    XX(PQresultErrorMessage,                            \
       char *,                                          \
       (const PGresult *result))                        \
                                                        \
    XX(xmlReadMemory,                                   \
       xmlDocPtr,                                       \
       (const char *,                                   \
        int,                                            \
        const char *,                                   \
        const char *,                                   \
        int))                                           \
                                                        \
    XX(xmlDocGetRootElement,                            \
       xmlNodePtr,                                      \
       (xmlDocPtr_maybeConst))                          \
                                                        \
    XX(xmlNewChild,                                     \
       xmlNodePtr,                                      \
       (xmlNodePtr,                                     \
        xmlNsPtr,                                       \
        const xmlChar*,                                 \
        const xmlChar*))                                \
                                                        \
    XX(xmlEncodeSpecialChars,                           \
       xmlChar*,                                        \
       (xmlDocPtr_maybeConst,                           \
        const xmlChar*))                                \
                                                        \
    XX(xmlNewNs,                                        \
       xmlNsPtr,                                        \
       (xmlNodePtr,                                     \
        const xmlChar*,                                 \
        const xmlChar*))                                \
                                                        \
    XX(xmlDocDumpMemory,                                \
       void,                                            \
       (xmlDocPtr,                                      \
        xmlChar**,                                      \
        int *))                                         \
                                                        \
    XX(xmlFreeDoc,                                      \
       void,                                            \
       (xmlDocPtr))                                     \
                                                        \
    XX(xmlStrcasecmp,                                   \
       int,                                             \
       (const xmlChar*,                                 \
        const xmlChar*))                                \
                                                        \
    XX(xmlAddPrevSibling,                               \
       xmlNodePtr,                                      \
       (xmlNodePtr,                                     \
        xmlNodePtr))
#endif


#endif
