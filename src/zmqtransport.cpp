/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "agent.h"
#include "zmqtransport.h"

#include <zmq.hpp>
#include "config_channel.h"
#include "timer.h"
#include "PHPAgentProtobufs.pb.h"
#include <bitset>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

static const int64_t MAX_QUEUE_CAPACITY = 10;
static const int NO_LINGER = 0;
static const int CONFIG_POLLBIND_TIMEOUT_MS = 2;
static const char ASYNC_REQ_PEER[] = "AsyncReqRouter";
static const size_t ASYNC_REQ_PEER_LEN = sizeof(ASYNC_REQ_PEER) - 1;
static const char SYNC_REQ_PEER[] = "SyncReqRouter";
static const size_t SYNC_REQ_PEER_LEN = sizeof(SYNC_REQ_PEER) - 1;
static const char CONTROL_REQ_PEER[] = "ControlReq";
static const size_t CONTROL_REQ_PEER_LEN = sizeof(CONTROL_REQ_PEER) - 1;

static const char CONFIG_TRANSPORT_TYPE[] = "config";
static const char BTINFO_TRANSPORT_TYPE[] = "btInfo";
static const char REPORT_TRANSPORT_TYPE[] = "reporting";
static const char CONTROL_TRANSPORT_TYPE[] = "control";

// Starting number for the ZMQ socket naming
int ZMQTransportBase::m_baseSocketNumber = 0;

//  Sleep for a number of milliseconds
static inline void s_sleep(int msecs)
{
    struct timespec t;
    t.tv_sec = msecs / 1000;
    t.tv_nsec = (msecs % 1000) * 1000000;
    nanosleep(&t, NULL);
}

template <size_t nTotalParts>
static inline bool readMessageParts(const AgentLogger& logger,
                                    zmq::socket_t* socket,
                                    std::bitset<nTotalParts> partsToPutInResult,
                                    zmq::message_t** result)
{
    int64_t more;           //  Multipart detection
    unsigned resultIndex = 0;
    for (unsigned i = 0; i < nTotalParts; ++i) {
        zmq::message_t part;
        if (socket->recv(&part, ZMQ_NOBLOCK) == false) {
            LOG4CXX_ERROR(logger, "failed to receive message part still expecting " <<
                                  (nTotalParts - i) << " message parts!");
            return false;
        }
        if (partsToPutInResult[i]) {
            result[resultIndex]->move(&part);
            ++resultIndex;
        }
        more = 0;
        size_t more_size = sizeof(more);
        socket->getsockopt(ZMQ_RCVMORE, &more, &more_size);
        if ((!more) && ((i + 1) < nTotalParts)) {
            LOG4CXX_ERROR(logger, "still expecting " << (nTotalParts - (i + 1)) << " message parts!");
            return false;
        }
    }

    if (more)
    {
        unsigned extraCount = 0;
        do {
            zmq::message_t part;
            if (socket->recv(&part, ZMQ_NOBLOCK) == false) {
                LOG4CXX_ERROR(logger, "failed to drain message part")
                return false;
            }
            more = 0;
            size_t more_size = sizeof(more);
            socket->getsockopt(ZMQ_RCVMORE, &more, &more_size);
            ++extraCount;
        } while (more);
        LOG4CXX_ERROR(logger, "received " << extraCount << " extra message parts!");
    }
    return true;
}

void ZMQTransportBase::close()
{
    if (m_socket)
    {
        LOG4CXX_TRACE(getLogger(), "closing and destroying transport socket");
        m_socket->close();
        delete m_socket;
        m_socket = NULL;
    }
    m_connected = false;
}

ZMQTransportBase::ZMQTransportBase(zmq::context_t& zmqContext,
                                   uint16_t zmqSocketNumber,
                                   short zmqSocketType,
                                   short zmqEventMask,
                                   int linger,
                                   int64_t hwm,
                                   const std::string& transportType,
                                   const std::string& loggerName)
    : m_connected(false)
    , m_socket(NULL)
    , m_socketNumber(zmqSocketNumber)
    , m_zmqContext(zmqContext)
    , m_zmqSocketType(zmqSocketType)
    , m_eventMask(zmqEventMask)
    , m_linger(linger)
    , m_hwm(hwm)
    , m_transportType(transportType)
    , m_logger(::getLogger(loggerName))
{
}

ZMQTransportBase::~ZMQTransportBase()
{
    close();
}

void ZMQTransportBase::connectNow()
{
    if (m_connected)
        return;

    const Timer* timer = AgentKernel::getTimer();

    try
    {
        const std::string& connectString = getConnectString();
        if (connectString.empty())
            return;

        if (!m_socket) {
            /*
             * Turn off lingering behavior; close socket even if there are queued
             * messages.
             */
            LOG4CXX_TRACE(getLogger(), "creating transport socket (" << m_linger << ", " << m_hwm << ")");
            m_socket = new zmq::socket_t(getZMQContext(), m_zmqSocketType);
            m_socket->setsockopt(ZMQ_LINGER, &m_linger, sizeof(m_linger));
            m_socket->setsockopt(ZMQ_HWM, const_cast<int64_t*>(&m_hwm), sizeof(m_hwm));
        }

        LOG4CXX_DEBUG(getLogger(), "connecting transport socket to: " << connectString);
        m_socket->connect(connectString.c_str());
        // Even if the zmq_poll times out, we're still connected and must not
        // call socket_t::connect() twice.
        m_connected = true;
    }
    catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "exception when connecting the socket: " << e.what());
    }
}

bool ZMQTransportBase::sendMessage(zmq::socket_t* socket, const google::protobuf::Message& message, int flags)
{
    size_t messageByteSize = message.ByteSize();
    zmq::message_t zmqMessage(messageByteSize);
    message.SerializeWithCachedSizesToArray(reinterpret_cast<uint8_t*>(zmqMessage.data()));
    return socket->send(zmqMessage, flags);
}

static void zmqNOPFreeFunction(void*, void*)
{
}

bool ZMQTransportBase::sendConstantBytes(zmq::socket_t* socket, const char* b, size_t len, int flags)
{
    zmq::message_t message(const_cast<char*>(b), len, zmqNOPFreeFunction);
    return socket->send(message, flags);
}

bool ZMQTransportBase::sendZeroBytes(zmq::socket_t* socket, int flags)
{
    return sendConstantBytes(socket, "", 0, flags);
}


bool ZMQTransportBase::sendInt64(zmq::socket_t* socket, int64_t i, int flags)
{
    zmq::message_t zmqMessage(sizeof(int64_t));
    *(reinterpret_cast<int64_t*>(zmqMessage.data())) = i;
    return socket->send(zmqMessage, flags);
}

bool ZMQTransportBase::getMultiPartResponse(zmq::message_t* response)
{
    bool received = false;
    BOOST_ASSERT(m_socket != NULL);
    try
    {
        while (true)
        {
            zmq::message_t msg;
            int64_t more = 0;           //  Multipart detection
            size_t more_size = sizeof(more);

            if (!(m_socket->recv(&msg, ZMQ_NOBLOCK)))
            {
                LOG4CXX_TRACE(getLogger(), "no messages waiting");
                return false;
            }
            m_socket->getsockopt(ZMQ_RCVMORE, &more, &more_size);

            if (!more)
            {
                // Last message part
                response->move(&msg);
                received = true;
                break;
            }
        }

    } catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "response exception " << e.what());
    }

    if (received)
        LOG4CXX_TRACE(getLogger(), "received response: " << response->size() << " bytes");

    return received;
}

bool ZMQTransportBase::doZMQPoll(long timeoutInMilliseconds,
                                 short events,
                                 const std::string& timeoutMessage)
{
    BOOST_ASSERT(m_socket);
    zmq_pollitem_t items [] = {
        { *m_socket, 0, events, 0 }
    };

    uint64_t startTime = AgentKernel::getTimer()->getTimestamp();
    int nItems = zmq_poll (&items[0], 1, timeoutInMilliseconds * 1000);
    uint64_t endTime = AgentKernel::getTimer()->getTimestamp();

    if (nItems == -1) {
        LOG4CXX_WARN(m_logger, "error condition when polling on socket: " << errno);
        return false;
    }
    if (nItems == 0) {
        LOG4CXX_DEBUG(m_logger, "timed out ("
                                << timeoutInMilliseconds * 1000
                                << "us) waiting for "
                                << timeoutMessage
                                << ". Actual wait time: "
                                << (endTime - startTime)
                                << "us");
        return false;
    }
    return nItems != 0;
}

template <size_t keepIndex>
bool ZMQTransportBase::getMultiPartSyncResponse(zmq::message_t* response,
                                                long timeoutInMilliseconds)
{
    bool received = false;

    BOOST_ASSERT(m_socket != NULL);
    BOOST_ASSERT(timeoutInMilliseconds >= -1);

    try
    {
        bool pollSuccess = doZMQPoll(timeoutInMilliseconds, ZMQ_POLLIN, m_transportType);
        if (!pollSuccess)
            return false;
        zmq::message_t* messageParts[1];
        messageParts[0] = response;
        std::bitset<keepIndex+1> responsePartsToKeep;
        responsePartsToKeep[keepIndex] = true;

        bool gotMultiPartMessage =
            readMessageParts(getLogger(), m_socket, responsePartsToKeep, messageParts);

        // If we failed to read the message parts we are looking for
        // bail out of here.  We'll only go around this loop again
        // if we successfully read a multi-part message we want to drop.
        if (!gotMultiPartMessage)
            return false;

        received = true;
    }
    catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "response exception " << e.what());
    }

    if (received)
        LOG4CXX_TRACE(getLogger(), "received response: " << response->size() << " bytes");

    return received;
}

void ZMQTransportBase::resetSocket()
{
    close();
    BOOST_ASSERT(!m_connected);
    connectNow();

    if (!m_connected) {
        LOG4CXX_ERROR(getLogger(), "could not connect transport");
        return;
    }

    LOG4CXX_TRACE(getLogger(), "initialized transport");
}

ZMQAgentTransportBase::ZMQAgentTransportBase(zmq::context_t& zmqContext,
                                             const boost::shared_ptr<ZMQControlTransport>& controlTransport,
                                             uint16_t zmqSocketNo,
                                             short zmqSocketType,
                                             short zmqEventMask,
                                             int linger,
                                             int64_t hwm,
                                             const std::string& transportType,
                                             const std::string& loggerName)
    : ZMQTransportBase(zmqContext,
                       zmqSocketNo,
                       zmqSocketType,
                       zmqEventMask,
                       linger,
                       hwm,
                       transportType,
                       loggerName)
    , m_controlTransport(controlTransport)
    , m_didComputeConnectString(false)
{

}

void ZMQAgentTransportBase::reset()
{
    m_didComputeConnectString = false;
    m_connectString.clear();
}

const std::string& ZMQAgentTransportBase::getConnectString()
{
    if (m_didComputeConnectString) {
        return m_connectString;
    }
    m_connectString.clear();
    boost::filesystem::path dataDir;
    if (!m_controlTransport->getDataDirectory(1000, &dataDir))
        return m_connectString;

    std::string ipc("ipc://");

    std::string socket("/");
    socket += boost::lexical_cast<std::string>(m_socketNumber);
    m_didComputeConnectString = true;
    m_connectString = ipc + dataDir.native() + socket;
    return m_connectString;
}


/* {{{ ZMQConfigTransport methods */
ZMQConfigTransport::ZMQConfigTransport(zmq::context_t& zmqContext,
                                       boost::shared_ptr<ZMQControlTransport> controlTransport,
                                       uint16_t zmqSocketNumber)
    : ZMQAgentTransportBase(zmqContext,
                            controlTransport,
                            zmqSocketNumber,
                            ZMQ_ROUTER,
                            ZMQ_POLLBIND,
                            NO_LINGER,
                            1,
                            CONFIG_TRANSPORT_TYPE,
                            std::string(LogContext::CONFIG) + ".ZMQConfigTransport")
    , m_request()
    , m_requestTimestamp(0)
    , m_bound(false)
{
}

ZMQConfigTransport::~ZMQConfigTransport()
{
}

void ZMQConfigTransport::reset()
{
    ZMQAgentTransportBase::reset();
    LOG4CXX_TRACE(getLogger(), "resetting config transport");
    close();

    m_request.reset();
    m_requestTimestamp = 0;
    m_bound = false;
}

void ZMQConfigTransport::connectNow()
{
    ZMQTransportBase::connectNow();

    if (m_connected && !m_bound) {
        uint64_t currentTime = AgentKernel::getTimer()->getTimestamp();
        bool success = doZMQPoll(CONFIG_POLLBIND_TIMEOUT_MS, m_eventMask, "bind");
        if (!success)
            return;
        m_bound = true;
        LOG4CXX_TRACE(getLogger(), "socket at "
                      << m_connectString
                      << " bound after "
                      << (AgentKernel::getTimer()->getTimestamp() - currentTime)
                      << "us");
    }
}

bool ZMQConfigTransport::sendRequest(const appdynamics::pb::ASyncRequest& request)
{
    if (isRequestSent()) {
        LOG4CXX_DEBUG(getLogger(), "sending config request while config request outstanding, resetting the transport");
        // When the config transport times out, then reset it, which will
        // cause it to re-connect and waiting until the other side is bound
        // again.
        reset();
    }

    connectNow();

    // Even if we did not connect for some reason, we
    // update the last request timestamp so that
    // we wait the config request interval before trying
    // to connect again.
    m_requestTimestamp = time(NULL);

    if (!haveConnectedSocket() || !m_bound)
        return false;

    try
    {
        LOG4CXX_TRACE(getLogger(), "sending ConfigRequest message");

        LOG4CXX_DEBUG(getLogger(), "config request: " << request.DebugString());
        bool rc = sendConstantBytes(m_socket, ASYNC_REQ_PEER, ASYNC_REQ_PEER_LEN, ZMQ_SNDMORE);
        rc = rc && sendZeroBytes(m_socket, ZMQ_SNDMORE);
        rc = rc && sendMessage(m_socket, request, 0);

        if (!rc)
        {
            LOG4CXX_ERROR(getLogger(), "could not send ConfigRequest message");
            return false;
        }
    }
    catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "ConfigRequest exception: " << e.what());
        return false;
    }

    LOG4CXX_TRACE(getLogger(), "sent ConfigRequest message");

    m_request.reset(new appdynamics::pb::ASyncRequest(request));

    return true;
}

bool ZMQConfigTransport::getResponse(appdynamics::pb::ConfigResponse* response, long timeoutInMilliseconds)
{
    if (!haveConnectedSocket() || !m_bound)
        return false;
    if (!isRequestSent())
        return false;

    zmq::message_t responseMessage;
    bool receivedCorrectResponse = false;
    appdynamics::pb::ConfigResponse tempResponse;
    uint64_t currentTime = AgentKernel::getTimer()->getTimestamp();
    uint64_t endTime = currentTime + timeoutInMilliseconds * 1000;

    do {
        bool receivedResponse;
        if (timeoutInMilliseconds == 0)
            receivedResponse = getMultiPartResponse(&responseMessage);
        else
            receivedResponse = getMultiPartSyncResponse<2>(&responseMessage, timeoutInMilliseconds);
        if (!receivedResponse)
            return false;

        bool didParse =
            tempResponse.ParseFromArray(responseMessage.data(), responseMessage.size());
        if (!didParse)
        {
            LOG4CXX_ERROR(getLogger(), "Failed to parse config response!");
            return false;
        } else if (tempResponse.requestid() != m_request->configreq().requestid()) {
            LOG4CXX_DEBUG(getLogger(), "Dropping response ID "
                                         << tempResponse.requestid()
                                         << " while waiting for response ID "
                                         << m_request->configreq().requestid());
            currentTime = AgentKernel::getTimer()->getTimestamp();
        } else {
            receivedCorrectResponse = true;
        }
    } while (!receivedCorrectResponse && (currentTime < endTime));

    if (receivedCorrectResponse) {
        LOG4CXX_INFO(getLogger(), "received config response for request ID " << m_request->configreq().requestid());
        response->CopyFrom(tempResponse);
        m_request.reset();
    }

    return receivedCorrectResponse;
}

/* }}} */

/* {{{ ZMQReportingTransport methods */
ZMQReportingTransport::ZMQReportingTransport(zmq::context_t& zmqContext,
                                             boost::shared_ptr<ZMQControlTransport> controlTransport,
                                             uint16_t zmqSocketNumber,
                                             int linger)
    : ZMQAgentTransportBase(zmqContext,
                            controlTransport,
                            zmqSocketNumber,
                            ZMQ_PUB,
                            ZMQ_POLLOUT, // If we drop some messages immediately after connecting it is no big deal.
                            linger,
                            MAX_QUEUE_CAPACITY,
                            REPORT_TRANSPORT_TYPE,
                            std::string(LogContext::REPORT) + ".ZMQReportingTransport")
{
}

ZMQReportingTransport::~ZMQReportingTransport()
{
}

void ZMQReportingTransport::reset()
{
    ZMQAgentTransportBase::reset();
    close();
}

bool ZMQReportingTransport::sendMessage(const appdynamics::pb::ASyncMessage& message)
{
    connectNow();
    if (!haveConnectedSocket()) {
        return false;
    }

    const std::string& messageName = ASyncMessage_Type_Name(message.type());
    try
    {
        LOG4CXX_TRACE(getLogger(), "sending " << messageName << " message" << message.DebugString());
        if (!ZMQTransportBase::sendMessage(m_socket, message))
        {
            LOG4CXX_ERROR(getLogger(), "could not send" << messageName << " message");
            return false;
        }
    }
    catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), messageName << " message exception: " << e.what());
        return false;
    }
    return true;

}

/* }}} */

/* {{{ ZMQBTInfoTransport methods */

ZMQBTInfoTransport::ZMQBTInfoTransport(zmq::context_t& zmqContext,
                                       boost::shared_ptr<ZMQControlTransport> controlTransport,
                                       uint16_t zmqSocketNumber,
                                       boost::shared_ptr<IConfigChannel> configChannel)
    : ZMQAgentTransportBase(zmqContext,
                            controlTransport,
                            zmqSocketNumber,
                            ZMQ_ROUTER,
                            ZMQ_POLLBIND,
                            NO_LINGER,
                            MAX_QUEUE_CAPACITY,
                            BTINFO_TRANSPORT_TYPE,
                            std::string(LogContext::AGENT) + ".ZMQBTInfoTransport")
    , m_configChannel(configChannel)
{
    m_configChannel->addListener(this);
}

ZMQBTInfoTransport::~ZMQBTInfoTransport()
{
}

void ZMQBTInfoTransport::configChanged(const appdynamics::pb::ConfigResponse&,
                                       const struct _zend_agent_globals* agentGlobals)
{
    connectNow();
}

bool ZMQBTInfoTransport::sendRequest(const appdynamics::pb::BTInfoRequest& request)
{
    connectNow();
    if (!haveConnectedSocket())
        return false;

    appdynamics::pb::ASyncRequest asyncRequest;
    asyncRequest.set_type(appdynamics::pb::ASyncRequest::BTINFO);
    asyncRequest.mutable_btinforeq()->CopyFrom(request);

    try
    {
        LOG4CXX_TRACE(getLogger(), "Sending BTInfoRequest: " << asyncRequest.DebugString());
        bool rc = sendConstantBytes(m_socket, ASYNC_REQ_PEER, ASYNC_REQ_PEER_LEN, ZMQ_SNDMORE);
        rc = rc && sendZeroBytes(m_socket, ZMQ_SNDMORE);
        rc = rc && sendMessage(m_socket, asyncRequest);
        if (!rc)
        {
            LOG4CXX_ERROR(getLogger(), "could not send request");
            return false;
        }
    } catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "request exception: " << e.what());
        return false;
    }

    return true;
}

bool ZMQBTInfoTransport::getResponse(const appdynamics::pb::BTInfoRequest* request,
                                     appdynamics::pb::BTInfoResponse* response,
                                     long timeoutInMilliseconds)
{
    if (!haveConnectedSocket())
        return false;

    bool received = false;
    const Timer* timer = AgentKernel::getTimer();
    uint64_t currentTime = timer->getTimestamp();
    uint64_t endTime = currentTime + timeoutInMilliseconds * 1000;
    uint64_t firstByteTime = 0;
    zmq::message_t responseMessage;
    appdynamics::pb::BTInfoResponse tempResponse;

    BOOST_ASSERT(response);

    try
    {
        do {
            zmq_pollitem_t items [] = {
                { *m_socket, 0, ZMQ_POLLIN, 0 }
            };
            long const currentTimeoutInMicroseconds = (endTime - currentTime);

            int nItems = zmq_poll (&items[0], 1, currentTimeoutInMicroseconds);
            if (nItems == -1) {
                LOG4CXX_DEBUG(getLogger(), "error condition when polling on socket: " << errno);
                return false;
            }

            if (nItems == 0) {
                LOG4CXX_WARN(getLogger(), "timed out ("
                                           << currentTimeoutInMicroseconds
                                           << "us) waiting for response to "
                                           << request->requestid()
                                           << ".  Actual wait time: "
                                           << (timer->getTimestamp() - currentTime)
                                           << "us");
                return false;
            }

            zmq::message_t* messageParts[1];
            messageParts[0] = &responseMessage;
            std::bitset<3> responsePartsToKeep;
            responsePartsToKeep[2] = true;

            if (firstByteTime == 0 && getLogger()->isTraceEnabled()) {
                firstByteTime = timer->getTimestamp();
                LOG4CXX_TRACE(getLogger(), "response is ready, time to first byte: "
                                            << ((firstByteTime - currentTime) / 1000)
                                            << "ms");
            }

            bool gotMultiPartMessage =
                readMessageParts(getLogger(), m_socket, responsePartsToKeep, messageParts);

            // If we failed to read the message parts we are looking for
            // bail out of here.  We'll only go around this loop again
            // if we successfully read a multi-part message we want to drop.
            if (!gotMultiPartMessage)
                return false;

            bool didParse = tempResponse.ParseFromArray(responseMessage.data(), responseMessage.size());
            if (!didParse)
            {
                LOG4CXX_ERROR(getLogger(), "Failed to parse BTInfoResponse!");
            }
            else if (tempResponse.requestid() != request->requestid()
                     ||
                     tempResponse.messageid() != request->messageid()) {
                LOG4CXX_DEBUG(getLogger(), "Dropping response ID "
                             << tempResponse.requestid()
                             << ", message ID "
                             << tempResponse.messageid()
                             << " while waiting for response ID "
                             << request->requestid()
                             << ", message ID "
                             << request->messageid());
                currentTime = timer->getTimestamp();
            }
            else
                received = true;

        } while ((!received) && (currentTime < endTime));

    }
    catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "response exception" << e.what());
    }

    if (received) {
        LOG4CXX_TRACE(getLogger(), "received response: " << responseMessage.size() << " bytes");
        LOG4CXX_TRACE(getLogger(), "BTInfoResponse: " << tempResponse.DebugString());
        response->CopyFrom(tempResponse);
    }

    return received;
}

void ZMQBTInfoTransport::reset()
{
    ZMQAgentTransportBase::reset();
    close();
}

/* }}} */

ZMQControlTransport::ZMQControlTransport(zmq::context_t& zmqContext,
                                         const char* zmqSocketDir,
                                         uint16_t zmqSocketNumber,
                                         const std::string& logsDir)
    : ZMQTransportBase(zmqContext,
                       zmqSocketNumber,
                       ZMQ_REQ,
                       ZMQ_POLLOUT,
                       NO_LINGER,
                       1,
                       CONTROL_TRANSPORT_TYPE,
                       std::string(LogContext::AGENT) + ".ZMQControlTransport")
    , m_connectString(computeConnectString(zmqSocketDir, zmqSocketNumber))
    , m_requestSent(false)
    , m_didGetDataDir(false)
    , m_startNodeSocketDir(zmqSocketDir)
    , m_logsDir(logsDir)
{
}

void ZMQControlTransport::fillStartNodeRequest(appdynamics::pb::StartNodeRequest* request,
                                               const AgentIdentity& agentIdentity)
{
    request->set_appname(agentIdentity.getApplication());
    request->set_tiername(agentIdentity.getTier());
    request->set_nodename(agentIdentity.getNode());
    request->set_controllerhost(agentIdentity.getControllerHost());
    request->set_controllerport(AG(controller_port));
    request->set_sslenabled(AG(controller_ssl_enabled));
    request->set_accountname(agentIdentity.getAccountName());
    // Optional fields:
    if (!std::string(AG(account_access_key)).empty())
        request->set_accountaccesskey(AG(account_access_key));
    if (!std::string(AG(proxy_host)).empty())
        request->set_httpproxyhost(AG(proxy_host));
    if (AG(proxy_port) >= 0)
        request->set_httpproxyport(AG(proxy_port));
    if (!std::string(AG(proxy_user)).empty())
        request->set_httpproxyuser(AG(proxy_user));
    if (!std::string(AG(proxy_password_file)).empty())
        request->set_httpproxypasswordfile(AG(proxy_password_file));
    if (!std::string(m_logsDir).empty())
        request->set_logsdir(m_logsDir);

}

bool ZMQControlTransport::sendRequest()
{
    BOOST_ASSERT(AG(kernel)->getAgentContext());
    appdynamics::pb::StartNodeRequest request;
    fillStartNodeRequest(&request, AG(kernel)->getAgentContext()->getAgentIdentity());

    if (!m_connected || m_requestSent) {
        reset();
        connectNow();
    }

    if (!haveConnectedSocket())
        return false;

    try
    {
        LOG4CXX_TRACE(getLogger(), "sending StartNodeRequest message");
        LOG4CXX_DEBUG(getLogger(), "start node request: " << request.DebugString());

        bool rc = sendZeroBytes(m_socket, ZMQ_SNDMORE);
        rc &= sendMessage(m_socket, request);

        if (!rc)
        {
            LOG4CXX_ERROR(getLogger(), "could not send StartNodeRequest message");
            return false;
        }
    }
    catch (zmq::error_t e)
    {
        LOG4CXX_ERROR(getLogger(), "StartNodeRequest exception: " << e.what());
        return false;
    }

    LOG4CXX_TRACE(getLogger(), "sent StartNodeRequest message");

    m_requestSent = true;
    m_requestTimestamp = time(NULL);

    return true;
}

bool ZMQControlTransport::getResponse(appdynamics::pb::StartNodeResponse* response, long timeoutInMilliseconds)
{
    bool pollSuccess = doZMQPoll(timeoutInMilliseconds, ZMQ_POLLIN, "control socket response");
    if (!pollSuccess)
        return false;

    // Drop the response after the two sends, not used.
    zmq::message_t drop;
    m_socket->recv(&drop);

    zmq::message_t responseMessage;
    if (!m_socket->recv(&responseMessage)) {
        LOG4CXX_ERROR(getLogger(), "Failed to receive start node response!");
        return false;
    }

    m_requestSent = false;


    LOG4CXX_INFO(getLogger(), "received start node response, size: " << responseMessage.size() << " bytes");

    bool didParse =
        response->ParseFromArray(responseMessage.data(), responseMessage.size());
    if (!didParse)
    {
        LOG4CXX_ERROR(getLogger(), "Failed to parse start node response!");
        return false;
    }

    return true;
}

bool ZMQControlTransport::getDataDirectory(long timeoutInMilliseconds,
                                           boost::filesystem::path* dataDirPath)
{
    if (m_didGetDataDir) {
        *dataDirPath = m_dataDirPath;
        return !m_dataDirPath.empty();
    }

    // Send startnode request.
    if (!sendRequest())
       return false;

    // Receive config.
    appdynamics::pb::StartNodeResponse response;
    if (!getResponse(&response, timeoutInMilliseconds))
        return false;

    // Take datasocketdirpath from protobuf.
    if (!response.has_datasocketdirpath())
        return false;

    m_didGetDataDir = true;

    std::string dataCommDir(response.datasocketdirpath());
    *dataDirPath = m_dataDirPath = boost::filesystem::path(dataCommDir);

    return !m_dataDirPath.empty();
}

ZMQControlTransport::~ZMQControlTransport()
{
}

std::string ZMQControlTransport::computeConnectString(const char* zmqSocketDir, uint16_t zmqSocketNumber)
{
    BOOST_ASSERT(zmqSocketDir != NULL);
    BOOST_ASSERT(zmqSocketDir[0] != '\0');
    return std::string("ipc://")
           + zmqSocketDir
           + '/'
           + boost::lexical_cast<std::string>(zmqSocketNumber);
}

const std::string& ZMQControlTransport::getConnectString()
{
    return m_connectString;
}

void ZMQControlTransport::reset()
{
    LOG4CXX_TRACE(getLogger(), "resetting control transport");
    close();

    m_requestSent = false;
    m_didGetDataDir = false;
    m_requestTimestamp = 0;
    m_dataDirPath.clear();

    auto i = m_dependentTransports.begin();
    while (i != m_dependentTransports.end()) {
        boost::shared_ptr<ZMQAgentTransportBase> transport(*i);
        if (!transport) {
            i = m_dependentTransports.erase(i);
            continue;
        }
        transport->reset();
        ++i;
    }
}
// vim: set fdm=marker
