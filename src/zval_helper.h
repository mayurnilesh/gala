/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */

#ifndef _ZVal_helper_h
#define _ZVal_helper_h

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/unordered_set.hpp>
#include <boost/utility.hpp>
#include <vector>

extern "C" {
#include <zend.h>
#include <Zend/zend_API.h>
#include <zend_list.h>
#include <php.h>
}

#include "zend_compatibility.h"
#include "zend_hashtable.h"
#include "intercept.h"
#include "common.h"
#include "safe_module_shutdown.h"

// Forward declarations
class ZValPointerAny;
class ZValPointerLong;
class ZValPointerArray;
class ZValPointerString;
inline void* getInternalObjectFromZval(const zval* object);
inline void* getInternalResourceFromZval(const zval* resource, int* resourceType);


namespace StringHelpers {
    /**
        Non-member function, returns a pretty string representation of zval instance.
        Behavior:
            IS_NULL     => "NULL"
            IS_LONG     => "<lval>"
            IS_DOUBLE   => "<dval>"
            IS_BOOL     => "True" or "False"
            IS_RESOURCE => "Resource"
            IS_STRING   => "\"<strval>\""
            IS_OBJECT   => "Object"
            IS_ARRAY    => "[<key1> => <value1>, <key2> => <value2>]"
        Array keys and values are displayed using the respective std::string
        formats specified above. Arrays can be nested.
    */
    std::string prettyPrint(const ZValPointerAny& value);
    std::string prettyPrint(const ZValPointerAny& value, boost::unordered_set<const HashTable*>& arraysTraversed);

    /**
        Generates a pretty string representation of this array for human-readable
        display in the controller (e.g. in data collectors). Example:
        [5, 4, "b", "3" => "b", Object].
    */
    std::string zvalArrayPrettyPrint(const ZValPointerArray&, boost::unordered_set<const HashTable*>& arraysTraversed);
}

/**
    Base class for a set of smart pointer classes
    that wrap zval* pointers to establish type safety
    and automatic increment/decrement of the zval ref count.

    These smart pointer classes are not aware of
    setjmp/longjmp ( which is what zend_bailout uses ), so
    the classes should generally only be used on the stack and should
    their lifetime should not span calls to function that could end up
    calling zend_bailout or longjmp without a corresponding zend_try or
    setjmp.

    This class should not be directly instantiated and has no
    public constructors or operator=.
 */
class ZValPointer
{
    friend class ZValPointerAny;
    friend std::string StringHelpers::zvalArrayPrettyPrint(const ZValPointerArray&, boost::unordered_set<const HashTable*>&);
public:
    /**
        Enum with values for all the different types of
        zval's.
     */
    enum ZValType
    {
#if PHP_VERSION_ID >= 70000
        ZVal_Undef = IS_UNDEF,
        ZVal_Null = IS_NULL,
        ZVal_False = IS_FALSE,
        ZVal_True = IS_TRUE,
        ZVal_Long = IS_LONG,
        ZVal_Double = IS_DOUBLE,
        ZVal_String = IS_STRING,
        ZVal_Array = IS_ARRAY,
        ZVal_Object = IS_OBJECT,
        ZVal_Resource = IS_RESOURCE,
        ZVal_Reference = IS_REFERENCE,
        /* fake type for compatibility reasons */
        ZVal_Bool = IS_BOOL
#else
        ZVal_Null = IS_NULL,
        ZVal_Long = IS_LONG,
        ZVal_Double = IS_DOUBLE,
        ZVal_Bool = IS_BOOL,
        ZVal_Array = IS_ARRAY,
        ZVal_Object = IS_OBJECT,
        ZVal_String = IS_STRING,
        ZVal_Resource = IS_RESOURCE
#endif
    };

    /**
        support for casting to a boolean.
        @return true if the wrapped zval* points to any value other
        than the IS_NULL zval, false if the wrapped zval* is IS_NULL.
     */
    operator bool() const;

    /**
        @return the type of the wrapped zval*.
     */
    ZValType getType() const;

    /**
        @param other ZValPointer to compare to.
        @return true if this and other wrap exactly the same zval.
     */
    bool operator==(const ZValPointer& other) const;

    /**
        @param other ZValPointer to compare to.
        @return opposite of operator==(other).
     */
    bool operator!=(const ZValPointer& other) const;

    /**
        WARNING!

        This can potentially run user code when an object is converted to
        a string. The implementation disables execution callback so that we
        do not instrument that code. However, it is still possible to get an
        exception or other bailout condition during execution.

        IF YOU ARE NOT WILLING TO ACCEPT THESE CONDITIONS, HANDLE THE OBJECT
        CASE SEPARATELY INSTEAD OF CALLING THIS METHOD.

        @return Return a string version of the wrapped zval. Conversion is
        performed according to PHP type conversion rules on a _copy_ of the
        contained zval.
     */
    ZValPointerString convertToStringAsCopy(const PHPExecEnvironment* phpExecEnv) const;

    /**
        Converts a zval to a long via the normal php conversion rules for
        internal function parameters (to conform to zend_parse_parameters() behavior).
        @param longValue pointer to a long that will receive the
        long value for this zval.  This parameter was ignored if this
        method returns false.
        @param saturate If true and the zval being converted converts to a double outside
        of the range (LONG_MIN, LONG_MAX) then set *longValue to LONG_MIN or LONG_MAX.
        @return true if the conversion succeeded, false otherwise.
     */
    bool convertInternalFunctionParameterToLong(long* longValue, bool saturate) const;

protected:
    ZValPointer(const ZValPointer& other);
    ~ZValPointer();

    ZValPointer& operator=(const ZValPointer& other);

    template <ZValType expectedType>
    bool isValid() const;
    zval* getFast() const;
    zval* getNonNull() const;
    static zval* makeNullZVal();
    zval* cloneZVal() const;
private:
    ZValPointer(zval* v, bool agentAllocated);
    mutable zval* m_val;
#if PHP_VERSION_ID >= 70000
    mutable bool m_agentAllocated;
#endif
};

/**
    Smart pointer that can wrap any type of zval* and has a method
    to down cast to another ZValPointer sub-class.

    This is a value class.
 */
class ZValPointerAny : public ZValPointer
{
    friend class ZValPointerObject;
public:
    /**
        Default constructor.  Wraps a new zval* whose type
        is IS_NULL.
     */
    ZValPointerAny();
    ZValPointerAny(const ZValPointer& other);
    ~ZValPointerAny();

    ZValPointerAny& operator=(const ZValPointer& other);

    /**
        Downcasts to a more specific ZValPointer.  Down casting to
        an incorrect type results in a ZValPointer instance that points to
        a zval of type IS_NULL.  This method is analogous to a dynamic_cast.

        @param t_toType The ZValPointer subclass to downcast to.
        @return A t_toType instance that wraps the same zval* as this ZValPointerAny.
     */
    template <class t_toType>
    t_toType cast() const;

    /**
        @return the wrapped zval*.  This method does not change the refcount of the
        zval.
     */
    zval* get() const;

    /**
        Clones the wrapped zval* according to PHP semantics of "clone" and returns the
        new wrapper.
     */
    ZValPointerAny clone() const;

    /**
        Creates a new ZValPointerAny that receives ownership of one increment
        of the specified zval.  This method will not cause a net increase the
        refcount of the zval.
        @param v Pointer to a zval to take ownership of, most not be NULL.
        @return a ZValPointerAny that wraps the specified zval*.
     */
    static ZValPointerAny take(zval* v);

    /**
        Create a new ZValPointerAny that wraps the specified zval*.  This method
        will cause a net increase of 1 in the refcount of the specified zval.
        @param v Pointer to a zval to wrap, must not be NULL.
        @return a ZValPointerAny that wraps the specified zval*.
     */
    static ZValPointerAny share(zval* v);

    /**
        Creates a new ZValPointerAny that wraps a zval* whose value is a copy
        of the value of the named constant.
        @param phpExecEnv PHP Execution environment to use to lookup the value
        of the specified constant.
        @param constantName Name of the constant to lookup.
        @param constantNameLen Length of the name of the constant in bytes, *not* including
        @return A ZValPointerAny that wraps a zval* with the value of the specified
        constant.
     */
    static ZValPointerAny getConstant(const PHPExecEnvironment* phpExecEnv,
                                      const char* constantName,
                                      size_t constantNameLen);

    /**
        If this ZValPointerAny has a valid long representation, return it. If not, return -1.
        Implemented for easy conversion to long representation of port, but works for all cases.
     */
    long convertToLong();

    /**
        Returns this zval as a String object.
        CAUTION!!! This will return a ZValPointerString, even if the underlying
        zval is not of IS_STRING type. May be gibberish.
      */
    ZValPointerString convertToString(const PHPExecEnvironment*);

    /**
        Returns true if the underlying zval is a reference.
      */
    bool isRef();
private:
    ZValPointerAny(zval* val, bool agentAllocated);
};

/**
    Smart pointer that can wrap zval*'s whose type is IS_STRING.

    This class is a value class.
 */
class ZValPointerString : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_String;
public:
    static ZValPointerString copy(const char* str);
    static ZValPointerString copy(const std::string& str);

    ZValPointerString(const ZValPointerString& other);

    ~ZValPointerString();
    ZValPointerString& operator=(const ZValPointerString& other);

    /**
        operator== for comparing a string zval to
        a const char*.
        @param str Null terminated string to compare to.
        @return true, if the zval's string value is the same
        as the specified null terminated string.  false otherwise.
     */
    bool operator==(const char* str) const;

    /**
        operator!= for comparing a string zval to
        a const char*.
        @param str Null terminated string to compare to.
        @return Complement of operater==(str).
     */
    bool operator!=(const char* str) const;

    /**
        @return A copy of the string in zval's whose type is IS_STRING, the empty
        string for zval's whose type is IS_NULL, undefined otherwise.
     */
    std::string getStringValue() const;

    /**
        Gets a pointer to the string in zval's whose type is IS_STRING.
        If the zval is not the correct type, then a NULL empty string is returned.
        @param str Pointer to a pointer that is set to point at the internal string
        buffer in the zval.
        @param stringLen Pointer a size_t that is set to the length of the string
        in bytes *not* including any terminating null bytes.
        @return true if zval was a string, false otherwise.
     */
    bool getStringValue(const char* * const str, size_t* stringLen);

    /**
        Mutate the underlying zval to the argument string. Returns true
        iff the reassignment succeeds.
      */
    bool setStringValue(const std::string& value);

    /**
        Return a double number representating the string (if possible).
        @param doubleVal Pointer to the double to be filled in
        @return bool true if string could be converted to double, false otherwise
     */
    double getDoubleValue() const;

    /**
        @return true for zval's whose type is IS_NULL, true for zval's whose
        type is IS_STRING and whose value is a zero length string, false for zval's
        whose type is IS_STRING and whose value is a non-empty string, undefined
        otherwise.
     */
    bool isNullOrEmpty() const;
private:
    ZValPointerString(const ZValPointerAny& other);
};

/**
    Smart pointer that can wrap zval*'s whose type is IS_LONG.

    This class is a value class.
 */
class ZValPointerLong : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_Long;
public:
    static ZValPointerLong create(long value);
    ZValPointerLong(const ZValPointerLong& other);
    ~ZValPointerLong();
    ZValPointerLong& operator=(const ZValPointerLong& other);

    /**
        @return The long value of any zval whose type is IS_LONG, 0 for
        zval's whose type is IS_NULL, undefined otherwise.
     */
    long getLongValue() const;
private:
    ZValPointerLong(const ZValPointerAny& other);
};

/**
    Smart pointer that can wrap zval*'s whose type is IS_DOUBLE.

    This class is a value class.
 */
class ZValPointerDouble : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_Double;
public:
    static ZValPointerDouble create(double value);
    ZValPointerDouble(const ZValPointerDouble& other);
    ~ZValPointerDouble();
    ZValPointerDouble& operator=(const ZValPointerDouble& other);

    /**
        @return The double value of any zval whose type is IS_DOUBLE, 0 for
        zval's whose type is IS_NULL, undefined otherwise.
     */
    double getDoubleValue() const;
private:
    ZValPointerDouble(const ZValPointerAny& other);
};
/**
    Smart pointer that can wrap zval*'s whose type is IS_BOOL.

    This class is a value class.
 */
class ZValPointerBool : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_Bool;
public:
    static ZValPointerBool create(bool value);
    ZValPointerBool(const ZValPointerBool& other);
    ~ZValPointerBool();
    ZValPointerBool& operator=(const ZValPointerBool& other);

    /**
        @return The boolean value of any zval whose type is IS_BOOL,
        false for zval's whose type is IS_NULL, undefined otherwise.
     */
    bool getBoolValue() const;
private:
    ZValPointerBool(const ZValPointerAny& other);
};

/**
    A class that is used to adapt the values
    in php engine HashTable's to ZValPointerAny's for
    the hash tables that store the values of a php array.
 */
class ZValHashTableValueAdapter
{
public:
    typedef ZValPointerAny t_ValueType;
#if PHP_VERSION_ID >= 70000
    static ZValPointerAny toValue(zval* foundValue);
#else
    static ZValPointerAny toValue(void* rawValue);
#endif
    static void* fromValue(ZValPointerAny wrappedValue);
private:
    ZValHashTableValueAdapter();
    ZValHashTableValueAdapter(const ZValHashTableValueAdapter&);
    ZValHashTableValueAdapter& operator=(const ZValHashTableValueAdapter&);
};

/**
 * Typedef for the Zend hash table that holds zvals.
 */
typedef ZendHashTable<ZValHashTableValueAdapter> ZValHashTable;

/**
    Smart pointer that can wrap zval*'s whose type is IS_ARRAY.

    This class is a value class.
 */
class ZValPointerArray : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_Array;
    typedef ZendHashTable<ZValHashTableValueAdapter> t_HashTable;
public:
    typedef t_HashTable::const_iterator const_iterator;

    ZValPointerArray(const ZValPointerArray& other);
    ~ZValPointerArray();
    ZValPointerArray& operator=(const ZValPointerArray& other);


    static ZValPointerArray create();
    /**
     * @return Number of elements in the array
     */
    size_t size() const;

    /**
        @return const_iterator to the beginning of the php array for any zval whose
        type is IS_ARRAY, the result of the end() method for zval's whose type is
        IS_NULL, undefined otherwise.
     */
    const_iterator begin() const;

    /**
        @return const_iterator to the end of the php array for any zval whose
        type is IS_ARRAY or IS_NULL, undefined otherwise.
     */
    const_iterator end()const;

    /**
        Looks up an entry in the php array by string name.
        @param name Name of the entry to lookup.
        @param nameLenInBytes Length in bytes *not* including the terminating null of the name of
        the entry to lookup.
        @param value pointer to a ZValPointerAny to assign to the value of the entry with the
        specified name if such an entry exists.  This parameter may be null.
        @return true if an entry with the specified name exists in the HashTable,
        false otherwise.
     */
    bool findEntryByName(const char* name, size_t nameLenInBytes, ZValPointerAny* value) const;

    /**
        Writes an entry in the php array by string key name.
        @param name Name of the entry to lookup.
        @param nameLenInBytes Length in bytes *not* including the terminating null of the name of
        the entry to lookup.
        @param value pointer to a ZValPointerAny to assign to the key in the php array.
        @return true iff array insertion succeeds.
     */
    bool putEntryByName(const char* name, size_t nameLenInBytes, const ZValPointerAny& value) const;

    /**
        Pushes a PHP object into a this array.
        @param value PHP object to insert.
        @return true iff array insertion succeeds.
    */
    bool pushEntry(const ZValPointerAny& value);
    bool pushEntry(const std::string& value);

    /**
        Deletes a PHP object from an array by key.
        @param key The key of the object to delete.
        @param keyLenInBytes The length of the key *not* including the null terminator.
        @return true iff the object was found and successfully deleted.
    */
    bool deleteEntryByKey(const char* key, size_t keyLenInBytes);

    /**
        Looks up an entry in the php array by string name.
        @param t_ValueType The ZValPointer subclass to downcast the entry's value to.
        @param name Name of the entry to lookup.
        @param nameLenInBytes Length in bytes *not* including the terminating null of the name of
        the entry to lookup.
        @return An instance of t_ValueType that wraps the entry's value if such an entry
        exists or an t_ValueType that wraps a zval whose type is IS_NULL if no such entry
        exists.
     */

    template <class t_ValueType>
    t_ValueType findEntryByName(const char* name, size_t nameLenInBytes) const;

    /**
        Looks up an entry in the php array by index.
        @param key index of the entry to lookup.
        @param value pointer to a ZValPointerAny to assign to the value of the entry with the
        specified index if such an entry exists.  This parameter may be null.
        @return true if an entry with the specified index exists in the HashTable,
        false otherwise.
     */
    bool findEntryByLong(long name, ZValPointerAny* value) const;

    /**
        Looks up an entry in the php array by index.
        @param t_ValueType The ZValPointer subclass to downcast the entry's value to.
        @param key index of the entry to lookup.
        @return An instance of t_ValueType that wraps the entry's value if such an entry
        exists or an t_ValueType that wraps a zval whose type is IS_NULL if no such entry
        exists.
     */
    template <class t_ValueType>
    t_ValueType findEntryByLong(long name) const;

    /**
        Returns the pointer to the internal Zend hash table.
     */
    const HashTable* getInternalHashTable() const;

private:
    ZValPointerArray(const ZValPointerAny& other);
    void copyForImmutable(zval* zvalue) const;
    t_HashTable m_hashTable;
};

/**
    Smart pointer that can wrap zval*'s whose type is IS_OBJECT.

    This class is a value class.
 */
class ZValPointerObject : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_Object;
public:

    typedef PHPExecEnvironment::AccessLevel AccessLevel;

    /**
        Sentinel zend_object_handle value for an invalid
        handle.
     */
    static const zend_object_handle ILLEGAL_OBJECT_HANDLE = 0;

    /**
        Constructs a "null" ZValPointerObject.
     */
    ZValPointerObject();

    ZValPointerObject(const ZValPointerObject& other);
    ~ZValPointerObject();
    ZValPointerObject& operator=(const ZValPointerObject& other);

    /**
        @return the name of the class which the object this pointer
        points at is an instance of.  If this not a valid reference to
        a php object, then this method returns the empty string.
     */
    std::string getClassName(const PHPExecEnvironment* phpExecEnv) const;

#if PHP_VERSION_ID >= 70000
    /**
        Return the zend_object* associated with the zval wrapped by this class
     */
    zend_object* getZendObject() const;
#endif

    /**
        Return the internal PHP object handle.
     */
    long getInternalHandle() const;

    /**
        Call an instance method on the object this pointer points at.
        @param methodName Name of the method to call.
        @param methodName Length of the name of the method to call
        @param params ZValPointerAny's for the parameters to pass to the instance method.
        @param retValPtr Pointer to a ZValPointerAny that will be made to point
        at the return value of the called method.  This must never be null.
        @return true if the method was successfully called, false otherwise.
     */
    bool callInstanceMethod(const PHPExecEnvironment* phpExecEnv,
                            const char* methodName,
                            size_t methodNameLen,
                            const std::vector<ZValPointerAny>& params,
                            ZValPointerAny* retValPtr);

    /**
        Call an instance method on the object this pointer points at.
        @param t_PointerType The ZValPointer sub-class that indicates the type of
        value the called method is expected to return.
        @param methodName Name of the method to call.
        @param params ZValPointerAny's for the parameters to pass to the instance method.
        @return The return value of the called method or a null ZValPointer if the method
        could not be called.
     */
    template <class t_PointerType>
    t_PointerType callInstanceMethod(const PHPExecEnvironment* phpExecEnv,
                                     const char* methodName,
                                     size_t methodNameLen,
                                     const std::vector<ZValPointerAny>& params);

    /**
        Call an instance method with no parameters on the object this pointer points at.
        @param t_PointerType The ZValPointer sub-class that indicates the type of
        value the called method is expected to return.
        @param methodName Name of the method to call.
        @return The return value of the called method or a null ZValPointer if the method
        could not be called.
     */
    template <class t_PointerType>
    t_PointerType callInstanceMethod(const PHPExecEnvironment* phpExecEnv,
                                     const char* methodName,
                                     size_t methodNameLen);

    /**
        @return the object handle for the object referenced
        by this pointer.
     */
    zend_object_handle getObjectHandle() const;

    /**
        Gets a pointer to the internal data structure
        used by an instance of a PHP class whose methods
        are implemented in native code.
        @param t_InternalObjectType The type to cast the void* in
        the PHP object to.
        @param phpExecEnv PHP execution environment
        @return The internal data of the PHP object.
     */
    template <class t_InternalObjectType>
    t_InternalObjectType* getInternalObject(const PHPExecEnvironment* phpExecEnv);

    /**
        @return the object properties for the object referenced
        by this pointer
     */
    ZValHashTable getProperties() const;

    /**
        Looks up an object property by string name. Can handle public,
        protected, and private ones, depending on the access level. It is
        not recommended to use this method directly. Instead use the
        overloaded ones that return the found property.
        @param accessLevel Access level of the property to lookup.
        @param key Name of the property to lookup.
        @param keyLen Length in bytes *not* including the terminating null of the name of
        the property to lookup.
        @param value pointer to a ZValPointerAny to assign to the value of the property with the
        specified name if such an entry exists.
        @return true if a property with the specified name exists, false otherwise.
     */
    bool findPropertyByName(const PHPExecEnvironment* phpExecEnv,
                            AccessLevel accessLevel,
                            const char* name,
                            size_t nameLenInBytes,
                            zend_class_entry* scope,
                            ZValPointerAny* value) const;

    /**
        Add a named property to an object with value 'value'.
        @param accessLevel Access level of the property to lookup.
        @param name Name of the property to add.
        @param nameLenInBytes Length of the property name not including the terminating
                              null.
        @param scope Scope for the property look-up (only makes sense for private properties)
        @param value a zval to be set as the value of the property
        @return bool, true if write succeeds else false
        Note: If property alread exists write will fail
     */
    bool writePropertyByName(const PHPExecEnvironment* phpExecEnv,
                             AccessLevel accessLevel,
                             const char* name,
                             size_t nameLenInBytes,
                             const zend_class_entry* scope,
                             const ZValPointerAny& value) const
    {
        if (!isValid<ZVal_Object>()) {
            return false;
        }
        zval* const object = getFast();
        BOOST_ASSERT(object != NULL);
        return phpExecEnv->writePropertyByName(object, accessLevel, name,
                                           nameLenInBytes, scope, value);
    }

    /**
        Add a named property to an object with value 'value'.
        Note: This api adds the property with access level PUBLIC
        @param name Name of the property to add.
        @param nameLenInBytes Length of the property name not including the terminating
                              null.
        @param value a zval to be set as the value of the property
        @return bool, true if write succeeds else false
        Note: If property alread exists write will fail
     */
    bool writePropertyByName(const PHPExecEnvironment* phpExecEnv,
                             const char* name,
                             size_t nameLenInBytes,
                             const ZValPointerAny& value) const
    {
        return writePropertyByName(phpExecEnv, PHPExecEnvironment::PUBLIC, name,
                                    nameLenInBytes, NULL, value);
    }

    /**
        Looks up an object property by string name (any property the object has access to).
        @param t_ValueType The ZValPointer subclass to downcast the property's value to.
        @param name Name of the property to lookup.
        @param nameLenInBytes Length in bytes *not* including the terminating null of the
        name of the property to lookup.
        @return An instance of t_ValueType that wraps the property's value if such a property
        exists or an t_ValueType that wraps a zval whose type is IS_NULL if no such property
        exists.
     */
    template <class t_ValueType>
    t_ValueType findPropertyByName(const PHPExecEnvironment* phpExecEnv,
                                   const char* name,
                                   size_t nameLenInBytes) const;

    /**
        Looks up a protected object property by string name.
        @param t_ValueType The ZValPointer subclass to downcast the property's value to.
        @param name Name of the property to lookup.
        @param nameLenInBytes Length in bytes *not* including the terminating null of the
        name of the property to lookup.
        @return An instance of t_ValueType that wraps the property's value if such a property
        exists or an t_ValueType that wraps a zval whose type is IS_NULL if no such property
        exists.
     */
    template <class t_ValueType>
    t_ValueType findProtectedPropertyByName(const PHPExecEnvironment* phpExecEnv,
                                            const char* name,
                                            size_t nameLenInBytes) const;

    /**
        Looks up a private object property by string name.
        @param t_ValueType The ZValPointer subclass to downcast the property's value to.
        @param name Name of the property to lookup.
        @param nameLenInBytes Length in bytes *not* including the terminating null of the
        name of the property to lookup.
        @param scope The scope in which to look up the property (the declaring class).
        @return An instance of t_ValueType that wraps the property's value if such a property
        exists or an t_ValueType that wraps a zval whose type is IS_NULL if no such property
        exists.
     */
    template <class t_ValueType>
    t_ValueType findPrivatePropertyByName(const PHPExecEnvironment* phpExecEnv,
                                          const char* name,
                                          size_t nameLenInBytes,
                                          zend_class_entry* scope) const;

    bool readPropertyByName(const PHPExecEnvironment* phpExecEnv,
                            const char* name,
                            size_t nameLenInBytes,
                            ZValPointerAny* value) const;

    /**
        Check if given class is in the object's inheritance hierarchy.
        @param targetClassEntry The class entry to check for.
        @return true if the object inherits from the given class in its
        hierarchy, false otherwise.
     */
    bool isInstanceOf(const PHPExecEnvironment* phpExecEnv, zend_class_entry* targetClassEntry) const;

    /**
        Get the class entry for a specified zval that refers to an object.
        @param zval Value that references an object
        @return the zend_class_entry* of the class of the object
        referenced by the specified zval or null if the specified zval
        does not refer to a zval.
    */
    zend_class_entry* getClassEntry(const PHPExecEnvironment* phpExecEnv) const;

    bool getPrimitiveValue(const PHPExecEnvironment* phpExecEnv, ZValPointerAny* value) const;

private:
    ZValPointerObject(const ZValPointerAny& other);
};

/**
    Smart pointer that can wrap zval*'s whose type is IS_RESOURCE.

    This class is a value class.
 */
class ZValPointerResource : public ZValPointer
{
    friend class ZValPointerAny;
private:
    static const ZValType type = ZVal_Resource;
public:

    /**
        Sentinel zend_object_handle value for an invalid
        resource.
     */
    static const zend_object_handle ILLEGAL_RESOURCE = 0;


    ZValPointerResource(const ZValPointerResource& other);
    ~ZValPointerResource();
    ZValPointerResource& operator=(const ZValPointerResource& other);

    /**
        @return the resource referenced
        by this pointer.
     */
#if PHP_VERSION_ID >= 70000
    void* getResource() const;
    int getResourceType() const;
#else
    long getResource() const;
#endif

    /**
     * @return true if both pointers reference the exact same resource
     * (handle)
     */
    bool isIdentical(const ZValPointerAny& other) const;

    /**
     * @param expectedTypes the type of resource we are trying to fetch.  Each
     * expected type must be an int.
     *
     * @return the internal resource structure referenced by this pointer.
     * If this pointer references a resource whose type does not match the
     * one passed in, returns NULL.
     */
    template <class t_InternalResourceType, typename... t_expected>
    t_InternalResourceType* getInternalResource(t_expected... expectedTypes) const;

    /**
        @return the php_stream referenced by this pointer
        if this pointer references a resource that is a stream, otherwise
        NULL.
     */
    php_stream* getStream() const;

#if PHP_VERSION_ID >= 70000
    /**
        Return the zend_resource* associated with the zval wrapped by this class
     */
    zend_resource* getZendResource() const;
#endif

private:
    template <typename... t_Args>
    bool resourceIsExpectedType(int actualType, int expectedType, t_Args...args) const;

    bool resourceIsExpectedType(int actualType) const;
    ZValPointerResource(const ZValPointerAny& other);
};

inline ZValPointer::operator bool() const
{
    return getType() != ZVal_Null;
}

inline ZValPointer::ZValType ZValPointer::getType() const
{
    if (!m_val)
        return ZVal_Null;
#if PHP_VERSION_ID >= 70000
    if (Z_TYPE_P(m_val) == IS_TRUE || Z_TYPE_P(m_val) == IS_FALSE)
        return ZValPointer::ZVal_Bool;
#endif
    return ((ZValPointer::ZValType)(Z_TYPE_P(m_val)));
}

inline bool ZValPointer::operator==(const ZValPointer& other) const
{
    if (m_val == other.m_val)
        return true;
    if ((!m_val) && (other.getType() == ZVal_Null))
        return true;
    if ((!other.m_val) && (getType() == ZVal_Null))
        return true;
    return false;
}

inline bool ZValPointer::operator!=(const ZValPointer& other) const
{
    return !(this->operator==(other));
}

// Need to turn off -Wuninitialized because of bugs in gcc and to a lesser degree in
// is_numeric_string ( which is a php function ).
#pragma GCC diagnostic ignored "-Wuninitialized"
inline bool ZValPointer::convertInternalFunctionParameterToLong(long* longValue, bool saturate) const
{
    switch (getType()) {
        case ZVal_Null:
            *longValue = 0;
            return true;
        case ZVal_Long:
            *longValue = Z_LVAL_P(getFast());
            return true;
        case ZVal_Double:
            if (saturate) {
                double doubleValue = Z_DVAL_P(getFast());
                if (doubleValue < LONG_MIN) {
                    *longValue = LONG_MIN;
                    return true;
                }
                else if (doubleValue > LONG_MAX) {
                    *longValue = LONG_MAX;
                    return true;
                }
            }
            // Fall through!!!

        case ZVal_Bool:
            {
                zval* val = getFast();
#if PHP_VERSION_ID >= 70000
                convert_to_long_ex(val);
#else
                convert_to_long_ex(&val);
#endif
                if (Z_REFCOUNTED_P(val))
                    Z_ADDREF_P(val);
                bool result = false;
                if (Z_TYPE_P(val) == IS_LONG) {
                    *longValue = Z_LVAL_P(val);
                    result = true;
                }
#if PHP_VERSION_ID >= 70000
                // can't just free a zval that is not refcounted. maybe still needed
                // by something else. TODO: not refcounted zvals should be copied to
                // a new instance in the case of PHP 7.
                //efree(val);
#else
                APPD_ZVAL_PTR_DTOR(&val);
#endif
                return result;
            }
        case ZVal_String:
            {
                zval* val = getFast();
                const char* stringBytes = Z_STRVAL_P(val);
                size_t nStringBytes = Z_STRLEN_P(val);
                zend_long localLongValue = 0;
                double doubleValue = 0;

                int parsedType = is_numeric_string(stringBytes,
                                                   nStringBytes,
                                                   &localLongValue,
                                                   &doubleValue,
                                                   0);
                if (!parsedType)
                    return false;
                if (parsedType == IS_DOUBLE) {
                    if (saturate) {
                        if (doubleValue < LONG_MIN) {
                            *longValue = LONG_MIN;
                            return true;
                        }
                        else if (doubleValue > LONG_MAX) {
                            *longValue = LONG_MAX;
                            return true;
                        }
                    }
                    if (doubleValue > LONG_MAX)
                        *longValue = static_cast<long>(static_cast<unsigned long>(doubleValue));
                    else
                        *longValue = static_cast<long>(doubleValue);
                    return true;
                }
                BOOST_ASSERT(parsedType == IS_LONG);
                *longValue = localLongValue;
                return true;
            }
        case ZVal_Array:
        case ZVal_Object:
        case ZVal_Resource:
            return false;
    }
    return false;
}
// Turn -Wuninitialized back on.
#pragma GCC diagnostic error "-Wuninitialized"

inline zval* ZValPointer::getFast() const
{
    return m_val;
}

inline zval* ZValPointer::getNonNull() const
{
    if (!m_val)
    {
        m_val = makeNullZVal();
#if PHP_VERSION_ID >= 70000
        m_agentAllocated = true;
#endif
    }
    return m_val;
}

inline zval* ZValPointer::cloneZVal() const
{
    zval* clone = makeNullZVal();
    const zval* orig = getNonNull();
    APPD_MAKE_COPY_ZVAL(clone, orig);
    return clone;
}

inline ZValPointer::ZValPointer(const ZValPointer& other)
    : m_val(other.m_val)
{
#if PHP_VERSION_ID >= 70000
    m_agentAllocated = other.m_agentAllocated;
#endif
    if (m_val && Z_REFCOUNTED_P(m_val))
        Z_ADDREF_P(m_val);
}

inline zval* ZValPointer::makeNullZVal()
{
    zval *result = NULL;
    APPD_MAKE_STD_ZVAL(result);
    ZVAL_NULL(result);
    return result;
}

#if PHP_VERSION_ID >= 70000
inline void destructZval(zval* val, bool agentAllocated) {
    if (!val) {
      return;
    }

    // DLNATIVE-1108, DLNATIVE-1172 - do not free scalar zvals in php7!
    // Since there is no reference counting for scalar zvals, we can never
    // free them even if (especially if!) we allocated them in the agent.
    // Because we allocate memory for the zval on the heap and return a
    // non-reference counted pointer to PHP, we have a biblical shitstorm of
    // memory corruption; by never freeing it, it ensures the memory cannot
    // be reused. Of course this causes a memory leak in PHP...  It is not
    // a 'real' memory leak per-se, since PHP cleans up all unfreed memory
    // at the end of each request.  However this is not ideal, and causes
    // lots of horrible warning logs for debug builds of PHP.

    if (Z_REFCOUNTED_P(val)) {
      if (!Z_COUNTED_P(val))
          return;
      if (Z_DELREF_P(val) == 0) {
        zval_dtor_func_for_ptr(Z_COUNTED_P(val));
        if (agentAllocated)
          efree(val);
      }
    }
#else
inline void destructZval(zval* val) {
    if (val) {
        zval_ptr_dtor(&val);
    }
#endif
}

inline ZValPointer::~ZValPointer()
{
    // We should never free a zval from module shutdown
    // doing so can cause the zend heap to be corrupted.
    // If this assert blows up you need to wrap your
    // ZValPointer instance with a SafeModuleShutdownWrapper.
    BOOST_ASSERT(!SafeModuleShutdown::inModuleShutdown());
#if PHP_VERSION_ID >= 70000
    destructZval(m_val, m_agentAllocated);
#else
    destructZval(m_val);
#endif

}

inline ZValPointer& ZValPointer::operator=(const ZValPointer& other)
{
// destroy original zval first
#if PHP_VERSION_ID >= 70000
    destructZval(m_val, m_agentAllocated);
#else
    destructZval(m_val);
#endif
// assign new zval
    m_val = other.m_val;
#if PHP_VERSION_ID >= 70000
    m_agentAllocated = other.m_agentAllocated;
#endif
    if (m_val && Z_REFCOUNTED_P(m_val))
        Z_ADDREF_P(m_val);
    return *this;
}

inline ZValPointerString ZValPointer::convertToStringAsCopy(const PHPExecEnvironment* phpExecEnv) const
{
    zval* conversionZVal = cloneZVal();
    return phpExecEnv->convertToString(conversionZVal);
}

template <ZValPointer::ZValType expectedType>
inline bool ZValPointer::isValid() const
{
    return getType() == expectedType;
}

inline ZValPointer::ZValPointer(zval* val, bool agentAllocated)
    : m_val(val)
{
#if PHP_VERSION_ID >= 70000
    m_agentAllocated = agentAllocated;
#endif
}

inline ZValPointerAny::ZValPointerAny()
    : ZValPointer(NULL, false)
{
}

inline ZValPointerAny::ZValPointerAny(const ZValPointer& other)
    : ZValPointer(other)
{
}

inline ZValPointerAny::~ZValPointerAny()
{
}

inline ZValPointerAny& ZValPointerAny::operator=(const ZValPointer& other)
{
    ZValPointer::operator=(other);
    return *this;
}

template <class t_toType>
inline t_toType ZValPointerAny::cast() const
{
    // If the type is not correct, then
    // construct the type specific pointer with a
    // null value.
    if (!isValid<t_toType::type>())
        return t_toType(ZValPointerAny());
    return t_toType(*this);
}

template <>
inline ZValPointerAny ZValPointerAny::cast<ZValPointerAny>() const
{
    return *this;
}


inline zval* ZValPointerAny::get() const
{
    return ZValPointer::getNonNull();
}

inline ZValPointerAny ZValPointerAny::clone() const
{
    return ZValPointerAny(cloneZVal(), true);
}

inline ZValPointerAny ZValPointerAny::take(zval* val)
{
    return ZValPointerAny(val, true);
}

inline ZValPointerAny ZValPointerAny::share(zval* val)
{
    if (Z_REFCOUNTED_P(val))
        Z_ADDREF_P(val);
    return ZValPointerAny(val, false);
}

inline long ZValPointerAny::convertToLong()
{
    long discoveredLong = 0;
    bool const thisIsLong = convertInternalFunctionParameterToLong(&discoveredLong, false);
    return thisIsLong ? discoveredLong : -1;
}

inline ZValPointerString ZValPointerAny::convertToString(const PHPExecEnvironment* execEnv)
{
    zval* currentZval = get();
    return execEnv->convertToString(currentZval);
}

inline bool ZValPointerAny::isRef()
{
#if PHP_VERSION_ID >= 70000
    return Z_TYPE_P(get()) == IS_REFERENCE;
#elif PHP_VERSION_ID >= 50300
    return get()->is_ref__gc != 0;
#else
    // PHP 5.2 case.
    return get()->is_ref != 0;
#endif

}

inline ZValPointerAny ZValPointerAny::getConstant(const PHPExecEnvironment* phpExecEnv,
                                                  const char* constantName,
                                                  size_t constantNameLen)
{
    zval* newZVal = makeNullZVal();
    if (!phpExecEnv->getConstant(constantName, constantNameLen, newZVal)) {
#if PHP_VERSION_ID >= 70000
        efree(newZVal);
#else
        APPD_ZVAL_PTR_DTOR(&newZVal);
#endif
        return ZValPointerAny();
    }
    return ZValPointerAny::take(newZVal);
}

inline ZValPointerAny::ZValPointerAny(zval* val, bool agentAllocated)
    : ZValPointer(val, agentAllocated)
{
}

inline ZValPointerString ZValPointerString::copy(const char* str)
{
    zval *result = NULL;
    APPD_MAKE_STD_ZVAL(result);
    APPD_ZVAL_STRING_DUP_TRUE(result, const_cast<char*>(str));
    return ZValPointerAny::take(result).cast<ZValPointerString>();
}

inline ZValPointerString ZValPointerString::copy(const std::string& str)
{
    zval *result = NULL;
    APPD_MAKE_STD_ZVAL(result);
    APPD_ZVAL_STRINGL_DUP_TRUE(result, const_cast<char*>(str.c_str()), str.length());
    return ZValPointerAny::take(result).cast<ZValPointerString>();
}

inline ZValPointerString::ZValPointerString(const ZValPointerString& other)
    : ZValPointer(other)
{
}

inline ZValPointerString::~ZValPointerString()
{
}

inline ZValPointerString& ZValPointerString::operator=(const ZValPointerString& other)
{
    ZValPointer::operator=(other);
    return *this;
}

inline bool ZValPointerString::operator==(const char* str) const
{
    // If this value is not actually a string,
    // return false.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_String>())
        return false;
    const char* const thisStr = Z_STRVAL_P(getFast());
    BOOST_ASSERT(thisStr != NULL);
    size_t const thisStrLen = Z_STRLEN_P(getFast());
    return strlen(str) == thisStrLen && strncmp(thisStr, str, thisStrLen) == 0;
}

inline bool ZValPointerString::operator!=(const char* str) const
{
    return !operator==(str);
}

inline std::string ZValPointerString::getStringValue() const
{
    // If this value is not actually a string,
    // return the empty string.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_String>())
        return std::string();
    BOOST_ASSERT(getFast() != NULL);
    return std::string(Z_STRVAL_P(getFast()), Z_STRLEN_P(getFast()));
}

inline double ZValPointerString::getDoubleValue() const
{
    // If this value is not actually a string,
    // return 0.0
    //
    // TODO create an exception policy
    if (!isValid<ZVal_String>())
        return 0.0;

    int type;
    zend_long longVal = 0;
    double doubleVal = 0.0;
    BOOST_ASSERT(getFast() != NULL);
    type = is_numeric_string(Z_STRVAL_P(getFast()), Z_STRLEN_P(getFast()), &longVal, &doubleVal, 0);
    switch (type) {
        case IS_LONG:
            doubleVal = (double)longVal;
            // drop through
        case IS_DOUBLE:
            return doubleVal;
        default:
            return 0.0;
    }
}

inline bool ZValPointerString::getStringValue(const char* * const str, size_t* stringLen)
{
    // If this value is not actually a string,
    // return the empty string.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_String>()) {
        *str = NULL;
        *stringLen = 0;
        return false;
    }

    *str = Z_STRVAL_P(getFast());
    *stringLen = Z_STRLEN_P(getFast());
    return true;
}

inline bool ZValPointerString::setStringValue(const std::string& value)
{
    if (!isValid<ZVal_String>())
        return false;

    zval_dtor(getFast());
    APPD_ZVAL_STRINGL_DUP_TRUE(getFast(), const_cast<char*>(value.c_str()), value.size());

    return true;
}

inline bool ZValPointerString::isNullOrEmpty() const
{
    // If this value is not actually a string,
    // return false.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_String>())
        return true;
    BOOST_ASSERT(getFast() != NULL);
    return Z_STRLEN_P(getFast()) == 0;
}

inline ZValPointerString::ZValPointerString(const ZValPointerAny& other)
    : ZValPointer(other)
{
}

inline ZValPointerLong ZValPointerLong::create(long value)
{
    zval* newZVal = makeNullZVal();
    ZVAL_LONG(newZVal, value);
    return ZValPointerAny::take(newZVal);
}

inline ZValPointerLong::ZValPointerLong(const ZValPointerLong& other)
    : ZValPointer(other)
{
}

inline ZValPointerLong::~ZValPointerLong()
{
}

inline ZValPointerLong& ZValPointerLong::operator=(const ZValPointerLong& other)
{
    ZValPointer::operator=(other);
    return *this;
}

inline long ZValPointerLong::getLongValue() const
{
    // If this value is not actually a long,
    // return 0.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Long>())
        return 0;
    BOOST_ASSERT(getFast() != NULL);
    return Z_LVAL_P(getFast());
}

inline ZValPointerLong::ZValPointerLong(const ZValPointerAny& other)
    : ZValPointer(other)
{
}

inline ZValPointerDouble ZValPointerDouble::create(double value)
{
    zval* newZVal = makeNullZVal();
    ZVAL_DOUBLE(newZVal, value);
    return ZValPointerAny::take(newZVal);
}

inline ZValPointerDouble::ZValPointerDouble(const ZValPointerDouble& other)
    : ZValPointer(other)
{
}

inline ZValPointerDouble::~ZValPointerDouble()
{
}

inline ZValPointerDouble& ZValPointerDouble::operator=(const ZValPointerDouble& other)
{
    ZValPointer::operator=(other);
    return *this;
}

inline double ZValPointerDouble::getDoubleValue() const
{
    // If this value is not actually a double,
    // return 0.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Double>())
        return 0;
    BOOST_ASSERT(getFast() != NULL);
    return Z_DVAL_P(getFast());
}

inline ZValPointerDouble::ZValPointerDouble(const ZValPointerAny& other)
    : ZValPointer(other)
{
}

inline ZValPointerBool ZValPointerBool::create(bool value)
{
    zval* newZVal = makeNullZVal();
    const int intValue = value ? 1 : 0;
    ZVAL_BOOL(newZVal, intValue);
    return ZValPointerAny::take(newZVal);
}

inline ZValPointerBool::ZValPointerBool(const ZValPointerBool& other)
    : ZValPointer(other)
{
}

inline ZValPointerBool::~ZValPointerBool()
{
}

inline ZValPointerBool& ZValPointerBool::operator=(const ZValPointerBool& other)
{
    ZValPointer::operator=(other);
    return *this;
}

inline bool ZValPointerBool::getBoolValue() const
{
    // If this value is not actually a bool,
    // return false.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Bool>())
        return false;
    BOOST_ASSERT(getFast() != NULL);
    return Z_BVAL_P(getFast());
}

inline ZValPointerBool::ZValPointerBool(const ZValPointerAny& other)
    : ZValPointer(other)
{
}

#if PHP_VERSION_ID >= 70000
inline ZValPointerAny ZValHashTableValueAdapter::toValue(zval* foundValue)
{
    return ZValPointerAny::share(foundValue);
}
#else
inline ZValPointerAny ZValHashTableValueAdapter::toValue(void* rawValue)
{
    zval** zVal = reinterpret_cast<zval**>(rawValue);
    return ZValPointerAny::share(*zVal);
}
#endif

inline void* ZValHashTableValueAdapter::fromValue(ZValPointerAny wrappedValue)
{
    return wrappedValue.get();
}

inline ZValPointerArray::ZValPointerArray(const ZValPointerArray& other)
    : ZValPointer(other)
    , m_hashTable(other.m_hashTable)
{
}

inline ZValPointerArray ZValPointerArray::create()
{
    zval* zvalArray;
    ALLOC_INIT_ZVAL(zvalArray);
    zend_array_init(zvalArray);

    ZValPointerAny zptr(ZValPointerAny::take(zvalArray));
    return zptr.cast<ZValPointerArray>();
}

inline ZValPointerArray::~ZValPointerArray()
{
}

inline ZValPointerArray& ZValPointerArray::operator=(const ZValPointerArray& other)
{
    ZValPointer::operator=(other);
    m_hashTable = other.m_hashTable;
    return *this;
}

inline size_t ZValPointerArray::size() const
{
    if (!isValid<ZVal_Array>())
        return 0;

    return m_hashTable.size();
}

inline ZValPointerArray::const_iterator ZValPointerArray::begin() const
{
    // If this value is not actually an array,
    // return end, such that the iteration is
    // immediately in the terminal condition.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Array>())
        return end();
    return m_hashTable.begin();
}

inline ZValPointerArray::const_iterator ZValPointerArray::end() const
{
    return m_hashTable.end();
}

inline ZValPointerArray::ZValPointerArray(const ZValPointerAny& other)
    : ZValPointer(other)
    , m_hashTable(t_HashTable::share(isValid<ZVal_Array>() ? Z_ARRVAL_P(getFast()) : NULL))
{
}

inline bool ZValPointerArray::findEntryByName(const char* name, size_t nameLenInBytes, ZValPointerAny* value) const
{
    // If this value is not actually an array,
    // return false ( no entry found ).
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Array>())
        return false;
    BOOST_ASSERT(name[nameLenInBytes] == '\0');
    return m_hashTable.find(name, nameLenInBytes + 1, value);
}

inline void ZValPointerArray::copyForImmutable(zval* zvalue) const
{
    // DLNATIVE-517 Opcache makes hashtables immutable in php7 - copy before modifying
#if PHP_VERSION_ID >= 70000
    if (Z_IMMUTABLE_P(zvalue)) {
        uint32_t refcount = Z_REFCOUNT_P(zvalue);
        zval_copy_ctor_func(zvalue);
        Z_SET_REFCOUNT_P(zvalue, refcount);
    }
#endif
}

inline bool ZValPointerArray::putEntryByName(const char* name, size_t nameLenInBytes, const ZValPointerAny& value) const
{
    // If this value is not actually an array,
    // return false ( no entry found ).
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Array>())
        return false;
    zval* pThis(getNonNull());
    if (!pThis)
        return false;

    copyForImmutable(pThis);

    zval* local = value.get();
    if (Z_REFCOUNTED_P(local))
        Z_ADDREF_P(local);
    BOOST_ASSERT(name[nameLenInBytes] == '\0');
    return add_assoc_zval_ex(pThis,
            const_cast<char*>(name),
#if PHP_VERSION_ID >= 70000
            nameLenInBytes, local) == SUCCESS;
#else
            nameLenInBytes + 1, local) == SUCCESS;
#endif
}

inline bool ZValPointerArray::pushEntry(const ZValPointerAny& value)
{
    if (!isValid<ZVal_Array>())
        return false;
    zval* pThis(getNonNull());
    if (!pThis)
        return false;

    copyForImmutable(pThis);

    zval* local = value.get();
    if (Z_REFCOUNTED_P(local))
        Z_ADDREF_P(local);
    return SUCCESS == add_next_index_zval(pThis, local);
}

inline bool ZValPointerArray::pushEntry(const std::string& value)
{
    if (!isValid<ZVal_Array>())
        return false;
    zval* pThis(getNonNull());
    if (!pThis)
        return false;

    copyForImmutable(pThis);

    return SUCCESS == add_next_index_stringl(pThis, const_cast<char*>(value.c_str()),
                                             value.length() APPD_DUP_TRUE);
}

inline bool ZValPointerArray::deleteEntryByKey(const char* key, size_t keyLenInBytes)
{
    BOOST_ASSERT(key[keyLenInBytes] == '\0');
#if PHP_VERSION_ID >= 70000
    // zend_hash_str_find/update/del/etc. takes strlen *without* terminating null byte
    return zend_hash_str_del(Z_ARRVAL_P(ZValPointerAny(*this).get()),
                         const_cast<char*>(key),
                         keyLenInBytes) == SUCCESS;
#else
    return zend_hash_del(Z_ARRVAL_P(ZValPointerAny(*this).get()),
                         const_cast<char*>(key),
                         keyLenInBytes + 1) == SUCCESS;
#endif
}

template <class t_ValueType>
inline t_ValueType ZValPointerArray::findEntryByName(const char* name, size_t nameLenInBytes) const
{
    ZValPointerAny value;
    findEntryByName(name, nameLenInBytes, &value);
    return value.cast<t_ValueType>();
}

inline bool ZValPointerArray::findEntryByLong(long name, ZValPointerAny* value) const
{
    // If this value is not actually an array,
    // return false ( no entry found ).
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Array>())
        return false;
    return m_hashTable.find(name, value);
}

template <class t_ValueType>
inline t_ValueType ZValPointerArray::findEntryByLong(long name) const
{
    ZValPointerAny value;
    findEntryByLong(name, &value);
    return value.cast<t_ValueType>();
}

inline const HashTable* ZValPointerArray::getInternalHashTable() const
{
    return m_hashTable.get();
}

inline std::string StringHelpers::zvalArrayPrettyPrint(const ZValPointerArray& arr,
        boost::unordered_set<const HashTable*>& arraysTraversed)
{
    if (!arr) return "";
    if (arr.size() == 0) return "[]";

    HashTable* ht = Z_ARRVAL_P(arr.getFast());
    // insert(ht).second is false if ht is already in there.
    if (!arraysTraversed.insert(ht).second)
        return "[*ARRAY RECURSION*]";

    std::string arrayString = "[";

    ZValPointerArray::const_iterator iter(arr.begin());
    // If the first key is a string or a nonzero long, treat as map and print
    // keys. (as a weak heuristic for differentiating arrays from maps)
    const bool isMap = (*iter).keyIsString() || (*iter).getKeyAsLong() != 0;
    while (iter != arr.end()) {
        // Prevents trailing comma.
        if (iter != arr.begin())
            arrayString += ", ";
        if (isMap) {
            // Parse keys.
            if ((*iter).keyIsString()) {
                arrayString += '"' + (*iter).getKeyAsString() + '"';
            }
            else if ((*iter).keyIsLong()) {
                ulong key = (*iter).getKeyAsLong();
                arrayString += boost::lexical_cast<std::string>(key);
            }
            else {
                // Key should always be long or string. If we get here,
                // something is wrong.
                BOOST_ASSERT_MSG(false, "Key is not of expected type (long or string)");
            }

            // Add key/value delimiter: <key> => <value>
            arrayString += " => ";
        }

        // Parse values.
        arrayString += StringHelpers::prettyPrint((*iter).getValue(), arraysTraversed);
        ++iter;
    }
    return arrayString + "]";
}

inline std::string StringHelpers::prettyPrint(
        const ZValPointerAny& value)
{
    boost::unordered_set<const HashTable*> arraysTraversed;
    return StringHelpers::prettyPrint(value, arraysTraversed);
}

inline std::string StringHelpers::prettyPrint(
        const ZValPointerAny& value,
        boost::unordered_set<const HashTable*>& arraysTraversed)
{
    if (!value) return "null";

    switch (value.getType()) {
        case ZValPointer::ZVal_Null:
            return "null";
        case ZValPointer::ZVal_Long: {
            ZValPointerLong longZVal(value.cast<ZValPointerLong>());
            BOOST_ASSERT(longZVal);
            return boost::lexical_cast<std::string>(longZVal.getLongValue());
        }
        case ZValPointer::ZVal_Double: {
            ZValPointerDouble doubleZVal(value.cast<ZValPointerDouble>());
            BOOST_ASSERT(doubleZVal);
            return boost::lexical_cast<std::string>(doubleZVal.getDoubleValue());
        }
        case ZValPointer::ZVal_Bool: {
            ZValPointerBool boolZVal(value.cast<ZValPointerBool>());
            BOOST_ASSERT(boolZVal);
            return boolZVal.getBoolValue() ? "true" : "false";
        }
        case ZValPointer::ZVal_String: {
            ZValPointerString stringZVal(value.cast<ZValPointerString>());
            return stringZVal.getStringValue();
        }
        case ZValPointer::ZVal_Array: {
            ZValPointerArray arrayZVal(value.cast<ZValPointerArray>());
            BOOST_ASSERT(arrayZVal);
            return zvalArrayPrettyPrint(arrayZVal, arraysTraversed);
        }
        case ZValPointer::ZVal_Object:
            BOOST_ASSERT(value.cast<ZValPointerObject>());
            return "Object";
        case ZValPointer::ZVal_Resource:
            BOOST_ASSERT(value.cast<ZValPointerResource>());
            return "Resource";
#if PHP_VERSION_ID >= 70000
        case ZValPointer::ZVal_Reference:
            return "Reference";
#endif
        default:
            // The zval should not have any other type.
            BOOST_ASSERT_MSG(false, "Unexpected zval type");
            return "";
    }
}

inline ZValPointerObject::ZValPointerObject()
    : ZValPointer(ZValPointerAny())
{
}

inline ZValPointerObject::ZValPointerObject(const ZValPointerObject& other)
    : ZValPointer(other)
{
}

inline ZValPointerObject::~ZValPointerObject()
{
}

inline ZValPointerObject& ZValPointerObject::operator=(const ZValPointerObject& other)
{
    ZValPointer::operator=(other);
    return *this;
}

inline std::string ZValPointerObject::getClassName(const PHPExecEnvironment* phpExecEnv) const
{
    // If this value is not actually an object,
    // return the empty string.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return std::string();

    zval* object = getFast();
    BOOST_ASSERT(object != NULL);
    return phpExecEnv->getClassName(object);
}

inline long ZValPointerObject::getInternalHandle() const
{
    // If this value is not actually an object,
    // return zero.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return 0;

    zval* object = getFast();
    BOOST_ASSERT(object != NULL);
    return Z_OBJ_HANDLE_P(object);
}

inline bool ZValPointerObject::callInstanceMethod(const PHPExecEnvironment* phpExecEnv,
                                                  const char* methodName,
                                                  size_t methodNameLen,
                                                  const std::vector<ZValPointerAny>& params,
                                                  ZValPointerAny* retValPtr)
{
    if (!isValid<ZVal_Object>())
        return false;

    zval* object = getFast();
    BOOST_ASSERT(object != NULL);
    BOOST_ASSERT(methodName != NULL);

    return phpExecEnv->callInstanceMethod(retValPtr, object, methodName, methodNameLen, &params);
}

template <class t_PointerType>
t_PointerType ZValPointerObject::callInstanceMethod(const PHPExecEnvironment* phpExecEnv,
                                                    const char* methodName,
                                                    size_t methodNameLen,
                                                    const std::vector<ZValPointerAny>& params)
{
    ZValPointerAny returnValue;
    callInstanceMethod(phpExecEnv, methodName, methodNameLen, params, &returnValue);
    return returnValue.cast<t_PointerType>();
}

template <class t_PointerType>
t_PointerType ZValPointerObject::callInstanceMethod(const PHPExecEnvironment* phpExecEnv,
                                                    const char* methodName,
                                                    size_t methodNameLen)
{
    return callInstanceMethod<t_PointerType>(phpExecEnv, methodName, methodNameLen, std::vector<ZValPointerAny>());
}

template <class t_InternalObjectType>
inline t_InternalObjectType* ZValPointerObject::getInternalObject(const PHPExecEnvironment* phpExecEnv)
{
    // If this value is not actually an object,
    // return NULL.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return NULL;

    void* opaqueInternalObject = getInternalObjectFromZval(getFast());
    t_InternalObjectType* internalObject =
        reinterpret_cast<t_InternalObjectType*>(opaqueInternalObject);
    return internalObject;
}

#if PHP_VERSION_ID >= 70000
inline zend_object* ZValPointerObject::getZendObject() const
{
    if (!isValid<ZVal_Object>() || getFast() == NULL)
        return NULL;
    return Z_OBJ_P(getFast());
}

inline zend_resource* ZValPointerResource::getZendResource() const
{
    if(!isValid<ZVal_Resource>() || getFast() == NULL)
        return NULL;
    return Z_RES_P(getFast());
}
#endif

inline zend_object_handle ZValPointerObject::getObjectHandle() const
{
    // If this value is not actually an object,
    // return an invalid object handle.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return ILLEGAL_OBJECT_HANDLE;
    zval* const val = getFast();
    BOOST_ASSERT(val != NULL);
    return Z_OBJ_HANDLE_P(val);
}

inline ZValHashTable ZValPointerObject::getProperties() const
{
    // If this value is not actually an object,
    // return empty array?
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return ZValHashTable::share(NULL);
    zval* const val = getFast();
    BOOST_ASSERT(val != NULL);
    return ZValHashTable::share(Z_OBJPROP_P(val));
}

inline bool ZValPointerObject::readPropertyByName(const PHPExecEnvironment* phpExecEnv,
                                                  const char* name,
                                                  size_t nameLenInBytes,
                                                  ZValPointerAny* value) const
{
    // If this value is not actually an object,
    // return false ( no entry found ).
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return false;
    zval* const object = getFast();
    BOOST_ASSERT(object != NULL);
    zval* propertyValue = phpExecEnv->readPropertyByName(object, name, nameLenInBytes);
    if (!propertyValue)
        return false;

#if PHP_VERSION_ID >= 70000
    // some values are of type IS_INDIRECT. When such a value is found,
    // it means that the actual value is stored in some other location.
    // Luckily this location is propertyValue->value.zv which we are
    // dereferencing through the macro Z_INDIRECT_P
    if (Z_TYPE_P(propertyValue) == IS_INDIRECT)
        propertyValue = Z_INDIRECT_P(propertyValue);
#endif

    *value = ZValPointerAny::share(propertyValue);
    return true;
}

inline bool ZValPointerObject::findPropertyByName(const PHPExecEnvironment* phpExecEnv,
                                                  AccessLevel accessLevel,
                                                  const char* name,
                                                  size_t nameLenInBytes,
                                                  zend_class_entry* scope,
                                                  ZValPointerAny* value) const
{
    // If this value is not actually an object,
    // return false ( no entry found ).
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Object>())
        return false;
    zval* const object = getFast();
    BOOST_ASSERT(object != NULL);
    zval* propertyValue = phpExecEnv->getPropertyByName(object, accessLevel, name, nameLenInBytes, scope);
    if (!propertyValue)
        return false;

#if PHP_VERSION_ID >= 70000
    // some values are of type IS_INDIRECT. When such a value is found,
    // it means that the actual value is stored in some other location.
    // Luckily this location is propertyValue->value.zv which we are
    // dereferencing through the macro Z_INDIRECT_P
    if (Z_TYPE_P(propertyValue) == IS_INDIRECT)
        propertyValue = Z_INDIRECT_P(propertyValue);
#endif

    *value = ZValPointerAny::share(propertyValue);
    return true;
}

template <class t_ValueType>
inline t_ValueType ZValPointerObject::findPropertyByName(const PHPExecEnvironment* phpExecEnv,
                                                         const char* name,
                                                         size_t nameLenInBytes) const
{
    ZValPointerAny value;
    findPropertyByName(phpExecEnv, PHPExecEnvironment::PUBLIC, name, nameLenInBytes, NULL, &value);
    return value.cast<t_ValueType>();
}

template <class t_ValueType>
inline t_ValueType ZValPointerObject::findProtectedPropertyByName(const PHPExecEnvironment* phpExecEnv,
                                                                  const char* name,
                                                                  size_t nameLenInBytes) const
{
    ZValPointerAny value;
    findPropertyByName(phpExecEnv, PHPExecEnvironment::PROTECTED, name, nameLenInBytes, NULL, &value);
    return value.cast<t_ValueType>();
}

template <class t_ValueType>
inline t_ValueType ZValPointerObject::findPrivatePropertyByName(const PHPExecEnvironment* phpExecEnv,
                                                                const char* name,
                                                                size_t nameLenInBytes,
                                                                zend_class_entry* scope) const
{
    ZValPointerAny value;
    findPropertyByName(phpExecEnv, PHPExecEnvironment::PRIVATE, name, nameLenInBytes, scope, &value);
    return value.cast<t_ValueType>();
}

inline bool ZValPointerObject::isInstanceOf(const PHPExecEnvironment* phpExecEnv, zend_class_entry* targetClassEntry) const
{
    BOOST_ASSERT(targetClassEntry != NULL);
    zend_class_entry* classEntry = getClassEntry(phpExecEnv);
    if (!classEntry)
        return false;
    return phpExecEnv->isAncestorClass(targetClassEntry, classEntry);
}

inline zend_class_entry* ZValPointerObject::getClassEntry(const PHPExecEnvironment* phpExecEnv) const
{
    if (!isValid<ZVal_Object>())
        return NULL;
    zval* const object = getFast();
    BOOST_ASSERT(object != NULL);
    zend_class_entry* classEntry = phpExecEnv->getObjectValueClassEntry(object);
    return classEntry;
}

inline bool ZValPointerObject::getPrimitiveValue(const PHPExecEnvironment* phpExecEnv,
                                                 ZValPointerAny* value) const
{
    if (!isValid<ZVal_Object>())
        return false;
    zval* const object = getFast();
    BOOST_ASSERT(object != NULL);

    if (Z_OBJ_HT_P(object)->get) {
#if PHP_VERSION_ID >= 70000
        zval rv;
        zval* getValue = Z_OBJ_HT_P(object)->get(object, &rv);
#else
        zval* getValue = Z_OBJ_HT_P(object)->get(object TSRMLS_CC);
#endif
        *value = ZValPointerAny::take(getValue);
        return true;
    }

    return false;
}

inline ZValPointerObject::ZValPointerObject(const ZValPointerAny& other)
    : ZValPointer(other)
{
}

inline ZValPointerResource::ZValPointerResource(const ZValPointerResource& other)
    : ZValPointer(other)
{
}

inline ZValPointerResource::~ZValPointerResource()
{
}

inline ZValPointerResource& ZValPointerResource::operator=(const ZValPointerResource& other)
{
    ZValPointer::operator=(other);
    return *this;
}

#if PHP_VERSION_ID >= 70000
inline void* ZValPointerResource::getResource() const
#else
inline long ZValPointerResource::getResource() const
#endif
{
    // If this value is not actually a resource
    // return the ILLEGAL_RESOURCE sentinel.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Resource>())
#if PHP_VERSION_ID >= 70000
        return NULL;
#else
        return ILLEGAL_RESOURCE;
#endif
    zval* const val = getFast();
    BOOST_ASSERT(val != NULL);
#if PHP_VERSION_ID >= 70000
    return Z_RES_VAL_P(val); // void*
#else
    return Z_RESVAL_P(val); // long
#endif
}

#if PHP_VERSION_ID >= 70000
inline int ZValPointerResource::getResourceType() const
{
    if (!isValid<ZVal_Resource>())
        return ILLEGAL_RESOURCE;
    zval* const val = getFast();
    BOOST_ASSERT(val != NULL);
    return Z_RES_TYPE_P(val);
}
#endif

inline bool ZValPointerResource::isIdentical(const ZValPointerAny& other) const
{
    // If this value is not actually a resource
    // return false.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Resource>())
        return false;

    if (!other.cast<ZValPointerResource>())
        return false;

    zval* const myVal = getFast();
    BOOST_ASSERT(myVal != NULL);

    zval* const otherVal = other.get();
    BOOST_ASSERT(otherVal != NULL);

#if PHP_VERSION_ID >= 70000
    return (Z_RES_VAL_P(myVal) == Z_RES_VAL_P(otherVal));
#else
    // Z_LVAL_P identical to Z_RESVAL_P
    return (Z_LVAL_P(myVal) == Z_LVAL_P(otherVal));
#endif
}

template <class t_InternalResourceType, typename... t_expected>
inline t_InternalResourceType* ZValPointerResource::getInternalResource(t_expected... expectedTypes) const
{
    // If this value is not actually a resource,
    // return NULL.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Resource>())
        return NULL;

    int actualType;
    void* resource = getInternalResourceFromZval(getFast(), &actualType);
    if (!resource)
        return NULL;
    if (!resourceIsExpectedType(actualType, expectedTypes...))
        return NULL;
    return reinterpret_cast<t_InternalResourceType*>(resource);
}

template <typename... t_Args>
inline bool ZValPointerResource::resourceIsExpectedType(int actualType, int expectedType, t_Args...args) const
{
    if (actualType == expectedType)
        return true;
    return resourceIsExpectedType(actualType, args...);
}

inline bool ZValPointerResource::resourceIsExpectedType(int actualType) const
{
    return false;
}

inline php_stream* ZValPointerResource::getStream() const
{
    // If this value is not actually an object,
    // return the empty string.
    //
    // TODO create an exception policy
    if (!isValid<ZVal_Resource>())
        return NULL;

    // Logic from this point on should be in sync
    // with zend_fetch_resource in zend_list.c
    //
    // We don't just call php_stream_from_zval or zend_fetch_resource
    // because zend_fetch_resource print error messages if a resource
    // is not found or if the found resource was of the wrong type.
#if PHP_VERSION_ID >= 70000
    int resourceType = Z_RES_TYPE_P(getFast());
    void* resourceData = getResource();
#else
    long resourceID = getResource();
    int resourceType;
    void* resourceData = zend_list_find(resourceID, &resourceType);
#endif
    if (!resourceData)
        return NULL;
    if ((resourceType != php_file_le_stream()) && (resourceType != php_file_le_pstream()))
        return NULL;
    return reinterpret_cast<php_stream*>(resourceData);
}

inline ZValPointerResource::ZValPointerResource(const ZValPointerAny& other)
    : ZValPointer(other)
{
}

inline void* getInternalObjectFromZval(const zval* object)
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
#if PHP_VERSION_ID >= 70000
    // zval directly stores the object pointer, no need to look up in objects store.
    zend_object* obj = Z_OBJ_P(object);
    // the offset of the real object header
    int offset = obj->handlers->offset;
    return ((char*)obj) - offset;
#else
    return zend_object_store_get_object(const_cast<zval*>(object) TSRMLS_CC);
#endif
}

inline void* getInternalResourceFromZval(const zval* resource, int* resourceType)
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    BOOST_ASSERT(resourceType);
#if PHP_VERSION_ID >= 70000
    // get the type and assign to return parameter
    *resourceType = Z_RES_TYPE_P(resource);
    // zval directly stores the resource pointer
    return Z_RES_VAL_P(resource);
#else
    return zend_list_find(Z_RESVAL_P(resource), resourceType);
#endif
}

template <class t_ZValPointerClass, bool runtimeParamCountCheck>
t_ZValPointerClass PHPExecEnvironment::getParam(unsigned paramIndex) const
{
    if (runtimeParamCountCheck && (getParamCount() <= paramIndex))
        return ZValPointerAny().cast<t_ZValPointerClass>();
    BOOST_ASSERT(getParamCount() > paramIndex);
    ZValPointerAny zval(ZValPointerAny::share(getParamImpl(paramIndex)));
    return zval.cast<t_ZValPointerClass>();
}

#endif
