#ifndef __handle_tracking_exitpoint_h
#define __handle_tracking_exitpoint_h

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "agent.h"
#include "exitpoint.h"
#include "backend_resolver.h"
#include "current_exit_call.h"
#include "zval_helper.h"

class ZValPointerAny;

/* {{{ class AExitPointHandleInfo */

/**
    AHandleInfo that is used by handle tracking interceptors.
    Instances of this class can only be created via the static
    create methods.

    FIXME
    Refactor - split into an abstract and a concrete class, so that only
    concrete classes have create() methods.
 */
class AExitPointHandleInfo : public AHandleInfo
{
    template <class T, class Arg1, class... Args>
    friend boost::shared_ptr<T> boost::make_shared(Arg1 &&, Args &&...);

    friend class HandleRegistry;
public:
    /**
        @return A const reference to the ExitCallInfo associated with the
        registered handle.
     */
    inline const ExitCallInfo& getExitCallInfo() const { return m_exitCallInfo; }
    /**
        @return A mutable reference to the ExitCallInfo associated with the
        registered handle.
     */
    inline ExitCallInfo& getExitCallInfo() { return m_exitCallInfo; }

    inline void setExitCallInfo(const ExitCallInfo& exitCallInfo) { m_exitCallInfo = exitCallInfo; }

protected:
    inline AExitPointHandleInfo(const ExitCallInfo& exitCallInfo,
                                const TypeTag* const typeTag)
        : AHandleInfo(typeTag)
        , m_exitCallInfo(exitCallInfo)
    {
    }
    inline AExitPointHandleInfo(const appdynamics::pb::Agent::ExitPointType& type,
                                const TypeTag* const typeTag)
        : AHandleInfo(typeTag)
        , m_exitCallInfo(type)
    {
    }
private:
    ExitCallInfo m_exitCallInfo;
};

/* }}} */

/* {{{ class HandleBasedCurrentExitCall */

/**
 * A version of CurrentExitCall that is used by interceptors that support handle
 * tracking. Has a pointer to the handle info created on backend resolution, but
 * does not own it.
 */
class HandleBasedCurrentExitCall : public CurrentExitCall {
    public:
        inline HandleBasedCurrentExitCall(uint64_t sequenceNumber) : CurrentExitCall(sequenceNumber) { }

        inline HandleBasedCurrentExitCall(uint64_t sequenceNumber,
                                          const boost::shared_ptr<AExitPointHandleInfo>& handleInfo)
            : CurrentExitCall(sequenceNumber)
            , m_handleInfo(handleInfo)
        {
        }

        inline void setHandleInfo(const boost::shared_ptr<AExitPointHandleInfo>& handleInfo)
        {
            m_handleInfo = handleInfo;
        }

        inline const boost::shared_ptr<AExitPointHandleInfo>& getHandleInfo() const { return m_handleInfo; }

    private:
        boost::shared_ptr<AExitPointHandleInfo> m_handleInfo;
};

/* }}} */

/* {{{ template HandleInitExitPointInterceptor */

/**
    A template base class for exit call interceptors that act on functions that
    initialize an object or a handle that needs to be registered in the
    HandleRegistry.
    <p>
    Handles are registered with the HandleRegistry before the call
    to AExitCallInterceptor::onCallableEnd. This means that
    the handle will be registered during backend identification.
    <p>
    The template arguments are the derived interceptor class that needs handle
    tracking after initialization, and the base class that it would normally
    inherit from.
    <p>
    The template expect two methods to exit in the derived class:
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv);
        bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                            const ExitCallInfo* exitCallInfo,
                            const PHPExecEnvironment* execEnv) const;
    <p>
    getHandle() fills in the handlePointer with the resource/object to be used
    as the handle. initHandleInfo() is supposed to create and fill handleInfo
    with the appropriate structure derived from AExitPointHandleInfo. In cases
    where backend properties and display name have already been generated,
    exitCallInfo should be used instead of generating them again.
 */
template <class t_DerivedInterceptor, class t_BaseInterceptor = AExitCallInterceptor>
class HandleInitExitPointInterceptor : public t_BaseInterceptor
{
public:
    virtual void onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                               void* state);
protected:
    HandleInitExitPointInterceptor(const char* className,
                                   InterceptorRegistry::ID id,
                                   appdynamics::pb::Agent::ExitPointType type);

    virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                          const PHPExecEnvironment* execEnv) const;

    virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                   uint64_t sequenceNumber) const
    {
        return new HandleBasedCurrentExitCall(sequenceNumber);
    }

    virtual inline bool resolveBackendOnCallableBegin() const
    {
        return false;
    }

    virtual inline bool resolveBackendOnCallableEnd() const
    {
        return true;
    }
};

/* }}} */

/* {{{ class AHandleBasedExitPointInterceptor */
template <class t_DerivedInterceptor, class t_HandleInfo>
class AHandleBasedExitPointInterceptor : public AExitCallInterceptor
{
public:
    AHandleBasedExitPointInterceptor(const char* className,
                                     InterceptorRegistry::ID id,
                                     appdynamics::pb::Agent::ExitPointType type);

protected:
    virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                   uint64_t sequenceNumber) const;

    /**
        Virtual method that sub-classes can overload to control instantiation of the
        CurrentExitCall object.
        @param execEnv The current PHP execution environment.
        @param handle A zval that refers to the handle being tracked.
        @param handleInfo The handle information that is associated with the
        tracked handle.
        @return A new HandleBasedCurrentExitCall object.
     */
    virtual HandleBasedCurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                              uint64_t sequenceNumber,
                                                              const ZValPointerAny& handle,
                                                              const boost::shared_ptr<t_HandleInfo>& handleInfo) const;

    virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                          const PHPExecEnvironment* execEnv) const;

    virtual bool resolveBackendOnCallableBegin() const
    {
        return true;
    }

    /**
        Pure virtual method implemented by sub-classes to parse the php function
        call information ( class name, function name, parameters, object, etc. ) to
        find the zval for the handle which should be unregistered from the HandleRegistry.
        <p>
        @param handlePointer Pointer to a ZValPointerAny that must assigned by this
        method to the zval that contains the handle to be unregistered from the
        HandleRegistry.
        @param execEnv PHPExecEnvironment for the currently executing php program.
        @return true if the php function call information was successfully parsed,
        false otherwise.  If this methods returns false the ZValPointerAny referenced
        by handlePointer is ignored.
     */
    virtual bool getHandle(ZValPointerAny* handlePointer,
                           const PHPExecEnvironment* execEnv) const = 0;
};
/* }}} */

/* {{{ template HandleCleanupInterceptor */

/**
    An template base class for exit call interceptors that need to unregister
    a handle from the HandleRegistry in their onCallableEnd method.
 */
template <class t_DerivedInterceptor, class t_BaseClass = AMethodInterceptor>
class HandleCleanupInterceptor : public t_BaseClass
{
public:
    template <typename... t_Args>
    HandleCleanupInterceptor(t_Args...args);
    virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);
    virtual void onCallableEnd(const PHPExecEnvironment*, void*);

    virtual bool needParamsInOnMethodBegin() const { return true; }
    virtual bool shouldCallOnMethodEnd() const { return false; }
    virtual bool needReturnValueInOnMethodEnd() const { return false; };
};

/* }}} */

/* {{{ Template implementations */

template <class t_DerivedInterceptor, class t_BaseInterceptor>
void HandleInitExitPointInterceptor<t_DerivedInterceptor, t_BaseInterceptor>::onCallableEnd(
        const PHPExecEnvironment* phpExecEnv,
        void* state)
{
    const ExitCallInfo* exitCallInfo = NULL;
    HandleBasedCurrentExitCall* currentExitCall = reinterpret_cast<HandleBasedCurrentExitCall*>(state);
    if (currentExitCall)
        exitCallInfo = currentExitCall->getExitCallInfo();

    t_DerivedInterceptor* const derivedThis = static_cast<t_DerivedInterceptor*>(this);

    ZValPointerAny handleZVal;
    bool const gotHandle = derivedThis->getHandle(&handleZVal,
                                                  phpExecEnv);

    boost::shared_ptr<AExitPointHandleInfo> handleInfo;
    bool const didInitHandleInfo =
        derivedThis->initHandleInfo(&handleInfo,
                                    exitCallInfo,
                                    phpExecEnv);
    if (didInitHandleInfo)
    {
        // If we did not get a handle, then don't register the
        // handle info with the handle registry.  It will be
        // destroyed when this method finishes.
        //
        // We'll get into cases where we have a handle info and not a
        // handle when intercepting a call to a constructor that throws
        // an exception.
        //
        // In that case we still want to resolve the backend, time the call,
        // increase the call count of the backend, and report an error for the backed.
        if (gotHandle)
            phpExecEnv->getHandleRegistry().registerHandle(handleZVal, handleInfo);

        if (currentExitCall)
            currentExitCall->setHandleInfo(handleInfo);
    }

    // If we did not get a handle, don't call the base interceptor, just report
    // the errors (if there were any).
    if (gotHandle) {
        t_BaseInterceptor::onCallableEnd(phpExecEnv,
                                         state);
    } else {
        derivedThis->detectErrors(currentExitCall, phpExecEnv);
        derivedThis->cleanupExitCall(currentExitCall);
    }
}

template <class t_DerivedInterceptor, class t_BaseInterceptor>
ExitCallInfo HandleInitExitPointInterceptor<t_DerivedInterceptor, t_BaseInterceptor>::makeExitCallInfo(
        const CurrentExitCall* exitCall,
        const PHPExecEnvironment* execEnv) const
{
    const HandleBasedCurrentExitCall* handleExitCall = static_cast<const HandleBasedCurrentExitCall*>(exitCall);
    boost::shared_ptr<AExitPointHandleInfo> handleInfo = handleExitCall->getHandleInfo();
    /*
     * HandleInit- based interceptors that resolve in onCallableBegin, should
     * override this method with their own implementation. This default
     * implementation expects to be called only from onCallableEnd, in which
     * case the handleInfo will already be valid if it will ever be valid.
     */
    BOOST_ASSERT(!resolveBackendOnCallableBegin());
    if (!handleInfo)
        return ExitCallInfo();
    return ExitCallInfo(handleInfo->getExitCallInfo());
}


template <class t_DerivedInterceptor, class t_BaseInterceptor>
HandleInitExitPointInterceptor<t_DerivedInterceptor, t_BaseInterceptor>::HandleInitExitPointInterceptor(
        const char* className,
        InterceptorRegistry::ID id,
        appdynamics::pb::Agent::ExitPointType type)
    : t_BaseInterceptor(className, id, type)
{
}

template <class t_DerivedInterceptor, class t_BaseClass>
void* HandleCleanupInterceptor<t_DerivedInterceptor,
                               t_BaseClass>::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    ZValPointerAny handleZVal;
    const t_DerivedInterceptor* derivedThis = static_cast<const t_DerivedInterceptor*>(this);
    if (!derivedThis->getHandle(&handleZVal, phpExecEnv))
        return NULL;

    phpExecEnv->getHandleRegistry().unregisterHandle(handleZVal);
    return NULL;
}

template <class t_DerivedInterceptor, class t_BaseClass>
void HandleCleanupInterceptor<t_DerivedInterceptor,
                              t_BaseClass>::onCallableEnd(const PHPExecEnvironment*, void*)
{

}

template <class t_DerivedInterceptor, class t_BaseClass>
template <typename... t_Args>
HandleCleanupInterceptor<t_DerivedInterceptor,
                         t_BaseClass>::HandleCleanupInterceptor(t_Args...args)
    : t_BaseClass(args...)
{
}

template <class t_DerivedInterceptor, class t_HandleInfo>
CurrentExitCall* AHandleBasedExitPointInterceptor<t_DerivedInterceptor,
                                                  t_HandleInfo>::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                                       uint64_t sequenceNumber) const
{
    ZValPointerAny handleZVal;
    if (!getHandle(&handleZVal, execEnv))
        return NULL;

    boost::shared_ptr<t_HandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<t_HandleInfo>(handleZVal));

    // If this handle is not being tracked, then bail out.
    if (!handleInfo)
        return NULL;

    HandleBasedCurrentExitCall* exitCall = createCurrentExitCall(execEnv, sequenceNumber, handleZVal, handleInfo);
    return exitCall;
}

template <class t_DerivedInterceptor, class t_HandleInfo>
HandleBasedCurrentExitCall* AHandleBasedExitPointInterceptor<t_DerivedInterceptor,
                                                             t_HandleInfo>::createCurrentExitCall(const PHPExecEnvironment*,
                                                                                                  uint64_t sequenceNumber,
                                                                                                  const ZValPointerAny& handle,
                                                                                                  const boost::shared_ptr<t_HandleInfo>& handleInfo) const
{
    return new HandleBasedCurrentExitCall(sequenceNumber, handleInfo);
}

template <class t_DerivedInterceptor, class t_HandleInfo>
ExitCallInfo AHandleBasedExitPointInterceptor<t_DerivedInterceptor,
                                              t_HandleInfo>::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                                              const PHPExecEnvironment* execEnv) const
{
    BOOST_ASSERT(exitCall);
    const HandleBasedCurrentExitCall* handleExitCall = static_cast<const HandleBasedCurrentExitCall*>(exitCall);
    boost::shared_ptr<AExitPointHandleInfo> handleInfo = handleExitCall->getHandleInfo();
    return handleInfo->getExitCallInfo();
}

template <class t_DerivedInterceptor, class t_HandleInfo>
AHandleBasedExitPointInterceptor<t_DerivedInterceptor,
                                 t_HandleInfo>::AHandleBasedExitPointInterceptor(const char* className,
                                                                                 InterceptorRegistry::ID id,
                                                                                 appdynamics::pb::Agent::ExitPointType type)
    : AExitCallInterceptor(className, id, type)
{
}

/* }}} */

#endif

// vim: set fdm=marker:
