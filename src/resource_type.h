#ifndef __resource_type_h
#define __resource_type_h


/**
    Gets the resource type id for the specified resource
    type name.
    @param resouceTypeName Resource typename to lookup.
    @return The resource type id for the specified resource
    or 0 if no such resource type exists.
 */
template <const char* resouceTypeName>
int getResourceTypeId()
{
    static bool didLookup = false;
    static int typeId = 0;
    if (didLookup)
        return typeId;
    didLookup = true;
    typeId = zend_fetch_list_dtor_id(const_cast<char*>(resouceTypeName));
    return typeId;
}


#endif
