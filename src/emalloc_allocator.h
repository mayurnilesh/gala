/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __emalloc_allocator_h
#define __emalloc_allocator_h

#include <boost/type_traits/has_trivial_destructor.hpp>

#include <zend.h>

/**
    STL allocator template that uses emalloc/efree to allocate
    and free memory.  This allocator is designed to be used
    in containers instiated on the stack in functions that
    may never return due to php calling longjmp to tear up the
    call stack.

    This allocator may only be used with containers that store types
    with trivial destructors because the destructor for the container
    will never be called if the call stack is torn up by longjmp.
 */
template <class T>
class EMallocAllocator
{
public:
    // assert that T has a trivial destructor because
    // the whole point of using this allocator is to allow
    // a vector to be used on a call stack that is torn up
    // by longjmp.
    BOOST_STATIC_ASSERT(boost::has_trivial_destructor<T>::value);

    // type definitions
    typedef T        value_type;
    typedef T*       pointer;
    typedef const T* const_pointer;
    typedef T&       reference;
    typedef const T& const_reference;
    typedef std::size_t    size_type;
    typedef std::ptrdiff_t difference_type;

    // rebind allocator to type U
    template <class U>
    struct rebind
    {
        typedef EMallocAllocator<U> other;
    };

    // return address of values
    inline pointer address(reference value) const
    {
        return &value;
    }

    inline const_pointer address(const_reference value) const
    {
        return &value;
    }

    inline EMallocAllocator() throw()
    {
    }

    template <class U>
    inline EMallocAllocator(const EMallocAllocator<U>&) throw()
    {
    }

    inline size_type max_size() const throw()
    {
        return std::numeric_limits<std::size_t>::max() / sizeof(T);
    }

    inline pointer allocate(size_type num, const void* = 0) const
    {
        size_type const nBytes = num * sizeof(T);
        pointer ret = reinterpret_cast<pointer>(emalloc(nBytes));
        return ret;
    }

    inline void construct(pointer p, const T& value) const
    {
        new (p) T(value);
    }

    // Empty destroy method because we don't
    // allow this allocator to be used with types that have
    // non-trivial destructors.
    inline void destroy(pointer p)
    {
    }

    inline void deallocate(pointer p, size_type num) const
    {
        efree(p);
    }
};

// return that all specializations of this allocator are interchangeable
template <class T1, class T2>
bool operator==(const EMallocAllocator<T1>&, const EMallocAllocator<T2>&) throw()
{
   return true;
}
template <class T1, class T2>
bool operator!=(const EMallocAllocator<T1>&, const EMallocAllocator<T2>&) throw()
{
   return false;
}

#endif
