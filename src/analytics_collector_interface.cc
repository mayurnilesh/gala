/*
 * Copyright (c) AppDynamics, Inc., and its affiliates
 * 2015
 * All Rights Reserved
 */
#include analytics_collector_interface.h
#include "../agent/json/agent_json.h"
#include <iostream>

#define NULLABLE(x) ((x).empty() || (x) == "null" || (x) == "undefined" ? Json::Value::null : (x))

namespace appd {
namespace agent {

void AnalyticsCollectorInterface::recordTransaction(const TransactionInfo& txn)
{
  if (!enabled_) {
    return;
  }

  std::string json;
  buildTransactionJson(json, txn);
  buffer_.push_back(json);
}

void AnalyticsCollectorInterface::reportTransactions(std::string& analyticsPayload)
{
  if (!buffer_.empty()) {
    std::ostringstream json;
    bool print_seperator = false;

    json << "[";
    for (const std::string& str : buffer_) {
      if (print_seperator) {
        json << ", ";
      } else {
        print_seperator = true;
      }
      json << str;
    }
    json << "]";
    analyticsPayload = json.str();
    buffer_.clear();
  }
}

void AnalyticsCollectorInterface::buildTransactionJson(std::string& json,
                                                       const TransactionInfo& txn)
{
  const TransactionContext& ctx = txn.transactionContext_;
  const HttpData& httpData = txn.httpData_;
  const UserDataType& userData = txn.userData_;
  const Config& config = txn.config_;
  Json::Value root(Json::objectValue);

  root["eventTimestamp"] = ctx.eventTimestamp_;
  root["applicationId"] = NULLABLE(config.appId_);
  root["application"] = NULLABLE(config.appName_);
  root["requestGUID"] = NULLABLE(ctx.requestGUID_);
  root["transactionName"] = NULLABLE(ctx.transactionName_);
  root["transactionId"] = ctx.transactionId_;

  root["segments"][0]["segmentTimestamp"] = NULLABLE(ctx.eventTimestamp_);
  root["segments"][0]["tierId"] = NULLABLE(config.tierId_);
  root["segments"][0]["tier"] = NULLABLE(config.tierName_);
  root["segments"][0]["nodeId"] = NULLABLE(config.nodeId_);
  root["segments"][0]["node"] = NULLABLE(config.nodeName_);
  root["segments"][0]["requestExperience"] = NULLABLE(ctx.requestExperience_);
  root["segments"][0]["transactionTime"] = ctx.transactionTime_;
  root["segments"][0]["entryPoint"] = ctx.isEntryPoint_;
  root["segments"][0]["clientRequestGUID"] = NULLABLE(ctx.clientRequestGUID_);

  const ExitCallData& exitCalls = txn.exitCalls_;
  if (!exitCalls.empty()) {
    int index = 0;
    auto calls = Json::Value(Json::arrayValue);
    for (const ExitCallInfo& info : exitCalls) {
      calls[index]["exitCallType"] = NULLABLE(info.exitCallType_);
      calls[index]["avgResponseTimeMillis"] = info.avgResponseTimeMillis_;
      calls[index]["numberOfCalls"] = info.numberOfCalls_;
      calls[index]["numberOfErrors"] = info.numberOfErrors_;
      calls[index]["isSynchronous"] = info.isSynchronous_;
      calls[index]["isCustomExitCall"] = info.isCustomExitCall_;
      calls[index]["customExitCallDefinitionId"] = NULLABLE(info.customExitCallDefinitionId_);
      calls[index]["toEntity"]["entityId"] = NULLABLE(info.toEntityId_);
      calls[index]["toEntity"]["entityType"] = NULLABLE(info.toEntityType_);
      index++;
    }
    root["segments"][0]["exitCalls"] = calls;
  }

  root["segments"][0]["httpData"]["url"] = NULLABLE(httpData.url_);
  root["segments"][0]["httpData"]["principal"] = NULLABLE(httpData.principal_);
  root["segments"][0]["httpData"]["sessionId"] = NULLABLE(httpData.sessionId_);
  insertKeyValuePairs(root["segments"][0]["httpData"], "sessionObjects", httpData.sessionObjects_);
  insertKeyValuePairs(root["segments"][0]["httpData"], "uriPathSegments",
                      httpData.uriPathSegments_);
  insertKeyValuePairs(root["segments"][0]["httpData"], "parameters", httpData.parameters_);
  insertKeyValuePairs(root["segments"][0]["httpData"], "headers", httpData.headers_);
  insertKeyValuePairs(root["segments"][0]["httpData"], "cookies", httpData.cookies_);

  insertUserData(root["segments"][0], "userData", userData);

  try {
    json = root.toStyledString();
  } catch (Json::LogicError e) {
    // shouldn't ever get here as the JSON should be valid
    return;
  }
}

void AnalyticsCollectorInterface::insertKeyValuePairs(Json::Value& root,
                                                      const std::string& prop,
                                                      const KeyValuePair& map)
{
  for (const auto& it : map) {
    root[prop][it.first] = NULLABLE(it.second);
  }
}

void AnalyticsCollectorInterface::insertUserData(Json::Value& root,
                                                 const std::string& prop,
                                                 const UserDataType& map)
{
  for (const auto& it : map) {
    if (it.second.type() == typeid(std::string)) {
      root[prop][it.first] = NULLABLE(boost::get<std::string>(it.second));
    } else if (it.second.type() == typeid(double)) {
      root[prop][it.first] = boost::get<double>(it.second);
    } else if (it.second.type() == typeid(uint32_t)) {
      root[prop][it.first] = boost::get<uint32_t>(it.second);
    } else if (it.second.type() == typeid(int32_t)) {
      root[prop][it.first] = boost::get<int32_t>(it.second);
    } else if (it.second.type() == typeid(bool)) {
      root[prop][it.first] = boost::get<bool>(it.second);
    }
  }
}

void AnalyticsCollectorInterface::setEnabled(bool enabled)
{
  enabled_ = enabled;
}

}  // namespace agent
}  // namespace appd
