/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __call_graph_collection_state_h
#define __call_graph_collection_state_h

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#include "snapshot.h"
#include "services.h"
#include "always_inline.h"

class TransactionMonitor;

namespace CallGraph
{
    class ExecStackFrame;

    /**
        Hold state need by ExecStackFrame to build
        a call graph.  Methods of the ExecStackFrame
        class directly mutate fields of this class.
     */
    class GraphCollectionState : public IBailoutHandler, boost::noncopyable
    {
        friend class GraphCollectionStateChanger;
    public:
        GraphCollectionState(const Timer& timer);

        /**
            Resets the graph collection state back
            to the ground state.  All counters are set to zero
            and the snapshot pointer is set to the specified snapshot.

            @param snapshot Pointer to current snapshot, may be NULL.
         */
        void clear(Snapshot* snapshot);

        void onTransactionRestart(Snapshot* snapshot, uint64_t restartTime);

        /**
            Set the op_array that is the bottom of the execution stack.  The root op_array
            is used to compute the file name of the root script.
            @param rootOpArray The op_array for the root php script.
         */
        inline void setRootOpArray(const zend_op_array* rootOpArray) { m_rootOpArray = rootOpArray; }

        /**
            Used for logging.
            @return The number of call graph nodes that were *not* created,
         */
        inline size_t getNumberOfSkippedCallGraphNodes() const { return m_nSkippedCallGraphNodes; }

        /**
            @return the ExecStackFrame that is at the top of the stack.
         */
        inline ExecStackFrame* getTopStackFrame() const { return m_topExecStackFrame; }

        /**
            Called when the php call stack is about to be unwound an
            exit opcode, which does a longjmp under the hood.
         */
        void onExitFromCallable();

        /**
            Called when a fatal error occurs.  Fatal errors
            cause php to do a longjmp under the hood.  This function
            unwinds the ExecStackFrame stack if there is one.  This function must
            be called *before* the longjmp happens.  This function will create
            a call graph that better indicates where the fatal error occurred than
            afterBailout would do.
         */
        void onFatalError();

        /**
           Called after the call stack has been unwound by a call to zend_bailout.
         */
        void afterBailout();

        /**
            Adds snapshot exit call details to the call graph node for the ExecStackFrame
            that is at the top of the stack.
            @param snapshotExitCall snapshot exit call details to add.
         */
        void addSnapshotExitCall(const boost::shared_ptr<SnapshotExitCall>& snapshotExitCall);

        /**
            Sets the end time on all the nodes to the timestamp passed.
        */
        void onTransactionEnd(uint64_t restartTime);

        const Timer& getTimer() const { return m_timer; }
        inline Snapshot* getSnapshot() { return m_snapshot; }
        Graph& getCallGraph();
        const AgentLogger& getLogger();

        size_t getNDeferredNodes() const { return m_nDeferredNodes; }
        size_t getNSkippedCallGraphNodes() const { return m_nSkippedCallGraphNodes; }
        size_t getDepth() const { return m_depth; }

    private:
        const Timer& m_timer;
        size_t m_nDeferredNodes;
        size_t m_nSkippedCallGraphNodes;
        size_t m_depth;

        ExecStackFrame* m_topExecStackFrame;

        Snapshot* m_snapshot;

        const zend_op_array* m_rootOpArray;
    };

    /*
     * Holds changes to be done to the GraphCollectionState and applies them all
     * at once in the destructor. This is the only way to modify the collection
     * state.
     */
    class GraphCollectionStateChanger : boost::noncopyable {
        friend class ExecStackFrame;
        private:
            inline GraphCollectionStateChanger(GraphCollectionState* state)
                : m_state(state)
                , m_nDeferredNodesChanged(false)
                , m_nSkippedCallGraphNodesChanged(false)
                , m_depthChanged(false)
                , m_topExecStackFrameChanged(false)
                , m_nDeferredNodes(state->m_nDeferredNodes)
                , m_nSkippedCallGraphNodes(state->m_nSkippedCallGraphNodes)
                , m_depth(state->m_depth)
                , m_topExecStackFrame(state->m_topExecStackFrame)
            { }

            inline size_t getNDeferredNodes() const { return m_nDeferredNodes; }
            inline size_t getNSkippedCallGraphNodes() const { return m_nSkippedCallGraphNodes; }
            inline size_t getDepth() const { return m_depth; }
            inline ExecStackFrame* getTopExecStackFrame() const { return m_topExecStackFrame; }

            inline const Timer& getTimer() const { return m_state->getTimer(); }
            inline Snapshot* getSnapshot() { return m_state->getSnapshot(); }
            inline Graph& getCallGraph() { return m_state->getCallGraph(); }
            inline const AgentLogger& getLogger() { return m_state->getLogger(); }

            void addNDeferredNodes(int32_t delta);
            void addNSkippedCallGraphNodes(int32_t delta);
            void addDepth(int32_t delta);
            void setTopExecStackFrame(ExecStackFrame* stackFrame);

            inline const zend_op_array* getRootOpArray() const { return m_state->m_rootOpArray; }

            ~GraphCollectionStateChanger();

            GraphCollectionState* m_state;

            bool m_nDeferredNodesChanged;
            bool m_nSkippedCallGraphNodesChanged;
            bool m_depthChanged;
            bool m_topExecStackFrameChanged;

            size_t m_nDeferredNodes;
            size_t m_nSkippedCallGraphNodes;
            size_t m_depth;
            ExecStackFrame* m_topExecStackFrame;
    };

    inline Graph& GraphCollectionState::getCallGraph()
    {
        BOOST_ASSERT(m_snapshot != NULL);
        return m_snapshot->getCallGraph();
    }

    inline const AgentLogger& GraphCollectionState::getLogger()
    {
        BOOST_ASSERT(m_snapshot != NULL);
        return m_snapshot->getLogger();
    }

    inline void GraphCollectionState::clear(Snapshot* snapshot)
    {
        m_nDeferredNodes = 0;
        m_nSkippedCallGraphNodes = 0;
        m_depth = 0;
        m_topExecStackFrame = NULL;
        m_snapshot = snapshot;
    }

    inline void GraphCollectionStateChanger::addDepth(int32_t delta)
    {
        m_depth += delta;
        m_depthChanged = true;
    }

    inline void GraphCollectionStateChanger::addNDeferredNodes(int32_t delta)
    {
        m_nDeferredNodes += delta;
        m_nDeferredNodesChanged = true;
    }

    inline ALWAYS_INLINE_IN_RELEASE void GraphCollectionStateChanger::addNSkippedCallGraphNodes(int32_t delta)
    {
        m_nSkippedCallGraphNodes += delta;
        m_nSkippedCallGraphNodesChanged = true;
    }

    inline void GraphCollectionStateChanger::setTopExecStackFrame(ExecStackFrame* stackFrame)
    {
        m_topExecStackFrame = stackFrame;
        m_topExecStackFrameChanged = true;
    }

    inline ALWAYS_INLINE_IN_RELEASE GraphCollectionStateChanger::~GraphCollectionStateChanger()
    {
        if (m_depthChanged)
            m_state->m_depth = m_depth;
        if (m_nDeferredNodesChanged)
            m_state->m_nDeferredNodes = m_nDeferredNodes;
        if (m_nSkippedCallGraphNodes)
            m_state->m_nSkippedCallGraphNodes = m_nSkippedCallGraphNodes;
        if (m_topExecStackFrameChanged)
            m_state->m_topExecStackFrame = m_topExecStackFrame;
    }
}

#endif
