/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __call_graph_h
#define __call_graph_h

#include <boost/utility.hpp>
#include <boost/pool/object_pool.hpp>
#include "call_graph_node_type.h"
#include "call_graph_node.h"

#include "PHPAgentProtobufs.pb.h"

class Timer;

namespace CallGraph
{
    class Node;
    class NodeName;

    /**
        Stores a call graph.
     */
    class Graph : public boost::noncopyable
    {
    public:
        inline Graph()
            : m_lastRoot(NULL)
            , m_nodesCount(0)
        {
        }

        ~Graph();

        /**
            Creates a new call graph Node.  This method increments the count of nodes
            in the call graph before returning and thus assumes any returned Node will
            eventually be reachable from the root node of the graph.
            <p>
            @param name Pointer to a NodeName for the new node whose fields moved
            into a NodeName embedded in the new Node.  In other words the class name and
            method name in the specified NodeName will be null after this method returns.
            @param type Type of call that caused the function associated with the new node
            to be executed.
            @param callElementType The call element type for the function associated
            with the new node.
            @param callerFileName Name of the file containing the function that called the
            function associated with the new Node.
            @param callLineNumber Number of the line that contains the expression or statement
            that called the function associated with the new Node.
            @return A new Node which must eventually be added to the Graph such that it is
            reachable from the root node of the Graph.
         */
        Node* newNode(NodeName* name,
                      NodeType type,
                      appdynamics::pb::CallElement_Type callElementType,
                      const char* callerFileName,
                      unsigned callerLineNumber,
                      uint64_t startTime);

        inline Node* getLastRoot() const { return m_lastRoot; }

        void addRoot(Node* root);

        size_t getNodesCount() const { return m_nodesCount; }

        void debugPrint(const Timer& timer) const;

    private:
        boost::object_pool<Node> m_nodePool;
        Node* m_lastRoot;
        size_t m_nodesCount;
    };

    inline void Graph::addRoot(Node* root)
    {
        root->m_nextSibling = m_lastRoot;
        m_lastRoot = root;
    }
}

#endif
