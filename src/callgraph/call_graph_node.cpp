/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/

#include "call_graph_node.h"
#include "call_graph_node_name.h"

namespace CallGraph
{
    Node::Node(const ConstructorParameterWad& wad)
        : m_type(wad.type)
        , m_hasExecutionTime(false)
        , m_callElementType(wad.callElementType)
        , m_callerFileName(wad.callerFileName)
        , m_callerLineNumber(wad.callerLineNumber)
        , m_startTime(wad.startTime)
        , m_nextSibling(NULL)
        , m_firstCalledNode(NULL)
        , m_lastCalledNode(NULL)
        , m_calledNodesCount(0)
    {
        NodeName::move(&m_name, wad.name);
    }
}

namespace std
{
    ostream& operator<<(ostream& out, CallGraph::Node& node)
    {
        out << node.getName();
        const char* const callerFileName = node.getCallerFilename();
        if (callerFileName)
            out << " @ " << callerFileName << ":" << node.getCallerLineNumber();
        return out;
    }
}
