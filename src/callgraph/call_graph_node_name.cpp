/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#include "call_graph_node_name.h"

namespace std
{
    ostream& operator<<(ostream& out, const CallGraph::NodeName& name)
    {
        const char* const className = name.getClassName();
        if (className)
            out << className << "::";
        out << name.getMethodName();
        return out;
    }
}

