/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __call_graph_node_name_h
#define __call_graph_node_name_h

#include <boost/utility.hpp>
#include <ostream>

#include "call_graph_node_type.h"
#include "intercept.h"

class PHPExecEnvironment;

namespace CallGraph
{
    class NodeName;
}

namespace std
{
    // For logging and debugging only.
    ostream& operator<<(ostream&, const CallGraph::NodeName&);
}

namespace CallGraph
{
    class Node;
    class ExecStackFrame;

    /**
        Class to hold the name of a call graph node, which consists of a
        class name and method name.  The class name may be null.
        <p>
        This class may not be copied, instead its contents are transfered
        with the static move method below.  The move method should only
        be called by the constructor of the Node class.
     */
    class NodeName : boost::noncopyable
    {
        // calls the private static move method to fill in the
        // fields of an instance of this class embedded in the Node class.
        friend class Node;
        // Fills in the fields of this class.
        friend class ExecStackFrame;

        //friend class ExecStackFrame;
    public:
        NodeName();
        ~NodeName();

        const char* getClassName() const;
        const char* getMethodName() const;

    private:
        static void move(NodeName* dest, NodeName* src);

        const char* m_className;
        const char* m_methodName;
        bool m_freeClassName : 1;
        bool m_freeMethodName : 1;
    };

    inline NodeName::NodeName()
        : m_className(NULL)
        , m_methodName(NULL)
        , m_freeClassName(false)
        , m_freeMethodName(false)
    {
    }

    inline NodeName::~NodeName()
    {
        if (m_freeClassName)
            efree(const_cast<char*>(m_className));
        if (m_freeMethodName)
            efree(const_cast<char*>(m_methodName));
    }

    inline void NodeName::move(NodeName* dest, NodeName* src)
    {
        dest->m_className = src->m_className;
        dest->m_methodName = src->m_methodName;
        dest->m_freeClassName = src->m_freeClassName;
        dest->m_freeMethodName = src->m_freeMethodName;

        // Now clear out the original name.
        src->m_className = NULL;
        src->m_methodName = NULL;
        src->m_freeClassName = false;
        src->m_freeMethodName = false;
    }

    inline const char* NodeName::getClassName() const
    {
        return m_className;
    }

    inline const char* NodeName::getMethodName() const
    {
        return m_methodName;
    }
}


#endif
