/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/

#include "call_graph_collection_state.h"
#include "exec_stack_frame.h"
#include "transactions.h"

namespace CallGraph
{
    GraphCollectionState::GraphCollectionState(const Timer& timer)
        : m_timer(timer)
        , m_nDeferredNodes(0)
        , m_nSkippedCallGraphNodes(0)
        , m_depth(0)
        , m_topExecStackFrame(NULL)
        , m_snapshot(NULL)
        , m_rootOpArray(NULL)
    {
    }

    void GraphCollectionState::onExitFromCallable()
    {
        // There should be at least one frame on the exec frame stack when this
        // method is called.
        BOOST_ASSERT(m_topExecStackFrame != NULL);
        m_topExecStackFrame->onExitFromCallable(*this);
    }

    void GraphCollectionState::onFatalError()
    {
        // If a fatal error is routed to our error callback
        // while there are no php call frames on the stack
        // then we can get in here when the exec frame stack
        // is empty.
        if (m_topExecStackFrame)
            m_topExecStackFrame->onExitFromCallable(*this);
    }

    void GraphCollectionState::afterBailout()
    {
        // onFatalError might have already unwound the exec frame
        // stack.
        if (!m_topExecStackFrame)
            return;

        if (!getSnapshot())
            return;

        if (!(getSnapshot()->isCallGraphEnabled()))
            return;

        BOOST_ASSERT(m_depth > 0);

        uint64_t endStamp = m_timer.getTimestamp();
        Graph& graph = getCallGraph();

        // The call graph may contain nodes that have not
        // yet had their execution time set.  These are
        // the call graph nodes that have been created, but whose
        // corresponding ExecStackFrame object will never have its
        // onCallableEnd or onExitFromCallable called because php
        // already used longjmp to tear of the call stack.
        // These nodes are all under the last root node in the call graph
        // and are always that last child of their parent node.
        Node* node = graph.getLastRoot();
        while ((node) && (!(node->hasExecutionTime())))
        {
            uint64_t const nodeStartTime = node->getStartTime();
            if (endStamp < nodeStartTime)
            {
                LOG4CXX_ERROR(getLogger(), "Bad execution time after bailout: "
                                                 << nodeStartTime
                                                 << " "
                                                 << endStamp
                                                 << " "
                                                 << node->getName());
            }
            uint64_t const elapsedTimeInMicros =
                m_timer.getElapsedTime(node->getStartTime(), endStamp);
            unsigned long const elapsedTimeInMillis =
                elapsedTimeInMicros / 1000;
            node->setExecutionTime(elapsedTimeInMillis);

            // Traverse to the last child of the current node.
            // We could make this faster by adding an accessor method
            // for the last child node ( which CallGraph::Node's keep in a member variable),
            // but this code should not be hot.
            node = node->getFirstCalledNode();
            while ((node) && (node->getNextSiblingNode()))
            {
                BOOST_ASSERT(node->hasExecutionTime());
                node = node->getNextSiblingNode();
            }
        }
        m_nSkippedCallGraphNodes += m_nDeferredNodes;
        m_nDeferredNodes = 0;
        m_depth = 0;
        m_topExecStackFrame = NULL;
    }

    void GraphCollectionState::addSnapshotExitCall(const boost::shared_ptr<SnapshotExitCall>& snapshotExitCall)
    {
        // Normally we'll always have at least one frame on the execution
        // stack when we get in here, but if the call graph fills up and
        // the program does an exit, and it has a shutdown call back registered,
        // we can end up in here when m_topExecStackFrame is null.
        if (!m_topExecStackFrame)
            return;

        m_topExecStackFrame->addSnapshotExitCall(*this, snapshotExitCall);
    }

    void GraphCollectionState::onTransactionRestart(Snapshot* snapshot, uint64_t restartTime)
    {
        ExecStackFrame* currentFrame = m_topExecStackFrame;

        if (!currentFrame)
            return;

        size_t count = 0;
        // The following is to avoid a warning in the release build.
        (void) count;

        do {
            currentFrame->setStartTimestamp(restartTime);
            currentFrame->convertNodeToName();
            currentFrame = currentFrame->getCallerFrame();
            count++;
        } while (currentFrame != NULL);

        BOOST_ASSERT(count == m_depth);
        m_nDeferredNodes = m_depth;

        m_snapshot = snapshot;
    }

    void GraphCollectionState::onTransactionEnd(uint64_t currentTime)
    {
        ExecStackFrame* currentFrame = m_topExecStackFrame;

        BOOST_ASSERT(currentFrame);

        do
        {
            if (currentFrame->hasCallGraphNode())
                currentFrame->computeAndSetExecutionTime(*this, getTimer(), currentTime);
            currentFrame = currentFrame->getCallerFrame();
        } while (currentFrame != NULL);
    }
}
