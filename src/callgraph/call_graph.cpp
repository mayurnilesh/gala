/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#include "call_graph.h"
#include "call_graph_node.h"
#include <iostream>
#include <iomanip>

namespace CallGraph
{
    Graph::~Graph()
    {
    }

    Node* Graph::newNode(NodeName* name,
                         NodeType type,
                         appdynamics::pb::CallElement_Type callElementType,
                         const char* callerFileName,
                         unsigned callerLineNumber,
                         uint64_t startTime)
    {
        ++m_nodesCount;
        Node::ConstructorParameterWad parameterWad = {
            name,
            type,
            callElementType,
            callerFileName,
            callerLineNumber,
            startTime
        };
        return m_nodePool.construct<const Node::ConstructorParameterWad&>(parameterWad);
    }

    /*
     * The next 2 functions are to support debugPrint(), which is generally not
     * used, but can be invoked to produce a pretty call graph output.
     */
    static std::string str_last_n_segs(const std::string& str, int segments)
    {
        int idx = str.length()+1;
        if (segments == 0) {
            return NULL;
        }
        while (idx > 0 && segments-- > 0)
        {
            idx = str.rfind('/', idx-1);
        }

        return str.substr(idx+1);
    }

    static void printNode(const Timer& timer, const Node* node, int indent)
    {
        if (!node) {
            return;
        }

        std::cout << std::setw(4) << node->getExecutionTime() << "ms ";
        if (node->getCallerFilename()) {
            std::cout << std::setw(25) << str_last_n_segs(node->getCallerFilename(), 2) << ":" << std::left << std::setw(4) << node->getCallerLineNumber() << "  ";
            std::cout << std::resetiosflags(std::ios::left);
        }
        std::cout << std::string(indent, ' ');
        bool const isInternal =
            node->getCallElementType() == appdynamics::pb::CallElement_Type_INTERNAL;
        if (isInternal) {
            std::cout << "[";
        }
        if (node->getName().getClassName()) {
            std::cout << node->getName().getClassName() << "::";
        }
        std::cout << node->getName().getMethodName();
        if (isInternal) {
            std::cout << "]";
        }
        std::cout << std::endl;
        const Node* calledNode = node->getFirstCalledNode();
        while (calledNode != NULL) {
            printNode(timer, calledNode, indent+2);
            calledNode = calledNode->getNextSiblingNode();
        }
    }

    void Graph::debugPrint(const Timer& timer) const
    {
        if (!m_lastRoot)
            return;
        const Node* root = m_lastRoot;
        std::cout << std::string(20, '-') << std::endl;
        while (root) {
            int indent = 0;
            std::cout << std::string(20, '-') << std::endl;
            printNode(timer, root, indent);
            root = root->getNextSiblingNode();
            if (!root)
                std::cout << std::string(20, '-') << std::endl;
        }
        std::cout << std::string(20, '-') << std::endl;

    }

}
