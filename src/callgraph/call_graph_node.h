/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __call_graph_node_h
#define __call_graph_node_h

#include <boost/utility.hpp>
#include <boost/pool/object_pool.hpp>
#include <vector>
#include <ostream>

#include "call_graph_node_type.h"
#include "call_graph_node_name.h"
#include "timer.h"

#include "PHPAgentProtobufs.pb.h"



class SnapshotExitCall;

namespace CallGraph
{
    class Node;
}

namespace CallGraph
{
    class Graph;

    /**
        A node in a call graph.
        <p>
        This class has an intrusive singly linked
        list run through it such that each Node has a list of
        child nodes that represents the list of functions called by the function
        for this Node.  boost::intrusive::slist could not be used in the class
        because instances of this class are allocated from a boost::object_pool, which
        really wants to destroy all the Node object in the pool in a random order when the
        pool is destroyed.
     */
    class Node : boost::noncopyable
    {
        // Allows the boost::object_pool to access
        // the private constructor below.
        friend class boost::object_pool<Node>;

        // Allows Graph to set the m_nextSibling field
        friend class Graph;

        // Allows ExecStackFrame to get the node name, type, and caller type.
        friend class ExecStackFrame;

        public:

            inline const NodeName& getName() const { return m_name; }

            inline NodeType getType() const { return m_type; }

            inline appdynamics::pb::CallElement_Type getCallElementType() const { return m_callElementType; }

            inline const char* getCallerFilename() const { return m_callerFileName; }

            inline unsigned getCallerLineNumber() const { return m_callerLineNumber; }

            inline void setExecutionTime(unsigned executionTimeInMS)
            {
                BOOST_ASSERT(!hasExecutionTime());
                m_hasExecutionTime = true;
                m_executionTimeInMS = executionTimeInMS;
            }

            inline unsigned getExecutionTime() const
            {
                BOOST_ASSERT(hasExecutionTime());
                return m_executionTimeInMS;
            }

            inline bool hasExecutionTime() const
            {
                return m_hasExecutionTime;
            }

            inline uint64_t getStartTime() const
            {
                BOOST_ASSERT(!hasExecutionTime());
                return m_startTime;
            }

            /**
                Adds a Node for a function that is called by the function represented by
                this Node in the call graph.
                @param callNode The node for the function that was called.
             */
            inline void addCalledNode(Node* calledNode)
            {
                if (m_lastCalledNode)
                    m_lastCalledNode->m_nextSibling = calledNode;
                m_lastCalledNode = calledNode;
                if (!m_firstCalledNode)
                    m_firstCalledNode = calledNode;
                ++m_calledNodesCount;
            }

            inline size_t getCalledNodesCount() const
            {
                return m_calledNodesCount;
            }

            inline const Node* getFirstCalledNode() const
            {
                return m_firstCalledNode;
            }

            inline Node* getFirstCalledNode()
            {
                return m_firstCalledNode;
            }

            inline const Node* getNextSiblingNode() const
            {
                return m_nextSibling;
            }

            inline Node* getNextSiblingNode()
            {
                return m_nextSibling;
            }

            inline void addSnapshotExitCall(const boost::shared_ptr<SnapshotExitCall>& snapshotExitCall)
            {
                m_exitCalls.push_back(snapshotExitCall);
            }

            inline const std::vector< boost::shared_ptr<SnapshotExitCall> >& getSnapshotExitCalls() const { return m_exitCalls; }

        private:

            struct ConstructorParameterWad
            {
                NodeName* name;
                NodeType type;
                appdynamics::pb::CallElement_Type callElementType;
                const char* callerFileName;
                unsigned callerLineNumber;
                uint64_t startTime;
            };

            Node(const ConstructorParameterWad& wad);

            NodeName m_name;
            NodeType const m_type : 3;
            bool m_hasExecutionTime : 1;
            appdynamics::pb::CallElement_Type const m_callElementType : 4;

            const char* const m_callerFileName;
            unsigned const m_callerLineNumber;

            union
            {
                unsigned m_executionTimeInMS;
                uint64_t m_startTime;
            };

            Node* m_nextSibling;
            Node* m_firstCalledNode;
            Node* m_lastCalledNode;
            size_t m_calledNodesCount;
            std::vector<boost::shared_ptr<SnapshotExitCall>> m_exitCalls;
    };
}

#endif
