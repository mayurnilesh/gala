/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __call_graph_node_type_h
#define __call_graph_node_type_h

namespace CallGraph
{
    /**
        Enum to model the type of call that caused a function to be executed.
     */
    enum NodeType
    {
        CALL_NOT_COMPUTED,
        CALL_UNKNOWN,
        CALL_NORMAL,
        CALL_INCLUDES,
        CALL_EVAL
    };
}

#endif
