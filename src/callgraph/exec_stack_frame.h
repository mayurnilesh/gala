/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#ifndef __exec_stack_frame_h
#define __exec_stack_frame_h

#include <stdint.h>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include "agent.h"
#include "always_inline.h"
#include "agent_logger.h"
#include "timer.h"
#include "transactions.h"

#include "call_graph_collection_state.h"
#include "call_graph_node_type.h"
#include "call_graph_node_name.h"
#include "call_graph_node.h"

#include "PHPAgentProtobufs.pb.h"

#include "Zend/zend_closures.h"

class SnapshotExitCall;

namespace CallGraph
{

    class Node;

    /**
        Class designed to be instantiated on the stack in the main execution handler.
        This class threads a singly linked list through the stack.  Each entry in the
        list represents a function call frame to be added to a CallGraph::Graph if
        the function call executed longer than a specified number of milliseconds.
     */
    class ExecStackFrame : boost::noncopyable
    {
        friend class CallGraph::GraphCollectionState;
    private:
        static const size_t s_maxCallGraphNodes = 5000;
        static const size_t s_maxDepth = 100;

        BOOST_STATIC_ASSERT(s_maxCallGraphNodes > 0);
        BOOST_STATIC_ASSERT(s_maxDepth > 0);
    public:
        ExecStackFrame(GraphCollectionState& state, const zend_execute_data* execData, bool isInternal);
        ~ExecStackFrame();

        /**
            Called when the PHP function for this frame has been returned.
            This method will create a CallGraph::Node if needed and hook it
            up to the CallGraph::Graph.
         */
        void onCallableEnd(GraphCollectionState& state);

        /**
            Called when the stack is about to be unwound by an exit opcode,
            which does a longjmp under the hood.
            <p>
            This method will create CallGraph::Node's for all
            frames on the stack that don't already have a CallGraph::Node and that
            have been currently executing for longer than the minimum call time.
         */
        void onExitFromCallable(GraphCollectionState& state);

        const NodeName& getNodeName();

        /**
            Called immediately before the PHP function for this frame has stated
            execution.
         */
        void startTimer(GraphCollectionState& state);

        /**
            @return The ExecStackFrame for the php function called the PHP function
            associated with this frame.
         */
        ExecStackFrame* getCallerFrame() const;

        /**
            Adds snapshot exit called details for the PHP function associated with the
            current frame if the exit call took longer than the minimum call time
            and if a Node.
         */
        void addSnapshotExitCall(GraphCollectionState& state,
                                 const boost::shared_ptr<SnapshotExitCall>& snapshotExitCall);

        /**
            @return The type of function call that created this frame.
         */
        NodeType getCallType() const;

        /**
            Sets the call element type of the PHP function associated with the current
            frame.
            @param type The call element type.
         */
        void setCallElementType(appdynamics::pb::CallElement_Type type);

        bool hasCallGraphNode() const;

        void convertNodeToName();

    private:
        Node* createCallGraphNode(GraphCollectionStateChanger& stateChanger);
        void createDeferredCallGraphNodes(GraphCollectionStateChanger& stateChanger, bool skipCreatedNodes);

        /**
            Compute the line and file number of a the call instruction that caused
            this ExecStackFrame to be created.
         */
        void computeLocation(const GraphCollectionStateChanger& state, const char** filename, unsigned* lineno) const;

        /**
            Compute the name of the function associated with this
            ExecStackFrame.
         */
        void computeNodeName(NodeName* name, NodeType* type) const;

        void computeAndSetExecutionTime(GraphCollectionState& state,
                                        const Timer& timer,
                                        uint64_t endStamp);

        inline void setStartTimestamp(uint64_t stamp) { m_startStamp = stamp; }

        NodeName* getOnStackNodeNamePtr() const;
        NodeType getOnStackCallType() const;

        uint64_t m_startStamp;
        union
        {
            Node* m_callGraphNode;
            struct
            {
                mutable uint8_t m_nodeNameBytes[sizeof(NodeName)];
                mutable NodeType m_callType : 3;
                appdynamics::pb::CallElement_Type m_callElementType : 4;
            } m_onStack;
        };
        ExecStackFrame* m_callerFrame;
        const zend_execute_data* const m_executeData;
        bool m_callNodeAllocated;
    };

    inline ALWAYS_INLINE_IN_RELEASE ExecStackFrame::ExecStackFrame(GraphCollectionState& state, const zend_execute_data* executeData, bool isInternal)
        : m_startStamp(0)
        , m_executeData(executeData)
    {
        GraphCollectionStateChanger stateChanger(&state);

        m_callNodeAllocated = false;
        new (m_onStack.m_nodeNameBytes) NodeName();
        m_onStack.m_callType = CALL_NOT_COMPUTED;

        if (!stateChanger.getSnapshot())
            return;

        if (!(stateChanger.getSnapshot()->isCallGraphEnabled()))
            return;

        stateChanger.addDepth(1);
        BOOST_ASSERT(stateChanger.getDepth() != 0);
        size_t const callGraphNodesCount = stateChanger.getCallGraph().getNodesCount();
        size_t const totalNodesCount = stateChanger.getNDeferredNodes() + callGraphNodesCount + 1;

        // We are too deep...
        if (stateChanger.getDepth() > s_maxDepth)
        {
            stateChanger.addNSkippedCallGraphNodes(1);
            return;
        }

        // The call graph is full.
        if (totalNodesCount > s_maxCallGraphNodes)
        {
            stateChanger.addNSkippedCallGraphNodes(1);
            return;
        }
        stateChanger.addNDeferredNodes(1);

        m_onStack.m_callElementType =
            isInternal  ? appdynamics::pb::CallElement_Type_INTERNAL
                        : appdynamics::pb::CallElement_Type_POPO;

        // This frame is right at the limit of the call graph.
        // Nothing this frame calls will be recorded.
        if (stateChanger.getDepth() == s_maxDepth)
        {
            const NodeName& nodeName = getNodeName();
            const char* callerFileName;
            unsigned callerLineNumber;
            computeLocation(stateChanger, &callerFileName, &callerLineNumber);
            LOG4CXX_INFO(stateChanger.getLogger(), "callgraph depth limit ["
                << s_maxDepth
                << "] reached at ["
                << nodeName << " @ "
                << callerFileName
                << ":"
                << callerLineNumber
                << "], stopping collection on this branch");
        }

        m_callerFrame = stateChanger.getTopExecStackFrame();

        // When a callgraph node is created for this frame, the
        // call graph will be full.
        if (totalNodesCount == s_maxCallGraphNodes)
        {
            const NodeName& nodeName = getNodeName();
            const char* callerFileName;
            unsigned callerLineNumber;
            computeLocation(stateChanger, &callerFileName, &callerLineNumber);
            LOG4CXX_INFO(stateChanger.getLogger(), "max callgraph nodes limit ["
                << s_maxCallGraphNodes
                << "] reached at ["
                << nodeName << " @ "
                << callerFileName
                << ":"
                << callerLineNumber
                << "], stopping call graph collection");
            // Force the last call graph node to be created, so we really do
            // stop callgraph collection at this point.
            createDeferredCallGraphNodes(stateChanger, false);
        }

        // Ensure we compute the node name for __call, since the zend_function
        // it's invoked through will be destroyed by the time we get to it in
        // onCallableEnd()
        if (m_executeData &&
                (APPD_EXECDATA_ZEND_FUNC(m_executeData)->common.fn_flags & ZEND_ACC_CALL_VIA_HANDLER)) {
            getOnStackNodeNamePtr();
        }

        stateChanger.setTopExecStackFrame(this);
    }

    inline ALWAYS_INLINE_IN_RELEASE ExecStackFrame::~ExecStackFrame()
    {
        if (!hasCallGraphNode())
            reinterpret_cast<NodeName*>(m_onStack.m_nodeNameBytes)->~NodeName();
    }

    inline ALWAYS_INLINE_IN_RELEASE void ExecStackFrame::onCallableEnd(GraphCollectionState& state)
    {
        GraphCollectionStateChanger stateChanger(&state);

        if (!stateChanger.getSnapshot())
            return;

        if (!(stateChanger.getSnapshot()->isCallGraphEnabled()))
            return;

        BOOST_ASSERT(stateChanger.getDepth() > 0);
        stateChanger.addDepth(-1);

        // We need to check to see if this ExecStackFrame
        // is at the top of the stack.  If this frame is not
        // at the top of the stack then we'll just bail out of here because that
        // indicates one or more of the following is true:
        // 1.  This ExecStackFrame is for a php frame is too deep in the call stack
        // to be recorded in the CallGraph::Graph
        // 2.  This ExecStackFRame is for a php frame that started after the
        // CallGraph::Graph was full.
        if (stateChanger.getTopExecStackFrame() != this)
            return;

        boost::shared_ptr<TransactionMonitor> txMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor();
        TransactionContext* const txContext = txMonitor->getTransactionContext();

        const boost::shared_ptr<SnapshotManager>& snapshotSvc = txMonitor->getSnapshotManager();

        // Bail if we are not supposed to build a call graph.
        if (!snapshotSvc->isCallGraphEnabled())
            return;

        // If we're at the end of a call to an internal function, it's a leaf
        // node, and might have to be trimmed, depending on config.
        // We do not want to trim it here if we have an exit call, that will be
        // done below.  If a call graph has already been created, then we know
        // the internal function is not a leaf.
        if (    (!hasCallGraphNode())
            &&  (m_onStack.m_callElementType == appdynamics::pb::CallElement_Type_INTERNAL)
            &&  snapshotSvc->skipInternals()
            &&  ((!txContext) || (!txContext->getCurrentExitCall()))) {
            stateChanger.addNDeferredNodes(-1);
            stateChanger.setTopExecStackFrame(getCallerFrame());
            return;
        }

        const Timer& timer = stateChanger.getTimer();
        uint64_t endTime = timer.getTimestamp();
        if (endTime < m_startStamp)
        {
            LOG4CXX_ERROR(stateChanger.getLogger(), "Bad execution time after function: "
                                             << m_startStamp
                                             << " "
                                             << endTime
                                             << " "
                                             << getNodeName());
        }
        uint64_t elapsedTimeInMillis =
            timer.getElapsedTime(m_startStamp, endTime) / 1000;

        if (hasCallGraphNode() || elapsedTimeInMillis >= snapshotSvc->getMinCallGraphNodeDuration())
        {
            // Skip creation of a call graph node for this frame
            // if the current frame is for an exit call, unless
            // we already created a call graph node because the exit call
            // called something else.
            if ((!hasCallGraphNode()) && txContext && txContext->getCurrentExitCall())
            {
                ExecStackFrame* const callerFrame = getCallerFrame();
                // exit calls should never be the root frame
                BOOST_ASSERT(callerFrame != NULL);

                // Create call nodes for any of our ancestors that don't already have one.
                // We need to do this so that addSnapshotExitCall's check for a
                // CallGraphNode will succeed.
                callerFrame->createDeferredCallGraphNodes(stateChanger, false);

                // Since we are not creating a node for this frame,
                // we need to decrement the deferred nodes count.
                stateChanger.addNDeferredNodes(-1);

                stateChanger.setTopExecStackFrame(callerFrame);

                return;
            }
            createDeferredCallGraphNodes(stateChanger, false);
            BOOST_ASSERT(hasCallGraphNode());

            m_callGraphNode->setExecutionTime(elapsedTimeInMillis);
        }
        else
        {
            stateChanger.addNDeferredNodes(-1);
        }
        stateChanger.setTopExecStackFrame(getCallerFrame());
    }

    inline void ExecStackFrame::onExitFromCallable(GraphCollectionState& state)
    {
        GraphCollectionStateChanger stateChanger(&state);

        BOOST_ASSERT(stateChanger.getTopExecStackFrame() == this);
        if (!stateChanger.getSnapshot())
            return;

        // Bail if we are not supposed to build a call graph.
        if (!(stateChanger.getSnapshot()->isCallGraphEnabled()))
            return;

        const Timer& timer = state.getTimer();
        uint64_t endTime = timer.getTimestamp();

        ExecStackFrame* currentFrame = this;

        // Time stamp will be 0 if we got interrupted by a signal. The current
        // frame is unusable so just destroy it and pretend it never existed.
        if (currentFrame->m_startStamp == 0) {
            currentFrame->~ExecStackFrame();
            currentFrame = currentFrame->getCallerFrame();
            BOOST_ASSERT(stateChanger.getDepth() > 0);
            stateChanger.addDepth(-1);
            stateChanger.addNDeferredNodes(-1);
        }

        currentFrame->createDeferredCallGraphNodes(stateChanger, true);

        // Loop over all the frames that we hooked up to the call graph.
        // If we ever need to support unwinding just part of the call stack, then
        // this loop should have a different termination condition ( It should terminate
        // when we get to a frame that will not be unwound ).
        do
        {
            currentFrame->computeAndSetExecutionTime(state, timer, endTime);
            // Need to call ~ExecStackFrame because the destructor for these frames
            // will not be called automatically when the exit opcode executes.
            currentFrame->~ExecStackFrame();
            currentFrame = currentFrame->getCallerFrame();
            BOOST_ASSERT(stateChanger.getDepth() > 0);
            stateChanger.addDepth(-1);
        } while (currentFrame != NULL);
        BOOST_ASSERT(stateChanger.getNDeferredNodes() == 0);
        BOOST_ASSERT(stateChanger.getDepth() == 0);
        stateChanger.setTopExecStackFrame(NULL);
    }

    inline const NodeName& ALWAYS_INLINE_IN_RELEASE ExecStackFrame::getNodeName()
    {
        if (hasCallGraphNode())
            return m_callGraphNode->getName();
        return *getOnStackNodeNamePtr();
    }

    inline void ALWAYS_INLINE_IN_RELEASE ExecStackFrame::startTimer(GraphCollectionState& state)
    {
        // If this frame is not the top of the frame stack,
        // then we don't need to start a timer.
        if (state.getTopStackFrame() != this)
            return;
        m_startStamp = state.getTimer().getTimestamp();
    }

    inline ExecStackFrame* ALWAYS_INLINE_IN_RELEASE ExecStackFrame::getCallerFrame() const
    {
        return m_callerFrame;
    }

    inline void ExecStackFrame::addSnapshotExitCall(GraphCollectionState& state,
                                                    const boost::shared_ptr<SnapshotExitCall>& snapshotExitCall)
    {
        BOOST_ASSERT(state.getSnapshot());
        BOOST_ASSERT(state.getSnapshot()->isCallGraphEnabled());
        BOOST_ASSERT(state.getTopStackFrame() == this);

        // Force the call graph chain to exist so that there is something to
        // attach the exit call to.
        GraphCollectionStateChanger stateChanger(&state);
        createDeferredCallGraphNodes(stateChanger, false);

        // Since this is called after onCallableEnd for this frame has already happened,
        // we can assume that if a node will ever exist for this frame, it will
        // already exist at this point.
        //
        // If a node does not exist here, one or more of the following is true:
        // 1.  The exit call was too fast to create a call graph node.
        // 2.  The exit call was on a very deep call stack and this frame
        // is not the one that actually did the exit call.
        // 3.  The call graph is or will be full and this frame is not the one
        // that actually did the exit call.
        if (hasCallGraphNode())
            m_callGraphNode->addSnapshotExitCall(snapshotExitCall);
    }

    inline NodeType ALWAYS_INLINE_IN_RELEASE ExecStackFrame::getCallType() const
    {
        if (hasCallGraphNode())
            return m_callGraphNode->getType();
        return getOnStackCallType();
    }

    inline void ALWAYS_INLINE_IN_RELEASE ExecStackFrame::setCallElementType(appdynamics::pb::CallElement_Type type)
    {
        BOOST_ASSERT(!hasCallGraphNode());
        m_onStack.m_callElementType = type;
    }

    inline bool ALWAYS_INLINE_IN_RELEASE ExecStackFrame::hasCallGraphNode() const
    {
        return m_callNodeAllocated;
    }

    inline void ALWAYS_INLINE_IN_RELEASE ExecStackFrame::convertNodeToName()
    {
        if (!hasCallGraphNode()) {
            return;
        }

        Node* node = m_callGraphNode;

        m_callGraphNode = NULL;
        m_callNodeAllocated = false;

        // Move NodeName back from the CallGraphNode to the NodeName embedded in the ExecStackFrame.
        NodeName* const nodeName = new (m_onStack.m_nodeNameBytes) NodeName();
        NodeName::move(nodeName, &(node->m_name));

        NodeType newNodeType = node->m_type;
        appdynamics::pb::CallElement_Type newCallElementType = node->m_callElementType;

        m_onStack.m_callType = newNodeType;
        m_onStack.m_callElementType = newCallElementType;

        BOOST_ASSERT(!hasCallGraphNode());
    }

    inline void ExecStackFrame::createDeferredCallGraphNodes(GraphCollectionStateChanger& stateChanger, bool skipCreatedNodes)
    {
        if (hasCallGraphNode())
            return;

        ExecStackFrame* currentFrame = this;

        // In case of signal interruption, we may have created a portion of the
        // nodes starting from the leaf already. Let's just skip them and get to
        // the ones not yet created.
        // If there are no uncreated nodes, bail out early.
        if (skipCreatedNodes) {
            while (currentFrame && currentFrame->hasCallGraphNode())
                currentFrame = currentFrame->getCallerFrame();
            if (!currentFrame)
                return;
        }

        // ** PERF This loop can potentially be improved by
        // peeling off the first iteration.
        size_t nNodesCreated = 0;

        // Walk up the call stack allocating and hooking
        // up CallGraphNode's as we go.
        Node* childFrameCallGraphNode = NULL;

        while ((currentFrame) && (!(currentFrame->hasCallGraphNode())))
        {
            Node* newCallGraphNode = currentFrame->createCallGraphNode(stateChanger);

            if (childFrameCallGraphNode)
                newCallGraphNode->addCalledNode(childFrameCallGraphNode);

            childFrameCallGraphNode = newCallGraphNode;

            BOOST_ASSERT(currentFrame->hasCallGraphNode());
            currentFrame = currentFrame->getCallerFrame();

            ++nNodesCreated;
        }

        if (nNodesCreated != 0)
            stateChanger.addNDeferredNodes(-nNodesCreated);

        if (!currentFrame)
        {
            BOOST_ASSERT(childFrameCallGraphNode != NULL);
            stateChanger.getCallGraph().addRoot(childFrameCallGraphNode);
        }
        else
        {
            BOOST_ASSERT(currentFrame->hasCallGraphNode());
            if (childFrameCallGraphNode)
                currentFrame->m_callGraphNode->addCalledNode(childFrameCallGraphNode);
        }
    }

    inline Node* ExecStackFrame::createCallGraphNode(GraphCollectionStateChanger& stateChanger)
    {
        BOOST_ASSERT(!hasCallGraphNode());
        const char* filename = NULL;
        unsigned lineno = 0;
        computeLocation(stateChanger, &filename, &lineno);
        NodeName* nodeName = getOnStackNodeNamePtr();
        NodeType callType = getOnStackCallType();
        Node* graphNode =
            stateChanger.getCallGraph().newNode(nodeName,
                                                callType,
                                                m_onStack.m_callElementType,
                                                filename,
                                                lineno,
                                                m_startStamp);
        reinterpret_cast<NodeName*>(m_onStack.m_nodeNameBytes)->~NodeName();
        m_callNodeAllocated = true;
        m_callGraphNode = graphNode;
        return graphNode;
    }

    inline void ExecStackFrame::computeAndSetExecutionTime(GraphCollectionState& state,
                                                           const Timer& timer,
                                                           uint64_t endStamp)
    {
        BOOST_ASSERT(hasCallGraphNode());
        if (endStamp < m_startStamp)
        {
            LOG4CXX_ERROR(state.getLogger(), "Bad execution time after exit: "
                                             << m_startStamp
                                             << " "
                                             << endStamp
                                             << " "
                                             << m_callGraphNode->getName());
        }
        uint64_t const elapsedTimeInMicros =
            timer.getElapsedTime(m_startStamp, endStamp);
        unsigned long const elapsedTimeInMillis =
            elapsedTimeInMicros / 1000;
        m_callGraphNode->setExecutionTime(elapsedTimeInMillis);
    }

    inline NodeName* ALWAYS_INLINE_IN_RELEASE ExecStackFrame::getOnStackNodeNamePtr() const
    {
        BOOST_ASSERT(!hasCallGraphNode());
        NodeName* const nodeName = reinterpret_cast<NodeName*>(m_onStack.m_nodeNameBytes);
        if (m_onStack.m_callType == CALL_NOT_COMPUTED) {
            NodeType callType;
            computeNodeName(nodeName,
                            &callType);
            m_onStack.m_callType = callType;
            BOOST_ASSERT(m_onStack.m_callType != CALL_NOT_COMPUTED);
        }
        return nodeName;
    }

    inline NodeType ALWAYS_INLINE_IN_RELEASE ExecStackFrame::getOnStackCallType() const
    {
        BOOST_ASSERT(!hasCallGraphNode());
        if (m_onStack.m_callType == CALL_NOT_COMPUTED)
            getOnStackNodeNamePtr();
        BOOST_ASSERT(m_onStack.m_callType != CALL_NOT_COMPUTED);
        return m_onStack.m_callType;
    }

    namespace {
        static inline unsigned long getCallTypeFromZendOP(const zend_op* op)
        {
        #if PHP_VERSION_ID >= 50400
            return op->extended_value;
        #else
            return static_cast<unsigned long>(op->op2.u.constant.value.lval);
        #endif
        }
    }

    inline void ExecStackFrame::computeNodeName(NodeName* name, NodeType* type) const
    {
        if (!m_executeData) {
            *type = CallGraph::CALL_UNKNOWN;
            // This const cast is ok, because we set free_method to false below!!!
            name->m_methodName = const_cast<char*>(ROOT_SYMBOL);
            name->m_freeMethodName = false;
            return;
        }
        zend_function* const zendFunction = APPD_EXECDATA_ZEND_FUNC(m_executeData);
        BOOST_ASSERT(zendFunction != NULL);

        const char* const functionName = zendFunction->common.function_name ?
            ZSTR_VAL(zendFunction->common.function_name) :
            NULL;

        if (functionName) {
            // If our function has a scope, we can compute a class name.
            if (zendFunction->common.scope) {
                name->m_className = zendFunction->common.scope->name ?
                    ZSTR_VAL(zendFunction->common.scope->name) :
                    NULL;
                name->m_freeClassName = false;
            }

            if (zendFunction->common.fn_flags & ZEND_ACC_CLOSURE) {
                // If the function is a closure, we need to
                // generate a function name that will help a user identify
                // where the closure is in their source code.
                const char* const baseFileName = zendFunction->op_array.filename ?
                    agent_get_base_filename(ZSTR_VAL(zendFunction->op_array.filename)) :
                    "";
                name->m_methodName = agent_sprintf(sizeof("{closure:%s:%d-%d}"),
                                                   "{closure:%s:%d-%d}",
                                                   baseFileName,
                                                   zendFunction->op_array.line_start,
                                                   zendFunction->op_array.line_end);
                name->m_freeMethodName = true;
            } else if (zendFunction->common.fn_flags & ZEND_ACC_CALL_VIA_HANDLER) {
                // If the current function is a synthetic function generated by the
                // __call method machinery, then we need to copy the function name.
                name->m_methodName = estrdup(functionName);
                name->m_freeMethodName = true;
            }
            else {
                name->m_methodName = functionName;
            }
            *type = CallGraph::CALL_NORMAL;
            return;
        }

        zend_execute_data *exd = const_cast<zend_execute_data*>(m_executeData);

        // In PHP7, we may need to look at the previous execute data as well.
        // In agentCommonEecute we saved m_executeData as the current frame
        // instead of the parent frame (see getExecuteDataForParentFrame()).
        if ((!exd->opline) ||
            (exd->opline->opcode != ZEND_INCLUDE_OR_EVAL)) {
#if PHP_VERSION_ID >= 70000
            // In PHP7, if the prev_execute_data is also not ZEND_INCLUDE_OR_EVAL,
            // then it's for sure an unknown call.
            if(exd->prev_execute_data &&
                exd->prev_execute_data->opline &&
                exd->prev_execute_data->opline->opcode == ZEND_INCLUDE_OR_EVAL) {
                exd = exd->prev_execute_data;
            } else {
#endif
                name->m_methodName = "???";
                *type = CallGraph::CALL_UNKNOWN;
                return;
#if PHP_VERSION_ID >= 70000
            }
#endif
        }

        BOOST_ASSERT(exd->opline != NULL);
        BOOST_ASSERT(exd->opline->opcode == ZEND_INCLUDE_OR_EVAL);
        switch (getCallTypeFromZendOP(exd->opline))
        {
            case ZEND_INCLUDE:
                name->m_className = "include";
                BOOST_ASSERT(zendFunction->type == ZEND_USER_FUNCTION);
                *type = CallGraph::CALL_INCLUDES;
                break;
            case ZEND_REQUIRE:
                name->m_className = "require";
                BOOST_ASSERT(zendFunction->type == ZEND_USER_FUNCTION);
                *type = CallGraph::CALL_INCLUDES;
                break;
            case ZEND_INCLUDE_ONCE:
                name->m_className = "include_once";
                BOOST_ASSERT(zendFunction->type == ZEND_USER_FUNCTION);
                *type = CallGraph::CALL_INCLUDES;
                break;
            case ZEND_REQUIRE_ONCE:
                name->m_className = "require_once";
                BOOST_ASSERT(zendFunction->type == ZEND_USER_FUNCTION);
                *type = CallGraph::CALL_INCLUDES;
                break;
            case ZEND_EVAL:
                name->m_methodName = "eval";
                BOOST_ASSERT(zendFunction->type == ZEND_EVAL_CODE);
                *type = CallGraph::CALL_EVAL;
                break;
            default:
                name->m_methodName = "???";
                *type = CallGraph::CALL_UNKNOWN;
                break;
        }
        if (!name->m_methodName)
            name->m_methodName = zendFunction->op_array.filename ?
                ZSTR_VAL(zendFunction->op_array.filename) :
                NULL;
    }

    inline void ExecStackFrame::computeLocation(
            const GraphCollectionStateChanger& stateChanger,
            const char** filename,
            unsigned* lineno) const
    {
        // m_executeData points to the execute data for the php call frame that
        // invoked a function that caused the current ExecStackFrame to be created.
        //
        // For any call site that is in C/C++ code instead of PHP code,
        // we'll use the call first call site in php code we find as
        // we walk up the stack.
        const zend_execute_data* currentFrame = m_executeData;

#if PHP_VERSION_ID >= 70000
        // we get the parent frame for user functions to get the correct line number
        // and file name.
        if (currentFrame && currentFrame->func->type != ZEND_INTERNAL_FUNCTION)
            currentFrame = currentFrame->prev_execute_data;

        while ((currentFrame) && ((!currentFrame->opline) || (!currentFrame->func)))
#else
        while ((currentFrame) && ((!currentFrame->opline) || (!currentFrame->op_array)))
#endif
            currentFrame = currentFrame->prev_execute_data;

#if PHP_VERSION_ID >= 70000
        // skip the internal handler.
        if (currentFrame &&
            (!currentFrame->func || !ZEND_USER_CODE(currentFrame->func->common.type)) &&
            currentFrame->prev_execute_data &&
            currentFrame->prev_execute_data->func &&
            ZEND_USER_CODE(currentFrame->prev_execute_data->func->common.type)) {
            currentFrame = currentFrame->prev_execute_data;
        }

        // if it's an internal function, op_array is not populated as part of
        // the union that is a zend_function (thus op_array->filename is garbage)
        if (!currentFrame || currentFrame->func->type == ZEND_INTERNAL_FUNCTION) {
#else
        if (!currentFrame) {
#endif
            // We did not find a call site in php or we
            // are the root frame, so the call site we'll use is
            // line 0 of the root script.
            *lineno = 0;
            const zend_op_array* rootOpArray = stateChanger.getRootOpArray();
            if (rootOpArray && rootOpArray->filename)
                *filename = ZSTR_VAL(rootOpArray->filename);
            else
                *filename = NULL;
            return;
        }

#if PHP_VERSION_ID >= 70000
        BOOST_ASSERT(currentFrame->func != NULL);
        if (currentFrame->func->op_array.filename != NULL)
            *filename = ZSTR_VAL(currentFrame->func->op_array.filename);
        else
            *filename = NULL;
#else
        BOOST_ASSERT(currentFrame->op_array != NULL);
        *filename = currentFrame->op_array->filename;
#endif

        BOOST_ASSERT(currentFrame->opline != NULL);
        *lineno = currentFrame->opline->lineno;
    }

}


#endif
