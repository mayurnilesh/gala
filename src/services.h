/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __SERVICES_H
#define __SERVICES_H

#include <boost/make_shared.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>
#include <memory>
#include <string>

#include "agent_logger.h"
#include "configs.h"

class AgentKernel;
class SnapshotManager;
class Timer;
class TransactionMonitor;
class ZMQControlTransport;

namespace zmq
{
    class context_t;
}

namespace EUM
{
    class Config;
}

namespace InfoPoint
{
    class ConfigManager;
}

namespace MethodData
{
    class ConfigManager;
}

class IBailoutHandler {
    public:
        virtual void afterBailout() = 0;
};

class AgentIdentity
{
    public:
        AgentIdentity(const std::string& application,
                      const std::string& tier,
                      const std::string& node,
                      const std::string& controllerHost,
                      const std::string& accountName);

        inline const std::string& getApplication() const { return m_application; }
        inline const std::string& getTier() const { return m_tier; }
        inline const std::string& getNode() const { return m_node; }
        inline const std::string& getControllerHost() const { return m_controllerHost; }
        inline const std::string& getAccountName() const { return m_accountName; }

        inline size_t getHashCode() const { return m_hashCode; }

    private:
        const std::string m_application;
        const std::string m_tier;
        const std::string m_node;
        const std::string m_controllerHost;
        const std::string m_accountName;

        size_t m_hashCode;
};

namespace std {
    template<> struct equal_to<AgentIdentity> {
        bool operator()(const AgentIdentity& a, const AgentIdentity& b) const {
            return (a.getApplication()    == b.getApplication()    &&
                    a.getTier()           == b.getTier()           &&
                    a.getNode()           == b.getNode()           &&
                    a.getControllerHost() == b.getControllerHost() &&
                    a.getAccountName()    == b.getAccountName());
        }
    };
}

namespace boost {
    // Hash functor for AgentIdentity
    template<> struct hash<AgentIdentity> {
        size_t operator()(const AgentIdentity& ae) const {
            return ae.getHashCode();
        }
    };
}

class AgentContext : boost::noncopyable
{
    template <class T, class Arg1, class... Args>
    friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
    friend class AgentKernel;

    public:
        inline const AgentIdentity& getAgentIdentity() const
        {
            return m_agentIdentity;
        }

        inline const boost::shared_ptr<ZMQControlTransport>& getControlTransport() const
        {
            return m_controlTransport;
        }

        inline const boost::shared_ptr<IConfigChannel>& getConfigChannel() const
        {
            return m_configChannel;
        }

        inline const boost::shared_ptr<TransactionMonitor>& getTransactionMonitor() const
        {
            return m_transactionMonitor;
        }

        inline const boost::shared_ptr<SnapshotManager>& getSnapshotManager() const
        {
            return m_snapshotManager;
        }

        inline const boost::shared_ptr<EUM::Config>& getEUMConfig() const
        {
            return m_eumConfig;
        }

        inline const boost::shared_ptr<InfoPoint::ConfigManager>& getInfoPointConfig() const
        {
            return m_infoPointConfig;
        }

        inline const boost::shared_ptr<MethodData::ConfigManager>& getMethodDataConfig() const
        {
            return m_methodDataConfig;
        }

        inline AgentKernel* getKernel() const
        {
            return m_kernel;
        }

        void onRequestBegin();
        void onRequestEnd();
        void enable();
        void disable();
        void afterBailout();

        inline void registerBailoutHandler(IBailoutHandler* handler)
        {
            m_bailoutHandlers.insert(handler);
        }

    private:
        AgentIdentity m_agentIdentity;
        AgentKernel* const m_kernel;
        boost::shared_ptr<ZMQControlTransport> m_controlTransport;
        boost::shared_ptr<IConfigChannel> m_configChannel;
        boost::shared_ptr<TransactionMonitor> m_transactionMonitor;
        boost::shared_ptr<SnapshotManager> m_snapshotManager;
        boost::shared_ptr<EUM::Config> m_eumConfig;
        boost::shared_ptr<InfoPoint::ConfigManager> m_infoPointConfig;
        boost::shared_ptr<MethodData::ConfigManager> m_methodDataConfig;

        boost::unordered_set<IBailoutHandler*> m_bailoutHandlers;

        AgentContext(const AgentIdentity& agentIdentity,
                     AgentKernel* kernel);
};

namespace boost {
    // Hash functor for boost::shared_ptr<AgentIdentity>
    template<> struct hash<boost::shared_ptr<AgentContext>> {
        size_t operator()(const boost::shared_ptr<AgentContext>& ac) const {
            return ac->getAgentIdentity().getHashCode();
        }
    };
}

class AgentKernel : boost::noncopyable {
    public:
        AgentKernel(const char* zmqCtrlDir, const std::string& logsDir, const Timer* timer);
        virtual ~AgentKernel();

        void onRequestBegin(const AgentIdentity& identity);
        void onRequestEnd();

        inline const char* getZMQControlDir() const { return m_zmqCtrlDir; }
        inline const std::string& getLogsDir() const { return m_logsDir; }
        inline zmq::context_t& getZMQContext() const { return *m_zmqContext; }

        inline const boost::shared_ptr<AgentContext>& getAgentContext()
        {
            BOOST_ASSERT(m_requestAgentContext);
            return m_requestAgentContext;
        }

        inline int64_t generateRandomNumber() { return m_randomNumberGenerator(); }

        static void setTimer(const Timer* timer) { s_timer = timer; }
        static const Timer* getTimer() { return s_timer; }

    private:
        // DO NOT change the order of declaration. It is important that the m_zmqContext
        // comes first, so that it is destructed last.
        unique_ptr<zmq::context_t>::type m_zmqContext;
        AgentLogger m_logger;
        const char* m_zmqCtrlDir;
        std::string m_logsDir;
        boost::unordered_map<AgentIdentity, boost::shared_ptr<AgentContext>> m_agentContexts;
        boost::shared_ptr<AgentContext> m_requestAgentContext;
        boost::random::mt19937_64 m_randomNumberGenerator;

        static const Timer* s_timer;
};

#endif /* __SERVICES_H */

// vim: set fdm=marker:
