/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __dynamic_jmp_table_h
#define __dynamic_jmp_table_h

#include <stdint.h>
#include <stddef.h>
#include <new>
#include <unistd.h>
#include <sys/mman.h>

namespace DynamicJmpTable
{
    /**
        Enum with values for each CPU architecture
        supported by the JmpTable template below.
     */
    enum CPUArch
    {
        CPUArch_x86_64,
        CPUArch_x86
    };

    /**
        The CPU architecture for which the compiler
        is currently compiling code for.  This value
        is used to set the default CPU architecture for
        the JmpTable template below.
     */
#if defined(__x86_64)
    static const CPUArch currentCPUArch = CPUArch_x86_64;
#elif defined(__i386)
    static const CPUArch currentCPUArch = CPUArch_x86;
#else
    #error "Missing support for your CPU!"
#endif


    namespace Private
    {
        /**
            Forward declaration of JmpInstruction class
            template.  The JmpInstruction template
            models a unconditional absolute jump instruction
            for a specified CPU architecture.
            <p>
            All specializations have a public static initialize
            method that takes two arguments:
                1.  A location to write a jump instruction
                2.  The location the jump instruction should target.
         */
        template <CPUArch cpuArch> class JmpInstruction;

        /**
            Specialization of the JmpInstruction template for
            x86-64 ( 64 bit x86 ).
         */
        template <>
        class __attribute__ ((__packed__)) JmpInstruction<CPUArch_x86_64>
        {
        public:

            static void initialize(uint8_t* entryLocation, void* target);

        private:
            JmpInstruction(void* target);

            /**
                Bytes of the jump instruction.  We use
                opcode 0xFF/5, which loads the target from
                an address relative to the following instruction.
                We 8-byte align the address containing the jmp
                target so the relative offset is 2 ( which skips past
                the 2 bytes of padding ).

                These bytes will contains the following values:
                0xFF 0x25 0x02 0x00 0x00 0x00
             */
            uint8_t m_inst[6];

            // uninitialized padding.
            uint8_t m_padding[2];

            /**
                The target of this jump instruction.
             */
            void* m_target;
        };

        /**
            Specialization of the JmpInstruction template for
            x86 ( 32 bit x86 ).
         */
        template <>
        class __attribute__ ((__packed__)) JmpInstruction<CPUArch_x86>
        {
        public:

            static void initialize(uint8_t* entryLocation, void* target);

        private:
            JmpInstruction(void* target);

            /**
                Opcode bytes of the jump instruction.
                The instruction includes the absolute address of the
                word containing the target of the jump instruction.

                The contents of these bytes is:
                0xFF 0x25
             */
            uint8_t m_inst[2];

            /**
                Address of the m_target field below.
             */
            void** m_addrOfTarget;

            // uninitialized padding
            uint8_t m_padding_0[2];

            /**
                The target of this jump instruction.
             */
            void* m_target;

            // uninitialized padding
            uint8_t m_padding_1[4];
        };
    }

    /**
        A class that creates a table of jump instructions
        for the specified CPU architecture.
        <p>
        The table of jump instructions can be used to assign
        an integer to a function.  When a function is added to the
        table, a jump instruction is generated in the table that will jump
        to the original function and the address of the jump instruction
        is returned.  References to the original function can be replaced
        with references to the jump instruction.  Given the address of the
        jump instruction this table can return the index of the jump table
        entry for that jump instruction in constant time.
        <p>
        Jump table entries may not be removed and this class has no
        destructor so jump table entries are valid from the time they are added
        to the time the process terminates.
     */
    template <CPUArch cpuArch = currentCPUArch, uint32_t maxNumEntries=1024>
    class JmpTable
    {
    private:
        typedef Private::JmpInstruction<cpuArch> t_JmpInstruction;
    public:
        JmpTable();

        /**
            Find the index of an entry in the jump table in constant time.
            @param targetFunction Function pointer to look up in the table.
            If this function is in a table, this will actually point to jump
            instruction that jumps to the original function that was "added"
            to the table.
            @param The index in the table of the specified targetFunction.
            @return true if the specified targetFunction is in the table, false
            otherwise.  If false, the value pointed at by index is not modified.
         */
        template <typename t_ReturnType, typename...t_Args>
        bool findEntry(t_ReturnType (*targetFunction)(t_Args...),
                       unsigned* index);
        /**
            Add an entry to the jump table.  If the specified targetFunction
            is already a jump table entry, then *jmpInstr is assigned to
            targetFunction and this method returns true.
            <p>
            @param targetFunction the function to add to the table.
            @param jmpInstr A pointer to function pointer that will be
            assigned to the jump instruction in the jump table.  References to
            targetFunction can safely be replaced with the returned function pointer.
            @param index A pointer to an unsigned that will be assigned
            to the index of the jump table entry.
            @return true if an entry was successfully added the table, false otherwise.
            If false, the function pointer pointed at by jmpInstr is not modified.
         */
        template <typename t_ReturnType, typename...t_Args>
        bool addEntry(t_ReturnType (*targetFunction)(t_Args...),
                      t_ReturnType (**jmpInstr)(t_Args...),
                      unsigned* index);
    private:

        void lazyInit();

        bool findEntry(uint8_t* pointerToJmp, unsigned* index);
        bool addEntry(uint8_t* targetFunction, void** jmpInstr, unsigned* index);

        /**
            bool to keep track of whether or not the jump table
            has been initialized yet.  Set to true by first
            successful addEntry call.
         */
        bool m_isInitialized;

        /**
            Number of bytes per page in the current process.
         */
        size_t m_pageSize;

        /**
            Pointers to the first entry.
         */
        union
        {
            uint8_t* m_start;
            Private::JmpInstruction<cpuArch>* m_x8664Jmps;
        };

        /**
            Pointer to the location immediately following
            the last possible jump table entry.
         */
        uint8_t* m_end;

        /**
            Pointer to the location to write the next jump table entry.
         */
        uint8_t* m_nextJmp;
    };



    namespace Private
    {
        void JmpInstruction<CPUArch_x86_64>::initialize(uint8_t* entryLocation, void* target)
        {
            // placement new...
            new (entryLocation) JmpInstruction<CPUArch_x86_64>(target);
        }


        JmpInstruction<CPUArch_x86_64>::JmpInstruction(void* target)
        {
            m_inst[0] = 0xFF;
            m_inst[1] = 0x25;
            m_inst[2] = 0x02;
            m_inst[3] = 0x00;
            m_inst[4] = 0x00;
            m_inst[5] = 0x00;
            m_target = target;
            (void)m_padding;
        }

        void JmpInstruction<CPUArch_x86>::initialize(uint8_t* entryLocation, void* target)
        {
            // placement new...
            new (entryLocation) JmpInstruction<CPUArch_x86>(target);
        }


        JmpInstruction<CPUArch_x86>::JmpInstruction(void* target)
        {
            m_inst[0] = 0xFF;
            m_inst[1] = 0x25;
            m_addrOfTarget = &m_target;
            m_target = target;
            (void)m_padding_0;
            (void)m_padding_1;
        }
    }

    template <CPUArch cpuArch, uint32_t maxNumEntries>
    JmpTable<cpuArch, maxNumEntries>::JmpTable()
        : m_isInitialized(false)
        , m_start(NULL)
        , m_end(NULL)
        , m_nextJmp(NULL)
    {
    }

    template <CPUArch cpuArch, uint32_t maxNumEntries>
    template <typename t_ReturnType, typename...t_Args>
    inline bool JmpTable<cpuArch, maxNumEntries>::findEntry(t_ReturnType (*pointerToJmp)(t_Args...),
                                 unsigned* index)
    {
        return findEntry(((uint8_t*)pointerToJmp), index);
    }

    template <CPUArch cpuArch, uint32_t maxNumEntries>
    bool JmpTable<cpuArch, maxNumEntries>::findEntry(uint8_t* pointerToJmp, unsigned* index)
    {
        if (!m_isInitialized)
            return false;
        if (pointerToJmp < m_start)
            return false;
        if (pointerToJmp >= m_end)
            return false;
        ptrdiff_t offset = pointerToJmp - m_start;
        *index = offset / sizeof(t_JmpInstruction);
        return true;
    }

    template <CPUArch cpuArch, uint32_t maxNumEntries>
    template <typename t_ReturnType, typename...t_Args>
    inline bool JmpTable<cpuArch, maxNumEntries>::addEntry(t_ReturnType (*targetFunction)(t_Args...),
                                       t_ReturnType (**jmpInstr)(t_Args...),
                                       unsigned* index)
    {
        // We are casting function pointers here so we use regular
        // C style casts.
        uint8_t* const targetFunctionBytes = (uint8_t*) targetFunction;
        void** const jmpInstrV = (void**)jmpInstr;
        return addEntry(targetFunctionBytes, jmpInstrV, index);
    }

    template <CPUArch cpuArch, uint32_t maxNumEntries>
    inline void JmpTable<cpuArch, maxNumEntries>::lazyInit()
    {
        if (m_isInitialized)
            return;

        long pageSize = sysconf(_SC_PAGESIZE);
        if (pageSize == -1)
            return;

        m_pageSize = static_cast<size_t>(pageSize);
        const size_t entriesPerPage = m_pageSize / sizeof(t_JmpInstruction);

        // nPagesNeeded = round_up(maxNumEntries/entriesPerPage)
        const size_t nPagesNeeded = ((maxNumEntries + entriesPerPage - 1) / entriesPerPage);

        const size_t nBytesToMap = nPagesNeeded * m_pageSize;
        m_start = reinterpret_cast<uint8_t*>(mmap(NULL,
                                                  nBytesToMap,
                                                  PROT_READ | PROT_EXEC | PROT_WRITE,
                                                  MAP_PRIVATE | MAP_ANON,
                                                  -1,
                                                  0));
        if (m_start == reinterpret_cast<uint8_t*>(MAP_FAILED))
            return;

        m_end = m_start + (maxNumEntries * sizeof(t_JmpInstruction));
        m_nextJmp = m_start;
        m_isInitialized = true;
    }

    template <CPUArch cpuArch, uint32_t maxNumEntries>
    bool JmpTable<cpuArch, maxNumEntries>::addEntry(uint8_t* targetFunction, void** jmpInstr, unsigned* index)
    {
        lazyInit();
        if (!m_isInitialized)
            return false;

        if (findEntry(targetFunction, index))
        {
            *jmpInstr = targetFunction;
            return true;
        }

        if (m_nextJmp == m_end)
            return false;

        t_JmpInstruction::initialize(m_nextJmp, targetFunction);
        *jmpInstr = m_nextJmp;

        ptrdiff_t offset = m_nextJmp - m_start;
        *index = offset / sizeof(t_JmpInstruction);

        m_nextJmp += sizeof(t_JmpInstruction);
        return true;
    }

}




#endif
