/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __exitcall_detail_h
#define __exitcall_detail_h

#include "snapshot.h"

class AErrorObject;

/**
    Templated base class that can be used by sub-classes of
    AExitCallInterceptor to implement addExitCallDetailForSnapshot.
    <p>
    Sub-classes whose exit point type is not EXIT_DB,
    must have a addExitCallDetailForSnapshot method:
    void addSnapshotExitCallDetail(const CurrentExitCall*,
                                   const PHPExecEnvironment*,
                                   const boost::shared_ptr<SnapshotExitCall>&) const;

    addSnapshotExitCallDetail should populate the specified SnapshotExitCall with
    information collected from the current PHP execution environment and the CurrentExitCall.

    <p>
    Sub-classes whose exit point type is EXIT_DB,
    must have the following methods:
    appdynamics::pb::SnapshotExitCall* createAndAddSnapshotDBCall(const PHPExecEnvironment*,
                                                                  TransactionContext*,
                                                                  Snapshot*,
                                                                  const CurrentExitCall*,
                                                                  uint64_t timeTakenMS) const;
    std::string getSQLType(const std::string& sql);
    const char* getStatementType() const;

    createAndAddSnapshotDBCall should create and add a SnapshotExitCall to the specified
    Snapshot.

    getSQLType should get the SQL type string given a specified SQL query.

    getStatementType should return NULL, "String", "Prepared Statement", depending
    on how the SQL string is specified.

    @param t_Base Base class the template instance should derive from.
    @param t_Derived Class that is deriving from this template instance.
    @param exitPointType the type of exit point the interceptor class deriving
    for this class is associated with.
 */
template <class t_Base, class t_Derived, appdynamics::pb::Agent::ExitPointType exitPointType>
class ExitCallDetailHelper : public t_Base
{
protected:
    template <typename... t_Args>
    ExitCallDetailHelper(t_Args...args);

    virtual void addExitCallDetailForSnapshot(const PHPExecEnvironment* phpExecEnv,
                                              TransactionContext* txContext,
                                              const CurrentExitCall* exitCall,
                                              const boost::shared_ptr<AErrorObject>& errorObject,
                                              const boost::shared_ptr<SnapshotManager>& snapshotManager,
                                              uint64_t timeTakenInMS) const;
};

template <class t_Base, class t_Derived, appdynamics::pb::Agent::ExitPointType exitPointType>
template <typename... t_Args>
ExitCallDetailHelper<t_Base, t_Derived, exitPointType>::ExitCallDetailHelper(t_Args...args)
    : t_Base(args...)
{
}

template <class t_Base, class t_Derived, appdynamics::pb::Agent::ExitPointType exitPointType>
void ExitCallDetailHelper<t_Base,
                          t_Derived,
                          exitPointType>::addExitCallDetailForSnapshot(const PHPExecEnvironment* phpExecEnv,
                                                                       TransactionContext* txContext,
                                                                       const CurrentExitCall* exitCall,
                                                                       const boost::shared_ptr<AErrorObject>& errorObject,
                                                                       const boost::shared_ptr<SnapshotManager>& snapshotManager,
                                                                       uint64_t timeTakenInMS) const
{
    Snapshot* snapshot = snapshotManager->getSnapshot();
    BOOST_ASSERT(snapshot != NULL); // we should never get here if there is no Snapshot.

    if (!snapshot->isCallGraphEnabled())
        return;

    uint64_t minDurationMS = errorObject ? 0 : snapshotManager->getMinExitCallTime();
    if (timeTakenInMS < minDurationMS)
        return;

    const t_Derived* const pThis = static_cast<const t_Derived*>(this);

    boost::shared_ptr<SnapshotExitCall> sec(pThis->createSnapshotExitCall(txContext,
                                                                          exitCall,
                                                                          errorObject,
                                                                          timeTakenInMS));
    if (!sec)
        return;

    pThis->addSnapshotExitCallDetail(exitCall, phpExecEnv, sec);
    pThis->reportSnapshotExitCall(sec, phpExecEnv);
}

/**
    Specialization of ExitCallDetailHelper for database exit call interceptors.
 */
template <class t_Base, class t_Derived>
class ExitCallDetailHelper<t_Base, t_Derived, appdynamics::pb::Agent::EXIT_DB> : public t_Base
{
protected:
    template <typename... t_Args>
    ExitCallDetailHelper(t_Args...args);

    virtual void addExitCallDetailForSnapshot(const PHPExecEnvironment* phpExecEnv,
                                              TransactionContext* txContext,
                                              const CurrentExitCall* exitCall,
                                              const boost::shared_ptr<AErrorObject>& errorObject,
                                              const boost::shared_ptr<SnapshotManager>& snapshotManager,
                                              uint64_t timeTakenMS) const;
};

template <class t_Base, class t_Derived>
template <typename... t_Args>
ExitCallDetailHelper<t_Base, t_Derived, appdynamics::pb::Agent::EXIT_DB>::ExitCallDetailHelper(t_Args...args)
    : t_Base(args...)
{
}

template <class t_Base, class t_Derived>
void ExitCallDetailHelper<t_Base,
                          t_Derived,
                          appdynamics::pb::Agent::EXIT_DB>::addExitCallDetailForSnapshot(const PHPExecEnvironment* phpExecEnv,
                                                                                  TransactionContext* txContext,
                                                                                  const CurrentExitCall* exitCall,
                                                                                  const boost::shared_ptr<AErrorObject>& errorObject,
                                                                                  const boost::shared_ptr<SnapshotManager>& snapshotManager,
                                                                                  uint64_t timeTakenInMS) const
{
    const t_Derived* const pThis = static_cast<const t_Derived*>(this);
    Snapshot* snapshot = snapshotManager->getSnapshot();
    BOOST_ASSERT(snapshot != NULL); // we should never get here if there is no Snapshot.
    const appdynamics::pb::SnapshotExitCall* const dbCall =
        pThis->createAndAddSnapshotDBCall(phpExecEnv,
                                          txContext,
                                          snapshot,
                                          exitCall,
                                          timeTakenInMS);
    if (!dbCall)
        return;

    if (!snapshot->isCallGraphEnabled())
        return;

    uint64_t minDurationMS = errorObject ? 0 : snapshotManager->getMinSQLExecTime();
    if (timeTakenInMS < minDurationMS)
        return;

    boost::shared_ptr<SnapshotExitCall> sec(pThis->createSnapshotExitCall(txContext,
                                                                          exitCall,
                                                                          errorObject,
                                                                          timeTakenInMS));
    if (!sec)
        return;

    const std::string& sqlString = dbCall->detailstring();
    sec->setDetailString(sqlString);

    const std::string& sqlType = pThis->getSQLType(sqlString);
    sec->addProperty("Query Type", sqlType);
    sec->addProperty("function", phpExecEnv->getCallableName());

    if (dbCall->has_boundparameters())
        sec->setBoundParams(&(dbCall->boundparameters()));
    const char* const statementType = pThis->getStatementType();
    if (statementType != NULL)
        sec->addProperty("Statement Type", statementType);
    pThis->reportSnapshotExitCall(sec, phpExecEnv);
}

#endif
