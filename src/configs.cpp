/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include <boost/foreach.hpp>
#include "configs.h"
#include "zmqtransport.h"
#include "services.h"

static const long DEFAULT_REQUEST_INTERVAL = 30;          /* seconds */
static const int MAX_REQS_WITHOUT_RESPONSE = 3;

//
// ConfigChannel
//

ConfigChannel::ConfigChannel(const boost::shared_ptr<ZMQControlTransport>& controlTransport)
    : config_state(appdynamics::pb::ConfigResponse::UNREGISTERED)
    , m_agentIdentity()
    , last_version(0)
    , last_response_time(0)
    , num_config_requests(0)
    , config_request_interval(DEFAULT_REQUEST_INTERVAL)
{
    logger = getLogger(std::string(LogContext::CONFIG) + ".ConfigChannel");
    m_transport =
      ZMQControlTransport::createTransport<ZMQConfigTransport>(controlTransport,
                                                               ZMQTransportBase::getBaseSocketNumber());
}

ConfigChannel::~ConfigChannel()
{
}

void ConfigChannel::setState(appdynamics::pb::ConfigResponse::AgentState new_state,
                             const struct _zend_agent_globals* agentGlobals)
{
    // static asserts to make sure this method fails to compile
    // if a new value is added to the agent state enum.
    BOOST_STATIC_ASSERT(appdynamics::pb::ConfigResponse_AgentState_AgentState_MIN == 0);
    BOOST_STATIC_ASSERT(appdynamics::pb::ConfigResponse_AgentState_AgentState_MAX == appdynamics::pb::ConfigResponse_AgentState_DISABLED);

    if (new_state == config_state)
        return;

    switch (new_state) {
        case appdynamics::pb::ConfigResponse::INITIALIZED:
            LOG4CXX_INFO(logger, "going into INITIALIZED state");
            agentGlobals->kernel->getAgentContext()->enable();
            break;

        case appdynamics::pb::ConfigResponse::DISABLED:
            LOG4CXX_INFO(logger, "going into DISABLED state");
            agentGlobals->kernel->getAgentContext()->disable();
            break;

        default:
            LOG4CXX_WARN(logger, "invalid state value in setState() - " << new_state);
            return;
    }

    config_state = new_state;
}

void ConfigChannel::requestUpdate()
{
    bool send_request = false;

    LOG4CXX_TRACE(logger, "request interval: " << config_request_interval);
    time_t now = time(NULL);
    if (num_config_requests == 0) {
        send_request = (now - last_response_time) >= config_request_interval;
        LOG4CXX_TRACE(logger, "no requests sent, config expired by: " << (now - last_response_time));
    }
    else {
        send_request = (now - m_transport->getRequestTimestamp()) >= config_request_interval;
        LOG4CXX_TRACE(logger, "requests sent: " << num_config_requests << ", config expired by: " << (now - m_transport->getRequestTimestamp()));
    }

    if (send_request)
    {
        appdynamics::pb::ASyncRequest asyncRequest;
        asyncRequest.set_type(appdynamics::pb::ASyncRequest::CONFIG);
        appdynamics::pb::ConfigRequest& configRequest =
            *(asyncRequest.mutable_configreq());
        if ((config_state == appdynamics::pb::ConfigResponse::UNREGISTERED) ||
            (config_state == appdynamics::pb::ConfigResponse::DISABLED))
        {
            LOG4CXX_DEBUG(logger, "requesting full config");
            configRequest.set_lastversion(-1);
        }
        else
        {
            LOG4CXX_DEBUG(logger, "requesting config, last version is " << last_version);
            configRequest.set_lastversion(last_version);
        }
        configRequest.set_requestid(AG(kernel)->generateRandomNumber());

        LOG4CXX_INFO(logger, "sending config request with version " << configRequest.lastversion());
        if (m_transport->sendRequest(asyncRequest) == false)
        {
            LOG4CXX_ERROR(logger, "could not send config request");
            return;
        }

        num_config_requests++;
    }
}

void ConfigChannel::checkForUpdate(const _zend_agent_globals* agentGlobals, long timeOutInMilliseconds)
{
    appdynamics::pb::ConfigResponse configResponse;
    if (m_transport->getResponse(&configResponse, timeOutInMilliseconds))
    {
        onReceivedConfigResponse(configResponse, agentGlobals);

        last_response_time = time(NULL);
        num_config_requests = 0;
    }
    else
    {
        LOG4CXX_TRACE(logger, "no config response");
        if (num_config_requests >= MAX_REQS_WITHOUT_RESPONSE)
        {
            if ((config_state != appdynamics::pb::ConfigResponse::UNREGISTERED) &&
                (config_state != appdynamics::pb::ConfigResponse::DISABLED))
            {
                LOG4CXX_WARN(logger, "no response to " << num_config_requests << " config requests in a row, disabling");
                setState(appdynamics::pb::ConfigResponse::DISABLED, agentGlobals);
                num_config_requests = 0;
            }
        }
    }

}

void ConfigChannel::onReceivedConfigResponse(const appdynamics::pb::ConfigResponse& configResponse,
                                             const struct _zend_agent_globals* agentGlobals)
{
    if (!configResponse.has_currentversion())
    {
        LOG4CXX_ERROR(logger, "No version number in config response: " << configResponse.DebugString());
        return;
    }

    if (!configResponse.has_agentstate())
    {
        LOG4CXX_ERROR(logger, "No agent state in config response: " << configResponse.DebugString());
        return;
    }

    int64_t responseVersion = configResponse.currentversion();
    if (last_version == responseVersion)
    {
        LOG4CXX_TRACE(logger, "config unchanged, version " << last_version);
        return;
    }

    // TODO check if this works anytime the version changes
    if (!configResponse.has_agentidentity()) {
        LOG4CXX_ERROR(logger, "No agent identity in config response: " << configResponse.DebugString());
    }
    m_agentIdentity.CopyFrom(configResponse.agentidentity());

    // Config changed, update timestamp skew.
    if (configResponse.timestampskew()) {
        m_timestampSkew = configResponse.timestampskew();
        LOG4CXX_TRACE(logger, "config changed, new timestamp skew: " << getTimestampSkew());
    }

    if (!agentGlobals->G->dit_flags.disableConfigUpdates) {
        LOG4CXX_TRACE(logger, "notifying " << configListeners.size() << " config listeners");
        /*
         * Notify listeners that the configuration has possibly changed. It may not
         * have, but it's up to them to detect that.
         */
        BOOST_FOREACH(IConfigListener* listener, configListeners)
        {
            listener->configChanged(configResponse, agentGlobals);
        }
        LOG4CXX_TRACE(logger, "config updated, version: " << responseVersion);
    } else {
        LOG4CXX_TRACE(logger, "config updates disabled, not processing");
    }

    last_version = responseVersion;

    setState(configResponse.agentstate(), agentGlobals);
}

void ConfigChannel::addListener(IConfigListener* listener)
{
    configListeners.insert(listener);
}

void ConfigChannel::setTimestampSkew(int64_t timestampSkew)
{
    m_timestampSkew = timestampSkew;
}

bool ConfigChannel::isAgentEnabled()
{
    // TODO act on agentEnabled?
    return ((config_state != appdynamics::pb::ConfigResponse::UNREGISTERED) &&
            (config_state != appdynamics::pb::ConfigResponse::DISABLED));
}

bool ConfigChannel::isInitialized()
{
    return config_state == appdynamics::pb::ConfigResponse::INITIALIZED;
}
