/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __snapshot_exitcall_h
#define __snapshot_exitcall_h

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <string>
#include <vector>
#include <ostream>
#include <map>
#include "enumfactory.h"
#include "backend_identifier.h"
#include "name_value_pair.h"

namespace appdynamics
{
    namespace pb
    {
        class BoundParameters;
    }
}

/* {{{ class SnapshotExitCall */

/**
 * Contains details about exit calls for storage in the snapshot (callgraph).
 * Not copyable right now.
 *
 * It is expected that all instances of this class will be wrapped in
 * shared_ptr's.
 */
class SnapshotExitCall : public boost::noncopyable
{
    public:
        SnapshotExitCall(const BackendIdentifierPtr& backendID,
                         long timeTakenInMs,
                         const std::string& sequenceInfo)
            : m_backendID(backendID)
            , m_timeTakenInMs(timeTakenInMs)
            , m_sequenceInfo(sequenceInfo)
            , m_boundParams(NULL) { }

        const std::string& getDetailString() const { return m_detailString; }
        const std::vector<boost::shared_ptr<NameValuePair>>& getProperties() const { return m_properties; }
        const BackendIdentifierPtr& getBackendID() const { return m_backendID; }
        long getTimeTakenInMs() const { return m_timeTakenInMs; }
        const std::string& getSequenceInfo() const { return m_sequenceInfo; }
        const std::string& getErrorDetails() const { return m_errorDetails; }

        inline void setDetailString(const std::string& detailString)
        {
            m_detailString = detailString;
        }

        inline void setProperties(const std::vector<boost::shared_ptr<NameValuePair>>& properties)
        {
            m_properties = properties;
        }

        inline void setErrorDetails(const std::string& errorDetails)
        {
            m_errorDetails = errorDetails;
        }

        inline void addProperty(const std::string& name, const std::string& value)
        {
            m_properties.push_back(boost::make_shared<NameValuePair>(name, value));
        }

        inline void addProperties(const std::vector<boost::shared_ptr<NameValuePair>>& properties)
        {
            std::copy(properties.begin(), properties.end(), std::back_inserter(m_properties));
        }

        inline void setBoundParams(const appdynamics::pb::BoundParameters* params)
        {
            m_boundParams = params;
        }

        inline const appdynamics::pb::BoundParameters* getBoundParams() { return m_boundParams; }

    private:
        std::string m_detailString;
        std::vector<boost::shared_ptr<NameValuePair>> m_properties;
        BackendIdentifierPtr const m_backendID;
        long const m_timeTakenInMs;
        std::string const m_sequenceInfo;
        std::string m_errorDetails;
        const appdynamics::pb::BoundParameters* m_boundParams;
};

/**
 * For debugging/logging output.
 */
std::ostream& operator<<(std::ostream& out, const SnapshotExitCall& call);

/* }}} */

#endif
