/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __transport_h
#define __transport_h

#include <boost/utility.hpp>
#include <time.h>

namespace appdynamics
{
    namespace pb
    {
        class ASyncMessage;
        class ASyncRequest;
        class BTMetricsResponse;
        class ConfigResponse;
        class BTInfoRequest;
        class BTInfoResponse;
    }
}

/**
    Interface that hides the details of the transport
    mechanism used to send configuration requests and receive
    configuration responses from the java proxy.
 */
class IConfigTransport : boost::noncopyable
{
public:
    virtual void reset() = 0;

    virtual bool sendRequest(const appdynamics::pb::ASyncRequest& request) = 0;
    virtual bool getResponse(appdynamics::pb::ConfigResponse* response, long timeoutInMilliseconds) = 0;
    virtual bool isRequestSent() const = 0;
    virtual time_t getRequestTimestamp() const = 0;
protected:
    IConfigTransport() {}
    virtual ~IConfigTransport() {}
};

/**
    Interface that hides the details of the transport
    mechanism used to send BT information requests to the proxy and get
    responses later.
 */
class IBTInfoTransport : boost::noncopyable
{
public:
    virtual bool sendRequest(const appdynamics::pb::BTInfoRequest& request) = 0;
    virtual bool getResponse(const appdynamics::pb::BTInfoRequest* request,
                             appdynamics::pb::BTInfoResponse* response,
                             long timeoutInMicroseconds) = 0;
protected:
    IBTInfoTransport() {}
    virtual ~IBTInfoTransport() {}
};

/**
    Interface that hides the details of the transport mechanism
    used to send asynchronous messages to the java proxy.  An asynchronous
    message can be used to report any of the following:
      - BT metrics
      - Snapshots
      - Aborted snapshots
 */
class IReportingTransport : boost::noncopyable
{
public:
    virtual bool sendMessage(const appdynamics::pb::ASyncMessage& message) = 0;
protected:
    IReportingTransport() {}
    virtual ~IReportingTransport() {}
};

#endif
