#include "agent.h"
#include "entrypoint.h"
#include "util.h"
#include "naming.h"
#include "transactions.h"
#include "eum/context.h"
#include "reporting.h"
#include "request_context.h"
#include <boost/make_shared.hpp>

std::ostream& operator<<(std::ostream& out, const appdynamics::pb::Agent::EntryPointType& type)
{
    out << appdynamics::pb::Agent::EntryPointType_Name(type);
    return out;
}

/* {{{ AEntryPointInterceptor methods */

AEntryPointInterceptor::~AEntryPointInterceptor()
{
}

bool AEntryPointInterceptor::acceptTransaction(const PHPExecEnvironment* phpExecEnv,
                                               const boost::shared_ptr<IMatchPointPayload>& payload)
{
    if (!payload)
        return false;

    boost::shared_ptr<ITransactionAcceptor> acceptor = getTransactionAcceptor();
    return acceptor->acceptTransaction(phpExecEnv, payload);
}

void AEntryPointInterceptor::detectErrors(const PHPExecEnvironment* phpExecEnv,
                                          void* state)
{
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    zval* exceptionObject = phpExecEnv->getExceptionObject();
    if (exceptionObject) {
        ErrorMonitor* errorMonitor = txMonitor->getErrorMonitor();
        errorMonitor->reportException(phpExecEnv, exceptionObject);
        boost::shared_ptr<RequestContext> const requestContext =
            phpExecEnv->getAgentGlobals().request_context;
        requestContext->eumContext().onUncaughtEntryPointException();
    }
}

bool AEntryPointInterceptor::isAgentEnabled() const
{
    return AG(kernel)->getAgentContext()->getConfigChannel()->isAgentEnabled();
}

bool AEntryPointInterceptor::isTransactionDetectionEnabled() const
{
    const AEntryPointDelegate* const delegate =
        AG(kernel)->getAgentContext()
                  ->getTransactionMonitor()
                  ->getEntryPointDelegate(getEntryPointType());
    return (delegate && !delegate->isDetectionDisabled());
}

bool AEntryPointInterceptor::isCorrelationHeaderPresent(const PHPExecEnvironment *execEnv) const
{
    return false;
}

// There is potential for optimization here. In case of nested BTs, the outer
// HTTP one could pick up notxdetect=true header and disable BT detection.
// We could check RequestContext for that and stop the inner acceptor from even
// firing. Could also roll this up into a public method on AEntryInterceptor so
// that AMethodInterceptor-based ones could invoke it to check whether they need
// to run or not.
bool AEntryPointInterceptor::isTxToBeIdentified(const PHPExecEnvironment *execEnv) const
{
    if (!isAgentEnabled()) {
        LOG4CXX_TRACE(m_logger, "transaction will not be monitored (agent not enabled)");
        return false;
    }

    if (!isTransactionDetectionEnabled() && !isCorrelationHeaderPresent(execEnv)) {
        LOG4CXX_DEBUG(m_logger, "transaction will not be identified, as entry point of type [" << getEntryPointName(getEntryPointType()) << "] is turned off/discovery is off and no custom rules to match");
        return false;
    }

    // TODO check for correlation disabled

    return true;
}

boost::shared_ptr<ITransactionAcceptor> AEntryPointInterceptor::getTransactionAcceptor()
{
    const AEntryPointDelegate* const delegate =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getEntryPointDelegate(getEntryPointType());
    boost::shared_ptr<ITransactionAcceptor> const acceptor(delegate->getTransactionAcceptor());
    return acceptor;
}

void* AEntryPointInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    if (!isTxToBeIdentified(phpExecEnv)) {
        return NULL;
    }

    boost::shared_ptr<RequestContext> requestContext =
            phpExecEnv->getAgentGlobals().request_context;

    // Don't proceed (aka "freeze the name") if the correlation has already
    // been performed in some exit call.
    if (requestContext->isCorrelationSent()) {
        LOG4CXX_DEBUG(m_logger, "correlation already sent, stopping identification");
        return NULL;
    }

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* existingContext = txMonitor->getTransactionContext();

    if (existingContext) {
        if (existingContext->isContinuingTransaction()) {
            LOG4CXX_DEBUG(m_logger, "already have a continuing transaction [" << getEntryPointName(existingContext->getEntryPointType()) << "], stopping identification");
            return NULL;
        }

        // Don't proceed if we already sent response headers with EUM information.
        // The EUM information in the response headers contains the current BT ID.  If
        // allow the code below to proceed, then we will have sent one BT ID to the
        // javascript agent and different BT ID to the controller.
        boost::shared_ptr<RequestContext> requestContext(existingContext->getRequestContext());
        BOOST_ASSERT(requestContext);
        const EUM::Context& eumContext = requestContext->eumContext();
        if (eumContext.headersSent()) {
            LOG4CXX_DEBUG(m_logger, "already sent EUM HTTP response headers can't change BT ["  \
                                    << existingContext->getTransactionName()
                                    << "], stopping identification");
            return NULL;
        }
    }

    std::string existingTransactionName;
    if (m_logger->isDebugEnabled()) {
        if (existingContext)
            existingTransactionName = existingContext->getTransactionName();
    }

    boost::shared_ptr<IMatchPointPayload> payload(createPayload(phpExecEnv));

    if (acceptTransaction(phpExecEnv, payload)) {
        TransactionContext* context = txMonitor->getTransactionContext();
        // context should not be null because acceptTransaction creates
        // the context.
        BOOST_ASSERT(context);
        if (!existingTransactionName.empty())
            LOG4CXX_DEBUG(m_logger, "transaction [" << existingTransactionName << "] has been replaced");

        LOG4CXX_DEBUG(m_logger, "transaction entry point begin for [" << context->getTransactionName() << "]");
        context->setInitiatingInterceptor(this);
        txMonitor->getTransactionReporter()->reportTransaction(context, requestContext);
        return context;
    }
    return NULL;
}

void AEntryPointInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                           void* state)
{
    if (!state)
        return;

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* const context = txMonitor->getTransactionContext();
    if (context != state)
        return;

    if (context) {
        detectErrors(phpExecEnv, state);
        if (m_logger->isDebugEnabled()) {
            appdynamics::pb::Agent::EntryPointType contextEntryPointType = context->getEntryPointType();
            if (contextEntryPointType == getEntryPointType())
                LOG4CXX_DEBUG(m_logger, "transaction entry point end for [" << context->getTransactionName() << "]");
        }
    }

}
/* }}} */

/* {{{ AEntryPointDelegate methods */

AEntryPointDelegate::~AEntryPointDelegate()
{
}

void AEntryPointDelegate::configure(const boost::shared_ptr<MatchPointConfig>& matchPointConfig,
                                    const struct _zend_agent_globals* agentGlobals)
{
    m_matchPointConfig = matchPointConfig;
    initAcceptor();

    BOOST_ASSERT(m_matchPointConfig);
    const appdynamics::pb::Agent::MatchPointConfig* const config =
        m_matchPointConfig->get();

    if (!config->enabled()
            || (!autoDiscoveryEnabled() && config->customdefinitions_size() == 0)) {
        m_detectionDisabled = true;
    } else {
        m_detectionDisabled = false;
    }

    applyRules(agentGlobals);
}

bool AEntryPointDelegate::autoDiscoveryEnabled()
{
    const appdynamics::pb::Agent::MatchPointConfig* const config =
        m_matchPointConfig->get();
    if (!config->has_discoveryconfig())
        return false;
    return config->discoveryconfig().enabled();
}
/* }}} */


// vim: set fdm=marker:
