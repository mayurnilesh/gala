/*
Copyright (c) AppDynamics, Inc., and its affiliates
2018
All Rights Reserved
*/

#include "analytics_collector.h"
#include "analytics.h"

AnalyticsCollector::AnalyticsCollector() : m_io_service(new boost::asio::io_service) {}

AnalyticsCollector::~AnalyticsCollector() 
{
	m_io_service->stop();
}

void AnalyticsCollector::reportData()
{
	boost::asio::streambuf request_;

  try
  {
    http_client *c = new http_client(*m_io_service, "localhost:9090", "/");
    m_io_service->run();
  }
  catch (std::exception& e)
  {
    //std::cout << "Exception: " << e.what() << "\n";
  }
}

