#include "correlation.h"
#include "current_exit_call.h"
#include "request_context.h"
#include <boost/lexical_cast.hpp>
#include <boost/static_assert.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

const char* const CorrelationHeader::MAIN_HEADER = "singularityheader";
const size_t CorrelationHeader::MAIN_HEADER_SIZE = sizeof("singularityheader")-1;

const char* const CorrelationHeader::ACCOUNT_GUID = "acctguid";
const char* const CorrelationHeader::CONTROLLER_GUID = "ctrlguid";

const char* const CorrelationHeader::APPLICATION_ID_HEADER = "appId";
const char* const CorrelationHeader::BUSINESS_TRANSACTION_ID_HEADER = "btid";
const char* const CorrelationHeader::BUSINESS_TRANSACTION_NAME_HEADER = "btname";
const char* const CorrelationHeader::ENTRY_POINT_TYPE_HEADER = "bttype";
const char* const CorrelationHeader::BT_COMPONENT_MAPPING_HEADER = "btcomp";
const char* const CorrelationHeader::EXIT_POINT_GUID_HEADER = "exitguid";
const char* const CorrelationHeader::UNRESOLVED_EXIT_ID_HEADER = "unresolvedexitid";
const char* const CorrelationHeader::COMPONENT_ID_FROM_HEADER = "cidfrom";
const char* const CorrelationHeader::COMPONENT_ID_TO_HEADER = "cidto";
const char* const CorrelationHeader::EXIT_CALL_TYPE_ORDER = "etypeorder";
const char* const CorrelationHeader::SNAPSHOT_ENABLE_HEADER = "snapenable";
const char* const CorrelationHeader::REQUEST_GUID_HEADER = "guid";
const char* const CorrelationHeader::MATCH_CRITERIA_TYPE_HEADER = "mctype";
const char* const CorrelationHeader::MATCH_CRITERIA_VALUE_HEADER = "mcvalue";
const char* const CorrelationHeader::ORIGIN_TIMESTAMP = "ts";
const char* const CorrelationHeader::DISABLE_TRANSACTION_DETECTION_HEADER = "notxdetect";
const char* const CorrelationHeader::DONOTRESOLVE = "donotresolve";
const char* const CorrelationHeader::DEBUG_ENABLED_HEADER = "debug";
const char* const CorrelationHeader::MUST_TAKE_SNAPSHOT = "appdynamicssnapshotenabled";

const char* const CorrelationHeader::HEADER_SEPARATOR = "*";
const char* const CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR = "=";
const char* const CorrelationHeader::VALUE_DELIMITER = ",";

const char* const CorrelationHeader::MATCH_CRITERIA_TYPE_DISCOVERED = "auto";
const char* const CorrelationHeader::MATCH_CRITERIA_TYPE_CUSTOM = "custom";

const char* const CorrelationHeader::BACKEND_CALL_PREFIX = "{[UNRESOLVED][";
const size_t CorrelationHeader::BACKEND_CALL_PREFIX_SIZE = sizeof("{[UNRESOLVED][")-1;
const char* const CorrelationHeader::BACKEND_CALL_SUFFIX = "]}";
const size_t CorrelationHeader::BACKEND_CALL_SUFFIX_SIZE = sizeof("[}")-1;

const char* const CorrelationHeader::TRUE_STRING = "true";
const char* const CorrelationHeader::ZERO_STRING = "0";

const char* const CorrelationHeader::CROSSAPP_CIDTO_PREFIX = "A";

namespace {
    enum BTPart {
        NAME, ID, TYPE, COMPONENT, MATCH_CRITERIA_TYPE, MATCH_CRITERIA_VALUE
    };

    static const char* const exitPointTypeToString[]
    {
        "HTTP",
        "DB",
        "CACHE",
        "RABBITMQ",
        "WEB_SERVICE",
        "CUSTOM",
        "JMS"
    };

    static const size_t nExitPointTypes =
        sizeof(exitPointTypeToString) / sizeof(exitPointTypeToString[0]);

    // If any of these static asserts fail, update the above table
    // before updating these asserts.
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_HTTP == 0);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_DB == 1);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_CACHE == 2);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_RABBITMQ == 3);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_WEBSERVICE == 4);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_CUSTOM == 5);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::EXIT_JMS == 6);
    BOOST_STATIC_ASSERT(appdynamics::pb::Agent::ExitPointType_ARRAYSIZE == nExitPointTypes);

    inline std::string convertBTPartToString(const boost::shared_ptr<AgentTransaction>& agentBT, BTPart part)
    {
        BOOST_ASSERT(agentBT);
        switch (part) {
            case NAME:
                return agentBT->getName();

            case ID:
                return boost::lexical_cast<std::string>(agentBT->getID());

            case TYPE:
                return agentBT->getEntryPointType();

            case COMPONENT:
                return boost::lexical_cast<std::string>(agentBT->getComponentID());

            case MATCH_CRITERIA_TYPE:
                if (agentBT->getMatchCriteria()->isAutoDiscovered())
                    return CorrelationHeader::MATCH_CRITERIA_TYPE_DISCOVERED;
                else
                    return CorrelationHeader::MATCH_CRITERIA_TYPE_CUSTOM;

            case MATCH_CRITERIA_VALUE:
                if (agentBT->getMatchCriteria()->isAutoDiscovered())
                    agentBT->getMatchCriteria()->getNamingSchemeType();
                else
                    agentBT->getMatchCriteria()->getCustomMatchRuleName();

            default:
                return std::string();
        }
    }
}

std::string ComponentLink::getUnResolvedBackendID() const
{
    size_t found = m_toComponentID.find(CorrelationHeader::BACKEND_CALL_PREFIX);
    if (found == std::string::npos)
        return std::string();
    std::string backendID(
            m_toComponentID.substr(CorrelationHeader::BACKEND_CALL_PREFIX_SIZE,
                m_toComponentID.rfind(CorrelationHeader::BACKEND_CALL_SUFFIX)-CorrelationHeader::BACKEND_CALL_PREFIX_SIZE));
    return backendID;
}

// Used in debug logging
std::ostream& operator<<(std::ostream& out, const std::vector<ComponentLink>& links)
{
    if (!links.empty()) {
        BOOST_FOREACH(const auto& link, boost::make_iterator_range(links.begin(), links.end()-1)) {
            out << "Component:" << link.getFromComponentID() << "|"
                << "Exit Call:" << link.getExitPointType() << "|"
                << "To:" << link.getToComponentID() << "|";
        }
        auto link = links.back();
        out << "Component:" << link.getFromComponentID() << "|"
            << "Exit Call:" << link.getExitPointType() << "|"
            << "To:" << link.getToComponentID();
    }
    return out;
}

void CorrelationHeaderBuilder::addSubHeader(const char* const subHeaderName, const std::string& subHeaderValue)
{
    if (!m_headerRoot.empty()) {
        m_headerRoot.append(CorrelationHeader::HEADER_SEPARATOR);
    }

    m_headerRoot.append(subHeaderName).append(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR).append(subHeaderValue);
}

void CorrelationHeaderBuilder::addComponentLinks(const std::vector<ComponentLink>& componentLinks,
                                                 const std::string& ourComponentID)
{
    size_t numLinks = componentLinks.size();
    size_t sizeEstimate = (1 + 2) * (numLinks + 1);
    m_fromComponentsSequence.reserve(sizeEstimate);
    m_toComponentsSequence.reserve(sizeEstimate);
    m_exitTypesSequence.reserve(sizeEstimate);

    BOOST_FOREACH(const auto& link, componentLinks) {
        m_fromComponentsSequence.append(link.getFromComponentID()).append(CorrelationHeader::VALUE_DELIMITER);
        m_toComponentsSequence.append(link.getToComponentID()).append(CorrelationHeader::VALUE_DELIMITER);
        m_exitTypesSequence.append(link.getExitPointType()).append(CorrelationHeader::VALUE_DELIMITER);
    }

    m_fromComponentsSequence.append(ourComponentID);
    addSubHeader(CorrelationHeader::COMPONENT_ID_FROM_HEADER, m_fromComponentsSequence);
}

std::string CorrelationHeaderBuilder::build(appdynamics::pb::Agent::ExitPointType exitType,
                                            const std::string& toComponentID,
                                            const std::string& unresolvedExitID,
                                            const std::string& exitCallSequence,
                                            bool snapshotEnabled)
{
    std::string headerValue(m_headerRoot);
    if (!headerValue.empty()) {
        headerValue.append(CorrelationHeader::HEADER_SEPARATOR);
    }

    headerValue.append(CorrelationHeader::UNRESOLVED_EXIT_ID_HEADER).append(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR).append(unresolvedExitID);
    headerValue.append(CorrelationHeader::HEADER_SEPARATOR);
    headerValue.append(CorrelationHeader::EXIT_POINT_GUID_HEADER).append(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR).append(exitCallSequence);

    const char* exitPointTypeStr = exitPointTypeToString[exitType];
    headerValue.append(CorrelationHeader::HEADER_SEPARATOR);
    headerValue.append(CorrelationHeader::COMPONENT_ID_TO_HEADER).append(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR).append(m_toComponentsSequence + toComponentID);
    headerValue.append(CorrelationHeader::HEADER_SEPARATOR);
    headerValue.append(CorrelationHeader::EXIT_CALL_TYPE_ORDER).append(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR).append(m_exitTypesSequence + exitPointTypeStr);
    if (snapshotEnabled) {
        headerValue.append(CorrelationHeader::HEADER_SEPARATOR);
        headerValue.append(CorrelationHeader::SNAPSHOT_ENABLE_HEADER).append(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR).append(CorrelationHeader::TRUE_STRING);
    }
    return headerValue;
}

/* {{{ CorrelationHeader methods */

void CorrelationHeader::parseSubHeaders(const std::string& header)
{
    typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
    boost::char_separator<char> subHeaderSep(CorrelationHeader::HEADER_SEPARATOR);
    boost::char_separator<char> valueSep(CorrelationHeader::ATTRIBUTE_VALUE_SEPARATOR);

    tokenizer subHeaders(header.begin(), header.end(), subHeaderSep);
    for (auto subHeader = subHeaders.begin(); subHeader != subHeaders.end(); ++subHeader) {
        tokenizer values(subHeader->begin(), subHeader->end(), valueSep);
        auto token = values.begin();
        std::string attribute(*token);
        if (++token != values.end())
            m_subHeaders[attribute] = *token;
        else
            m_subHeaders[attribute] = "";
    }
}

std::string CorrelationHeader::sanitizeHeader(const std::string& header)
{
    // Based on CORE-20346, it looks we have to split on ", " and act only on
    // the last header in the sequence.
    size_t found = header.rfind(", ");
    if (found == std::string::npos)
        return header;
    return header.substr(found + 2);
}

bool CorrelationHeader::getDoNotSelfResolve()
{
    if (m_didCacheDoNotSelfResolve)
        return m_doNotSelfResolve;

    m_doNotSelfResolve = getBooleanSubHeader(CorrelationHeader::DONOTRESOLVE);
    m_didCacheDoNotSelfResolve = true;
    return m_doNotSelfResolve;
}

bool CorrelationHeader::isSnapshotEnabled()
{
    if (m_didCacheSnapshotEnabled)
        return m_snapshotEnabled;

    m_snapshotEnabled = getBooleanSubHeader(CorrelationHeader::SNAPSHOT_ENABLE_HEADER);
    m_didCacheSnapshotEnabled = true;
    return m_snapshotEnabled;
}

bool CorrelationHeader::isDebugEnabled()
{
    if (m_didCacheDebugEnabled)
        return m_debugEnabled;

    m_debugEnabled = getBooleanSubHeader(CorrelationHeader::DEBUG_ENABLED_HEADER);
    m_didCacheDebugEnabled = true;
    return m_debugEnabled;
}

const std::string& CorrelationHeader::getOriginTimestamp()
{
    if (m_didCacheOriginTimestamp)
        return m_originTimestamp;

    m_originTimestamp = getSubHeader(CorrelationHeader::ORIGIN_TIMESTAMP);

    m_didCacheOriginTimestamp = true;
    return m_originTimestamp;
}

CorrelationHeader::CorrelationType CorrelationHeader::getType()
{
    if (m_didCacheCorrelationType) {
        return m_correlationType;
    }

    m_correlationType = computeType();
    m_didCacheCorrelationType = true;
    return m_correlationType;
}

CorrelationHeader::CorrelationType CorrelationHeader::computeType()
{
    if (m_subHeaders.empty())
        return NONE;

    std::string const remoteControllerGUID(getSubHeader(CONTROLLER_GUID));
    if (!remoteControllerGUID.empty()) {
        const std::string& localControllerGUID = m_configChannel->getControllerGUID();
        if (remoteControllerGUID != localControllerGUID) {
            LOG4CXX_DEBUG(m_logger, "Remote controller GUID ["
              << remoteControllerGUID
              << "] and local ["
              << localControllerGUID
              << "] do not match, not processing");
            return NONE;
        }
    }

    std::string const remoteAccountGUID(getSubHeader(ACCOUNT_GUID));
    if (!remoteAccountGUID.empty()) {
        const std::string& localAccountGUID = m_configChannel->getAccountGUID();
        if (remoteAccountGUID != localAccountGUID) {
            LOG4CXX_DEBUG(m_logger, "Remote account GUID ["
              << remoteAccountGUID
              << "] and local ["
              << localAccountGUID
              << "] do not match, not processing");
            return NONE;
        }
    }

    std::string const remoteAppID(getSubHeader(CorrelationHeader::APPLICATION_ID_HEADER));
    if (remoteAppID.empty()) {
        LOG4CXX_ERROR(m_logger, "Missing app ID in correlation header");
        return NONE;
    }

    std::string const localAppID(boost::lexical_cast<std::string>(m_configChannel->getAppID()));
    if (remoteAppID != localAppID) {
        bool crossAppDisabled = remoteControllerGUID.empty() || remoteAccountGUID.empty();
        if (crossAppDisabled) {
            LOG4CXX_DEBUG(m_logger, "Remote app ID [" << remoteAppID << "] and local app ID [" << localAppID << "] do not match, not processing");
            return NONE;
        }
        return CROSSAPP;
    }

    return INAPP;
}

void CorrelationHeader::computeComponentLinks()
{
    std::vector<std::string> fromComponentIDs, toComponentIDs, exitPointTypes;

    parseSubHeader(getSubHeader(COMPONENT_ID_FROM_HEADER), fromComponentIDs);
    parseSubHeader(getSubHeader(COMPONENT_ID_TO_HEADER), toComponentIDs);
    parseSubHeader(getSubHeader(EXIT_CALL_TYPE_ORDER), exitPointTypes);

    if ( (fromComponentIDs.size() != toComponentIDs.size())
      || (fromComponentIDs.size() != exitPointTypes.size())) {
        LOG4CXX_WARN(m_logger, "Malformed caller chain, from: [" <<
                                getSubHeader(COMPONENT_ID_FROM_HEADER)
                                << "], to: [" <<
                                getSubHeader(COMPONENT_ID_TO_HEADER) <<
                                "], exit types: [" <<
                                getSubHeader(EXIT_CALL_TYPE_ORDER) << "]");
        BOOST_ASSERT(m_componentLinks.empty());
        return;
    }

    for (int i = 0; i < fromComponentIDs.size(); i++) {
        m_componentLinks.push_back(ComponentLink(fromComponentIDs[i], toComponentIDs[i], exitPointTypes[i]));
    }

    if (getDoNotSelfResolve()) {
        int64_t const ourComponentID = m_configChannel->getTierID();
        m_componentLinks.push_back(ComponentLink(m_componentLinks.back().getToComponentID(),
                                                 boost::lexical_cast<std::string>(ourComponentID),
                                                 m_componentLinks.back().getExitPointType()));
    }
}

uint64_t CorrelationHeader::computeUnResolvedExitID()
{
    std::string const unresolvedExitIDSubHeader(getSubHeader(UNRESOLVED_EXIT_ID_HEADER));
    if (unresolvedExitIDSubHeader.empty()) {
        LOG4CXX_DEBUG(m_logger, "Empty " << UNRESOLVED_EXIT_ID_HEADER << " sub-header.");
        return 0;
    }

    try {
        uint64_t const unresolvedExitID =
            boost::lexical_cast<uint64_t>(unresolvedExitIDSubHeader);
        if (unresolvedExitID == 0) {
            LOG4CXX_WARN(m_logger, "Backend id of 0 found in "
                                   << UNRESOLVED_EXIT_ID_HEADER
                                   << " sub-header.");
        }
        return unresolvedExitID;
    }
    catch (const boost::bad_lexical_cast& e) {
        LOG4CXX_WARN(m_logger, "Failed to parse \""
                               << unresolvedExitIDSubHeader
                               << "\" as an int from "
                               << UNRESOLVED_EXIT_ID_HEADER
                               << " sub-header.");
        return 0;
    }
}


uint64_t CorrelationHeader::computeSelfResolutionBackendID()
{
    uint64_t const unresolvedExitID = getUnResolvedExitID();
    if (unresolvedExitID != 0) {
        return unresolvedExitID;
    }
    const ComponentLink& lastComponent = getComponentLinks().back();
    std::string const unresolvedExitIDString(lastComponent.getUnResolvedBackendID());
    if (unresolvedExitIDString.empty()) {
      LOG4CXX_DEBUG(m_logger, "Did not find unresolved exit id in cidto: "
                              << lastComponent.getToComponentID());
      return 0;
    }

    try {
        return boost::lexical_cast<uint64_t>(unresolvedExitIDString);
    }
    catch (const boost::bad_lexical_cast& e) {
        LOG4CXX_WARN(m_logger, "Failed to parse \""
                               << lastComponent.getToComponentID()
                               << "\" as a uint64_t.");
        return 0;
    }
}

uint64_t CorrelationHeader::computeCrossAppUnResolvedBackendID()
{
    if (getDoNotSelfResolve()) {
        return 0;
    }

    uint64_t const unresolvedBackendID = getLastLinkUnResolvedBackendID();

    if (unresolvedBackendID) {
        return unresolvedBackendID;
    }

    uint64_t const thisAppId = m_configChannel->getAppID();
    uint64_t const cidToAppId = getLastLinkAppID();

    if (thisAppId == cidToAppId) {
        return 0;
    }

    uint64_t const unresolvedBackendId = getUnResolvedExitID();
    if (!unresolvedBackendId) {
        return 0;
    }

    return unresolvedBackendID;
}

uint64_t CorrelationHeader::computeLastLinkUnResolvedBackendID()
{
    const ComponentLink& lastLink = getComponentLinks().back();
    std::string lastComponentUnResolvedBackendId(lastLink.getUnResolvedBackendID());
    if (lastComponentUnResolvedBackendId.empty()) {
        return 0;
    }

    try {
        uint64_t const unresolvedBackendID =
            boost::lexical_cast<uint64_t>(lastComponentUnResolvedBackendId);
        if (!unresolvedBackendID) {
            LOG4CXX_WARN(m_logger, "Found backend id of 0 in cidto"
                                 << lastLink.getToComponentID());
            return 0;
        }

        return unresolvedBackendID;
    }
    catch (const boost::bad_lexical_cast& e) {
        LOG4CXX_WARN(m_logger, "Unable to parse "
                               << lastComponentUnResolvedBackendId
                               << " as a uint64_t from cidto "
                               << lastLink.getToComponentID());
        return 0;
    }

}

uint64_t CorrelationHeader::computeLastLinkAppID()
{
    const ComponentLink& lastLink = getComponentLinks().back();
    const std::string& lastLinkToComponentID(lastLink.getToComponentID());
    bool const lastCIDToIsAppId =
        boost::algorithm::starts_with(lastLinkToComponentID,
                                     CorrelationHeader::CROSSAPP_CIDTO_PREFIX);
    if (!lastCIDToIsAppId)
        return 0;

    std::string cidToAppIdString(lastLinkToComponentID, 1, lastLinkToComponentID.length() - 1);
    try {
        uint64_t const cidToAppId =
            boost::lexical_cast<uint64_t>(cidToAppIdString);

        return cidToAppId;
    }
    catch (const boost::bad_lexical_cast& e) {
        LOG4CXX_WARN(m_logger, "Unable to parse \""
                               << cidToAppIdString
                               << "\" to a uint64_t from a cross app cidto: "
                               << lastLinkToComponentID);
    }

    return 0;
}

void CorrelationHeader::parseSubHeader(const std::string& subHeader, std::vector<std::string>& parts)
{
    parts.clear();
    if (!subHeader.empty())
        boost::split(parts, subHeader, boost::is_any_of(VALUE_DELIMITER));
}

const std::string& CorrelationHeader::getExitCallSequence()
{
    if (m_didCacheExitCallSequence)
        return m_exitCallSequence;

    m_exitCallSequence = getSubHeader(CorrelationHeader::EXIT_POINT_GUID_HEADER);
    m_didCacheExitCallSequence = true;
    return m_exitCallSequence;
}

/* }}} */

/* {{{ TransactionCorrelator methods */

std::string TransactionCorrelator::getCorrelationHeader(TransactionContext* context,
                                                        CurrentExitCall* exitCall,
                                                        const AgentLogger& logger)
{
    BOOST_ASSERT(exitCall);
    appdynamics::pb::Agent::ExitPointType const exitType =
        exitCall->getExitCallInfo()->getExitPointType();
    const std::string& toComponentID = exitCall->getComponentID();

    boost::shared_ptr<CorrelationHeader> incomingHeader(context->getCorrelationHeader());
    bool const isCrossApp =
        incomingHeader && (incomingHeader->getType() == CorrelationHeader::CROSSAPP);

    if (isCrossApp) {
        incomingHeader.reset();
    }

    if (exitCall->isPropagationDisabled()) {
        CorrelationHeaderBuilder headerBuilder;
        headerBuilder.addSubHeader(CorrelationHeader::DISABLE_TRANSACTION_DETECTION_HEADER, CorrelationHeader::TRUE_STRING);

        if (incomingHeader && incomingHeader->isDebugEnabled())
            headerBuilder.addSubHeader(CorrelationHeader::DEBUG_ENABLED_HEADER, CorrelationHeader::TRUE_STRING);

        std::string corrHeader(headerBuilder.build());

        if (logger->isDebugEnabled()) {
            LOG4CXX_DEBUG(logger, "disabling correlation header generated [" << corrHeader << "]");
        }

        return corrHeader;
    }

    appdynamics::pb::BTInfoResponse* btInfoResponse = context->getRequestContext()->getBTInfoResponse();
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();

    boost::shared_ptr<CorrelationHeaderBuilder> headerBuilder(context->getCorrelationHeaderBuilder());

    if (!headerBuilder) {
        headerBuilder = boost::make_shared<CorrelationHeaderBuilder>();
        context->setCorrelationHeaderBuilder(headerBuilder);
        int64_t appID = txMonitor->getConfigChannel()->getAppID();
        headerBuilder->addSubHeader(CorrelationHeader::APPLICATION_ID_HEADER,
                                    boost::lexical_cast<std::string>(appID));

        headerBuilder->addSubHeader(CorrelationHeader::CONTROLLER_GUID,
                                    txMonitor->getConfigChannel()->getControllerGUID());

        headerBuilder->addSubHeader(CorrelationHeader::ACCOUNT_GUID,
                                    txMonitor->getConfigChannel()->getAccountGUID());

        const boost::shared_ptr<AgentTransaction>& agentBT = context->getTransaction();
        if (agentBT->isRegistered()) {
            headerBuilder->addSubHeader(CorrelationHeader::BUSINESS_TRANSACTION_ID_HEADER,
                                    convertBTPartToString(agentBT, ID));

        } else {
            headerBuilder->addSubHeader(CorrelationHeader::BUSINESS_TRANSACTION_NAME_HEADER,
                                    convertBTPartToString(agentBT, NAME));
            headerBuilder->addSubHeader(CorrelationHeader::ENTRY_POINT_TYPE_HEADER,
                                    convertBTPartToString(agentBT, TYPE));
            headerBuilder->addSubHeader(CorrelationHeader::BT_COMPONENT_MAPPING_HEADER,
                                    convertBTPartToString(agentBT, COMPONENT));
            headerBuilder->addSubHeader(CorrelationHeader::MATCH_CRITERIA_TYPE_HEADER,
                                    convertBTPartToString(agentBT, MATCH_CRITERIA_TYPE));
            headerBuilder->addSubHeader(CorrelationHeader::MATCH_CRITERIA_VALUE_HEADER,
                                    convertBTPartToString(agentBT, MATCH_CRITERIA_VALUE));
        }

        const std::string& requestGUID = context->getRequestContext()->getRequestGUID();
        headerBuilder->addSubHeader(CorrelationHeader::REQUEST_GUID_HEADER, requestGUID);

        if (incomingHeader && incomingHeader->isDebugEnabled()) {
            headerBuilder->addSubHeader(CorrelationHeader::DEBUG_ENABLED_HEADER, CorrelationHeader::TRUE_STRING);
        }

        headerBuilder->addSubHeader(
                CorrelationHeader::ORIGIN_TIMESTAMP,
                boost::lexical_cast<std::string>(context->getStartTime().getSkewAdjustedWallTime()));

        headerBuilder->addComponentLinks(
                incomingHeader ? incomingHeader->getComponentLinks() : std::vector<ComponentLink>(),
                boost::lexical_cast<std::string>(txMonitor->getConfigChannel()->getTierID()));
    }

    // Set snapenable=true header only if:
    // a) it's a continuing transaction and upstream tier told us to take snapshot,
    // b) otherwise if we got a response from the proxy and the response
    //    told us that snapshot is required.
    bool enableSnapshot = false;
    if ((context->isContinuingTransaction() && incomingHeader->isSnapshotEnabled()) ||
        (btInfoResponse && btInfoResponse->issnapshotrequired()))
        enableSnapshot = true;

    std::string corrHeader(headerBuilder->build(exitType,
                                                toComponentID,
                                                getUnresolvedExitIdHeader(exitCall),
                                                getExitCallSequenceString(context, exitCall->getSequenceNumber()),
                                                enableSnapshot));

    if (logger->isDebugEnabled()) {
        LOG4CXX_DEBUG(logger, "correlation header generated [" << corrHeader << "]");
    }

    return corrHeader;
}

std::string TransactionCorrelator::getExitCallSequenceString(TransactionContext* context,
                                                             uint64_t exitCallNumber)
{
    const boost::shared_ptr<CorrelationHeader>& corrHeader(context->getCorrelationHeader());
    if ((!corrHeader) ||
        (corrHeader->getExitCallSequence().empty()) ||
        (corrHeader->getType() == CorrelationHeader::CROSSAPP)) {
        return boost::lexical_cast<std::string>(exitCallNumber);
    } else {
        std::string exitCallSequence(corrHeader->getExitCallSequence());
        std::string exitCallNumberString(boost::lexical_cast<std::string>(exitCallNumber));
        exitCallSequence.reserve(exitCallSequence.size() + 1 + exitCallNumberString.size());
        exitCallSequence.append("|");
        exitCallSequence.append(exitCallNumberString);
        return exitCallSequence;
    }
}

std::string TransactionCorrelator::getUnresolvedExitIdHeader(CurrentExitCall *exitCall)
{
    // If the backend resolves to a component/tier, the result is supposed to be
    // the backend ID, and 0 otherwise.
    if (exitCall->hasRegisteredBackendID()) {
        boost::shared_ptr<RegisteredBackendIdentifier> const backendID(
            boost::static_pointer_cast<RegisteredBackendIdentifier>(exitCall->getBackendID()));
        return boost::lexical_cast<std::string>(backendID->getRegisteredBackend().backendid());
    }
    return std::string();
}

boost::shared_ptr<CorrelationHeader>
    TransactionCorrelator::processContinuingTransaction(const std::string& correlationHeaderString,
                                                        appdynamics::pb::Agent::EntryPointType interceptingEntryPointType,
                                                        const TimePoint& startTime,
                                                        const AgentLogger& logger)
{
    if (correlationHeaderString.empty())
        return boost::shared_ptr<CorrelationHeader>();

    LOG4CXX_TRACE(logger, "Processing continuing transaction: " << correlationHeaderString);

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();

    // TODO check request context if tx is disabled by the originating tier already?
    boost::shared_ptr<CorrelationHeader> corrHeader(
            boost::make_shared<CorrelationHeader>(logger,
                                                  txMonitor->getConfigChannel(),
                                                  correlationHeaderString));


    if (corrHeader->isDebugEnabled())
        txMonitor->getRequestContext()->forceDebugLogging();

    bool disableTxDetection(
            corrHeader->getBooleanSubHeader(CorrelationHeader::DISABLE_TRANSACTION_DETECTION_HEADER));

    if (disableTxDetection) {
        LOG4CXX_DEBUG(logger, "Transaction disabled from the originating tier, not processing");
        txMonitor->disableBTDetection();
        return boost::shared_ptr<CorrelationHeader>();
    }

    if (!corrHeader->isComponentLinksValid()) {
        LOG4CXX_INFO(logger, "Correlation header not well-formed, disabling transaction detection");
        txMonitor->disableBTDetection();
        return boost::shared_ptr<CorrelationHeader>();
    }

    bool const doNotSelfResolve = corrHeader->getDoNotSelfResolve();
    if (corrHeader->getType() == CorrelationHeader::CROSSAPP) {
        if (doNotSelfResolve)
            return corrHeader;
        if (!handleCrossAppCorrelation(corrHeader, txMonitor, logger))
            return boost::shared_ptr<CorrelationHeader>();
        LOG4CXX_DEBUG(logger, "Cross App Request GUID read [" << corrHeader->getSubHeader(CorrelationHeader::REQUEST_GUID_HEADER) << "]");
        LOG4CXX_DEBUG(logger, "Cross App Snapshot enabled for continuing transaction [" << corrHeader->isSnapshotEnabled() << "]");
        return corrHeader;
    }

    if (!doNotSelfResolve) {
        if (checkForUnresolvedComponentID(corrHeader)) {
            LOG4CXX_TRACE(logger, "Found unresolved backend ID [" << corrHeader->getSelfResolutionBackendID() << "]");
        }
    }

    if (handleWrongUnresolvedExitID(corrHeader, txMonitor, logger)) {
            LOG4CXX_DEBUG(logger, "Exit id [" << corrHeader->getSubHeader(CorrelationHeader::UNRESOLVED_EXIT_ID_HEADER) << "] does not correspond to correct component. Will stop detection and correlating downstream requests");
            txMonitor->disableBTDetection();
            return boost::shared_ptr<CorrelationHeader>();
    }

    if (isCallerChainTooLong(corrHeader)) {
        LOG4CXX_DEBUG(logger, "Component chain exceeds metric length. Will not correlate current transaction.");
        return boost::shared_ptr<CorrelationHeader>();
    }

    TransactionContext* const txContext =
            txMonitor->addContinuingTransaction(corrHeader, interceptingEntryPointType, startTime);
    if (!txContext) {
        return boost::shared_ptr<CorrelationHeader>();
    }

    std::string skewAdjustedOriginTimestamp(corrHeader->getSubHeader(CorrelationHeader::ORIGIN_TIMESTAMP));
    if (!skewAdjustedOriginTimestamp.empty()) {
        LOG4CXX_TRACE(logger, "Origin timestamp header " << skewAdjustedOriginTimestamp);
    }

    // It is a bit goofy that we set this here instead of in
    // our caller but I think it is ok for now.
    txContext->setCorrelationHeader(corrHeader);

    LOG4CXX_DEBUG(logger, "Continuing transaction caller chain read [" << corrHeader->getComponentLinks() << "]");
    LOG4CXX_DEBUG(logger, "Request GUID read [" << corrHeader->getSubHeader(CorrelationHeader::REQUEST_GUID_HEADER) << "]");
    LOG4CXX_DEBUG(logger, "Snapshot enabled for continuing transaction [" << corrHeader->isSnapshotEnabled() << "]");

    return corrHeader;
}

bool TransactionCorrelator::checkForUnresolvedComponentID(const boost::shared_ptr<CorrelationHeader>& corrHeader)
{
    uint64_t backendID = corrHeader->getLastLinkUnResolvedBackendID();
    if (!backendID)
        return false;
    return true;
}

bool TransactionCorrelator::handleWrongUnresolvedExitID(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                                        const boost::shared_ptr<TransactionMonitor>& txMonitor,
                                                        const AgentLogger& logger)
{
    ExitCallRegistry* const exitCallRegistry = txMonitor->getTransactionRegistry()->getExitCallRegistry();

    uint64_t const unresolvedExitID = corrHeader->getUnResolvedExitID();
    if (!unresolvedExitID)
        return false;

    int64_t correlatedComponentID = exitCallRegistry->getComponentForForeignBackendID(unresolvedExitID);
    LOG4CXX_TRACE(logger, "unresolvedExitID: " << unresolvedExitID << " correlatedComponentID:" << correlatedComponentID);
    if (correlatedComponentID == -1)
        // No component ID is associated with the given exit call ID, bail out
        return false;

    // If the component matches our component ID, or the exit ID is bogus,
    // it means we're speaking with the expected component ID.
    if (correlatedComponentID == txMonitor->getConfigChannel()->getTierID() ||
            unresolvedExitID <= 0)
        return false;

    LOG4CXX_WARN(logger, "Exit id " << unresolvedExitID << " is associated with component "
                         << correlatedComponentID << ", not " << txMonitor->getConfigChannel()->getTierID()
                         << ". Marking backend for self-resolution.");

    exitCallRegistry->reResolveBackendToSelf(txMonitor->getTransactionReporter(),
                                             unresolvedExitID);
    return true;
}

bool TransactionCorrelator::handleCrossAppCorrelation(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                                      const boost::shared_ptr<TransactionMonitor>& txMonitor,
                                                      const AgentLogger& logger)
{
    uint64_t const thisAppId = txMonitor->getConfigChannel()->getAppID();

    uint64_t const unresolvedBackendID = corrHeader->getLastLinkUnResolvedBackendID();
    if (unresolvedBackendID) {
        LOG4CXX_DEBUG(logger, "Resolving backend "
                                << unresolvedBackendID
                                << " to app "
                                << thisAppId);
        return true;
    }


    uint64_t const cidToAppId = corrHeader->getLastLinkAppID();

    if (thisAppId == cidToAppId) {
        LOG4CXX_DEBUG(logger, "Cross app backend is already resolved to app "
                                << thisAppId
                                << ".");
        return true;
    }
    LOG4CXX_WARN(logger, "Last cross app component link referenced app "
                            << cidToAppId
                            << " this agent is in app "
                            << thisAppId
                            << ".");
    if (!cidToAppId) {
        LOG4CXX_WARN(logger, "Last cross app component link \""
                               << corrHeader->getComponentLinks().back().getToComponentID()
                               << "\" is not valid.");
    }

    uint64_t const unresolvedBackendId = corrHeader->getUnResolvedExitID();
    if (unresolvedBackendId == 0) {
        LOG4CXX_ERROR(logger, "Unable to re-resolve cross app backend, because "
                               << CorrelationHeader::UNRESOLVED_EXIT_ID_HEADER
                               << " sub-header is missing or invalid.");
        return false;
    }

    LOG4CXX_WARN(logger, "Re-resolving backend id "
                         << unresolvedBackendId
                         << " to application "
                         << thisAppId
                         << ".");
    return true;
}

bool TransactionCorrelator::isCallerChainTooLong(const boost::shared_ptr<CorrelationHeader>& corrHeader)
{
    // Borrowed from Java agent code.

    if (corrHeader->getComponentLinks().size() == 0)
        return false;

    // On average each component chain could be 51 chars long with UNRESOLVED IDs - 13x50 = 650
    int length = 0;

    //   "Component:".length() + fromComponentID.length()      + "|"
    //  ("Th:".length()        + threadAddId.length()          + "|")
    // + "Exit Call:".length() + exitPointType.name().length() + "|"
    // + "To:".length()        + toComponentID.length()        + "|";
    /*********** converts to ***************************************/
    //   10                    + fromComponentID.length()      + 1
    // + 3                     + threadAddId.length()          + 1
    // + 10                    + exitPointType.name().length() + 1
    // + 3                     + toComponentID.length()        + 1;
    BOOST_FOREACH(const auto& link, corrHeader->getComponentLinks()) {
        length += 30
                + link.getFromComponentID().size()
                + link.getExitPointType().size()
                + link.getToComponentID().size();
    }

    // BTM|BTs|BT:4888|            16
    // Component:9021|             15
    // Number of Very Slow Calls   20
    return (length + 16 + 15 + 20) > TransactionMonitor::MAX_METRIC_NAME_LENGTH;
}

/* }}} */

// vim: set fdm=marker:
