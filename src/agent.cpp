/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

extern "C" {
    #include <stdlib.h>
    #include <string.h>
    #include <sys/wait.h>
    #include <sys/stat.h>
    #include <pthread.h>
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <signal.h>

}

#include <SAPI.h>
#include <zend.h>
#include <zend_errors.h>

#include <iostream>

#if !defined(__APPLE__)
#include <sys/prctl.h>
#include <register_atfork.h>
#endif

#include "agent.h"
#include "agent_api.h"
#include "always_inline.h"
#include "caching/caching_exitpoint.h"
#include "callgraph/exec_stack_frame.h"
#include "correlation.h"
#include "db/db_exitpoint.h"
#include "emalloc_allocator.h"
#include "entrypoint.h"
#include "eum/config.h"
#include "http/httpexitpoint.h"
#include "infopoints.h"
#include "intercept.h"
#include "method_gatherer.h"
#include "naming.h"
#include "queue/queue_exitpoint.h"
#include "request_context.h"
#include "safe_module_shutdown.h"
#include "snapshot.h"
#include "util.h"
#include "webservices/soapclient_exitpoint.h"
#include "webservices/webservices_exitpoint.h"
#include "zval_helper.h"
#include "analytics_collector.h"
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits/is_same.hpp>

// The follow include needs to use <> instead of "" to make sure
// the earlier include path entries
// will have precedence over the directory containing this file.
#include <agent_version.h>

// Function pointer type for compile file handler.
typedef zend_op_array* (*t_compileFileHandleFunction)(zend_file_handle *file_handle,
                                                      int type TSRMLS_DC);

// Function pointer type for send_headers SAPI handler.
typedef int (*t_sendHeadersFunction)(sapi_headers_struct *sapi_headers TSRMLS_DC);

ZEND_BEGIN_ARG_INFO_EX(arginfo_ADExitCall_getCorrelationHeader, 0, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ADExitCall_setSQLQueryInfo, 0, 0, 1)
    ZEND_ARG_INFO(0, query)
    ZEND_ARG_ARRAY_INFO(0, bparray, 1)
ZEND_END_ARG_INFO()

static zend_function_entry ad_exit_call_methods[] = {
    PHP_ME(ADExitCall, __construct, NULL, ZEND_ACC_PRIVATE)
    PHP_ME(ADExitCall, getCorrelationHeader, arginfo_ADExitCall_getCorrelationHeader, ZEND_ACC_PUBLIC)
    PHP_ME(ADExitCall, setSQLQueryInfo, arginfo_ADExitCall_setSQLQueryInfo, ZEND_ACC_PUBLIC)
    { NULL, NULL, NULL, 0, 0 }
};

/**
 * ***********************
 * GLOBAL STATIC VARIABLES
 * ***********************
 */

ZEND_DECLARE_MODULE_GLOBALS(agent)

/* Init module */
#ifdef COMPILE_DL_APPDYNAMICS_AGENT
#undef ZEND_DLEXPORT
#define ZEND_DLEXPORT __attribute__ ((visibility("default")))
ZEND_GET_MODULE(appdynamics_agent)
#endif

// in MB
static const uint16_t defaultProxyMaxMemory = 120;

// in ms
static const int32_t defaultConfigTimeout = 20;
static const int32_t defaultCLIConfigTimeout = 5000;

static const char zmqSocketDirTemplate[] = "/tmp/ad-XXXXXX";

// true global
int agent_reserved_offset = -1;

static ZEND_INI_MH(OnEnabledFrameworks);
static ZEND_INI_DISP(OnDisplayEnabledFrameworks);
static ZEND_INI_MH(OnDITFlags);
static ZEND_INI_DISP(OnDisplayDITFlags);

/* {{{ INI Entries */
PHP_INI_BEGIN()

/* TODO see if this setting is really necessary or if we can create log4cxx
 * config on the fly using the log_dir */
STD_PHP_INI_ENTRY("agent.log4cxx_config",            "php/conf/appdynamics_agent_log4cxx.xml", \
                                                             PHP_INI_SYSTEM,     OnUpdateStringUnempty,    log4cxx_config,            zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.config_request_interval",   "10",   PHP_INI_SYSTEM,     OnUpdateLong,             config_request_interval,   zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.php_agent_root",            "",     PHP_INI_SYSTEM,     OnUpdateStringUnempty,    php_agent_root,            zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.time_update_period_us",     "500",  PHP_INI_SYSTEM,     OnUpdateLong,             time_update_period_us,     zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.bt_info_response_timeout",  "10",   PHP_INI_SYSTEM,     OnUpdateLong,             bt_info_response_timeout,  zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.reporting_linger",          "100",  PHP_INI_SYSTEM,     OnUpdateLong,             reporting_linger,          zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.proxy_output",              "",     PHP_INI_SYSTEM,     OnUpdateStringUnempty,    proxy_output_filename,     zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.proxy_ctrl_dir",            "",     PHP_INI_SYSTEM,     OnUpdateStringUnempty,    proxy_ctrl_dir,            zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.auto_launch_proxy",         "1",    PHP_INI_SYSTEM,     OnUpdateBool,             auto_launch_proxy,         zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.proxy_script",              "proxy/runProxy", \
                                                             PHP_INI_SYSTEM,     OnUpdateStringUnempty,    proxyScript,               zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.shared_timer",              "1",    PHP_INI_SYSTEM,     OnUpdateBool,             shared_timer,              zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.cli_enabled",               "0",    PHP_INI_SYSTEM,     OnUpdateBool,             cli_enabled,               zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.cli_long_running",          "1",    PHP_INI_SYSTEM,     OnUpdateBool,             cli_long_running,          zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.first_config_timeout",      "2",    PHP_INI_SYSTEM,     OnUpdateLong,             first_config_timeout,      zend_agent_globals, agent_globals)

STD_PHP_INI_ENTRY("agent.controller.hostName",       "",     PHP_INI_PERDIR,     OnUpdateStringUnempty,    controller_host,           zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.controller.port",           "-1",   PHP_INI_PERDIR,     OnUpdateLong,             controller_port,           zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.controller.ssl.enabled",    "0",    PHP_INI_PERDIR,     OnUpdateBool,             controller_ssl_enabled,    zend_agent_globals, agent_globals)

STD_PHP_INI_ENTRY("agent.applicationName",           "",     PHP_INI_PERDIR,     OnUpdateStringUnempty,    application_name,          zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.tierName",                  "",     PHP_INI_PERDIR,     OnUpdateStringUnempty,    tier_name,                 zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.nodeName",                  "",     PHP_INI_PERDIR,     OnUpdateStringUnempty,    node_name,                 zend_agent_globals, agent_globals)

STD_PHP_INI_ENTRY("agent.accountName",               "",     PHP_INI_PERDIR,     OnUpdateString,           account_name,              zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.accountAccessKey",          "",     PHP_INI_PERDIR,     OnUpdateString,           account_access_key,        zend_agent_globals, agent_globals)

STD_PHP_INI_ENTRY("agent.http.proxyHost",            "",     PHP_INI_SYSTEM,     OnUpdateString,           proxy_host,                zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.http.proxyPort",            "-1",   PHP_INI_SYSTEM,     OnUpdateLong,             proxy_port,                zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.http.proxyUser",            "",     PHP_INI_SYSTEM,     OnUpdateString,           proxy_user,                zend_agent_globals, agent_globals)
STD_PHP_INI_ENTRY("agent.http.proxyPasswordFile",    "",     PHP_INI_SYSTEM,     OnUpdateString,           proxy_password_file,       zend_agent_globals, agent_globals)
//STD_PHP_INI_ENTRY("agent.uniqueHostId",              "",     PHP_INI_SYSTEM,     OnUpdateStringUnempty,    unique_host_id,            zend_agent_globals, agent_globals)

STD_PHP_INI_ENTRY("agent.early_start_node",          "0",    PHP_INI_SYSTEM,     OnUpdateBool,             early_start_node,          zend_agent_globals, agent_globals)


PHP_INI_ENTRY_EX( "agent.enabled_frameworks",        "",     PHP_INI_SYSTEM,     OnEnabledFrameworks,      OnDisplayEnabledFrameworks)
PHP_INI_ENTRY_EX( "agent.dit",                       "",     PHP_INI_SYSTEM,     OnDITFlags,               OnDisplayDITFlags   )

PHP_INI_END()

/* }}} */

/**
* ****************************
* STATIC FUNCTION DECLARATIONS
* ****************************
*/
static void agent_init_globals(zend_agent_globals *agent_globals_p TSRMLS_DC);
static void agent_destroy_globals(zend_agent_globals *agent_globals_p TSRMLS_DC);
static void agent_lazy_init_globals(TSRMLS_D);


/**
 * *********************
 * FUNCTION DECLARATIONS
 * *********************
 */

// Pointer to the original compile function
static t_compileFileHandleFunction agent_orig_compile_file = NULL;

// Forward declaration of our compile function
static zend_op_array* agent_compile_file(zend_file_handle *file_handle, int type TSRMLS_DC);


class CallUserFuncArrayInterceptor : public AMethodInterceptor
{
    public:
        CallUserFuncArrayInterceptor()
            : AMethodInterceptor("CallUserFuncArrayInterceptor",
                                 InterceptorRegistry::CallUserFuncArrayInterceptor_ID)
        {
        }

        void* onCallableBegin(const PHPExecEnvironment* execEnv);
        void  onCallableEnd(const PHPExecEnvironment* execEnv, void* state);
        inline bool isActive(const PHPExecEnvironment* phpExecEnv) const { return true; }
        inline bool needParamsInOnMethodBegin() const { return true; }
        inline bool shouldCallOnMethodEnd() const { return true; }
        inline bool needReturnValueInOnMethodEnd() const { return true; }

    private:
        static zend_internal_function* target;
        static void (*targetHandler)(INTERNAL_FUNCTION_PARAMETERS);
        static const InterceptorSet* targetInterceptorSet;

        static void newHandler(INTERNAL_FUNCTION_PARAMETERS);

};

zend_internal_function* CallUserFuncArrayInterceptor::target = NULL;
void (*CallUserFuncArrayInterceptor::targetHandler)(INTERNAL_FUNCTION_PARAMETERS) = NULL;
const InterceptorSet* CallUserFuncArrayInterceptor::targetInterceptorSet = NULL;

static CallUserFuncArrayInterceptor callUserFuncArrayInterceptor;

/**
    Variadic template used to select arguments from
    the zend_execute and zend_execute_internal callbacks.

    Different version of php have different signatures for these
    two callbacks.  However we need to access three things in
    any callback:
        1. The current zend_op_array if we about to run a regular php function
        2. The current zend_execute_data
        3. The current zend_fcall_info if there is one.

    The agent's zend_execute and zend_execute_internal callbacks are variadic
    templates that use this template to access the three things listed above.

    The template arguments to this variadic template are the types of the arguments
    of the zend_execute or zend_execute_internal callback from which this template
    is being used.
 */
template <typename... t_Args> struct ExecuteHelper
{
};

/**
    Specialization for zend_execute callback for PHP versions before 5.5.0.
 */
template <typename... t_Args> struct ExecuteHelper<zend_op_array*, t_Args...>
{
    static inline zend_op_array* ALWAYS_INLINE_IN_RELEASE getOpArray(zend_op_array* opArray, t_Args...)
    {
        return opArray;
    }

    static inline zend_execute_data* ALWAYS_INLINE_IN_RELEASE getExecuteData(zend_op_array* opArray, t_Args...)
    {
        return EG(current_execute_data);
    }

    static inline zend_fcall_info* ALWAYS_INLINE_IN_RELEASE getFCallInfo(zend_op_array* executeData, t_Args...)
    {
        return NULL;
    }
};

#if PHP_VERSION_ID >= 70000
/**
    Specialization for zend_execute_internal for PHP versions at or after 7.0.0
 */
template <typename... t_Args> struct ExecuteHelper<zend_execute_data*, zval*, t_Args...>
{
    static inline zval* ALWAYS_INLINE_IN_RELEASE getReturnValue(zend_execute_data* executeData, zval* return_value, t_Args...)
    {
        return return_value;
    }

    static inline zend_op_array* ALWAYS_INLINE_IN_RELEASE getOpArray(zend_execute_data* executeData, zval* return_value, t_Args...)
    {
        return &(executeData->func->op_array);
    }

    static inline zend_execute_data* ALWAYS_INLINE_IN_RELEASE getExecuteData(zend_execute_data* executeData, zval* return_value, t_Args...)
    {
        return executeData;
    }

    static inline zend_fcall_info* ALWAYS_INLINE_IN_RELEASE getFCallInfo(zend_execute_data* executeData, zval* return_value, t_Args...)
    {
        return NULL;
    }
};
#endif

/**
    Specialization for zend_execute_internal for PHP versions before 5.5.0 and
    for zend_execute for PHP versions at or after 5.5.0.
    getReturnValue only applicable for PHP versions at or after 7.0.0
 */
template <typename... t_Args> struct ExecuteHelper<zend_execute_data*, t_Args...>
{
    static inline zval* ALWAYS_INLINE_IN_RELEASE getReturnValue(zend_execute_data* executeData, t_Args...)
    {
#if PHP_VERSION_ID >= 70000
      return executeData->return_value;
#else
      return NULL;
#endif    
    }

    static inline zend_op_array* ALWAYS_INLINE_IN_RELEASE getOpArray(zend_execute_data* executeData, t_Args...)
    {
#if PHP_VERSION_ID >= 70000
        return &(executeData->func->op_array);
#else
        return EG(active_op_array);
#endif
    }

    static inline zend_execute_data* ALWAYS_INLINE_IN_RELEASE getExecuteData(zend_execute_data* executeData, t_Args...)
    {
        return executeData;
    }

    static inline zend_fcall_info* ALWAYS_INLINE_IN_RELEASE getFCallInfo(zend_execute_data* executeData, t_Args...)
    {
        return NULL;
    }
};

/**
    Specialization for zend_execute_internal for PHP versions at or after 5.5.0.
 */
template <typename... t_Args> struct ExecuteHelper<zend_execute_data*, zend_fcall_info*, t_Args...>
{
    static inline zend_op_array* ALWAYS_INLINE_IN_RELEASE getOpArray(zend_execute_data*, zend_fcall_info*, t_Args...)
    {
        return NULL;
    }

    static inline zend_execute_data* ALWAYS_INLINE_IN_RELEASE getExecuteData(zend_execute_data* executeData, zend_fcall_info*, t_Args...)
    {
        return executeData;
    }

    static inline zend_fcall_info* ALWAYS_INLINE_IN_RELEASE getFCallInfo(zend_execute_data*, zend_fcall_info* fCallInfo, t_Args...)
    {
        return fCallInfo;
    }
};

// PHP 7 changes the signatures of the execute delegates again
#if PHP_VERSION_ID >= 70000
#define EXEC_FUNCTION zend_execute_ex
typedef void (*t_executeFunction)(zend_execute_data *execute_data);
typedef void (*t_executeInternalFunction)(zend_execute_data *current_execute_data,
                                          zval* return_value);
// PHP 5.5 changes the signatures of the execute delegates
// It also changes the name of the zend_execute function
#elif PHP_VERSION_ID >= 50500
#define EXEC_FUNCTION zend_execute_ex
typedef void (*t_executeFunction)(zend_execute_data *execute_data TSRMLS_DC);
typedef void (*t_executeInternalFunction)(zend_execute_data *current_execute_data,
                                          struct _zend_fcall_info *fci,
                                          int return_value_used TSRMLS_DC);
#else
#define EXEC_FUNCTION zend_execute
typedef void (*t_executeFunction)(zend_op_array *opArray TSRMLS_DC);
typedef void (*t_executeInternalFunction)(zend_execute_data *current_execute_data,
                                          int return_value_used TSRMLS_DC);
#endif

// Pointer to the original execute function
static t_executeFunction agent_orig_execute = NULL;

// Pointer to the original execute_internal function
static t_executeInternalFunction agent_orig_execute_internal = NULL;

/**
    Struct that stores the "state" void* returned
    by the onCallableBeginMethod of an interceptor and
    a pointer to the interceptor.  Storing the interceptor
    in this struct allows an interceptor to remove itself
    in its onCallableBeginMethod ( which is something one of the
    Drupal interceptors needs to do ).
 */
struct InterceptorState
{
    InterceptorState()
        : m_state(NULL)
        , m_interceptor(NULL)
    {
    }

    InterceptorState(AMethodInterceptor* i)
        : m_state(NULL)
        , m_interceptor(i)
    {
    }

    void* m_state;
    AMethodInterceptor* m_interceptor;
};

static inline void ALWAYS_INLINE_IN_RELEASE callCallableBegin(const InterceptorSet* interceptors,
                                                              PHPExecEnvironment* execEnv,
                                                              bool* const needReturnValue,
                                                              std::vector<InterceptorState,
                                                                  EMallocAllocator<InterceptorState> >* stateVars)
{
    if (!interceptors)
        return;
    const size_t interceptorCount = interceptors->size();
    stateVars->reserve(interceptorCount);
    int stateVarIndex = 0;

    // Ugh... We have to walk over all the interceptors in the
    // interceptors set once to build up the stateVar's vector
    // so each interceptor can remove itself from the interceptor
    // set if it wants to.
    BOOST_FOREACH(AMethodInterceptor* interceptor, *interceptors)
    {
        BOOST_ASSERT(interceptor != NULL);
        if (interceptor->isActive(execEnv))
            stateVars->push_back(InterceptorState(interceptor));
    }

    // Loop over the stateVars vector instead of the interceptor set
    // so that the inceptors can remove themselves from the interceptor
    // set if they want to.
    BOOST_REVERSE_FOREACH(InterceptorState& interceptorState, *stateVars)
    {
        BOOST_ASSERT(interceptorState.m_interceptor != NULL);
        AMethodInterceptor* const interceptor = interceptorState.m_interceptor;
        if (!(*needReturnValue) && interceptor->needReturnValueInOnMethodEnd())
            *needReturnValue = true;
        interceptorState.m_state =
            MethodInterceptorDelegate::safeOnCallableBegin(interceptor,
                                                           execEnv);
    }
}

static inline void ALWAYS_INLINE_IN_RELEASE callCallableEnd(const InterceptorSet* interceptors,
                                                            PHPExecEnvironment* execEnv,
                                                            const std::vector<InterceptorState,
                                                            EMallocAllocator<InterceptorState> >& stateVars)
{
    if (!interceptors)
        return;
    // Loop over the stateVars vector instead of the interceptor set
    // because the interceptor might have removed itself from the interceptor
    // set in onCallableBegin.
    BOOST_FOREACH(const InterceptorState& interceptorState, stateVars)
    {
        BOOST_ASSERT(interceptorState.m_interceptor != NULL);
        MethodInterceptorDelegate::safeOnCallableEnd(interceptorState.m_interceptor,
                                                     execEnv,
                                                     interceptorState.m_state);
    }
}

template <bool internal>
inline zend_execute_data* ALWAYS_INLINE_IN_RELEASE getExecuteDataForParentFrame(zend_execute_data* execData)
{
#if PHP_VERSION_ID >= 70000
    /* that's the very first execData, the one that
     * we'll report as the root frame.
     * Since in the PHP 5 case that frame was returned
     * as NULL and our implementation assumes so, it was
     * adapted to be NULL in the PHP 7 case also.
     */
    if (execData->prev_execute_data == NULL)
        return NULL;
    else
        return execData;
#elif PHP_VERSION_ID >= 50500
    if (internal)
        return execData;
    else
        return execData->prev_execute_data;
#else
    return execData;
#endif
}

template<typename T, typename... t_Args, typename boost::enable_if<boost::is_same<T, t_executeFunction>, int>::type = 0>
inline void ALWAYS_INLINE_IN_RELEASE executeDelegate(t_Args... args)
{
    agent_orig_execute(args...);
}

template<typename T, typename... t_Args, typename boost::enable_if<boost::is_same<T, t_executeInternalFunction>, int>::type = 0>
inline void ALWAYS_INLINE_IN_RELEASE executeDelegate(t_Args... args)
{
    agent_orig_execute_internal(args...);
}

/* {{{ agentCommonExecute */
/*
 * Guts of our execution callback.  In release builds
 * we mark this function as always inline to reduce the amount
 * of stack trace we use.
 *
 * TODO
 * Collect call graph only if transaction context is present?
 */
template <bool internal, typename T, typename... t_Args>
inline void ALWAYS_INLINE_IN_RELEASE agentCommonExecute(t_Args... args)
{
    // DO NOT ADD LOCAL VARIABLES TO THIS FUNCTION WITHOUT
    // AN EXTREMELY GOOD REASON!!!!  Local variables in this function
    // will increase the amount of stack space we add to each and every php
    // call frame.  The more stack we use, the less stack the php program can use.
    //
    // ** SETJMP/LONGJMP **
    // This function might never return!!!! If PHP does a longjmp
    // to unwind the call stack, then any value classes on the stack
    // in this function will not be destroyed!!!!
    //
    // DO NOT MAKE CHANGES HERE WITHOUT ALSO MAKING RELEVANT CHANGES TO:
    // - CallUserFuncArrayInterceptor::newHandler()
    // - SOAPClient::callHandler()
    // - SOAPClient::doRequestHandler()
    // They have a copy of most of this code and need to be kept in sync.

    zend_execute_data* const execData = getExecuteDataForParentFrame<internal>(ExecuteHelper<t_Args...>::getExecuteData(args...));
    zend_op_array* const opArray = ExecuteHelper<t_Args...>::getOpArray(args...);
    zend_fcall_info* const fCallInfo = ExecuteHelper<t_Args...>::getFCallInfo(args...);
#if PHP_VERSION_ID >= 70000
    zval* return_value = ExecuteHelper<t_Args...>::getReturnValue(args...);
#else
    zval* returnValue = NULL;
#endif

    const InterceptorSet* interceptors = NULL;
    bool needReturnValue = false;

    // We have to use the EMallocAllocator, so that we don't need
    // to worry if the destructors of these vectors are ever called
    // because php might longjmp and tear up the call stack.
    std::vector<InterceptorState, EMallocAllocator<InterceptorState> > stateVars;

    if (!AG(G)->dit_flags.disableInterceptorExecution) {
        AG(icept_engine)->onExecuteBegin(&interceptors,
                                         execData,
                                         opArray,
                                         internal);
    }

    CallGraph::ExecStackFrame stackFrame(AG(G)->callGraphCollectionState,
                                         execData,
                                         internal);

    AG(G)->exec_env->setExecuteDataOnCallableBegin(execData, fCallInfo, &stackFrame, opArray, internal);

    callCallableBegin(interceptors,
                      AG(G)->exec_env.get(),
                      &needReturnValue,
                      &stateVars);

    /* {{{ Execute op array or the internal function handler and time it */
    stackFrame.startTimer(AG(G)->callGraphCollectionState);

#if PHP_VERSION_ID < 70000
    if (!internal)
    {
        if (needReturnValue && (!EG(return_value_ptr_ptr)))
        {
            EG(return_value_ptr_ptr) = &returnValue;
        }
    }
#endif

    executeDelegate<T>(args...);

    stackFrame.onCallableEnd(AG(G)->callGraphCollectionState);
    /* }}} */

#if PHP_VERSION_ID >= 70000
    if (internal)
        AG(G)->exec_env->setExecuteDataOnCallableEnd(execData, fCallInfo, &stackFrame, opArray, internal, return_value);
    else
        AG(G)->exec_env->setExecuteDataOnCallableEnd(execData, fCallInfo, &stackFrame, opArray, internal, return_value);
#else
    AG(G)->exec_env->setExecuteDataOnCallableEnd(execData, fCallInfo, &stackFrame, opArray, internal);
#endif

    callCallableEnd(interceptors,
                    AG(G)->exec_env.get(),
                    stateVars);

//TODO: check if return_value needs to be free'd for PHP 7 and above.
#if PHP_VERSION_ID < 70000
    if (EG(return_value_ptr_ptr) == &returnValue)
    {
        if (*(EG(return_value_ptr_ptr)))
            APPD_ZVAL_PTR_DTOR(EG(return_value_ptr_ptr));

        EG(return_value_ptr_ptr) = NULL;
    }
#endif

    AG(G)->exec_env->clearExecuteData();

}
/* }}} */

/**
    Helper function that does a zend_try before delegating
    to agentCommonExecute.  When we "catch" a zend_bailout, we
    clear the call graph collection state so that the call graph
    collection state is not pointing to ExecStackFrame's that are on
    a part of the stack that does not exist anymore.

    This function is marked with no inline,
    because we don't want the JMPBUF this function puts on the stack ( due to the
    zend_try macro ) to eat up stack space in every php call frame.  We also don't
    want the setjmp in this function to prevent the compiler from omitting the frame
    pointer in any function that calls this function.
 */
template <bool internal, typename T, typename... t_Args>
static void __attribute__ ((noinline)) agentCommonExecuteWithTry(t_Args... args)
{
    AG(G)->inExecution = true;
    zend_try {
        agentCommonExecute<internal, T>(args...);
    }
    zend_catch {
        AG(G)->exec_env->clearExecuteData();
        AG(kernel)->getAgentContext()->afterBailout();
        AG(G)->inExecution = false;
        // Re-throw.
        LONGJMP(*EG(bailout), FAILURE);
    }
    zend_end_try();
    AG(G)->inExecution = false;
}

/* {{{ agentExecute */

/**
    Our main execute callback for all php functions,
    templated on the particular delegate we are wrapping. Ideally in an optimized
    build this function will contain a call instruction that delegates
    to executeDelegate in the fast path.
*/
template<bool internal, typename T, typename... t_Args>
static void agentExecute(t_Args... args)
{
    // DO NOT ADD LOCAL VARIABLES TO THIS FUNCTION WITHOUT
    // AN EXTREMELY GOOD REASON!!!!  Local variables in this function
    // will increase the amount of stack space we add to each and every php
    // call frame.  The more stack we use, the less stack the php program can use.
    //
    // ** SETJMP/LONGJMP **
    // This function might never return!!!! If PHP does a longjmp
    // to unwind the call stack, then any value classes on the stack
    // in this function will not be destroyed!!!!

    if ((AG(G)->disable_execute_callback) || (AG(G)->dit_flags.disableExecuteCallback)) {
        executeDelegate<T>(args...);
        return;
    }

    if (!AG(G)->inExecution) {
        if (!internal) {
            auto currentOpArray = ExecuteHelper<t_Args...>::getOpArray(args...);
            if (currentOpArray)
                AG(G)->callGraphCollectionState.setRootOpArray(currentOpArray);
        }

        agentCommonExecuteWithTry<internal, T>(args...);
        return;
    }
    agentCommonExecute<internal, T>(args...);
}

// Pointer to the original send header function in the SAPI.
static t_sendHeadersFunction agentOriginalSendHeaders = NULL;

// Forward declaration of our send_headers handler.
static int agentSendHeaders(sapi_headers_struct *sapi_headers TSRMLS_DC);

namespace Private
{
    /**
        Helper class that keeps the proxy java program running by using
        a background thread in the root process of the system that contains
        php ( apache or fastcgi ).
     */
    class ProxyDaemon : boost::noncopyable
    {
        public:
            /**
                Starts the proxy if needed.  This should be called from
                our MINIT hook.
                <p>
                This function will fail if there is something wrong with the
                ini settings that control running of the proxy.  This function
                will return true if the proxy is not supposed to be started or
                if the daemon will attempt to keep the proxy running.
                @return true on success, false otherwise.
             */
            static bool start();

            /**
                If the current process is the process from which start was first
                called ( there are multiple processes because php's container calls
                fork. ), then kill the proxy if one was started.  It is always
                safe to call this method.
             */
            static void stop();

            /**
                @return The directory that should be used for proxy control (e.g.
                start/stop node).
            */
            static const boost::filesystem::path& getProxyControlDirectory();

        private:
            // These constants control how often we'll attempt to start
            // the proxy.  maxStartupTimeInSeconds is the maximum amount of time
            // we expect the proxy to startup successfully.  If a proxy process runs
            // for less than this amount of time, we'll assume that it is malfunctioning
            // in some way at startup.  If we think the proxy is malfunctioning at startup
            // we'll wait for waitTimeInSeconds before trying to run the proxy again.
            // If the proxy process has run for longer than maxStartupTimeInSeconds when
            // we detect that it has stopped, then we'll try to run it again immediately.
            static const uint64_t maxStartupTimeInSeconds;
            static const uint64_t waitTimeInSeconds;


            ProxyDaemon();
            ~ProxyDaemon();
            static bool isOK();

            bool initProxyControlDirectory();
            void initProxyExecInfo();
            void exec();
            static void* threadProc(void* opaqueThis);
            void threadMethod();

            struct ExecInfo
            {
                std::string proxyOutputFile;
                std::string runProxyFileName;
                std::string proxyDirArg;
                std::string proxyCtrlDirArg;
                std::string agentLogDirArg;
                std::string controllerHostArg;
                std::string controllerPortArg;
                std::string controllerSslEnabledArg;
                std::string applicationNameArg;
                std::string tierNameArg;
                std::string nodeNameArg;
                std::string accountNameArg;
                std::string accountAccessKeyArg;
                std::string proxyHostArg;
                std::string proxyPortArg;
                std::string proxyUserArg;
                std::string proxyPasswordFileArg;
                std::string uniqueHostIdArg;
                char* argList[20];
            };

            static ProxyDaemon* s_instance;

            pid_t m_proxyPID;
            pid_t const m_rootPID;
            pthread_mutex_t m_mutex;
            pthread_cond_t m_condition;
            pthread_t m_watchdogThread;

            ExecInfo m_execInfo;

            bool m_shouldStartProxy;
            bool m_validConfig;
            bool m_shutdown;
            bool m_proxyCtrlDirIsTemporary;
            boost::filesystem::path m_proxyCtrlDir;
            uint64_t m_lastStartTime;
    };

    const uint64_t ProxyDaemon::maxStartupTimeInSeconds = 30;
    const uint64_t ProxyDaemon::waitTimeInSeconds = 60;

    ProxyDaemon* ProxyDaemon::s_instance = NULL;

    bool ALWAYS_INLINE_IN_RELEASE ProxyDaemon::start()
    {
        BOOST_ASSERT(s_instance == NULL);
        s_instance = new ProxyDaemon();
        return isOK();
    }

    void ProxyDaemon::stop()
    {
        delete s_instance;
        s_instance = NULL;
    }

    const boost::filesystem::path& ProxyDaemon::getProxyControlDirectory()
    {
        BOOST_ASSERT(s_instance != NULL);
        return s_instance->m_proxyCtrlDir;
    }

    ProxyDaemon::ProxyDaemon()
        : m_proxyPID(0)
        , m_rootPID(getpid())
        , m_shouldStartProxy(false)
        , m_validConfig(false)
        , m_shutdown(false)
        , m_proxyCtrlDirIsTemporary(false)
        , m_lastStartTime(0)
    {
        initProxyExecInfo();
        pthread_mutex_init(&m_mutex, NULL);
        pthread_cond_init(&m_condition, NULL);
        if (!m_shouldStartProxy)
            return;
        pthread_create(&m_watchdogThread, NULL, threadProc, this);
    }

    ProxyDaemon::~ProxyDaemon()
    {
        if (m_rootPID == getpid()) {
            if (m_shouldStartProxy) {
                pthread_mutex_lock(&m_mutex);
                m_shutdown = true;
                if (m_proxyPID > 0)
                    kill(m_proxyPID, SIGKILL);
                pthread_cond_signal(&m_condition);
                pthread_mutex_unlock(&m_mutex);
                pthread_join(m_watchdogThread, NULL);
            }
            if (m_proxyCtrlDirIsTemporary) {
                boost::system::error_code error;
                boost::filesystem::remove_all(m_proxyCtrlDir, error);
                if (error.value() != boost::system::errc::success)
                    logStartupError("[AD agent] could not remove socket dir");
            }
        }
        pthread_cond_destroy(&m_condition);
        pthread_mutex_destroy(&m_mutex);
    }

    bool ALWAYS_INLINE_IN_RELEASE ProxyDaemon::isOK()
    {
        if (!s_instance)
            return false;
        if (s_instance->m_validConfig)
            return true;
        return false;
    }

    bool ProxyDaemon::initProxyControlDirectory()
    {
        namespace fs = boost::filesystem;
        TSRMLS_FETCH();

        if (!m_proxyCtrlDir.empty())
            return true;

        else if (AG(proxy_ctrl_dir)) {
            fs::path path(AG(proxy_ctrl_dir));
            if (!exists(path) || !is_directory(path)) {
                logStartupError(boost::format("[AD agent] specified proxy ctrl directory is invalid, agent won't run: %1%") % path.native());
                return false;
            }
            m_proxyCtrlDir = path;
            m_proxyCtrlDirIsTemporary = false;
        }
        else {
            // The return value actually points to the modified
            // zmqSocketDirTemplate, so no need to free.
            char* rawPath = reinterpret_cast<char*>(alloca(sizeof(zmqSocketDirTemplate)));
            memcpy(rawPath, &(zmqSocketDirTemplate[0]), sizeof(zmqSocketDirTemplate));
            rawPath = mkdtemp(rawPath);
            if (!rawPath) {
                logStartupError("[AD agent] could not create proxy ctrl directory, agent won't run");
                return false;
            }
            fs::path path(rawPath);
            boost::system::error_code error;
            fs::permissions(path, fs::add_perms | fs::group_read | fs::group_exe | fs::others_read | fs::others_exe, error);
            if (error.value() != boost::system::errc::success) {
                logStartupError(boost::format(
                        "[AD agent] could not set permissions on proxy ctrl directory, agent might have problems: %1%")
                                % error.message());
            }
            m_proxyCtrlDir = path;
            m_proxyCtrlDirIsTemporary = true;
        }
        return true;
    }

    void ProxyDaemon::initProxyExecInfo()
    {
        TSRMLS_FETCH();

        try {
            if (!initProxyControlDirectory())
                return;

            // If we are not supposed to auto start the proxy ( which is handy for
            // debugging the proxy ), we don't need to check any other config
            // settings related to the proxy.
            if (!AG(auto_launch_proxy)) {
                m_validConfig = true;
                if (!AG(is_cli_sapi))
                    logStartupError(boost::format("[AD agent] not auto-launching proxy, please supply %1% ctrl directory to proxy manually") % m_proxyCtrlDir.native());
                return;
            }

            if (!AG(php_agent_root)) {
                // The installer sets the agent.php_agent_root ini settings, so
                // this should only happen if the user did not use the installer.
                logStartupError("[AD agent] agent.php_agent_root is not set in appdynamics_agent.ini. Agent is disabled.");
                return;
            }

            boost::filesystem::path const phpAgentRootPath(AG(php_agent_root));
            if (!phpAgentRootPath.is_absolute()) {
                logStartupError(boost::format("[AD agent] agent.php_agent_root: %1% is not an absolute path.  Agent is disabled.") % phpAgentRootPath);
                return;
            }

            if (!AG(proxyScript)) {
                logStartupError("[AD agent] agent.proxy_script is not set.  Agent is disabled");
                return;
            }

            boost::filesystem::path runProxy(boost::filesystem::absolute(boost::filesystem::path(AG(proxyScript)),
                                                                         phpAgentRootPath));


            int runProxyFD = open(runProxy.native().c_str(), O_RDONLY, 0);
            if (runProxyFD == -1) {
                logStartupError(boost::format("[AD agent] Invalid path for agent.proxy_script: %1%. Agent is disabled.") % runProxy);
                return;
            }
            close(runProxyFD);

            m_execInfo.runProxyFileName = runProxy.native();
            boost::filesystem::path const proxyDir(phpAgentRootPath / "proxy");
            m_execInfo.proxyDirArg = proxyDir.native();
            boost::filesystem::path const agentLogsDir(phpAgentRootPath / "logs");
            m_execInfo.proxyCtrlDirArg = m_proxyCtrlDir.native();
            m_execInfo.agentLogDirArg = agentLogsDir.native();

            if (!AG(controller_host)) {
                logStartupError("[AD agent] agent.controller.hostName is not set.  Agent is disabled.");
                return;
            }

            if (AG(controller_port) <= 0 || AG(controller_port) > 65535) {
                logStartupError("[AD agent] agent.controller.port is invalid or not set.  Agent is disabled.");
                return;
            }

            if (!AG(application_name)) {
                logStartupError("[AD agent] agent.applicationName is not set.  Agent is disabled.");
                return;
            }

            if (!AG(tier_name)) {
                logStartupError("[AD agent] agent.tierName is not set.  Agent is disabled.");
                return;
            }

            if (!AG(node_name)) {
                logStartupError("[AD agent] agent.nodeName is not set.  Agent is disabled.");
                return;
            }

            if (AG(early_start_node)) {

                m_execInfo.controllerHostArg = std::string("-Dappdynamics.controller.hostName=") + AG(controller_host);
                m_execInfo.controllerPortArg = std::string("-Dappdynamics.controller.port=") +
                    boost::lexical_cast<std::string>(AG(controller_port));
                if (AG(controller_ssl_enabled))
                    m_execInfo.controllerSslEnabledArg = "-Dappdynamics.controller.ssl.enabled=True";
                m_execInfo.applicationNameArg = std::string("-Dappdynamics.agent.applicationName=") + AG(application_name);
                m_execInfo.tierNameArg = std::string("-Dappdynamics.agent.tierName=") + AG(tier_name);
                m_execInfo.nodeNameArg = std::string("-Dappdynamics.agent.nodeName=") + AG(node_name);
                std::string accountName(AG(account_name));
                if (!accountName.empty())
                    m_execInfo.accountNameArg = std::string("-Dappdynamics.agent.accountName=") + accountName;
                std::string accountAccessKey(AG(account_access_key));
                if (!accountAccessKey.empty())
                    m_execInfo.accountAccessKeyArg = std::string("-Dappdynamics.agent.accountAccessKey=") + accountAccessKey;
                std::string proxyHost(AG(proxy_host));
                if (!proxyHost.empty())
                    m_execInfo.proxyHostArg = std::string("-Dappdynamics.http.proxyHost=") + proxyHost;
                if (AG(proxy_port) > -1)
                    m_execInfo.proxyPortArg = std::string("-Dappdynamics.http.proxyPort=") + boost::lexical_cast<std::string>(AG(proxy_port));
                std::string proxyUser(AG(proxy_user));
                if (!proxyUser.empty())
                    m_execInfo.proxyUserArg = std::string("-Dappdynamics.http.proxyUser=") + proxyUser;
                std::string proxyPasswordFile(AG(proxy_password_file));
                if (!proxyPasswordFile.empty())
                    m_execInfo.proxyPasswordFileArg = std::string("-Dappdynamics.http.proxyPasswordFile=") + proxyPasswordFile;

            }

            m_validConfig = true;

            unsigned currentIndex = 0;
            m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.runProxyFileName.c_str()); /* argv[0], the name of the program.  */
            m_execInfo.argList[currentIndex++] = "-d";
            m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.proxyDirArg.c_str());
            m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.proxyCtrlDirArg.c_str());
            m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.agentLogDirArg.c_str());

            if (AG(early_start_node)) {
                m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.controllerHostArg.c_str());
                m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.controllerPortArg.c_str());
                m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.applicationNameArg.c_str());
                m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.tierNameArg.c_str());
                m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.nodeNameArg.c_str());

                if (!m_execInfo.controllerSslEnabledArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.controllerSslEnabledArg.c_str());
                if (!m_execInfo.accountNameArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.accountNameArg.c_str());
                if (!m_execInfo.accountAccessKeyArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.accountAccessKeyArg.c_str());
                if (!m_execInfo.proxyHostArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.proxyHostArg.c_str());
                if (!m_execInfo.proxyPortArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.proxyPortArg.c_str());

                if (!m_execInfo.proxyUserArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.proxyUserArg.c_str());
                if (!m_execInfo.proxyPasswordFileArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.proxyPasswordFileArg.c_str());
                if (!m_execInfo.uniqueHostIdArg.empty())
                    m_execInfo.argList[currentIndex++] = const_cast<char*>(m_execInfo.uniqueHostIdArg.c_str());

                m_execInfo.argList[currentIndex++] = const_cast<char*>("-Dregister=true");
            }

            m_execInfo.argList[currentIndex] = NULL;

            if (AG(proxy_output_filename)) {
                boost::filesystem::path proxyOutputPath(AG(proxy_output_filename));
                m_execInfo.proxyOutputFile =
                    boost::filesystem::absolute(proxyOutputPath, agentLogsDir).native();
            }
            m_shouldStartProxy = true;
        }
        catch (std::exception& e) {
            logStartupError(boost::format("[AD agent] Unable to initialize proxy execution information! Exception: %1%") % e.what());
            m_validConfig = false;
            m_shouldStartProxy = false;
        }
        catch (...) {
            logStartupError("[AD agent] Unable to initialize proxy execution information!");
            m_validConfig = false;
            m_shouldStartProxy = false;
        }
    }

    void ProxyDaemon::exec()
    {
        m_proxyPID = 0;
        m_lastStartTime = Timer::getCurrentTime();
        /* Duplicate this process.  */
        pid_t proxyPID = fork();
        if (proxyPID == 0) {
            // Enable all signals now that we forked
            sigset_t signal_set;
            int rc = sigemptyset (&signal_set);
            BOOST_ASSERT(rc == 0);
            rc = pthread_sigmask (SIG_BLOCK, &signal_set, NULL);
            BOOST_ASSERT(rc == 0); (void)rc;
#if !defined(__APPLE__)
            // Ask to be killed when our parent dies.
            // This does not work on OS X.
            prctl(PR_SET_PDEATHSIG, SIGKILL, 0, 0, 0);
#endif
            // Set umask to 0011 (only owner can execute.)
            umask(S_IXGRP | S_IXOTH);
            static const int openMode = S_IRUSR | S_IWUSR |
                                        S_IRGRP | S_IWGRP |
                                        S_IROTH | S_IWOTH;
            static const int openFlags = O_WRONLY | O_TRUNC | O_APPEND | O_CREAT;
            int proxyOutputFD = -1;
            if (!m_execInfo.proxyOutputFile.empty())
                proxyOutputFD = open(m_execInfo.proxyOutputFile.c_str(), openFlags, openMode);
            if (proxyOutputFD != -1) {
                dup2(proxyOutputFD, 1);
                dup2(proxyOutputFD, 2);
            }
            else {
                // Not capturing stdout and stderr so close those file descriptors.
                close(1);
                close(2);
            }
            // We don't need stdin, so close it.
            close(0);
            // Close all file descriptors that are not stdin, stdout, and stderr.
            int maxFD = getdtablesize();
            for (int fd = 3; fd < maxFD; ++fd)
                close(fd);

            // start the proxy!!
            int i = execv(m_execInfo.runProxyFileName.c_str(), m_execInfo.argList);
            // we should not get here.
            logStartupError(boost::format("[AD agent] an error occurred in execv %1% %2%") % i % errno);
            abort();
        }
        else if (proxyPID < 0) {
            logStartupError("[AD agent] something is wrong in fork");
        }
        m_proxyPID = proxyPID;
    }

    void* ProxyDaemon::threadProc(void* opaqueThis)
    {
        // Disable all signals in the watchdog thread
        sigset_t signal_set;
        int rc = sigfillset (&signal_set);
        BOOST_ASSERT(rc == 0);
        rc = pthread_sigmask (SIG_BLOCK, &signal_set, NULL);
        BOOST_ASSERT(rc == 0); (void)rc;
        ProxyDaemon* pThis = reinterpret_cast<ProxyDaemon*>(opaqueThis);
        pThis->threadMethod();
        return NULL;
    }

    void ProxyDaemon::threadMethod()
    {
        pthread_mutex_lock(&m_mutex);
        while(!m_shutdown) {
            exec();
            pid_t localPID = m_proxyPID;
            if (localPID > 0) {
                pthread_mutex_unlock(&m_mutex);
                {
                    int status;
                    waitpid(localPID, &status, 0);
                }
                pthread_mutex_lock(&m_mutex);
            }

            if (!m_shutdown) {
                logStartupError("[AD agent] Proxy stopped executing.");
                uint64_t currentTimeInMS = Timer::getCurrentTime();
                uint64_t runtimeInMS = currentTimeInMS - m_lastStartTime;
                if (runtimeInMS <= (maxStartupTimeInSeconds * 1000)) {
                    logStartupError(boost::format("[AD agent] Proxy ran for %1%"
                                                  "ms, which is < %2%"
                                                  " seconds.  Waiting for %3%"
                                                  " seconds before starting proxy again.")
                                    % runtimeInMS % maxStartupTimeInSeconds % waitTimeInSeconds);
                    timespec waitTimeSpec;
                    waitTimeSpec.tv_sec = (currentTimeInMS / 1000) + waitTimeInSeconds;
                    waitTimeSpec.tv_nsec = (currentTimeInMS % 1000) * 1000000;
                    pthread_cond_timedwait(&m_condition, &m_mutex, &waitTimeSpec);
                }

            }
        }
        pthread_mutex_unlock(&m_mutex);
    }
}

static bool isFPMSAPI = false;
static bool fpmDidRootProcessInit = false;

static inline std::string getProcessName()
{
    int procSelfCmdLineFD = open("/proc/self/cmdline", O_RDONLY);
    if (procSelfCmdLineFD == -1) {
        logStartupError("[AD agent] Unable to read php-fpm command line, not starting proxy.");
        return std::string();
    }

    static const size_t readSize = 512;
    std::vector<char> buffer;
    ssize_t readRet = 1;
    size_t nBytesRead = 0;
    do {
        int currentBufferSize = buffer.size();
        buffer.resize(std::max<int>(currentBufferSize, nBytesRead + readSize));
        readRet = read(procSelfCmdLineFD, &buffer[nBytesRead], readSize);
        if (readRet > 0)
            nBytesRead += readRet;
    } while (readRet > 0);
    buffer.resize(nBytesRead);
    close(procSelfCmdLineFD);

    // By manually finding the first null terminator, we can deal
    // with strings that are or are not null terminated.
    auto i = buffer.begin();
    auto bufferEnd = buffer.end();
    while ((i != bufferEnd) && (*i != '\0'))
        ++i;

    return std::string(&buffer[0], i - buffer.begin());
}

void timerBeforeFork()
{
    TSRMLS_FETCH();
    // AG(G) can be null here if mshutdown has been called,
    // but we have not been unloaded yet *and* a fork happens.
    if (AG(G))
        AG(G)->timer.beforeFork();
}

void timerAfterForkParent()
{
    TSRMLS_FETCH();
    // AG(G) can be null here if mshutdown has been called,
    // but we have not been unloaded yet *and* a fork happens.
    if (AG(G))
        AG(G)->timer.afterForkParent();
}

void timerAfterForkChild()
{
    TSRMLS_FETCH();
    // AG(G) can be null here if mshutdown has been called,
    // but we have not been unloaded yet *and* a fork happens.
    if (AG(G))
        AG(G)->timer.afterForkChild();
}

typedef void (*t_AtForkHandler)(void);

template <void (*prepare) (void),
          void (*parent) (void),
          void (*child) (void)>
inline int registerAtFork()
{
    static bool didRegister = false;
    if (didRegister)
        return 0;
#if defined(__APPLE__)
    int const result = pthread_atfork(prepare, parent, child);
#else
    int const result = __register_atfork(prepare, parent, child, &__dso_handle);
#endif
    if (result == 0)
        didRegister = true;
    return result;
}

static inline bool finishMINIT()
{
    TSRMLS_FETCH();

    // finishMINIT is called only from the root process of the php host, so
    // this is a safe place to call ProxyDaemon::start.

    // The proxy is presumed to be running already for CLI SAPI. We do need to
    // check that we have a proxy comm directory specified either in an
    // environment variable or as an INI setting.
    if (AG(is_cli_sapi)) {
        char* envProxyCtrlDir = getenv("APPD_PROXY_CTRL_DIR");
        if (envProxyCtrlDir)
            AG(proxy_ctrl_dir) = envProxyCtrlDir;
        if (!AG(proxy_ctrl_dir)) {
            logStartupError("[AD agent] CLI mode expects proxy ctrl dir to be specified via APPD_PROXY_CTRL_DIR environment variable or agent.proxy_ctrl_dir INI setting");
            return false;
        }

        // Disable proxy auto-launch. The ProxyDaemon start() below will just
        // initialize communication settings.
        AG(auto_launch_proxy) = false;
    }

    uint64_t const timeUpdatePeriodNanos =
            static_cast<uint64_t>(AG(time_update_period_us)) * static_cast<uint64_t>(1000);

    int atForkRet = registerAtFork<timerBeforeFork, timerAfterForkParent, timerAfterForkChild>();
    if (atForkRet)
        logStartupError(boost::format("[AD agent] pthread_atfork for timer failed:%1%") % atForkRet);

    AG(G)->timer.init(AG(shared_timer), timeUpdatePeriodNanos);


    // finishMINIT is called only from the root process of the php host, so
    // this is a safe place to call ProxyDaemon::start
    if (!Private::ProxyDaemon::start())
        return false;

    AG(is_disabled) = false;

    if (!AG(G)->dit_flags.disableEUM) {
        agentOriginalSendHeaders = sapi_module.send_headers;
        sapi_module.send_headers = agentSendHeaders;
    }

    return true;
}

void fpmBeforeFork()
{
    if (fpmDidRootProcessInit)
        return;

    std::string processName(getProcessName());
    if (processName.find("master process") == std::string::npos)
        return;

    fpmDidRootProcessInit = true;
    finishMINIT();
}

void fpmAfterFork()
{
}

/**
 * Module init callback.
 */
PHP_MINIT_FUNCTION(agent) {
    int i;

    preForkInitLogging();

    ZEND_INIT_MODULE_GLOBALS(agent, agent_init_globals, agent_destroy_globals);

    REGISTER_INI_ENTRIES();

    REGISTER_LONG_CONSTANT("AD_WEB",             appdynamics::pb::Agent::EntryPointType::PHP_WEB,         CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_MVC",             appdynamics::pb::Agent::EntryPointType::PHP_MVC,         CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_DRUPAL",          appdynamics::pb::Agent::EntryPointType::PHP_DRUPAL,      CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_WORDPRESS",       appdynamics::pb::Agent::EntryPointType::PHP_WORDPRESS,   CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_CLI",             appdynamics::pb::Agent::EntryPointType::PHP_CLI,         CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_WEBSERVICE",      appdynamics::pb::Agent::EntryPointType::PHP_WEB_SERVICE, CONST_CS | CONST_PERSISTENT);

    REGISTER_LONG_CONSTANT("AD_EXIT_HTTP",       appdynamics::pb::Agent::EXIT_HTTP,       CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_EXIT_DB",         appdynamics::pb::Agent::EXIT_DB,         CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_EXIT_CACHE",      appdynamics::pb::Agent::EXIT_CACHE,      CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_EXIT_RABBITMQ",   appdynamics::pb::Agent::EXIT_RABBITMQ,   CONST_CS | CONST_PERSISTENT);
    REGISTER_LONG_CONSTANT("AD_EXIT_WEBSERVICE", appdynamics::pb::Agent::EXIT_WEBSERVICE, CONST_CS | CONST_PERSISTENT);

    zend_class_entry ce;
    INIT_CLASS_ENTRY(ce, "ADExitCall", ad_exit_call_methods);
    exit_call_ce = zend_register_internal_class(&ce TSRMLS_CC);
    exit_call_ce->create_object = php_exit_call_create;
#if PHP_VERSION_ID >= 70000
    exit_call_ce->ce_flags |= ZEND_ACC_FINAL;
#else
    exit_call_ce->ce_flags |= ZEND_ACC_FINAL_CLASS;
#endif

    // Figure out if the agent should be disabled, either due to bad
    // config or the fact that we are running in the command line
    // version of php.
    //
    // We don't write to AG(is_disabled) until the end of this function
    // so all the early outs in the function are leaving the agent in
    // the disabled state, even though they return 'SUCCESS'.  Reading
    // the php code it looks like it would be a bad idea to return anything other
    // than 'SUCCESS' from this function.

    AG(is_cli_sapi) = strcmp("cli", sapi_module.name) == 0;

    if (AG(is_cli_sapi) && !AG(cli_enabled))
        return SUCCESS;

    if (!AG(log4cxx_config))
        return SUCCESS;

    boost::filesystem::path log4CXXConfig(AG(log4cxx_config));
    // It's ok if AG(php_agent_root) is null here because it only
    // needs to be non-null if we are auto-starting the proxy.  If
    // we don't have a php_agent_root path, then we require
    // log4cxx_config to be an absolute path.
    if (AG(php_agent_root))
        log4CXXConfig = boost::filesystem::absolute(log4CXXConfig, AG(php_agent_root));

    if (!log4CXXConfig.is_absolute()) {
        logStartupError(boost::format("[AD agent] unable to create absolute path to log4cxx config: %1%")
                    % log4CXXConfig);
        return SUCCESS;
    }

    if (!boost::filesystem::exists(log4CXXConfig)) {
        logStartupError(boost::format("[AD agent] log4cxx config does not exist: %1%") % log4CXXConfig);
        return SUCCESS;
    }

    zend_extension dummy_ext;
    agent_reserved_offset = zend_get_resource_handle(&dummy_ext);
    if (agent_reserved_offset == -1) {
        logStartupError("[AD agent] could not obtain resource handle, agent won't run");
        return SUCCESS;
    }

    isFPMSAPI = strcmp("fpm-fcgi", sapi_module.name) == 0;

    if (isFPMSAPI) {
        int atForkRet = registerAtFork<fpmBeforeFork, fpmAfterFork, fpmAfterFork>();
        if (atForkRet)
            logStartupError(boost::format("[AD agent] pthread_atfork failed:%1%") % atForkRet);
        return SUCCESS;
    }

    finishMINIT();

    return SUCCESS;
}

#if PHP_VERSION_ID >= 50300
static int agentExitOpCodeHandler(ZEND_OPCODE_HANDLER_ARGS)
{
    user_opcode_handler_t const previousExitOpCodeHandler =
        AG(G)->prev_exit_opcode_handler;
    if (previousExitOpCodeHandler)
    {
        int const prevHandlerRet =
            previousExitOpCodeHandler(ZEND_OPCODE_HANDLER_ARGS_PASSTHRU);
        // If the handler we delegate to does not want the longjmp to
        // happen, then we do *not* force it to happen.  If the longjmp is
        // not going to happen, then we don't need unwind our ExecStackFrame stack.
        if (prevHandlerRet != ZEND_USER_OPCODE_DISPATCH)
            return prevHandlerRet;
    }

    // retrieve the start function interceptor set and call safeOnCallableEnd
    // on every interceptor
    const InterceptorSet* targetInterceptorSet =
        AG(G)->exec_env->getInterceptionEngine()->getStartFunctionInterceptorSet().get();
    if (targetInterceptorSet)
    {
        BOOST_FOREACH(AMethodInterceptor* interceptor, *targetInterceptorSet)
        {
            BOOST_ASSERT(interceptor != NULL);
            MethodInterceptorDelegate::safeOnCallableEnd(interceptor,
                                                        AG(G)->exec_env.get(),
                                                        NULL);
        }
    }

    // If we get here there either was no previous exit opcode handler
    // or the previous handler returned ZEND_USER_OPCODE_DISPATCH.
    AG(G)->callGraphCollectionState.onExitFromCallable();

    return ZEND_USER_OPCODE_DISPATCH;
}

static inline void unHookExitOpCodeIfPossible(TSRMLS_D)
{
    // If the agent was never started we never installed
    // the opcode handler, so just bail out of here.
    if (!AG(agent_started))
        return;

    user_opcode_handler_t const currentExitHandler =
        zend_get_user_opcode_handler(ZEND_EXIT);
    if (currentExitHandler == agentExitOpCodeHandler)
    {
        zend_set_user_opcode_handler(ZEND_EXIT, AG(G)->prev_exit_opcode_handler);
        LOG4CXX_TRACE(AG(G)->log_agent, "Exit opcode handler uninstalled.");
    }
    else
    {
        LOG4CXX_ERROR(AG(G)->log_agent,
               "Unable to remove exit opcode handler! "
            << "Current Handler: "
            << ((const void*)currentExitHandler));
    }
}

static inline void hookExitOpCode(TSRMLS_D)
{
    user_opcode_handler_t const currentExitHandler =
        zend_get_user_opcode_handler(ZEND_EXIT);
    AG(G)->prev_exit_opcode_handler = currentExitHandler;

    LOG4CXX_TRACE(AG(G)->log_agent, "Exit opcode handler installed.");
    zend_set_user_opcode_handler(ZEND_EXIT, agentExitOpCodeHandler);

    if (currentExitHandler)
    {
        LOG4CXX_DEBUG(AG(G)->log_agent,
               "Exit opcode handler will delegate to previous handler at:"
            << ((const void*)currentExitHandler));
    }
}

#else

static inline void hookExitOpCode(TSRMLS_D)
{
}

static void unHookExitOpCodeIfPossible(TSRMLS_D)
{
}

#endif

/**
 * Module shutdown callback.
 */
PHP_MSHUTDOWN_FUNCTION(agent) {
    // If SAPI is still running, that means the request shutdown was not called.
    // Set the unclean_shutdown flag so that the logs don't get inundated with a
    // bunch of memory leaks that really don't matter.
    if (SG(sapi_started)) {
        if (AG(G)->log_agent)
            LOG4CXX_INFO(AG(G)->log_agent, "module shutdown called in the middle of a request, silencing memory leak messages");
        CG(unclean_shutdown) = 1;
    }

    // Tell the world we are in MSHUTDOWN and
    // should not free zval's.
    SafeModuleShutdown safeModuleShutdown;

    // Stop the proxy daemon if this process started it.  It is always
    // safe to call the stop method of ProxyDaemon.
    Private::ProxyDaemon::stop();

    if (AG(did_lazy_init))
    {
        BOOST_ASSERT(!AG(is_disabled));
        if (AG(G)->log_agent)
            LOG4CXX_INFO(AG(G)->log_agent, "module shutdown start");

        if (AG(agent_started)) {
            ErrorMonitor::uninstallErrorHandler(AG(G)->log_agent);
        }

        unHookExitOpCodeIfPossible(TSRMLS_C);
    }

    UNREGISTER_INI_ENTRIES();
    if (AG(G)->log_agent)
        LOG4CXX_INFO(AG(G)->log_agent, "module shutdown end");
#ifdef ZTS
    ts_free_id(agent_globals_id);
#else
    agent_destroy_globals(&agent_globals TSRMLS_CC);
#endif


    return SUCCESS;
}

static bool RINITstartTransaction()
{
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();

    ErrorMonitor* const errorMonitor = txMonitor->getErrorMonitor();
    AG(G)->request_context = RequestContext::create(AG(G)->exec_env,
                                                    errorMonitor,
                                                    AG(kernel)->getAgentContext()->getEUMConfig());

    txMonitor->getTransactionReporter()->setBTInfoRepsonseTimeout(AG(bt_info_response_timeout));
    txMonitor->getSnapshotManager()->setCallGraphEnabled(!AG(G)->dit_flags.disableCallGraph);
    txMonitor->onRequestBegin(AG(G)->exec_env.get(), AG(G)->request_context);

    return true;
}

/**
 * Request init callback.
 */
PHP_RINIT_FUNCTION(agent)
{
    AG(request_initialized) = false;

    // If the PHP is running in an interactive mode or purely
    // informational one, then bail out.
#if PHP_VERSION_ID >= 70000
    //TODO: check that this condition is true for both
    // interactive mode and purely informational mode.
    // CG(interactive) was removed in PHP 7.
    if (!SG(request_info).path_translated)
#else
    if (CG(interactive) || !SG(request_info).path_translated)
#endif
        return SUCCESS;

    // If the agent is disabled, then bail out.
    if (AG(is_disabled))
        return SUCCESS;

    agent_lazy_init_globals(TSRMLS_C);

    if (!AG(controller_host)) {
        LOG4CXX_ERROR(AG(G)->log_agent, "agent.controller.hostName is not set.  Agent is disabled.");
        return SUCCESS;
    }

    if (AG(controller_port) <= 0 || AG(controller_port) > 65535) {
        LOG4CXX_ERROR(AG(G)->log_agent, "agent.controller.port is invalid or not set.  Agent is disabled.");
        return SUCCESS;
    }

    if (!AG(application_name)) {
        LOG4CXX_ERROR(AG(G)->log_agent, "agent.applicationName is not set.  Agent is disabled.");
        return SUCCESS;
    }

    if (!AG(tier_name)) {
        LOG4CXX_ERROR(AG(G)->log_agent, "agent.tierName is not set.  Agent is disabled.");
        return SUCCESS;
    }

    if (!AG(node_name)) {
        LOG4CXX_ERROR(AG(G)->log_agent, "agent.nodeName is not set.  Agent is disabled.");
        return SUCCESS;
    }

    if (AG(agent_started)) {
        DisableSignalsScope noSignals;

        // Force update app/tier/node name in the logging context
        log4cxx::MDC::remove("appName");
        log4cxx::MDC::remove("tierName");
        log4cxx::MDC::remove("nodeName");
        log4cxx::MDC::put("appName", AG(application_name));
        log4cxx::MDC::put("tierName", AG(tier_name));
        log4cxx::MDC::put("nodeName", AG(node_name));

        LOG4CXX_TRACE(AG(G)->log_agent, "request init start, pid " << getpid());

        // Switch to the appropriate agent context for the request, given the
        // specified agent identity
        LOG4CXX_DEBUG(AG(G)->log_agent, "agent identity is [" <<
                                        AG(application_name)  << "/" <<
                                        AG(tier_name)         << "/" <<
                                        AG(node_name)         << "/" <<
                                        AG(controller_host)   << "/" <<
                                        AG(account_name)      << "]");
        AgentIdentity agentIdentity(AG(application_name),
                                    AG(tier_name),
                                    AG(node_name),
                                    AG(controller_host),
                                    AG(account_name));
        AG(kernel)->onRequestBegin(agentIdentity);
        AG(G)->configChannel = AG(kernel)->getAgentContext()->getConfigChannel();

        AG(G)->timer.onRequestBegin();
        AG(G)->ociNameCache.onRequestBegin();

        AG(G)->configChannel->requestUpdate();

        bool const hookObjectAndResourceDestructors = AG(is_cli_sapi) && AG(cli_long_running);

        if (hookObjectAndResourceDestructors) {
            LOG4CXX_TRACE(AG(G)->log_agent, "destructor hooking enabled");
        }

        /*
         * Set up execution environment for the interception.
         */
        AG(G)->exec_env = PHPExecEnvironment::create(hookObjectAndResourceDestructors,
                                                     hookObjectAndResourceDestructors
                                                     TSRMLS_CC);
        AG(icept_engine)->onRequestBegin(AG(G)->exec_env);

        if (!AG(G)->didAddCallUserFuncInterceptor) {
            AG(icept_engine)->addInterceptorForCallable(CallableInfo("call_user_func_array"),
                                                        &callUserFuncArrayInterceptor, true);
            AG(G)->didAddCallUserFuncInterceptor = true;
        }

        long configTimeout = AG(first_config_timeout);
        if (AG(is_cli_sapi)) {
            if (configTimeout == 0)
                configTimeout = defaultCLIConfigTimeout;
        }

        AG(G)->configChannel->checkForUpdate(&agent_globals, configTimeout);

        // Set the execution callback pointers, unless disabled by the flag.
        if (!AG(G)->dit_flags.disableExecuteCallback) {
             agent_orig_execute = EXEC_FUNCTION;
             EXEC_FUNCTION = agentExecute<false, t_executeFunction>;

             agent_orig_execute_internal = zend_execute_internal;
             if (!agent_orig_execute_internal)
                agent_orig_execute_internal = execute_internal;

            zend_execute_internal = agentExecute<true, t_executeInternalFunction>;
        }

        // Set the compile callback pointer, unless disabled by the flag.
        if (!AG(G)->dit_flags.disableInterceptorInstallation) {
            agent_orig_compile_file = zend_compile_file;
            zend_compile_file = agent_compile_file;
        }

        AG(kernel)->getAgentContext()->registerBailoutHandler(&AG(G)->callGraphCollectionState);

        RINITstartTransaction();

        LOG4CXX_TRACE(AG(G)->log_agent, "request init end");
    }

    AG(request_initialized) = true;

    return SUCCESS;
}

//template <bool ON_RSHUTDOWN>
static bool RSHUTDOWNendTransaction()
{
    AnalyticsCollector *analytics_c = new AnalyticsCollector();
    analytics_c->reportData();
    delete analytics_c;

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* txContext = txMonitor->getTransactionContext();
    txMonitor->onRequestEnd(AG(G)->exec_env.get(), AG(G)->request_context, true);

    if (AG(G)->mysqlDefaultLinkHandle) {
        APPD_ZVAL_PTR_DTOR(&AG(G)->mysqlDefaultLinkHandle);
        AG(G)->mysqlDefaultLinkHandle = NULL;
    }
    if (AG(G)->pgsqlDefaultLinkHandle) {
        APPD_ZVAL_PTR_DTOR(&AG(G)->pgsqlDefaultLinkHandle);
        AG(G)->pgsqlDefaultLinkHandle = NULL;
    }

    if (!txContext)
        return false;

    return true;
}

/**
 * Request shutdown callback.
 */
PHP_RSHUTDOWN_FUNCTION(agent) {
    // Bail out if the request was not initialized
    if (!AG(request_initialized))
        return SUCCESS;

    LOG4CXX_TRACE(AG(G)->log_agent, "request shutdown start, pid " << getpid());
    if (AG(agent_started)) {
        DisableSignalsScope noSignals;

        RSHUTDOWNendTransaction();

        if (!AG(G)->dit_flags.disableExecuteCallback) {
            /*
             * Restore execution callback pointers.
             */
            EXEC_FUNCTION = agent_orig_execute;
            agent_orig_execute = NULL;

            if (agent_orig_execute_internal != execute_internal)
                zend_execute_internal = agent_orig_execute_internal;
            else
                zend_execute_internal = NULL;
            agent_orig_execute_internal = NULL;
        }

        if (!AG(G)->dit_flags.disableInterceptorInstallation) {
            if (agent_orig_compile_file)
                zend_compile_file = agent_orig_compile_file;
            agent_orig_compile_file = NULL;
        }

        AG(icept_engine)->onRequestEnd();
        AG(G)->exec_env.reset();

        AG(first_request_done) = true;

        AG(G)->request_context.reset();
        AG(G)->configChannel.reset();
        AG(G)->timer.onRequestEnd();

        AG(kernel)->onRequestEnd();
    }
    LOG4CXX_TRACE(AG(G)->log_agent, "request shutdown end");

    return SUCCESS;
}



/**
 * Module info callback. Returns the AGENT version.
 */
PHP_MINFO_FUNCTION(agent)
{
    php_info_print_table_start();
    php_info_print_table_header(2, "appdynamics_agent", AGENT_VERSION);
    php_info_print_table_end();
    DISPLAY_INI_ENTRIES();
}


CXXGlobals::CXXGlobals(TSRMLS_D)
    : callGraphCollectionState(timer)
    , delayed_functions()
    , delayed_ext_globals(TSRMLS_C)
#if PHP_VERSION_ID >= 50300
    , prev_exit_opcode_handler(NULL)
#endif
    , disable_execute_callback(false)
    , inExecution(false)
    , doesMysqlUseMysqlnd(false)
    , soapClientClassEntry(NULL)
    , soapFaultClassEntry(NULL)
    , mysqlDefaultLinkHandle(NULL)
    , pgsqlDefaultLinkHandle(NULL)
    , didAddCallUserFuncInterceptor(false)
{
}

CXXGlobals::~CXXGlobals()
{
}

/* {{{ agent_init_globals */
static void agent_init_globals(zend_agent_globals *agent_globals_p TSRMLS_DC)
{
    AG(did_lazy_init) = false;
    AG(agent_started) = false;
    AG(request_initialized) = false;
    AG(is_cli_sapi) = false;
    AG(cli_enabled) = false;
    AG(cli_long_running) = true;
    AG(first_config_timeout) = defaultConfigTimeout;
    AG(first_request_done) = false;
    AG(log4cxx_config) = NULL;
    AG(php_agent_root) = NULL;
    AG(time_update_period_us) = 0;
    AG(bt_info_response_timeout) = 10;
    AG(reporting_linger) = 100;
    AG(proxy_output_filename) = NULL;
    AG(auto_launch_proxy) = false;
    AG(icept_engine) = NULL;
    AG(kernel) = NULL;
    AG(proxy_ctrl_dir) = NULL;
    AG(proxyScript) = NULL;
    AG(shared_timer) = true;
    AG(is_disabled) = true;

    AG(controller_host) = NULL;
    AG(controller_port) = -1;
    AG(controller_ssl_enabled) = false;
    AG(application_name) = NULL;
    AG(tier_name) = NULL;
    AG(node_name) = NULL;
    AG(account_name) = "";
    AG(account_access_key) = "";
    AG(proxy_host) = "";
    AG(proxy_port) = -1;
    AG(proxy_user) = "";
    AG(proxy_password_file) = "";
    AG(early_start_node) = false;

    CXXGlobals::create(TSRMLS_C);
}
/* }}} */

/* {{{ agent_destroy_globals */
static void agent_destroy_globals(zend_agent_globals *agent_globals_p TSRMLS_DC)
{
    /*
     * It will be safe to destroy lazily-init'ed globals here. The parent
     * process will have these as NULL, but it's safe to delete them.
     */
    delete AG(icept_engine);
    delete AG(kernel);
    CXXGlobals::destroy(TSRMLS_C);
}
/* }}} */

static const char variablesOrderSettingName[] = "variables_order";
static const size_t variablesOrderSettingNameLen = sizeof(variablesOrderSettingName);

/**
    Checks the value of the variables_order setting to ensure
    that it contains "S" which causes the $_SERVER superglobal
    to be populated.  If the $_SERVER superglobal is not present
    we need to log a message indicating that the agent is will not
    be able to analyze request URL's to generate BT names.

    @return true if the variables_order setting's value is known to contain
    'S', false otherwise.
 */
static inline bool checkVariablesOrderSetting(TSRMLS_D)
{
    const char* const variablesOrderValue =
        PG(variables_order) ? PG(variables_order) : "";

    bool variablesOrderContainsS =
        (strchr(variablesOrderValue, 'S') ||
         strchr(variablesOrderValue, 's'));

    if (variablesOrderContainsS)
        return true;

    LOG4CXX_ERROR(AG(G)->log_agent,
              "variables_order setting value '" <<
              variablesOrderValue <<
              "' does not contain 'S'.  " <<
              "The 'PHP Web' entry point will not function properly!");
    return false;

}

static inline void checkIfMysqlUsesMysqlnd(TSRMLS_D)
{
    DelayedExtensionGlobals::t_ModuleHashTable moduleHashTable(DelayedExtensionGlobals::t_ModuleHashTable::share(&module_registry));
    const zend_module_entry* module = NULL;
    if (!moduleHashTable.find("mysql", sizeof("mysql"), &module))
        return;
    if (module && module->deps) {
        const zend_module_dep *dep = module->deps;
        while (dep->name) {
            if (dep->type == MODULE_DEP_REQUIRED && !strcmp(dep->name, "mysqlnd")) {
                AG(G)->doesMysqlUseMysqlnd = true;
                break;
            }
            ++dep;
        }
    }
}

enum SuhosinStatus
{
    Agent_Suhosin_Compatible,
    Agent_Suhosin_InCompatible
};

static inline SuhosinStatus checkForSuhosinLongRunningCLI(const AgentLogger& logger TSRMLS_DC)
{
    if (!AG(is_cli_sapi))
        return Agent_Suhosin_Compatible;
    if (!AG(cli_long_running))
        return Agent_Suhosin_Compatible;

    static const char SUHOSIN_PATCH[] = "SUHOSIN_PATCH";
    static const size_t SUHOSIN_PATCH_LEN = sizeof(SUHOSIN_PATCH) - 1;

#if PHP_VERSION_ID >= 70000
    zval* result = zend_get_constant_str(SUHOSIN_PATCH, SUHOSIN_PATCH_LEN);
    if (result == NULL) {
        return Agent_Suhosin_Compatible;
    }
    // no need to destruct result, constructor was not called on result in zend_get_constant_str()
#else
    zval zvalForSuhosinPatch;
    bool const hasSuhosinPatch =
        zend_get_constant(const_cast<char*>(SUHOSIN_PATCH),
                          SUHOSIN_PATCH_LEN,
                          &zvalForSuhosinPatch TSRMLS_CC);
    if (!hasSuhosinPatch)
        return Agent_Suhosin_Compatible;
    zval_dtor(&zvalForSuhosinPatch);
#endif

    LOG4CXX_ERROR(logger,
                  "FATAL: Suhosin patch detected when SAPI is cli and agent.cli_long_running is true, disabling agent!");
    return Agent_Suhosin_InCompatible;
}

/* {{{ agent_lazy_init_globals */
static void agent_lazy_init_globals(TSRMLS_D)
{
    // If we already did the lazy init, then bailout.
    if (AG(did_lazy_init))
        return;

    // If the agent is disabled, via the is_disabled agent
    // global, then RINIT ( nor anyone else ) should never call this function.
    BOOST_ASSERT(!AG(is_disabled));

    // If we failed to get a reserver field offset, then
    // AG(is_disabled) should be true and RINIT ( nor anyone else ) should never
    // call this function.
    BOOST_ASSERT(agent_reserved_offset != -1);

    BOOST_ASSERT(AG(log4cxx_config));
    /* Init logging */
    boost::filesystem::path log4CXXConfig(AG(log4cxx_config));
    // It's ok if AG(php_agent_root) is null here because it only
    // needs to be non-null if we are auto-starting the proxy.  If we don't
    // have a agent_root, then we require that the config file path
    // be an absolute path.
    if (AG(php_agent_root))
        log4CXXConfig = boost::filesystem::absolute(log4CXXConfig, AG(php_agent_root));
    // The logic in MINIT should prevent us from getting here
    // if the log4CXXConfig is not an absolute path.
    BOOST_ASSERT(log4CXXConfig.is_absolute());
    initLogging(log4CXXConfig.native() TSRMLS_CC);
    AG(G)->log_agent = getLogger("agent");

    // If the agent and its configuration is not compatible with the Suhosin
    // patch and the Suhosin patch is present then refuse to startup the agent.
    if (checkForSuhosinLongRunningCLI(AG(G)->log_agent TSRMLS_CC) == Agent_Suhosin_InCompatible) {
        // tell the world the agent is disabled and don't continue the agent
        // boot sequence.
        AG(is_disabled) = true;
        return;
    }

    AG(did_lazy_init) = true;

    // If this returns false we could refuse to start up the agent.
    // For now we'll just let it log a message, since an
    // unfavorable variables_order setting should only break the 'PHP Web'
    // entry point.  Other entry points should work irrespective of the value
    // of variables_order setting.
    checkVariablesOrderSetting(TSRMLS_C);

    checkIfMysqlUsesMysqlnd(TSRMLS_C);

    LOG4CXX_TRACE(AG(G)->log_agent, "initializing process globals");
    LOG4CXX_DEBUG(AG(G)->log_agent, "agent reserved offset: " << agent_reserved_offset);

    logDITFlags(AG(G)->log_agent, AG(G)->dit_flags);

    boost::filesystem::path logsDir;
    if (!getLoggingDirectory(&logsDir)) {
        LOG4CXX_WARN(AG(G)->log_agent, "Unable to determine agent log directory; falling back to agent root log directory.");
        logsDir = boost::filesystem::path(AG(php_agent_root)) / "logs";
    }
    AG(kernel) = new AgentKernel(Private::ProxyDaemon::getProxyControlDirectory().native().c_str(),
                                 logsDir.native(),
                                 &AG(G)->timer);
    AG(agent_started) = true;

    AG(icept_engine) = new InterceptionEngine(agent_reserved_offset);

    hookExitOpCode(TSRMLS_C);

    ErrorMonitor::installErrorHandler(AG(G)->log_agent);
}
/* }}} */


static zend_op_array* agent_compile_file(zend_file_handle *fileHandle, int type TSRMLS_DC)
{
    zend_op_array* const opArray = agent_orig_compile_file(fileHandle, type TSRMLS_CC);

    if (!opArray)
        return NULL;

    if (!(AG(G)->configChannel))
        return opArray;

    if (!(AG(G)->configChannel->isInitialized()))
        return opArray;

    try {
        AG(icept_engine)->onFileCompiled(opArray);
    }
    catch (...) {
        LOG4CXX_ERROR(AG(G)->log_agent, "onFileCompiled had an uncaught exception!");
        LOG4CXX_ERROR(AG(G)->log_agent, "filename:" << (fileHandle->filename ? fileHandle->filename : ""));
        LOG4CXX_ERROR(AG(G)->log_agent, "opened_path:" << (fileHandle->opened_path ? ZSTR_VAL(fileHandle->opened_path) : ""));
    }

    return opArray;
}

/**
    SAPI send_headers callback.  We replace the real send_headers call back
    so we can be notified when the http response headers are about to be flushed.  This
    allows us to add extra headers for EUM.
 */
static int agentSendHeaders(sapi_headers_struct *sapi_headers TSRMLS_DC)
{
    bool eumEnabled = true;
    if (AG(agent_started)) {
        const boost::shared_ptr<AgentContext>& agentContext = AG(kernel)->getAgentContext();
        if (!(AG(G)->request_context))
            eumEnabled = false;
        else if (!(agentContext->getEUMConfig()))
            eumEnabled = false;
        else if (!(agentContext->getEUMConfig()->enabled()))
            eumEnabled = false;
        else {
            TransactionContext* const txContext =
                agentContext->getTransactionMonitor()->getTransactionContext();
            if (txContext && txContext->getOrigin() == TransactionContext::API) {
                LOG4CXX_ERROR(AG(G)->log_agent, "current transaction started by agent API call, not sending EUM headers.");
                eumEnabled = false;
            }
        }
    } else {
        eumEnabled = false;
    }

    if (eumEnabled) {
        boost::shared_ptr<const PHPExecEnvironment> const execEnv(AG(G)->exec_env);
        // We need to add a few headers, so temporarily clear the headers_sent flag.
        unsigned char originalHeadersSent = SG(headers_sent);
        SG(headers_sent) = 0;
        AG(G)->request_context->eumContext().onSendHeaders(execEnv,
                                                           AG(G)->request_context,
                                                           AG(G)->timer);
        // Restore the original value of the headers_sent flag.
        SG(headers_sent) = originalHeadersSent;
    }

    if (!agentOriginalSendHeaders)
        return SAPI_HEADER_DO_SEND;

    return agentOriginalSendHeaders(sapi_headers TSRMLS_CC);

}

/* {{{ Function entries */

ZEND_BEGIN_ARG_INFO_EX(arginfo_begin_exit_call, 0, ZEND_RETURN_VALUE, 3)
    ZEND_ARG_INFO(0, type)
    ZEND_ARG_INFO(0, label)
    ZEND_ARG_ARRAY_INFO(0, properties, 0)
    ZEND_ARG_INFO(0, isExclusive)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_end_exit_call, 0, ZEND_RETURN_VALUE, 1)
    ZEND_ARG_OBJ_INFO(0, exitCall, ADExitCall, 0)
    ZEND_ARG_OBJ_INFO(0, exception, Exception, 1)
ZEND_END_ARG_INFO()

static zend_function_entry agent_functions[] = {
    PHP_FE(appdynamics_start_transaction, NULL)
    PHP_FE(appdynamics_continue_transaction, NULL)
    PHP_FE(appdynamics_end_transaction, NULL)
    PHP_FE(appdynamics_begin_exit_call, arginfo_begin_exit_call)
    PHP_FE(appdynamics_end_exit_call, arginfo_end_exit_call)
#if 0
    PHP_FE(appdynamics__get_handle_registry_size, NULL)
#endif
    { NULL, NULL, NULL, 0, 0 }
};
/* }}} */

/* {{{ Module entry */
zend_module_entry appdynamics_agent_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    "appdynamics_agent",            /* Name of the extension */
    agent_functions,                /* List of functions exposed */
    PHP_MINIT(agent),               /* Module init callback */
    PHP_MSHUTDOWN(agent),           /* Module shutdown callback */
    PHP_RINIT(agent),               /* Request init callback */
    PHP_RSHUTDOWN(agent),           /* Request shutdown callback */
    PHP_MINFO(agent),               /* Module info callback */
#if ZEND_MODULE_API_NO >= 20010901
    const_cast<char*>(AGENT_VERSION),
#endif
    STANDARD_MODULE_PROPERTIES
};
/* }}} */


static ZEND_INI_MH(OnEnabledFrameworks)
{
    std::string frameworksString(new_value ? ZSTR_VAL(new_value) : "",
        new_value ? ZSTR_LEN(new_value) : 0);
    parseFrameworksFlags(frameworksString, &(AG(G)->enabled_frameworks));
    return SUCCESS;
}

static inline std::string getINIValueToDisplay(const zend_ini_entry* iniEntry, int type)
{
    if (type == ZEND_INI_DISPLAY_ORIG && iniEntry->modified) {
        const char* value = (iniEntry->orig_value ? ZSTR_VAL(iniEntry->orig_value) : NULL );
        // the following macro expands to iniEntry->orig_value_length under PHP 5:
        int valueLen = ZSTR_LEN(iniEntry->orig_value);
        return std::string(value, valueLen);
    } else if (iniEntry->value) {
        const char* value = ZSTR_VAL(iniEntry->value);
        int valueLen = ZSTR_LEN(iniEntry->value);
        return std::string(value, valueLen);
    }
    return std::string();
}

static ZEND_INI_DISP(OnDisplayEnabledFrameworks)
{
    std::string frameworksString(getINIValueToDisplay(ini_entry, type));
    FrameworksFlags frameworksFlags;
    parseFrameworksFlags(frameworksString, &frameworksFlags);
    frameworksString = frameworksFlagsToString(frameworksFlags);
    ZEND_WRITE(frameworksString.c_str(), frameworksString.length());
}

static ZEND_INI_MH(OnDITFlags)
{
    std::string ditString(new_value ? ZSTR_VAL(new_value) : "",
        new_value ? ZSTR_LEN(new_value) : 0);
    parseDITFlags(ditString, &(AG(G)->dit_flags));
    return SUCCESS;
}

static ZEND_INI_DISP(OnDisplayDITFlags)
{
    std::string ditString(getINIValueToDisplay(ini_entry, type));
    DITFlags ditFlags;
    parseDITFlags(ditString, &ditFlags);
    ditString = ditFlagsToString(ditFlags);
    ZEND_WRITE(ditString.c_str(), ditString.length());
}

// If asserts are enabled we have to force boost to use a custom handler,
// because the php headers define NDEBUG.  However, our custom handler
// can just thunk to the regular assert handler, because NDEBUG does not cause
// the regular hander to not be defined in the CRT.
#if (defined(BOOST_ENABLE_ASSERT_HANDLER) && BOOST_ENABLE_ASSERT_HANDLER)

// forward declaration from assert.h
#if defined(__APPLE__)
extern "C" void __assert_rtn(const char *function,
                             const char *file,
                             int line,
                             const char *expr) __attribute__((__noreturn__));
#else
extern "C" void __assert_fail (__const char *__assertion,
                               __const char *__file,
                               unsigned int __line,
                               __const char *__function) __THROW __attribute__ ((__noreturn__));
#endif

namespace boost
{
    void assertion_failed(char const * expr,
                          char const * function,
                          char const * file,
                          long line)
    {
#if defined(__APPLE__)
        __assert_rtn(function, file, line, expr);
#else
        __assert_fail(expr, file, line, function);
#endif
    }

    void assertion_failed_msg(char const * expr,
                              char const * msg,
                              char const * function,
                              char const * file,
                              long line)
    {
        size_t exprLen = strlen(expr);
        size_t msgLen = strlen(msg);
        static const size_t seperatorLen = sizeof(" - ") - 1;
        size_t bufferLen = exprLen + msgLen + seperatorLen + 1;
        char* buffer = reinterpret_cast<char*>(alloca(bufferLen));
        char* dest = buffer;
        memcpy(dest, msg, msgLen);
        dest += msgLen;
        memcpy(dest, " - ", seperatorLen);
        dest += seperatorLen;
        memcpy(dest, expr, exprLen);
        buffer[bufferLen - 1] = '\0';
        assertion_failed(buffer, function, file, line);
    }
}

#endif

void* CallUserFuncArrayInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    zend_fcall_info fci;
    zend_fcall_info_cache fcc;

    // We only support one level of call_user_func invocation
    if (target)
        return NULL;

    if (execEnv->getParamCount() < 2)
        return NULL;

    zval* callable = execEnv->getParam<zval*>(0);
    if (!callable)
        return NULL;

#if PHP_VERSION_ID >= 50300
    if (zend_fcall_info_init(callable, 0, &fci, &fcc, NULL, NULL TSRMLS_CC) == FAILURE) {
#else
    if (zend_fcall_info_init(callable, &fci, &fcc TSRMLS_CC) == FAILURE) {
#endif
        return NULL;
    }

    if (fcc.initialized == 0 || fcc.function_handler == NULL)
        return NULL;

    // We are only interested in internal functions. The user ones will go
    // through the regular execute callback.
    if (fcc.function_handler->common.type != ZEND_INTERNAL_FUNCTION)
        return NULL;

    boost::intrusive_ptr<InterceptorSet> interceptorSet(
                execEnv->getInterceptionEngine()->findExistingInterceptorSetForFunction(fcc.function_handler));
    if (!interceptorSet)
        return NULL;

    target = (zend_internal_function*)(fcc.function_handler);
    targetHandler = target->handler;
    targetInterceptorSet = interceptorSet.get();

    target->handler = CallUserFuncArrayInterceptor::newHandler;

    return NULL;
}

void CallUserFuncArrayInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
{
    if (!target)
        return;

    target->handler = targetHandler;
    target = NULL;
    targetHandler = NULL;
    targetInterceptorSet = NULL;
}

void CallUserFuncArrayInterceptor::newHandler(INTERNAL_FUNCTION_PARAMETERS)
{
    zend_execute_data* execData = EG(current_execute_data);
    PHPExecEnvironment* execEnv = AG(G)->exec_env.get();

    CallGraph::ExecStackFrame stackFrame(AG(G)->callGraphCollectionState, execData, true);
    execEnv->setExecuteDataOnCallableBegin(execData, NULL, &stackFrame, NULL, true);

    bool needReturnValue = false;
    std::vector<InterceptorState, EMallocAllocator<InterceptorState> > stateVars;
    callCallableBegin(targetInterceptorSet,
                      execEnv,
                      &needReturnValue,
                      &stateVars);

    stackFrame.startTimer(AG(G)->callGraphCollectionState);

    targetHandler(INTERNAL_FUNCTION_PARAM_PASSTHRU);

    stackFrame.onCallableEnd(AG(G)->callGraphCollectionState);
    execEnv->setExecuteDataOnCallableEnd(execData, NULL, &stackFrame, NULL, true, return_value);

    callCallableEnd(targetInterceptorSet,
                    execEnv,
                    stateVars);

    execEnv->clearExecuteData();
}

/*
 * vim: et sw=4 ts=4 fdm=marker:
 */
