#ifndef __PAYLOAD_H
#define __PAYLOAD_H

#include <boost/utility.hpp>

class IServerEnvironment {
    public:
        virtual ~IServerEnvironment() {}

        virtual char* getMethod() = 0;
        virtual char* getURL() = 0;
};

class ApacheEnvironment : public IServerEnvironment {
    public:
        virtual char* getMethod();
        virtual char* getURL();
};

class IMatchPointPayload : public boost::noncopyable
{
    public:
        virtual ~IMatchPointPayload() {}
};

#endif /* __PAYLOAD_H */
