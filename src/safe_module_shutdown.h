/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */
#ifndef __safe_module_shutdown_h
#define __safe_module_shutdown_h

#include <boost/utility.hpp>
#include <stdint.h>

/**
    Scope object that is used to determine when
    we are in the MSHUTDOWN callback.  When in that
    callback we should not attempt to free *any* zval.
 */
class SafeModuleShutdown : boost::noncopyable
{
    public:
        SafeModuleShutdown();
        ~SafeModuleShutdown();
        static inline bool inModuleShutdown() { return s_inModuleShutdown; }
    private:
        static bool s_inModuleShutdown;
};

/**
    Wraps an object with a destructor such that the
    objects destructor will not be called during module
    shutdown.
 */
template <class t_Type>
class SafeModuleShutdownWrapper
{
public:
    SafeModuleShutdownWrapper();
    SafeModuleShutdownWrapper(const SafeModuleShutdownWrapper<t_Type>& other);
    SafeModuleShutdownWrapper(const t_Type& obj);
    ~SafeModuleShutdownWrapper();

    const t_Type& get() const;
    t_Type& get();

    SafeModuleShutdownWrapper<t_Type>& operator=(const SafeModuleShutdownWrapper<t_Type>& other);

    template <class t_Other>
    SafeModuleShutdownWrapper<t_Type>& operator=(const t_Other& other);

    operator bool() const;

    operator const t_Type&() const;

    operator t_Type&();

private:
    const uint8_t* getBytes() const;
    uint8_t* getBytes();

    union
    {
        void* __align;
        uint8_t bytes[sizeof(t_Type)];
    } m_alignedBytes;
};

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::SafeModuleShutdownWrapper()
{
    new (getBytes()) t_Type();
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::SafeModuleShutdownWrapper(const SafeModuleShutdownWrapper<t_Type>& other)
{
    new (getBytes()) t_Type(other.get());
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::SafeModuleShutdownWrapper(const t_Type& obj)
{
    new (getBytes()) t_Type(obj);
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::~SafeModuleShutdownWrapper()
{
    if (SafeModuleShutdown::inModuleShutdown())
        return;
    get().~t_Type();
}

template <class t_Type>
inline const t_Type& SafeModuleShutdownWrapper<t_Type>::get() const
{
    const t_Type* ptr =
        reinterpret_cast<const t_Type*>(getBytes());
    return *ptr;
}

template <class t_Type>
inline t_Type& SafeModuleShutdownWrapper<t_Type>::get()
{
    t_Type* ptr =
        reinterpret_cast<t_Type*>(getBytes());
    return *ptr;
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>& SafeModuleShutdownWrapper<t_Type>::operator=(const SafeModuleShutdownWrapper<t_Type>& other)
{
    get() = other.get();
    return *this;
}


template <class t_Type>
template <class t_Other>
inline SafeModuleShutdownWrapper<t_Type>& SafeModuleShutdownWrapper<t_Type>::operator=(const t_Other& other)
{
    get() = other;
    return *this;
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::operator bool() const
{
    return get();
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::operator const t_Type&() const
{
    return get();
}

template <class t_Type>
inline SafeModuleShutdownWrapper<t_Type>::operator t_Type&()
{
    return get();
}

template <class t_Type>
inline const uint8_t* SafeModuleShutdownWrapper<t_Type>::getBytes() const
{
    return &(m_alignedBytes.bytes[0]);
}

template <class t_Type>
inline uint8_t* SafeModuleShutdownWrapper<t_Type>::getBytes()
{
    return &(m_alignedBytes.bytes[0]);
}

#endif
