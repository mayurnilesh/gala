/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/
#include <zend.h>
#include "common.h"
#include "intercept.h"
#include "Zend/zend_execute.h"
#include "Zend/zend_builtin_functions.h"
#include "agent.h"
#include "util.h"
#include <boost/foreach.hpp>
#include <boost/mpl/if.hpp>
#include <stdexcept>
#include <utility>
#include "dynamic_jmp_table.h"
#include "zend_hashtable.h"
#include "zval_helper.h"
#include "zend_exceptions.h"
#include "emalloc_allocator.h"
#include "callgraph/exec_stack_frame.h"
#include "request_context.h"
#include "frameworks/detector.h"
#include "http/httpentrypoint.h"
#include "webservices/webservices_entrypoint.h"

const char PHP_CONSTRUCT_METHOD[] = "__construct";
const char ROOT_SYMBOL[] = "{main}";

//#if PHP_VERSION_ID >= 70000
#if 0
WebServices::NativeSOAPCallUserFuncInterceptor nativeSOAPCallUserFuncInterceptor;
#endif
bool MethodInterceptorDelegate::s_disabled = false;

static const uint32_t DISABLED_INTERCEPTOR_STATE = 0xd15ab1ed;

static void php_zval_print_wrapper(const char* str, uint32_t str_length)
{
    std::cout << str;
}

std::ostream& operator<<(std::ostream& out, const CallableInfo& callable)
{
    if (!callable.getClass().empty()) {
        out << callable.getClass() << "::";
    }
    out << callable.getFunc();
    return out;
}

/**
    Interceptor that is registered on op_array's for scripts immediately after
    they are compiled.  This interceptor's onCallableEnd calls
    InterceptionEngine::updateZendFunctions.  The onCallableEnd of this interceptor
    is the earliest point we can lookup functions and classes defined by a script
    that was recently compiled.
 */
class CompiledScriptInterceptor : public AMethodInterceptor
{
    public:
        static boost::shared_ptr<CompiledScriptInterceptor> create(InterceptionEngine* interceptEngine);
        CompiledScriptInterceptor(InterceptionEngine* interceptEngine);

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return false; };
        virtual bool needReturnValueInOnMethodEnd() const { return false; };
        virtual bool shouldCallOnMethodEnd() const { return true; };

        virtual bool isActive(const PHPExecEnvironment* execEnv) const { return true; }
    private:
        InterceptionEngine* const m_interceptEngine;
};

class DisableExecuteCallback : boost::noncopyable
{
public:
    DisableExecuteCallback(const void* threadSafetyContext)
        : m_threadSafetyContext(threadSafetyContext)
    {
        TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext); (void)m_threadSafetyContext;
        AG(G)->disable_execute_callback = true;
    }

    ~DisableExecuteCallback()
    {
        TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
        AG(G)->disable_execute_callback = false;
    }
private:
    const void* m_threadSafetyContext;
};

inline boost::shared_ptr<CompiledScriptInterceptor> CompiledScriptInterceptor::create(InterceptionEngine* interceptionEngine)
{
    return boost::make_shared<CompiledScriptInterceptor>(interceptionEngine);
}

CompiledScriptInterceptor::CompiledScriptInterceptor(InterceptionEngine* interceptEngine)
    : AMethodInterceptor("CompiledScriptInterceptor",
                         InterceptorRegistry::CompiledScriptInterceptor_ID)
    , m_interceptEngine(interceptEngine)
{
}

void* CompiledScriptInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    return NULL;
}

void CompiledScriptInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                              void* state)
{
    // TODO potentially move this check to onFileCompiled()
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    if (!txMonitor->getConfigChannel()->isAgentEnabled())
        return;

    // Attempt to detect if this script is part of a framework, but only if
    // we haven't already detected a framework and the entry point of the
    // framework hasn't been hit.
    const boost::shared_ptr<RequestContext>& requestContext = phpExecEnv->getAgentGlobals().request_context;

    // txContext can be null if PHP_WEB bt detection is turned off or if an exclude
    // rule has been configured for the current URL.
    const TransactionContext* const txContext = txMonitor->getTransactionContext();
    if ((!requestContext->isFrameworkDetected()) &&
        ((!txContext) || txContext->isWeb())) {
        bool detected = FrameworkDetector::isFrameworkLoaded(phpExecEnv);
        if (detected) {
            LOG4CXX_TRACE(phpExecEnv->getAgentGlobals().log_agent, "Framework detected");
            requestContext->setFrameworkDetected(true);
        }
    }

    m_interceptEngine->updateZendFunctions();
}

AMethodInterceptor::~AMethodInterceptor()
{
}

bool AMethodInterceptor::reportErrorIfNeeded(const PHPExecEnvironment* execEnv)
{
    zval* exceptionObject = execEnv->getExceptionObject();
    if (exceptionObject) {
        ErrorMonitor* errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->reportException(execEnv, exceptionObject);
        return true;
    }
    return false;
}


/* }}} */

/* {{{ InterceptionEngine methods */

InterceptionEngine::InterceptionEngine(int zendFunctionOpArrayReservedOffset)
    : m_logger(getLogger(std::string(LogContext::INSTRUMENT) + ".InterceptionEngine"))
    , m_zendFunctionOpArrayReservedOffset(zendFunctionOpArrayReservedOffset)
    , m_compiledScriptInterceptor(CompiledScriptInterceptor::create(this))
    , m_compiledScriptInterceptorSet(InterceptorSet::create())
    , m_anyOpArraysInstrumented(false)
{
    m_compiledScriptInterceptorSet->insert(m_compiledScriptInterceptor.get(), true);
}

InterceptionEngine::~InterceptionEngine()
{
}

/**
    Helper function that uses the zend_str_tolower_copy to lower
    case the contents of a CallableInfo.
    @param info CallableInfo to lower case with zend_str_tolower_copy.
    @return Lower case version of the specified CallableInfo.
 */
static inline CallableInfo zend_str_tolower(const CallableInfo& info)
{
    return CallableInfo(zend_str_tolower(info.getClass()), zend_str_tolower(info.getFunc()));
}

void InterceptionEngine::addInterceptorForCallable(const CallableInfo& callable,
                                                   AMethodInterceptor* const interceptor,
                                                   bool resolveOnce)
{
    if (m_execEnv->getAgentGlobals().dit_flags.disableInterceptorInstallation) {
        LOG4CXX_TRACE(m_logger, "instrumentation of " << callable << " is disabled");
        return;
    }

    // We need to keep null interceptor pointers out of the interceptor sets.
    BOOST_ASSERT(interceptor != NULL);

    // There is a special interceptor set for the root symbol.  If we are
    // adding an interceptor for the root callable, we need to add
    // the interceptor to m_startFunctionInterceptorSet.
    if ((callable.getClass().empty()) && (callable.getFunc() == ROOT_SYMBOL)) {
        if (!m_startFunctionInterceptorSet)
            m_startFunctionInterceptorSet = InterceptorSet::create(callable, false);
        if (!(m_startFunctionInterceptorSet->insert(interceptor, false).second))
            LOG4CXX_DEBUG(m_logger, "interceptor " << interceptor->getClass() << " for " << ROOT_SYMBOL << " already exists");
        return;
    }

    CallableInfo normalizedCallableInfo(zend_str_tolower(callable));

    // Try to resolve the function we are trying to instrument.
    // We do this first so that if there is an interceptor set
    // on a zend_function due to a call to addInterceptorForOpArray,
    // we'll re-use existing interceptor set.
    zend_function* const target = m_execEnv->lookupCallable(normalizedCallableInfo);
    boost::intrusive_ptr<InterceptorSet> interceptors(findExistingInterceptorSetForFunction(target));

    auto const callableToInterceptorSetEnd = m_callableToInterceptorSet.end();
    auto callableToInterceptorSetIter = callableToInterceptorSetEnd;
    bool instrumentZendFunction = false;
    if (!interceptors) {
        // No interceptor set on the zend_function, so see if we already have
        // an existing interceptor set.
        callableToInterceptorSetIter = m_callableToInterceptorSet.find(normalizedCallableInfo);
        if (callableToInterceptorSetIter != m_callableToInterceptorSet.end()) {
            interceptors = callableToInterceptorSetIter->second;
            instrumentZendFunction = true;
        }
    }

    if (!interceptors) {
        interceptors = InterceptorSet::create(normalizedCallableInfo, resolveOnce);
        instrumentZendFunction = true;
    }

    if (instrumentZendFunction) {
        // If we could not find the zend_function to instrument, then
        // put the new interceptor set in the unresolved list.
        // If we did find the zend_function to instrument, then
        // instrument the function.
        if (!target)
            m_unresolvedInterceptors.insert(*interceptors);
        else
            instrumentZendFunctionIfPossible(callable, target, interceptors);
    }

    // Actually add the interceptor to the interceptor set.
    interceptors->insert(interceptor, resolveOnce);

    // If we did not find the interceptor set in the m_callableToInterceptorSet table,
    // then put it in the table.  This will be the case when we instrument a by name
    // for the first time.
    if (callableToInterceptorSetIter == callableToInterceptorSetEnd)
        m_callableToInterceptorSet[normalizedCallableInfo] = interceptors;
}

void InterceptionEngine::removeInterceptorForCallable(const CallableInfo& callable,
                                                      AMethodInterceptor* const interceptor)
{
    if (m_execEnv->getAgentGlobals().dit_flags.disableInterceptorInstallation)
        return;

    if (m_startFunctionInterceptorSet && (callable.getClass().empty()) && (callable.getFunc() == ROOT_SYMBOL)) {
        size_t eraseCount = m_startFunctionInterceptorSet->erase(interceptor);
        if (!eraseCount)
            LOG4CXX_DEBUG(m_logger, "interceptor " << interceptor->getClass() << " not registered with callable " << ROOT_SYMBOL);
        return;
    }

    CallableInfo normalizedCallableInfo(zend_str_tolower(callable));

    auto callableToInterceptorSetIter = m_callableToInterceptorSet.find(normalizedCallableInfo);
    if (callableToInterceptorSetIter == m_callableToInterceptorSet.end())
        return;
    size_t eraseCount = callableToInterceptorSetIter->second->erase(interceptor);
    if (!eraseCount)
        LOG4CXX_DEBUG(m_logger, "interceptor " << interceptor->getClass() << " not registered with callable " << callable);
}

bool InterceptionEngine::addInterceptorSetToOpArray(zend_op_array* opArray,
                                                    const boost::intrusive_ptr<InterceptorSet>& interceptorSet)
{
    if (m_execEnv->getAgentGlobals().dit_flags.disableInterceptorInstallation)
        return true;

    // One could argue this should be a BOOST_ASSERT, however
    // it seems worth while to be a little defensive in here.
    // Only the onFileCompiled method calls this method without
    // first checking if the zend_op_array is already instrumented.
    if (opArray->reserved[m_zendFunctionOpArrayReservedOffset])
    {
        LOG4CXX_ERROR(m_logger, "Unable to add interceptor to op_array: " << getNameForOpArray(opArray));
        return false;
    }
    opArray->reserved[m_zendFunctionOpArrayReservedOffset] = interceptorSet.get();
    m_anyOpArraysInstrumented = true;
    return true;
}

bool InterceptionEngine::addInterceptorForOpArray(zend_op_array* opArray,
                                                  AMethodInterceptor* const interceptor)
{
    if (m_execEnv->getAgentGlobals().dit_flags.disableInterceptorInstallation) {
        LOG4CXX_TRACE(m_logger, "instrumentation of user function " << getNameForOpArray(opArray) << " is disabled");
        return true;
    }

    // We need to keep null interceptors out of interceptor sets.
    BOOST_ASSERT(interceptor != NULL);
    LOG4CXX_TRACE(m_logger, "instrumenting user function: " << getNameForOpArray(opArray));

    bool didCreateSet = false;
    // See if the target zend_op_array is already instrumented
    boost::intrusive_ptr<InterceptorSet> interceptors(findExistingInterceptorSetForOpArray(opArray));
    if (!interceptors) {
        // zend_op_array was not yet instrumented, so
        // we need to create an interceptor set.
        interceptors = InterceptorSet::create();
        didCreateSet = true;
    }

    interceptors->insert(interceptor, true);

    // Add enough information to the m_requestInterceptorSets vector such
    // that we can remove this interceptor from the interceptor set
    // at the end of the php request.
    t_RequestUnRegisterInfo unregisterInfo(interceptor, interceptors);
    m_requestInterceptorSets.push_back(unregisterInfo);

    // If this method created the interceptor set, then we know
    // we need to instrument the zend_op_array.
    if (didCreateSet)
        return addInterceptorSetToOpArray(opArray, interceptors);
    return true;
}

void InterceptionEngine::removeAllInterceptorsFromOpArray(zend_op_array* opArray)
{
    if (m_execEnv->getAgentGlobals().dit_flags.disableInterceptorInstallation)
        return;
    LOG4CXX_TRACE(m_logger, "removing interceptors from: " << getNameForOpArray(opArray));
    opArray->reserved[m_zendFunctionOpArrayReservedOffset] = NULL;
}

void InterceptionEngine::onFileCompiled(zend_op_array* oparray)
{
    addInterceptorSetToOpArray(oparray, m_compiledScriptInterceptorSet);
}

/**
    Global jump table used in intercepting internal php functions.
    <p>
    TODO To support the thread safe version of PHP we'll need
    a lock to guard this jump table.
 */
static DynamicJmpTable::JmpTable<> internalFunctionsJmpTable;

void InterceptionEngine::updateZendFunctions()
{
    // The unresolved set is expected to be small or possibly empty. This is
    // why we iterate through it and do a lookup in the class/function
    // tables, rather than the other way around.
    auto unresolvedEnd = m_unresolvedInterceptors.end();

    for (auto i = m_unresolvedInterceptors.begin(); i != unresolvedEnd;) {

        // Stuff the current interceptor set into a smart pointer
        // so we know the interceptor set will not be destroyed until
        // we get to the bottom of the loop.
        boost::intrusive_ptr<InterceptorSet> interceptorSet(&(*i), true);

        // No need to instrument if the set has no interceptors
        if (interceptorSet->size() == 0) {
            ++i;
            continue;
        }

        const CallableInfo* callableInfo = interceptorSet->getCallableInfo();
        BOOST_ASSERT(callableInfo != NULL);
        zend_function* const target = m_execEnv->lookupCallable(*callableInfo);
        if (!target) {
            // If we are unable to find the zend_function to instrument,
            // and all interceptors in the interceptor set only want to try
            // to resolve once, then remove the interceptor set from the
            // unresolved interceptors list.  We'll never attempt to resolve
            // the interceptor set again for the lifetime of the process.
            if (interceptorSet->resolveOnce())
                i = m_unresolvedInterceptors.erase(i);
            else
                ++i;
            continue;
        }

        // We found the zend_function to instrument.  It might already be instrumented
        // due to a call to addInterceptorSetToOpArray.  In that case we need to
        // add all the interceptors from the unresolved interceptor set to the interceptor
        // set already attached to the zend_function.
        boost::intrusive_ptr<InterceptorSet> const existingInterceptorSet(findExistingInterceptorSetForFunction(target));
        if (existingInterceptorSet) {
            existingInterceptorSet->insert(interceptorSet->begin(), interceptorSet->end(), interceptorSet->resolveOnce());
            existingInterceptorSet->setCallableInfo(*callableInfo);
            // Even though we copy all the interceptors in the interceptor set
            // that was already attached the zend_function, we still
            // put the current interceptor set in the m_resolvedToOpArrayInterceptors
            // list so that it will get put back into the m_unresolvedInterceptors list
            // at the end of the php request.
            m_resolvedToOpArrayInterceptors.insert(*interceptorSet);
            i = m_unresolvedInterceptors.erase(i);
            continue;
        }

        // If we get here nothing fancy happened, we just resolved an unresolved interceptor
        // set and now need to instrument the zend_function.
        instrumentZendFunctionIfPossible(*callableInfo, target, boost::intrusive_ptr<InterceptorSet>(&(*i), true));
        i = m_unresolvedInterceptors.erase(i);
    }
}

void InterceptionEngine::resetRequestInterceptorSets()
{
    auto const end = m_requestInterceptorSets.end();
    for (auto i = m_requestInterceptorSets.begin(); i != end; ++i)
        i->second->erase(i->first);
    m_requestInterceptorSets.clear();
}

void InterceptionEngine::resetResolvedInterceptorSets()
{
    // Any interceptor sets that resolved to zend_op_array's during the last php request
    // need to be put back into the m_resolvedToOpArrayInterceptors list.
    auto const end = m_resolvedToOpArrayInterceptors.end();
    for (auto i = m_resolvedToOpArrayInterceptors.begin(); i != end;) {
        m_unresolvedInterceptors.insert(*i);
        i = m_resolvedToOpArrayInterceptors.erase(i);
    }
}

void InterceptionEngine::onRequestBegin(boost::shared_ptr<PHPExecEnvironment> env)
{
    // Due to bugs in the php engine, there are cases where
    // onRequestEnd will not be called.  Thus we reset the interception engine
    // state in onRequestBegin and in onRequestEnd.
    resetRequestInterceptorSets();
    resetResolvedInterceptorSets();
    m_anyOpArraysInstrumented = false;
    m_execEnv = env;
    updateZendFunctions();
}

void InterceptionEngine::onRequestEnd()
{
    m_execEnv.reset();
    resetRequestInterceptorSets();
    resetResolvedInterceptorSets();
    m_anyOpArraysInstrumented = false;
}

void InterceptionEngine::onExecuteBegin(const InterceptorSet** interceptorSet,
                                        zend_execute_data* edata,
                                        zend_op_array* op,
                                        bool internal)
{
    *interceptorSet = NULL;

    //  TODO: Revisit this when we refactor agent.cpp, an object holding the state
    //  of the interception could just have a flag for whether we've applied
    //  the root interceptor set
    //  FIXME: This test may have a side case when destructors and shutdown hooks
    //  run

    if (!edata)
    {
        *interceptorSet = m_startFunctionInterceptorSet.get();
    }
    else if (!internal)
    {
        const InterceptorSet* interceptors = reinterpret_cast<InterceptorSet*>(op->reserved[m_zendFunctionOpArrayReservedOffset]);
        if (interceptors) {
            // Defend against memory smashes, other extensions stealing our
            // reserved offset, and bad opcode caches.
            // If we have not instrumented any oparrays yet, then
            // there can not possibly be an interceptor set on this oparray.
            if (m_anyOpArraysInstrumented) {
                *interceptorSet = interceptors;
            }
            else {
                uintptr_t interceptorsPtr = reinterpret_cast<uintptr_t>(interceptors);
                LOG4CXX_ERROR(m_logger, "Interceptors found on op_array when no interceptors have been registered!");
                LOG4CXX_ERROR(m_logger, "interceptors: " << std::hex << interceptorsPtr);
                LOG4CXX_ERROR(m_logger, "m_zendFunctionOpArrayReservedOffset: " << m_zendFunctionOpArrayReservedOffset);
            }
        }
//#if PHP_VERSION_ID >= 70000
#if 0
        else if(AG(G) && AG(G)->exec_env && AG(G)->exec_env->isSoapServerHandleFuncInExecution()){
		      const boost::intrusive_ptr<InterceptorSet> interceptors=InterceptorSet::create();
		      interceptors->insert(&nativeSOAPCallUserFuncInterceptor, true);
		      *interceptorSet= interceptors.get();
        }
#endif
    }
    else
    {
        auto handler = APPD_EXECDATA_ZEND_FUNC(edata)->internal_function.handler;
        unsigned jmpTableIndex;
        if (internalFunctionsJmpTable.findEntry(handler, &jmpTableIndex))
        {
            if (jmpTableIndex < m_internalFunctionsInterceptorSets.size())
                *interceptorSet = m_internalFunctionsInterceptorSets[jmpTableIndex].get();
        }
    }
}

inline boost::intrusive_ptr<InterceptorSet> InterceptionEngine::findExistingInterceptorSetForOpArray(const zend_op_array* opArray)
{
    void* existingInterceptorSetOpaque = opArray->reserved[m_zendFunctionOpArrayReservedOffset];
    if (!existingInterceptorSetOpaque)
        return boost::intrusive_ptr<InterceptorSet>();
    return boost::intrusive_ptr<InterceptorSet>(reinterpret_cast<InterceptorSet*>(existingInterceptorSetOpaque), true);
}

inline boost::intrusive_ptr<InterceptorSet> InterceptionEngine::findExistingInterceptorSetForInternalFunction(const zend_internal_function* internalFunction)
{
    auto handler = internalFunction->handler;
    unsigned jmpTableIndex = 0;
    if (!internalFunctionsJmpTable.findEntry(handler, &jmpTableIndex))
        return boost::intrusive_ptr<InterceptorSet>();
    return m_internalFunctionsInterceptorSets[jmpTableIndex];
}

boost::intrusive_ptr<InterceptorSet> InterceptionEngine::findExistingInterceptorSetForFunction(const zend_function* f)
{
    if (!f)
        return boost::intrusive_ptr<InterceptorSet>();
    if (f->type == ZEND_USER_FUNCTION)
        return findExistingInterceptorSetForOpArray(&(f->op_array));
    if (f->type == ZEND_INTERNAL_FUNCTION)
        return findExistingInterceptorSetForInternalFunction(&(f->internal_function));

    return boost::intrusive_ptr<InterceptorSet>();
}

bool InterceptionEngine::addInterceptorSetToFunction(zend_function* f,
                                                     const boost::intrusive_ptr<InterceptorSet>& interceptors)
{
    BOOST_ASSERT(f != NULL);
    BOOST_ASSERT(interceptors);
    // We should not get in here if the zend_function already has
    // an interceptor set bound to it.
    BOOST_ASSERT(!(findExistingInterceptorSetForFunction(f)));
    if (f->type == ZEND_USER_FUNCTION) {
        f->op_array.reserved[m_zendFunctionOpArrayReservedOffset] = interceptors.get();
        m_anyOpArraysInstrumented = true;
        m_resolvedToOpArrayInterceptors.insert(*interceptors);
        return true;
    }
    else if (f->type == ZEND_INTERNAL_FUNCTION) {
        auto handler = f->internal_function.handler;
        unsigned jmpTableIndex = 0;
        auto ptrToHandlerField = &(f->internal_function.handler);
        if (internalFunctionsJmpTable.addEntry(handler, ptrToHandlerField, &jmpTableIndex)) {
            if (m_internalFunctionsInterceptorSets.size() <= jmpTableIndex)
                m_internalFunctionsInterceptorSets.resize(jmpTableIndex + 1);
            m_internalFunctionsInterceptorSets[jmpTableIndex] = interceptors;
            return true;
        }
    }
    return false;
}

void InterceptionEngine::instrumentZendFunctionIfPossible(const CallableInfo& callable,
                                                          zend_function* const target,
                                                          const boost::intrusive_ptr<InterceptorSet>& interceptors)
{
    BOOST_ASSERT(target != NULL);

    if (m_logger->isTraceEnabled()) {
        if (target->type == ZEND_USER_FUNCTION) {
            LOG4CXX_TRACE(m_logger, "instrumenting user function: " << callable);
        }
        else if (target->type == ZEND_INTERNAL_FUNCTION) {
            LOG4CXX_TRACE(m_logger, "instrumenting internal function: " << callable);
        }
    }

    bool const didInstrument = addInterceptorSetToFunction(target, interceptors);
    if (!didInstrument)
        LOG4CXX_ERROR(m_logger, "failed to instrument: " << callable);
}

/* }}} */

/* {{{ MethodInterceptorDelegate methods */

void* MethodInterceptorDelegate::safeOnCallableBegin(AMethodInterceptor* interceptor,
                                                     const PHPExecEnvironment* phpExecEnv)
{
    if (s_disabled)
        return const_cast<uint32_t*>(&DISABLED_INTERCEPTOR_STATE);

    // TODO check for re-entrancy

    BOOST_ASSERT(interceptor != NULL);

    try {
        return interceptor->onCallableBegin(phpExecEnv);
    }
    catch (...) {
        const intptr_t* const interceptorVTable =
            reinterpret_cast<const intptr_t*>(interceptor);

        LOG4CXX_ERROR(AG(G)->log_agent,
                      "onCallableBegin had uncaught exception.");
        LOG4CXX_ERROR(AG(G)->log_agent,
                      "vtable: " << std::hex << interceptorVTable);
        LOG4CXX_ERROR(AG(G)->log_agent,
                      "class: " << interceptor->getClass());
        return const_cast<uint32_t*>(&DISABLED_INTERCEPTOR_STATE);
    }
}

void MethodInterceptorDelegate::safeOnCallableEnd(AMethodInterceptor* interceptor,
                                                  const PHPExecEnvironment* phpExecEnv,
                                                  void *state)
{
    if (state == &DISABLED_INTERCEPTOR_STATE)
        return;

    // TODO check for re-entrancy

    if (interceptor == NULL) {
        std::string const callableName(phpExecEnv->getCallableName());
        LOG4CXX_WARN(AG(G)->log_agent, "NULL interceptor, on callable end for " << callableName);
        return;
    }
    try {
        interceptor->onCallableEnd(phpExecEnv, state);
    }
    catch (...) {
        const intptr_t* const interceptorVTable =
            reinterpret_cast<const intptr_t*>(interceptor);

        LOG4CXX_ERROR(AG(G)->log_agent,
                      "onCallableEnd had uncaught exception.");
        LOG4CXX_ERROR(AG(G)->log_agent,
                      "vtable: " << std::hex << interceptorVTable);
        LOG4CXX_ERROR(AG(G)->log_agent,
                      "class: " << interceptor->getClass());
    }
}

/* }}} */

/* {{{ PHPExecEnvironment methods */


PHPExecEnvironment::ExceptionState::ExceptionState(const void* threadSafetyContext)
    : m_threadSafetyContext(threadSafetyContext)
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    m_exception = EG(exception);
    EG(exception) = NULL;
#if PHP_VERSION_ID >= 50300
    m_prevException = EG(prev_exception);
    EG(prev_exception) = NULL;
#endif
    if (EG(current_execute_data))
        m_savedOpline = const_cast<zend_op*>(EG(current_execute_data)->opline);
    m_savedBeforeExceptionOpLine = const_cast<zend_op*>(EG(opline_before_exception));
    EG(opline_before_exception) = NULL;
}

PHPExecEnvironment::ExceptionState::~ExceptionState()
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext); (void)m_threadSafetyContext;
    // Stuff the exception globals into local variables
    // so we can clear the globals before invoking any
    // destruction logic.

    t_ExceptionType exception = EG(exception);
    EG(exception) = NULL;

    // Destroy any exceptions that
    // were accumulated since our constructor
    // was invoked.

#if PHP_VERSION_ID >= 50300
    t_ExceptionType prevException = EG(prev_exception);
    EG(prev_exception) = NULL;

    if (prevException)
# if PHP_VERSION_ID >= 70000
        OBJ_RELEASE(prevException);
# else
        zval_ptr_dtor(&prevException);
# endif
#endif

    if (exception)
#if PHP_VERSION_ID >= 70000
        OBJ_RELEASE(exception);
#else
        zval_ptr_dtor(&exception);
#endif

    // Restore the previous exception values.
#if PHP_VERSION_ID >= 50300
    EG(prev_exception) = m_prevException;
#endif
    EG(exception) = m_exception;

    if (EG(current_execute_data))
        EG(current_execute_data)->opline = m_savedOpline;
    EG(opline_before_exception) = m_savedBeforeExceptionOpLine;

    m_exception = NULL;
    m_prevException = NULL;
    m_savedOpline = NULL;
    m_savedBeforeExceptionOpLine = NULL;
}

#if PHP_VERSION_ID >= 50300
PHPExecEnvironment::ArgStackState::ArgStackState(
        const std::vector<ZValPointerAny>& args,
        zend_execute_data* executeData)
    : m_args(args)
    , m_executeData(executeData)
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);

# if PHP_VERSION_ID >= 70000
    size_t mArgsNumElts = m_args.size();

    unsigned argCount = ZEND_CALL_NUM_ARGS(m_executeData);
    BOOST_ASSERT(argCount > 0); // If this fails, make sure method
                                // works when adding first argument.
    BOOST_ASSERT(mArgsNumElts > 0);

    // extends the stack if needed. Increases EG(vm_stack_top) by mArgsNumElts
    // if there is enough space in the current call frame. If not, it allocates a new
    // call frame, copies the contents of m_executeData over to the new call
    // frame, and points m_executeData to the new call frame.

    // TODO: make sure this function is usable!
    zend_vm_stack_extend_call_frame(&m_executeData, argCount, mArgsNumElts);

    // newArg is the next empty argument slot (= argCount + 1)
    zval* newArg = ZEND_CALL_ARG(m_executeData, argCount + 1);
    for (unsigned i = 0; i < mArgsNumElts; i++) {
        ZVAL_COPY_VALUE(newArg, m_args[i].get());
        newArg++;
    }

    ZEND_CALL_NUM_ARGS(m_executeData) += mArgsNumElts;
    const_cast<zend_op*>(m_executeData->opline)->extended_value += mArgsNumElts;

# else

    size_t mArgsNumElts = m_args.size();

    unsigned argCount =
        reinterpret_cast<zend_uintptr_t>(
                *m_executeData->function_state.arguments
                );
    BOOST_ASSERT(argCount > 0); // If this fails, make sure method
                                // works when adding first argument.
    BOOST_ASSERT(mArgsNumElts > 0);

    ZEND_VM_STACK_GROW_IF_NEEDED(mArgsNumElts);

    EG(argument_stack)->top += mArgsNumElts;
    for (unsigned i = 0; i < mArgsNumElts; i++) {
        *(m_executeData->function_state.arguments) = m_args[i].get();
        m_executeData->function_state.arguments += 1;
    }
    *(m_executeData->function_state.arguments) =
        reinterpret_cast<void*>(argCount + mArgsNumElts);
    m_executeData->opline->extended_value += mArgsNumElts;
# endif
}

PHPExecEnvironment::ArgStackState::~ArgStackState()
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    // Until tested, do not use on PHP methods. Use only on internals.

# if PHP_VERSION_ID >= 70000
    int currentStackArgCount = ZEND_CALL_NUM_ARGS(m_executeData);
    size_t nPopArgs = m_args.size();
# else
    int currentStackArgCount =
        reinterpret_cast<zend_uintptr_t>(
                *m_executeData->function_state.arguments
                );
    size_t nPopArgs = m_args.size();
# endif

    BOOST_ASSERT(currentStackArgCount > 0);
    BOOST_ASSERT(m_args.size() > 0);

    if (currentStackArgCount == 0
            || currentStackArgCount < m_args.size())
        return;

# if PHP_VERSION_ID >= 70000
    EG(vm_stack_top) -= nPopArgs;
    ZEND_CALL_NUM_ARGS(m_executeData) -= nPopArgs;
    const_cast<zend_op*>(m_executeData->opline)->extended_value -= nPopArgs;
# else
    EG(argument_stack)->top -= nPopArgs;
    m_executeData->function_state.arguments -= nPopArgs;
    *(m_executeData->function_state.arguments) =
        reinterpret_cast<void*>(currentStackArgCount - nPopArgs);
    m_executeData->opline->extended_value -= nPopArgs;
# endif
}

#endif

static inline unsigned long getCallTypeFromZendOP(const zend_op* op)
{
#if PHP_VERSION_ID >= 50400
    return op->extended_value;
#else
    return static_cast<unsigned long>(op->op2.u.constant.value.lval);
#endif
}

zval* PHPExecEnvironment::getInvokedObject(unsigned numCallFramesUp) const
{
    unsigned idx = 0;
    const zend_execute_data* currentExecuteData = m_executeData;
    while (currentExecuteData != NULL && idx++ < numCallFramesUp) {
        currentExecuteData = currentExecuteData->prev_execute_data;
    }
    if (currentExecuteData) {
#if PHP_VERSION_ID >= 70000
        return const_cast<zval*>(&currentExecuteData->This);
#else
        return currentExecuteData->object;
#endif
    }

    return NULL;
}

zval* PHPExecEnvironment::getInvokedObject() const
{
    return getInvokedObject(0);
}

#if PHP_VERSION_ID < 70000
static inline temp_variable* getReturnValueTempVariableFromExecData(const zend_execute_data* execData)
{
#if PHP_VERSION_ID >= 50500
    temp_variable* const result =
        EX_TMP_VAR(execData, execData->opline->result.var);
#elif PHP_VERSION_ID >= 50400
    uint8_t* const tempVarAddr =
        reinterpret_cast<uint8_t*>(execData->Ts) + execData->opline->result.var;
    temp_variable* const result =
        reinterpret_cast<temp_variable*>(tempVarAddr);
#else
    uint8_t* const tempVarAddr =
        reinterpret_cast<uint8_t*>(execData->Ts) + execData->opline->result.u.var;
    temp_variable* const result =
        reinterpret_cast<temp_variable*>(tempVarAddr);
#endif
    return result;
}
#endif

static inline zval* getReturnValueFromExecData(const zend_execute_data* execData, zend_fcall_info* fcallInfo)
{
#if PHP_VERSION_ID >= 70000
    if (fcallInfo)
        return fcallInfo->retval;
    return execData->return_value;
#elif PHP_VERSION_ID >= 50500
    if (fcallInfo)
        return *fcallInfo->retval_ptr_ptr;
#else
    (void)fcallInfo;
#endif
#if PHP_VERSION_ID < 70000
    temp_variable* returnValueTempVariable =
        getReturnValueTempVariableFromExecData(execData);
    return returnValueTempVariable->var.ptr;
#endif
}

static inline zval* computeReturnValue(bool isInternal, zend_execute_data* executeData, zend_fcall_info* fcallInfo TSRMLS_DC)
{
    zval *return_value = &EG(uninitialized_zval);
    if (EG(exception))
        return &EG(uninitialized_zval);

    if (isInternal)
    {
        if (executeData)
        {
            zval* ed_retval = getReturnValueFromExecData(executeData, fcallInfo);
            return ed_retval ? ed_retval : &EG(uninitialized_zval);
        }
    }
#if PHP_VERSION_ID >= 70000
    else if (executeData->return_value)
    {
      zval* retval;
      APPD_MAKE_STD_ZVAL(retval);
      ZVAL_COPY_VALUE(retval, executeData->return_value);
      zval_copy_ctor(retval);
      return retval;  
    }
#else
    else if (EG_RET_VAL_PTR_PTR)
    {
        return *(EG_RET_VAL_PTR_PTR);
    }
#endif
    return &EG(uninitialized_zval);
}

zval* PHPExecEnvironment::getReturnValue(bool isDataCollectorConfigured) const
{
    // Don't call getReturnValue from AMethodInterceptor::onCallableBegin!
    BOOST_ASSERT(m_isCallableEnd);
    if (!m_isCallableEnd)
        return NULL;
    if (m_returnValueComputed)
#if PHP_VERSION_ID < 70000
        return m_cachedReturnValue;
#else
    if(!isDataCollectorConfigured)
       return m_cachedReturnValue;
#endif
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    m_cachedReturnValue = computeReturnValue(m_isInternal, m_executeData, m_fcallInfo TSRMLS_CC);
    m_returnValueComputed = true;
    return m_cachedReturnValue;
}

zval* PHPExecEnvironment::getExceptionObject() const
{
    // Don't call getExceptionObject from AMethodInterceptor::onCallableBegin!
    BOOST_ASSERT(m_isCallableEnd);
    if (!m_isCallableEnd)
        return NULL;

    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
#if PHP_VERSION_ID >= 70000
    if (EG(exception) == NULL)
        return NULL;
    /* EG(exception) is of type zend_object*.
     * Since this method returns a zval*, we need
     * to create a zval and associate it with the
     * zend_object* stored in EG(exception).
     */

    m_exceptionZval.reset(new zval);
    zval *retval = m_exceptionZval.get();
    *retval = EG(uninitialized_zval);
    ZVAL_OBJ(retval, EG(exception));
    return retval;
#else
    return EG(exception);
#endif
}

bool PHPExecEnvironment::doesExceptionObjectExist() const
{
    BOOST_ASSERT(m_isCallableEnd);
    if (!m_isCallableEnd)
        return false;

    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);

    return EG(exception) != NULL;
}

const char* PHPExecEnvironment::getClassName() const
{
    BOOST_ASSERT(m_execStackFrame != NULL);
    return m_execStackFrame->getNodeName().getClassName();
}

const char* PHPExecEnvironment::getFunctionName() const
{
    BOOST_ASSERT(m_execStackFrame != NULL);
    return m_execStackFrame->getNodeName().getMethodName();
}

std::string PHPExecEnvironment::getCallableName() const
{
    const char* const className = getClassName();
    const char* const functionName = getFunctionName();
    size_t const functionNameLen = strlen(functionName);
    std::string result;
    if (className) {
        size_t const classNameLen = strlen(className);
        result.reserve(classNameLen + 2 + functionNameLen);
        result.append(className, classNameLen);
        result.append("::", 2);
    }
    else {
        result.reserve(functionNameLen);
    }
    result.append(functionName);
    return result;
}

#if PHP_VERSION_ID >= 70000
zend_uintptr_t PHPExecEnvironment::getParamCount() const
{
    if (!m_executeData)
        return 0;
    return ZEND_CALL_NUM_ARGS(m_executeData);
}
#else
zend_uintptr_t PHPExecEnvironment::getParamCount() const
{
# if PHP_VERSION_ID >= 50300
    if (!m_executeData)
        return 0;
    void** argumentsInfo = m_executeData->function_state.arguments;
# else
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (EG(argument_stack).top < 2)
        return 0;
    void **argumentsInfo = EG(argument_stack).top_element - 2;
# endif
    zend_uintptr_t paramsCount = *(reinterpret_cast<zend_uintptr_t*>(argumentsInfo));
    return paramsCount;
}
#endif


#if PHP_VERSION_ID >= 70000
zval* PHPExecEnvironment::getParamImpl(unsigned paramIndex) const
{
    BOOST_ASSERT(getParamCount() > paramIndex);

    // almost identical to zend_builtin_functions.c:func_get_arg
    uint32_t first_extra_arg;
    zval *arg, *retval;

    first_extra_arg = m_executeData->func->op_array.num_args;

    if (paramIndex >= first_extra_arg && (ZEND_CALL_NUM_ARGS(m_executeData) > first_extra_arg)) {
        arg = ZEND_CALL_VAR_NUM(m_executeData, m_executeData->func->op_array.last_var +
                                m_executeData->func->op_array.T) +
              (paramIndex - first_extra_arg);
    } else {
        arg = ZEND_CALL_ARG(m_executeData, paramIndex + 1);
    }

    if (EXPECTED(!Z_ISUNDEF_P(arg))) {
        ZVAL_DEREF(arg);
    }

    // DLNATIVE-1501, DLNATIVE-1579, DLNATIVE-1521 - we need to copy the zval "arg" containing the arguments passed to the internal PHP functions 
    // which are retrieved using ZEND_CALL_ARG and ZEND_CALL_VAR_NUM
    // This is because this zval is stored in our agent in many places
    // When it is destroyed from our agent and later accessed from PHP it leads to memory corruption, ergo we need to soft copy it, increasing its ref count before utilizing it.
    // Though copying the zval may lead to a "pseudo" memory leak, nevertheless PHP cleans up such unfreed memory at the close of each request
    // Altogether, copying this zval fixes all such issues, barring few harmless warnings in the agent logs of debug build of our agent.

    APPD_MAKE_STD_ZVAL(retval);
    ZVAL_COPY(retval, arg);
    return retval;	
}
#else
zval* PHPExecEnvironment::getParamImpl(unsigned paramIndex) const
{
# if PHP_VERSION_ID >= 50300
    if (!m_executeData)
        return NULL;
    void** argumentsInfo = m_executeData->function_state.arguments;
# else
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (EG(argument_stack).top < 2)
        return NULL;
    void** argumentsInfo = EG(argument_stack).top_element - 2;
# endif
    zend_uintptr_t paramsCount = *(reinterpret_cast<zend_uintptr_t*>(argumentsInfo));
    BOOST_ASSERT(paramsCount > paramIndex);
    zval* const* arguments =
        reinterpret_cast<zval* const*>(argumentsInfo) - paramsCount;
    return arguments[paramIndex];
}
#endif

/**
    A class that is used to adapt the values
    in php engine HashTable's to zend_function*'s for
    the hash tables that store functions.
 */
class FunctionHashTableValueAdapter
{
public:
    typedef zend_function* t_ValueType;
#if PHP_VERSION_ID >= 70000
    static zend_function* toValue(zval* foundValue);
#else
    static zend_function* toValue(void* rawValue);
#endif

private:
    FunctionHashTableValueAdapter();
    FunctionHashTableValueAdapter(const FunctionHashTableValueAdapter&);
    FunctionHashTableValueAdapter& operator=(const FunctionHashTableValueAdapter&);
};

#if PHP_VERSION_ID >= 70000
inline zend_function* FunctionHashTableValueAdapter::toValue(zval* foundValue)
{
    // zvals that store zend_function*s are of type IS_PTR
    BOOST_ASSERT(Z_TYPE_P(foundValue) == IS_PTR);
    return Z_FUNC_P(foundValue);
}
#else
inline zend_function* FunctionHashTableValueAdapter::toValue(void* rawValue)
{
    return reinterpret_cast<zend_function*>(rawValue);
}
#endif

zend_function* PHPExecEnvironment::lookupCallable(const CallableInfo& callable) const
{
    ZendHashTable<FunctionHashTableValueAdapter> functionTable(ZendHashTable<FunctionHashTableValueAdapter>::share(CG(function_table)));
    const std::string& className = callable.getClass();
    if (!className.empty())
    {
        zend_class_entry* classEntry = lookupClass(className.c_str(), className.length());
        if (!classEntry)
            return NULL;
        functionTable = ZendHashTable<FunctionHashTableValueAdapter>::share(&(classEntry->function_table));
    }
    zend_function* result = NULL;
    if (!functionTable.find(callable.getFunc(), &result))
        return NULL;
    return result;
}

zend_function* PHPExecEnvironment::lookupCallable(const ZValPointerAny& callableValue) const
{
    zend_fcall_info_cache callInfo;

    bool isCallable = populateFunctionCallInfoCache(&callInfo, callableValue);

    if (!isCallable)
        return NULL;

    return callInfo.function_handler;
}

zend_class_entry* PHPExecEnvironment::lookupClass(const char* className,
                                                  size_t classNameLen) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);

#if PHP_VERSION_ID >= 70000
    zend_string* classNameZStr = zend_string_init(className, classNameLen, false);
    zend_class_entry* result =  zend_lookup_class_ex(classNameZStr, NULL, 0);
    zend_string_release(classNameZStr);
    return result;
#else
    zend_class_entry** classEntry;
    if (zend_lookup_class_ex(const_cast<char*>(className),
                             classNameLen,
    #ifdef ZEND_ENGINE_2_4
                             NULL,
    #endif
                             0,
                             &classEntry TSRMLS_CC) == FAILURE)
            return NULL;
    return *classEntry;
#endif
}

zend_function* PHPExecEnvironment::lookupMethod(const ZValPointerObject& object,
                                                const char* methodName,
                                                size_t methodNameLen) const
{
  ZValPointerArray callable(ZValPointerArray::create());
  callable.pushEntry(object);
  callable.pushEntry(ZValPointerString::copy(methodName));
  return lookupCallable(callable);
}

zend_function* PHPExecEnvironment::lookupFunction(const char* funcName,
                                                  size_t funcNameLen) const
{
    zend_function* result = NULL;
    ZendHashTable<FunctionHashTableValueAdapter> functionTable(ZendHashTable<FunctionHashTableValueAdapter>::share(CG(function_table)));
    if (!functionTable.find(funcName, funcNameLen + 1, &result))
        return NULL;
    return result;
}

bool PHPExecEnvironment::getAutoGlobalValue(const char* autoGlobalName,
                                            size_t autoGlobalNameLen,
                                            ZValPointerAny* value) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    // See if the specified auto global exists, which
    // cause it to exist if it is created lazily.
    if (!zend_is_auto_global_str(const_cast<char*>(autoGlobalName), (autoGlobalNameLen) TSRMLS_CC))
        return false;

    // Look up the global in the regular symbol table to get its value.
    ZendHashTable<> symbolTable(ZendHashTable<>::share(&(EG(symbol_table))));

    BOOST_ASSERT(autoGlobalName[autoGlobalNameLen] == '\0');

    return symbolTable.find(autoGlobalName, autoGlobalNameLen + 1, value);
}

bool PHPExecEnvironment::getConstantAsLong(const char* constantName,
                                           size_t constantNameLen,
                                           long* value) const
{
    if (constantNameLen == 0)
        return false;

    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    BOOST_ASSERT(constantName != NULL);
    BOOST_ASSERT(constantName[constantNameLen] == '\0');

#if PHP_VERSION_ID >= 70000
    zval* result = zend_get_constant_str(constantName, constantNameLen);
    if (result == NULL || Z_TYPE_P(result) != IS_LONG)
        return false;
    // no dtor call needed, ctor on result was not called in zend_get_constant_str().
    *value = Z_LVAL_P(result);
#else
    zval zvalForConst;
    bool didGetConstant = zend_get_constant(const_cast<char*>(constantName), constantNameLen, &zvalForConst TSRMLS_CC);
    if (!didGetConstant || Z_TYPE(zvalForConst) != IS_LONG)
        return false;
    *value = Z_LVAL(zvalForConst);
    zval_dtor(&zvalForConst);
#endif
    return true;
}

bool PHPExecEnvironment::getConstant(const char* constantName,
                                     size_t constantNameLen,
                                     zval* value) const
{
    if (constantNameLen == 0)
        return false;

    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    BOOST_ASSERT(constantName != NULL);
    BOOST_ASSERT(constantName[constantNameLen] == '\0');

#if PHP_VERSION_ID >= 70000
    zval* result = zend_get_constant_str(constantName, constantNameLen);
    // we need to do the copying ourselves (PHP 5's version of zend_get_constant did it for us)
    if (result != NULL) {
        ZVAL_DUP(value, result);
        return true;
    }
    return false;
#else
    bool didGetConstant = zend_get_constant(const_cast<char*>(constantName),
                                            constantNameLen,
                                            value TSRMLS_CC);
    return didGetConstant;
#endif
}

bool PHPExecEnvironment::getCallableName(const ZValPointerAny& callableValue, std::string* name) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    char* callableName_cstr = NULL;
    int callableNameLen = 0;

    // zend_is_callable_ex is evil in that
    // it sometimes allocates a new zend_function
    // and a string for the function's name.  We
    // need to free those before leaving this method.
#if PHP_VERSION_ID >= 70000
    zend_string* callableNameZStr = NULL;
    zend_bool isCallable =
        zend_is_callable_ex(callableValue.get(),
            NULL,
            IS_CALLABLE_CHECK_SILENT,
            &callableNameZStr,
            NULL,
            NULL);
#elif PHP_VERSION_ID >= 50300
    zend_bool isCallable =
        zend_is_callable_ex(callableValue.get(),
            NULL,
            IS_CALLABLE_CHECK_SILENT,
            &callableName_cstr,
            &callableNameLen,
            NULL,
            NULL TSRMLS_CC);
#else
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    ErrorMonitor::SuppressionScope errorSuppression(errorMonitor, true);
    zend_bool isCallable =
        zend_is_callable_ex(callableValue.get(),
            0,
            &callableName_cstr,
            &callableNameLen,
            NULL,
            NULL,
            NULL TSRMLS_CC);
#endif

    if (!isCallable)
    {
        // Grrr... Reading the code in the php engine
        // it looks like callableName can reference
        // an allocated buffer even if zend_is_callable_ex
        // returns false!
#if PHP_VERSION_ID >= 70000
        if (callableNameZStr)
            zend_string_release(callableNameZStr);
#else
        if (callableName_cstr)
            efree(callableName_cstr);
#endif
        return false;
    }

#if PHP_VERSION_ID >= 70000
    if (callableNameZStr)
        name->assign(ZSTR_VAL(callableNameZStr), ZSTR_LEN(callableNameZStr));
    else
        name->clear();
    zend_string_release(callableNameZStr);
#else
    name->assign(callableName_cstr, callableNameLen);
    efree(callableName_cstr);
#endif
    return true;
}

CXXGlobals& PHPExecEnvironment::getAgentGlobals() const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return CXXGlobals::get(TSRMLS_C);
}

boost::shared_ptr<IConfigChannel>& PHPExecEnvironment::getConfigChannel() const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return AG(G)->configChannel;
}

const boost::shared_ptr<HTTPPayload>& PHPExecEnvironment::getHTTPPayload() const
{
    if (!m_httpPayload) {
        const PHPExecEnvironment* execEnv = this;
        m_httpPayload = boost::make_shared<HTTPPayload>(execEnv, m_logger);
    }

    return m_httpPayload;
}

zend_class_entry* PHPExecEnvironment::getDefaultExceptionClassEntry() const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return zend_exception_get_default(TSRMLS_C);
}

zend_class_entry* PHPExecEnvironment::getObjectValueClassEntry(zval* object) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (Z_TYPE_P(object) != IS_OBJECT)
        return NULL;
    return APPD_GET_CE(object);
}

bool PHPExecEnvironment::isAncestorClass(zend_class_entry* ancestorClass,
                                         zend_class_entry* derivedClass) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return instanceof_function(derivedClass, ancestorClass TSRMLS_CC);
}

namespace
{
    class RawZValHashTableValueAdapter : boost::noncopyable
    {
    public:
        typedef zval* t_ValueType;
#if PHP_VERSION_ID >= 70000
        static zval* toValue(zval* foundValue) { return foundValue; }
#else
        static zval* toValue(void* rawValue)
        {
            zval** zVal = reinterpret_cast<zval**>(rawValue);
            return *zVal;
        }
#endif
    private:
        RawZValHashTableValueAdapter();
    };
}
/**
Mangles the property name and stores it in the out-parameters.
    @param accessLevel Access level of the property to lookup.
    @param name Name of the property to lookup.
    @param nameLenInBytes Length of the property name not including the terminating null.
    @param scope Scope for the property look-up (only makes sense for private properties)
    @param mangledName outparameter
    @param mangledNameLen outparameter
    @param mangledNameZStr outparameter (only in PHP7)
    @param freeMangledName outparameter
*/
void manglePropertyName(PHPExecEnvironment::AccessLevel accessLevel,
                        const char* name,
                        size_t nameLenInBytes,
                        const zend_class_entry* scope,
                        char** mangledName,
                        int* mangledNameLen,
#if PHP_VERSION_ID >= 70000
                        zend_string** mangledNameZStr,
#endif
                        bool* freeMangledName)
{
    switch (accessLevel)
    {
    case PHPExecEnvironment::AccessLevel::PUBLIC:
        *mangledName = const_cast<char*>(name);
        *mangledNameLen = nameLenInBytes;
        *freeMangledName = false;
        break;
    case PHPExecEnvironment::AccessLevel::PROTECTED:
#if PHP_VERSION_ID >= 70000
        *mangledNameZStr =
            zend_mangle_property_name(
                                  "*",
                                  1,
                                  const_cast<char*>(name),
                                  nameLenInBytes,
                                  false);
        *mangledName = ZSTR_VAL(*mangledNameZStr);
        *mangledNameLen = ZSTR_LEN(*mangledNameZStr);
#else
        zend_mangle_property_name(mangledName,
                                  mangledNameLen,
                                  "*",
                                  1,
                                  const_cast<char*>(name),
                                  nameLenInBytes,
                                  false);
#endif
        break;
    case PHPExecEnvironment::AccessLevel::PRIVATE:
#if PHP_VERSION_ID >= 70000
        *mangledNameZStr =
            zend_mangle_property_name(
                                  scope->name ? ZSTR_VAL(scope->name) : "",
                                  scope->name ? ZSTR_LEN(scope->name) : 0,
                                  const_cast<char*>(name),
                                  nameLenInBytes,
                                  false);
        *mangledName = ZSTR_VAL(*mangledNameZStr);
        *mangledNameLen = ZSTR_LEN(*mangledNameZStr);
#else
        zend_mangle_property_name(mangledName,
                                  mangledNameLen,
                                  scope->name,
                                  scope->name_length,
                                  const_cast<char*>(name),
                                  nameLenInBytes,
                                  false);
#endif
    }
}

zval* PHPExecEnvironment::getPropertyByName(zval* object,
                                            PHPExecEnvironment::AccessLevel accessLevel,
                                            const char* name,
                                            size_t nameLenInBytes,
                                            const zend_class_entry* scope) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (Z_TYPE_P(object) != IS_OBJECT)
        return NULL;

    char* mangledName = NULL;
    int mangledNameLen = 0;
    bool freeMangledName = true;

    // mangle the name
#if PHP_VERSION_ID >= 70000
    zend_string* mangledNameZStr = NULL;
    manglePropertyName(accessLevel, name, nameLenInBytes, scope, &mangledName, &mangledNameLen, &mangledNameZStr, &freeMangledName);
#else
    manglePropertyName(accessLevel, name, nameLenInBytes, scope, &mangledName, &mangledNameLen, &freeMangledName);
#endif

    // read the property from the hashtable
    ZendHashTable<RawZValHashTableValueAdapter> propertyTable(ZendHashTable<RawZValHashTableValueAdapter>::share(Z_OBJPROP_P(object)));
    zval* value = NULL;
    bool found = propertyTable.find(mangledName, mangledNameLen+1, &value);

    // free up resources after mangling
    if (freeMangledName) {
#if PHP_VERSION_ID >= 70000
        zend_string_release(mangledNameZStr);
        // mangledName points to a value in mangledNameZStr and doesn't need to be free'd separately.
#else
        pefree(mangledName, false);
#endif
    }

    if (!found)
        return NULL;

    return value;
}

bool PHPExecEnvironment::writePropertyByName(zval* object,
                                             PHPExecEnvironment::AccessLevel accessLevel,
                                             const char* name,
                                             size_t nameLenInBytes,
                                             const zend_class_entry* scope,
                                             const ZValPointerAny& value) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (Z_TYPE_P(object) != IS_OBJECT) {
        return false;
    }

    char* mangledName = NULL;
    int mangledNameLen = 0;
    bool freeMangledName = true;

    // mangle the name
#if PHP_VERSION_ID >= 70000
    zend_string* mangledNameZStr = NULL;
    manglePropertyName(accessLevel, name, nameLenInBytes, scope, &mangledName, &mangledNameLen, &mangledNameZStr, &freeMangledName);
#else
    manglePropertyName(accessLevel, name, nameLenInBytes, scope, &mangledName, &mangledNameLen, &freeMangledName);
#endif

    // write property to hashtable
    zval* local = value.get();
    Z_ADDREF_P(local);
    HashTable* obj_hash = (Z_OBJPROP_P(object));
#if PHP_VERSION_ID >= 70000
    zval* result = zend_hash_str_add(obj_hash, mangledName, mangledNameLen + 1, local);
    int res = result ? SUCCESS : FAILURE;
#else
    int res = zend_hash_add(obj_hash, mangledName, mangledNameLen + 1, &local, sizeof(zval*), NULL);
#endif

    // free up resources after mangling
    if (freeMangledName) {
#if PHP_VERSION_ID >= 70000
        zend_string_release(mangledNameZStr);
        // mangledName points to a value in mangledNameZStr and doesn't need to be free'd separately.
#else
        pefree(mangledName, false);
#endif
    }

    if (res != SUCCESS) {
        return false;
    }

    return true;
}

zval* PHPExecEnvironment::readPropertyByName(zval* object,
                                             const char* name,
                                             size_t nameLenInBytes) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (Z_TYPE_P(object) != IS_OBJECT)
        return NULL;

    PHPExecEnvironment::ExceptionState exceptionState(m_threadSafetyContext);
    DisableExecuteCallback disableExecute(m_threadSafetyContext);

    typedef boost::mpl::if_c< PHP_VERSION_ID >= 50400, const char*, char*>::type t_propNameType;
    t_propNameType propName = const_cast<t_propNameType>(name);
#if PHP_VERSION_ID >= 70000
    zval* rv;
    APPD_MAKE_STD_ZVAL(rv);
    zval* ptrToAllocatedZval = rv;
    zval* retval = zend_read_property(Z_OBJCE_P(object), object, propName, nameLenInBytes, 1, rv);
    if (ptrToAllocatedZval != rv)
        efree(ptrToAllocatedZval);
    return retval;
#else
    return zend_read_property(Z_OBJCE_P(object), object, propName, nameLenInBytes, 1 TSRMLS_CC);
#endif
}

std::string PHPExecEnvironment::getClassName(zval* object) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (Z_TYPE_P(object) != IS_OBJECT)
        return std::string();

#if PHP_VERSION_ID >= 70000
    zend_string* classNameZStr = Z_OBJCE_P(object) ? Z_OBJCE_P(object)->name : NULL;
    if (classNameZStr)
        return std::string(ZSTR_VAL(classNameZStr), ZSTR_LEN(classNameZStr));
    return std::string();

#else

    typedef boost::mpl::if_c< PHP_VERSION_ID >= 50400, const char*, char*>::type t_ClassName;

    t_ClassName className;
    zend_uint classNameLen;
    int dup = zend_get_object_classname(object, &className, &classNameLen TSRMLS_CC);

    std::string result(className, classNameLen);
    if (!dup)
        efree((void*)className);
    return result;
#endif
}

template <typename t_ThreadSafetyContext, typename t_Ret, typename t_callable, typename...t_Args>
struct CallWithExecuteDisabled
{
    static inline t_Ret call(t_ThreadSafetyContext threadSafetyContext,
                             const t_callable& callable,
                             t_Args...args)
    {
        t_Ret ret;
        bool didBail = true;
        {
            PHPExecEnvironment::ExceptionState exceptionState(threadSafetyContext);
            DisableExecuteCallback disableExecute(threadSafetyContext);

            zend_try {
                ret = callable(args...);
                didBail = false;
            }
            zend_catch {
            }
            zend_end_try();
        }
        // clear flag that disables execute callback.
        //
        // re-throw
        if (didBail)
            LONGJMP(*EG(bailout), FAILURE);
        return ret;
    }
};

template <typename t_ThreadSafetyContext, typename t_callable, typename...t_Args>
struct CallWithExecuteDisabled<t_ThreadSafetyContext, void, t_callable, t_Args...>
{
    static inline void call(t_ThreadSafetyContext threadSafetyContext,
                            const t_callable& callable,
                            t_Args...args)
    {
        bool didBail = true;
        {
            PHPExecEnvironment::ExceptionState exceptionState(threadSafetyContext);
            DisableExecuteCallback disableExecute(threadSafetyContext);

            zend_try {
                callable(args...);
                didBail = false;
            }
            zend_catch {
            }
            zend_end_try();
        }
        // clear flag that disables execute callback.
        //
        // re-throw
        if (didBail)
            LONGJMP(*EG(bailout), FAILURE);
    }
};

template <typename t_ThreadSafetyContext, typename t_callable, typename...t_Args>
typename boost::function_types::result_type<t_callable>::type callWithExecuteDisabled(t_ThreadSafetyContext threadSafetyContext, const t_callable& callable, t_Args...args)
{
    typedef typename boost::function_types::result_type<t_callable>::type t_Ret;
    return CallWithExecuteDisabled<t_ThreadSafetyContext,
                                   t_Ret,
                                   t_callable,
                                   t_Args...>::call(threadSafetyContext,
                                                    callable,
                                                    args...);
}


ZValPointerString PHPExecEnvironment::convertToString(zval* value) const
{
    // wrapper around convert_to_string(value);
    callWithExecuteDisabled(m_threadSafetyContext, _convert_to_string, value ZEND_FILE_LINE_CC);
    return ZValPointerAny::share(value).cast<ZValPointerString>();
}

ZValPointerAny PHPExecEnvironment::getStackTrace(int skipTopTraces, int options) const
{
    zval* trace;
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);

    APPD_MAKE_STD_ZVAL(trace);

#if PHP_VERSION_ID < 50400
    zend_fetch_debug_backtrace(trace, skipTopTraces, options TSRMLS_CC);
#else
    // 0 = full backtrace
    zend_fetch_debug_backtrace(trace, skipTopTraces, options, 0 TSRMLS_CC);
#endif
    return ZValPointerAny::take(trace);
}

ZValPointerAny PHPExecEnvironment::getSymbolFromSymbolTable(const std::string& symbolName) const
{
    if (!m_executeData)
        return ZValPointerAny();

    ZendHashTable<> symbolTable(ZendHashTable<>::share(m_executeData->symbol_table));
    if (!symbolTable.isValid())
        return ZValPointerAny();
    ZValPointerAny symbol;
    if (!symbolTable.find(symbolName, &symbol))
        return ZValPointerAny();
    return symbol;
}

bool PHPExecEnvironment::callCallable(ZValPointerAny* returnValue,
                                      const ZValPointerAny& callable,
                                      const std::vector<ZValPointerAny>* params) const
{
    zend_fcall_info_cache callInfoCache;
    if (!populateFunctionCallInfoCache(&callInfoCache, callable))
        return false;
    return callCallableViaCache(returnValue, callInfoCache, params);
}

bool PHPExecEnvironment::callInstanceMethod(ZValPointerAny* returnValue,
                                            zval* object,
                                            const char* methodName,
                                            size_t methodNameLen,
                                            const std::vector<ZValPointerAny>* params) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    if (Z_OBJ_HT_P(object)->get_method == NULL) {
        LOG4CXX_INFO(m_logger, Z_OBJ_CLASS_NAME_P(object) << " object does not support method calls");
        return false;
    }

#if PHP_VERSION_ID >= 70000
    zend_string* tempMethodNameZStr = zend_string_init(methodName, methodNameLen, false);
    zend_function* zendMethod = Z_OBJ_HT_P(object)->get_method(&(Z_OBJ_P(object)),
                                                               tempMethodNameZStr,
                                                               NULL);
    zend_string_release(tempMethodNameZStr);
#else
    zend_function* zendMethod = Z_OBJ_HT_P(object)->get_method(&object,
                                                               const_cast<char*>(methodName),
                                                               methodNameLen
    #if PHP_VERSION_ID >= 50400
                                                               , NULL
    #endif
                                                               TSRMLS_CC);
#endif

    if (zendMethod == NULL) {
        LOG4CXX_INFO(m_logger, "call to undefined method " << Z_OBJ_CLASS_NAME_P(object) << "::" << methodName);
        return false;
    }

    if (zendMethod->common.fn_flags & ZEND_ACC_ABSTRACT) {
        LOG4CXX_INFO(m_logger, "cannot invoked abstract method " << Z_OBJ_CLASS_NAME_P(object) << "::" << methodName);
        return false;
    }

    zend_class_entry* objectClassEntry = Z_OBJCE_P(object);

    zend_fcall_info_cache fcc;
    fcc.initialized = 1;
    fcc.function_handler = zendMethod;

#if PHP_VERSION_ID >= 70000
    fcc.called_scope = objectClassEntry;
    if (zendMethod->common.fn_flags & ZEND_ACC_STATIC) {
        fcc.object = NULL;
        fcc.calling_scope = zendMethod->common.scope;
    } else {
        BOOST_ASSERT(Z_TYPE_P(object) == IS_OBJECT);
        fcc.object = Z_OBJ_P(object);
        fcc.calling_scope = objectClassEntry;
    }
#elif PHP_VERSION_ID >= 50300
    fcc.called_scope = objectClassEntry;
    if (zendMethod->common.fn_flags & ZEND_ACC_STATIC) {
        fcc.object_ptr = NULL;
        fcc.calling_scope = zendMethod->common.scope;
    } else {
        fcc.object_ptr = const_cast<zval*>(object);
        fcc.calling_scope = objectClassEntry;
    }
#else
    if (zendMethod->common.fn_flags & ZEND_ACC_STATIC) {
        fcc.object_pp = NULL;
        fcc.calling_scope = zendMethod->common.scope;
    } else {
        fcc.object_pp = const_cast<zval**>(&object);
        fcc.calling_scope = objectClassEntry;
    }
#endif

    return callCallableViaCache(returnValue, fcc, params);
}

void PHPExecEnvironment::errorDocRef(const char* docRef, int type, const std::string& message) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    ErrorMonitor::SuppressionScope errorSuppressionScope(errorMonitor);
    php_error_docref(docRef, type, "%s", message.c_str());
}

InterceptionEngine* PHPExecEnvironment::getInterceptionEngine() const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return AG(icept_engine);
}

zend_execute_data* PHPExecEnvironment::getExecuteData() const
{
    return m_executeData;
}

#if PHP_VERSION_ID < 70000
zend_object_store_bucket* PHPExecEnvironment::getObjectStoreBucket(const zend_object_handle& handle) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return &EG(objects_store).object_buckets[handle];
}
#endif

bool PHPExecEnvironment::isCLISAPI() const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    return AG(is_cli_sapi);
}

bool PHPExecEnvironment::callCallableViaCache(ZValPointerAny* returnValue,
                                              zend_fcall_info_cache& callInfoCache,
                                              const std::vector<ZValPointerAny>* params) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
    zend_fcall_info callInfo = empty_fcall_info;
    zval* retZVal;
    callInfo.size = sizeof(zend_fcall_info);
#if PHP_VERSION_ID >= 70000
    APPD_MAKE_STD_ZVAL(retZVal);
    callInfo.retval = retZVal;
    if (params) {
        callInfo.params = reinterpret_cast<zval*>(alloca(sizeof(zval) * params->size()));
        int i = 0;
        BOOST_FOREACH(const auto& it, *params) {
            callInfo.params[i] = *(it.get());
            ++i;
        }
#else
    callInfo.retval_ptr_ptr = &retZVal;
    if (params) {
        zval** paramZVals = reinterpret_cast<zval**>(alloca(sizeof(zval*) * params->size()));
        callInfo.params = reinterpret_cast<zval***>(alloca(sizeof(zval**) * params->size()));
        int i = 0;
        BOOST_FOREACH(const auto& it, *params) {
            paramZVals[i] = it.get();
            callInfo.params[i] = &paramZVals[i];
            ++i;
        }
#endif
        callInfo.param_count = params->size();
    } else {
        callInfo.param_count = 0;
    }
    callInfo.no_separation = 1;

    AG(G)->disable_execute_callback = true;
    ExceptionState exceptionState(m_threadSafetyContext);
    int callRet = zend_call_function(&callInfo, &callInfoCache TSRMLS_CC);
    AG(G)->disable_execute_callback = false;

    if (callRet == FAILURE)
    {
        if (retZVal)
#if PHP_VERSION_ID >= 70000
            efree(retZVal);
#else
            APPD_ZVAL_PTR_DTOR(&retZVal);
#endif
        return false;
    }

    // if we get here, retZVal is pointing to a zval that was
    // not allocated in the agent, but rather to the return value
    // of the callable we called above.
    *returnValue = ZValPointerAny::take(retZVal);
    return true;
}

bool PHPExecEnvironment::populateFunctionCallInfoCache(zend_fcall_info_cache* functionCallInfoCache,
                                                       const ZValPointerAny& callableValue) const
{
    TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);

#if PHP_VERSION_ID >= 50300
    zend_bool isCallable =
        zend_is_callable_ex(callableValue.get(),
            NULL,
            IS_CALLABLE_CHECK_SILENT,
    #if PHP_VERSION_ID < 70000
            NULL, // char** and int* before PHP7, zend_string** in PHP7
    #endif
            NULL,
            functionCallInfoCache,
            NULL TSRMLS_CC);
    // There is a bug in zend_is_callable_ex() which does not alway set the
    // initialized flag even when the callable is found.
    if (isCallable)
        functionCallInfoCache->initialized = 1;
    return isCallable;
#else
    // Silence all errors from zend_is_callable_ex (called by
    // zend_fcall_info_init)
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    ErrorMonitor::SuppressionScope suppressionScope(errorMonitor, true);
    zend_fcall_info functionCallInfo;

    int result =
        zend_fcall_info_init(callableValue.get(),
                &functionCallInfo,
                functionCallInfoCache TSRMLS_CC);
    return (result == SUCCESS);
#endif
}

/* }}} */


// vim: set fdm=marker:
