/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#include "handle_registry.h"
#include "zval_helper.h"
#include "agent.h"

AHandleInfo::~AHandleInfo()
{
}

// unregister handle
// replace the dtor field with the original
// call orig destructor
#if PHP_VERSION_ID >= 70000
void HandleRegistry::objectDestructor(zend_object* object)
{
    BOOST_ASSERT(object);
    if(!object)
    {
      return;
    }
    zend_object_handle handle = object->handle;
    Key key(object);
#else
void HandleRegistry::objectDestructor(void* object, zend_object_handle handle TSRMLS_DC)
{
    Key key(handle);
#endif
    HandleRegistry& handleRegistry = AG(G)->exec_env->getHandleRegistry();
    auto i = handleRegistry.m_map.find(key);

    BOOST_ASSERT(i != handleRegistry.m_map.end());
    if(i == handleRegistry.m_map.end())
    {
        LOG4CXX_DEBUG(AG(G)->log_agent, "Key not found in  HandleRegistry::objectDestructor of object for key address = " << &key);
        return;
    }
    // Retrieve the original destructor function stored registerHandle(...)
    appd_obj_dtor_t orig_dtor = i->second.objDtor;

    // Erase the handle from the handle registry.
    handleRegistry.m_map.erase(i);

    // Replace the original destructor function and call the destructor.
#if PHP_VERSION_ID >= 70000
    APPD_OBJ_DTOR_P = orig_dtor;
    orig_dtor(object);
#else
    AG(G)->exec_env->getObjectStoreBucket(handle)->bucket.obj.dtor = orig_dtor;
    orig_dtor(object, handle TSRMLS_CC);
#endif
}

//void HandleRegistry::resourceDestructor(zend_rsrc_list_entry *rsrc TSRMLS_DC)
#if PHP_VERSION_ID >= 70000
void HandleRegistry::resourceDestructor(zval* rsrc)
{
    Key key(Z_RES_P(rsrc));
#else
void HandleRegistry::resourceDestructor(void* pDest TSRMLS_DC)
{
    zend_rsrc_list_entry* rsrc = (zend_rsrc_list_entry*) pDest;
    Key key(rsrc);
#endif

    HandleRegistry& handleRegistry = AG(G)->exec_env->getHandleRegistry();
    BOOST_ASSERT(handleRegistry.m_origResourceDtor != NULL);

    if(handleRegistry.m_origResourceDtor == NULL)
    {
      return;
    }
    auto i = handleRegistry.m_map.find(key);

    if(i != handleRegistry.m_map.end()) {
        // Erase the handle from the handle registry.
        handleRegistry.m_map.erase(i);
    }

    // takes a zend_rsrc_list_entry* in PHP 5, and a zend_resource* in PHP 7.
    handleRegistry.m_origResourceDtor(rsrc);
}

HandleRegistry::HandleRegistry(bool hookResourceDestruction,
                               bool hookObjectDestruction)
   : m_origResourceDtor(NULL)
   , m_hookResourceDestruction(hookResourceDestruction)
   , m_hookObjectDestruction(hookObjectDestruction)
   , m_didHookResourceDestructor(false)
{
}

HandleRegistry::~HandleRegistry()
{
    if (m_didHookResourceDestructor) {
        // Re-set the shared destructor for all resources.
        EG(regular_list).pDestructor = m_origResourceDtor;
    }

    if (m_hookObjectDestruction) {
        // Essential to call clear() to unhook destructors.
        clear();
    }
}

void HandleRegistry::unregisterHandle(const ZValPointerObject& zpObject)
{
#if PHP_VERSION_ID >= 70000
    Key key(zpObject.getZendObject());
#else
    Key key(zpObject.getObjectHandle());
#endif

    auto i = m_map.find(key);

    if (i == m_map.end())
        return;

    if (m_hookObjectDestruction) {
        // Retrieve the original destructor function stored registerHandle(...)
        appd_obj_dtor_t orig_dtor = i->second.objDtor;

        if (orig_dtor != NULL) {
            // Replace the original destructor function.
#if PHP_VERSION_ID >= 70000
            zend_object* object = zpObject.getZendObject();
            BOOST_ASSERT(object);
            if(!object)
            {
                return;
            }
#else
            zend_object_store_bucket* bucket = AG(G)->exec_env->getObjectStoreBucket(zpObject.getObjectHandle());
#endif
            APPD_OBJ_DTOR_P = orig_dtor;
            i->second.objDtor = NULL;
        }
    }

    m_map.erase(i);
}

void HandleRegistry::registerHandle_impl(const ZValPointerObject& zpObject, const boost::shared_ptr<AHandleInfo>& handleInfo)
{
    BOOST_ASSERT(handleInfo);

    if(!handleInfo)
    {
      return;
    }
    // Retrieve the destructor pointer for the object in question.
#if PHP_VERSION_ID >= 70000
    zend_object* object = zpObject.getZendObject();
    Key key(object);
#else
    zend_object_handle objectHandle(zpObject.getObjectHandle());
    Key key(objectHandle);
    zend_object_store_bucket* bucket = AG(G)->exec_env->getObjectStoreBucket(objectHandle);
#endif
    appd_obj_dtor_t objDtor = APPD_OBJ_DTOR_P;

    if (objDtor == &HandleRegistry::objectDestructor) {
        auto i = m_map.find(key);
        BOOST_ASSERT(i != m_map.end());
        if(i == m_map.end())
        {
          LOG4CXX_DEBUG(AG(G)->log_agent, "Key not found in HandleRegistry::registerHandle_impl of object for key address = " << &key);
          return;
        }
        objDtor = i->second.objDtor;
    }

    if (m_hookObjectDestruction) {
        // Replace the original object destructor with our own.
        APPD_OBJ_DTOR_P = &HandleRegistry::objectDestructor;
    }

    // Store the original destructor function in our HandleRegistry.
    m_map[key] = Value(handleInfo, objDtor);
}

const boost::shared_ptr<AHandleInfo> HandleRegistry::getHandleInfo_impl(const ZValPointerObject& zpObject, const AHandleInfo::TypeTag* const typeTag) const
{
#if PHP_VERSION_ID >= 70000
    Key key(zpObject.getZendObject());
#else
    Key key(zpObject.getObjectHandle());
#endif
    auto entryIter = m_map.find(key);
    if (entryIter == m_map.end() || !(entryIter->second.handleInfo) )
        return boost::shared_ptr<AHandleInfo>();
    BOOST_ASSERT(entryIter->second.handleInfo);
    // Make sure the handle info in the map is of the type
    // we are looking for, if not pretend the map did not
    // have an entry.
    if (entryIter->second.handleInfo->getTypeTag() != typeTag)
        return boost::shared_ptr<AHandleInfo>();
    return entryIter->second.handleInfo;
}


#if PHP_VERSION_ID >= 70000
static inline zend_resource* getResourceListEntry(const ZValPointerResource& zval)
{
    return zval.getZendResource();
}
#else
static inline zend_rsrc_list_entry* getResourceListEntry(const ZValPointerResource& zval)
{
    // If this assert is triggered, then the value in the HashTable is the size of the pointer, and the location
    // of the value in the hash table can be moved in zend_hash_update_current_key (since the key and value are
    // stored together in zend's hash table buckets.
    //
    // If bigger than the size of a void*, the value is never moved in memory, and our pointer logic is valid.
    BOOST_STATIC_ASSERT(sizeof(zend_rsrc_list_entry) > sizeof(void*));
    zend_rsrc_list_entry* le;
    if (zend_hash_index_find(&EG(regular_list), zval.getResource(), (void **) &le)==FAILURE)
        return NULL;

    return le;
}
#endif

void HandleRegistry::unregisterHandle(const ZValPointerResource& resource)
{
    t_HandleRegRsrcType const res = getResourceListEntry(resource);
    if (!res)
        return;

    Key key(res);
    m_map.erase(key);
}

void HandleRegistry::registerHandle_impl(const ZValPointerResource& resource, const boost::shared_ptr<AHandleInfo>& handleInfo)
{
    t_HandleRegRsrcType const res = getResourceListEntry(resource);
    if (!res) {
        // We should never get here, since this registerHandle should only be called on valid resources.
        BOOST_ASSERT_MSG(res, "registerHandle should only be called on valid resources");
        return;
    }
    Key key(res);
    BOOST_ASSERT(handleInfo);
    if(!handleInfo)
    {
      return;
    }
    m_map[key] = Value(handleInfo);

    if (m_hookResourceDestruction && (!m_didHookResourceDestructor)) {
        m_didHookResourceDestructor = true;
        m_origResourceDtor = EG(regular_list).pDestructor;
        EG(regular_list).pDestructor = HandleRegistry::resourceDestructor;
    }
}

const boost::shared_ptr<AHandleInfo> HandleRegistry::getHandleInfo_impl(const ZValPointerResource& resource, const AHandleInfo::TypeTag* const typeTag) const
{

    t_HandleRegRsrcType const res = getResourceListEntry(resource);
    if (!res) {
        // We should never get here, since this getHandleInfo should only be called on valid resources.
        BOOST_ASSERT_MSG(res, "getHandleInfo should only be called on valid resources");
        return boost::shared_ptr<AHandleInfo>();
    }
    Key key(res);
    auto entryIter = m_map.find(key);
    if (entryIter == m_map.end() || !(entryIter->second.handleInfo))
        return boost::shared_ptr<AHandleInfo>();
    BOOST_ASSERT(entryIter->second.handleInfo);
    // Make sure the handle info in the map is of the type
    // we are looking for, if not pretend the map did not
    // have an entry.
    if (entryIter->second.handleInfo->getTypeTag() != typeTag)
        return boost::shared_ptr<AHandleInfo>();
    return entryIter->second.handleInfo;
}

void HandleRegistry::unregisterHandle(const ZValPointerString& handle)
{
    Key key(handle.getStringValue());
    m_map.erase(key);
}

void HandleRegistry::registerHandle_impl(const ZValPointerString& handle, const boost::shared_ptr<AHandleInfo>& handleInfo)
{
    Key key(handle.getStringValue());
    BOOST_ASSERT(handleInfo);
    if(!handleInfo)
    {
        return;
    }
    m_map[key] = Value(handleInfo);
}

const boost::shared_ptr<AHandleInfo> HandleRegistry::getHandleInfo_impl(const ZValPointerString& handle, const AHandleInfo::TypeTag* const typeTag) const
{
    Key key(handle.getStringValue());
    auto entryIter = m_map.find(key);
    if (entryIter == m_map.end() || !(entryIter->second.handleInfo))
        return boost::shared_ptr<AHandleInfo>();
    BOOST_ASSERT(entryIter->second.handleInfo);
    // Make sure the handle info in the map is of the type
    // we are looking for, if not pretend the map did not
    // have an entry.
    if (entryIter->second.handleInfo->getTypeTag() != typeTag)
        return boost::shared_ptr<AHandleInfo>();
    return entryIter->second.handleInfo;
}

void HandleRegistry::unregisterHandle(const ZValPointerAny& any)
{
    switch (any.getType())
    {
    case ZValPointer::ZVal_Object:
        {
            ZValPointerObject object(any.cast<ZValPointerObject>());
            unregisterHandle(object);
        }
        break;
    case ZValPointer::ZVal_Resource:
        {
            ZValPointerResource resource(any.cast<ZValPointerResource>());
            unregisterHandle(resource);
        }
        break;
    case ZValPointer::ZVal_String:
        {
            ZValPointerString handle(any.cast<ZValPointerString>());
            unregisterHandle(handle);
        }
        break;
    };
}

void HandleRegistry::registerHandle_impl(const ZValPointerAny& any, const boost::shared_ptr<AHandleInfo>& handleInfo)
{
    BOOST_ASSERT(handleInfo);
    if(!handleInfo)
    {
        return;
    }
    switch (any.getType())
    {
    case ZValPointer::ZVal_Object:
        {
            ZValPointerObject object(any.cast<ZValPointerObject>());
            registerHandle(object, handleInfo);
        }
        break;
    case ZValPointer::ZVal_Resource:
        {
            ZValPointerResource resource(any.cast<ZValPointerResource>());
            registerHandle(resource, handleInfo);
        }
        break;
    case ZValPointer::ZVal_String:
        {
            ZValPointerString handle(any.cast<ZValPointerString>());
            registerHandle(handle, handleInfo);
        }
        break;
    };
}

HandleRegistry::Key HandleRegistry::Key::create(const ZValPointerAny& p)
{
    switch (p.getType())
    {
        case ZValPointer::ZVal_Object:
            {
#if PHP_VERSION_ID >= 70000
                return Key(p.cast<ZValPointerObject>().getZendObject());
#else
                return Key(p.cast<ZValPointerObject>().getObjectHandle());
#endif
            }
            break;

        case ZValPointer::ZVal_Resource:
            {
                ZValPointerResource resource(p.cast<ZValPointerResource>());
                t_HandleRegRsrcType const res = getResourceListEntry(resource);
                return Key(res);
            }
            break;

        case ZValPointer::ZVal_String:
            {
                return Key(p.cast<ZValPointerString>().getStringValue());
            }
            break;

        default:
            // TODO: mysqli AQueryInterceptor getHandle does not check if the zval is of type False
            // before it calls the handle registration. For now, this Key is created with the default empty string.
            //BOOST_ASSERT_MSG(false, "HandleRegistry::Key must have zval of type object, resource, or string");
            return Key("");
    };
}

const boost::shared_ptr<AHandleInfo> HandleRegistry::getHandleInfo_impl(const ZValPointerAny& any, const AHandleInfo::TypeTag* const typeTag) const
{
    Key key(Key::create(any));

    auto entryIter = m_map.find(key);
    if (entryIter == m_map.end() || !(entryIter->second.handleInfo))
        return boost::shared_ptr<AHandleInfo>();
    BOOST_ASSERT(entryIter->second.handleInfo);
    // Make sure the handle info in the map is of the type
    // we are looking for, if not pretend the map did not
    // have an entry.
    if (entryIter->second.handleInfo->getTypeTag() != typeTag)
        return boost::shared_ptr<AHandleInfo>();
    return entryIter->second.handleInfo;
}

void HandleRegistry::clear()
{
    // Re-set any replaced destructors for objects
    for (auto i = m_map.begin(); i != m_map.end(); ++i) {
        if (i->first.type != KeyType_Object)
            continue;

        // Get the object from the object handle.

        BOOST_ASSERT(i->second.objDtor != NULL);
        if(i->second.objDtor == NULL)
        {
            return;
        }

        // Set the object's destructor to the original destructor stored on registerHandle(...)
#if PHP_VERSION_ID >= 70000
        zend_object* object = i->first.object;
        BOOST_ASSERT(object);
        if(!object)
        {
            return;
        }
#else
        zend_object_store_bucket* bucket = &(EG(objects_store).object_buckets[i->first.object]);
#endif
        APPD_OBJ_DTOR_P = i->second.objDtor;
        i->second.objDtor = NULL;
    }

    m_map.clear();
}
