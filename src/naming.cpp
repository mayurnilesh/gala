/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#include "enumfactory.h"
#include "http/httpentrypoint.h"
#include "naming.h"
#include "zval_helper.h"
#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

/*
 * uri naming scheme keys
 */
#define URI_NAMING_SCHEME_KEYS_DECL(XX) \
    XX(URI_NAMING_KEY_SEGMENT_SCHEME, "uri-length",) \
    XX(URI_NAMING_KEY_SEGMENT_LENGTH, "segment-length",) \
    XX(URI_NAMING_KEY_SUFFIX_SCHEME, "uri-suffix-scheme",) \
    XX(URI_NAMING_KEY_SUFFIX_KEY, "suffix-key",) \
    XX(URI_NAMING_KEY_DELIMITER, "delimiter",)
DECLARE_ENUM(URINamingSchemeKeys, URI_NAMING_SCHEME_KEYS_DECL)

/*
 * uri naming segment scheme enum
 */
#define URISEGMENTSCHEME_DECL(XX) \
    XX(URI_SEGMENT_SCHEME_FULL, "full",) \
    XX(URI_SEGMENT_SCHEME_FIRST_N, "first-n-segments",) \
    XX(URI_SEGMENT_SCHEME_LAST_N, "last-n-segments",)
DECLARE_ENUM(URISegmentScheme, URISEGMENTSCHEME_DECL)

/*
 * uri naming suffix scheme enum
 */
#define URI_SUFFIX_SCHEME_DECL(XX) \
    XX(URI_SUFFIX_SCHEME_SEGMENT_NUMBER, "uri-segment-number",) \
    XX(URI_SUFFIX_SCHEME_PARAM_VALUE, "param-value",) \
    XX(URI_SUFFIX_SCHEME_METHOD, "method",) \
    XX(URI_SUFFIX_SCHEME_COOKIE_VALUE, "cookie-value",) \
    XX(URI_SUFFIX_SCHEME_SESSION_VALUE, "session-key",) \
    XX(URI_SUFFIX_SCHEME_HEADER_VALUE, "header-value",)
DECLARE_ENUM(URISuffixScheme, URI_SUFFIX_SCHEME_DECL)


DEFINE_ENUM(URINamingSchemeKeys, URI_NAMING_SCHEME_KEYS_DECL)
DEFINE_ENUM(URISegmentScheme, URISEGMENTSCHEME_DECL)
DEFINE_ENUM(URISuffixScheme, URI_SUFFIX_SCHEME_DECL)

/**
 * Name of the property that specifies whether to prefix the transaction name
 * with the virtual host value.
 */
static const char VIRTUAL_HOST_ENABLED_KEY[] = "PHP_VIRTUAL_HOST_ENABLED";

/**
    Null terminated string containing the URI_SEGMENT_SEPARATOR character,
    which is used to separate the segments of a path in a URI.
 */
static const char URI_SEGMENT_SEPARATOR_STR[] = {URI_SEGMENT_SEPARATOR, '\0'};

/**
    Character used to separate multiple values in the URI_NAMING_KEY_SUFFIX_KEY
    entry in the string dictionary the configures the automatic business transaction
    logic
  */
static const char URI_SUFFIX_KEY_SEPARATOR = ',';

/**
    Null terminated string containing the URI_SUFFIX_KEY_SEPARATOR, which is
    used to separate multiple values in the URI_NAMING_KEY_SUFFIX_KEY
    entry in the string dictionary the configures the automatic business transaction
    logic
  */
static const char URI_SUFFIX_KEY_SEPARATOR_STR[] = {URI_SUFFIX_KEY_SEPARATOR, '\0'};


static const char URI_PARAMETER_DELIMITER = '.';
static const char URI_COOKIE_DELIMITER = '.';
static const char URI_SESSION_DELIMITER = '.';
static const char URI_HEADER_DELIMITER = '.';
static const char URI_SUFFIX_SEPARATOR = '.';

static const char VIRTUAL_HOST_SEPARATOR_STR[] = ": ";


namespace GetRequestData
{

/**
    The name of the entry of the server array that contains the HTTP method of
    the request for the currently running php script.
 */
static const char serverRequestMethodKey[] = "REQUEST_METHOD";
static const size_t serverRequestMethodKeyLen = sizeof(serverRequestMethodKey) - 1;

/**
    The name of the entry of the server array that contains the name of the virtual
    host that the received the request for the currently running php script.
 */
static const char serverServerNameAddrKey[] = "SERVER_NAME";
static const size_t serverServerNameAddrKeyLen = sizeof(serverServerNameAddrKey) - 1;


}

/**
    Parse a comma separated list of integers greater than 0 from the specified string into
    the specified boost::dynamic_bitset.

    This function is used to parse the the url components list from the UI.

    @param s The string to parse.  Segment numbers in this string should be separated by
    URI_SUFFIX_KEY_SEPARATOR_STR.  Any segment number that contains a character other than
    a decimal digit will be ignored.
    @param result A bitset where each integer parsed out of the specified string s, will
    have its corresponding bit in the bitset set.  Since the integers parsed out
    of s can not be 0, the index of a bit in the bitset for a given segment number n
    is n - 1.
 */
static void parseSegmentNumbers(const std::string& s, boost::dynamic_bitset<>* result)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(URI_SUFFIX_KEY_SEPARATOR_STR);
    tokenizer keyTokens(s.begin(), s.end(), sep);
    auto i = keyTokens.begin();
    const auto end = keyTokens.end();
    while (i != end)
    {
        unsigned segmentNumber = 0;
        std::string segmentNumberString = *i;
        ++i;
        try
        {
            segmentNumber = boost::lexical_cast<unsigned>(segmentNumberString);
            //0 segment means nothing. Segment numbers
            //are 1 indexed, not 0 indexed.
            if (segmentNumber == 0)
                continue;
            --segmentNumber; // convert to 0 indexing.
            if (result->size() <= segmentNumber)
                result->resize(segmentNumber + 1, false);
            result->set(segmentNumber, true);
        }
        catch (const boost::bad_lexical_cast& e)
        {
            // Ignore things we can't convert to an unsigned int.
        }
    }
}

/**
    Builds a business transaction name using the segments of specified the URI path
    specified by the comma separated list of integers in the specified suffix key.
    The extracted segments are joined together with the specified delimiter string.

    For example:
        transformNameWithURISegments("/foo/bar/baz/a.php", "/foo/bar/baz/a.php",
            "3,1", ".") will yield "foo.baz".  Notice that the order of
            the integers in the suffixKey does not matter.

    If the result of this function would otherwise be empty, the specified
    baseName string is returned.

    @param baseName The name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.  This is the name that will be returned
    if no valid segment numbers are parsed out of the specified suffixKey string.
    @param urlPath The path of a the URL used to request the currently executing php
    script.  This is the path from which segments are extracted.
    @param suffixKey A string containing a comma separated list of integers which
    specifies which segments to extract from the specified urlPath string to build
    the business transaction name returned by this function.
    @param delimiter A string that a should be used to join the list of url segments
    together to build the business transaction name returned by this function.

    @return A business transaction name.
 */
static std::string transformNameWithURISegments(const std::string baseName,
    const std::string& urlPath,
    const std::string& suffixKey,
    const std::string& delimiter)
{
    std::string result;
    // as good as guess as any as to the size of the result...
    result.reserve(urlPath.length());

    boost::dynamic_bitset<> segmentNumbersBitSet;
    parseSegmentNumbers(suffixKey, &segmentNumbersBitSet);

    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(URI_SEGMENT_SEPARATOR_STR, "", boost::keep_empty_tokens);
    tokenizer uriTokens(urlPath.begin(), urlPath.end(), sep);
    auto i = uriTokens.begin();
    const auto end = uriTokens.end();

    // Skip the first token if it exists and is an empty token.
    // There is almost certainly always an empty token at the
    // front of the URL, because all URLs are absolute and
    // thus start with a '/' ( URI_SEGMENT_SEPARATOR ).
    if ((i != end) && (((*i).length()) == 0))
        ++i;

    unsigned currentSegmentNumber = 0;
    while ((i != end) && (currentSegmentNumber < segmentNumbersBitSet.size()))
    {
        if (segmentNumbersBitSet.test(currentSegmentNumber))
        {
            if (!result.empty())
                result += delimiter;
            result += *i;
        }
        ++currentSegmentNumber;
        ++i;
    }
    // If there were no valid segment numbers
    // in the suffix key, just return the original base name.
    if (result.empty())
        return baseName;
    return result;
}

/**
    Appends the value of a parameter with the specified name from the specified php
    array to the specified target string.

    @param target string to append to.  This string is being used to build up
    a business transaction name.
    @param paramDictionary php array containing parameter values.
    @param paramName The name of the entry to extract for the specified php array.
 */
static void appendParamValue(std::string* target,
                             const ZValPointerArray& paramDictionary,
                             const std::string& paramName)
{
    ZValPointerString paramValueString(paramDictionary.findEntryByName<ZValPointerString>(paramName.c_str(), paramName.length()));
    if (paramValueString)
    {
        if (!target->empty())
            (*target) += URI_PARAMETER_DELIMITER;
        (*target) += paramValueString.getStringValue();
    }
}

/**
    Builds a transaction name using the values of parameters specified
    in a comma separated list of parameter names.  Both POST and GET parameter
    values are used.

    @param baseName Base name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @param suffixKey A string containing a comma separated list of parameter names.
 */
static std::string transformNameWithParamValue(const boost::shared_ptr<HTTPPayload>& payload,
                                               const std::string& baseName,
                                               const std::string suffixKey)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(URI_SUFFIX_KEY_SEPARATOR_STR);
    tokenizer keyTokens(suffixKey.begin(), suffixKey.end(), sep);
    auto i = keyTokens.begin();
    const auto end = keyTokens.end();

    ZValPointerArray postDataArray(payload->getPostParams());
    if (!postDataArray)
        return baseName;

    ZValPointerArray getDataArray(payload->getGetParams());
    if (!getDataArray)
        return baseName;

    std::string result;

    while (i != end)
    {
        std::string paramName = *i;
        appendParamValue(&result, postDataArray, paramName);
        appendParamValue(&result, getDataArray, paramName);
        ++i;
    }
    if (result.empty())
        return baseName;
    return baseName + URI_SUFFIX_SEPARATOR + result;
}

/**
    Extracts a value from the $_SERVER php superglobal array.
    @param serverArrayKey Name of the entry from the $_SERVER superglobal php array to
    extract.
    @param serverArrayKeyLen Length of serverArrayKey excluding terminating null in bytes.
    @return ZValPointerString that refers to the value of the specified entry in the
    $_SERVER php superglobal array.
 */
static ZValPointerString getServerArrayValue(const boost::shared_ptr<HTTPPayload>& payload,
                                             const char* serverArrayKey,
                                             size_t serverArrayKeyLen)
{
    ZValPointerArray serverArray(payload->getServerArray());
    if (!serverArray)
        return ZValPointerAny().cast<ZValPointerString>();

    return serverArray.findEntryByName<ZValPointerString>(serverArrayKey, serverArrayKeyLen);
}

/**
    Helper function that builds a business transaction name from the value of an entry
    in the $_SERVER superglobal php array.

    @param baseName The name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @param serverArrayKey The name of the entry in the $_SERVER superglobal php array
    whose value should be used to build the business transaction name.
    @param serverArrayKeyLen Length of serverArrayKey excluding terminating null in bytes.
    @return A business transaction name.
 */
static std::string transformNameWithServerArrayValue(const boost::shared_ptr<HTTPPayload>& payload,
                                                     const std::string& baseName,
                                                     const char* serverArrayKey,
                                                     size_t serverArrayKeyLen)
{
    ZValPointerString serverArrayValue(getServerArrayValue(payload,
                                                           serverArrayKey,
                                                           serverArrayKeyLen));
    if (!serverArrayValue)
        return baseName;

    return baseName + URI_SUFFIX_SEPARATOR + serverArrayValue.getStringValue();
}

/**
    Builds a business transaction name using the specified baseName and the HTTP method
    of the request of the currently executing php script.

    @param baseName The name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @return A business transaction name.
 */
static std::string transformNameWithHTTPMethod(const boost::shared_ptr<HTTPPayload>& payload,
                                               const std::string& baseName)
{
    return transformNameWithServerArrayValue(payload,
                                             baseName,
                                             GetRequestData::serverRequestMethodKey,
                                             GetRequestData::serverRequestMethodKeyLen);
}

/**
    Builds a transaction name using the values of cookies specified
    in a comma separated list of cookie names.

    @param baseName Base name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @param suffixKey A string containing a comma separated list of cookie names.
    @return A business transaction name.
 */
static std::string transformNameWithCookieValue(const boost::shared_ptr<HTTPPayload>& payload,
                                                const std::string& baseName,
                                                const std::string& suffixKey)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(URI_SUFFIX_KEY_SEPARATOR_STR);
    tokenizer keyTokens(suffixKey.begin(), suffixKey.end(), sep);
    auto i = keyTokens.begin();
    const auto end = keyTokens.end();

    ZValPointerArray cookiesArray(payload->getCookies());
    if (!cookiesArray)
        return baseName;

    std::string result;
    while (i != end)
    {
        std::string cookieName(*i);
        ++i;
        ZValPointerString cookieValue(cookiesArray.findEntryByName<ZValPointerString>(cookieName.c_str(), cookieName.length()));
        if (!cookieValue)
            continue;
        if (!result.empty())
            result += URI_COOKIE_DELIMITER;
        result += cookieValue.getStringValue();
    }

    if (result.empty())
        return baseName;
    return baseName + URI_SUFFIX_SEPARATOR + result;
}

/**
    Builds a transaction name using the values of session parameters specified
    in a comma separated list of session parameter names.

    @param baseName Base name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @param suffixKey A string containing a comma separated list of session parameter
    names.
    @return A business transaction name.
 */
static std::string transformNameWithSessionValue(const std::string& baseName, const std::string& suffixKey)
{
    // TODO implement me!!!!
    return baseName;
}

/**
    Builds a transaction name using the values of the HTTP headers specified
    in a comma separated list of HTTP header names.

    @param baseName Base name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @param suffixKey A string containing a comma separated list of HTTP header
    names.
    @return A business transaction name.
 */
static std::string transformNameWithHeaderValue(const boost::shared_ptr<HTTPPayload>& payload, const std::string& baseName, const std::string& suffixKey)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(URI_SUFFIX_KEY_SEPARATOR_STR);
    tokenizer keyTokens(suffixKey.begin(), suffixKey.end(), sep);
    auto i = keyTokens.begin();
    const auto end = keyTokens.end();

    std::string result;
    while (i != end)
    {
        std::string httpHeaderName(*i);
        std::string httpHeaderValue;
        ++i;
        bool haveHeader =
            payload->getHeaderValue(httpHeaderName, &httpHeaderValue);
        if (!haveHeader)
            continue;
        if (!result.empty())
            result += URI_HEADER_DELIMITER;
        result += httpHeaderValue;
    }

    if (result.empty())
        return baseName;
    return baseName + URI_SUFFIX_SEPARATOR + result;
}

/**
    Helper function that extracts a property from a StringMap of properties.  If there
    is no specified property with the specified key, the empty string is returned.

    @param properties StringMap of properties.
    @param key Name of the property to extract.
    @return The value of the specified property or the empty string if there is no
    property with the specified key in the StringMap.
 */
static inline std::string getProperty(const StringMap& properties, const std::string key)
{
    auto i = properties.find(key);
    if (i == properties.end())
        return std::string();
    return i->second;
}

std::string transformNameWithSplit(const boost::shared_ptr<HTTPPayload>& payload,
                                                   const std::string& baseName,
                                                   const std::string& fullURIPath,
                                                   const std::map<std::string, std::string>& properties)
{
    std::string const suffixSchemeString =
        getProperty(properties, enum2str(URI_NAMING_KEY_SUFFIX_SCHEME));

    if ((enum2str(URI_SEGMENT_SCHEME_FIRST_N) != suffixSchemeString) &&
        (enum2str(URI_SEGMENT_SCHEME_LAST_N) != suffixSchemeString) &&
        (enum2str(URI_SUFFIX_SCHEME_SEGMENT_NUMBER) != suffixSchemeString))
        return transformNameWithAdditionalRequestInfo(payload, baseName, fullURIPath, properties);

    std::string const suffixKey =
        getProperty(properties, enum2str(URI_NAMING_KEY_SUFFIX_KEY));

    std::string splitSuffix;
    if (suffixSchemeString == enum2str(URI_SUFFIX_SCHEME_SEGMENT_NUMBER)) {
        splitSuffix = transformNameWithURISegments(baseName,
                                                   fullURIPath,
                                                   suffixKey,
                                                   getProperty(properties,
                                                               enum2str(URI_NAMING_KEY_DELIMITER)));
    }
    else {
        try {
            unsigned nSegments = boost::lexical_cast<unsigned>(suffixKey);
            if (suffixSchemeString == enum2str(URI_SEGMENT_SCHEME_FIRST_N)) {
                splitSuffix = getFirstNSegments<std::string>(fullURIPath, nSegments);
            }
            else if (suffixSchemeString == enum2str(URI_SEGMENT_SCHEME_LAST_N)) {
                splitSuffix = getLastNSegments<std::string>(fullURIPath, nSegments);
            }
        }
        catch (const boost::bad_lexical_cast& e) {
            return baseName;
        }
    }
    if (splitSuffix.empty())
        return baseName;
    return baseName + URI_SUFFIX_SEPARATOR + splitSuffix;
}
/**
    Transforms the base business transaction name into the final business transaction
    name using information from the HTTP request of the currently executing php script.

    @param baseName Base name of the business transaction computed using the
    URI_NAMING_KEY_SEGMENT_SCHEME.
    @param fullURIPath The full path of the URI in the request of the currently running
    php script.
    @param properties StringMap of key/value pairs that configures the business transaction
    name algorithm.  For a set of possible keys see the URINamingSchemeKeys enum.
    @param A string allocated on the default C++ heap containing a business transaction
    name.
 */
std::string transformNameWithAdditionalRequestInfo(const boost::shared_ptr<HTTPPayload>& payload,
                                                   const std::string& baseName,
                                                   const std::string& fullURIPath,
                                                   const std::map<std::string, std::string>& properties)
{
    std::string const suffixSchemeString =
        getProperty(properties, enum2str(URI_NAMING_KEY_SUFFIX_SCHEME));

    URISuffixScheme const suffixScheme = str2enum<URISuffixScheme>(suffixSchemeString);

    std::string suffixKey =
        getProperty(properties, enum2str(URI_NAMING_KEY_SUFFIX_KEY));

    switch (suffixScheme)
    {
    case URI_SUFFIX_SCHEME_SEGMENT_NUMBER:
        return transformNameWithURISegments(baseName,
                                            fullURIPath,
                                            suffixKey,
                                            getProperty(properties, enum2str(URI_NAMING_KEY_DELIMITER)));
    case URI_SUFFIX_SCHEME_PARAM_VALUE:
        return transformNameWithParamValue(payload, baseName, suffixKey);
    case URI_SUFFIX_SCHEME_METHOD:
        return transformNameWithHTTPMethod(payload, baseName);
    case URI_SUFFIX_SCHEME_COOKIE_VALUE:
        return transformNameWithCookieValue(payload, baseName, suffixKey);
    case URI_SUFFIX_SCHEME_SESSION_VALUE:
        return transformNameWithSessionValue(baseName, suffixKey);
    case URI_SUFFIX_SCHEME_HEADER_VALUE:
        return transformNameWithHeaderValue(payload, baseName, suffixKey);
    };

    return baseName;
}

std::string transformNameWithVirtualHost(const std::string& baseName,
                                         const std::map<std::string, std::string>* properties,
                                         const PHPExecEnvironment* execEnv)
{
    auto virtualHostEnabledIter = properties->find(VIRTUAL_HOST_ENABLED_KEY);
    if (virtualHostEnabledIter == properties->end())
        return baseName;

    bool useVirtualHost =  (virtualHostEnabledIter->second == "true");
    if (!useVirtualHost)
        return baseName;

    ZValPointerString serverNameValue(getServerArrayValue(execEnv->getHTTPPayload(),
                                                          GetRequestData::serverServerNameAddrKey,
                                                          GetRequestData::serverServerNameAddrKeyLen));
    if (!serverNameValue)
        return baseName;

    return serverNameValue.getStringValue() + VIRTUAL_HOST_SEPARATOR_STR + baseName;
}

std::string getHTTPEntryPointBTName(const AgentLogger& logger,
                                    const boost::shared_ptr<HTTPPayload>& httpPayload,
                                    const std::map<std::string, std::string>* properties)
{
    // The logic in the method should be more or less the same as the logic in
    // com.singularity.ee.agent.appagent.services.transactionmonitor.http.servlet.config.naming.URINamingScheme.getName()

    const std::string& url = httpPayload->getURL();

    // We can't name empty URLs.
    if (url.empty())
        return std::string();

    std::string const suffixSchemeString =
        getProperty(*properties, enum2str(URI_NAMING_KEY_SUFFIX_SCHEME));

    URISuffixScheme const suffixScheme = str2enum<URISuffixScheme>(suffixSchemeString);

    // look for a scheme
    auto segmentSchemeIter = properties->find(enum2str(URI_NAMING_KEY_SEGMENT_SCHEME));
    if (segmentSchemeIter == properties->end())
        return transformNameWithAdditionalRequestInfo(httpPayload, url, url, *properties);

    // look for the number of segments to consider.
    auto nSegmentsIter = properties->find(enum2str(URI_NAMING_KEY_SEGMENT_LENGTH));
    if (nSegmentsIter == properties->end())
        return transformNameWithAdditionalRequestInfo(httpPayload, url, url, *properties);

    // Convert the scheme string into an enum value.
    URISegmentScheme scheme = str2enum<URISegmentScheme>(segmentSchemeIter->second);
    if (scheme == -1)
    {
        LOG4CXX_WARN(logger, "unrecognized URI naming scheme: " << segmentSchemeIter->second);
        return url;
    }

    if (scheme == URI_SEGMENT_SCHEME_FULL)
        return transformNameWithAdditionalRequestInfo(httpPayload, url, url, *properties);

    // Convert the string for the number of segments
    // to an integer.
    int nSegments;
    try
    {
        nSegments = boost::lexical_cast<int>(nSegmentsIter->second);
    }
    catch (const boost::bad_lexical_cast& e)
    {
        LOG4CXX_WARN(logger,
            "URI naming scheme: Unable to convert " << nSegmentsIter->second << " to an integer: " << e.what());
        return url;
    }

    if (nSegments < 1)
    {
        LOG4CXX_WARN(logger, "URI naming scheme requires a positive segment count.  " << nSegments << "was specified!");
        return url;
    }

    std::string baseName(url);
    if (scheme == URI_SEGMENT_SCHEME_FIRST_N)
        baseName = getFirstNSegments<std::string>(url, nSegments);
    else if (scheme == URI_SEGMENT_SCHEME_LAST_N)
        baseName = getLastNSegments<std::string>(url, nSegments);

    baseName = transformNameWithAdditionalRequestInfo(httpPayload, baseName, url, *properties);
    return transformNameWithVirtualHost(baseName, properties, httpPayload->getExecEnvironment());
}
