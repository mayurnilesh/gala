/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/


#ifndef __AGENT_H
#define __AGENT_H

#include <sys/resource.h>
#include "php.h"
extern "C" {

#include "php_ini.h"
#include "ext/standard/info.h"
#include "Zend/zend_extensions.h"
#include "TSRM.h"

extern zend_module_entry appdynamics_agent_module_entry;

}

#if ZEND_MODULE_API_NO >= 20100409
#define ZEND_ENGINE_2_4
#endif
#if ZEND_MODULE_API_NO > 20060613
#define ZEND_ENGINE_2_3
#endif
#if ZEND_MODULE_API_NO > 20050922
#define ZEND_ENGINE_2_2
#endif
#if ZEND_MODULE_API_NO > 20050921
#define ZEND_ENGINE_2_1
#endif


#ifdef ZTS
#define AG(v) TSRMG(agent_globals_id, zend_agent_globals *, v)
#else
#define AG(v) (agent_globals.v)
#endif

#include "callgraph/call_graph_collection_state.h"
#include "common.h"
#include "configs.h"
#include "db/oracle_connection_string.h"
#include "delayed_functions.h"
#include "dit_flags.h"
#include "extension_globals.h"
#include "frameworks/frameworks.h"
#include "http/http_correlation.h"
#include "intercept.h"
#include "services.h"
#include "timer.h"
#include "zval_helper.h"
#include <boost/utility.hpp>
#include <zmq.hpp>

class RequestContext;

namespace EUM
{
    class Config;
}

class DisableSignalsScope : boost::noncopyable
{
    public:
        DisableSignalsScope()
        {
            // Disable all signals
            sigset_t signalSet;
            int rc = sigfillset (&signalSet);
            BOOST_ASSERT(rc == 0);
            rc = pthread_sigmask (SIG_BLOCK, &signalSet, &m_oldSet);
            BOOST_ASSERT(rc == 0); (void)rc;
        }

        ~DisableSignalsScope()
        {
            int rc = pthread_sigmask(SIG_SETMASK, &m_oldSet, NULL);
            BOOST_ASSERT(rc == 0); (void)rc;
        }

    private:
        sigset_t m_oldSet;
};

/* {{{ Module globals */

/*
 * This class holds non-pointer C++ object module globals.
 * We need this because module globals is a POD (and we use offsetof() in
 * it).
 */
class CXXGlobals : boost::noncopyable
{
    public:
        static void create(TSRMLS_D);
        static void destroy(TSRMLS_D);

        static CXXGlobals& get(TSRMLS_D);

        Timer timer;
        log4cxx::LoggerPtr log_agent;
        boost::shared_ptr<PHPExecEnvironment> exec_env;
        boost::shared_ptr<RequestContext> request_context;

        CallGraph::GraphCollectionState callGraphCollectionState;

        /**
            A table of functions that we can't link directly against and still
            load properly on CentOS.
         */
        DelayedFunctions::FunctionTable const delayed_functions;

        /**
            A table of pointers to TSRM aware global state from other
            extensions.  We can't directly reference the global state
            and still load properly on CentOS.
         */
        DelayedExtensionGlobals::Globals const delayed_ext_globals;

        HTTPCorrelationContext httpCorrelationContext;

#if PHP_VERSION_ID >= 50300
        user_opcode_handler_t prev_exit_opcode_handler;
#endif

        bool disable_execute_callback;

        bool inExecution;

        bool doesMysqlUseMysqlnd;

        FrameworksFlags enabled_frameworks;

        DITFlags dit_flags;

        oci::OCINameCache ociNameCache;

        struct
        {
            void (*__call)(INTERNAL_FUNCTION_PARAMETERS);
            void (*__soapCall)(INTERNAL_FUNCTION_PARAMETERS);
            void (*__doRequest)(INTERNAL_FUNCTION_PARAMETERS);
        } soapClientOrigHandlers;

        const zend_class_entry* soapClientClassEntry;
        const zend_class_entry* soapFaultClassEntry;

        zval* mysqlDefaultLinkHandle;
        zval* pgsqlDefaultLinkHandle;

        inline bool isExitPointEnabled(appdynamics::pb::Agent::ExitPointType type) const
        {
            return m_isExitPointEnabled[type];
        }
        inline void setExitPointEnabled(appdynamics::pb::Agent::ExitPointType type, bool enabled)
        {
            BOOST_ASSERT(appdynamics::pb::Agent::ExitPointType_IsValid(type));
            m_isExitPointEnabled[type] = enabled;
        }

        bool didAddCallUserFuncInterceptor;

        boost::shared_ptr<IConfigChannel> configChannel;

    private:
        CXXGlobals(TSRMLS_D);
        ~CXXGlobals();
        bool m_isExitPointEnabled[appdynamics::pb::Agent::ExitPointType_ARRAYSIZE];
};

ZEND_BEGIN_MODULE_GLOBALS(agent)
    /* INI variables */

    /**
        Flag that ensures agent_lazy_init_globals
        only does anything the first time it is called.
    */
    bool did_lazy_init;
    bool agent_started;
    bool request_initialized;

    /**
     * Flag that indicates that RINIT ran successfully to completion with
     * all the corresponding data structures initialized.
     */
    bool is_cli_sapi;
    zend_bool cli_enabled;
    zend_bool cli_long_running;
    long first_config_timeout;
    bool first_request_done;
    char *log4cxx_config;
    char *php_agent_root;
    long time_update_period_us;
    long bt_info_response_timeout;
    long reporting_linger;
    char* proxy_output_filename;
    zend_bool auto_launch_proxy;
    InterceptionEngine* icept_engine;
    AgentKernel *kernel;
    char* proxy_ctrl_dir;
    char* proxyScript;

    zend_bool shared_timer;

    /**
        Flag that indicates whether the agent is disabled due
        to something in the configuration or environment.  This is
        a lower level style of disabled than the disabled state that might
        come from the proxy.
        If this flag is true, we will not even create the log file.
    */
    bool is_disabled;
    long config_request_interval;

    char* controller_host;
    long controller_port;
    zend_bool controller_ssl_enabled;
    char* application_name;
    char* tier_name;
    char* node_name;
    char* account_name;
    char* account_access_key;
    char* proxy_host;
    long proxy_port;
    char* proxy_user;
    char* proxy_password_file;

    zend_bool early_start_node;

    /*
    char* proxy_user;
    char* proxy_password_file;
    char* unique_host_id;
    */

    CXXGlobals *G;
ZEND_END_MODULE_GLOBALS(agent)
/* }}} */

ZEND_EXTERN_MODULE_GLOBALS(agent)

PHP_MINIT_FUNCTION(agent);
PHP_MSHUTDOWN_FUNCTION(agent);
PHP_RINIT_FUNCTION(agent);
PHP_RSHUTDOWN_FUNCTION(agent);
PHP_MINFO_FUNCTION(agent);

PHP_FUNCTION(appdynamics_start_transaction);
PHP_FUNCTION(appdynamics_continue_transaction);
PHP_FUNCTION(appdynamics_end_transaction);
#if 0
PHP_FUNCTION(appdynamics__get_handle_registry_size);
#endif

inline void CXXGlobals::create(TSRMLS_D)
{
    AG(G) = new CXXGlobals(TSRMLS_C);
}

inline void CXXGlobals::destroy(TSRMLS_D)
{
    delete AG(G);
    AG(G) = NULL;
}

inline CXXGlobals& CXXGlobals::get(TSRMLS_D)
{
    return *(AG(G));
}

static inline std::string getNameForOpArray(zend_op_array* opArray)
{
    const char* functionName = opArray->function_name ? ZSTR_VAL(opArray->function_name) : NULL;
    if (!functionName)
        return std::string("unknown");

    const char* className = opArray->scope && opArray->scope->name ? ZSTR_VAL(opArray->scope->name) : NULL;
    if (!className)
        return std::string(functionName);
    std::string methodName(className);
    methodName += "::";
    methodName += functionName;
    return methodName;
}

#endif /* __AGENT_H */

/*
 * vim: et sw=4 ts=4 fdm=marker:
 */
