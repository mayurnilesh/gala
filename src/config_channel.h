/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __CONFIG_CHANNEL_H
#define __CONFIG_CHANNEL_H

#include <stdint.h>
#include <string>

class ZMQControlTransport;

struct _zend_agent_globals;

namespace zmq
{
    class context_t;
}

class IServiceManager;
class IConfigListener;

class IConfigChannel {
    public:
        virtual ~IConfigChannel() {}

        virtual void requestUpdate() = 0;
        virtual void checkForUpdate(const struct _zend_agent_globals* agentGlobals, long timeoutInMilliseconds) = 0;
        virtual bool isAgentEnabled() = 0;
        virtual bool isInitialized() = 0;

        virtual void addListener(IConfigListener* listener) = 0;

        virtual void setRequestInterval(long interval) = 0;
        virtual long getRequestInterval() = 0;

        virtual int64_t getNodeID() const = 0;
        virtual int64_t getTierID() const = 0;
        virtual int64_t getAppID() const = 0;
        virtual const std::string& getControllerGUID() const = 0;
        virtual const std::string& getAccountGUID() const = 0;

        virtual void setTimestampSkew(int64_t timestampSkew) = 0;
        virtual int64_t getTimestampSkew() const = 0;
    protected:
        int64_t m_timestampSkew;
};

#endif
