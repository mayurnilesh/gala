/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "string_match.h"
#include "errors.h"
#include "transactions.h"
#include "agent.h"

bool StringMatch::matchString(const AgentLogger& logger,
                              const PHPExecEnvironment* phpExecEnv,
                              const std::string& input)
{
    MatchResult matchResult = ::matchString(phpExecEnv, m_protobufObj, input);
    if (matchResult == MatchResult::INVALID_CONDITION && !m_didLogError) {
        m_didLogError = true;
        LOG4CXX_ERROR(logger, "Invalid StringMatchCondition. The condition type required one or more match strings, but there were none.");
    }
    else if (matchResult == MatchResult::INVALID_REGEX && !m_didLogError) {
        m_didLogError = true;
        LOG4CXX_ERROR(logger, "Invalid regex in StringMatchCondition: " << m_protobufObj.matchstrings(0));
    }
    else if (matchResult == MatchResult::INVALID_REGEX_STATE && !m_didLogError) {
        m_didLogError = true;
        LOG4CXX_ERROR(logger, "Invalid regex state");
    }
    else if (matchResult == MatchResult::REGEX_ERROR && !m_didLogError) {
        m_didLogError = true;
        LOG4CXX_ERROR(logger, "Regex error: " << matchResult.getRegexErrorCode());
    }
    // if matchresult either invalid case
    // then: check m_didLogBadRegex,
    //  if not logged, log about it
    //  set flag to true
    return matchResult == MatchResult::MATCHED;
}

MatchResult matchString(const PHPExecEnvironment* phpExecEnv,
                        const appdynamics::pb::Common::StringMatchCondition& matchCondition,
                        const std::string& input)
{
    if (matchCondition.matchstrings_size() == 0)
    {
        if (matchCondition.type() != appdynamics::pb::Common::StringMatchCondition::IS_NOT_EMPTY
                && matchCondition.type() != appdynamics::pb::Common::StringMatchCondition::IS_IN_LIST)
            return MatchResult::INVALID_CONDITION;
        else
            return MatchResult::EMPTY_CONDITION;
    }

    using namespace appdynamics::pb::Common;
    bool match = true;

    if (matchCondition.matchstrings_size() == 1) {
        std::string matchstring = matchCondition.matchstrings(0);

        switch (matchCondition.type())
        {
            case StringMatchCondition::IS_IN_LIST:
            case StringMatchCondition::EQUALS:
                match = matchstring == input;
                break;
            case StringMatchCondition::STARTS_WITH:
                match = input.compare(0, matchstring.size(), matchstring) == 0;
                break;
            case StringMatchCondition::CONTAINS:
                match = input.find(matchstring) != std::string::npos;
                break;
            case StringMatchCondition::ENDS_WITH:
                if (input.size() > matchstring.size())
                    match = input.compare(input.size() - matchstring.size(), matchstring.size(), matchstring) == 0;
                else
                    match = false;
                break;
            case appdynamics::pb::Common::StringMatchCondition::MATCHES_REGEX: {
                MatchResult result = matchRegex(phpExecEnv, matchstring, input, NULL);
                switch (result) {
                    case MatchResult::MATCHED:
                        match = true;
                        break;
                    default:
                        match = false;
                        return result;
                }
                break;
            }
            case StringMatchCondition::IS_NOT_EMPTY:
                match = !input.empty();
                break;
            default:
                match = false;
                BOOST_ASSERT_MSG(false, "Unrecognized string match type");
                break;
        }
    }
    else {
        if (matchCondition.type() != StringMatchCondition::IS_IN_LIST) {
            return MatchResult::UNMATCHED;
        }
        match = false;
        const auto& matchstrings = matchCondition.matchstrings();
        for (auto it = matchstrings.begin(); it != matchstrings.end(); ++it) {
            if (input == *it)
            {
                match = true;
                break;
            }
        }
    }

    // xor operation equivalent to applying isnot() from protobuf.
    return match ^ matchCondition.isnot() ? MatchResult::MATCHED : MatchResult::UNMATCHED;
}

MatchResult matchRegex(const PHPExecEnvironment* phpExecEnv,
                       const std::string& pattern,
                       const std::string& input,
                       std::vector<std::string>* subPatterns)
{
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    // Completely squelch all errors about regex compilation.
    ErrorMonitor::SuppressionScope errorSuppression(errorMonitor, true);

    const auto& pcreGetCompiledRegEx =
        phpExecEnv->getAgentGlobals().delayed_functions.get_pcre_get_compiled_regex();
    const auto& pcreExec =
        phpExecEnv->getAgentGlobals().delayed_functions.get_pcre_exec();
    const auto& pcreFullInfo =
        phpExecEnv->getAgentGlobals().delayed_functions.get_pcre_fullinfo();
    const auto& pcreGetSubstringList =
        phpExecEnv->getAgentGlobals().delayed_functions.get_pcre_get_substring_list();

    if (!(pcreGetCompiledRegEx.isValid() && pcreExec.isValid() && pcreFullInfo.isValid()))
        return MatchResult::INVALID_REGEX_STATE;

    pcre* pcre = NULL;
    pcre_extra* pcreExtra = NULL;
    int pcreOptions = 0;

#if PHP_VERSION_ID >= 70000
    zend_string* patternStr = zend_string_init(pattern.c_str(), pattern.length(), false);
#else
    char* patternStr = const_cast<char*>(pattern.c_str());
#endif

    pcre = phpExecEnv->callWithTSRMContext(pcreGetCompiledRegEx,
                                           patternStr,
                                           &pcreExtra,
                                           &pcreOptions);

#if PHP_VERSION_ID >= 70000
    zend_string_release(patternStr);
#endif

    if (!pcre)
        return MatchResult::INVALID_REGEX;

    int numOffsets = 0;
    int* offsets = NULL;

    if (subPatterns) {
        int numGroups = 1; // we always have the full match
        int rc = pcreFullInfo(pcre, pcreExtra, PCRE_INFO_CAPTURECOUNT, reinterpret_cast<void*>(&numGroups));
        if (rc < 0)
            return MatchResult::INVALID_REGEX_STATE;
        numGroups++; // account for the full match
        numOffsets = numGroups * 3;
        offsets = new int[numOffsets](); // will be initialized to 0
    }

    int result = pcreExec(pcre, pcreExtra, input.c_str(), input.size(), 0, 0, offsets, numOffsets);
    if (result < -1) {
        return MatchResult(MatchResult::REGEX_ERROR, result);
    }

    if (subPatterns) {
        const char** stringList;
        int rc = pcreGetSubstringList(input.c_str(), offsets, result, &stringList);
        if (rc < 0) {
            delete [] offsets;
            return MatchResult::INVALID_REGEX_STATE;
        }

        for (int i = 0; i < result; i++) {
            if (offsets[i<<1] != -1)
                subPatterns->push_back(std::string(stringList[i], offsets[(i<<1)+1] - offsets[i<<1]));
        }
        free(stringList);
        delete [] offsets;
    }

    return (result >= 0) ? MatchResult::MATCHED : MatchResult::UNMATCHED;
}

