/*
   Copyright 2015 AppDynamics.
   All rights reserved.
 */
#ifndef __zend_compatibility_h
#define __zend_compatibility_h

#if PHP_VERSION_ID >= 70000

# if PHP_VERSION_ID >= 70100
   // zval_dtor_func() is equivalent to zval_dtor_func_for_ptr() since PHP 7.0
   // and the latter got removed entirely in PHP 7.1
#  define zval_dtor_func_for_ptr zval_dtor_func
# endif

# define Z_BVAL_P(zvp) Z_TYPE_P(zvp) == IS_TRUE ? true : false
  typedef uint32_t zend_object_handle;
  typedef zend_object_dtor_obj_t appd_obj_dtor_t;
# define IS_BOOL 1000 // IS_BOOL does not exist in PHP 7. we have it to stay compatible
# define APPD_MAKE_STD_ZVAL(zvp) zvp = (zval*) ecalloc(sizeof(zval), 1);
# define ALLOC_INIT_ZVAL(zvp) APPD_MAKE_STD_ZVAL(zvp)
# define APPD_MAKE_COPY_ZVAL(clone, orig) ZVAL_DUP(clone, orig)
# define APPD_ZVAL_PTR_DTOR(zvpp) zval_ptr_dtor(*zvpp)
# define APPD_ZVAL_STRING_DUP_TRUE(result, str) ZVAL_STRING(result, str)
# define APPD_ZVAL_STRING_DUP_FALSE(result, str) APPD_ZVAL_STRING_DUP_TRUE(result, str)
# define APPD_ZVAL_STRINGL_DUP_TRUE(result, str, len) ZVAL_STRINGL(result, str, len)
# define APPD_ZVAL_STRINGL_DUP_FALSE(result, str, len) APPD_ZVAL_STRINGL_DUP_TRUE(result, str, len)
# define APPD_DUP_TRUE
# define APPD_DUP_FALSE
# define APPD_EXECDATA_ZEND_FUNC(ed) ed->func
# define APPD_GET_CE(object) Z_OBJ_P(object)->ce
# define APPD_OBJ_DTOR_P const_cast<zend_object_handlers*>(object->handlers)->dtor_obj
# define EG_RET_VAL_PTR_PTR &(EG(current_execute_data)->return_value)
# define Z_OBJ_CLASS_NAME_P(zval) ((zval) && Z_TYPE_P(zval) == IS_OBJECT && \
    Z_OBJ_P(zval)->ce != NULL && Z_OBJ_P(zval)->ce->name != NULL ? \
    ZSTR_VAL(Z_OBJ_P(zval)->ce->name) : "")
# define ZEND_OPCODE_HANDLER_ARGS zend_execute_data *execute_data
# define ZEND_OPCODE_HANDLER_ARGS_PASSTHRU execute_data
# define HASH_KEY_NON_EXISTANT HASH_KEY_NON_EXISTENT

# define appd_zend_str_t zend_string
// return the internal zend_string* for an 'IS_STRING' zval, incrementing the string's refcount.
// always remember to call `APPD_ZSTR_FREE` when you are finished with the string.
# define APPD_ZSTR_P(zvp) zend_string_copy(Z_STR_P(zvp))
// return a pointer to the internal cstring.
# define APPD_ZSTR_INTERNAL(zstr_p) ZSTR_VAL(zstr_p)
// decrement the string's refcount, and deallocate memory if necessary.
# define APPD_ZSTR_FREE(zstr_p) zend_string_release(zstr_p)

#else

# define APPD_MAKE_STD_ZVAL(zvp) MAKE_STD_ZVAL(zvp)
# define Z_REFCOUNTED_P(dummy) 1
  typedef long zend_long;
  typedef zend_objects_store_dtor_t appd_obj_dtor_t;
# define APPD_MAKE_COPY_ZVAL(clone, orig) MAKE_COPY_ZVAL(&orig, clone)
# define APPD_ZVAL_PTR_DTOR(zvpp) zval_ptr_dtor(zvpp)
# define APPD_ZVAL_STRING_DUP_TRUE(result, str) ZVAL_STRING(result, str, 1)
# define APPD_ZVAL_STRING_DUP_FALSE(result, str) ZVAL_STRING(result, str, 0)
# define APPD_ZVAL_STRINGL_DUP_TRUE(result, str, len) ZVAL_STRINGL(result, str, len, 1)
# define APPD_ZVAL_STRINGL_DUP_FALSE(result, str, len) ZVAL_STRINGL(result, str, len, 0)
# define APPD_DUP_TRUE , 1
# define APPD_DUP_FALSE , 0
# define zend_is_auto_global_str zend_is_auto_global
# define ZSTR_VAL(zstr) zstr
# define ZSTR_LEN(zstr) zstr##_length
# define APPD_EXECDATA_ZEND_FUNC(ed) ed->function_state.function
# define APPD_GET_CE(object) zend_get_class_entry(object TSRMLS_CC)
# define APPD_OBJ_DTOR_P bucket->bucket.obj.dtor
# define EG_RET_VAL_PTR_PTR EG(return_value_ptr_ptr)

# define appd_zend_str_t const char
// return the internal char* for an 'IS_STRING' zval.
// unlike the equivalent on php7, we _do not_ need to `APPD_ZSTR_FREE` after using this macro.
# define APPD_ZSTR_P(zvp) Z_STRVAL_P(zvp)
// return a pointer to the internal cstring.
# define APPD_ZSTR_INTERNAL(char_p) const_cast<char*>(char_p)
// deallocate memory assigned for this string.
# define APPD_ZSTR_FREE(char_p) efree(APPD_ZSTR_INTERNAL(char_p))

#endif

#endif /* __zend_compatibility_h */
