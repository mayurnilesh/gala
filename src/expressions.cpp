/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#include "expressions.h"
#include "errors.h"
#include "string_match.h"
#include "transactions.h"
#include <boost/algorithm/string/iter_find.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/bind.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/foreach.hpp>

namespace Expression
{

using namespace appdynamics::pb;


static inline bool isValueNode(int32_t nodeType)
{
    return (nodeType >= Instrumentation::ExpressionNode::ENTITY);
}

static inline bool isNumber(ZValPointer::ZValType type)
{
    return (type == ZValPointer::ZVal_Long || type == ZValPointer::ZVal_Double);
}

static inline ZValPointerString nodeToString(const Instrumentation::ExpressionNode& node)
{
    BOOST_ASSERT(node.type() == Instrumentation::ExpressionNode::STRING);
    LOG4CXX_TRACE(getLogger(), "evaluating STRING node (" << node.stringvalue() << " )");

    return ZValPointerString::copy(node.stringvalue());
}

static inline ZValPointerLong nodeToInteger(const Instrumentation::ExpressionNode& node)
{
    BOOST_ASSERT(node.type() == Instrumentation::ExpressionNode::INTEGER);
    LOG4CXX_TRACE(getLogger(), "evaluating INTEGER node ( " << node.integervalue() << " )");

    return ZValPointerLong::create(node.integervalue());
}

static inline ZValPointerAny nodeToEntity(const Instrumentation::ExpressionNode& node,
                                          const Context& context)
{
    BOOST_ASSERT(node.type() == Instrumentation::ExpressionNode::ENTITY);


    const Instrumentation::EntityCollector& entityValue = node.entityvalue();
    const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();

    if (getLogger()->isTraceEnabled()) {
        std::string entityValueString(boost::replace_all_copy(entityValue.DebugString(), "\n", " "));
        LOG4CXX_TRACE(getLogger(), "evaluating ENTITY node ( " << entityValueString << " )");
    }
    switch (entityValue.type()) {
        case Instrumentation::EntityCollector::INVOKED_OBJECT:
        {
            zval* object = execEnv->getInvokedObject();
            if (object)
                return ZValPointerAny::share(object);
            break;
        }

        case Instrumentation::EntityCollector::RETURN_VALUE:
        {
            zval* returnValue = execEnv->getReturnValue(true);
            if (returnValue)
                return ZValPointerAny::share(returnValue);
            break;
        }

        case Instrumentation::EntityCollector::PARAMETER:
        {
            BOOST_ASSERT(entityValue.has_parameterindex());
            if (entityValue.has_parameterindex()) {
                return execEnv->getParam<ZValPointerAny, true>(entityValue.parameterindex());
            }
            break;
        }

        case Instrumentation::EntityCollector::IDENTIFYING_PROPERTY:
        {
            BOOST_ASSERT(entityValue.has_propertyname());
            if (entityValue.has_propertyname()) {
                const StringMap* properties = context.getIdentifyingProperties();
                BOOST_ASSERT(properties != NULL);
                if (properties) {
                    auto it = properties->find(entityValue.propertyname());
                    if (it != properties->end()) {
                        return ZValPointerString::copy(it->second);
                    }
                }
            }
            break;
        }

        default:
            BOOST_ASSERT_MSG(false, "Unrecognized entity value type");
            break;
    }

    return ZValPointerAny();
}

//
// The equality comparison operator implements a subset of Abstract Equality
// Comparison Algorithm from the ECMA spec. It is expanded to include
// PHP-speficic types such as resources and arrays.
//
// http://www.ecma-international.org/ecma-262/5.1/#sec-11.9.3
//

static inline ZValPointerAny objectToPrimitive(const ZValPointerObject& object,
                                               const Context& context)
{
    ZValPointerAny value;
    const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();
    if (object.getPrimitiveValue(execEnv, &value))
        return value;
    return object.convertToStringAsCopy(execEnv);
}

static inline bool isStringEqualToNumber(const ZValPointerString& str, const ZValPointerAny& num)
{
    double stringValue = str.getDoubleValue();

    switch (num.getType()) {
        case ZValPointer::ZVal_Long:
            return (stringValue == (double)num.cast<ZValPointerLong>().getLongValue());

        case ZValPointer::ZVal_Double:
            return (stringValue == num.cast<ZValPointerDouble>().getDoubleValue());

        default:
            return false;
    }

}

static bool isEqual(const ZValPointerAny& lhs,
                    const ZValPointerAny& rhs,
                    const Context& context)
{
    ZValPointer::ZValType leftType = lhs.getType();
    ZValPointer::ZValType rightType = rhs.getType();

    if (leftType == rightType) {

        // same types

        switch (leftType) {
            case ZValPointer::ZVal_Null:
                return true;

            case ZValPointer::ZVal_Long:
                return lhs.cast<ZValPointerLong>().getLongValue() ==
                       rhs.cast<ZValPointerLong>().getLongValue();

            case ZValPointer::ZVal_Double:
                return lhs.cast<ZValPointerDouble>().getDoubleValue() ==
                       rhs.cast<ZValPointerDouble>().getDoubleValue();

            case ZValPointer::ZVal_Bool:
                return lhs.cast<ZValPointerBool>().getBoolValue() ==
                       rhs.cast<ZValPointerBool>().getBoolValue();

            case ZValPointer::ZVal_String:
                return lhs.cast<ZValPointerString>().getStringValue().
                         compare(rhs.cast<ZValPointerString>().getStringValue()) == 0;

            case ZValPointer::ZVal_Resource:
                // In PHP 5, resource IDs are unique, so they can be used for identity
                // In PHP 7, getResource() returns a void* to the resource.
                return lhs.cast<ZValPointerResource>().getResource() ==
                       rhs.cast<ZValPointerResource>().getResource();

            case ZValPointer::ZVal_Array:
                // Assume that HashTable pointer equality is the same as identity
                return lhs.cast<ZValPointerArray>().getInternalHashTable() ==
                       rhs.cast<ZValPointerArray>().getInternalHashTable();

            case ZValPointer::ZVal_Object:
                // Object IDs are unique, so can be used for identity
                return lhs.cast<ZValPointerObject>().getObjectHandle() ==
                       rhs.cast<ZValPointerObject>().getObjectHandle();

            default:
                return false;
        }

    } else {

        // different types

        const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();

        if (leftType == ZValPointer::ZVal_Null || rightType == ZValPointer::ZVal_Null)
            return false;

        if (leftType == ZValPointer::ZVal_Long && rightType == ZValPointer::ZVal_Double)
            return ((double)lhs.cast<ZValPointerLong>().getLongValue()) ==
                            rhs.cast<ZValPointerDouble>().getDoubleValue();

        if (leftType == ZValPointer::ZVal_Double && rightType == ZValPointer::ZVal_Long)
            return ((double)rhs.cast<ZValPointerLong>().getLongValue()) ==
                            lhs.cast<ZValPointerDouble>().getDoubleValue();

        if (isNumber(leftType) && rightType == ZValPointer::ZVal_String)
            return isStringEqualToNumber(rhs.cast<ZValPointerString>(), lhs);

        if (isNumber(rightType) && leftType == ZValPointer::ZVal_String)
            return isStringEqualToNumber(lhs.cast<ZValPointerString>(), rhs);

        if (leftType == ZValPointer::ZVal_Bool)
            return isEqual(ZValPointerLong::create(lhs.cast<ZValPointerBool>().getBoolValue()), rhs, context);

        if (rightType == ZValPointer::ZVal_Bool)
            return isEqual(lhs, ZValPointerLong::create(rhs.cast<ZValPointerBool>().getBoolValue()), context);

        if ((leftType == ZValPointer::ZVal_String || isNumber(leftType))
                && rightType == ZValPointer::ZVal_Object)
            return isEqual(lhs, objectToPrimitive(rhs.cast<ZValPointerObject>(), context), context);

        if (leftType == ZValPointer::ZVal_Object &&
                (rightType == ZValPointer::ZVal_String || isNumber(rightType)))
            return isEqual(objectToPrimitive(lhs.cast<ZValPointerObject>(), context), rhs, context);

        //
        // Returning false for arrays and resources will cause inconsistent
        // behavior with NOT_EQUALS implementation, which re-uses this function
        // with negation. That's okay though, since we don't really care about
        // arrays or resources.
        //

        return false;
    }

}

static bool isStrictlyLessThan(const ZValPointerAny& lhs,
                               const ZValPointerAny& rhs,
                               const Context& context)
{
    ZValPointer::ZValType leftType = lhs.getType();
    ZValPointer::ZValType rightType = rhs.getType();

    if (leftType == ZValPointer::ZVal_Long && rightType == ZValPointer::ZVal_Long)
        return lhs.cast<ZValPointerLong>().getLongValue() < rhs.cast<ZValPointerLong>().getLongValue();

    //
    // This will cause inconsistent behavior with LE/GT/GE implementations,
    // since they re-use this function with different parameter order. That's
    // okay though, since we don't really care about arrays or resources.
    //
    if (leftType == ZValPointer::ZVal_Array || rightType == ZValPointer::ZVal_Array ||
        leftType == ZValPointer::ZVal_Resource || rightType == ZValPointer::ZVal_Resource)
        return false;

    ZValPointerAny leftPrim(leftType == ZValPointer::ZVal_Object ?
                                objectToPrimitive(lhs.cast<ZValPointerObject>(), context) : lhs);
    ZValPointerAny rightPrim(rightType == ZValPointer::ZVal_Object ?
                                objectToPrimitive(rhs.cast<ZValPointerObject>(), context) : rhs);

    if (leftPrim.getType() == ZValPointer::ZVal_String && rightPrim.getType() == ZValPointer::ZVal_String)
        return leftPrim.cast<ZValPointerString>().getStringValue().
                 compare(rightPrim.cast<ZValPointerString>().getStringValue()) < 0;

    double leftValue = getDoubleValue(leftPrim);
    double rightValue = getDoubleValue(rightPrim);

    return leftValue < rightValue;
}

static inline bool opEquals(const Instrumentation::ExpressionNode& lhsNode,
                            const Instrumentation::ExpressionNode& rhsNode,
                            const Context& context)
{
    ZValPointerAny lhs(evaluate(lhsNode, context));
    ZValPointerAny rhs(evaluate(rhsNode, context));

    return isEqual(lhs, rhs, context);
}

static inline bool opStrictlyLessThan(const Instrumentation::ExpressionNode& lhsNode,
                                      const Instrumentation::ExpressionNode& rhsNode,
                                      const Context& context)
{
    ZValPointerAny lhs(evaluate(lhsNode, context));
    ZValPointerAny rhs(evaluate(rhsNode, context));

    return isStrictlyLessThan(lhs, rhs, context);
}

static inline bool opNot(const Instrumentation::Operator::Not& notNode,
                         const Context& context)
{
    ZValPointerAny operand(evaluate(notNode.operand(), context));
    return getBooleanValue(operand);
}

static inline bool opAnd(const Instrumentation::Operator::And& andNode,
                         const Context& context)
{
    BOOST_FOREACH(const auto& node, andNode.operands()) {
        ZValPointerAny operand(evaluate(node, context));
        if (!getBooleanValue(operand))
            return false;
    }
    return true;
}

static inline bool opOr(const Instrumentation::Operator::Or& orNode,
                        const Context& context)
{
    BOOST_FOREACH(const auto& node, orNode.operands()) {
        ZValPointerAny operand(evaluate(node, context));
        if (getBooleanValue(operand))
            return true;
    }
    return false;
}

static inline ZValPointerAny opGetter(const Instrumentation::Operator::Getter& getterNode,
                                      const Context& context)
{
    ZValPointerAny base(evaluate(getterNode.base(), context));

    if (base.getType() != ZValPointer::ZVal_Object) {
        LOG4CXX_INFO(getLogger(), "GETTER base is not an object");
        return ZValPointerAny();
    }

    const std::string& fieldName = getterNode.field();
    bool doMethodCall = false;
    const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();
    std::string lcFieldName = zend_str_tolower(fieldName);
    if (execEnv->lookupMethod(base.cast<ZValPointerObject>(),
                              lcFieldName.c_str(),
                              lcFieldName.size())) {
      doMethodCall = true;
    }

    ZValPointerAny result;
    if (doMethodCall) {
      std::vector<ZValPointerAny> params;
      base.cast<ZValPointerObject>()
          .callInstanceMethod(execEnv,
                              fieldName.c_str(), fieldName.size(), params,
                              &result);
    } else {
      bool found = base.cast<ZValPointerObject>()
                       .readPropertyByName(execEnv,
                                           fieldName.c_str(), fieldName.size(), &result);
      if (!found) {
          LOG4CXX_INFO(getLogger(), "property " << fieldName << " not found");
      }
    }

    return result;
}

static inline bool opStringMatch(const Instrumentation::Operator::StringMatch& stringMatchNode,
                                           const Context& context)
{
    const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();
    ZValPointerAny input(evaluate(stringMatchNode.input(), context));
    if (input.getType() != ZValPointer::ZVal_String) {
        input = input.convertToStringAsCopy(execEnv);
    }
    StringMatch condition(stringMatchNode.condition());
    return condition.matchString(getLogger(), execEnv, input.cast<ZValPointerString>().getStringValue());
}

static std::string opMerge(const Instrumentation::Operator::Merge& mergeNode,
                           const Context& context)
{
    std::vector<std::string> pieces;
    ZValPointerArray array(evaluate(mergeNode.inputarray(), context).cast<ZValPointerArray>());

    if (!array)
        return std::string();

    const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();
    for(ZValPointerArray::const_iterator it = array.begin(); it != array.end(); ++it) {
        ZValPointerAny piece((*it).getValue());
        if (piece.getType() != ZValPointer::ZVal_String) {
            piece = piece.convertToStringAsCopy(execEnv);
        }
        pieces.push_back(piece.cast<ZValPointerString>().getStringValue());
    }
    return boost::algorithm::join(pieces, mergeNode.delimiter());
}

static inline void makeBitsetFromArray(const google::protobuf::RepeatedField<uint32>& segments,
                                       boost::dynamic_bitset<>* result)
{
    BOOST_FOREACH(auto segmentNumber, segments) {
        if (segmentNumber == 0)
            continue;
        --segmentNumber;
        if (result->size() <= segmentNumber)
            result->resize(segmentNumber + 1, false);
        result->set(segmentNumber, true);
    }
}

static ZValPointerArray opSplit(const Instrumentation::Operator::Split& splitNode,
                                const Context& context)
{
    std::vector<std::string> pieces;

    ZValPointerAny input(evaluate(splitNode.input(), context));
    if (input.getType() != ZValPointer::ZVal_String) {
        input = input.convertToStringAsCopy(context.getPHPExecEnvironment());
    }
    std::string str(input.cast<ZValPointerString>().getStringValue());

    // This could be optimized for first/last N segments case to use iter_find
    // and stop iteration once the required number of segments were found
    boost::iter_split(pieces, str, boost::first_finder(splitNode.delimiter()));

    // Remove empty pieces
    pieces.erase(std::remove_if(pieces.begin(), pieces.end(),
                                boost::bind(std::equal_to<std::string>(), _1, "")),
                 pieces.end());

    ZValPointerArray result(ZValPointerArray::create());

    if (splitNode.has_segments()) {
        const Instrumentation::Segments& segments = splitNode.segments();
        switch (segments.type()) {
            case Instrumentation::Segments::FIRST:
                if (segments.has_numsegments()) {
                    uint32_t nSegments = std::min(static_cast<size_t>(segments.numsegments()),
                                                  pieces.size());
                    BOOST_FOREACH(auto s,
                                  std::make_pair(pieces.begin(), pieces.begin() + nSegments)) {
                        result.pushEntry(ZValPointerString::copy(s));
                    }
                }
                break;
            case Instrumentation::Segments::LAST:
                if (segments.has_numsegments()) {
                    uint32_t nSegments = std::min(static_cast<size_t>(segments.numsegments()),
                                           pieces.size());
                    BOOST_FOREACH(auto s,
                           std::make_pair(pieces.end() - nSegments, pieces.end())) {
                        result.pushEntry(ZValPointerString::copy(s));
                    }
                }

            case Instrumentation::Segments::SELECTED:
                if (segments.selectedsegments_size() > 0) {
                    size_t numSegments = pieces.size();
                    BOOST_FOREACH(auto segment, segments.selectedsegments()) {
                    if (segment > 0 && segment <= numSegments)
                        result.pushEntry(ZValPointerString::copy(pieces[segment-1]));
                }
            }

            default:
                LOG4CXX_ERROR(getLogger(), "Invalid segment type: " << segments.type());
                BOOST_ASSERT_MSG(false, "Invalid segment type");
                break;
        }
    } else {
        BOOST_ASSERT_MSG(false, "Invalid input");
    }

    return result;
}

ZValPointerArray opRegexCapture(const Instrumentation::Operator::RegexCapture& regexCaptureNode,
                                const Context& context)
{
    const PHPExecEnvironment* execEnv = context.getPHPExecEnvironment();
    ZValPointerAny input(evaluate(regexCaptureNode.input(), context));
    if (input.getType() != ZValPointer::ZVal_String) {
        input = input.convertToStringAsCopy(execEnv);
    }
    const std::string& pattern(regexCaptureNode.regex());

    std::vector<std::string> subGroups;
    MatchResult result = matchRegex(execEnv,
                                    regexCaptureNode.regex(),
                                    input.cast<ZValPointerString>().getStringValue(),
                                    &subGroups);
    switch (result) {
        case MatchResult::MATCHED: {
            ZValPointerArray result(ZValPointerArray::create());

            if (regexCaptureNode.regexgroups_size() > 0) {
                size_t numGroups = subGroups.size();
                BOOST_FOREACH(auto group, regexCaptureNode.regexgroups()) {
                    if (group < numGroups)
                        result.pushEntry(ZValPointerString::copy(subGroups[group]));
                }
            } else {
                result.pushEntry(ZValPointerString::copy(subGroups[0]));
            }
            return result;
        }

        case MatchResult::UNMATCHED:
            break;

        case MatchResult::INVALID_REGEX:
            LOG4CXX_ERROR(getLogger(), "Invalid regex pattern: " << regexCaptureNode.regex());
            break;

        case MatchResult::INVALID_REGEX_STATE:
            LOG4CXX_ERROR(getLogger(), "Invalid regex state");
            break;

        case MatchResult::REGEX_ERROR:
            LOG4CXX_ERROR(getLogger(), "Regex error: " << result.getRegexErrorCode());
            break;
    }

    return ZValPointerAny().cast<ZValPointerArray>();
}


static ZValPointerAny evaluateOpNode(const Instrumentation::ExpressionNode& node,
                                     const Context& context)
{
    ZValPointerAny result;

    switch (node.type()) {
        case Instrumentation::ExpressionNode::EQUALS:
            LOG4CXX_TRACE(getLogger(), "evaluating EQUALS (");
            result = ZValPointerBool::create(opEquals(node.comparisonop().lhs(),
                                                      node.comparisonop().rhs(),
                                                      context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::NOT_EQUALS:
            LOG4CXX_TRACE(getLogger(), "evaluating NOT_EQUALS (");
            result = ZValPointerBool::create(!opEquals(node.comparisonop().lhs(),
                                                       node.comparisonop().rhs(),
                                                       context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::LT:
            LOG4CXX_TRACE(getLogger(), "evaluating LT (");
            result = ZValPointerBool::create(opStrictlyLessThan(node.comparisonop().lhs(),
                                                                node.comparisonop().rhs(),
                                                                context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::GT:
            LOG4CXX_TRACE(getLogger(), "evaluating GT (");
            result = ZValPointerBool::create(opStrictlyLessThan(node.comparisonop().rhs(),
                                                                node.comparisonop().lhs(),
                                                                context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::LE:
            LOG4CXX_TRACE(getLogger(), "evaluating LE (");
            result = ZValPointerBool::create(!opStrictlyLessThan(node.comparisonop().rhs(),
                                                                 node.comparisonop().lhs(),
                                                                 context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::GE:
            LOG4CXX_TRACE(getLogger(), "evaluating GE (");
            result = ZValPointerBool::create(!opStrictlyLessThan(node.comparisonop().lhs(),
                                                                 node.comparisonop().rhs(),
                                                                 context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::NOT:
            LOG4CXX_TRACE(getLogger(), "evaluating NOT (");
            result = ZValPointerBool::create(opNot(node.notop(), context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::AND:
            LOG4CXX_TRACE(getLogger(), "evaluating AND (");
            result = ZValPointerBool::create(opAnd(node.andop(), context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::OR:
            LOG4CXX_TRACE(getLogger(), "evaluating OR (");
            result = ZValPointerBool::create(opOr(node.orop(), context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::GETTER:
            LOG4CXX_TRACE(getLogger(), "evaluating GETTER (");
            result = ZValPointerAny(opGetter(node.getterop(), context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::STRINGMATCH:
            LOG4CXX_TRACE(getLogger(), "evaluating STRINGMATCH (");
            result = ZValPointerBool::create(opStringMatch(node.stringmatchop(), context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::MERGE:
            LOG4CXX_TRACE(getLogger(), "evaluating MERGE (");
            result = ZValPointerString::copy(opMerge(node.mergeop(), context));
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::SPLIT:
            LOG4CXX_TRACE(getLogger(), "evaluating SPLIT (");
            result = opSplit(node.splitop(), context);
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        case Instrumentation::ExpressionNode::REGEXCAPTURE:
            LOG4CXX_TRACE(getLogger(), "evaluating REGEXCAPTURE (");
            result = opRegexCapture(node.regexcaptureop(), context);
            LOG4CXX_TRACE(getLogger(), ")");
            break;

        default:
            LOG4CXX_INFO(getLogger(), "unknown operator node type: " << node.type());
            break;
    }

    return result;
}

ZValPointerAny evaluate(const Instrumentation::ExpressionNode& node,
                        const Context& context)
{
    if (isValueNode(node.type())) {
        switch (node.type()) {
            case Instrumentation::ExpressionNode::ENTITY:
                return nodeToEntity(node, context);

            case Instrumentation::ExpressionNode::STRING:
                return nodeToString(node);

            case Instrumentation::ExpressionNode::INTEGER:
                return nodeToInteger(node);

            default:
                LOG4CXX_INFO(getLogger(), "unknown value node type: " << node.type());
                break;
        }
    } else {
        return evaluateOpNode(node, context);
    }

    return ZValPointerAny();
}

}
