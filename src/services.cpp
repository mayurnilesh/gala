/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "agent.h"
#include "eum/config.h"
#include "infopoints.h"
#include "method_gatherer.h"
#include "services.h"
#include "snapshot.h"
#include "transactions.h"
#include "zmqtransport.h"

// The follow include needs to use <> instead of "" to make sure
// the earlier include path entries
// will have precedence over the directory containing this file.
#include <agent_version.h>
#include <boost/filesystem.hpp>

const Timer* AgentKernel::s_timer = NULL;

AgentIdentity::AgentIdentity(const std::string& application,
                             const std::string& tier,
                             const std::string& node,
                             const std::string& controllerHost,
                             const std::string& accountName)
    : m_application(application)
    , m_tier(tier)
    , m_node(node)
    , m_controllerHost(controllerHost)
    , m_accountName(accountName)
{
    size_t seed = 0;
    boost::hash_combine(seed, m_application);
    boost::hash_combine(seed, m_tier);
    boost::hash_combine(seed, m_node);
    boost::hash_combine(seed, m_controllerHost);
    boost::hash_combine(seed, m_accountName);
    m_hashCode = seed;
}

AgentKernel::AgentKernel(const char* zmqCtrlDir, const std::string& logsDir, const Timer* timer)
    : m_zmqCtrlDir(zmqCtrlDir)
    , m_logsDir(logsDir)
{
    m_logger = getLogger(std::string(LogContext::AGENT) + ".AgentKernel");
    LOG4CXX_INFO(m_logger, "starting up agent kernel [" << AGENT_VERSION << "]");

    s_timer = timer;
    m_zmqContext.reset(new zmq::context_t(1));

    size_t seed = 0;
    boost::hash_combine(seed, getpid());
    boost::hash_combine(seed, getTimer()->getTimestamp());
    m_randomNumberGenerator.seed(seed);
}

AgentKernel::~AgentKernel()
{
    LOG4CXX_DEBUG(m_logger, "shutting down agent kernel");
}

void AgentKernel::onRequestBegin(const AgentIdentity& identity)
{
    boost::shared_ptr<AgentContext> context;
    auto it = m_agentContexts.find(identity);
    if (it == m_agentContexts.end()) {
        context = boost::make_shared<AgentContext>(identity, this);
        m_agentContexts.insert(std::make_pair(identity, context));
    } else {
        context = it->second;
    }

    m_requestAgentContext = context;
    m_requestAgentContext->onRequestBegin();
}

void AgentKernel::onRequestEnd()
{
    m_requestAgentContext->onRequestEnd();
}

AgentContext::AgentContext(const AgentIdentity& agentIdentity,
                           AgentKernel* kernel)
   : m_agentIdentity(agentIdentity)
   , m_kernel(kernel)
{
    // Create control transport and use it to retrieve the data communication directory for this tenant.
    m_controlTransport = boost::make_shared<ZMQControlTransport>(m_kernel->getZMQContext(),
                                                                 m_kernel->getZMQControlDir(),
                                                                 ZMQTransportBase::getBaseSocketNumber(),
                                                                 m_kernel->getLogsDir());
    m_configChannel = boost::make_shared<ConfigChannel>(m_controlTransport);
    m_configChannel->setRequestInterval(AG(config_request_interval));
    m_snapshotManager = boost::make_shared<SnapshotManager>(this);
    m_transactionMonitor = boost::make_shared<TransactionMonitor>(this);
    m_eumConfig = boost::make_shared<EUM::Config>(AG(icept_engine), m_configChannel);
    m_infoPointConfig = boost::make_shared<InfoPoint::ConfigManager>(AG(icept_engine), m_configChannel);
    m_methodDataConfig = boost::make_shared<MethodData::ConfigManager>(AG(icept_engine), m_configChannel);
}

void AgentContext::onRequestBegin()
{
    m_infoPointConfig->activateProbes(true);
    m_methodDataConfig->activateProbes(true);
}

void AgentContext::onRequestEnd()
{
    m_infoPointConfig->activateProbes(false);
    m_methodDataConfig->activateProbes(false);
    m_transactionMonitor->getErrorMonitor()->onRequestEnd();
    m_bailoutHandlers.clear();
}

void AgentContext::enable()
{
    m_transactionMonitor->enable();
    m_snapshotManager->enable();
    m_infoPointConfig->activateProbes(true);
    m_methodDataConfig->activateProbes(true);
}

void AgentContext::disable()
{
    m_transactionMonitor->disable();
    m_snapshotManager->disable();
    m_infoPointConfig->activateProbes(false);
    m_methodDataConfig->activateProbes(false);
}

void AgentContext::afterBailout()
{
    BOOST_FOREACH(IBailoutHandler* handler, m_bailoutHandlers)
    {
        handler->afterBailout();
    }
}
