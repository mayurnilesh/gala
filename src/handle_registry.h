/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */

#ifndef __handle_registry_h
#define __handle_registry_h

#include <boost/functional/hash.hpp>
#include <zend.h>
#include <zend_list.h>
#include <zend_objects_API.h>

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/static_assert.hpp>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>

class PHPExecEnvironment;

#include "common.h"
#include "zend_compatibility.h"

class ZValPointerObject;
class ZValPointerResource;
class ZValPointerString;
class ZValPointerAny;

#if PHP_VERSION_ID >= 70000
typedef zend_object* t_HandleRegObjType;
typedef zend_resource* t_HandleRegRsrcType;
#else
typedef zend_object_handle t_HandleRegObjType;
typedef zend_rsrc_list_entry* t_HandleRegRsrcType;
#endif
/**
    Abstract base class for classes that
    contain information associated with registered
    handles.

    This class has a pointer to a constant instance ( called m_typeTag ) of an
    empty class that is used as a simple RTTI mechanism.

    Sub-classes of this class should either be "final" ( ie. not allow sub-classes )
    or be "abstract" ( not allow creation of instances ).

    Non-abstract descendent classes of this class must define a static const member
    of type AHandleInfo::TypeTag and pass a pointer to that member to
    this class' constructor.
 */
class AHandleInfo : boost::noncopyable
{
public:
    class TypeTag : boost::noncopyable
    {
        public:
            inline TypeTag() {}
    };

    virtual ~AHandleInfo();

    inline const TypeTag* getTypeTag() const { return m_typeTag; }

protected:
    inline AHandleInfo(const TypeTag* typeTag)
        : m_typeTag(typeTag)
    {
        BOOST_ASSERT(typeTag != NULL);
    }

    const TypeTag* const m_typeTag;
};

/**
    Registry of handles that allows interceptors to associate
    information with open handles or resources.  For example, this registry
    can be used to associate a URL with a resource returned from fopen.

    <p>
    This class has separate overloaded methods for object vs. resources.  It
    would be possible to have one set of methods that operate on ZPointerAny's
    but chances are interceptors will be down casting from ZPointerAny to either
    ZPointerObject and ZPointerResource for other reasons before interacting
    with this registry, so it seemed natural to me to have two sets of
    methods ( on for objects and another for resources ).

    <p>
    Since object handles ( and maybe resource handles? ) can be reused during
    a php program, we need to make sure that clients always get an instance
    of a AHandleInfo subclass that they are expecting.   This classes
    has its own simple RTTI strategy.
 */
class HandleRegistry : boost::noncopyable
{
private:

    struct Key;

    enum KeyType
    {
        KeyType_Object,
        KeyType_Resource,
        KeyType_String,

        // --------------
        KeyType_LAST
    };

    struct Key
    {
        /*
         * Define explicit constructors and destructor due to anonymous
         * union member.
         */
        Key(t_HandleRegObjType o)
            : type(KeyType_Object)
            , object(o) { }
        Key(t_HandleRegRsrcType r)
            : type(KeyType_Resource)
            , resource(r) { }
        Key(const std::string& handle)
            : type(KeyType_String)
        {
            new (stringHandleBytes) std::string(handle);
        }
        ~Key()
        {
            if (type == KeyType_String) {
                getStringHandle().std::basic_string<char>::~basic_string();
            }
        }

        inline Key(const Key& other)
        {
            type = other.type;

            // If we add a new value to the enum, then
            // this switch statement will need a new case!
            BOOST_STATIC_ASSERT(HandleRegistry::KeyType_LAST == 3);
            switch (type) {
                case HandleRegistry::KeyType_Object:
                    object = other.object;
                    break;
                case HandleRegistry::KeyType_Resource:
                    resource = other.resource;
                    break;
                case HandleRegistry::KeyType_String:
                    new (stringHandleBytes) std::string(other.getStringHandle());
                    break;
            }
        }

        static Key create(const ZValPointerAny& p);

        Key& operator=(const Key&) = delete;

        bool operator==(const Key& other) const;
        bool operator!=(const Key& other) const;


        inline const std::string& getStringHandle() const
        {
            BOOST_ASSERT(type == KeyType_String);
            return *reinterpret_cast<const std::string*>(stringHandleBytes);
        }

        KeyType type;
        union
        {
            t_HandleRegObjType object;
            t_HandleRegRsrcType resource;
            uint8_t stringHandleBytes[sizeof(std::string)];
        };
    };

    struct KeyHash
    {
        size_t operator()(const Key& c) const;
    };

    struct Value
    {
        Value()
            : handleInfo(boost::shared_ptr<AHandleInfo>())
            , objDtor(NULL) { }

        Value(boost::shared_ptr<AHandleInfo> hInfo)
            : handleInfo(hInfo)
            , objDtor(NULL) { }

        Value(boost::shared_ptr<AHandleInfo> hInfo, appd_obj_dtor_t dtor)
            : handleInfo(hInfo)
            , objDtor(dtor) { }

        boost::shared_ptr<AHandleInfo> handleInfo;
        appd_obj_dtor_t objDtor;
    };

public:
    HandleRegistry(bool hookResourceDestruction, bool hookObjectDestruction);

    ~HandleRegistry();

    /**
        Associates extra information with the object referenced by
        the specified zval.
        <p>
        If there is already any extra information associated with the specified object,
        it is replaced with the specified extra information.
        <p>
        If the specified zval does not reference an object, then this
        method does nothing.
        @param t_HandleInfo Sub-class of AHandleInfo that contains extra
        information to associate with the specified zval.
        @param object Pointer to a zval that references the object to associate extra
        information with.
        @param handleInfo Pointer to extra information to associate with the object
        referenced by the specified zval.
     */
    template <class t_HandleInfo>
    void registerHandle(const ZValPointerObject& object, const boost::shared_ptr<t_HandleInfo>& handleInfo);

    /**
        Drops any information associated with the object referenced
        by the specified zval.
        <p>
        If the specified zval does not reference an object, then this
        method does nothing.
        @param object Pointer to a zval that references the object whose associated
        information should be dropped.
     */
    void unregisterHandle(const ZValPointerObject& object);

    /**
        Gets the extra information associated with the object referenced by the specified
        zval.
        <p>
        If the specified zval does not reference an object, then this
        method returns null.
        @param object Pointer to a zval that references the object whose associated
        information should be retrieved.
        @return The information associated with the object referenced by the specified
        zval.
     */
    template <class t_HandleInfo>
    boost::shared_ptr<t_HandleInfo> getHandleInfo(const ZValPointerObject& object) const;

    /**
        Associates extra information with the object referenced by
        the specified zval.
        <p>
        If there is already any extra information associated with the specified object,
        it is replaced with the specified extra information.
        <p>
        If the specified zval does not reference an object, then this
        method does nothing.
        @param t_HandleInfo Sub-class of AHandleInfo that contains extra
        information to associate with the specified zval.
        @param object Pointer to a zval that references the object to associate extra
        information with.
        @param handleInfo Pointer to extra information to associate with the object
        referenced by the specified zval.
     */
    template <class t_HandleInfo>
    void registerHandle(const ZValPointerResource& object, const boost::shared_ptr<t_HandleInfo>& handleInfo);

    /**
        Drops any information associated with the object referenced
        by the specified zval.
        <p>
        If the specified zval does not reference an object, then this
        method does nothing.
        @param object Pointer to a zval that references the object whose associated
        information should be dropped.
     */
    void unregisterHandle(const ZValPointerResource& object);

    /**
        Gets the extra information associated with the object referenced by the specified
        zval.
        <p>
        If the specified zval does not reference an object, then this
        method returns null.
        @param object Pointer to a zval that references the object whose associated
        information should be retrieved.
        @return The information associated with the object referenced by the specified
        zval.
     */
    template <class t_HandleInfo>
    boost::shared_ptr<t_HandleInfo> getHandleInfo(const ZValPointerResource& object) const;

    /**
        Associates extra information with the object referenced by
        the specified string.
        <p>
        If there is already any extra information associated with the specified string,
        it is replaced with the specified extra information.
        <p>
        If the specified string does not reference an object, then this
        method does nothing.
        @param t_HandleInfo Sub-class of AHandleInfo that contains extra
        information to associate with the specified string.
        @param handle Reference to a string to associate extra information with.
        @param handleInfo Pointer to extra information to associate with the specified string.
     */
    template <class t_HandleInfo>
    void registerHandle(const ZValPointerString& handle, const boost::shared_ptr<t_HandleInfo>& handleInfo);

    /**
        Drops any information associated with the specified string.
        <p>
        If the specified string handle has not been registered, then this
        method does nothing.
        @param handle Reference to a string whose associated information
        should be dropped.
     */
    void unregisterHandle(const ZValPointerString& handle);

    /**
        Gets the extra information associated with the object referenced by the specified
        string.
        <p>
        If the specified string does not reference an object, then this
        method returns null.
        @param handle Reference to a string whose associated information
        should be retrieved.
        @return The information associated with the the specified string.
     */
    template <class t_HandleInfo>
    boost::shared_ptr<t_HandleInfo> getHandleInfo(const ZValPointerString& handle) const;

    /**
        Gets the extra information associated with the handle referenced by the specified
        zval.
        <p>
        If the specified zval does not reference an object or a resource, then this
        method returns null.
        @param object Pointer to a zval that references the object or
        resource whose associated information should be retrieved.
        @return The information associated with the object or resource referenced by the specified
        zval.
     */
    template <class t_HandleInfo>
    boost::shared_ptr<t_HandleInfo> getHandleInfo(const ZValPointerAny& any) const;

    template <class t_HandleInfo>
    void registerHandle(const ZValPointerAny& object, const boost::shared_ptr<t_HandleInfo>& handleInfo);

    void unregisterHandle(const ZValPointerAny& object);

    /**
        Drops information associated with all known objects.
        This method should be called at the end of every request.
     */
    void clear();

    inline int size() const { return m_map.size(); }

private:
    void registerHandle_impl(const ZValPointerObject& object, const boost::shared_ptr<AHandleInfo>& handleInfo);
    const boost::shared_ptr<AHandleInfo> getHandleInfo_impl(const ZValPointerObject& object, const AHandleInfo::TypeTag* const typeTag) const;

    void registerHandle_impl(const ZValPointerResource& resource, const boost::shared_ptr<AHandleInfo>& handleInfo);
    const boost::shared_ptr<AHandleInfo> getHandleInfo_impl(const ZValPointerResource& object, const AHandleInfo::TypeTag* const typeTag) const;

    void registerHandle_impl(const ZValPointerString& handle, const boost::shared_ptr<AHandleInfo>& handleInfo);
    const boost::shared_ptr<AHandleInfo> getHandleInfo_impl(const ZValPointerString& handle, const AHandleInfo::TypeTag* const typeTag) const;

    void registerHandle_impl(const ZValPointerAny& any, const boost::shared_ptr<AHandleInfo>& handleInfo);
    const boost::shared_ptr<AHandleInfo> getHandleInfo_impl(const ZValPointerAny& any, const AHandleInfo::TypeTag* const typeTag) const;
#if PHP_VERSION_ID >= 70000
    static void objectDestructor(zend_object* object);
    static void resourceDestructor(zval* resource);
#else
    static void objectDestructor(void* object, zend_object_handle handle TSRMLS_DC);
    static void resourceDestructor(void* pDest TSRMLS_DC);
#endif
    dtor_func_t m_origResourceDtor;
    boost::unordered_map<Key, Value, KeyHash> m_map;

    bool const m_hookResourceDestruction : 1;
    bool const m_hookObjectDestruction : 1;
    bool m_didHookResourceDestructor : 1;
};

inline bool HandleRegistry::Key::operator==(const HandleRegistry::Key& other) const
{
    if (type != other.type)
        return false;

    // If we add a new value to the enum, then
    // this switch statement will need a new case!
    BOOST_STATIC_ASSERT(HandleRegistry::KeyType_LAST == 3);
    switch (other.type)
    {
    case HandleRegistry::KeyType_Object:
        return object == other.object;
    case HandleRegistry::KeyType_Resource:
        return resource == other.resource;
    case HandleRegistry::KeyType_String:
        return getStringHandle() == other.getStringHandle();
    }
    // Should be un-reachable code...
    return false;
}

inline bool HandleRegistry::Key::operator!=(const HandleRegistry::Key& other) const
{
    return !(this->operator==(other));
}

inline size_t HandleRegistry::KeyHash::operator()(const HandleRegistry::Key& c) const
{
    size_t seed = 0;
    boost::hash_combine(seed, static_cast<unsigned>(c.type));
    // If we add a new value to the enum, then
    // this switch statement will need a new case!
    BOOST_STATIC_ASSERT(HandleRegistry::KeyType_LAST == 3);
    switch (c.type)
    {
    case HandleRegistry::KeyType_Object:
        boost::hash_combine(seed, /*static_cast<unsigned>*/(c.object));
        break;
    case HandleRegistry::KeyType_Resource:
        boost::hash_combine(seed, /*static_cast<unsigned>*/(c.resource));
        break;
    case HandleRegistry::KeyType_String:
        boost::hash_combine(seed, c.getStringHandle());
        break;
    }
    return seed;
}

template <class t_HandleInfo>
inline void HandleRegistry::registerHandle(const ZValPointerObject& object, const boost::shared_ptr<t_HandleInfo>& handleInfo)
{
    registerHandle_impl(object, handleInfo);
}

template <class t_HandleInfo>
inline boost::shared_ptr<t_HandleInfo> HandleRegistry::getHandleInfo(const ZValPointerObject& resource) const
{
    return boost::static_pointer_cast<t_HandleInfo>(getHandleInfo_impl(resource, &t_HandleInfo::typeTag));
}

template <class t_HandleInfo>
inline void HandleRegistry::registerHandle(const ZValPointerResource& resource, const boost::shared_ptr<t_HandleInfo>& handleInfo)
{
    registerHandle_impl(resource, handleInfo);
}

template <class t_HandleInfo>
inline boost::shared_ptr<t_HandleInfo> HandleRegistry::getHandleInfo(const ZValPointerResource& resource) const
{
    return boost::static_pointer_cast<t_HandleInfo>(getHandleInfo_impl(resource, &t_HandleInfo::typeTag));
}

template <class t_HandleInfo>
inline void HandleRegistry::registerHandle(const ZValPointerString& handle, const boost::shared_ptr<t_HandleInfo>& handleInfo)
{
    registerHandle_impl(handle, handleInfo);
}

template <class t_HandleInfo>
inline boost::shared_ptr<t_HandleInfo> HandleRegistry::getHandleInfo(const ZValPointerString& handle) const
{
    return boost::static_pointer_cast<t_HandleInfo>(getHandleInfo_impl(handle, &t_HandleInfo::typeTag));
}

template <class t_HandleInfo>
inline void HandleRegistry::registerHandle(const ZValPointerAny& any, const boost::shared_ptr<t_HandleInfo>& handleInfo)
{
    registerHandle_impl(any, handleInfo);
}

template <class t_HandleInfo>
inline boost::shared_ptr<t_HandleInfo> HandleRegistry::getHandleInfo(const ZValPointerAny& any) const
{
    return boost::static_pointer_cast<t_HandleInfo>(getHandleInfo_impl(any, &t_HandleInfo::typeTag));
}

#endif
