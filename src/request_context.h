/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#ifndef __request_context_h
#define __request_context_h

#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>

#include "errors.h"
#include "exitpoint.h"
#include "eum/context.h"
#include "PHPAgentProtobufs.pb.h"

class CallMetrics;
namespace InfoPoint
{
    class Metrics;
}

class Timer;
class IConfigChannel;

/**
    Class that keeps track of timing at the start of either a request or a transaction.
    Timing data is recorded at the beginning of each request, and that timing data is usually
    transferred to the transaction, unless there is an API-initiated transaction, in which
    case the timing at the start of the API call is used instead.
 */
class TimePoint {
private:
    uint64_t m_timestamp;
    uint64_t m_wallTime;
    uint64_t m_skewAdjustedWallTime;

public:
    TimePoint(const Timer* timer, uint64_t timestampSkew) {
        reset(timer, timestampSkew);
    }

    void reset(const Timer* timer, uint64_t timestampSkew);

    inline uint64_t getTimestamp() const { return m_timestamp; }
    inline uint64_t getWallTime() const { return m_wallTime; }
    inline uint64_t getSkewAdjustedWallTime() const { return m_skewAdjustedWallTime; }
};

/**
    Class that keeps track of request specific information.
    An instance of this class is created at the beginning of each
    php request and destroyed at the end of the php request.
 */
class RequestContext : boost::noncopyable
{
    template <class T, class Arg1, class... Args>
    friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);

    typedef boost::shared_ptr<CallMetrics> t_CallMetricsPtr;
    typedef boost::shared_ptr<InfoPoint::Metrics> t_InfoPointMetricsPtr;
    typedef boost::unordered_map<ExitCallMetricsKey, t_CallMetricsPtr> t_RegisteredExitCallMetricsMap;
    typedef boost::unordered_map<uint32_t, t_InfoPointMetricsPtr> t_InfoPointMetricsMap;


public:
    /**
        @return A new RequestContext wrapped in a shared_ptr.
     */
    static boost::shared_ptr<RequestContext> create(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                                                    ErrorMonitor* errorMonitor,
                                                    const boost::shared_ptr<const EUM::Config>& eumConfig);

    virtual ~RequestContext();

    static void forceDebugLogging()
    {
        RequestDebugLoggingManager::enable();
    }

    inline const TimePoint& getStartTime() const { return m_startTime; }

    inline const Timer* getTimer() const { return m_timer; }

    inline const boost::shared_ptr<IConfigChannel>& getConfigChannel() const { return m_configChannel; }

    const std::string& getRequestGUID() const {
        if (!m_requestGUIDComputed) {
            m_requestGUIDComputed = true;
            m_requestGUID = computeRequestGUID();
        }
        return m_requestGUID;
    }

    inline void setRequestGUID(const std::string& requestGUID) {
        m_requestGUIDComputed = true;
        m_requestGUID = requestGUID;
    }

    inline void clearRequestGUID() { m_requestGUIDComputed = false; }

    inline ErrorRegistry* getErrorRegistry() { return &m_errorRegistry; }

    void reportError(const boost::shared_ptr<AErrorObject>& errorObject, bool markTransactionAsError);

    inline bool isErrorRequest() const { return m_markedAsError; }

    inline void clearErrorStatus() { m_markedAsError = false; }

    inline bool isFrameworkDetected() const { return m_frameworkDetected; }

    inline void setFrameworkDetected(bool value) { m_frameworkDetected = value; }

    inline bool isCorrelationSent() const { return m_correlationSent; }

    inline void setCorrelationSent(bool value) { m_correlationSent = value; }

    inline bool isBTDetectionDisabled() const { return m_btDetectionDisabled; }

    inline void setBTDetectionDisabled(bool value) { m_btDetectionDisabled = value; }

    inline void setProxyRequestID(int64_t requestID) { m_proxyRequestID = requestID; }

    inline int64_t getProxyRequestID() const { return m_proxyRequestID; }

    inline void resetProxyMessageID() { m_proxyMessageID = 0; }

    inline int32_t nextProxyMessageID() { return ++m_proxyMessageID; }

    inline void setBTInfoRequest(appdynamics::pb::BTInfoRequest* request) { m_btInfoRequest.reset(request); }

    inline appdynamics::pb::BTInfoRequest* getBTInfoRequest() { return m_btInfoRequest.get(); }

    inline void setBTInfoResponse(appdynamics::pb::BTInfoResponse* response) { m_btInfoResponse.reset(response); }

    inline appdynamics::pb::BTInfoResponse* getBTInfoResponse() { return m_btInfoResponse.get(); }

    inline EUM::Context& eumContext() { return m_eumContext; }

    inline const EUM::Context& eumContext() const { return m_eumContext; }

    /**
     * Reports elapsed time for an exit call against the given backend
     * and category.
     * Increments the call count by 1 and error count, if there is an
     * error.
     *
     * @param backendID Identifier of the backend.
     * @param category The category to assign the metrics to (can be
     * empty)
     * @param exitType Exit point type (here to help proxy avoid an
     * extra look-up)
     * @param timeTaken Elapsed time, in microseconds.
     * @param callCount Number of calls to count during the elapsed time
     * (can be 0, to accumulate time without incrementing call count)
     * @param hasError Whether the exit call encountered an error
     * condition.
     * @param errorCount The number of errors that occurred during the exit call.
     */
    void reportExitCallData(const BackendIdentifierPtr& backendID,
                            const std::string& category,
                            long timeTaken,
                            long callCount,
                            unsigned errorCount);

    void reportInfoPointCall(uint32_t infoPointID,
                             int64_t timeTakenUS,
                             unsigned errorCount);

    void reportInfoPointCustomMetric(uint32_t infoPointID,
                                     const std::string& metricName,
                                     appdynamics::pb::Instrumentation::CustomMetricDefinition::Rollup rollup,
                                     double metricValue);
    /**
     * Returns exit call metrics map. The key is the registered backend
     * ID and the value is the object maintaining exit call metrics.
     */
    const t_RegisteredExitCallMetricsMap& getRegisteredExitCallMetrics() const { return m_registeredExitCallMetrics; }

    const t_InfoPointMetricsMap& getInfoPointMetrics() const { return m_infoPointMetrics; }

    /**
        Evaluates the current snapshot policy if we have received a policy from the
        proxy.

        @param requestElapsedTimeMS The current request execution time.
        @param includeContinuing Whether to include continuing transactions into evaluation
        @param transactionContext The current transaction context
        @return NULL, if no snapshot is needed.  Otherwise a pointer to
        a static const field of appdynamics::pb::SnapshotInfo for the snapshot
        trigger that caused the snapshot to be taken.  The return value never
        needs to be free'd.
     */
    const appdynamics::pb::SnapshotInfo::Trigger* evaluateSnapshotPolicy(uint64_t const requestElapsedTimeMS,
                                                                         bool includeContinuing,
                                                                         const TransactionContext* transactionContext);

    /**
        Gets the average response time of the current BT for the last minute from the
        BTInfoResponse if we have one.
     */
    bool getAverageResponseTimeForLastMinute(int64_t* dest);

    /**
        Resets any state in the request context, prepares it for use in a fresh transaction.
     */
    void clear();

private:
    RequestContext(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                   ErrorMonitor* errorMonitor,
                   const boost::shared_ptr<const EUM::Config>& eumConfig);

    t_CallMetricsPtr getRegisteredExitCallMetrics(const ExitCallMetricsKey& metricsKey);
    void reportExitCallData(const t_CallMetricsPtr& metrics,
                            long timeTaken,
                            long callCount,
                            unsigned errorCount);

    t_InfoPointMetricsPtr getInfoPointMetrics(uint32_t infoPointID);

    std::string computeRequestGUID() const;

    const Timer* const m_timer;

    boost::shared_ptr<IConfigChannel> m_configChannel;

    TimePoint m_startTime;

    mutable bool m_requestGUIDComputed;
    mutable std::string m_requestGUID;

    ErrorRegistry m_errorRegistry;

    t_RegisteredExitCallMetricsMap m_registeredExitCallMetrics;
    t_InfoPointMetricsMap m_infoPointMetrics;

    bool m_markedAsError;
    bool m_frameworkDetected;
    bool m_correlationSent;
    bool m_btDetectionDisabled;

    int64_t m_proxyRequestID;
    int32_t m_proxyMessageID;

    unique_ptr<appdynamics::pb::BTInfoRequest>::type m_btInfoRequest;
    unique_ptr<appdynamics::pb::BTInfoResponse>::type m_btInfoResponse;

    EUM::Context m_eumContext;
};

#endif
