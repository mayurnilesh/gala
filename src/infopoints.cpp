/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#include "agent.h"
#include "infopoints.h"
#include "request_context.h"
#include "timer.h"
#include "expressions.h"

#include <boost/make_shared.hpp>

namespace InfoPoint
{

AgentLogger Interceptor::m_logger = NullLoggerPtr;

ConfigManager::ConfigManager(InterceptionEngine* interceptionEngine,
                             const boost::shared_ptr<IConfigChannel>& configChannel)
    : m_interceptionEngine(interceptionEngine)
    , m_logger(getLogger(std::string(LogContext::INFOPOINT) + ".InfoPoint::ConfigManager"))
{
    configChannel->addListener(this);
}

ConfigManager::~ConfigManager()
{
}

void ConfigManager::configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                  const struct _zend_agent_globals* agentGlobals)
{
    if (configResponse.has_infopointconfig()) {
        boost::shared_ptr<appdynamics::pb::InformationPointConfig> infoPointConfig(
                boost::make_shared<appdynamics::pb::InformationPointConfig>());
        infoPointConfig->CopyFrom(configResponse.infopointconfig());
        configure(infoPointConfig);
    }
}

void ConfigManager::configure(const boost::shared_ptr<appdynamics::pb::InformationPointConfig>& config)
{
    // Clear probes first because they depend on the config in m_config.
    clearProbes();
    m_config = config;

    size_t numInfoPoints = config->infopoints_size();
    for (int i = 0; i < numInfoPoints; i++) {
        const Instrumentation::InformationPoint* infoPoint = &(config->infopoints(i));

        if (!config->infopoints(i).probe().has_phpdefinition()) continue;

        boost::shared_ptr<InstrumentationSite> instrumentationSite
            = InstrumentationSite::create(infoPoint->probe().phpdefinition(), m_logger);
        if (!instrumentationSite) {
            LOG4CXX_WARN(m_logger, "Could not resolve callable from instrumentation probe config: " << infoPoint->probe().phpdefinition().DebugString());
            continue;
        }

        uint32_t const infoPointID = config->infopointids(i);
        boost::shared_ptr<Interceptor> interceptor(
                boost::make_shared<Interceptor>(infoPoint,
                                                infoPointID));

        m_interceptionEngine->addInterceptorForCallable(instrumentationSite->getCallableInfo(), interceptor.get());
        LOG4CXX_TRACE(m_logger, "adding infopoint interceptor to callable " << instrumentationSite->getCallableInfo());
        m_probes.push_back(std::make_pair(instrumentationSite, interceptor));
    }
}

void ConfigManager::activateProbes(bool active)
{
    BOOST_FOREACH(auto& it, m_probes)
    {
        it.second->setActive(active);
    }
}

void ConfigManager::clearProbes()
{
    BOOST_FOREACH(const auto& it, m_probes)
    {
        LOG4CXX_TRACE(m_logger, "removing infopoint interceptor from callable " << it.first->getCallableInfo());
        m_interceptionEngine->removeInterceptorForCallable(it.first->getCallableInfo(), it.second.get());
    }
    m_probes.clear();
}

Interceptor::Interceptor(const Instrumentation::InformationPoint* infoPoint,
                         uint32_t infoPointID)
    : AMethodInterceptor("InfoPoint::Interceptor",
                         InterceptorRegistry::InfoPointInterceptor_ID)
    , m_infoPoint(infoPoint)
    , m_infoPointID(infoPointID)
    , m_active(false)
{
    if (m_logger == NullLoggerPtr) {
        m_logger = getLogger(std::string(LogContext::INFOPOINT) + ".InfoPoint::Interceptor");
    }
}

void* Interceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    // Use emalloc() so that the if there is a bailout and on onCallableEnd() does not
    // have a chance to release the memory, the Zend engine will.
    uint64_t* startTime = reinterpret_cast<uint64_t*>(emalloc(sizeof(uint64_t)));
    *startTime = AgentKernel::getTimer()->getTimestamp();
    return startTime;
}

void Interceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
{
    BOOST_ASSERT(state != NULL);
    uint64_t startTime = *reinterpret_cast<uint64_t*>(state);
    uint64_t timeTakenUS = AgentKernel::getTimer()->getElapsedTime(startTime);
    efree(state);

    zval* exceptionObject = execEnv->getExceptionObject();

    LOG4CXX_DEBUG(m_logger, "info point ID " << m_infoPointID << " condition expression evaluation start");

    if (m_infoPoint->probe().phpdefinition().has_methodmatch()) {
        if (!InstrumentationUtil::matchCallableEnvironment(
                    m_infoPoint->probe().phpdefinition().methodmatch(), execEnv)) {
            LOG4CXX_DEBUG(m_logger, "info point ID " << m_infoPointID << " condition expression did not match");
            return;
        }

        LOG4CXX_DEBUG(m_logger, "info point ID " << m_infoPointID << " condition expression matched, time: " << timeTakenUS << " us");
    }

    const boost::shared_ptr<RequestContext>& requestContext = execEnv->getAgentGlobals().request_context;

    requestContext->reportInfoPointCall(m_infoPointID, timeTakenUS, (exceptionObject != NULL));

    BOOST_FOREACH(auto metricDef, m_infoPoint->metricdefinitions())
    {
        LOG4CXX_TRACE(m_logger, "getting value for metric [" << metricDef.name() << "]");
        double metricValue = InstrumentationUtil::getMetricFromCallable(metricDef, execEnv);
        requestContext->reportInfoPointCustomMetric(m_infoPointID, metricDef.name(), metricDef.rollup(), metricValue);
    }
}

}
