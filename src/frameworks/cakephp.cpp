/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#include "cakephp.h"
#include "frameworks/mvc.h"
#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include <boost/algorithm/string/join.hpp>

namespace CakePHP
{
    static const char NAME_PROPERTY_NAME[] = "name";
    static const char PARAMS_PROPERTY_NAME[] = "params";
    static const char ACTION_PROPERTY_NAME[] = "action";

    ControllerInvokeActionInterceptor::ControllerInvokeActionInterceptor()
        : AEntryPointInterceptor("CakePHP::ControllerInvokeActionInterceptor",
                                 InterceptorRegistry::CakePHPControllerInvokeActionInterceptor_ID)
        , m_logger(getLogger(std::string(LogContext::AGENT) + ".ControllerInvokeActionInterceptor"))
    {
    }

    boost::shared_ptr<IMatchPointPayload>
        ControllerInvokeActionInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        // CakePHP _invoke method that we're intercepting only has 2 params in version 2.8.3
        if (execEnv->getParamCount() != 3 && execEnv->getParamCount() != 2) {
            LOG4CXX_WARN(m_logger, "The param count for the intercepted method does not match our expectations!"
                << " Not creating payload.");
            return boost::shared_ptr<IMatchPointPayload>();
        }            

        ZValPointerObject controllerObject(
                execEnv->getParam<ZValPointerObject>(0));
        if (!controllerObject)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerString nameObject(
                controllerObject.findPropertyByName<ZValPointerString>(execEnv,
                    NAME_PROPERTY_NAME, strlen(NAME_PROPERTY_NAME)));
        if (!nameObject)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerObject requestObject(
                execEnv->getParam<ZValPointerObject>(1));
        if (!requestObject)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerArray paramsArray(
                requestObject.findPropertyByName<ZValPointerArray>(execEnv,
                    PARAMS_PROPERTY_NAME, strlen(PARAMS_PROPERTY_NAME)));
        if (!paramsArray)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerString actionObject(
                paramsArray.findEntryByName<ZValPointerString>(
                    ACTION_PROPERTY_NAME, strlen(ACTION_PROPERTY_NAME)));
        if (!actionObject)
            return boost::shared_ptr<IMatchPointPayload>();

        CallGraph::ExecStackFrame* const topStackElement =
            execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();

        if (topStackElement)
            topStackElement->setCallElementType(appdynamics::pb::CallElement_Type_CAKEPHP);

        std::string controller = nameObject.getStringValue();
        std::string action = actionObject.getStringValue();


        return boost::make_shared<MVCPayload,
                                  const std::string&,
                                  const std::string&,
                                  const std::string&,
                                  const PHPExecEnvironment* const&>(std::string(),
                                                                    controller,
                                                                    action,
                                                                    execEnv);
    }

    bool ControllerInvokeActionInterceptor::isTxToBeIdentified(const PHPExecEnvironment *execEnv) const
    {
        const TransactionContext* const txContext =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
        if (txContext && txContext->getInitiatingInterceptor() == this)
            return false;
        return AEntryPointInterceptor::isTxToBeIdentified(execEnv);
    }

    appdynamics::pb::Agent::EntryPointType ControllerInvokeActionInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }
}
