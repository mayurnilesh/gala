/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#include "symfony2.h"
#include "intercept.h"
#include "zval_helper.h"
#include "frameworks/mvc.h"
#include "agent.h"

#include "callgraph/exec_stack_frame.h"

#include <zend.h>

#include "emalloc_allocator.h"


namespace Symfony2
{

    static inline bool canHookHandler(zend_function* handler)
    {
        if (!handler)
            return false;
        switch (handler->type)
        {
        case ZEND_INTERNAL_FUNCTION:
            return false;
        case ZEND_OVERLOADED_FUNCTION_TEMPORARY:
            return false;
        case ZEND_OVERLOADED_FUNCTION:
            return false;
        default:
            return true;
        }
    }

    static inline void freeHandlerZendFunctionIfNeeded(zend_function* handler)
    {
        if (!handler)
            return;
        bool isCallViaHandlerInternalFunc =
            handler->type == ZEND_INTERNAL_FUNCTION &&
            (handler->common.fn_flags & ZEND_ACC_CALL_VIA_HANDLER);
        if (    isCallViaHandlerInternalFunc
             || handler->type == ZEND_OVERLOADED_FUNCTION_TEMPORARY
             || handler->type == ZEND_OVERLOADED_FUNCTION )
        {

            if (handler->type != ZEND_OVERLOADED_FUNCTION)
            {
#if PHP_VERSION_ID >= 70000
                zend_string_release(handler->common.function_name);
#else
                efree(const_cast<char*>(handler->common.function_name));
#endif
            }
            efree(handler);
        }
    }

    GetArgumentsInterceptor::GetArgumentsInterceptor()
        : AMethodInterceptor("Symfony2::GetArgumentsInterceptor",
                             InterceptorRegistry::Symfony2GetArgumentsInterceptor_ID)
    {
    }

    bool GetArgumentsInterceptor::isActive(const PHPExecEnvironment* execEnv) const
    {
        return !AG(kernel)->getAgentContext()
                          ->getTransactionMonitor()
                          ->getEntryPointDelegate(appdynamics::pb::Agent::PHP_MVC)
                          ->isDetectionDisabled();
    }

    void* GetArgumentsInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        if (phpExecEnv->getParamCount() != 2)
            return NULL;

        ZValPointerAny param1(phpExecEnv->getParam(1));

        zend_function* handler = phpExecEnv->lookupCallable(param1);
        if (!canHookHandler(handler))
        {
            // Some zend_function's that we can't
            // intercept need to be free'd.  All of the
            // zend_function's we can intercept do *not* need
            // to be free'd, so this is the only place
            // we need to free zend_function's in this method.
            freeHandlerZendFunctionIfNeeded(handler);
            return NULL;
        }

        BOOST_ASSERT(handler->type != ZEND_INTERNAL_FUNCTION);
        InterceptionEngine* interceptEngine = phpExecEnv->getInterceptionEngine();
        zend_op_array* opArray = &(handler->op_array);
        interceptEngine->addInterceptorForOpArray(opArray, &symfony2HandlerInterceptor);
        return NULL;
    }

    void GetArgumentsInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                void* state)
    {
    }

    HandlerInterceptor::HandlerInterceptor()
        : AEntryPointInterceptor("Symfony2::HandlerInterceptor",
                                 InterceptorRegistry::Symfony2HandlerInterceptor_ID)
    {

    }

    boost::shared_ptr<IMatchPointPayload> HandlerInterceptor::createPayload(const PHPExecEnvironment* const execEnv)
    {
        CallGraph::ExecStackFrame* const topStackElement =
            execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        if (topStackElement)
            topStackElement->setCallElementType(appdynamics::pb::CallElement_Type_SYMFONY);
        const char* className = execEnv->getClassName();
        const char* functionName = execEnv->getFunctionName();
        return boost::make_shared<MVCPayload,
                                  const std::string&,
                                  const std::string&,
                                  const std::string&,
                                  const PHPExecEnvironment* const&>(std::string(),
                                                                    className ? className : "",
                                                                    functionName,
                                                                    execEnv);
    }

    appdynamics::pb::Agent::EntryPointType HandlerInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }
}
