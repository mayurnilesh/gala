/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#ifndef __symfony2_h
#define __symfony2_h

#include <boost/shared_ptr.hpp>

#include "entrypoint.h"

namespace Symfony2
{
    // Forward declaration of the interceptor used to intercept calls
    // the Symfony handler's
    class HandlerInterceptor;

    /**
        Interceptor for intercepting calls to
        Symfony\\Component\\HttpKernel\\Controller\\ControllerResolver::getArguments.
        <p>
        This interceptor adds the HandlerInterceptor below to the callable specified
        by the first parameter passed to the getArguments method.
     */
    class GetArgumentsInterceptor : public AMethodInterceptor
    {
    public:
        GetArgumentsInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const;
    };

    /**
        Interceptor for Symfony2 handlers.  This interceptor is
        added to handler callables by the GetArgumentsInterceptor above.
     */
    class HandlerInterceptor : public AEntryPointInterceptor
    {
    public:
        HandlerInterceptor();
        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return false; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }
    };
}

extern Symfony2::HandlerInterceptor symfony2HandlerInterceptor;

#endif
