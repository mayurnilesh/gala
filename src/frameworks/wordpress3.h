/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __wordpress3_h
#define __wordpress3_h

#include "entrypoint.h"

namespace Wordpress3
{
    /**
        Entry point interceptor for the apply_filters call in wordpress 3's
        template-loader.php.
     */
    class ApplyFiltersInterceptor : public AEntryPointInterceptor
    {
    public:
        ApplyFiltersInterceptor();

        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);

        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return true; }
    };

    /**
        Delegate that applies interception rules to detect business
        transactions in Drupa7.
     */
    class EntryPointDelegate : public AEntryPointDelegate
    {
        public:
            EntryPointDelegate(InterceptionEngine* interceptEngine,
                               TransactionMonitor *txMonitor);
            virtual ~EntryPointDelegate();
        protected:
            virtual void configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                   const struct _zend_agent_globals* agentGlobals);
            virtual void initAcceptor();
            virtual void applyRules(const struct _zend_agent_globals* agentGlobals);
        private:
            static bool s_didApplyRules;
    };


}

#endif
