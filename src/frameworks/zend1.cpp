/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#include "zend1.h"
#include "zval_helper.h"
#include "mvc.h"

static const char GET_REQUEST_NAME[] = "getRequest";
static const size_t GET_REQUEST_NAME_LEN = sizeof("getRequest")-1;

static const char GET_MODULE_NAME[] = "getModuleName";
static const size_t GET_MODULE_NAME_LEN = sizeof("getModuleName")-1;

static const char GET_CONTROLLER_NAME[] = "getControllerName";
static const size_t GET_CONTROLLER_NAME_LEN = sizeof("getControllerName")-1;

static const char GET_ACTION_NAME[] = "getActionName";
static const size_t GET_ACTION_NAME_LEN = sizeof("getActionName")-1;

Zend1EntryPointInterceptor::Zend1EntryPointInterceptor()
    : AEntryPointInterceptor("Zend1EntryPointInterceptor",
                             InterceptorRegistry::Zend1EntryPointInterceptor_ID)
{
}

Zend1EntryPointInterceptor::~Zend1EntryPointInterceptor()
{
}

boost::shared_ptr<IMatchPointPayload> Zend1EntryPointInterceptor::createPayload(const PHPExecEnvironment* execEnv)
{
    // Intercepted method's signature:
    // public function Zend_Controller_Action::dispatch($action)

    // The dispatch method is an instance method, so
    // if object is NULL then this interceptor does not
    // recognize the method being called!
    zval* object = execEnv->getInvokedObject();
    if (!object)
        return boost::shared_ptr<IMatchPointPayload>();
    ZValPointerObject phpObject(ZValPointerAny::share(const_cast<zval*>(object)).cast<ZValPointerObject>());
    if (!phpObject)
        return boost::shared_ptr<IMatchPointPayload>();

    // Call this->getRequest(), which will return a request object from which we can
    // extract Module/Controller/Action.
    ZValPointerObject requestObject(phpObject.callInstanceMethod<ZValPointerObject>(execEnv, GET_REQUEST_NAME, GET_REQUEST_NAME_LEN));
    if (!requestObject)
        return boost::shared_ptr<IMatchPointPayload>();

    // Call this->getRequest()->getModuleName() to get the module name.
    ZValPointerString moduleName(requestObject.callInstanceMethod<ZValPointerString>(execEnv, GET_MODULE_NAME, GET_MODULE_NAME_LEN));

    // Call this->getRequest()->getControllerName() to get the controller name.
    ZValPointerString controllerName(requestObject.callInstanceMethod<ZValPointerString>(execEnv, GET_CONTROLLER_NAME, GET_CONTROLLER_NAME_LEN));

    // Call this->getRequest()->getActionName() to get the action name.
    ZValPointerString actionName(requestObject.callInstanceMethod<ZValPointerString>(execEnv, GET_ACTION_NAME, GET_ACTION_NAME_LEN));

    return boost::make_shared<MVCPayload>(moduleName.getStringValue(), controllerName.getStringValue(), actionName.getStringValue(), execEnv);
}

appdynamics::pb::Agent::EntryPointType Zend1EntryPointInterceptor::getEntryPointType() const
{
    return appdynamics::pb::Agent::PHP_MVC;
}

Zend1RedirectInterceptor::Zend1RedirectInterceptor()
    : AEntryPointInterceptor("Zend1EntryPointInterceptor",
                             InterceptorRegistry::Zend1EntryPointInterceptor_ID)
{
}

Zend1RedirectInterceptor::~Zend1RedirectInterceptor()
{
}

boost::shared_ptr<IMatchPointPayload> Zend1RedirectInterceptor::createPayload(const PHPExecEnvironment* execEnv)
{
    // Intercepted method's signatures:
    // public function redirect($url, array $options = array())
    // public function _redirect($url, array $options = array())

    // The redirect method is an instance method, so
    // if object is NULL then this interceptor does not
    // recognize the method being called!
    zval* object = execEnv->getInvokedObject();
    if (!object)
        return boost::shared_ptr<IMatchPointPayload>();
    ZValPointerObject phpObject(ZValPointerAny::share(const_cast<zval*>(object)).cast<ZValPointerObject>());
    if (!phpObject)
        return boost::shared_ptr<IMatchPointPayload>();

    // Call this->getRequest(), which will return a request object from which we can
    // extract the module name and controller name.
    ZValPointerObject requestObject(phpObject.callInstanceMethod<ZValPointerObject>(execEnv, GET_REQUEST_NAME, GET_REQUEST_NAME_LEN));
    if (!requestObject)
        return boost::shared_ptr<IMatchPointPayload>();

    // Call this->getRequest()->getModuleName() to get the module name.
    ZValPointerString moduleName(requestObject.callInstanceMethod<ZValPointerString>(execEnv, GET_MODULE_NAME, GET_MODULE_NAME_LEN));

    // Call this->getRequest()->getControllerName() to get the controller name.
    ZValPointerString controllerName(requestObject.callInstanceMethod<ZValPointerString>(execEnv, GET_CONTROLLER_NAME, GET_CONTROLLER_NAME_LEN));

    return boost::make_shared<MVCPayload>(moduleName.getStringValue(), controllerName.getStringValue(), "[redirect]", execEnv);
}

appdynamics::pb::Agent::EntryPointType Zend1RedirectInterceptor::getEntryPointType() const
{
    return appdynamics::pb::Agent::PHP_MVC;
}