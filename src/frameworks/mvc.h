/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#ifndef __MVC_H
#define __MVC_H

#include <string>
#include "payload.h"
#include "transactions.h"
#include "entrypoint.h"
#include "main/php_version.h"

class Symfony1EntryPointInterceptor;
namespace Symfony2
{
    class GetArgumentsInterceptor;
    class HandlerInterceptor;
}

class Zend1EntryPointInterceptor;
class Zend1RedirectInterceptor;

namespace zend2
{
    class ActionControllerEntryPointInterceptor;
    class RestfulControllerEntryPointInterceptor;
    class RestfulControllerHTTPMethodInterceptor;
}

namespace CodeIgniter
{
    class SetRoutingInterceptor;
    class HandlerInterceptor;
}

namespace FuelPHP
{
    class RequestExecuteInterceptor;
}

namespace CakePHP
{
    class ControllerInvokeActionInterceptor;
}

namespace Drupal8
{
    class PageCacheSetInterceptor;
    class PageCacheGetInterceptor;
}

/**
    Delegate that applies interception rules to detect business
    transaction that are dispatched by mvc frameworks such as:
      Symfony( http://symfony.com/ ) framework.
      Zend1
 */
class MVCEntryPointDelegate : public AEntryPointDelegate {
    public:
        MVCEntryPointDelegate(InterceptionEngine* interceptEngine,
                              TransactionMonitor* txMonitor);
        virtual ~MVCEntryPointDelegate();
    protected:
        virtual void configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                               const struct _zend_agent_globals* agentGlobals);
        virtual void initAcceptor();
        virtual void applyRules(const struct _zend_agent_globals* agentGlobals);
    private:
        static bool s_didApplyRules;
};

/**
    Transaction acceptor for business transaction detected with the
    SymfonyEntryPointInterceptor.
 */
class MVCTransactionAcceptor : public ATransactionAcceptor {
    public:
        inline MVCTransactionAcceptor(const boost::shared_ptr<MatchPointConfig>& config,
                                      TransactionMonitor* monitor)
            : ATransactionAcceptor(config, monitor) { }

    protected:
        virtual bool customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                            const boost::shared_ptr<IMatchPointPayload>& payload);
        virtual bool matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& iPayload,
                                          const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition);
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() { return appdynamics::pb::Agent::PHP_MVC; }
        virtual std::string getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& payload);

};

/**
    Class to carry around information extracted by
    the Model/View/Controller framework interceptors.
 */
class MVCPayload : public IMatchPointPayload {
    public:
        MVCPayload(const std::string& module,
                   const std::string& controller,
                   const std::string& action,
                   const PHPExecEnvironment* execEnv)
            : m_module(module)
            , m_controller(controller)
            , m_action(action)
            , m_execEnv(execEnv)
        {
        }
        virtual ~MVCPayload() { }

        /**
            @return the name of the module containing the controller that an mvc framework
            dispatches a business transaction too.
         */
        const std::string& getModule() const { return m_module; }

        /**
            @return the name of the controller containing the action that an mvc framework
            dispatches a business transaction too.
         */
        const std::string& getController() const { return m_controller; }

        /**
            @return the name of the action that a mvc framework
            dispatches a business transaction too.
         */
        const std::string& getAction() const { return m_action; }

        const PHPExecEnvironment* getExecEnvironment() const { return m_execEnv; }

    private:
        std::string m_module;
        std::string m_controller;
        std::string m_action;
        const PHPExecEnvironment* const m_execEnv;
};

#endif /* __MVC_H */
