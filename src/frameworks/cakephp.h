/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#ifndef __cakephp_h
#define __cakephp_h

#include "entrypoint.h"

namespace CakePHP
{
    /*
     * Interceptor that is set on Controller::invokeAction() method and names
     * the BT based on the information contained in the $params property of the
     * request. The invokeAction() method re-throws any exceptions it gets, so we
     * the agent should be able to report them.
     */
    class ControllerInvokeActionInterceptor : public AEntryPointInterceptor
    {
        public:
            ControllerInvokeActionInterceptor();
            virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
            virtual bool isTxToBeIdentified(const PHPExecEnvironment *execEnv) const;
            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

            virtual bool needParamsInOnMethodBegin() const { return false; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return false; }

        private:
            AgentLogger m_logger;

     };
}

#endif // __cakephp_h
