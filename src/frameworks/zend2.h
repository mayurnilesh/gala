/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */
#ifndef __zend2_h
#define __zend2_h

#include "main/php_version.h"
#if PHP_VERSION_ID >= 50300

#include "zval_helper.h"
#include "entrypoint.h"
#include "mvc.h"
#include "agent.h"

namespace zend2
{
    /**
        Interceptor used to name business transactions that are dispatched
        by the Zend2 framework to sub-classes of AbstractActionController.
     */
    class ActionControllerEntryPointInterceptor : public AEntryPointInterceptor {
        public:
            ActionControllerEntryPointInterceptor();
            ~ActionControllerEntryPointInterceptor();
            virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

            virtual bool needParamsInOnMethodBegin() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return false; }
    };

    class RestfulControllerHTTPMethodInterceptor : public AEntryPointInterceptor {
        private:
            std::string m_controllerName;
        public:
            RestfulControllerHTTPMethodInterceptor();
            ~RestfulControllerHTTPMethodInterceptor();
            virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

            virtual bool needParamsInOnMethodBegin() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return false; }

            inline void setControllerName(std::string controllerName){ m_controllerName = controllerName; }
    };

    /**
        Same as ActionControllerEntryPointInterceptor, but for
        AbstractRestfulControllers.
        We intercept onDispatch() of the abstract class and then add
        a RestfulControllerHTTPMethodInterceptor for each HTTP method in the
        implemeting class.
     */
    class RestfulControllerEntryPointInterceptor : public AMethodInterceptor {
        private:
            std::string m_controllerClass;
            RestfulControllerHTTPMethodInterceptor m_httpMethodInterceptor;
        public:
            RestfulControllerEntryPointInterceptor();
            ~RestfulControllerEntryPointInterceptor();
            virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);
            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

            virtual bool needParamsInOnMethodBegin() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return true; }

            bool isActive(const PHPExecEnvironment* execEnv) const;
    };
}

#endif

#endif
