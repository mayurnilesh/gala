#ifndef __detector__h
#define __detector__h

#include "agent.h"

namespace FrameworkDetector
{
    /**
        Determines if a framework for which detection is
        enabled is loaded.

        If the entry point for a framework is disabled then
        this function will not attempt to detect that framework.

        If detection of a specific framework is disabled in the framework
        flags structure ( see frameworks.h ), then this function will
        not attempt to detect that framework.

        @param execEnv PHP execution environment.  Used to access
        agent global state.

        @return true if any framework for which detection is enabled is
        known to be loaded.  false otherwise.
     */
    bool isFrameworkLoaded(const PHPExecEnvironment* execEnv);
}

#endif // __detector__h
