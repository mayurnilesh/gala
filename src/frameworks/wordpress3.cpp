/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "wordpress3.h"
#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include "emalloc_allocator.h"
#include "naming.h"

#include <boost/filesystem.hpp>
#include <limits>

namespace
{

    boost::filesystem::path naive_uncomplete(const boost::filesystem::path& path,
                                             const boost::filesystem::path& base)
    {
        if (path.has_root_path()) {
            if (path.root_path() != base.root_path())
                return path;
            return naive_uncomplete(path.relative_path(), base.relative_path());
        }

        BOOST_ASSERT(!base.has_root_path());
        if (base.has_root_path())
            return boost::filesystem::path();

        auto pathIter = path.begin();
        auto baseIter = base.begin();
        while ((pathIter != path.end()) &&
               (baseIter != base.end()) &&
               (*pathIter == *baseIter)) {
            ++pathIter;
            ++baseIter;
        }

        boost::filesystem::path result;
        while (baseIter != base.end()) {
            result /= "..";
            ++baseIter;
        }

        while (pathIter != path.end()) {
            result /= *pathIter;
            ++pathIter;
        }
        return result;
    }

}

namespace Wordpress3
{
    static const char APPLY_FILTERS_SYMBOL[] = "apply_filters";
    static const char TEMPLATE_LOADER_FILENAME[] = "/wp-includes/template-loader.php";
    static size_t TEMPLATE_LOADER_FILENAME_LEN = strlen(TEMPLATE_LOADER_FILENAME);
    static const char TEMPLATE_INCLUDE_FILTER[] = "template_include";
    static const char TEMPLATE_PATH_SYMBOL[] = "TEMPLATEPATH";

    namespace
    {
        ApplyFiltersInterceptor applyFiltersInterceptor;

        class Payload : public IMatchPointPayload
        {
        public:
            Payload(const std::string& templateName,
                    const PHPExecEnvironment* execEnv)
                : m_templateName(templateName)
                , m_execEnv(execEnv)
            {
            }

            const std::string& getTemplateName() const
            {
                return m_templateName;
            }

            const PHPExecEnvironment* getExecEnvironment() const { return m_execEnv; }

        private:
            std::string const m_templateName;
            const PHPExecEnvironment* const m_execEnv;
        };

        class TransactionAcceptor : public ATransactionAcceptor
        {
        public:
            TransactionAcceptor(const boost::shared_ptr<MatchPointConfig>& config, TransactionMonitor* monitor)
                : ATransactionAcceptor(config, monitor)
            {
            }
            virtual bool matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& iPayload,
                                              const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition)
            {
                const boost::shared_ptr<Payload> payload = boost::static_pointer_cast<Payload>(iPayload);
                const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
                appdynamics::pb::Agent::EntryPointMatchCondition::WordpressMatchRule matchRule = matchCondition.wordpress();

                bool matches = true;
                if (matchRule.has_pagetemplatename())
                    matches = matches && configPtr->matchAndLogError(payload->getExecEnvironment(),
                                                                     &(matchRule.pagetemplatename()),
                                                                     payload->getTemplateName(),
                                                                     getLogger());
                return matches;
            }
            virtual bool customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                                const boost::shared_ptr<IMatchPointPayload>& iPayload)
            {
                *matchedTransaction = boost::shared_ptr<AgentTransaction>();
                const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
                const appdynamics::pb::Agent::MatchPointConfig* config = configPtr->get();

                const auto& customDefinitions = config->customdefinitions();

                int maxPriority = std::numeric_limits<int>::min();
                const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* prioritizedMatchRule = NULL;

                for (auto it = customDefinitions.begin(); it != customDefinitions.end(); ++it) {

                    if (!it->condition().has_wordpress())
                        continue;

                    LOG4CXX_TRACE(getLogger(), "Attempting to match custom match rule: " << it->btname());
                    bool matches = matchTransactionRule(iPayload, it->condition());

                    if (!matches)
                        continue;

                    LOG4CXX_DEBUG(getLogger(), "Potential match rule found: " << it->btname() << " Priority: " << it->priority() << " Current maximum matching priority: " << maxPriority);

                    if (it->priority() > maxPriority) {
                        prioritizedMatchRule = &(*it);
                        maxPriority = it->priority();
                    }
                }

                if (!prioritizedMatchRule)
                    return false;
                *matchedTransaction = getTransaction(prioritizedMatchRule->btname(), prioritizedMatchRule);
                 return true;
            }

            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType()
            {
                return appdynamics::pb::Agent::PHP_WORDPRESS;
            }

            virtual std::string getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& iPayload)
            {
                boost::shared_ptr<Payload> payload(boost::static_pointer_cast<Payload>(iPayload));
                const std::map<std::string, std::string>& properties = getConfig()->getDiscoveryProperties();
                return transformNameWithVirtualHost(payload->getTemplateName(),
                                                    &properties,
                                                    payload->getExecEnvironment());
            }
        };
    }

    ApplyFiltersInterceptor::ApplyFiltersInterceptor()
        : AEntryPointInterceptor("Wordpress3::ApplyFiltersInterceptor",
                                 InterceptorRegistry::Wordpress3ApplyFiltersInterceptor_ID)
    {
    }

    boost::shared_ptr<IMatchPointPayload> ApplyFiltersInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        // If we are not passed exactly two string parameters then this
        // is not the apply_filters call we are looking for:
        if (execEnv->getParamCount() != 2)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerString arg0(execEnv->getParam<ZValPointerString>(0));
        if (!arg0)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerString arg1(execEnv->getParam<ZValPointerString>(1));
        if (!arg1)
            return boost::shared_ptr<IMatchPointPayload>();

        // We need to check if the caller of apply_filters is
        // template-loader.php.  If template-loader.php is
        // not the caller, then we need to bail out of here.
        CallGraph::ExecStackFrame* thisCallFrame =
            execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        BOOST_ASSERT(thisCallFrame != NULL);

        CallGraph::ExecStackFrame* callerFrame =
            thisCallFrame->getCallerFrame();

        // If we don't have a caller frame then template-loader.php
        // is not calling us.
        if (!callerFrame)
            return boost::shared_ptr<IMatchPointPayload>();

        // template-loader.php is an included file, so if our caller was
        // not an included file then we know our caller is not template-loader.php
        if (callerFrame->getCallType() != CallGraph::CALL_INCLUDES)
            return boost::shared_ptr<IMatchPointPayload>();

        const CallGraph::NodeName& callerNodeName = callerFrame->getNodeName();

        // Check the method name of our caller to see if it
        // ends with /wp-includes/template-loader.php.

        const char* const callingFileName = callerNodeName.getMethodName();

        BOOST_ASSERT(callingFileName != NULL);
        size_t callingFileNameLen = strlen(callingFileName);
        if (callingFileNameLen < TEMPLATE_LOADER_FILENAME_LEN)
            return boost::shared_ptr<IMatchPointPayload>();

        size_t offsetOfTemplateLoaderFileName =
            callingFileNameLen - TEMPLATE_LOADER_FILENAME_LEN;
        if (strncmp(TEMPLATE_LOADER_FILENAME,
                    callingFileName + offsetOfTemplateLoaderFileName,
                    TEMPLATE_LOADER_FILENAME_LEN) != 0)
            return boost::shared_ptr<IMatchPointPayload>();

        // If the first string argument is not template_include, then
        // this is not the apply_filters call we are looking for.
        if (arg0 != TEMPLATE_INCLUDE_FILTER)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerAny templateBasePathZVal(ZValPointerAny::getConstant(execEnv,
                                                                        TEMPLATE_PATH_SYMBOL,
                                                                        strlen(TEMPLATE_PATH_SYMBOL)));
        if (!templateBasePathZVal)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerString templateBasePathStringZVal(templateBasePathZVal.cast<ZValPointerString>());
        if (!templateBasePathStringZVal)
            return boost::shared_ptr<IMatchPointPayload>();

        boost::filesystem::path templateBasePath(templateBasePathStringZVal.getStringValue());
        templateBasePath.remove_filename();
        boost::filesystem::path templatePath(arg1.getStringValue());

        boost::filesystem::path relativePath(naive_uncomplete(templatePath,
                                                              templateBasePath));

        // Now that we have named the BT, we can remove all interceptors from
        // apply-filters for the rest of the request, which will speed things up.
        zend_function* const zf = execEnv->lookupCallable(CallableInfo(APPLY_FILTERS_SYMBOL));

        if (zf && (zf->type != ZEND_INTERNAL_FUNCTION)) {
            zend_op_array* const opArray = &(zf->op_array);
            InterceptionEngine* interceptionEngine = execEnv->getInterceptionEngine();
            interceptionEngine->removeAllInterceptorsFromOpArray(opArray);
        }

        return boost::make_shared<Payload>(relativePath.native(), execEnv);
    }

    appdynamics::pb::Agent::EntryPointType ApplyFiltersInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_WORDPRESS;
    }

    bool EntryPointDelegate::s_didApplyRules = false;

    EntryPointDelegate::EntryPointDelegate(InterceptionEngine* interceptEngine,
                                           TransactionMonitor* txMonitor)
        : AEntryPointDelegate(interceptEngine, txMonitor)
    {
    }

    EntryPointDelegate::~EntryPointDelegate()
    {
    }

    void EntryPointDelegate::configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                       const struct _zend_agent_globals* agentGlobals)
    {
        const appdynamics::pb::Agent::MatchPointConfig* configProto = &(txConfig->wordpress());
        boost::shared_ptr<MatchPointConfig> config(boost::make_shared<MatchPointConfig>(txConfig,
                                                                                        configProto));
        AEntryPointDelegate::configure(config, agentGlobals);
    }

    void EntryPointDelegate::initAcceptor()
    {
        m_txAcceptor = boost::make_shared<TransactionAcceptor>(m_matchPointConfig,
                                                               m_txMonitor);
    }

    void EntryPointDelegate::applyRules(const struct _zend_agent_globals* agentGlobals)
    {
        if (s_didApplyRules)
            return;
        if (!agentGlobals->G->enabled_frameworks.wordpress3)
            return;
        s_didApplyRules = true;
        m_interceptEngine->addInterceptorForCallable(CallableInfo(APPLY_FILTERS_SYMBOL),
                                                     &applyFiltersInterceptor);
    }
}
