/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "drupal7.h"
#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include "emalloc_allocator.h"
#include "naming.h"
#include "transactions.h"
#include "reporting.h"
#include <limits>

static const char DRUPAL_MENU_EXECUTE_ACTIVE_HANDLER[] = "menu_execute_active_handler";
static const char CALL_USER_FUNC_ARRAY[] = "call_user_func_array";

static const char DRUPAL_PAGE_GET_CACHE[] = "drupal_page_get_cache";
static const char CACHE_GET[] = "cache_get";

static const char DRUPAL_PAGE_SET_CACHE[] = "drupal_page_set_cache";
static const char CACHE_SET[] = "cache_set";

static const char DRUPAL_DB_SCHEMA_SYMBOL[] = "DatabaseSchema_mysql";
static const char TABLE_EXISTS_METHOD[] = "tableExists";

static const char BT_KEY[] = "___APPD_BT_KEY";
static const char DATA_KEY[] = "data";

namespace Drupal7
{
    namespace
    {
        CallUserFuncArrayInterceptor callUserFuncArrayInterceptor;
        MenuExecuteActiveHandlerInterceptor menuExitActiveHandlerInterceptor;
        DBTableExistsInterceptor dbTableExistsInterceptor;
        DrupalPageGetCacheInterceptor drupalPageGetCacheInterceptor;
        CacheGetInterceptor cacheGetInterceptor;
        DrupalPageSetCacheInterceptor drupalPageSetCacheInterceptor;
        CacheSetInterceptor cacheSetInterceptor;

        class Payload : public IMatchPointPayload
        {
        public:
            Payload(const std::string& pageCallbackName,
                    const PHPExecEnvironment* execEnv)
                : m_pageCallbackName(pageCallbackName)
                , m_execEnv(execEnv)
            {
            }

            const std::string& getPageCallbackName() const
            {
                return m_pageCallbackName;
            }

            const PHPExecEnvironment* getExecEnvironment() const { return m_execEnv; }

        private:
            std::string const m_pageCallbackName;
            const PHPExecEnvironment* const m_execEnv;
        };

        class TransactionAcceptor : public ATransactionAcceptor
        {
        public:
            TransactionAcceptor(const boost::shared_ptr<MatchPointConfig>& config,
                                TransactionMonitor* monitor)
                : ATransactionAcceptor(config, monitor)
            {
            }

            virtual bool matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& iPayload,
                                              const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition)
            {
                const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
                appdynamics::pb::Agent::EntryPointMatchCondition::DrupalMatchRule matchRule = matchCondition.drupal();
                const boost::shared_ptr<Payload> payload = boost::static_pointer_cast<Payload>(iPayload);

                bool matches = true;
                if (matchRule.has_pagecallbackname())
                    matches = matches && configPtr->matchAndLogError(payload->getExecEnvironment(),
                                                                     &(matchRule.pagecallbackname()),
                                                                     payload->getPageCallbackName(),
                                                                     getLogger());
                return matches;
            }

            virtual bool customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                                const boost::shared_ptr<IMatchPointPayload>& iPayload)
            {
                *matchedTransaction = boost::shared_ptr<AgentTransaction>();
                const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
                const appdynamics::pb::Agent::MatchPointConfig* config = configPtr->get();

                const auto& customDefinitions = config->customdefinitions();

                int maxPriority = std::numeric_limits<int>::min();
                const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* prioritizedMatchRule = NULL;

                for (auto it = customDefinitions.begin(); it != customDefinitions.end(); ++it) {

                    if (!it->condition().has_drupal())
                        continue;

                    LOG4CXX_TRACE(getLogger(), "Attempting to match custom match rule: " << it->btname());
                    bool matches = matchTransactionRule(iPayload, it->condition());

                    if (!matches)
                        continue;

                    LOG4CXX_DEBUG(getLogger(), "Potential match rule found: " << it->btname() << " Priority: " << it->priority() << " Current maximum matching priority: " << maxPriority);

                    if (it->priority() > maxPriority) {
                        prioritizedMatchRule = &(*it);
                        maxPriority = it->priority();
                    }
                }

                if (!prioritizedMatchRule)
                    return false;
                *matchedTransaction = getTransaction(prioritizedMatchRule->btname(), prioritizedMatchRule);
                return true;
            }

            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType()
            {
                return appdynamics::pb::Agent::PHP_DRUPAL;
            }

            virtual std::string getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& iPayload)
            {
                boost::shared_ptr<Payload> payload(boost::static_pointer_cast<Payload>(iPayload));
                const std::map<std::string, std::string>& properties =
                    getConfig()->getDiscoveryProperties();
                return transformNameWithVirtualHost(payload->getPageCallbackName(),
                                                    &properties,
                                                    payload->getExecEnvironment());
            }
        };
    }


    MenuExecuteActiveHandlerInterceptor::MenuExecuteActiveHandlerInterceptor()
        : AMethodInterceptor("Drupal7::MenuExecuteActiveHandlerInterceptor",
                             InterceptorRegistry::Drupal7MenuExecuteActiveHandlerInterceptor_ID)
    {
    }

    void* MenuExecuteActiveHandlerInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->addInterceptorForCallable(CallableInfo(CALL_USER_FUNC_ARRAY),
                                                   &callUserFuncArrayInterceptor);
        return NULL;
    }

    void MenuExecuteActiveHandlerInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                            void* state)
    {
    }

    DrupalPageGetCacheInterceptor::DrupalPageGetCacheInterceptor()
        : AMethodInterceptor("Drupal7::RenderCacheGetInterceptor",
                             InterceptorRegistry::Drupal7DrupalPageGetCacheInterceptor_ID)
        , m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".DrupalPageGetCacheInterceptor"))
    {
    }

    void* DrupalPageGetCacheInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->addInterceptorForCallable(CallableInfo(CACHE_GET),
                                                   &cacheGetInterceptor);
        return NULL;
    }

    void DrupalPageGetCacheInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                      void* state)
    {
    }

    CacheGetInterceptor::CacheGetInterceptor()
        : AEntryPointInterceptor("Drupal7::CacheGetInterceptor",
          InterceptorRegistry::Drupal7CacheGetInterceptor_ID)
        , m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".CacheGetInterceptor"))
    {
    }

    void* CacheGetInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        return NULL;
    }

    void CacheGetInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                            void *state)
    {
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->removeInterceptorForCallable(CallableInfo(CACHE_GET),
                                                      this);
        AEntryPointInterceptor::onCallableBegin(phpExecEnv);
    }

    boost::shared_ptr<IMatchPointPayload> CacheGetInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        ZValPointerAny ret(ZValPointerAny::share(execEnv->getReturnValue()));
        ZValPointerObject obj = ret.cast<ZValPointerObject>();
        if (!obj) {
            return boost::shared_ptr<IMatchPointPayload>();
        }

        ZValPointerAny value;
        obj.readPropertyByName(execEnv, DATA_KEY,
                               sizeof(DATA_KEY) - 1, &value);
        ZValPointerArray data = value.cast<ZValPointerArray>();
        if (!data) {
            return boost::shared_ptr<IMatchPointPayload>();
        }

        ZValPointerAny tvalue;
        if (!data.findEntryByName(BT_KEY, sizeof(BT_KEY) - 1, &tvalue)) {
            return boost::shared_ptr<IMatchPointPayload>();
        }

        ZValPointerString callableName = tvalue.cast<ZValPointerString>();
        if(!callableName) {
            return boost::shared_ptr<IMatchPointPayload>();
        }
        data.deleteEntryByKey(BT_KEY, sizeof(BT_KEY) - 1);

        return boost::make_shared<Payload>(callableName.getStringValue(), execEnv);
    }

    appdynamics::pb::Agent::EntryPointType CacheGetInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_DRUPAL;
    }

    DrupalPageSetCacheInterceptor::DrupalPageSetCacheInterceptor()
        : AMethodInterceptor("Drupal7::RenderCacheSetInterceptor",
                             InterceptorRegistry::Drupal7DrupalPageSetCacheInterceptor_ID)
        , m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".DrupalPageSetCacheInterceptor"))
    {
    }

    void* DrupalPageSetCacheInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->addInterceptorForCallable(CallableInfo(CACHE_SET),
                                                   &cacheSetInterceptor);
        return NULL;
    }

    void DrupalPageSetCacheInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                      void* state)
    {
    }

    CacheSetInterceptor::CacheSetInterceptor()
        : AMethodInterceptor("Drupal7::CacheSetInterceptor",
          InterceptorRegistry::Drupal7CacheSetInterceptor_ID)
        , m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".CacheSetInterceptor"))
    {
    }

    void* CacheSetInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        boost::shared_ptr<TransactionMonitor> txMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor();
        TransactionContext* existingContext = txMonitor->getTransactionContext();

        if (!existingContext || (existingContext->getEntryPointType() != appdynamics::pb::Agent::PHP_DRUPAL)) {
            return NULL;
        }

        CallGraph::ExecStackFrame* thisCallFrame =
            phpExecEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        BOOST_ASSERT(thisCallFrame != NULL);

        CallGraph::ExecStackFrame* callerFrame = thisCallFrame->getCallerFrame();

        /* We need to check if this function was invoked by the
         * drupal_page_set_cache function, otherwise we simply return
         */
        if (!callerFrame) {
            return NULL;
        }

        const CallGraph::NodeName& callerNodeName = callerFrame->getNodeName();

        // If our caller is a method of a class then it is
        // not drupal_page_set_cache.
        if (callerNodeName.getClassName() != NULL) {
            return NULL;
        }

        // Check the method name of our caller to see if it is
        // drupal_page_set_cache.
        BOOST_ASSERT(callerNodeName.getMethodName() != NULL);
        if (strcmp(callerNodeName.getMethodName(), DRUPAL_PAGE_SET_CACHE) != 0) {
            return NULL;
        }

        if (phpExecEnv->getParamCount() != 4) {
            return NULL;
        }

        ZValPointerAny resp(phpExecEnv->getParam(1));
        ZValPointerArray data = resp.cast<ZValPointerArray>();
        if (!data) {
            return NULL;
        }

        boost::shared_ptr<Payload> payload = boost::dynamic_pointer_cast<Payload>(
                                             existingContext->getTransactionPayload());

        // Inject the Transaction name in the cache payload, so that we can retrieve
        // it the next time the cache is hit
        ZValPointerString btName = ZValPointerString::copy(payload->getPageCallbackName());
        data.putEntryByName(BT_KEY, sizeof(BT_KEY) - 1, btName);

        // Remove the interceptor, it will be readded the next
        // time drupal_page_set_cache is invoked
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->removeInterceptorForCallable(CallableInfo(CACHE_SET),
                                                      this);
        return NULL;
    }

    void CacheSetInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                             void *state)
    {
    }

    CallUserFuncArrayInterceptor::CallUserFuncArrayInterceptor()
        : AEntryPointInterceptor("Drupal7::CallUserFuncArrayInterceptor",
          InterceptorRegistry::Drupal7CallUserFuncArrayInterceptor_ID)
    {
    }

    void* CallUserFuncArrayInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        // We need to check if the caller of call_user_func_array is
        // menu_execute_active_handler.  If menu_execute_active_handler is
        // not the caller, then we need to bail out of here.
        CallGraph::ExecStackFrame* thisCallFrame =
            phpExecEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        BOOST_ASSERT(thisCallFrame != NULL);

        CallGraph::ExecStackFrame* callerFrame =
            thisCallFrame->getCallerFrame();

        // If we don't have a caller frame then menu_execute_active_handler
        // is not calling us.
        if (!callerFrame)
            return NULL;

        const CallGraph::NodeName& callerNodeName = callerFrame->getNodeName();

        // If our caller is a method of a class then it is
        // not menu_execute_active_handler.
        if (callerNodeName.getClassName() != NULL)
            return NULL;

        // Check the method name of our caller to see if it is
        // menu_execute_active_handler.
        BOOST_ASSERT(callerNodeName.getMethodName() != NULL);
        if (strcmp(callerNodeName.getMethodName(), DRUPAL_MENU_EXECUTE_ACTIVE_HANDLER) != 0)
            return NULL;

        // Remove the interceptor because we don't want to incur overhead
        // on every call to call_user_func_array.
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->removeInterceptorForCallable(CallableInfo(CALL_USER_FUNC_ARRAY),
                                                      this);

        // Delegate to the base class to do the real work once
        // we figure out that this is the correct call
        // to call_user_func_array.
        return AEntryPointInterceptor::onCallableBegin(phpExecEnv);
    }

    boost::shared_ptr<IMatchPointPayload> CallUserFuncArrayInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        if (execEnv->getParamCount() != 2)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerAny param0(execEnv->getParam(0));
        std::string callableName;
        if (!execEnv->getCallableName(param0, &callableName))
            return boost::shared_ptr<IMatchPointPayload>();
        return boost::make_shared<Payload>(callableName, execEnv);
    }

    appdynamics::pb::Agent::EntryPointType CallUserFuncArrayInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_DRUPAL;
    }

    DBTableExistsInterceptor::DBTableExistsInterceptor()
        : AMethodInterceptor("Drupal7::DBTableExistsInterceptor",
                InterceptorRegistry::Drupal7DBTableExistsInterceptor_ID)
    {
    }

    void* DBTableExistsInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        ErrorMonitor* errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->addToErrorSuppressionDepth(1);
        return NULL;
    }

    void DBTableExistsInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                 void* state)
    {
        ErrorMonitor* errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->addToErrorSuppressionDepth(-1);
    }

    bool DBTableExistsInterceptor::isActive(const PHPExecEnvironment* execEnv) const
    {
        const AEntryPointDelegate* const delegate =
        AG(kernel)->getAgentContext()
                  ->getTransactionMonitor()
                  ->getEntryPointDelegate(appdynamics::pb::Agent::PHP_DRUPAL);
        return (delegate && !delegate->isDetectionDisabled());
    }

    bool EntryPointDelegate::s_didApplyRules = false;

    EntryPointDelegate::EntryPointDelegate(InterceptionEngine* interceptEngine,
                                           TransactionMonitor* txMonitor)
        : AEntryPointDelegate(interceptEngine, txMonitor)
    {
    }

    EntryPointDelegate::~EntryPointDelegate()
    {
    }

    void EntryPointDelegate::configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                       const struct _zend_agent_globals* agentGlobals)
    {
        const appdynamics::pb::Agent::MatchPointConfig* configProto = &(txConfig->drupal());
        boost::shared_ptr<MatchPointConfig> config(boost::make_shared<MatchPointConfig>(txConfig,
                                                                                        configProto));
        AEntryPointDelegate::configure(config, agentGlobals);
    }


    void EntryPointDelegate::initAcceptor()
    {
        m_txAcceptor = boost::make_shared<TransactionAcceptor>(m_matchPointConfig,
                                                               m_txMonitor);
    }

    void EntryPointDelegate::applyRules(const struct _zend_agent_globals* agentGlobals)
    {
        if (s_didApplyRules)
            return;
        if (!agentGlobals->G->enabled_frameworks.drupal7)
            return;
        s_didApplyRules = true;
        m_interceptEngine->addInterceptorForCallable(CallableInfo(DRUPAL_MENU_EXECUTE_ACTIVE_HANDLER),
                                                     &menuExitActiveHandlerInterceptor);
        m_interceptEngine->addInterceptorForCallable(CallableInfo(DRUPAL_DB_SCHEMA_SYMBOL, TABLE_EXISTS_METHOD),
                                                     &dbTableExistsInterceptor);
        m_interceptEngine->addInterceptorForCallable(CallableInfo(DRUPAL_PAGE_GET_CACHE),
                                                     &drupalPageGetCacheInterceptor);
        m_interceptEngine->addInterceptorForCallable(CallableInfo(DRUPAL_PAGE_SET_CACHE),
                                                     &drupalPageSetCacheInterceptor);
    }
}
