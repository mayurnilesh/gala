#include "frameworks/frameworks.h"
#include "frameworks/detector.h"
#include "transactions.h"
#include "entrypoint.h"
#include "PHPAgentProtobufs.pb.h"

// Macro to automatically specify the string length for
// the DetectionPoint struct
#define ADSTR(x) x, sizeof(x)-1

namespace FrameworkDetector {

enum DetectionType
{
    DETECT_CLASS,
    DETECT_FUNCTION
};

struct DetectionPoint
{
    Framework framework;
    appdynamics::pb::Agent::EntryPointType entryPointType;
    enum DetectionType type;
    const char* const symbol;
    size_t symbolLength;
    const char* const filename;
    size_t filenameLength;
};

namespace
{
    using namespace appdynamics::pb::Agent;
// Contains the list of properties by which we detect whether a framework is
// loaded. Classes are checked against EG(class_table) and functions against
// EG(function_table). Each type can have an optional filename check to ensure
// that it is the framework we're looking for.
static const DetectionPoint DETECTION_POINTS[] = {
    { Framework_zend1,        PHP_MVC,        DETECT_CLASS,    ADSTR("zend_application"),            NULL, 0 },
    { Framework_zend1,        PHP_MVC,        DETECT_CLASS,    ADSTR("mage_core_model_app"),         NULL, 0 },
    { Framework_zend2,        PHP_MVC,        DETECT_CLASS,    ADSTR("zend\\mvc\\application"),      NULL, 0 },
    { Framework_symfony1,     PHP_MVC,        DETECT_CLASS,    ADSTR("sfcontext"),                   NULL, 0 },
    { Framework_symfony2,     PHP_MVC,        DETECT_CLASS,    ADSTR("\\symfony\\component\\httpkernel\\kernel"), NULL, 0 },
    { Framework_codeigniter,  PHP_MVC,        DETECT_CLASS,    ADSTR("ci_router"),                   NULL, 0 },
    { Framework_fuelphp,      PHP_MVC,        DETECT_CLASS,    ADSTR("\\fuel\\core\\fuel"),          NULL, 0 },
    { Framework_drupal8,      PHP_MVC,        DETECT_CLASS,    ADSTR("\\drupal\\core\\drupalkernel"), NULL, 0 },
    // CakePHP
    { Framework_cakephp,      PHP_MVC,        DETECT_FUNCTION, ADSTR("__n"),                         ADSTR("Cake/basics.php")  },
    { Framework_wordpress3,   PHP_WORDPRESS,  DETECT_FUNCTION, ADSTR("wp_fix_server_vars"),          ADSTR("wp-includes/load.php") },
    { Framework_drupal7,      PHP_DRUPAL,     DETECT_FUNCTION, ADSTR("menu_execute_active_handler"), NULL, 0 }
};

}

static const size_t DETECTION_POINTS_COUNT =
    sizeof(DETECTION_POINTS) / sizeof(DETECTION_POINTS[0]);

static inline bool checkFilename(const char* detectFilename,
                                 size_t detectFilenameLength,
                                 const char* entryFilename)
{
    // Check if the entryFilename ends in detectFilename
    size_t entryFilenameLength = strlen(entryFilename);
    if (entryFilenameLength < detectFilenameLength)
        return false;

    size_t filenameOffset = entryFilenameLength - detectFilenameLength;
    if (strncmp(detectFilename, entryFilename + filenameOffset, detectFilenameLength) != 0)
        return false;

    return true;
}

static inline const char* getClassEntryFilename(const zend_class_entry* classEntry)
{
#if PHP_VERSION_ID < 50400
    return classEntry->filename;
#else
    return classEntry->info.user.filename ? ZSTR_VAL(classEntry->info.user.filename) : NULL;
#endif
}

bool isFrameworkLoaded(const PHPExecEnvironment* execEnv)
{
    zend_class_entry* classEntry = NULL;
    zend_function* functionEntry = NULL;
    bool symbolMatch = false;

    const CXXGlobals& globals = execEnv->getAgentGlobals();
    const FrameworksFlags& enabledFrameworks = globals.enabled_frameworks;

    if (!enabledFrameworks.anySet())
        return false;

    for (int i = 0; i < DETECTION_POINTS_COUNT; i++) {
        if (!enabledFrameworks.isSet(DETECTION_POINTS[i].framework))
            continue;

        auto entryPointDelegate =
            AG(kernel)->getAgentContext()
                      ->getTransactionMonitor()
                      ->getEntryPointDelegate(DETECTION_POINTS[i].entryPointType);
        if ((!entryPointDelegate) || (entryPointDelegate->isDetectionDisabled())) {
            continue;
        }

        switch (DETECTION_POINTS[i].type) {
            case DETECT_CLASS:
                classEntry = execEnv->lookupClass(DETECTION_POINTS[i].symbol, DETECTION_POINTS[i].symbolLength);
                if (classEntry) {
                    if (DETECTION_POINTS[i].filename)
                        return checkFilename(DETECTION_POINTS[i].filename, DETECTION_POINTS[i].filenameLength,
#if PHP_VERSION_ID < 50400
                                classEntry->filename
#else
                                classEntry->info.user.filename ? ZSTR_VAL(classEntry->info.user.filename) : NULL
#endif
                               );
                    return true;
                }
                break;

            case DETECT_FUNCTION:
                functionEntry = execEnv->lookupFunction(DETECTION_POINTS[i].symbol, DETECTION_POINTS[i].symbolLength);
                if (functionEntry) {
                    if (DETECTION_POINTS[i].filename)
                        return checkFilename(DETECTION_POINTS[i].filename,
                            DETECTION_POINTS[i].filenameLength,
                            functionEntry->op_array.filename ? ZSTR_VAL(functionEntry->op_array.filename) : NULL);
                    return true;
                }
                break;

            default:
                BOOST_ASSERT_MSG(false, "Unrecognized detection point type");
                // should never get here
                return false;
        }
    }

    return false;
}

}
