/*
   Copyright 2016 AppDynamics.
   All rights reserved.
*/

#include "drupal8.h"
#include "symfony2.h"
#include "frameworks/mvc.h"
#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include "emalloc_allocator.h"
#include "naming.h"
#include "transactions.h"
#include "reporting.h"
#include <limits>
#include <boost/algorithm/string.hpp>

namespace Drupal8
{
    namespace
    {
        static const char btKeyController[] = "___APPD_BT_KEY_CONTROLLER";
        static const char btKeyAction[] = "___APPD_BT_KEY_ACTION";

        void setBTBaseName(const PHPExecEnvironment* phpExecEnv,
                           ZValPointerObject& resp,
                           boost::shared_ptr<MVCPayload>& payload)
        {
            const std::string& controllerName = payload->getController();
            if (!controllerName.empty()) {
                ZValPointerString cValue = ZValPointerString::copy(controllerName);
                resp.writePropertyByName(phpExecEnv, btKeyController,
                                    sizeof(btKeyController) - 1, cValue);
            }

            const std::string& actionName = payload->getAction();
            if (!actionName.empty()) {
                ZValPointerString aValue = ZValPointerString::copy(actionName);
                resp.writePropertyByName(phpExecEnv, btKeyAction,
                                  sizeof(btKeyAction) - 1, aValue);
            }
        }

        bool deletePropertyByName(zval* object,
                                  const char* name,
                                  size_t nameLenInBytes)
        {
            TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
            if (Z_TYPE_P(object) != IS_OBJECT) {
                return false;
            }

            HashTable* obj_hash = (Z_OBJPROP_P(object));
#if PHP_VERSION_ID >= 70000
            int res  = zend_hash_str_del(obj_hash, const_cast<char*>(name), nameLenInBytes + 1);
#else
            int res  = zend_hash_del(obj_hash, const_cast<char*>(name), nameLenInBytes + 1);
#endif
            if (res != SUCCESS) {
                return false;
            }

            return true;
        }
    }

    PageCacheSetInterceptor::PageCacheSetInterceptor()
        : AMethodInterceptor("Drupal8::PageCacheSetInterceptor",
                             InterceptorRegistry::Drupal8PageCacheSetInterceptor_ID)
    {
    }

    void* PageCacheSetInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        if (phpExecEnv->getParamCount() != 4) {
            return NULL;
        }

        ZValPointerAny resp(phpExecEnv->getParam(1));
        ZValPointerObject resp_obj = resp.cast<ZValPointerObject>();
        if(!resp_obj) {
            return NULL;
        }

        boost::shared_ptr<TransactionMonitor> txMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor();
        TransactionContext* existingContext = txMonitor->getTransactionContext();

        if (!existingContext || (existingContext->getEntryPointType() != appdynamics::pb::Agent::PHP_MVC)) {
            return NULL;
        }

        boost::shared_ptr<MVCPayload> payload = boost::dynamic_pointer_cast<MVCPayload>(existingContext->getTransactionPayload());
        if (!payload) {
            return NULL;
        }

        setBTBaseName(phpExecEnv, resp_obj, payload);
        return NULL;
    }

    void PageCacheSetInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                            void* state)
    {
    }

    PageCacheGetInterceptor::PageCacheGetInterceptor()
        : AEntryPointInterceptor("Drupal8::PageCacheGetInterceptor", InterceptorRegistry::Drupal8PageCacheGetInterceptor_ID)
        , m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".PageCacheGetInterceptor"))
    {
    }

    void* PageCacheGetInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        return NULL;
    }

    void PageCacheGetInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                            void* state)
    {
        AEntryPointInterceptor::onCallableBegin(phpExecEnv);
    }

    boost::shared_ptr<IMatchPointPayload> PageCacheGetInterceptor::createPayload(const PHPExecEnvironment* const execEnv)
    {
        ZValPointerAny ret(ZValPointerAny::share(execEnv->getReturnValue()));
        ZValPointerObject resp = ret.cast<ZValPointerObject>();
        if (!resp) {
           return boost::shared_ptr<IMatchPointPayload>();
        }

        ZValPointerAny cValue;
        if (!resp.readPropertyByName(execEnv, btKeyController,
                        sizeof(btKeyController) - 1, &cValue)) {
           return boost::shared_ptr<IMatchPointPayload>();
        }
        ZValPointerString controllerName = cValue.cast<ZValPointerString>();
        if (!controllerName) {
           return boost::shared_ptr<IMatchPointPayload>();
        }

        ZValPointerAny aValue;
        if (!resp.readPropertyByName(execEnv, btKeyAction,
                        sizeof(btKeyAction) - 1, &aValue)) {
           return boost::shared_ptr<IMatchPointPayload>();
        }
        ZValPointerString actionName = aValue.cast<ZValPointerString>();
        if (!actionName) {
           return boost::shared_ptr<IMatchPointPayload>();
        }

        deletePropertyByName(ret.get(), btKeyController, sizeof(btKeyController) - 1);
        deletePropertyByName(ret.get(), btKeyAction, sizeof(btKeyAction) - 1);

        return boost::make_shared<MVCPayload,
                                  const std::string&,
                                  const std::string&,
                                  const std::string&,
                                  const PHPExecEnvironment* const&>(std::string(),
                                                                    controllerName.getStringValue(),
                                                                    actionName.getStringValue(),
                                                                    execEnv);
    }

    appdynamics::pb::Agent::EntryPointType PageCacheGetInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }
}
