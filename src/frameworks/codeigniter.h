/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#ifndef __codeigniter_h
#define __codeigniter_h

#include "entrypoint.h"

namespace CodeIgniter
{
    // Forward declaration of the interceptor used to intercept calls
    // to the controller/method from the CodeIgniter route
    class HandlerInterceptor;

    /*
     * Interceptor used to detect the routing decisions in
     * Router::_set_routing() and Router::_set_overrides() and install a
     * HandlerInterceptor on what will be the actual entry point.
     */
    class SetRoutingInterceptor : public AMethodInterceptor
    {
        public:
            SetRoutingInterceptor();

            virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

            virtual void  onCallableEnd(const PHPExecEnvironment* execEnv, void* state);

            virtual bool needParamsInOnMethodBegin() const { return false; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return true; }

            bool isActive(const PHPExecEnvironment* execEnv) const;
    };

    class HandlerInterceptor : public AEntryPointInterceptor
    {
        public:
            HandlerInterceptor();
            virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
            virtual bool isTxToBeIdentified(const PHPExecEnvironment *execEnv) const;
            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

            virtual bool needParamsInOnMethodBegin() const { return false; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return false; }
    };
}

extern CodeIgniter::HandlerInterceptor ciHandlerInterceptor;

#endif // __codeigniter_h
