/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#include "symfony1.h"
#include "intercept.h"
#include "zval_helper.h"
#include "frameworks/mvc.h"
#include <zend.h>
#include "emalloc_allocator.h"

Symfony1EntryPointInterceptor::Symfony1EntryPointInterceptor()
    : AEntryPointInterceptor("Symfony1EntryPointInterceptor",
                             InterceptorRegistry::Symfony1EntryPointInterceptor_ID)
{
}

Symfony1EntryPointInterceptor::~Symfony1EntryPointInterceptor()
{
}

boost::shared_ptr<IMatchPointPayload> Symfony1EntryPointInterceptor::createPayload(const PHPExecEnvironment* execEnv)
{
    if (execEnv->getParamCount() != 2)
        return boost::shared_ptr<IMatchPointPayload>();

    ZValPointerString moduleNameZVal(execEnv->getParam<ZValPointerString>(0));
    if (!moduleNameZVal)
        return boost::shared_ptr<IMatchPointPayload>();
    std::string moduleName(moduleNameZVal.getStringValue());

    ZValPointerString actionNameZVal(execEnv->getParam<ZValPointerString>(1));
    if (!actionNameZVal)
        return boost::shared_ptr<IMatchPointPayload>();
    std::string actionName(actionNameZVal.getStringValue());

    return boost::make_shared<MVCPayload>(moduleName, std::string(), actionName, execEnv);
}

appdynamics::pb::Agent::EntryPointType Symfony1EntryPointInterceptor::getEntryPointType() const
{
    return appdynamics::pb::Agent::PHP_MVC;
}

static const char SF_STOP_EXCEPTION_CLASS_NAME[] = "sfStopException";
static const char SF_STOP_EXCEPTION_CLASS_NAME_LEN = sizeof(SF_STOP_EXCEPTION_CLASS_NAME) - 1;

static inline bool haveSFStopException(const PHPExecEnvironment* phpExecEnv)
{
    zval* exceptionObjectRawZVal = phpExecEnv->getExceptionObject();
    if (!exceptionObjectRawZVal)
        return false;

    ZValPointerObject exceptionObject(ZValPointerAny::share(exceptionObjectRawZVal).cast<ZValPointerObject>());
    if (!exceptionObject)
        return false;

    zend_class_entry* const sfStopExceptionClass =
        phpExecEnv->lookupClass(SF_STOP_EXCEPTION_CLASS_NAME,
                                SF_STOP_EXCEPTION_CLASS_NAME_LEN);
    // If the sfStopException class not yet exist, then the current
    // exception can't possibly be a instance of sfStopException.
    if (!sfStopExceptionClass)
        return false;

    bool const isSfStopException = exceptionObject.isInstanceOf(phpExecEnv,
                                                                sfStopExceptionClass);
    return isSfStopException;
}

void Symfony1EntryPointInterceptor::detectErrors(const PHPExecEnvironment* phpExecEnv,
                                                 void* state)
{
    if (haveSFStopException(phpExecEnv))
        return;
    AEntryPointInterceptor::detectErrors(phpExecEnv, state);
}

