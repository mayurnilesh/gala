/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#ifndef __zend1_h
#define __zend1_h

#include "entrypoint.h"

/**
    Interceptor used to name business transactions that are dispatched
    by the Zend1 framework.
 */
class Zend1EntryPointInterceptor : public AEntryPointInterceptor {
    public:
        Zend1EntryPointInterceptor();
        ~Zend1EntryPointInterceptor();
        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }
};

/**
    Interceptor used for calls to _redirect() and redirect().
    The BT will be named "<module> : <controller> : [redirect]"
    uniformally
 */
class Zend1RedirectInterceptor : public AEntryPointInterceptor {
    public:
        Zend1RedirectInterceptor();
        ~Zend1RedirectInterceptor();
        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }
};

#endif
