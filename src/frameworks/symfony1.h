/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#ifndef __symfony1_h
#define __symfony1_h

#include "entrypoint.h"

/**
    Interceptor used to name business transactions that are dispatched
    by version 1.X of the Symfony( http://symfony.com/legacy ) framework.
 */
class Symfony1EntryPointInterceptor : public AEntryPointInterceptor {
    public:
        Symfony1EntryPointInterceptor();
        ~Symfony1EntryPointInterceptor();
        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        void detectErrors(const PHPExecEnvironment* phpExecEnv, void* state);

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }
};

#endif
