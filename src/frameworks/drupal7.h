/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __drupal7_h
#define __drupal7_h

#include "entrypoint.h"

namespace Drupal7
{
    class CallUserFuncArrayInterceptor;

    /**
        Interceptor for intercepting calls to
        menu_execute_active_handler.
        <p>
        This interceptor adds the CallUserFuncArrayInterceptor below to the
        call_user_func_array function.
     */
    class MenuExecuteActiveHandlerInterceptor : public AMethodInterceptor
    {
    public:
        MenuExecuteActiveHandlerInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return false; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        // TODO could be optimized by checking if the entry point delegate is enabled
        // but since all it does is add the CallUserFuncArrayInterceptor, it's okay for now
        bool isActive(const PHPExecEnvironment* execEnv) const { return true; }
    };

    /**
        Entry point interceptor for the call_user_func call in drupal's
        menu_execute_active_handler function.
        <p>
        This interceptor removes itself from call_user_func when its
        onCallableBegin method is called and the calling php function
        is menu_execute_active_handler.
     */
    class CallUserFuncArrayInterceptor : public AEntryPointInterceptor
    {
    public:
        CallUserFuncArrayInterceptor();
        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);

        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return true; }
    };

    /**
        Interceptor for intercepting calls to
        drupal_page_set_cache.
        <p>
        This interceptor adds the CacheSetInterceptor below to the
        cache_set function.
     */
    class DrupalPageSetCacheInterceptor : public AMethodInterceptor
    {
    public:
        DrupalPageSetCacheInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);
        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return false; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const { return true; }
    private:
        AgentLogger m_logger;
    };

    /**
        Entry point interceptor for the cache_set call in drupal's
        drupal_page_set_cache function.
        <p>
        This interceptor removes itself from cache_set when its
        onCallableEnd method is called
     */
    class CacheSetInterceptor : public AMethodInterceptor
    {
    public:
        CacheSetInterceptor();
        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);
        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const { return true; }
    private:
        AgentLogger m_logger;
    };

    /**
        Interceptor for intercepting calls to
        drupal_page_get_cache.
        <p>
        This interceptor adds the CacheGetInterceptor below to the
        cache_get function.
     */
    class DrupalPageGetCacheInterceptor : public AMethodInterceptor
    {
    public:
        DrupalPageGetCacheInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);
        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return false; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const { return true; }
    private:
        AgentLogger m_logger;
    };

    /**
        Entry point interceptor for the cache_get call in drupal's
        drupal_page_get_cache function.
        <p>
        This interceptor removes itself from cache_get when its
        onCallableEnd method is called
     */
    class CacheGetInterceptor : public AEntryPointInterceptor
    {
    public:
        CacheGetInterceptor();
        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);
        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);

        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }
        virtual bool shouldCallOnMethodEnd() const { return true; }
    private:
        AgentLogger m_logger;
    };

    /*
     * Interceptor that is used to suppress errors that happen in its scope when
     * Drupal tries to check if a table exists.
     */
    class DBTableExistsInterceptor : public AMethodInterceptor
    {
        public:
            DBTableExistsInterceptor();

            virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);

            virtual inline bool needParamsInOnMethodBegin() const { return false; }
            virtual inline bool shouldCallOnMethodEnd() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }

            bool isActive(const PHPExecEnvironment* execEnv) const;
    };

    /**
        Delegate that applies interception rules to detect business
        transactions in Drupa7.
     */
    class EntryPointDelegate : public AEntryPointDelegate
    {
        public:
            EntryPointDelegate(InterceptionEngine* interceptEngine,
                               TransactionMonitor* txMonitor);
            virtual ~EntryPointDelegate();
        protected:
            virtual void configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                   const struct _zend_agent_globals* agentGlobals);
            virtual void initAcceptor();
            virtual void applyRules(const struct _zend_agent_globals* agentGlobals);
        private:
            static bool s_didApplyRules;
    };


}

#endif
