/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#ifndef __frameworks_table_h
#define __frameworks_table_h

/**
    Defines a table of frameworks supported by the agent.
 */
#define FRAMEWORKS_TABLE(XX)        \
    XX(cakephp)                     \
    XX(codeigniter)                 \
    XX(drupal7)                     \
    XX(drupal8)                     \
    XX(fuelphp)                     \
    XX(predis)                      \
    XX(symfony1)                    \
    XX(symfony2)                    \
    XX(wordpress3)                  \
    XX(zend1)                       \
    XX(zend2)

#endif
