/*
   Copyright 2016 AppDynamics.
   All rights reserved.
*/

#ifndef __drupal8_h
#define __drupal8_h

#include "entrypoint.h"

namespace Drupal8
{
    /**
        Interceptor for intercepting calls to PageCache::set.
     */
    class PageCacheSetInterceptor : public AMethodInterceptor
    {
    public:
        PageCacheSetInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const { return true; }
    };

    /**
        Interceptor for intercepting calls to PageCache::get.
     */
    class PageCacheGetInterceptor : public AEntryPointInterceptor
    {
    public:
        PageCacheGetInterceptor();

        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

        virtual bool needParamsInOnMethodBegin() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }
        virtual bool shouldCallOnMethodEnd() const { return true; }
    private:
        AgentLogger m_logger;
    };
}

#endif
