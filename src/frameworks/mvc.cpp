/*
    Copyright 2012 AppDynamics
    All rights reserved.
 */
#include "mvc.h"
#include "symfony1.h"
#include "symfony2.h"
#include "zend1.h"
#include "zend2.h"
#include "codeigniter.h"
#include "fuelphp.h"
#include "cakephp.h"
#include "drupal8.h"
#include "naming.h"
#include "agent.h"

#include <boost/algorithm/string/join.hpp>
#include <limits>

struct MVCDispatchMethod
{
    const char* const m_className;
    const char* const m_methodName;
};

Symfony1EntryPointInterceptor symfony1Interceptor;
Symfony2::HandlerInterceptor symfony2HandlerInterceptor;
Symfony2::GetArgumentsInterceptor symfony2GetArgumentsInterceptor;
Zend1EntryPointInterceptor zend1Interceptor;
Zend1RedirectInterceptor zend1RedirectInterceptor;
CodeIgniter::HandlerInterceptor ciHandlerInterceptor;
CodeIgniter::SetRoutingInterceptor ciSetRoutingInterceptor;
CakePHP::ControllerInvokeActionInterceptor cakeControllerInterceptor;
Drupal8::PageCacheSetInterceptor drupal8PageCacheSetInterceptor;
Drupal8::PageCacheGetInterceptor drupal8PageCacheGetInterceptor;

#if PHP_VERSION_ID >= 50300
FuelPHP::RequestExecuteInterceptor fuelRequestInterceptor;
zend2::ActionControllerEntryPointInterceptor zend2ActionOnDispatchInterceptor;
zend2::RestfulControllerEntryPointInterceptor zend2RestfulOnDispatchInterceptor;
#endif

bool MVCEntryPointDelegate::s_didApplyRules = false;

static const MVCDispatchMethod SYMFONY1_WEB_CONTROLLER_METHODS[] = {
    { "sfWebController", "forward" },
    { "sfFrontWebController", "forward" }
};

static const size_t SYMFONY1_WEB_CONTROLLER_METHODS_COUNT =
    sizeof(SYMFONY1_WEB_CONTROLLER_METHODS) / sizeof(SYMFONY1_WEB_CONTROLLER_METHODS[0]);

static const MVCDispatchMethod DRUPAL8_PAGE_CACHE_GET_METHODS[] = {
        { "Drupal\\page_cache\\StackMiddleware\\PageCache", "get" }
    };

static const size_t DRUPAL8_PAGE_CACHE_GET_METHODS_COUNT =
    sizeof(DRUPAL8_PAGE_CACHE_GET_METHODS) / sizeof(DRUPAL8_PAGE_CACHE_GET_METHODS[0]);

static const MVCDispatchMethod DRUPAL8_PAGE_CACHE_SET_METHODS[] = {
        { "Drupal\\page_cache\\StackMiddleware\\PageCache", "set" }
    };

static const size_t DRUPAL8_PAGE_CACHE_SET_METHODS_COUNT =
    sizeof(DRUPAL8_PAGE_CACHE_SET_METHODS) / sizeof(DRUPAL8_PAGE_CACHE_SET_METHODS[0]);

static const MVCDispatchMethod SYMFONY2_CONTROLLER_RESOLVER_METHODS[] = {
    { "Symfony\\Component\\HttpKernel\\Controller\\ControllerResolver", "getArguments" },
    { "Symfony\\Bundle\\FrameworkBundle\\Controller\\ControllerResolver", "getArguments" },
    { "JMS\\DiExtraBundle\\HttpKernel\\ControllerResolver", "getArguments" }
};

static const size_t SYMFONY2_CONTROLLER_RESOLVER_METHODS_COUNT =
    sizeof(SYMFONY2_CONTROLLER_RESOLVER_METHODS) / sizeof(SYMFONY2_CONTROLLER_RESOLVER_METHODS[0]);

static const MVCDispatchMethod ZEND1_CONTROLLER_REDIRECT_METHODS[] = {
    { "Zend_Controller_Action", "_redirect" },
    { "Zend_Controller_Action", "redirect" }
};

static const size_t ZEND1_CONTROLLER_REDIRECT_METHODS_COUNT =
    sizeof(ZEND1_CONTROLLER_REDIRECT_METHODS) / sizeof(ZEND1_CONTROLLER_REDIRECT_METHODS[0]);

static const MVCDispatchMethod ZEND1_CONTROLLER_ACTION_METHODS[] = {
    { "Zend_Controller_Action", "dispatch" },
    { "Mage_Core_Controller_Varien_Action", "dispatch" }
};

static const size_t ZEND1_CONTROLLER_ACTION_METHODS_COUNT =
    sizeof(ZEND1_CONTROLLER_ACTION_METHODS) / sizeof(ZEND1_CONTROLLER_ACTION_METHODS[0]);

static const MVCDispatchMethod ZEND2_ACTION_CONTROLLER_ONDISPATCH_METHODS[] = {
    { "Zend\\Mvc\\Controller\\AbstractActionController", "onDispatch" }
};

static const size_t ZEND2_ACTION_CONTROLLER_ONDISPATCH_METHODS_COUNT =
    sizeof(ZEND2_ACTION_CONTROLLER_ONDISPATCH_METHODS) / sizeof(ZEND2_ACTION_CONTROLLER_ONDISPATCH_METHODS[0]);

static const MVCDispatchMethod ZEND2_RESTFUL_CONTROLLER_ONDISPATCH_METHODS[] = {
    { "Zend\\Mvc\\Controller\\AbstractRestfulController", "onDispatch"},
};

static const size_t ZEND2_RESTFUL_CONTROLLER_ONDISPATCH_METHODS_COUNT =
    sizeof(ZEND2_RESTFUL_CONTROLLER_ONDISPATCH_METHODS) / sizeof(ZEND2_RESTFUL_CONTROLLER_ONDISPATCH_METHODS[0]);

static const MVCDispatchMethod CODEIGNITER_ROUTER_METHODS[] = {
    { "CI_Router", "_set_routing" },
    { "CI_Router", "_set_overrides" }
};
static const size_t CODEIGNITER_ROUTER_METHODS_COUNT =
    sizeof(CODEIGNITER_ROUTER_METHODS) / sizeof(CODEIGNITER_ROUTER_METHODS[0]);

static const MVCDispatchMethod FUELPHP_REQUEST_EXECUTE_METHODS[] = {
    { "Fuel\\Core\\Request", "execute" }
};
static const size_t FUELPHP_REQUEST_EXECUTE_METHODS_COUNT =
    sizeof(FUELPHP_REQUEST_EXECUTE_METHODS) / sizeof(FUELPHP_REQUEST_EXECUTE_METHODS[0]);

static const MVCDispatchMethod CAKEPHP_REQUEST_EXECUTE_METHODS[] = {
    { "Dispatcher", "_invoke" }
};
static const size_t CAKEPHP_REQUEST_EXECUTE_METHODS_COUNT =
    sizeof(CAKEPHP_REQUEST_EXECUTE_METHODS) / sizeof(CAKEPHP_REQUEST_EXECUTE_METHODS[0]);

MVCEntryPointDelegate::MVCEntryPointDelegate(InterceptionEngine* interceptEngine,
                                             TransactionMonitor* txMonitor)
    : AEntryPointDelegate(interceptEngine, txMonitor)
{
}

MVCEntryPointDelegate::~MVCEntryPointDelegate()
{
}

void MVCEntryPointDelegate::configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                      const struct _zend_agent_globals* agentGlobals)
{
    const appdynamics::pb::Agent::MatchPointConfig* configProto = &(txConfig->mvc());
    boost::shared_ptr<MatchPointConfig> config(boost::make_shared<MatchPointConfig>(txConfig,
                                                                                    configProto));
    AEntryPointDelegate::configure(config, agentGlobals);
}


void MVCEntryPointDelegate::initAcceptor()
{
    m_txAcceptor = boost::make_shared<MVCTransactionAcceptor>(m_matchPointConfig, m_txMonitor);
}

static inline void addInterceptors(const MVCDispatchMethod* methods,
                                   size_t methodCount,
                                   InterceptionEngine* interceptorEngine,
                                   const AMethodInterceptor* interceptor)
{
    for (unsigned i = 0; i < methodCount; ++i)
    {
        const MVCDispatchMethod& m = methods[i];
        interceptorEngine->addInterceptorForCallable(CallableInfo(m.m_className,
                                                                  m.m_methodName),
                                                     const_cast<AMethodInterceptor*>(interceptor));
    }
}

void MVCEntryPointDelegate::applyRules(const struct _zend_agent_globals* agentGlobals)
{
    if (s_didApplyRules)
        return;

    s_didApplyRules = true;

    if (agentGlobals->G->enabled_frameworks.symfony1) {
        addInterceptors(SYMFONY1_WEB_CONTROLLER_METHODS,
                        SYMFONY1_WEB_CONTROLLER_METHODS_COUNT,
                        m_interceptEngine,
                        &symfony1Interceptor);
    }

    if (agentGlobals->G->enabled_frameworks.symfony2) {
        addInterceptors(SYMFONY2_CONTROLLER_RESOLVER_METHODS,
                        SYMFONY2_CONTROLLER_RESOLVER_METHODS_COUNT,
                        m_interceptEngine,
                        &symfony2GetArgumentsInterceptor);
    }

    if (agentGlobals->G->enabled_frameworks.drupal8) {
        addInterceptors(DRUPAL8_PAGE_CACHE_GET_METHODS,
                        DRUPAL8_PAGE_CACHE_GET_METHODS_COUNT,
                        m_interceptEngine,
                        &drupal8PageCacheGetInterceptor);

        addInterceptors(DRUPAL8_PAGE_CACHE_SET_METHODS,
                        DRUPAL8_PAGE_CACHE_SET_METHODS_COUNT,
                        m_interceptEngine,
                        &drupal8PageCacheSetInterceptor);
    }

    if (agentGlobals->G->enabled_frameworks.zend1) {
        addInterceptors(ZEND1_CONTROLLER_ACTION_METHODS,
                        ZEND1_CONTROLLER_ACTION_METHODS_COUNT,
                        m_interceptEngine,
                        &zend1Interceptor);
        addInterceptors(ZEND1_CONTROLLER_REDIRECT_METHODS,
                        ZEND1_CONTROLLER_REDIRECT_METHODS_COUNT,
                        m_interceptEngine,
                        &zend1RedirectInterceptor);
    }

    if (agentGlobals->G->enabled_frameworks.codeigniter) {
        addInterceptors(CODEIGNITER_ROUTER_METHODS,
                        CODEIGNITER_ROUTER_METHODS_COUNT,
                        m_interceptEngine,
                        &ciSetRoutingInterceptor);
    }

    if (agentGlobals->G->enabled_frameworks.cakephp) {
        addInterceptors(CAKEPHP_REQUEST_EXECUTE_METHODS,
                        CAKEPHP_REQUEST_EXECUTE_METHODS_COUNT,
                        m_interceptEngine,
                        &cakeControllerInterceptor);
    }

#if PHP_VERSION_ID >= 50300
    if (agentGlobals->G->enabled_frameworks.fuelphp) {
        addInterceptors(FUELPHP_REQUEST_EXECUTE_METHODS,
                        FUELPHP_REQUEST_EXECUTE_METHODS_COUNT,
                        m_interceptEngine,
                        &fuelRequestInterceptor);
    }

    if (agentGlobals->G->enabled_frameworks.zend2) {
        addInterceptors(ZEND2_ACTION_CONTROLLER_ONDISPATCH_METHODS,
                        ZEND2_ACTION_CONTROLLER_ONDISPATCH_METHODS_COUNT,
                        m_interceptEngine,
                        &zend2ActionOnDispatchInterceptor);

        addInterceptors(ZEND2_RESTFUL_CONTROLLER_ONDISPATCH_METHODS,
                        ZEND2_RESTFUL_CONTROLLER_ONDISPATCH_METHODS_COUNT,
                        m_interceptEngine,
                        &zend2RestfulOnDispatchInterceptor);
    }
#endif
}

bool MVCTransactionAcceptor::matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& iPayload,
                                                  const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition)
{
    const boost::shared_ptr<MVCPayload> payload = boost::static_pointer_cast<MVCPayload>(iPayload);
    const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
    bool matches = true;

    appdynamics::pb::Agent::EntryPointMatchCondition::MVCMatchRule matchRule = matchCondition.mvc();

    if (matchRule.has_controller())
        matches = matches && configPtr->matchAndLogError(payload->getExecEnvironment(),
                                                         &(matchRule.controller()),
                                                         payload->getController(),
                                                         getLogger());

    if (matchRule.has_action())
        matches = matches && configPtr->matchAndLogError(payload->getExecEnvironment(),
                                                         &(matchRule.action()),
                                                         payload->getAction(),
                                                         getLogger());

    if (matchRule.has_module())
        matches = matches && configPtr->matchAndLogError(payload->getExecEnvironment(),
                                                         &(matchRule.module()),
                                                         payload->getModule(),
                                                         getLogger());

    return matches;
}

bool MVCTransactionAcceptor::customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                                    const boost::shared_ptr<IMatchPointPayload>& iPayload)
{
    *matchedTransaction = boost::shared_ptr<AgentTransaction>();
    const boost::shared_ptr<MVCPayload> payload = boost::static_pointer_cast<MVCPayload>(iPayload);

    LOG4CXX_TRACE(getLogger(), "MVC payload: [" << payload->getModule() << ", " << payload->getController() << ", " << payload->getAction() << "]");

    const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
    const appdynamics::pb::Agent::MatchPointConfig* config = configPtr->get();

    const auto& customDefinitions = config->customdefinitions();

    int maxPriority = std::numeric_limits<int>::min();
    const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* prioritizedMatchRule = NULL;

    for (auto it = customDefinitions.begin(); it != customDefinitions.end(); ++it) {

        if (!it->condition().has_mvc())
            continue;

        LOG4CXX_TRACE(getLogger(), "Attempting to match custom match rule: " << it->btname());
        bool matches = matchTransactionRule(payload, it->condition());

        if (!matches)
            continue;

        LOG4CXX_DEBUG(getLogger(), "Potential match rule found: " << it->btname() << " Priority: " << it->priority() << " Current maximum matching priority: " << maxPriority);

        if (it->priority() > maxPriority) {
            prioritizedMatchRule = &(*it);
            maxPriority = it->priority();
        }
    }

    if (!prioritizedMatchRule)
        return false;
    *matchedTransaction = getTransaction(prioritizedMatchRule->btname(), prioritizedMatchRule);
    return true;
}


std::string MVCTransactionAcceptor::getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& payload)
{
    const MVCPayload& mvcPayload = *boost::static_pointer_cast<MVCPayload>(payload);

    std::vector<std::string> nameParts;
    nameParts.reserve(3);

    const std::string& moduleName = mvcPayload.getModule();
    if (!moduleName.empty())
        nameParts.push_back(moduleName);

    const std::string& controllerName = mvcPayload.getController();
    if (!controllerName.empty())
        nameParts.push_back(controllerName);

    const std::string& actionName = mvcPayload.getAction();
    if (!actionName.empty())
        nameParts.push_back(actionName);

    std::string baseName(boost::algorithm::join(nameParts, " : "));

    const std::map<std::string, std::string>& properties = getConfig()->getDiscoveryProperties();
    return transformNameWithVirtualHost(baseName,
                                        &properties,
                                        mvcPayload.getExecEnvironment());
}
