/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#include "main/php_version.h"
#if PHP_VERSION_ID >= 50300

#include "fuelphp.h"
#include "frameworks/mvc.h"
#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include <boost/algorithm/string/join.hpp>

#include "Zend/zend_closures.h"

namespace FuelPHP
{
    static const char ROUTE_PROPERTY_NAME[] = "route";
    static size_t ROUTE_PROPERTY_NAME_LEN = sizeof(ROUTE_PROPERTY_NAME) - 1;

    static const char SEGMENTS_PROPERTY_NAME[] = "segments";
    static size_t SEGMENTS_PROPERTY_NAME_LEN = sizeof(SEGMENTS_PROPERTY_NAME) - 1;

    static const char METHOD_PARAMS_PROPERTY_NAME[] = "method_params";
    static size_t METHOD_PARAMS_PROPERTY_NAME_LEN = sizeof(METHOD_PARAMS_PROPERTY_NAME) - 1;

    static const char PATH_PROPERTY_NAME[] = "path";
    static size_t PATH_PROPERTY_NAME_LEN = sizeof(PATH_PROPERTY_NAME) - 1;

    static const char CALLABLE_PROPERTY_NAME[] = "callable";
    static size_t CALLABLE_PROPERTY_NAME_LEN = sizeof(CALLABLE_PROPERTY_NAME) - 1;

    static const char MODULE_PROPERTY_NAME[] = "module";
    static size_t MODULE_PROPERTY_NAME_LEN = sizeof(MODULE_PROPERTY_NAME) - 1;

    static const char ACTION_PROPERTY_NAME[] = "action";
    static size_t ACTION_PROPERTY_NAME_LEN = sizeof(ACTION_PROPERTY_NAME) - 1;

    RequestExecuteInterceptor::RequestExecuteInterceptor()
        : AEntryPointInterceptor("FuelPHP::RequestExecuteInterceptor",
                                 InterceptorRegistry::FuelPHPRequestExecuteInterceptor_ID)
    {
    }

    boost::shared_ptr<IMatchPointPayload>
        RequestExecuteInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        zval* object = execEnv->getInvokedObject();
        if (!object)
            return boost::shared_ptr<IMatchPointPayload>();
        ZValPointerObject requestObject(ZValPointerAny::share(const_cast<zval*>(object)).cast<ZValPointerObject>());
        if (!requestObject)
            return boost::shared_ptr<IMatchPointPayload>();

        ZValPointerObject routerObject(
                requestObject.findPropertyByName<ZValPointerObject>(execEnv,
                    ROUTE_PROPERTY_NAME, ROUTE_PROPERTY_NAME_LEN));
        if (!routerObject)
            return boost::shared_ptr<IMatchPointPayload>();

        CallGraph::ExecStackFrame* const topStackElement =
            execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        if (topStackElement)
            topStackElement->setCallElementType(appdynamics::pb::CallElement_Type_FUELPHP);

        ZValPointerObject callable(
                routerObject.findPropertyByName<ZValPointerObject>(execEnv,
                    CALLABLE_PROPERTY_NAME, CALLABLE_PROPERTY_NAME_LEN));

        // If the route action is a closure, name BT based on the 'path'
        // property
        if (callable && callable.isInstanceOf(execEnv, zend_ce_closure)) {
            std::string name;
            if (getNameForCallable(routerObject, name, execEnv))
                return boost::make_shared<MVCPayload,
                                          const std::string&,
                                          const std::string&,
                                          const std::string&,
                                          const PHPExecEnvironment* const&>(std::string(),
                                                                            std::string(),
                                                                            name,
                                                                            execEnv);
            else
                return boost::shared_ptr<IMatchPointPayload>();
        } else {
            std::string module, controller, action;
            if (getNameForAction(routerObject, module, controller, action, execEnv))
                return boost::make_shared<MVCPayload,
                                          const std::string&,
                                          const std::string&,
                                          const std::string&,
                                          const PHPExecEnvironment* const&>(module,
                                                                            controller,
                                                                            action,
                                                                            execEnv);
            else
                return boost::shared_ptr<IMatchPointPayload>();
        }

    }

    bool RequestExecuteInterceptor::getNameForCallable(const ZValPointerObject& routerObject,
                                                       std::string& name,
                                                       const PHPExecEnvironment* execEnv)
    {
        ZValPointerString pathProperty(
                routerObject.findPropertyByName<ZValPointerString>(execEnv,
                    PATH_PROPERTY_NAME, PATH_PROPERTY_NAME_LEN));
        if (!pathProperty)
            return false;

        name = pathProperty.getStringValue();
        return true;
    }

    bool RequestExecuteInterceptor::getNameForAction(const ZValPointerObject& routerObject,
                                                     std::string& moduleName,
                                                     std::string& controllerName,
                                                     std::string& actionName,
                                                     const PHPExecEnvironment* execEnv)
    {
        ZValPointerString moduleProperty(
                routerObject.findPropertyByName<ZValPointerString>(execEnv,
                    MODULE_PROPERTY_NAME, MODULE_PROPERTY_NAME_LEN));
        if (moduleProperty)
            moduleName = moduleProperty.getStringValue();

        // Use the 'segments' property to figure out the controller name.  We
        // don't want to use the 'controller' property because it contains fully
        // qualified and prefixed class name.
        //
        // The segments contain 1-N controller segments + action + 1-N method
        // params.

        ZValPointerArray segmentsProperty(
                routerObject.findPropertyByName<ZValPointerArray>(execEnv,
                    SEGMENTS_PROPERTY_NAME, SEGMENTS_PROPERTY_NAME_LEN));

        ZValPointerArray methodParamsProperty(
                routerObject.findPropertyByName<ZValPointerArray>(execEnv,
                    METHOD_PARAMS_PROPERTY_NAME, METHOD_PARAMS_PROPERTY_NAME_LEN));

        size_t nNameSegments = segmentsProperty.size() - methodParamsProperty.size();

        ZValPointerString actionProperty(
                routerObject.findPropertyByName<ZValPointerString>(execEnv,
                    ACTION_PROPERTY_NAME, ACTION_PROPERTY_NAME_LEN));
        if (actionProperty) {
            actionName = actionProperty.getStringValue();
            nNameSegments--;
        } else {
            actionName = "index";
        }

        if (nNameSegments == 0)
            return false;

        std::vector<std::string> controllerSegments;
        size_t currentSegment = 0;
        for (auto it = segmentsProperty.begin();
                it != segmentsProperty.end() && currentSegment < nNameSegments; ++it) {
            controllerSegments.push_back((*it).getValue().cast<ZValPointerString>().getStringValue());
            currentSegment++;
        }

        controllerName = boost::algorithm::join(controllerSegments, "/");

        std::transform(moduleName.begin(), moduleName.end(), moduleName.begin(), ::tolower);
        std::transform(controllerName.begin(), controllerName.end(), controllerName.begin(), ::tolower);
        std::transform(actionName.begin(), actionName.end(), actionName.begin(), ::tolower);

        return true;
    }

    bool RequestExecuteInterceptor::isTxToBeIdentified(const PHPExecEnvironment *execEnv) const
    {
        const TransactionContext* const txContext =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
        // txContext can be null if the PHP_WEB entrypoint is disabled or if there
        // is an exclude rule configured for the current URL.
        if (txContext && (txContext->getInitiatingInterceptor() == this))
            return false;
        return AEntryPointInterceptor::isTxToBeIdentified(execEnv);
    }

    appdynamics::pb::Agent::EntryPointType RequestExecuteInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }
}

#endif
