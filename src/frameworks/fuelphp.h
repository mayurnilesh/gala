/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#ifndef __fuelphp_h
#define __fuelphp_h

#include "main/php_version.h"
#if PHP_VERSION_ID >= 50300

#include "entrypoint.h"

namespace FuelPHP
{
    /*
     * Interceptor that is set on Fuel\Core\Request::execute() method and names
     * the BT based on the information contained in the $router property of the
     * request. The execute() method re-throws any exceptions it gets, so we
     * the agent should be able to report them.
     */
    class RequestExecuteInterceptor : public AEntryPointInterceptor
    {
        public:
            RequestExecuteInterceptor();
            virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv);
            virtual bool isTxToBeIdentified(const PHPExecEnvironment *execEnv) const;
            virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;

            virtual bool needParamsInOnMethodBegin() const { return false; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }
            virtual bool shouldCallOnMethodEnd() const { return false; }

        private:
            bool getNameForCallable(const ZValPointerObject& routerObject, std::string& name, const PHPExecEnvironment* execEnv);
            bool getNameForAction(const ZValPointerObject& routerObject, std::string& moduleName, std::string& controllerName, std::string& actionName, const PHPExecEnvironment* execEnv);
    };
}

#endif

#endif // __fuelphp_h
