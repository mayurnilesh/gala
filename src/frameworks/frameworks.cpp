/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#include "frameworks.h"
#include <boost/preprocessor/stringize.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <vector>

#define SET_FRAMEWORK_FLAG(f)                       \
    if (frameworkName == BOOST_PP_STRINGIZE(f)) {   \
        frameworksFlags->f = true;                  \
        return;                                     \
    }
static inline void setFlag(const std::string& frameworkName,
                           FrameworksFlags* frameworksFlags)
{
    BOOST_ASSERT(frameworksFlags != NULL);
    FRAMEWORKS_TABLE(SET_FRAMEWORK_FLAG)
}

#undef SET_FRAMEWORK_FLAG

void parseFrameworksFlags(const std::string& frameworksString,
                          FrameworksFlags* frameworksFlags)
{
    BOOST_ASSERT(frameworksFlags != NULL);
    typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer;
    boost::escaped_list_separator<char> sep;

    tokenizer frameworkTokens(frameworksString.begin(), frameworksString.end(), sep);
    auto frameworkTokensEnd = frameworkTokens.end();
    auto frameworkTokensBegin = frameworkTokens.begin();

    if (frameworkTokensBegin != frameworkTokensEnd)
        frameworksFlags->clear();

    for (auto i = frameworkTokensBegin; i != frameworkTokensEnd; ++i) {
        std::string framework(*i);
        boost::algorithm::trim(framework);
        if (framework == "none") {
            frameworksFlags->clear();
            return;
        }
        setFlag(framework, frameworksFlags);
    }
}

#define ADD_FRAMEWORK_FLAG(f)                               \
    if (frameworksFlags.f)                                  \
        frameworksNames.push_back(BOOST_PP_STRINGIZE(f));

std::string frameworksFlagsToString(const FrameworksFlags& frameworksFlags)
{
    if (frameworksFlags.allSet())
        return std::string();

    if (!frameworksFlags.anySet())
        return std::string("none");

    std::vector<std::string> frameworksNames;
    frameworksNames.reserve(Framework_LAST);
    FRAMEWORKS_TABLE(ADD_FRAMEWORK_FLAG);
    return boost::algorithm::join(frameworksNames, ", ");
}

#undef ADD_FRAMEWORK_FLAG
