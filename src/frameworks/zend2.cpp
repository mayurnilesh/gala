/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#include "zend2.h"
#if PHP_VERSION_ID >= 50300

#include "emalloc_allocator.h"

namespace zend2
{
    static const char ROUTE_MATCH_PROPERTY_NAME[] = "routeMatch";
    static const char PARAMS_PROPERTY_NAME[] = "params";
    static const char CONTROLLER_PARAM_NAME[] = "controller";
    static const char ACTION_PARAM_NAME[] = "action";
    static const std::string ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS[] =
    {
        "get",
        "getList",
        "delete",
        "deleteList",
        "patch",
        "patchList",
        "update",
        "replaceList",
        "create",
        "head",
        "options"
    };
    static const size_t ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS_COUNT =
        sizeof(ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS) /
        sizeof(ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS[0]);

    ZValPointerAny getRouteMatchParamsArray(const PHPExecEnvironment* execEnv)
    {
        // Intercepted method's signature:
        // public function Zend\Mvc\Controller\Abstract<Action|Restful>Controller::onDispatch($mvcEvent)

        // The dispatch method is an instance method, so
        // if object is NULL then this interceptor does not
        // recognize the method being called!
        zval* object = execEnv->getInvokedObject();
        if (!object)
            return ZValPointerAny();
        ZValPointerObject phpObject(ZValPointerAny::share(const_cast<zval*>(object)).cast<ZValPointerObject>());
        if (!phpObject)
            return ZValPointerAny();

        // We expect at least one parameter
        if (execEnv->getParamCount() < 1)
            return ZValPointerAny();

        ZValPointerObject eventObject(execEnv->getParam<ZValPointerObject>(0));
        if (!eventObject)
            return ZValPointerAny();

        ZValPointerObject routeMatchObject(eventObject.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                                      ROUTE_MATCH_PROPERTY_NAME,
                                                                                                      strlen(ROUTE_MATCH_PROPERTY_NAME)));
        if (!routeMatchObject)
            return ZValPointerAny();

        ZValPointerArray routeMatchParamsArray(routeMatchObject.findProtectedPropertyByName<ZValPointerArray>(execEnv,
                                                                                                              PARAMS_PROPERTY_NAME,
                                                                                                              strlen(PARAMS_PROPERTY_NAME)));
        return routeMatchParamsArray;
    }

    /*
     * Assumes a valid routeMatchParamsArray object
     */
    std::string getControllerName(ZValPointerArray routeMatchParamsArray)
    {
        ZValPointerString controllerNameZVal(routeMatchParamsArray.findEntryByName<ZValPointerString>(CONTROLLER_PARAM_NAME,
                                                                                                      strlen(CONTROLLER_PARAM_NAME)));
        if (!controllerNameZVal)
            return "";

        return controllerNameZVal.getStringValue();
    }

    /*
     * Assumes a valid routeMatchParamsArray object
     */
    std::string getActionName(ZValPointerArray routeMatchParamsArray)
    {
        ZValPointerString actionNameZVal(routeMatchParamsArray.findEntryByName<ZValPointerString>(ACTION_PARAM_NAME,
                                                                                                  strlen(ACTION_PARAM_NAME)));
        if (!actionNameZVal)
            return "";

        return actionNameZVal.getStringValue();
    }

    /* Action Controller Entrypoint Interceptor */
    ActionControllerEntryPointInterceptor::ActionControllerEntryPointInterceptor()
        : AEntryPointInterceptor("Zend2ActionEntryPointInterceptor",
                                 InterceptorRegistry::Zend2ActionEntryPointInterceptor_ID)
    {
    }

    ActionControllerEntryPointInterceptor::~ActionControllerEntryPointInterceptor()
    {
    }

    boost::shared_ptr<IMatchPointPayload> ActionControllerEntryPointInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        ZValPointerArray routeMatchParamsArray = getRouteMatchParamsArray(execEnv).cast<ZValPointerArray>();
        if (!routeMatchParamsArray)
            return boost::shared_ptr<IMatchPointPayload>();

        std::string controllerName = getControllerName(routeMatchParamsArray);
        if (controllerName.empty())
            return boost::shared_ptr<IMatchPointPayload>();

        std::string actionName = getActionName(routeMatchParamsArray);
        if (actionName.empty())
            return boost::shared_ptr<IMatchPointPayload>();

        return boost::make_shared<MVCPayload>(std::string(),
                                              controllerName,
                                              actionName,
                                              execEnv);
    }

    appdynamics::pb::Agent::EntryPointType ActionControllerEntryPointInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }

    /* Restful Controller Entrypoint Interceptor */
    RestfulControllerEntryPointInterceptor::RestfulControllerEntryPointInterceptor()
        : AMethodInterceptor("Zend2RestfulOnDispatchInterceptor",
                             InterceptorRegistry::Zend2RestfulEntryPointInterceptor_ID)
    {
    }

    RestfulControllerEntryPointInterceptor::~RestfulControllerEntryPointInterceptor()
    {
    }

    void* RestfulControllerEntryPointInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
    {
        // retrieve the controller class name (including the "Controller" suffix)
        zval* object = execEnv->getInvokedObject();
        if (!object)
            return NULL;
        m_controllerClass = execEnv->getClassName(object);
        if(m_controllerClass.empty())
            return NULL;

        // retrieve the canonical controller name (without the "Controller" suffix)
        // and assign it to the HTTP Method Interceptor
        ZValPointerArray routeMatchParamsArray = getRouteMatchParamsArray(execEnv).cast<ZValPointerArray>();
        if (!routeMatchParamsArray)
            return NULL;
        std::string controllerName = getControllerName(routeMatchParamsArray);
        if (controllerName.empty())
            return NULL;
        m_httpMethodInterceptor.setControllerName(controllerName);

        // register the HTTP Method interceptors
        InterceptionEngine* interceptEngine = AG(icept_engine);
        for(int i = 0; i < ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS_COUNT; ++i)
        {
            std::string method = ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS[i];
            interceptEngine->addInterceptorForCallable(CallableInfo(m_controllerClass, method),
                                                       &m_httpMethodInterceptor);
        }

        // We're done and don't delegate to the base class' onCallableBegin, we only wanted to
        // register the interceptor for the HTTP methods.
        return NULL;
    }

    void RestfulControllerEntryPointInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                                            void* state)
    {
        if(!m_controllerClass.empty())
        {
            InterceptionEngine* interceptEngine = AG(icept_engine);
            for(int i = 0; i < ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS_COUNT; ++i)
            {
                std::string method = ZEND2_RESTFUL_CONTROLLER_HTTP_INTERCEPT_METHODS[i];
                interceptEngine->removeInterceptorForCallable(CallableInfo(m_controllerClass, method),
                                                              &m_httpMethodInterceptor);
            }
        }
        m_httpMethodInterceptor.setControllerName("");
    }

    bool RestfulControllerEntryPointInterceptor::isActive(const PHPExecEnvironment* execEnv) const
    {
        return !AG(kernel)->getAgentContext()
                          ->getTransactionMonitor()
                          ->getEntryPointDelegate(appdynamics::pb::Agent::PHP_MVC)
                          ->isDetectionDisabled();
    }

    /* Restful Controller HTTP Method Interceptor.
       This interceptor becomes registered dynamically when the AbstractRestfulController::onDispatch()
       method is intercepted (in RestfulControllerEntryPointInterceptor::onCallableBegin).
       We do this because we need to extract the name of the implementing Restful Controller class during
       the call of onDispatch().
    */
    RestfulControllerHTTPMethodInterceptor::RestfulControllerHTTPMethodInterceptor()
        : AEntryPointInterceptor("Zend2RestfulHTTPMethodInterceptor",
                                 InterceptorRegistry::Zend2RestfulHTTPMethodInterceptor_ID)
    {
    }

    RestfulControllerHTTPMethodInterceptor::~RestfulControllerHTTPMethodInterceptor()
    {
    }

    boost::shared_ptr<IMatchPointPayload> RestfulControllerHTTPMethodInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        if(m_controllerName.empty())
            return boost::shared_ptr<IMatchPointPayload>();

        std::string actionName(execEnv->getFunctionName());
        if(actionName.empty())
            return boost::shared_ptr<IMatchPointPayload>();

        return boost::make_shared<MVCPayload>(std::string(),
                                              m_controllerName,
                                              actionName,
                                              execEnv);
    }

    appdynamics::pb::Agent::EntryPointType RestfulControllerHTTPMethodInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }
}

#endif