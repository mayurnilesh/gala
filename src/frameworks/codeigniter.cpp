/*
    Copyright 2013 AppDynamics
    All rights reserved.
 */

#include "codeigniter.h"
#include "frameworks/mvc.h"
#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include <algorithm>
#include <cctype>

namespace CodeIgniter
{
    static const char ROUTE_CLASS_PROPERTY_NAME[] = "class";
    static size_t ROUTE_CLASS_PROPERTY_NAME_LEN = sizeof(ROUTE_CLASS_PROPERTY_NAME) - 1;

    static const char ROUTE_METHOD_PROPERTY_NAME[] = "method";
    static size_t ROUTE_METHOD_PROPERTY_NAME_LEN = sizeof(ROUTE_METHOD_PROPERTY_NAME) - 1;

    static const char REMAP_METHOD_NAME[] = "_remap";
    static const char REMAPPED_ACTION[] = "[remapped]";

    SetRoutingInterceptor::SetRoutingInterceptor()
        : AMethodInterceptor("CodeIgniter::SetRoutingInterceptor",
                             InterceptorRegistry::CodeIgniterSetRoutingInterceptor_ID)
    {
    }

    bool SetRoutingInterceptor::isActive(const PHPExecEnvironment* execEnv) const
    {
        return !AG(kernel)->getAgentContext()
                          ->getTransactionMonitor()
                          ->getEntryPointDelegate(appdynamics::pb::Agent::PHP_MVC)
                          ->isDetectionDisabled();
    }

    void* SetRoutingInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
    {
        return NULL;
    }

    void SetRoutingInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
    {
        // The routing method is an instance method, so if the object is NULL
        // then this interceptor does not recognize the method being called
        zval* object = execEnv->getInvokedObject();
        if (!object)
            return;
        ZValPointerObject routerObject(ZValPointerAny::share(const_cast<zval*>(object)).cast<ZValPointerObject>());
        if (!routerObject)
            return;

        ZValPointerString classProperty(
                routerObject.findPropertyByName<ZValPointerString>(execEnv,
                    ROUTE_CLASS_PROPERTY_NAME, ROUTE_CLASS_PROPERTY_NAME_LEN));
        if (!classProperty)
            return;

        ZValPointerString methodProperty(
                routerObject.findPropertyByName<ZValPointerString>(execEnv,
                    ROUTE_METHOD_PROPERTY_NAME, ROUTE_METHOD_PROPERTY_NAME_LEN));
        if (!methodProperty)
            return;

        std::string className(classProperty.getStringValue());
        std::string methodName(methodProperty.getStringValue());

        if (className.empty() || methodName.empty())
            return;

        InterceptionEngine* interceptEngine = execEnv->getInterceptionEngine();
        interceptEngine->addInterceptorForCallable(CallableInfo(className, methodName), &ciHandlerInterceptor);
        interceptEngine->addInterceptorForCallable(CallableInfo(className, REMAP_METHOD_NAME), &ciHandlerInterceptor);
    }

    HandlerInterceptor::HandlerInterceptor()
        : AEntryPointInterceptor("CodeIgniter::HandlerInterceptor",
                                 InterceptorRegistry::CodeIgniterHandlerInterceptor_ID)
    {

    }

    boost::shared_ptr<IMatchPointPayload> HandlerInterceptor::createPayload(const PHPExecEnvironment* execEnv)
    {
        CallGraph::ExecStackFrame* const topStackElement =
            execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        if (topStackElement)
            topStackElement->setCallElementType(appdynamics::pb::CallElement_Type_CODEIGNITER);

        std::string className(execEnv->getClassName());
        std::transform(className.begin(), className.end(), className.begin(), ::tolower);

        std::string methodName(execEnv->getFunctionName());
        if (methodName == REMAP_METHOD_NAME) {
            methodName = REMAPPED_ACTION;
        } else {
            std::transform(methodName.begin(), methodName.end(), methodName.begin(), ::tolower);
        }

        return boost::make_shared<MVCPayload,
                                  const std::string&,
                                  const std::string&,
                                  const std::string&,
                                  const PHPExecEnvironment* const&>(std::string(),
                                                                    className,
                                                                    methodName,
                                                                    execEnv);
    }

    bool HandlerInterceptor::isTxToBeIdentified(const PHPExecEnvironment *execEnv) const
    {
        const TransactionContext* const txContext =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();
        // txContext can be null if the PHP_WEB entrypoint is disabled or if there
        // is an exclude rule configured for the current URL.
        if (txContext && (txContext->getInitiatingInterceptor() == this))
            return false;
        return AEntryPointInterceptor::isTxToBeIdentified(execEnv);
    }

    appdynamics::pb::Agent::EntryPointType HandlerInterceptor::getEntryPointType() const
    {
        return appdynamics::pb::Agent::PHP_MVC;
    }
}
