/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#ifndef __frameworks_h
#define __frameworks_h

#include <string>

#include "frameworks_table.h"

#define DEFINE_FRAMEWORK_FLAG(f)        \
    bool f : 1;

#define DEFINE_FRAMEWORK_FLAG_INITIALIZER(f)    \
    f(true),

#define DEFINE_FRAMEWORK_FLAG_CLEARED(f)    \
    f = false;

#define DEFINE_FRAMEWORK_FLAG_AND_IS_SET(f)    \
    && f

#define DEFINE_FRAMEWORK_FLAG_OR_IS_SET(f)    \
    || f

#define DEFINE_FRAMEWORK_ENUM_ENTRY(f)  \
    Framework_##f,

#define DEFINE_FRAMEWORK_FLAG_CASE(f)   \
    case Framework_##f: return f;

/**
    An enum that has an entry for each supported framework and an extra entry
    that can be used to determine the number of supported frameworks.
 */
enum Framework
{
    FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_ENUM_ENTRY)
    Framework_LAST
};

/**
    A struct containing a flag for each "framework" supported by the agent.
    If the flag is set then the interceptors needed to instrument that "framework"
    are registered with the interception engine.  The table of supported "framework"'s
    is in frameworks_table.h.
 */
struct FrameworksFlags
{
    FrameworksFlags() : FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_FLAG_INITIALIZER) m_unused(true) {}

    void clear() { FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_FLAG_CLEARED); }

    bool allSet() const { return true FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_FLAG_AND_IS_SET); }
    bool anySet() const { return false FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_FLAG_OR_IS_SET); }

    bool isSet(Framework f) const { switch (f) { FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_FLAG_CASE); } return false; }

    FRAMEWORKS_TABLE(DEFINE_FRAMEWORK_FLAG)
    bool m_unused : 1;
};

#undef DEFINE_FRAMEWORK_FLAG
#undef DEFINE_FRAMEWORK_FLAG_INITIALIZER
#undef DEFINE_FRAMEWORK_FLAG_CLEARED
#undef DEFINE_FRAMEWORK_FLAG_IS_SET
#undef DEFINE_FRAMEWORK_ENUM_ENTRY
#undef DEFINE_FRAMEWORK_FLAG_CASE

/**
    Parses a comma separated list of framework names into a FrameworksFlags structure.
 */
extern void parseFrameworksFlags(const std::string& frameworksString,
                                 FrameworksFlags* frameworksFlags);

/**
    Generates a comma separated list of framework names from a FrameworksFlags structure.
 */
extern std::string frameworksFlagsToString(const FrameworksFlags& frameworksFlags);

#endif
