#ifndef __txinfo_h
#define __txinfo_h
/*
    Copyright 2012 AppDynamics.
    All rights reserved.
*/


#include <string>
#include <boost/functional/hash.hpp>

/* {{{ TxInfo and related */
class TxInfo {
    public:
        inline TxInfo()
            : m_valid(false)
        {
        }

        inline TxInfo(const std::string& name,
                      const std::string type,
                      int64_t componentID)
            : m_valid(true)
            , m_name(name)
            , m_type(type)
            , m_componentID(componentID)
        {
        }

        inline TxInfo(const TxInfo& other)
            : m_name(other.m_name)
            , m_type(other.m_type)
            , m_componentID(other.m_componentID)
        {
        }

        inline bool isValid() const { return m_valid; }
        inline const std::string& getName() const { return m_name; }
        inline const std::string& getEntryPointType() const { return m_type; }
        inline int64_t getComponentID() const { return m_componentID; }
        inline TxInfo& operator=(const TxInfo& other)
        {
            if (this == &other)
                return *this;

            m_name = other.m_name;
            m_type = other.m_type;
            m_componentID = other.m_componentID;
            return *this;
        }
    private:
        bool m_valid;
        std::string m_name;
        std::string m_type;
        int64_t m_componentID;
};

inline bool operator==(const TxInfo& lhs, const TxInfo& rhs)
{
    if (lhs.getEntryPointType() != rhs.getEntryPointType())
        return false;
    if (lhs.getName() != rhs.getName())
        return false;
    if (lhs.getComponentID() != rhs.getComponentID())
        return false;
    return true;
}

// Hash functor for TxInfo
namespace boost {
    template<> struct hash<TxInfo> {
        size_t operator()(const TxInfo& t) const {
            size_t result = 0;
            boost::hash_combine(result, t.getName());
            boost::hash_combine(result, t.getEntryPointType());
            return result;
        }
    };
}

/* }}} */

#endif
