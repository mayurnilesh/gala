#ifndef __ENTRYPOINT_H
#define __ENTRYPOINT_H

/*
 * Forward declarations.
 */
class ITransactionAcceptor;
class MatchPointConfig;

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include "payload.h"
#include "configs.h"
#include "intercept.h"
#include "PHPAgentProtobufs.pb.h"
#include "util.h"
#include "naming.h"
#include "name_value_pair.h"
#include "transactions.h"


struct _zend_agent_globals;

static inline const std::string& getEntryPointName(appdynamics::pb::Agent::EntryPointType entryPointType)
{
    return appdynamics::pb::Agent::EntryPointType_Name(entryPointType);
}

/*
 * Interceptor.
 *
 * Responsible for determining if tx is to be identified, generating the
 * payload, and calling the acceptor to identify and accept the tx.
 */

class IEntryPointInterceptor : boost::noncopyable {
    public:
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const = 0;
};

class AEntryPointInterceptor : public IEntryPointInterceptor, public AMethodInterceptor
{
    public:

        virtual ~AEntryPointInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return false; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return false; }

        virtual inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return isTxToBeIdentified(execEnv);
        }

    protected:
        AEntryPointInterceptor(const char* const className,
                               InterceptorRegistry::ID interceptorID)
            : AMethodInterceptor(className, interceptorID)
            , m_logger(getLogger(std::string(LogContext::TX_SERVICE) + ".AEntryPointInterceptor"))
        {
        }

        virtual boost::shared_ptr<IMatchPointPayload> createPayload(const PHPExecEnvironment* execEnv) = 0;

        virtual boost::shared_ptr<ITransactionAcceptor> getTransactionAcceptor();
        virtual bool acceptTransaction(const PHPExecEnvironment* phpExecEnv,
                                       const boost::shared_ptr<IMatchPointPayload>& payload);

        virtual void detectErrors(const PHPExecEnvironment* phpExecEnv, void* state);

        virtual bool isTxToBeIdentified(const PHPExecEnvironment *execEnv) const;

        // To be overriden by interceptors which could be disabled, but
        // want BT to be detected only if a correlation header is present in the
        // incoming call.
        virtual bool isCorrelationHeaderPresent(const PHPExecEnvironment *execEnv) const;

        bool isAgentEnabled() const;
        bool isTransactionDetectionEnabled() const;
    private:
        AgentLogger m_logger;
};

/*
 * Entry point delegates.
 *
 * Holds match point config and the transaction acceptor for this entry
 * point type.
 */
class AEntryPointDelegate : boost::noncopyable
{
public:
    AEntryPointDelegate(InterceptionEngine* interceptEngine,
                        TransactionMonitor* txMonitor)
        : m_txMonitor(txMonitor)
        , m_interceptEngine(interceptEngine)
        , m_detectionDisabled(false)
    {
    }

    virtual ~AEntryPointDelegate();

    virtual void configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                           const struct _zend_agent_globals* agentGlobals) = 0;

    inline const boost::shared_ptr<ITransactionAcceptor>& getTransactionAcceptor() const { return m_txAcceptor; }

    inline bool isDetectionDisabled() const
    {
        return m_detectionDisabled;
    }

protected:

    /**
        Called by entry point delegates ( sub-classes of this class ) to configure
        match point configuration from the protobuf.
        @param matchPointConfig match point configuration for this entry point
                                delegate.
     */
    void configure(const boost::shared_ptr<MatchPointConfig>& matchPointConfig,
                   const struct _zend_agent_globals* agentGlobals);

    virtual void initAcceptor() = 0;

    /**
        Pure virtual callback that entry point
        delegates should implement to register their
        interceptors. This is called always and individual
        interceptors should check the config to see if they should be applied.
     */
    virtual void applyRules(const struct _zend_agent_globals* agentGlobals) = 0;

    bool autoDiscoveryEnabled();

    boost::shared_ptr<MatchPointConfig> m_matchPointConfig;
    TransactionMonitor* const m_txMonitor;
    InterceptionEngine* const m_interceptEngine;
    boost::shared_ptr<ITransactionAcceptor> m_txAcceptor;
    bool m_detectionDisabled;
};


#endif /* __ENTRYPOINT_H */
