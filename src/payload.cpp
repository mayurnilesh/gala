#include "payload.h"

#include "SAPI.h"

/*
 * ApacheEnvironment
 */

char* ApacheEnvironment::getMethod()
{
    // TODO fix this so we don't retrieve it every time
    TSRMLS_FETCH();

    assert(sapi_module.getenv);
    char *method = sapi_module.getenv("REQUEST_METHOD", sizeof("REQUEST_METHOD")-1 TSRMLS_CC);
    if (method) {
        return estrdup(method);
    } else {
        return NULL;
    }

}

char* ApacheEnvironment::getURL()
{
    // TODO fix this so we don't retrieve it every time
    TSRMLS_FETCH();

    assert(sapi_module.getenv);
    char *uri = sapi_module.getenv("REQUEST_URI", sizeof("REQUEST_URI")-1 TSRMLS_CC);
    if (uri) {
        return estrdup(uri);
    } else {
        return NULL;
    }

}


