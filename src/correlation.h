#ifndef __correlation_h
#define __correlation_h

#include <boost/algorithm/string/predicate.hpp>
#include <boost/unordered_map.hpp>

#include "agent.h"
#include "config_channel.h"
#include "current_exit_call.h"
#include "exitcall_detail.h"
#include "request_context.h"
#include "reporting.h"
#include "transactions.h"

#include <boost/algorithm/string/predicate.hpp>

/* {{{ class ComponentLink */
class ComponentLink
{
    public:
        inline ComponentLink(const std::string& fromComponentID,
                             const std::string& toComponentID,
                             const std::string& exitPointType)
            : m_fromComponentID(fromComponentID)
            , m_toComponentID(toComponentID)
            , m_exitPointType(exitPointType)
        {
        }

        inline ComponentLink(const ComponentLink& other)
            : m_fromComponentID(other.m_fromComponentID)
            , m_toComponentID(other.m_toComponentID)
            , m_exitPointType(other.m_exitPointType)
        {
        }

        inline ComponentLink& operator=(const ComponentLink& other)
        {
            if (this == &other)
                return *this;
            m_fromComponentID = other.m_fromComponentID;
            m_toComponentID = other.m_toComponentID;
            m_exitPointType = other.m_exitPointType;
            return *this;
        }

        inline const std::string& getFromComponentID() const { return m_fromComponentID; }
        inline const std::string& getToComponentID() const { return m_toComponentID; }
        inline const std::string& getExitPointType() const { return m_exitPointType; }

        /**
            @return The backend id in a To Component Id of the form:
            {[UNRESOLVED][n]}.  Empty string if To Component Id is not
            of that form.
         */
        std::string getUnResolvedBackendID() const;

    private:
        std::string m_fromComponentID;
        std::string m_toComponentID;
        std::string m_exitPointType;
};
/* }}} */

/* {{{ class CorrelationHeader */

/**
 * Represents the incoming correlation header. Provides methods to retrieve
 * portions of header.
 */
class CorrelationHeader
{
    public:
        static const char* const MAIN_HEADER;
        static const size_t MAIN_HEADER_SIZE;

        static const char* const ACCOUNT_GUID;
        static const char* const CONTROLLER_GUID;

        static const char* const APPLICATION_ID_HEADER;
        static const char* const BUSINESS_TRANSACTION_ID_HEADER;
        static const char* const BUSINESS_TRANSACTION_NAME_HEADER;
        static const char* const ENTRY_POINT_TYPE_HEADER;
        static const char* const BT_COMPONENT_MAPPING_HEADER;
        static const char* const EXIT_POINT_GUID_HEADER;
        static const char* const UNRESOLVED_EXIT_ID_HEADER;
        static const char* const COMPONENT_ID_FROM_HEADER;
        static const char* const COMPONENT_ID_TO_HEADER;
        static const char* const EXIT_CALL_TYPE_ORDER;
        static const char* const SNAPSHOT_ENABLE_HEADER;
        static const char* const REQUEST_GUID_HEADER;
        static const char* const MATCH_CRITERIA_TYPE_HEADER;
        static const char* const MATCH_CRITERIA_VALUE_HEADER;
        static const char* const ORIGIN_TIMESTAMP;
        static const char* const DISABLE_TRANSACTION_DETECTION_HEADER;
        static const char* const DONOTRESOLVE;
        static const char* const DEBUG_ENABLED_HEADER;
        static const char* const MUST_TAKE_SNAPSHOT;

        static const char* const HEADER_SEPARATOR;
        static const char* const ATTRIBUTE_VALUE_SEPARATOR;
        static const char* const VALUE_DELIMITER;

        static const char* const MATCH_CRITERIA_TYPE_DISCOVERED;
        static const char* const MATCH_CRITERIA_TYPE_CUSTOM;

        static const char* const BACKEND_CALL_PREFIX;
        static const size_t BACKEND_CALL_PREFIX_SIZE;
        static const char* const BACKEND_CALL_SUFFIX;
        static const size_t BACKEND_CALL_SUFFIX_SIZE;

        static const char* const TRUE_STRING;
        static const char* const ZERO_STRING;

        static const char* const CROSSAPP_CIDTO_PREFIX;

        enum CorrelationType
        {
            NONE,
            INAPP,
            CROSSAPP
        };

        inline CorrelationHeader(const AgentLogger& logger,
                                 const boost::shared_ptr<IConfigChannel>& configChannel,
                                 const std::string& header)
            : m_logger(logger)
            , m_configChannel(configChannel)
            , m_unresolvedExitID(0)
            , m_didCacheDoNotSelfResolve(false)
            , m_didCacheSnapshotEnabled(false)
            , m_didCacheExitCallSequence(false)
            , m_didCacheDebugEnabled(false)
            , m_didCacheOriginTimestamp(false)
            , m_didCacheCorrelationType(false)
            , m_didCacheComponentLinks(false)
            , m_didCacheUnResolvedExitID(false)
            , m_didCacheSelfResolutionBackendID(false)
            , m_didCacheCrossAppUnResolvedBackendID(false)
            , m_didCacheLastLinkUnResolvedBackendID(false)
            , m_didCacheLastLinkAppID(false)
        {
            parseSubHeaders(sanitizeHeader(header));
        }

        inline boost::unordered_map<std::string, std::string>& getSubHeaders() { return m_subHeaders; }

        inline std::string getSubHeader(const char* subHeaderName) const
        {
            BOOST_ASSERT(subHeaderName);
            auto it = m_subHeaders.find(subHeaderName);
            if (it != m_subHeaders.end())
                return it->second;
            else
                return std::string();
        }

        inline bool getBooleanSubHeader(const char* subHeaderName) const
        {
            BOOST_ASSERT(subHeaderName);
            auto it = m_subHeaders.find(subHeaderName);
            if (it != m_subHeaders.end())
                return boost::iequals(it->second, TRUE_STRING);
            else
                return false;
        }

        inline bool isComponentLinksValid()
        {
            return !(getComponentLinks().empty());
        }

        inline const std::vector<ComponentLink>& getComponentLinks()
        {
            if (m_didCacheComponentLinks) {
                return m_componentLinks;
            }
            m_didCacheComponentLinks = true;
            computeComponentLinks();
            return m_componentLinks;
        }

        bool getDoNotSelfResolve();

        const std::string& getExitCallSequence();

        bool isSnapshotEnabled();

        bool isDebugEnabled();

        const std::string& getOriginTimestamp();

        CorrelationType getType();

        bool isUnResolvedExitIdValid()
        {
            return getUnResolvedExitID() != 0;
        }

        uint64_t getUnResolvedExitID()
        {
            if (m_didCacheUnResolvedExitID) {
                return m_unresolvedExitID;
            }
            m_didCacheUnResolvedExitID = true;
            m_unresolvedExitID = computeUnResolvedExitID();
            return m_unresolvedExitID;
        }

        uint64_t getSelfResolutionBackendID()
        {
            if (m_didCacheSelfResolutionBackendID) {
                return m_selfResolutionBackendID;
            }
            m_didCacheSelfResolutionBackendID = true;
            m_selfResolutionBackendID = computeSelfResolutionBackendID();
            return m_selfResolutionBackendID;
        }

        uint64_t getCrossAppUnResolvedBackendID()
        {
            if (m_didCacheCrossAppUnResolvedBackendID) {
                return m_crossAppUnResolvedBackendID;
            }
            m_didCacheCrossAppUnResolvedBackendID = true;
            m_crossAppUnResolvedBackendID =
                computeCrossAppUnResolvedBackendID();
            return m_crossAppUnResolvedBackendID;
        }

        uint64_t getLastLinkUnResolvedBackendID()
        {
            if (m_didCacheLastLinkUnResolvedBackendID) {
                return m_lastLinkUnResolvedBackendID;
            }
            m_didCacheLastLinkUnResolvedBackendID = true;
            m_lastLinkUnResolvedBackendID = computeLastLinkUnResolvedBackendID();
            return m_lastLinkUnResolvedBackendID;
        }

        uint64_t getLastLinkAppID()
        {
            if (m_didCacheLastLinkAppID) {
                return m_lastLinkAppID;
            }
            m_didCacheLastLinkAppID = true;
            m_lastLinkAppID = computeLastLinkAppID();
            return m_lastLinkAppID;
        }

    private:
        CorrelationType computeType();
        void computeComponentLinks();
        uint64_t computeUnResolvedExitID();
        uint64_t computeSelfResolutionBackendID();
        uint64_t computeCrossAppUnResolvedBackendID();
        uint64_t computeLastLinkUnResolvedBackendID();
        uint64_t computeLastLinkAppID();

        static void parseSubHeader(const std::string& subHeader,
                                   std::vector<std::string>& parts);

        AgentLogger const m_logger;
        boost::shared_ptr<IConfigChannel> m_configChannel;

        boost::unordered_map<std::string, std::string> m_subHeaders;

        std::vector<ComponentLink> m_componentLinks;
        uint64_t m_unresolvedExitID;
        uint64_t m_selfResolutionBackendID;
        uint64_t m_crossAppUnResolvedBackendID;
        uint64_t m_lastLinkUnResolvedBackendID;
        uint64_t m_lastLinkAppID;

        CorrelationType m_correlationType : 2;

        bool m_didCacheDoNotSelfResolve : 1;
        bool m_didCacheSnapshotEnabled : 1;
        bool m_didCacheExitCallSequence : 1;
        bool m_didCacheDebugEnabled : 1;
        bool m_didCacheOriginTimestamp : 1;
        bool m_didCacheCorrelationType : 1;
        bool m_didCacheComponentLinks : 1;
        bool m_didCacheUnResolvedExitID: 1;
        bool m_didCacheSelfResolutionBackendID : 1;
        bool m_didCacheCrossAppUnResolvedBackendID : 1;
        bool m_didCacheLastLinkUnResolvedBackendID : 1;
        bool m_didCacheLastLinkAppID : 1;


        bool m_doNotSelfResolve : 1;
        bool m_snapshotEnabled : 1;
        bool m_debugEnabled : 1;

        std::string m_exitCallSequence;
        std::string m_originTimestamp;

        void parseSubHeaders(const std::string& header);
        std::string sanitizeHeader(const std::string& header);
};

/* }}} */

/**
 * Builds the outgoing correlation header. Uses CorrelationHeader class
 * constants.
 */
class CorrelationHeaderBuilder : boost::noncopyable
{
    public:

        inline CorrelationHeaderBuilder()
        {
            m_headerRoot.reserve(100);
        }

        /**
         * Append a sub-header to the correlation header string.
         * @param headerName the name of the header
         * @param headervalue the value of the header
         */
        void addSubHeader(const char* const subHeaderName, const std::string& subHeaderValue);

        /**
         * Adds the incoming component links to the header
         * @param componentLinks component links to add
         * @param ourComponentID current tier ID
         */
        void addComponentLinks(const std::vector<ComponentLink>& componentLinks,
                               const std::string& ourComponentID);

        /**
         * Builds the correlation header value for the given exit point.
         */
        std::string build(appdynamics::pb::Agent::ExitPointType exitType,
                          const std::string& toComponentID,
                          const std::string& unresolvedExitID,
                          const std::string& exitCallSequenceString,
                          bool snapshotEnabled);

        /**
         * Builds the correlation header without exit point info.
         */
        inline std::string build()
        {
            return m_headerRoot;
        }

    private:
        std::string m_headerRoot;
        std::string m_fromComponentsSequence;
        std::string m_toComponentsSequence;
        std::string m_exitTypesSequence;
};

/* {{{ class TransactionCorrelator */

/**
 * A helper class that encapsulates the logic for creating the correlation
 * header from the current context and exit call.
 *
 * All the methods are static.
 */
class TransactionCorrelator : boost::noncopyable
{
    public:
        /**
         * Generates and returns a correlation header for the current exit call.
         *
         * @param context the current transaction context
         * @param exitType the type of the current exit call
         * @param toComponentID resolved or unresolved component ID
         * @param logger the logger to use when generating the header
         */
        static std::string getCorrelationHeader(TransactionContext* context,
                                                CurrentExitCall* exitCall,
                                                const AgentLogger& logger);

        static boost::shared_ptr<CorrelationHeader>
            processContinuingTransaction(const std::string& correlationHeaderString,
                                         appdynamics::pb::Agent::EntryPointType interceptingEntryPointType,
                                         const TimePoint& startTime,
                                         const AgentLogger& logger);

        static std::string getExitCallSequenceString(TransactionContext* context,
                                                     uint64_t exitCallNumber);

        inline static void makeFullHeader(std::string& header, const std::string& headerValue)
        {
            header.clear();
            header.reserve(CorrelationHeader::MAIN_HEADER_SIZE + 2 + headerValue.size());
            header.append(CorrelationHeader::MAIN_HEADER);
            header.append(": ");
            header.append(headerValue);
        }


    private:
        static bool parseCallerChain(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                     const AgentLogger& logger);
        static void parseSubHeader(const std::string& subHeader, std::vector<std::string>& parts);
        static bool checkForUnresolvedComponentID(const boost::shared_ptr<CorrelationHeader>& corrHeader);
        static bool isCallerChainTooLong(const boost::shared_ptr<CorrelationHeader>& corrHeader);

        /**
            Checks for and handles the situation where the correlation header has an
            unresolved backend id that is currently resolved to a tier other than the tier
            of the currently instrumented PHP application.

            If this method returns true, then BT detection should be disabled and
            the correlated transaction should not be processed.

            @return true if the correlation header has an unresolved backend id that is
            currently resolved to a tier other than the tier of the currently instrumented
            PHP application, false otherwise.
         */
        static bool handleWrongUnresolvedExitID(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                                const boost::shared_ptr<TransactionMonitor>& txMonitor,
                                                const AgentLogger& logger);

        static bool handleCrossAppCorrelation(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                              const boost::shared_ptr<TransactionMonitor>& txMonitor,
                                              const AgentLogger& logger);
        static std::string getUnresolvedExitIdHeader(CurrentExitCall* exitCall);
};

/* }}} */

/* {{{ class CorrelationEmitter */

/**
 * Templated base class that can be used by sub-classes of AExitCallInterceptor
 * to add correlation support.
 *
 * Sub-classes must have the the emitCorrelationInfo method:
 *
 * void emitCorrelationInfo(TransactionContext* context,
 *                          CurrentExitCall* exitCall,
 *                          const PHPExecEnvironment* execEnv) const
 *
 * This class sub-classes ExitCallDetailHelper, so the methods required by that
 * class have to be implemented as well.

 * @param t_Base Base class the template instance should derive from.
 * @param t_Derived Class that is deriving from this template instance.
 * @param exitPointType the type of exit point the interceptor class deriving
 * for this class is associated with.
 */
template <class t_Base, class t_Derived, appdynamics::pb::Agent::ExitPointType exitPointType>
class CorrelationEmitter : public ExitCallDetailHelper<t_Base, t_Derived, exitPointType>
{
protected:
    template <typename... t_Args>
    CorrelationEmitter(t_Args...args);

    virtual void setupCorrelation(TransactionContext* context,
                                  CurrentExitCall* exitCall,
                                  const PHPExecEnvironment* phpExecEnv);

    AgentLogger m_correlationLogger;
};

template <class t_Base, class t_Derived, appdynamics::pb::Agent::ExitPointType exitPointType>
template <typename... t_Args>
CorrelationEmitter<t_Base, t_Derived, exitPointType>::CorrelationEmitter(t_Args...args)
    : ExitCallDetailHelper<t_Base, t_Derived, exitPointType>(args...)
{
    const t_Derived* const pThis = static_cast<const t_Derived*>(this);
    m_correlationLogger = getLogger(std::string(LogContext::CORRELATION) + "." + pThis->getClass());
}

template <class t_Base, class t_Derived, appdynamics::pb::Agent::ExitPointType exitPointType>
void CorrelationEmitter<t_Base, t_Derived, exitPointType>::setupCorrelation(TransactionContext* context,
                                                                            CurrentExitCall* exitCall,
                                                                            const PHPExecEnvironment* execEnv)
{
    BOOST_ASSERT(exitCall != NULL);
    if (!exitCall->isCorrelationEnabledByConfig()) {
        LOG4CXX_DEBUG(m_correlationLogger, "correlation disabled by config rule");
        return;
    }

    BOOST_ASSERT(context != NULL);

    const boost::shared_ptr<RequestContext>& requestContext = context->getRequestContext();

    if (requestContext->isFrameworkDetected() && context->getEntryPointType() == appdynamics::pb::Agent::PHP_WEB) {
        LOG4CXX_INFO(m_correlationLogger, "framework detected, but BT is still WEB, not correlating");
        return;
    }

    TransactionReporter* txReporter =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionReporter();
    if (!txReporter->receiveTransactionInfo(requestContext, false)) {
        LOG4CXX_WARN(m_correlationLogger, "did not receive BTInfoResponse, downstream snapshot disabled");
        // If we timed out, we expect the response field of the request context to still be null.
        BOOST_ASSERT(!(requestContext->getBTInfoResponse()));
    }

    const t_Derived* const pThis = static_cast<const t_Derived*>(this);

    if (exitCall->hasComponentID()) {
        pThis->emitCorrelationInfo(context, exitCall, execEnv);
    } else {
        exitCall->disablePropagation();
        pThis->emitCorrelationInfo(context, exitCall, execEnv);
    }

    // Some backends may not have actually transmitted the correlation header
    // yet, e.g. stream-based ones, but we'll set the flag anyway because there
    // is no better place to set it.
    requestContext->setCorrelationSent(true);
}

/* }}} */

#endif // __correlation_h

// vim: set fdm=marker:
