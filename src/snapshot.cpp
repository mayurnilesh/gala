/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include <boost/unordered_set.hpp>

#include "agent.h"
#include "data_interceptor.h"
#include "snapshot.h"
#include "callgraph/call_graph_collection_state.h"
#include "transactions.h"
#include "http/httpentrypoint.h"
#include "utf8_sanitize.h"

#include <boost/foreach.hpp>

const std::string SnapshotManager::name("SnapshotManager");
static const char SESSIONS_VAR_NAME[] = "_SESSION";
static const size_t SESSIONS_VAR_NAME_LEN = sizeof(SESSIONS_VAR_NAME) /
                                            sizeof(SESSIONS_VAR_NAME_LEN);
bool HTTPDataGatherer::s_didAddInterceptor = false;

HttpDataMainScriptInterceptor httpDataMainScriptInterceptor;

SnapshotManager::SnapshotManager(const AgentContext* agentContext)
    : m_enabled(true)
    , m_callgraphEnabled(true)
    , m_minExitCallTime(MIN_EXIT_CALL_DURATION)
    , m_minSQLExecTime(MIN_DB_EXIT_CALL_DURATION)
    , m_minCallGraphNodeDuration(MIN_CALLGRAPH_NODE_DURATION)
    , m_skipInternals(false)
    , m_captureRawSQL(false)
    , m_maxUniqSQLCalls(MAX_UNIQUE_SQL_CALLS)
    , m_httpDataGatherer(boost::make_shared<HTTPDataGatherer>())
    , snapshot(NULL)
{
    logger = getLogger(LogContext::SNAPSHOT);
    agentContext->getConfigChannel()->addListener(this);
}

SnapshotManager::~SnapshotManager()
{
    delete snapshot;
}

void SnapshotManager::setURL(const std::string& url)
{
    if (!isEnabled())
        return;
    if (!snapshot)
        return;
    snapshot->setURL(url);
}

void SnapshotManager::disable()
{
    m_enabled = false;
    setCallGraphEnabled(false);
    m_httpDataGatherer->setEnabled(false);
}

void SnapshotManager::enable()
{
    m_enabled = true;
    setCallGraphEnabled(true);
    m_httpDataGatherer->setEnabled(true);
}

Snapshot* SnapshotManager::startSnapshot(CallGraph::GraphCollectionState* callGraphCollectionState,
                                         bool purgeCallGraph)
{
    if (!m_enabled)
    {
        callGraphCollectionState->clear(NULL);
        return NULL;
    }

    Snapshot* newSnapshot = new Snapshot(this);

    if (purgeCallGraph)
        callGraphCollectionState->clear(newSnapshot);
    else
        callGraphCollectionState->onTransactionRestart(newSnapshot, AG(G)->timer.getTimestamp());

    // TODO: rename snapshot to m_snapshot
    delete snapshot;
    snapshot = newSnapshot;

    return snapshot;
}

void SnapshotManager::resetSnapshot(CallGraph::GraphCollectionState* callGraphCollectionState)
{
    delete snapshot;
    snapshot = NULL;
}

Snapshot* SnapshotManager::finishSnapshot(const CallGraph::GraphCollectionState* callGraphCollectionState)
{
    if (!m_enabled)
        return NULL;

    if (!snapshot)
        return NULL;

    if (logger->isTraceEnabled())
    {
        CallGraph::Graph& callGraph = snapshot->getCallGraph();
        const CallGraph::Node* rootNode = callGraph.getLastRoot();
        if (rootNode)
        {
            unsigned executionTime = 0;
            do
            {
                executionTime += rootNode->getExecutionTime();
                rootNode = rootNode->getNextSiblingNode();
            } while (rootNode);

            LOG4CXX_TRACE(logger, "** execution time:       " << executionTime << " ms");
            LOG4CXX_TRACE(logger, "** num nodes:            " << callGraph.getNodesCount());
            LOG4CXX_TRACE(logger, "** num skipped:          " << callGraphCollectionState->getNumberOfSkippedCallGraphNodes());
        }
    }
    return snapshot;
}

void SnapshotManager::configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                    const _zend_agent_globals* agentGlobals)
{
    if (configResponse.has_callgraphconfig()) {
        m_minExitCallTime = configResponse.callgraphconfig().minmethodexectime();
        m_minSQLExecTime = configResponse.callgraphconfig().minsqlexectime();
        m_skipInternals = configResponse.callgraphconfig().excludeinternalfunctions();
        m_captureRawSQL = configResponse.callgraphconfig().capturerawsql();
        m_minCallGraphNodeDuration = configResponse.callgraphconfig().minmethodexectime();
    }

    if (configResponse.datagatherers_size() > 0) {
        m_httpDataGatherer->setDataGatherers(configResponse.datagatherers());
    }

    if (bool hasNewConfig = configResponse.has_datagathererbtconfig()) {
        m_httpDataGatherer->setDataGathererBTConfig(configResponse.datagathererbtconfig());
    }
}

/**
    Helper function that converts string map into repeated NameValuePair.
*/
inline void mapToPBNameValuePair(const StringMap& map,
        ::google::protobuf::RepeatedPtrField<appdynamics::pb::Common::NameValuePair>* pbMap)
{
    BOOST_FOREACH(const auto& pair, map)
    {
        appdynamics::pb::Common::NameValuePair* pbPair = pbMap->Add();
        pbPair->set_name(pair.first);
        pbPair->set_value(sanitizeUTF8(pair.second));
    }
};

void Snapshot::mergeHttpDataIntoSnapshot(appdynamics::pb::Snapshot* pbSnapshot)
{
    appdynamics::pb::HTTPRequestData* pbData = pbSnapshot->mutable_httprequestdata();

    pbData->set_sessionid(m_httpData->getSessionId());
    mapToPBNameValuePair(m_httpData->getHttpParams(),
                         pbData->mutable_httpparams());
    mapToPBNameValuePair(m_httpData->getSessionEntries(),
                         pbData->mutable_sessionentries());
    mapToPBNameValuePair(m_httpData->getHeaders(),
                         pbData->mutable_headers());
    mapToPBNameValuePair(m_httpData->getCookies(),
                         pbData->mutable_cookies());

    if (!m_httpData->getRequestMethod().empty())
        pbData->set_requestmethod(m_httpData->getRequestMethod());
    if (!m_httpData->getResponseCode().empty())
        pbData->set_responsecode(m_httpData->getResponseCode());
}

void HTTPDataGatherer::setEnabled(bool enabled)
{
    if (enabled && !s_didAddInterceptor) {
        AG(icept_engine)->addInterceptorForCallable(CallableInfo(ROOT_SYMBOL),
                                                    &httpDataMainScriptInterceptor);
        s_didAddInterceptor = true;
    }
    m_enabled = enabled;
    LOG4CXX_DEBUG(m_logger, "HTTP data gathering is " << (enabled ? "enabled" : "disabled"));
}

void HTTPDataGatherer::setDataGatherers(const google::protobuf::RepeatedPtrField<appdynamics::pb::DataGatherer>& dataGatherers)
{
    m_dataGatherers.clear();
    BOOST_FOREACH(const auto& it, dataGatherers)
    {
        // Skip non-HTTP gatherers
        if (it.type() != appdynamics::pb::DataGatherer::HTTP)
            continue;
        boost::shared_ptr<appdynamics::pb::DataGatherer> gatherer(boost::make_shared<appdynamics::pb::DataGatherer>());
        gatherer->CopyFrom(it);
        m_dataGatherers[it.gathererid()] = gatherer;
    }
}

void HTTPDataGatherer::setDataGathererBTConfig(const appdynamics::pb::DataGathererBTConfig& dataGathererBTConfig)
{
    m_btToGatherersMap.clear();
    BOOST_FOREACH(const auto& it, dataGathererBTConfig.btconfig())
    {
        int64_t const btID = it.btid();
        BOOST_FOREACH(const auto gathererID, it.gathererids())
        {
            // Skip non-HTTP gatherers
            if (m_dataGatherers.find(gathererID) == m_dataGatherers.end())
                continue;
            m_btToGatherersMap.insert(std::make_pair(btID, gathererID));
        }
    }
}

void HTTPDataGatherer::process(const PHPExecEnvironment* execEnv,
                               const TransactionContext* context) {
    if (!m_enabled)
        return;
    // If there's no transaction context, this transaction is being excluded or deleted, so ignore process request.
    if (!context)
        return;
    // Don't process unregistered transactions
    if (context->getTransaction()->getID() == AgentTransaction::NULL_BT_ID)
        return;

    const boost::shared_ptr<HTTPPayload>& payload = execEnv->getHTTPPayload();
    if (!payload)
        return;

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    const TransactionRegistry* registry = txMonitor->getTransactionRegistry();
    auto gatherers = m_btToGatherersMap.equal_range(context->getTransaction()->getID());
    if (gatherers.first == gatherers.second) {
        LOG4CXX_DEBUG(m_logger, "no HTTP gatherers found for BT id " << context->getTransaction()->getID());
        return;
    }

    boost::unordered_set<std::string> cookiesToGather;
    boost::unordered_set<std::string> headersToGather;
    boost::unordered_set<std::string> sessionKeysToGather;

    StringMap httpParams;
    StringMap cookies;
    StringMap headers;
    StringMap sessionEntries;

    std::string sessionId;

    ZValPointerArray getParamsArray(payload->getGetParams());
    ZValPointerArray postParamsArray(payload->getPostParams());

    // Loop for accumulating all cookies, headers, and session keys. Note that
    // http params are excluded. This is because http params are the only data points
    // that have independent display names.
    for (auto itGatherer = gatherers.first; itGatherer != gatherers.second; ++itGatherer)
    {
        LOG4CXX_DEBUG(m_logger, "Processing HTTP data gatherer id " << itGatherer->second);
        auto itGathererConfig = m_dataGatherers.find(itGatherer->second);
        if (itGathererConfig == m_dataGatherers.end()) {
            LOG4CXX_DEBUG(m_logger, "Data Gatherer config inconsistent, perhaps the controller-sent config was not atomically updated?");
            continue;
        }

        const boost::shared_ptr<appdynamics::pb::DataGatherer>& gathererConfig = itGathererConfig->second;
        if (gathererConfig->type() != appdynamics::pb::DataGatherer::HTTP) {
            LOG4CXX_DEBUG(m_logger, "Expected HTTP Data Gatherer type HTTP, actual type is not HTTP. Skipping this data gatherer, id:["
                << boost::lexical_cast<std::string>(gathererConfig->gathererid()));
            continue;
        }

        const appdynamics::pb::HTTPDataGathererConfig& httpConfig = gathererConfig->httpdatagathererconfig();

        std::copy(httpConfig.cookienames().begin(),
                  httpConfig.cookienames().end(),
                  std::inserter(cookiesToGather, cookiesToGather.begin()));
        std::copy(httpConfig.headers().begin(),
                  httpConfig.headers().end(),
                  std::inserter(headersToGather, headersToGather.begin()));
        std::copy(httpConfig.sessionkeys().begin(),
                  httpConfig.sessionkeys().end(),
                  std::inserter(sessionKeysToGather, sessionKeysToGather.begin()));

        if (getParamsArray)
            gatherHTTPRequestParameters(getParamsArray, httpConfig.requestparams(), httpParams);
        if (postParamsArray)
            gatherHTTPRequestParameters(postParamsArray, httpConfig.requestparams(), httpParams);

        // Gather session entries
        // If session is loaded and we can find
        // its globals, gather the requested session values.
        if (httpConfig.gathersessionid() && !payload->getSessionId(&sessionId))
            sessionId = "";
    }

    ZValPointerArray sessionArray(payload->getSessionArray());
    if (sessionArray) {
        BOOST_FOREACH(auto sessionKey, sessionKeysToGather)
        {
            ZValPointerAny sessionVal(
                    sessionArray.findEntryByName<ZValPointerAny>(
                        sessionKey.c_str(), sessionKey.length()));
            if (sessionVal)
                sessionEntries[sessionKey] = StringHelpers::prettyPrint(sessionVal);
        }
    }
    // Gather cookies
    if (cookiesToGather.size() > 0) {
        ZValPointerArray cookiesArray(payload->getCookies());
        if (cookiesArray) {
            BOOST_FOREACH(auto cookieName, cookiesToGather)
            {
                ZValPointerString cookieValue(cookiesArray.findEntryByName<ZValPointerString>(cookieName.c_str(), cookieName.length()));
                if (cookieValue)
                    cookies[cookieName] = cookieValue.getStringValue();
            }
        }
    }

    // Gather headers
    BOOST_FOREACH(auto headerName, headersToGather)
    {
        std::string headerValue;
        if (payload->getHeaderValue(headerName, &headerValue))
            headers[headerName] = headerValue;
    }

    boost::shared_ptr<HTTPData> httpData(
        txMonitor->getSnapshotManager()
                 ->getSnapshot()
                 ->getHttpData()
    );

    const char* const requestMethod = SG(request_info).request_method;
    if (requestMethod) {
        httpData->setRequestMethod(requestMethod);
    }
    httpData->setResponseCode(SG(sapi_headers).http_response_code);

    httpData->setSessionId(sessionId);
    httpData->setHttpParams(httpParams);
    httpData->setCookies(cookies);
    httpData->setHeaders(headers);
    httpData->setSessionEntries(sessionEntries);
}

void HTTPDataGatherer::gatherHTTPRequestParameters(const ZValPointerArray& paramsArray,
                                                   const google::protobuf::RepeatedPtrField<appdynamics::pb::HTTPRequestParameter>& paramsToGather,
                                                   StringMap& target)
{
    if (!paramsArray)
        return;

    // Iff the first param to gather is '*', then ignore all display names
    // and use param keys as display names, param values as display values.
    if (paramsToGather.size() == 0)
        return;
    const auto gatherBegin = paramsToGather.begin();
    if (gatherBegin->name() == "*") {
        gatherAllHTTPRequestParameters(paramsArray, target);
        return;
    }

    ZValPointerAny paramValue;
    BOOST_FOREACH(const auto& it, paramsToGather)
    {
        if (paramsArray.findEntryByName(it.name().c_str(),
                                        it.name().length(),
                                        &paramValue))
        {
            std::string value(StringHelpers::prettyPrint(paramValue));
            target[it.displayname()] = value;
        }
    }
}

void HTTPDataGatherer::gatherAllHTTPRequestParameters(const ZValPointerArray& paramsArray,
                                                      StringMap& target)
{
    std::string currDisplayName;

    // Iterate through HTTP params array.
    for (ZValPointerArray::const_iterator itParam(paramsArray.begin());
            itParam != paramsArray.end();
            ++itParam)
    {
        BOOST_ASSERT((*itParam).keyIsLong() || (*itParam).keyIsString());
        // Set the display name as the param key.
        currDisplayName = (*itParam).keyIsLong() ?
            boost::lexical_cast<std::string>((*itParam).getKeyAsLong()) :
            (*itParam).getKeyAsString();
        // Set the display value as the param value.
        target[currDisplayName] = StringHelpers::prettyPrint((*itParam).getValue());
    }
    return;
}
