/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#include "main_script_interceptor.h"

#include "agent.h"
#include "eum/config.h"
#include "request_context.h"

namespace EUM
{
    bool MainScriptInterceptor::isActive(const PHPExecEnvironment* execEnv) const
    {
        return AG(kernel)->getAgentContext()->getEUMConfig()->enabled();
    }

    void MainScriptInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                              void* state)
    {
        phpExecEnv->getAgentGlobals().request_context->eumContext().onMainScriptDone();
    }
}
