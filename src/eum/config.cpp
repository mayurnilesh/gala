#include "eum/config.h"
#include "eum/main_script_interceptor.h"
#include "configs.h"
#include "agent.h"
#include <boost/make_shared.hpp>


namespace EUM
{
    bool Config::s_didAddInterceptor = false;
    MainScriptInterceptor mainScriptInterceptor;

    Config::Config(InterceptionEngine* interceptionEngine,
                   const boost::shared_ptr<IConfigChannel>& configChannel)
        : m_interceptionEngine(interceptionEngine)
    {
        configChannel->addListener(this);
    }

    void Config::configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                               const struct _zend_agent_globals* agentGlobals)
    {
        if (agentGlobals->G->dit_flags.disableEUM)
            return;
        if (configResponse.has_eumconfig()) {
            boost::shared_ptr<appdynamics::pb::EUMConfig> eumConfig(boost::make_shared<appdynamics::pb::EUMConfig>());
            eumConfig->CopyFrom(configResponse.eumconfig());
            configure(eumConfig);
        }
    }

    void Config::configure(const boost::shared_ptr<appdynamics::pb::EUMConfig>& config)
    {
        m_config = config;
        if (!s_didAddInterceptor) {
            m_interceptionEngine->addInterceptorForCallable(CallableInfo(ROOT_SYMBOL),
                                                            &mainScriptInterceptor);
            s_didAddInterceptor = true;
        }
    }
}
