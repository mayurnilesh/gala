/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "eum/context.h"
#include <boost/lexical_cast.hpp>

extern "C"
{
#include <php.h>
#include <ext/standard/php_standard.h>
}

#include "timer.h"
#include "intercept.h"
#include "http/httpentrypoint.h"
#include "request_context.h"
#include "PHPAgentProtobufs.pb.h"
#include "eum/config.h"

static const char ADRUM_MASTER_COOKIE_NAME[] = "ADRUM_BT";
static const size_t ADRUM_MASTER_COOKIE_NAME_LEN = sizeof(ADRUM_MASTER_COOKIE_NAME) - 1;

static const char ADRUM_PREFIX[] = "ADRUM_";
static const size_t ADRUM_PREFIX_LEN = sizeof(ADRUM_PREFIX) - 1;

static const char CLIENT_REQUEST_GUID_LONG[] = "clientRequestGUID";
static const char CLIENT_REQUEST_GUID_SHORT[] = "g";

static const char BT_ID_LONG[] = "btId";
static const char BT_ID_SHORT[] = "i";

static const char BT_ART_LONG[] = "btERT";
static const char BT_ART_SHORT[] = "e";

static const char BT_DURATION_LONG[] = "btDuration";
static const char BT_DURATION_SHORT[] = "d";

static const char SERVER_SNAPSHOT_TYPE_LONG[] = "serverSnapshotType";
static const char SERVER_SNAPSHOT_TYPE_SHORT[] = "s";

static const char HAS_ENTRY_POINT_ERRORS_LONG[] = "hasEntryPointErrors";
static const char HAS_ENTRY_POINT_ERRORS_SHORT[] = "h";

static const char GLOBAL_ACCOUNT_NAME_LONG[] = "globalAccountName";
static const char GLOBAL_ACCOUNT_NAME_SHORT[] = "n";

static const size_t MAX_DECIMAL_DIGITS = 20; // 2^64 needs 20 decimal digits

static inline std::string urlEncode(const std::string s)
{
#if PHP_VERSION_ID >= 70000
    zend_string* encodedBufferZStr = php_url_encode(s.c_str(), s.length());
    if (!encodedBufferZStr)
        return std::string();

    std::string result(ZSTR_VAL(encodedBufferZStr), ZSTR_LEN(encodedBufferZStr));
    zend_string_release(encodedBufferZStr);
#else
    int encodedLength = 0;
    char* encodedBuffer = php_url_encode(s.c_str(), s.length(), &encodedLength);
    if (!encodedBuffer)
        return std::string();

    std::string result(encodedBuffer, encodedLength);
    efree(encodedBuffer);
#endif
    return result;
}

/**
    Convenience wrapper around php_setcookie.
 */
static inline void setCookie(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                             const char* cookieName,
                             size_t cookieNameLen,
                             const std::string& value,
                             bool secure)
{
    // See comment in EUMContext.java about:
    // CORE-19525: RUM: server agent fails to stale delete cookies written for another path
    static const char path[] = "/";
    static const size_t pathLen = sizeof(path) - 1;

    // We already url encoded the value, so we don't
    // need setcookie to do it again for us.
    static const int URL_NOENCODE = 0;

    // We want javascript in the page to be able to read the cookie.
    static const int HTTP_ONLY = 0;

#if PHP_VERSION_ID >= 70000
    zend_string* cookieNameZStr = zend_string_init(cookieName, cookieNameLen, false);
    zend_string* valueZStr = zend_string_init(value.c_str(), value.length(), false);
    zend_string* pathZStr = zend_string_init(path, pathLen, false);
    execEnv->callWithTSRMContext(php_setcookie,
                                 cookieNameZStr,
                                 valueZStr,
                                 time(NULL) + 30,
                                 pathZStr,
                                 reinterpret_cast<zend_string*>(NULL),
                                 secure,
                                 URL_NOENCODE,
                                 HTTP_ONLY);
    zend_string_release(cookieNameZStr);
    zend_string_release(valueZStr);
    zend_string_release(pathZStr);
#else
    execEnv->callWithTSRMContext(php_setcookie,
                                 const_cast<char*>(cookieName),
                                 cookieNameLen,
                                 const_cast<char*>(value.c_str()),
                                 value.length(),
                                 time(NULL) + 30,
                                 const_cast<char*>(&(path[0])),
                                 pathLen,
                                 reinterpret_cast<char*>(NULL),
                                 0,
                                 secure,
                                 URL_NOENCODE,
                                 HTTP_ONLY);
#endif
}

/**
    Convenience wrapper around php_setcookie that deletes a cookie.
 */
static inline void deleteCookie(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                                const std::string& name)
{
    // See comment in EUMContext.java about:
    // CORE-19525: RUM: server agent fails to stale delete cookies written for another path
    static const char path[] = "/";
    static const size_t pathLen = sizeof(path) - 1;

    // Cookies that we set should be available to http and https.
    static const int HTTPS_ONLY = 0;

    // We are deleting the cookie, so there is nothing to encode.
    static const int URL_NOENCODE = 0;

    // We want javascript in the page to be able to read the cookie.
    static const int HTTP_ONLY = 0;

    // A unix time which is old enough to force the browser
    // to expire the cookie, but not 0 because that means the cookie
    // is a session cookie.
    static const time_t MIN_COOKIE_EXPIRE_TIME = 1;
#if PHP_VERSION_ID >= 70000
    zend_string* nameZStr = zend_string_init(name.c_str(), name.length(), false);
    zend_string* pathZStr = zend_string_init(path, pathLen, false);
    execEnv->callWithTSRMContext(php_setcookie,
                                 nameZStr,
                                 reinterpret_cast<zend_string*>(NULL),
                                 MIN_COOKIE_EXPIRE_TIME,
                                 pathZStr,
                                 reinterpret_cast<zend_string*>(NULL),
                                 HTTPS_ONLY,
                                 URL_NOENCODE,
                                 HTTP_ONLY);
    zend_string_release(nameZStr);
    zend_string_release(pathZStr);
#else
    execEnv->callWithTSRMContext(php_setcookie,
                                 const_cast<char*>(name.c_str()),
                                 name.length(),
                                 reinterpret_cast<char*>(NULL),
                                 0,
                                 MIN_COOKIE_EXPIRE_TIME,
                                 const_cast<char*>(&(path[0])),
                                 pathLen,
                                 reinterpret_cast<char*>(NULL),
                                 0,
                                 HTTPS_ONLY,
                                 URL_NOENCODE,
                                 HTTP_ONLY);
#endif
}

/**
    Convenience wrapper around sapi_header_op.
 */
static inline void setHeader(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                             const std::string& name,
                             const std::string& value)
{
    std::string headerLineStr;
    headerLineStr.reserve(name.length() + value.length() + 2);
    headerLineStr = name;
    headerLineStr += ": ";
    headerLineStr += urlEncode(value);
    sapi_header_line headerLine = { 0, 0, 0 };
    headerLine.line = const_cast<char*>(headerLineStr.c_str());
    headerLine.line_len = headerLineStr.length();
    execEnv->callWithTSRMContext(sapi_header_op,
                                 SAPI_HEADER_REPLACE,
                                 &headerLine);
}

static inline bool is381RUMCookieName(const std::string& name)
{
    if (name.length() != ADRUM_MASTER_COOKIE_NAME_LEN)
        return false;
    if (name.compare(0,
                     ADRUM_MASTER_COOKIE_NAME_LEN,
                     ADRUM_MASTER_COOKIE_NAME,
                     ADRUM_MASTER_COOKIE_NAME_LEN) != 0)
        return false;
    return true;
}

// See if the cookie name is of the form:
// ADRUM_[0-9]+_[0-9]+
static inline bool is37XRUMCookieName(const std::string& name)
{
    // There must be at least three digits with '_' between
    // them for the cookie to be a RUM cookie.
    if (name.length() < ADRUM_PREFIX_LEN + 5)
        return false;
    if (name.compare(0, ADRUM_PREFIX_LEN, ADRUM_PREFIX, ADRUM_PREFIX_LEN) != 0)
        return false;

    std::string::const_iterator i = name.begin() + ADRUM_PREFIX_LEN;
    std::string::const_iterator end = name.end();
    // skip past the first set of digits.
    while ((i != end) && (*i != '_')) {
        if (!std::isdigit(*i))
            return false;
        ++i;
    }

    if (i == end)
        return false;

    // skip past the '_'.
    ++i;

    while ((i != end) && (*i != '_')) {
        if (!std::isdigit(*i))
            return false;
        ++i;
    }

    if (i == end)
        return false;

    // skip past the '_'.
    ++i;

    // we can assert we are not at the end of the string because we checked the length of
    // name at the top of this function.
    BOOST_ASSERT(i != end);

    do {
        if (!std::isdigit(*i))
            return false;
        ++i;
    } while (i != end);

    return true;
}

static inline bool isRUMCookieName(const std::string& name)
{
    if (is381RUMCookieName(name))
        return true;

    if (is37XRUMCookieName(name))
        return true;
    return false;
}

namespace EUM
{
    Context::Context(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                     const boost::shared_ptr<const Config>& config)
        : m_config(config)
        , m_counter(0)
        , m_logger(::getLogger(std::string(LogContext::AGENT) + ".EUMContext"))
        , m_isAJAX(isAJAX(execEnv))
        , m_isMobile(isMobile(execEnv))
        , m_guidComputed(false)
        , m_masterCookieValueInitialized(false)
        , m_headersSent(false)
        , m_sendBTDuration(false)
        , m_setHTTPSCookiesComputed(false)
        , m_setHTTPSCookies(false)
    {
    }

    void Context::onSendHeaders(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                                const boost::shared_ptr<RequestContext>& requestContext,
                                const Timer& timer)
    {
        if (m_headersSent)
            return;
        m_headersSent = true;

        LOG4CXX_TRACE(m_logger, "sendHeaders begin");

        // See comment in EUMContext.java about:
        // CORE-18055: RUM: Navigating from non-instrumented page to
        // instrumented page with same referrer causes problems
        clearCookies(execEnv);
        if (m_isAJAX)
            onSendHeaders<true>(execEnv, requestContext, timer);
        else
            onSendHeaders<false>(execEnv, requestContext, timer);

        LOG4CXX_TRACE(m_logger, "sendHeaders end");
    }

    void Context::onReportSnapshot(appdynamics::pb::Snapshot* snapshot)
    {
        if (m_config->enabled())
            snapshot->set_eumguid(getGUID());
    }

    bool Context::isAJAX(const boost::shared_ptr<const PHPExecEnvironment>& execEnv)
    {
        boost::shared_ptr<HTTPPayload> httpPayload(execEnv->getHTTPPayload());

        std::string adrumHeaderValue;

        if (!httpPayload->getHeaderValue("ADRUM", &adrumHeaderValue))
            return false;

        if (adrumHeaderValue.find("isAjax:true") == std::string::npos)
            return false;
        return true;
    }

    bool Context::isMobile(const boost::shared_ptr<const PHPExecEnvironment>& execEnv)
    {
        boost::shared_ptr<HTTPPayload> httpPayload(execEnv->getHTTPPayload());

        std::string adrumHeaderValue;

        if (!httpPayload->getHeaderValue("ADRUM_1", &adrumHeaderValue))
            return false;

        if (adrumHeaderValue.find("isMobile:true") == std::string::npos)
            return false;
        return true;
    }

    template <bool isAjax>
    inline void Context::setMetadataField(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                                          const char* const longName,
                                          const char* const shortName,
                                          const std::string& value)
    {
        LOG4CXX_TRACE(m_logger, "Setting meta-data name: " << longName << " value: " << value);

        std::string metadataString;
        const char* const name = (m_isMobile ? longName : shortName);
        size_t nameLen = strlen(name);
        metadataString.reserve(nameLen + value.length() + 1);
        metadataString.append(name, nameLen);
        metadataString += ':';
        metadataString += value;

        if (isAjax) {
            std::string fieldName;
            fieldName.reserve(ADRUM_PREFIX_LEN + 1 + MAX_DECIMAL_DIGITS);
            fieldName = ADRUM_PREFIX;
            fieldName += boost::lexical_cast<std::string>(m_counter);
            ++m_counter;
            setHeader(execEnv, fieldName, metadataString);
        }
        else {
            addSubCookie(execEnv, metadataString);
        }
    }

    template <bool isAjax>
    void Context::onSendHeaders(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                                const boost::shared_ptr<RequestContext>& requestContext,
                                const Timer& timer)
    {
        boost::shared_ptr<TransactionMonitor> txMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor();
        TransactionContext* txContext = txMonitor->getTransactionContext();
        if (!txContext) {
            LOG4CXX_DEBUG(m_logger, "headers flushed with null txContext guid:"
                                <<  getGUID() << " url: " << execEnv->getHTTPPayload()->getURL());
            return;
        }

        if (txContext->isContinuingTransaction()) {
            if (txContext->isCurrentTransactionRegistered()) {
                LOG4CXX_TRACE(m_logger, "headers flushed for registered continuing transaction: "
                                        << txContext->getTransaction()->getID()
                                        << " url: "
                                        << execEnv->getHTTPPayload()->getURL());
            }
            else {
                LOG4CXX_TRACE(m_logger, "headers flushed for unregistered continuing transaction: "
                                        << txContext->getTransaction()->getName()
                                        << " url: "
                                        << execEnv->getHTTPPayload()->getURL());
            }
            return;
        }

        setMetadataField<isAjax>(execEnv,
                                 CLIENT_REQUEST_GUID_LONG,
                                 CLIENT_REQUEST_GUID_SHORT,
                                 getGUID());

        if (m_config->getConfig()->has_globalaccountname()) {
            setMetadataField<isAjax>(execEnv,
                                     GLOBAL_ACCOUNT_NAME_LONG,
                                     GLOBAL_ACCOUNT_NAME_SHORT,
                                     m_config->getConfig()->globalaccountname());
        }

        if (txContext->isCurrentTransactionRegistered()) {
            int64_t const btId = txContext->getTransaction()->getID();
            std::string btIdString(boost::lexical_cast<std::string>(btId));
            setMetadataField<isAjax>(execEnv,
                                     BT_ID_LONG,
                                     BT_ID_SHORT,
                                     btIdString);
        }

        uint64_t const durationMS =
            timer.getElapsedTime(txContext->getStartTime().getTimestamp()) / 1000;

        if (m_sendBTDuration) {
            std::string durationStr(boost::lexical_cast<std::string>(durationMS));
            setMetadataField<isAjax>(execEnv,
                                     BT_DURATION_LONG,
                                     BT_DURATION_SHORT,
                                     durationStr);
        }
        else {
            LOG4CXX_DEBUG(m_logger, "headers flushed before main script finished, guid:"
                                <<  getGUID() << " bt:" << txContext->getTransaction()->getID());
        }

        TransactionReporter* const txReporter = txMonitor->getTransactionReporter();
        if (txReporter->receiveTransactionInfo(requestContext, false)) {
            bool willSendSnapshot =
                requestContext->evaluateSnapshotPolicy(durationMS, false, NULL) != NULL;
            if (willSendSnapshot) {
                setMetadataField<isAjax>(execEnv,
                                         SERVER_SNAPSHOT_TYPE_LONG,
                                         SERVER_SNAPSHOT_TYPE_SHORT,
                                         "f");
            }

            if (m_uncaughtEntryPointException) {
                setMetadataField<isAjax>(execEnv,
                                         HAS_ENTRY_POINT_ERRORS_LONG,
                                         HAS_ENTRY_POINT_ERRORS_SHORT,
                                         "e");
            }

            int64_t averageResponseTimeForLastMinute;
            if (requestContext->getAverageResponseTimeForLastMinute(&averageResponseTimeForLastMinute)) {
                std::string artForLastMinute(boost::lexical_cast<std::string>(averageResponseTimeForLastMinute));
                setMetadataField<isAjax>(execEnv,
                                         BT_ART_LONG,
                                         BT_ART_SHORT,
                                         artForLastMinute);
            }
        }
        else {
            LOG4CXX_WARN(m_logger, "failed to receive BT info response before headers flushed, guid:"
                                   <<  getGUID() << " bt:" << txContext->getTransaction()->getID());
        }

        if (!isAjax && m_masterCookieValueInitialized) {
            std::string const encodedMasterCookieValue(urlEncode(m_masterCookieValue));
            setCookie(execEnv,
                      ADRUM_MASTER_COOKIE_NAME,
                      ADRUM_MASTER_COOKIE_NAME_LEN,
                      encodedMasterCookieValue,
                      secureCookies(execEnv));
        }
    }

    const std::string& Context::getGUID()
    {
        if (m_guidComputed)
            return m_guid;
        m_guidComputed = true;
        m_guid = AG(kernel)->getAgentContext()->getTransactionMonitor()->getGUIDMaker().getAndIncrement();
        return m_guid;
    }

    void Context::addSubCookie(const boost::shared_ptr<const PHPExecEnvironment>& execEnv, const std::string value)
    {
        if (!m_masterCookieValueInitialized) {
            // Totally arbitrary.
            m_masterCookieValue.reserve(512);
            m_masterCookieValueInitialized = true;
            boost::shared_ptr<HTTPPayload> httpPayload(execEnv->getHTTPPayload());
            size_t const referrerLength = httpPayload->getReferrer().length();
            m_masterCookieValue += "R:";
            m_masterCookieValue += boost::lexical_cast<std::string>(referrerLength);
        }
        m_masterCookieValue += "|";
        m_masterCookieValue += value;
    }

    void Context::clearCookies(const boost::shared_ptr<const PHPExecEnvironment>& execEnv)
    {
        // See comment in EUMContext.java about:
        // CORE-18055: RUM: Navigating from non-instrumented page to
        // instrumented page with same referrer causes problems

        boost::shared_ptr<HTTPPayload> httpPayload(execEnv->getHTTPPayload());
        ZValPointerArray const cookiesArray(httpPayload->getCookies());
        ZValPointerArray::const_iterator const cookiesEnd = cookiesArray.end();

        // Copy the cookie names into a std::vector so we don't
        // have to think about whether it is safe to call
        // php_setcookie while enumerating the php array containing
        // all the cookies.
        std::vector<std::string> cookiesToClear;
        cookiesToClear.reserve(cookiesArray.size());

        for (ZValPointerArray::const_iterator it = cookiesArray.begin(); it != cookiesEnd; ++it) {
            std::string cookieName((*it).getKeyAsString());
            if (isRUMCookieName(cookieName))
                cookiesToClear.push_back(cookieName);
        }

        std::vector<std::string>::const_iterator const cookiesToClearEnd = cookiesToClear.end();
        for (std::vector<std::string>::const_iterator it = cookiesToClear.begin();
             it != cookiesToClearEnd;
             ++it) {
            LOG4CXX_TRACE(m_logger, "Deleting cookie:" << *it);
            deleteCookie(execEnv, *it);
        }
    }

    bool Context::secureCookies(const boost::shared_ptr<const PHPExecEnvironment>& execEnv)
    {
        if (m_setHTTPSCookiesComputed)
            return m_setHTTPSCookies;

        m_setHTTPSCookiesComputed = true;
        m_setHTTPSCookies = execEnv->getHTTPPayload()->isHTTPs();
        return m_setHTTPSCookies;
    }
}
