/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __eum_config_h
#define __eum_config_h

#include "config_listener.h"

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#include "PHPAgentProtobufs.pb.h"

class InterceptionEngine;
class IConfigChannel;

namespace EUM
{
    /**
        Holds EUM configuration and listens for changes to EUM configuration.
     */
    class Config : IConfigListener
                 , boost::noncopyable
    {
    public:
        Config(InterceptionEngine* interceptionEngine,
               const boost::shared_ptr<IConfigChannel>& configChannel);

        inline bool enabled() const { return m_config && m_config->enabled(); }

        inline const boost::shared_ptr<appdynamics::pb::EUMConfig>& getConfig() const
        { return m_config; }

    private:
        virtual void configChanged(const appdynamics::pb::ConfigResponse&,
                                   const struct _zend_agent_globals* agentGlobals);

        void configure(const boost::shared_ptr<appdynamics::pb::EUMConfig>& config);

        InterceptionEngine* const m_interceptionEngine;
        boost::shared_ptr<appdynamics::pb::EUMConfig> m_config;
        static bool s_didAddInterceptor;
    };
}

#endif
