/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __eum_context_h
#define __eum_context_h

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <string>

#include "agent_logger.h"

class Timer;
class PHPExecEnvironment;
class RequestContext;

namespace appdynamics
{
    namespace pb
    {
        class Snapshot;
    }
}

namespace EUM
{
    class Config;

    /**
        Keeps track of EUM state in a php request.
     */
    class Context : boost::noncopyable
    {
    public:
        Context(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                const boost::shared_ptr<const Config>& config);

        /**
            Called when PHP is flushing http response headers to its client.
            @param execEnv PHPExecEnvironment for the current request.
            @param requestContext context for the current php request.
            @param timer Timer instance used to compute BT execution time.
         */
        void onSendHeaders(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                           const boost::shared_ptr<RequestContext>& requestContext,
                           const Timer& timer);

        /**
            Called by entrypoint.cpp to determine if the BT name can change.  If
            have already sent out EUM headers, we can't change the BT name.
            @return true if HTTP response headers have been sent, false otherwise.
         */
        bool headersSent() const { return m_headersSent; }

        /**
            Adds the EUM request GUID to the specified snapshot protobuf.
            @param snapshot Snapshot to add the EUM request GUID to.
         */
        void onReportSnapshot(appdynamics::pb::Snapshot* snapshot);

        /**
            Called by the MainScriptInterceptor when the main php script of a request
            has finished executing.  If this is called before onSendHeaders, we can
            add a BT time cookie/header to the response headers.
            <p>
            If this is called after onSendHeaders than no BT time cookie/header will
            be sent.
            <p>
            If a shutdown callback or destructor causes the HTTP headers to be sent *and*
            another shutdown callback or destructor after that takes a significant amount
            of time the server BT time will be different than the BT time reported in the
            BT time cookie/header.
         */
        void onMainScriptDone() { m_sendBTDuration = true; }

        /**
            Called by AEntryPointInterceptor when there is an uncaught exception
            in the entry point.
         */
        void onUncaughtEntryPointException() { m_uncaughtEntryPointException = true; }

    private:
        static bool isAJAX(const boost::shared_ptr<const PHPExecEnvironment>& execEnv);
        static bool isMobile(const boost::shared_ptr<const PHPExecEnvironment>& execEnv);

        /**
            Template that is used to generate code to set cookies/headers for EUM
            metadata.  This template can only be called from context.cpp.
            @param isAjax if true metadata is sent in HTTP headers, otherwise metadata
            is sent via cookies.
            @param execEnv PHPExecEnvironment for the current php request.
            @param requestContext Context for the current php request.
            @param timer Timer to be used to compute BT time.
         */
        template<bool isAjax>
        void onSendHeaders(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                           const boost::shared_ptr<RequestContext>& requestContext,
                           const Timer& timer);

        /**
            Template that is used to add meta data to the http response headers.
            @param isAjax if true an HTTP response header generated, otherwise a
            cookie is set.
            @param execEnv PHPExecEnvironment for the current php request.
            @param longName long name of the metadata field to set
            @param shortName short name of the metadata field to set.
            @param value Value of the metadata field.
         */
        template<bool isAjax>
        void setMetadataField(const boost::shared_ptr<const PHPExecEnvironment>& execEnv,
                              const char* const longName,
                              const char* const shortName,
                              const std::string& value);

        /**
            Lazily computes and EUM guid for the current php request.
            @return GUID as a string.
         */
        const std::string& getGUID();

        /**
            Adds a subordinate cookie to the master EUM cookie.
         */
        void addSubCookie(const boost::shared_ptr<const PHPExecEnvironment>& execEnv, const std::string value);

        /**
            Clear any pre-existing EUM cookies.
            @see CORE-18055: RUM: Navigating from non-instrumented page to instrumented page with same referrer causes problems
         */
        void clearCookies(const boost::shared_ptr<const PHPExecEnvironment>& execEnv);

        /**
            Lazily computes whether or not we need to set the secure flag on any cookies
            we set.
         */
        bool secureCookies(const boost::shared_ptr<const PHPExecEnvironment>& execEnv);

        std::string m_guid;
        std::string m_masterCookieValue;

        boost::shared_ptr<const Config> const m_config;
        unsigned int m_counter;
        AgentLogger m_logger;
        bool m_isAJAX : 1;
        bool m_isMobile : 1;
        bool m_guidComputed : 1;
        bool m_masterCookieValueInitialized : 1;
        bool m_headersSent : 1;
        bool m_sendBTDuration : 1;
        bool m_uncaughtEntryPointException : 1;
        bool m_setHTTPSCookiesComputed : 1;
        bool m_setHTTPSCookies : 1;
    };
}

#endif
