/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __main_script_interceptor_h
#define __main_script_interceptor_h

#include "intercept.h"

namespace EUM
{
    /**
        Interceptor used by EUM to determine if the main php script has finished
        before http response headers have been flushed.
        <p>
        The EUM context should only send BT time to the http client if the main script
        finishes before the http headers are flushed, so that the BT time we send to the
        client is at least plausible.
        <p>
        This interceptor only does anything on onCallableEnd.
     */
    class MainScriptInterceptor : public AMethodInterceptor
    {
    public:
        inline MainScriptInterceptor()
            : AMethodInterceptor("EUM::MainScriptInterceptor",
                                 InterceptorRegistry::EUM_MainScriptInterceptor_ID)
            {
            }

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv) { return NULL; }

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const { return false; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
        virtual bool shouldCallOnMethodEnd() const { return true; }

        bool isActive(const PHPExecEnvironment* execEnv) const;
    };

}

#endif
