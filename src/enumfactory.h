#ifndef __ENUMFACTORY_H
#define __ENUMFACTORY_H

// expansion macro for enum value definition
#define ENUM_VALUE(name,label,assign) name assign,

// expansion macro for enum to label conversion
#define ENUM_CASE(name,label,assign) case name: return label;

// expansion macro for label to enum conversion
#define ENUM_STRCMP(name,label,assign) if (!strcmp(str,label)) return name;

/// declare the access function and define enum values
#define DECLARE_ENUM(EnumType,ENUM_DEF) \
  enum EnumType { \
    ENUM_DEF(ENUM_VALUE) \
    EnumType##_Invalid = -1, \
  }; \
  template <class T> const char* enum2str(T) __attribute__ ((visibility ("hidden"))); \
  template <class T> T str2enum(const char*) __attribute__ ((visibility ("hidden"))); \
  template <class T> T str2enum(const std::string&) __attribute__ ((visibility ("hidden")));

/// define the access function names
#define DEFINE_ENUM(EnumType,ENUM_DEF) \
  template <> const char* enum2str<EnumType>(EnumType value) \
  { \
    switch(value) \
    { \
      ENUM_DEF(ENUM_CASE) \
      default: return ""; /* handle input error */ \
    } \
  } \
  template <> EnumType str2enum<EnumType>(const char *str) \
  { \
    ENUM_DEF(ENUM_STRCMP) \
    return EnumType##_Invalid; /* handle input error */ \
  } \
  template <> EnumType str2enum<EnumType>(const std::string& str) \
  { \
      return str2enum<EnumType>(str.c_str()); \
  }

#endif /* __ENUMFACTORY_H */
