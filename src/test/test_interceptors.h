/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/

#ifndef __test_intereceptors_h
#define __test_intereceptors_h

#include "intercept.h"

// This file contains interceptors that are only ever used to make sure the
// interception infrastructure is working.  These should never be referenced by
// production code.

class BasicTestInterceptor : public AMethodInterceptor {
    public:
        inline BasicTestInterceptor()
            : AMethodInterceptor("BasicTestInterceptor",
                                 InterceptorRegistry::BasicTestInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const;
        virtual bool needReturnValueInOnMethodEnd() const;
        virtual bool shouldCallOnMethodEnd() const;
    protected:
        inline BasicTestInterceptor(const char* const className,
                                    InterceptorRegistry::ID interceptorID)
            : AMethodInterceptor(className, interceptorID)
        {
        }
};

class FullTestInterceptor : public BasicTestInterceptor {
    public:
        inline FullTestInterceptor()
            : BasicTestInterceptor("FullTestInterceptor",
                                   InterceptorRegistry::FullTestInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual bool needParamsInOnMethodBegin() const;
        virtual bool needReturnValueInOnMethodEnd() const;
        virtual bool shouldCallOnMethodEnd() const;

        static int MYSTATE;
};

#endif
