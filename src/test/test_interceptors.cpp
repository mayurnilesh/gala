/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/

#include "test_interceptors.h"
#include <zend.h>
#include <boost/foreach.hpp>

#include "emalloc_allocator.h"

#include <iostream>

// This file contains interceptors that are only ever used to make sure the
// interception infrastructure is working.  These should never be referenced by
// production code.

static void php_zval_print_wrapper(const char* str, uint32_t str_length)
{
   std::cout << str;
}

/* {{{ BasicTestInterceptor */
bool BasicTestInterceptor::needParamsInOnMethodBegin() const
{
    return false;
}

bool BasicTestInterceptor::needReturnValueInOnMethodEnd() const
{
    return false;
}

bool BasicTestInterceptor::shouldCallOnMethodEnd() const
{
    return false;
}

static inline void printCallableName(const PHPExecEnvironment* phpExecEnv)
{
    std::cout << "( " << phpExecEnv->getCallableName() << " )";
}

void* BasicTestInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    std::cout << "-- In " << getClass() << "::onCallableBegin";
    const char* const className = phpExecEnv->getClassName();
    printCallableName(phpExecEnv);
    return NULL;
}

void BasicTestInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                         void* state)
{
    std::cout << "-- In " << getClass() << "::onCallableEnd";
    printCallableName(phpExecEnv);
}

bool FullTestInterceptor::needParamsInOnMethodBegin() const
{
    return true;
}

bool FullTestInterceptor::needReturnValueInOnMethodEnd() const
{
    return true;
}

bool FullTestInterceptor::shouldCallOnMethodEnd() const
{
    return true;
}

void* FullTestInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    std::cout << "-- In " << getClass() << "::onCallableBegin";
    printCallableName(phpExecEnv);

    if (phpExecEnv->getParamCount()) {
        std::cout << "   params:" << std::endl;
        for (unsigned i = 0; i < phpExecEnv->getParamCount(); ++i) {
            zval* const param = phpExecEnv->getParam<zval*>(i);
            std::cout << std::string(5, ' ');
#if PHP_VERSION_ID >= 70100
            zend_print_zval_r(param, 5);
#else
            zend_print_zval_r_ex((zend_write_func_t) php_zval_print_wrapper, param, 5 TSRMLS_CC);
#endif
            std::cout << std::endl;
        }
    }
    const zval* object = phpExecEnv->getInvokedObject();
    if (object) {
        std::cout << "   object: ";
#if PHP_VERSION_ID >= 70100
        zend_print_zval_r((zval*) object, 11);
#else
        zend_print_zval_r_ex((zend_write_func_t) php_zval_print_wrapper, (zval*)object, 11 TSRMLS_CC);
#endif
        std::cout << std::endl;
    }

    return (void *)&MYSTATE;
}

void  FullTestInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                         void* state)
{
    std::cout << "-- In " << getClass() << "::onCallableEnd";
    printCallableName(phpExecEnv);

    zval* return_value = phpExecEnv->getReturnValue();
    if (return_value) {
        std::cout << "   return: ";
#if PHP_VERSION_ID >= 70100
        zend_print_zval_r((zval*) return_value, 11);
#else
        zend_print_zval_r_ex((zend_write_func_t) php_zval_print_wrapper, (zval*)return_value, 11 TSRMLS_CC);
#endif
        std::cout << std::endl;
    }
}

int FullTestInterceptor::MYSTATE = 0;

/* }}} */
