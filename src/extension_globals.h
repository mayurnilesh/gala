/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __delayed_extension_globals_h
#define __delayed_extension_globals_h

#include <boost/assert.hpp>
#include <boost/utility.hpp>
#include <boost/type_traits/add_reference.hpp>
#include <boost/type_traits/remove_pointer.hpp>
#include <boost/preprocessor/stringize.hpp>

#include <TSRM.h>
#include <Zend/zend_modules.h>

#include "zend_hashtable.h"
#include "extension_globals_table.h"

// Table of globals for other php extensions that we want to reference, but can't link
// directly against on CentOS/RHEL linux distros.
//

#define DELAYED_EXT_GLOBALS_FWD_TYPE_DECL(extName)                                      \
    struct _zend_##extName##_globals;                                                   \
    typedef struct _zend_##extName##_globals zend_##extName##_globals;
DELAYED_EXTENSION_GLOBALS_TABLE(DELAYED_EXT_GLOBALS_FWD_TYPE_DECL)
#undef DELAYED_EXT_GLOBALS_FWD_TYPE_DECL

#ifdef ZTS
#error "ZTS aware code in this file has never been compiled or tested!!!"
#endif // ZTS

namespace DelayedExtensionGlobals
{
    class Globals;

    namespace
    {
        class ModuleTableValueAdapter
        {
        public:
            typedef const zend_module_entry* t_ValueType;
#if PHP_VERSION_ID >= 70000
            static const zend_module_entry* toValue(zval* foundValue);
#else
            static const zend_module_entry* toValue(void* rawValue);
#endif
        private:
            ModuleTableValueAdapter();
            ModuleTableValueAdapter(const ModuleTableValueAdapter&);
            ModuleTableValueAdapter& operator=(const ModuleTableValueAdapter&);
        };

#if PHP_VERSION_ID >= 70000
        const zend_module_entry* ModuleTableValueAdapter::toValue(zval* foundValue)
        {
            BOOST_ASSERT(Z_TYPE_P(foundValue) == IS_PTR);
            return reinterpret_cast<const zend_module_entry*>(Z_PTR_P(foundValue));
        }
#else
        const zend_module_entry* ModuleTableValueAdapter::toValue(void* rawValue)
        {
            return reinterpret_cast<const zend_module_entry*>(rawValue);
        }
#endif

        typedef ZendHashTable<ModuleTableValueAdapter> t_ModuleHashTable;
    }

    /**
        Pointer template used by the Globals class below to hold
        lazily compute and hold a pointer to the globals of a php extension.

     */
    template <typename t_globalType,
              const char* extensionName,
              unsigned extensionNameLen>
    class GlobalPtr : public boost::noncopyable
    {
        // To allow access to the constructor
        friend class Globals;

        public:
            t_globalType* operator->() const
            {
                return get();
            }

            t_globalType& operator*() const
            {
                return *(get());
            }

            operator bool() const
            {
                return get();
            }

            const zend_module_entry* getModuleEntry() const
            {
                if (m_initialized)
                    return m_moduleEntry;

                m_initialized = true;
                t_ModuleHashTable moduleHashTable(t_ModuleHashTable::share(&module_registry));
                const zend_module_entry* moduleEntry = NULL;
                if (!moduleHashTable.find(extensionName, extensionNameLen, &moduleEntry))
                    return NULL;
                if (!moduleEntry)
                    return NULL;
                m_moduleEntry = const_cast<zend_module_entry*>(moduleEntry);
                return m_moduleEntry;
            }

        private:
            GlobalPtr(TSRMLS_D)
                : m_initialized(false)
                , m_moduleEntry(NULL)
            {
                TSRMLS_SET_CTX(m_threadSafetyContext);
            }

            t_globalType* get() const
            {
                const zend_module_entry* moduleEntry = getModuleEntry();
#ifndef ZTS
                void* opaqueGlobal = moduleEntry->globals_ptr;
#else
                ts_rsrc_id* resourceID = moduleEntry->globals_id_ptr;
                if (!resourceID)
                    return NULL;
                TSRMLS_FETCH_FROM_CTX(m_threadSafetyContext);
                void** threadLocalArray = (*reinterpret_cast<void***>(tsrm_ls));
                void* opaqueGlobal =
                    threadLocalArray[TSRM_UNSHUFFLE_RSRC_ID(*resourceID)];
#endif
                return reinterpret_cast<t_globalType*>(opaqueGlobal);
            }

            mutable bool m_initialized;
            mutable zend_module_entry* m_moduleEntry;
#ifdef ZTS
            void* m_threadSafetyContext;
#endif
    };

#define DELAYED_EXT_GLOBALS_STORAGE_DECL(extName)                                       \
    static const char delayed___##extName##___name[];                                   \
    GlobalPtr<zend_##extName##_globals,                                                 \
              delayed___##extName##___name,                                             \
              sizeof(BOOST_PP_STRINGIZE(extName))> const m_##extName;

// Create the DELAYED_EXT_GLOBALS_ACCESSOR macro to:
//   * declare and define an inline getter to get the
//     GlobalPtr<> value for the delayed extension global.
#define DELAYED_EXT_GLOBALS_ACCESSOR(extName)                                           \
    inline const GlobalPtr<zend_##extName##_globals,                                    \
                           delayed___##extName##___name,                                \
                           sizeof(BOOST_PP_STRINGIZE(extName))> & get_##extName() const \
    {                                                                                   \
        return m_##extName;                                                             \
    }

    class Globals : boost::noncopyable
    {
        private:
            DELAYED_EXTENSION_GLOBALS_TABLE(DELAYED_EXT_GLOBALS_STORAGE_DECL)
            #undef DELAYED_EXT_GLOBALS_STORAGE_DECL

        public:
            Globals(TSRMLS_D);

            DELAYED_EXTENSION_GLOBALS_TABLE(DELAYED_EXT_GLOBALS_ACCESSOR)
            #undef DELAYED_EXT_GLOBALS_ACCESSOR
    };

}

#endif
