/*
Copyright (c) AppDynamics, Inc., and its affiliates
2018
All Rights Reserved
*/
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
using boost::asio::ip::tcp;
#include <time.h>
#include <cstdlib>

#include "analytics.h"
#include "agent.h"
#include "transactions.h"

// copied the below constants from nodejs 
#define MIN_REPORTING_FREQ 30
#define MAX_EVENTS_THRESHOLD 10000 // FIXME: make tunable



http_client::http_client(boost::asio::io_service& io_service, const std::string& server, const std::string& path) 
: resolver_(io_service), socket_(io_service)
{
  const AgentIdentity& agentIdentity = AG(kernel)->getAgentContext()->getAgentIdentity();
  
  boost::shared_ptr<TransactionMonitor> txMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor();
  TransactionContext* const txContext = txMonitor->getTransactionContext();

  if (!txContext) 
	  return;

    boost::shared_ptr<AgentTransaction> agentBT(txContext->getTransaction());

    std::ostream request_stream(&request_);

    boost::property_tree::ptree root;


   time_t transactionTime = AG(kernel)->getTimer()->getCurrentTime()/1000;
    struct tm * ptm;
  ptm = localtime (&transactionTime);

  ptm->tm_year += 1900;
  ptm->tm_mon += 1;
  char buff[100];

  sprintf(buff,"%d-%02d-%02dT%02d:%02d:%02d.000+0000",ptm->tm_year,ptm->tm_mon,ptm->tm_mday,ptm->tm_hour,ptm->tm_min,ptm->tm_sec);


    const Timer* const timer = AgentKernel::getTimer();
      uint64_t requestEndTimeStamp = timer->getTimestamp();

    char timeDetailsbuff[200];
    uint64_t const requestStartTimeStamp = txContext->getStartTime().getTimestamp();
  //LOG4CXX_INFO(AG(G)->log_agent, "Request start time is :"+ requestStartTimeStamp);
  uint64_t const requestElapsedTimeMS =
  AgentKernel::getTimer()->getElapsedTime(requestStartTimeStamp, requestEndTimeStamp) / 1000;

  sprintf(timeDetailsbuff,"Request start timestamnp is %ld, request end timestamp is %ld, elapsed timestamp is %ld",requestStartTimeStamp,requestEndTimeStamp,requestElapsedTimeMS);
  
  
  std::string timeDetails = timeDetailsbuff;

    LOG4CXX_INFO(AG(G)->log_agent, "request and response details  :"+timeDetails);
      



  LOG4CXX_INFO(AG(G)->log_agent, "Captured time is  :"+strTransactionTime);

   std::string tempVal = buff;

   LOG4CXX_INFO(AG(G)->log_agent, "Captured time is  with sprintf is :"+tempVal);
    boost::shared_ptr<AgentTransaction> agentBT(txContext->getTransaction());
    boost::property_tree::ptree root;

  char *cstr = new char[strTransactionTime.length() + 1];
  std::strcpy(cstr, strTransactionTime.c_str());


  root.put("eventTimestamp", buff);


    /** actual data till dummy data
    root.put("segments..tier", agentIdentity.getTier());
    root.put("segments..tierId", txMonitor->getConfigChannel()->getTierID());


    root.put("segments..node", agentIdentity.getNode());
    root.put("segments..nodeId", txMonitor->getConfigChannel()->getNodeID());


    root.put("segments..requestExperience", "NORMAL");

    root.put("segments..entryPoint", "True");
    
    dummy data
    root.put("segments..uniqueSegmentId", "1");
    root.put("segments..clientRequestGUID", "22222222-2222-2222-2222-222222222222");

    root.put("segments..transactionTime", "2");
    */

    //root.put("application", "testvalue");


   root.put("segments..tier", agentIdentity.getTier());
    root.put("segments..tierId", txMonitor->getConfigChannel()->getTierID());
  root.put<int>("segments..transactionTime", requestElapsedTimeMS);

    root.put("segments..node", agentIdentity.getNode());
    root.put("segments..nodeId", txMonitor->getConfigChannel()->getNodeID());



  
   int64_t transactionmTime;
  LOG4CXX_INFO(AG(G)->log_agent, "Fetching avg response time  :"); 
   if(txContext->getRequestContext()->getAverageResponseTimeForLastMinute(&transactionmTime) == true)
    {
  LOG4CXX_INFO(AG(G)->log_agent, "Avg response time is %ld  :"+transactionmTime);
          root.put("transactionTime", transactionmTime);
    } 

   root.put("segments..requestExperience", "NORMAL");

    root.put("segments..entryPoint", "True");



    root.put("application", agentIdentity.getApplication());
    
    root.put("applicationId", txMonitor->getConfigChannel()->getAppID());
    
    //root.put("requestGUID", "testvalue");
    root.put("requestGUID", txMonitor->getRequestContext()->getRequestGUID());
    
    //root.put("transactionName", "testvalue");
    root.put("transactionName", agentBT->getName());
    
    //root.put<int>("transactionId", 365);
    root.put<int>("transactionId", agentBT->getID());









    std::ostringstream buf;
    boost::property_tree::write_json (buf, root, false);
    std::string json = buf.str();

    //remove trailing newline
    json = json.substr(0, json.size() - 1);
    buffer_.push_back(json);

   int count = buffer_.size();

  if ((count > 0 && delta > MIN_REPORTING_FREQ) || count > MAX_EVENTS_THRESHOLD) {






    std::list<std::string>::const_iterator begin = buffer_.begin();
    std::list<std::string>::const_iterator end = buffer_.end();
    std::ostringstream collectedjson;

    collectedjson << "[";
    for (std::list<std::string>::const_iterator it = begin; it != end; it++) {
      if (it != begin) collectedjson << ", ";
      collectedjson << *it;
    }
    collectedjson << "]";

    lastReported_ = now;
    buffer_.clear();



  std::string some_host = " localhost:9090";

   //request_stream << "POST /v2/sinks/bt HTTP/1.1\r\nHost: localhost:9090\r\nUser-Agent: PHP agent/1.0\r\n\r\n";
  request_stream << "POST /v2/sinks/bt HTTP/1.1\r\n";
  request_stream << "Host:" << some_host << "\r\n";
  request_stream << "User-Agent: PHP agent/1.0\r\n";
  request_stream << "Accept: */*\r\n";
  request_stream << "Content-Type:application/json;X-Analytics-Agent-Access-Key:SJ5b2m7d154\r\n";
  request_stream << "Content-Length: " << json.str().size() << "\r\n\r\n";
  request_stream << json.str() << "\r\n\r\n";

    // Start an asynchronous resolve to translate the server and service names
    // into a list of endpoints.


    tcp::resolver::query query("localhost", "9090");
    //tcp::resolver::query query(server, "http");
    resolver_.async_resolve(query,
        boost::bind(&http_client::handle_resolve, this,
          boost::asio::placeholders::error,
          boost::asio::placeholders::iterator));
  }
}

void http_client::handle_resolve(const boost::system::error_code& err,
      tcp::resolver::iterator endpoint_iterator)
{
    if (!err)
    {
      boost::asio::async_connect(socket_, endpoint_iterator,
          boost::bind(&http_client::handle_connect, this,
            boost::asio::placeholders::error));
    }
    else
    {
      //printf("error in handle_resolve\n");
      // LOG4CXX_INFO(AG(G)->log_agent, "error connecting in handle_resolve");
    }
}

  void http_client::handle_connect(const boost::system::error_code& err)
  {
    if (!err)
    {
      boost::asio::async_write(socket_, request_,
          boost::bind(&http_client::handle_write_request, this,
            boost::asio::placeholders::error));
    }
    else
    {
      //printf("error in handle_connect\n");
      //LOG4CXX_INFO(AG(G)->log_agent, "error connecting in handle_connect");
    }
  }

void http_client::handle_write_request(const boost::system::error_code& err) {}

void http_client::handle_read_status_line(const boost::system::error_code& err) {}

void http_client::handle_read_headers(const boost::system::error_code& err) {}

void http_client::handle_read_content(const boost::system::error_code& err) {}
