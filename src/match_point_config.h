#ifndef __match_point_config_h
#define __match_point_config_h

#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_set.hpp>
#include <boost/utility.hpp>

#include "PHPAgentProtobufs.pb.h"
#include "string_match.h"
#include "services.h"

// todo fixme
extern void convertPBNameValuePairsToMap(const google::protobuf::RepeatedPtrField<appdynamics::pb::Common::NameValuePair>& nameValues,
                                         std::map<std::string, std::string>& nvMap);


/**
    Contains match point configuration for an entry point.

    Lazily parses and caches name value pairs from protobuf into a std::map.
 */
class MatchPointConfig : boost::noncopyable
{
    template <class T, class Arg1, class... Args>
    friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
public:
    const std::map<std::string, std::string>& getDiscoveryProperties() const
    {
        if (m_discoveryNameValuePairs.size())
            return m_discoveryNameValuePairs;
        // If we don't have discovery config, discovery should be
        // disabled and we should never get here.
        BOOST_ASSERT(m_config->has_discoveryconfig());
        const appdynamics::pb::Agent::MatchPointConfig_Discovery& discoveryConfig =
            m_config->discoveryconfig();
        const appdynamics::pb::Agent::NamingScheme& namingScheme =
            discoveryConfig.namingscheme();
        convertPBNameValuePairsToMap(namingScheme.properties(), m_discoveryNameValuePairs);
        return m_discoveryNameValuePairs;
    }


    const appdynamics::pb::Agent::MatchPointConfig* get() const
    {
        return m_config;
    }

    bool matchAndLogError(const PHPExecEnvironment* execEnv, const appdynamics::pb::Common::StringMatchCondition* condition, const std::string& input, const AgentLogger& logger) const
    {
        MatchResult matchResult = matchString(execEnv, *condition, input);
        if (matchResult == MatchResult::EMPTY_CONDITION)
            return true;

        std::string errorMsg;
        if (matchResult == MatchResult::INVALID_REGEX) {
            errorMsg = "StringMatchCondition has invalid regex: " + condition->matchstrings(0);
        }
        else if (matchResult == MatchResult::REGEX_ERROR) {
            errorMsg = "StringMatchCondition has regex execution error: " +
                    boost::lexical_cast<std::string>(matchResult.getRegexErrorCode());
        }
        else if (matchResult == MatchResult::INVALID_CONDITION) {
            errorMsg = "StringMatchCondition is invalid: the condition has zero match strings.";
        }
        if (errorMsg.size() > 0 && m_loggedMatchError.find(condition) == m_loggedMatchError.end()) {
            m_loggedMatchError.insert(condition);
            LOG4CXX_ERROR(logger, errorMsg);
        }

        return matchResult == MatchResult::MATCHED;
    }

private:
    MatchPointConfig(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                     const appdynamics::pb::Agent::MatchPointConfig* const config)
        : m_txConfig(txConfig)
        , m_config(config)
    {
        BOOST_ASSERT(txConfig);
        BOOST_ASSERT(config);
    }



    boost::shared_ptr<appdynamics::pb::TransactionConfig> const m_txConfig;
    const appdynamics::pb::Agent::MatchPointConfig* const m_config;
    mutable std::map<std::string, std::string> m_discoveryNameValuePairs;
    mutable std::map<std::string, std::string> m_splitNameValuePairs;
    // set here, mutable,
    // set of protobuf StringMatchCondition objects, for which we've omitted regex log message
    mutable boost::unordered_set<const appdynamics::pb::Common::StringMatchCondition*> m_loggedMatchError;
};


#endif
