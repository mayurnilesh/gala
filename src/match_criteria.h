/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#ifndef __match_criteria_h
#define __match_criteria_h

#include <boost/shared_ptr.hpp>
#include "PHPAgentProtobufs.pb.h"
#include "match_point_config.h"

class MatchCriteria : boost::noncopyable
{
    public:
        virtual const std::string& getNamingSchemeType() const = 0;
        virtual const std::string& getCustomMatchRuleName() const = 0;

        virtual bool isCustom() const = 0;
        virtual bool isAutoDiscovered() const = 0;
};

/**
 * Class that points to a CustomMatch structure embedded in a MatchPointConfig.
 */
class MatchCriteriaLocal : public MatchCriteria
{
    public:
        MatchCriteriaLocal()
            : m_customMatchRule(NULL)
        {
        }

        MatchCriteriaLocal(const boost::shared_ptr<const MatchPointConfig>& matchPointConfig)
            : m_matchPointConfig(matchPointConfig)
            , m_customMatchRule(NULL)
        {
            BOOST_ASSERT(matchPointConfig);
        }

        MatchCriteriaLocal(const boost::shared_ptr<const MatchPointConfig>& matchPointConfig,
                      const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* customMatchRule)
            : m_matchPointConfig(matchPointConfig)
            , m_customMatchRule(customMatchRule)
        {
            BOOST_ASSERT(matchPointConfig);
        }

        const std::string& getNamingSchemeType() const
        {
            // If we don't have a discovery config for this entry point
            // we should never get here.
            BOOST_ASSERT(m_matchPointConfig->get()->has_discoveryconfig());
            return m_matchPointConfig->get()->discoveryconfig().namingscheme().type();
        }

        const std::string& getCustomMatchRuleName() const
        {
            return m_customMatchRule->btname();
        }

        bool isCustom() const { return m_customMatchRule; }

        bool isAutoDiscovered() const { return (!isCustom()) && m_matchPointConfig; }

        const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* getCustomMatchRule() const
        {
            return m_customMatchRule;
        }

    private:
        boost::shared_ptr<const MatchPointConfig> m_matchPointConfig;
        const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* m_customMatchRule;
};

class MatchCriteriaContinuing : public MatchCriteria
{
    public:
        inline MatchCriteriaContinuing(const appdynamics::pb::Agent::MatchPointConfig_Discovery& config)
            : m_isCustom(false)
            , m_namingSchemeType(config.namingscheme().type())
        {
        }

        inline MatchCriteriaContinuing(const appdynamics::pb::Agent::MatchPointConfig_CustomMatch& config)
            : m_isCustom(true)
            , m_customMatchRuleName(config.btname())
        {
        }

        const std::string& getNamingSchemeType() const { return m_namingSchemeType; }
        const std::string& getCustomMatchRuleName() const { return m_customMatchRuleName; }

        virtual bool isCustom() const { return m_isCustom; }
        virtual bool isAutoDiscovered() const { return !m_isCustom; }

    private:
        bool m_isCustom;
        std::string m_namingSchemeType;
        std::string m_customMatchRuleName;
};

#endif
