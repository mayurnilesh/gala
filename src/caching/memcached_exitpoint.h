/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#ifndef __memcached__exitpoint_h
#define __memcached__exitpoint_h

/*
 * TODO
 * - add logging
 */

#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "exitcall_detail.h"
#include "caching_traffic_interceptor.h"
#include "method_selector.h"

class MemcachedHandleInfo : public AExitPointHandleInfo {
        friend class HandleRegistry;
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<MemcachedHandleInfo> create(const appdynamics::pb::Agent::ExitPointType& type) { return boost::make_shared<MemcachedHandleInfo>(type); }

        inline bool isResolved() const { return m_resolved; }
        inline void clearResolved() { m_resolved = false; }
        inline void setResolved() { m_resolved = true; }

    private:
        inline MemcachedHandleInfo(const appdynamics::pb::Agent::ExitPointType& type)
            : AExitPointHandleInfo(type, &typeTag)
            , m_resolved(false) {}

        bool m_resolved;
};

class MemcachedConstructorInterceptor : public AMethodInterceptor
{
    public:
        MemcachedConstructorInterceptor()
            : AMethodInterceptor("MemcachedConstructorInterceptor",
                                 InterceptorRegistry::MemcachedConstructorInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_CACHE);
        }
};

class MemcachedPropertyGenerator : public IBackendPropertyGenerator
{
    public:
        bool getProperties(const PHPExecEnvironment* execEnv,
                           const CurrentExitCall* exitCall,
                           StringMap& properties) const;
};
extern MemcachedPropertyGenerator memcachedPropertyGenerator;

class AMemcachedTrafficInterceptor : public ACachingTrafficInterceptor<AMemcachedTrafficInterceptor,
                                                                       MemcachedPropertyGenerator,
                                                                       &memcachedPropertyGenerator,
                                                                       MemcachedHandleInfo>
                                   , public AMethodInvocationSelector
{
        friend class ACachingTrafficInterceptor<AMemcachedTrafficInterceptor,
                                                MemcachedPropertyGenerator,
                                                &memcachedPropertyGenerator,
                                                MemcachedHandleInfo>;
    public:
        static const char cacheTypeName[];
        AMemcachedTrafficInterceptor(const char* const className, InterceptorRegistry::ID id)
            : ACachingTrafficInterceptor<AMemcachedTrafficInterceptor,
                                         MemcachedPropertyGenerator,
                                         &memcachedPropertyGenerator,
                                         MemcachedHandleInfo>(className, id)
            , AMethodInvocationSelector(AMethodInvocationSelector::kMethod)
        {
        }
};

class MemcachedReadInterceptor : public AMemcachedTrafficInterceptor
{
    public:
        MemcachedReadInterceptor()
            : AMemcachedTrafficInterceptor("MemcachedReadInterceptor", InterceptorRegistry::MemcachedReadInterceptor_ID) { }
        virtual ~MemcachedReadInterceptor() { }

    protected:
        AccessType getAccessType(const PHPExecEnvironment* execEnv) const { return READ; }

};

class MemcachedWriteInterceptor : public AMemcachedTrafficInterceptor
{
    public:
        MemcachedWriteInterceptor()
            : AMemcachedTrafficInterceptor("MemcachedWriteInterceptor", InterceptorRegistry::MemcachedWriteInterceptor_ID) { }
        virtual ~MemcachedWriteInterceptor() { }

    protected:
        AccessType getAccessType(const PHPExecEnvironment* execEnv) const { return WRITE; }

};

class MemcachedResetServerListInterceptor : public AMethodInterceptor {
    public:
        MemcachedResetServerListInterceptor()
            : AMethodInterceptor("MemcachedResetServerListInterceptor",
                                 InterceptorRegistry::MemcachedResetServerListInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return false; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_CACHE);
        }
};

#endif // __memcached__h
