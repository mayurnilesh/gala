/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#ifndef __caching_exitpoint_h
#define __caching_exitpoint_h

#include <boost/utility.hpp>
#include "enumfactory.h"
#include "main/php_version.h"
#include "exitpoint.h"

struct _zend_agent_globals;

#define CACHING_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL(XX) \
    XX(CachingExitPoint_SERVER_POOL,"SERVER POOL",) \
    XX(CachingExitPoint_VENDOR,"VENDOR",)

DECLARE_ENUM(CachingExitPointIndentifyingPropertyNames, CACHING_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

class InterceptionEngine;
class MemcachedConstructorInterceptor;
class MemcachedResetServerListInterceptor;
class MemcachedReadInterceptor;
class MemcachedWriteInterceptor;

namespace memcache
{
    class ServerConnectionProcedureInterceptor;
    class AddServerConnectionProcedureInterceptor;
    class ReadProcedureInterceptor;
    class WriteProcedureInterceptor;
    class CloseProcedureInterceptor;

    class ServerConnectionMethodInterceptor;
    class ReadMethodInterceptor;
    class WriteMethodInterceptor;
    class CloseMethodInterceptor;
}

#if PHP_VERSION_ID >= 50300
class PredisConstructorInterceptor;
class PredisCallTrafficInterceptor;
class PredisExecuteCommandTrafficInterceptor;
class PredisBatchTrafficInterceptor;
class PredisCloseInterceptor;
#endif

/**
    Exit point delegate that sets up the interceptors to
    detect caching backends.
 */
class CachingExitPointDelegate : public AExitPointDelegate
{
    public:
        CachingExitPointDelegate(InterceptionEngine* interceptEngine,
                                 TransactionMonitor* txMonitor);

        appdynamics::pb::Agent::ExitPointType getExitPointType() const
        {
            return appdynamics::pb::Agent::EXIT_CACHE;
        }

        void configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                       const struct _zend_agent_globals* agentGlobals);

        std::string generateDisplayName(const ExitCallInfo& exitCallInfo);

    protected:
        void applyRules(const _zend_agent_globals* agentGlobals);

        bool processMatchRule(const PHPExecEnvironment* execEnv,
                              const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                              const StringMap& properties) const;

        void processNamingRule(const PHPExecEnvironment* execEnv,
                               const appdynamics::pb::Agent::BackendRule& backendRule,
                               const StringMap& properties,
                               StringMap& identifyingProperties) const;
    private:
        static bool s_didApplyRules;
};

#endif // __caching_exitpoint_h
