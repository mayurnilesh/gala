#include "main/php_version.h"
#if PHP_VERSION_ID >= 50300

#include <boost/algorithm/string/join.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/unordered_set.hpp>
#include <sstream>
#include <vector>

#include "common.h"
#include "predis_exitpoint.h"
#include "db/db_exitpoint.h"
#include "caching_exitpoint.h"
#include "emalloc_allocator.h"
#include "zend_ini.h"
#include "caching_traffic_interceptor.h"
#include "transactions.h"


static const char CONNECTION_PROPERTY_NAME[]= "connection";
static const char OPTIONS_VARIABLE_NAME[] = "options";
static const char REPLICATION_OPTION_NAME[] = "replication";
static const char MASTER_ATTRIBUTE_NAME[] = "master";
static const char SLAVE_ATTRIBUTE_NAME[] = "slaves";
static const char SERVER_POOL_NAME[] = "pool";
static const char PARAMETERS_ATTRIBUTE_NAME[] = "parameters";
static const char HOST_ATTR_NAME[] = "host";
static const char PORT_ATTR_NAME[] = "port";

/*

Following Predis commands have not been included in any of the categories (READ_METHODS, WRITE_METHODS, READ_WRITE_METHODS, BATCH_METHODS):
    AUTH, bitpos, echo, geodist, hscan, hstrlen, object, ping, sscan, zscan, psubscribe, punsubscribe, pubsub, subscribe, quit, scan, select, unsubscribe, watch, unwatch

*/

static const boost::unordered_set<std::string> READ_METHODS = boost::assign::list_of
        ("exists") ("KeyExists")
        ("type") ("KeyType")
        ("keys") ("KeyKeys")
        ("randomkey") ("KeyRandom")
        ("ttl") ("KeyTimeToLive")
        ("get") ("StringGet")
        ("mget") ("StringGetMultiple")
        ("llen") ("ListLength")
        ("lrange") ("ListRange")
        ("lindex") ("ListIndex")
        ("scard") ("SetCardinality")
        ("sismember") ("SetIsMember")
        ("sinter") ("SetIntersection")
        ("sunion") ("SetUnion")
        ("sdiff") ("SetDifference")
        ("smembers") ("SetMembers")
        ("srandmember") ("SetRandomMembers")
        ("zrange") ("ZSetRange")
        ("zrevrange") ("ZSetReverseRange")
        ("zrangebyscore") ("ZSetRangeByScore")
        ("zcard") ("ZSetCardinality")
        ("zscore") ("ZSetScore")
        ("info") ("ServerInfo")
        ("slaveof") ("ServerSlaveOf")
        ("monitor") ("ServerMonitor")
        ("dbsize") ("ServerDatabaseSize")
        ("lastsave") ("ServerLastSave")
        ("substr") ("StringSubstr")
        ("zcount") ("ZSetCount")
        ("zrank") ("ZSetRank")
        ("zrevrank") ("ZSetReverseRank")
        ("hget") ("HashGet")
        ("hmget") ("HashGetMultiple")
        ("hexists") ("HashExists")
        ("hlen") ("HashLength")
        ("hkeys") ("HashKeys")
        ("hvals") ("HashValues")
        ("hgetall") ("HashGetAll")
        ("config") ("ServerConfig")
        ("strlen") ("StringStrlen")
        ("getrange") ("StringGetRange")
        ("getbit") ("StringGetBit")
        ("zrevrangebyscore") ("ZSetReverseRangeByScore")
        ("slowlog") ("ServerSlowLog")
        ("pttl") ("KeyPreciseTimeToLive")
        ("bitcount") ("StringBitCount")
        ("info") ("ServerInfoV26x")
        ("time") ("ServerTime")
        ("getProfile")
        ("getOptions")
        ("getConnectionFactory")
        ("getClientFor")
        ("getConnection")
        ("geohash") ("GeospatialGeoHash")
        ("geopos") ("GeospatialGeoPos")
        ("pfcount") ("HyperLogLogCount")
        ("zlexcount") ("ZSetLexCount")
        ("zrangebylex") ("ZSetRangeByLex")
        ("zrevrangebylex") ("ZSetReverseRangeByLex")
        ("client-getname")
        ("client-list")
        ("config-get");

static const boost::unordered_set<std::string> WRITE_METHODS = boost::assign::list_of
        ("del") ("KeyDelete")
        ("rename") ("KeyRename")
        ("renamenx") ("KeyRenamePreserve")
        ("expire") ("KeyExpire")
        ("expireat") ("KeyExpireAt")
        ("move") ("KeyMove")
        ("sort") ("KeySort")
        ("dump") ("KeyDump")
        ("restore") ("KeyRestore")
        ("set") ("StringSet")
        ("setnx") ("StringSetPreserve")
        ("mset") ("StringSetMultiple")
        ("msetnx") ("StringSetMultiplePreserve")
        ("incr") ("StringIncrement")
        ("incrby") ("StringIncrementBy")
        ("decr") ("StringDecrement")
        ("decrby") ("StringDecrementBy")
        ("rpush") ("ListPushTail")
        ("lpush") ("ListPushHead")
        ("ltrim") ("ListTrim")
        ("lset") ("ListSet")
        ("lrem") ("ListRemove")
        ("lpop") ("ListPopFirst")
        ("rpop") ("ListPopLast")
        ("rpoplpush") ("ListPopLastPushHead")
        ("sadd") ("SetAdd")
        ("srem") ("SetRemove")
        ("spop") ("SetPop")
        ("smove") ("SetMove")
        ("zadd") ("ZSetAdd")
        ("zincrby") ("ZSetIncrementBy")
        ("zrem") ("ZSetRemove")
        ("flushdb") ("ServerFlushDatabase")
        ("flushall") ("ServerFlushAll")
        ("save") ("ServerSave")
        ("bgsave") ("ServerBackgroundSave")
        ("setex") ("StringSetExpire")
        ("append") ("StringAppend")
        ("blpop") ("ListPopFirstBlocking")
        ("brpop") ("ListPopLastBlocking")
        ("zunionstore") ("ZSetUnionStore")
        ("zinterstore") ("ZSetIntersectionStore")
        ("hset") ("HashSet")
        ("hsetnx") ("HashSetPreserve")
        ("hmset") ("HashSetMultiple")
        ("hincrby") ("HashIncrementBy")
        ("hdel") ("HashDelete")
        ("setrange") ("StringSetRange")
        ("setbit") ("StringSetBit")
        ("rpushx") ("ListPushTailX")
        ("lpushx") ("ListPushHeadX")
        ("linsert") ("ListInsert")
        ("brpoplpush") ("ListPopLastPushHeadBlocking")
        ("pexpire") ("KeyPreciseExpire")
        ("pexpireat") ("KeyPreciseExpireAt")
        ("psetex") ("StringPreciseSetExpire")
        ("incrbyfloat") ("StringIncrementByFloat")
        ("hincrbyfloat") ("HashIncrementByFloat")
        ("bgrewriteaof") ("ServerBackgroundRewriteAOF")
        ("geoadd") ("GeospatialGeoAdd")
        ("georadius") ("GeospatialGeoRadius")
        ("georadiusbymember") ("GeospatialGeoRadiusByMember")
        ("migrate") ("KeyMigrate")
        ("persist") ("KeyPersist")
        ("pfadd") ("HyperLogLogAdd")
        ("pfmerge") ("HyperLogLogMerge")
        ("publish") ("PubSubPublish")
        ("shutdown") ("ServerShutdown")
        ("zremrangebylex") ("ZSetRemoveRangeByLex")
        ("client-kill")
        ("client-setname")
        ("config-resetstat")
        ("config-rewrite")
        ("config-set");

static const boost::unordered_set<std::string> READ_WRITE_METHODS = boost::assign::list_of
        ("getset") ("StringGetSet")
        ("sinterstore") ("SetIntersectionStore")
        ("sunionstore") ("SetUnionStore")
        ("sdiffstore") ("SetDifferenceStore")
        ("zremrangebyscore") ("ZSetRemoveRangeByScore")
        ("zremrangebyrank") ("ZSetRemoveRangeByRank")
        ("bitop") ("StringBitOp")
        ("bitfield") ("StringBitField");

static const boost::unordered_set<std::string> BATCH_METHODS = boost::assign::list_of
        ("pipeline")
        ("transaction")
        ("multiExec")
        ("TransactionMulti")
        ("discard") ("TransactionDiscard")
        ("eval") ("ServerEval")
        ("evalsha") ("ServerEvalSHA")
        ("exec") ("TransactionExec")
        ("multi") ("TransactionMulti")
        ("script") ("ServerScript");

PredisAccessLookupTable::PredisAccessLookupTable()
{
    for (auto it = READ_METHODS.begin(); it != READ_METHODS.end(); ++it)
        m_hashTable[*it] = READ;
    for (auto it = WRITE_METHODS.begin(); it != WRITE_METHODS.end(); ++it)
        m_hashTable[*it] = WRITE;
    for (auto it = READ_WRITE_METHODS.begin(); it != READ_WRITE_METHODS.end(); ++it)
        m_hashTable[*it] = READ_WRITE;
    for (auto it = BATCH_METHODS.begin(); it != BATCH_METHODS.end(); ++it)
        m_hashTable[*it] = READ_WRITE;
}

AccessType PredisAccessLookupTable::getAccessType(std::string method)
{
    auto it = m_hashTable.find(method);
    return it == m_hashTable.end() ? MISC : it->second;
}

static const std::string REDIS_DEFAULT_PORT = "6379";

const AHandleInfo::TypeTag PredisHandleInfo::typeTag;

PredisPropertyGenerator predisPropertyGenerator;

bool PredisPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                            const CurrentExitCall* exitCall,
                                            StringMap& properties) const
{
    BOOST_ASSERT(exitCall);
    const HandleBasedCurrentExitCall* handleExitCall = static_cast<const HandleBasedCurrentExitCall*>(exitCall);
    boost::shared_ptr<PredisHandleInfo> handleInfo(
        boost::static_pointer_cast<PredisHandleInfo>(handleExitCall->getHandleInfo())
    );
    if (!handleInfo)
        return false;

    std::string masterSlaveString(handleInfo->getMaster());
    if (handleInfo->getSlaves()->size() > 0) {
        masterSlaveString += "\n";
        std::string slaveListString = boost::algorithm::join<std::set<std::string>,
                                                             std::string>(*handleInfo->getSlaves(), "\n");
        masterSlaveString += slaveListString;
    }

    properties[enum2str(CachingExitPoint_SERVER_POOL)] = masterSlaveString;
    properties[enum2str(CachingExitPoint_VENDOR)] = "REDIS";

    return true;
}

void* PredisConstructorInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void PredisConstructorInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                                 void* state)
{
    zval* exceptionObject = execEnv->getExceptionObject();
    if (exceptionObject) {
        ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->reportException(execEnv, exceptionObject);
        return;
    }

    ZValPointerObject handle(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());

    if (!handle)
        return;

    boost::shared_ptr<PredisHandleInfo> handleInfo;
    boost::shared_ptr<std::set<std::string>> slaveServers = boost::make_shared<std::set<std::string>>();
    boost::shared_ptr<std::string> handleName = boost::make_shared<std::string>();

    if (execEnv->getParamCount() > 0) {
        if (!PredisHandleInfo::generateHandleName(execEnv, &handleName, &slaveServers))
            return;
        // If generateHandleName returns empty string, something likely failed during connection.
        if (handleName->size() == 0)
            return;

        handleInfo = PredisHandleInfo::create(*handleName, &slaveServers);
        execEnv->getHandleRegistry().registerHandle(handle, handleInfo);
        return;
    }

    if (!handleInfo) {
        handleInfo = PredisHandleInfo::create();
        execEnv->getHandleRegistry().registerHandle(handle, handleInfo);
    }
}

/**
 * Generates an appropriate handle name for the predis backend. slaveServers is modified to include the slaves if applicable.
 */
bool PredisHandleInfo::generateHandleName(const PHPExecEnvironment* execEnv,
                                          boost::shared_ptr<std::string>* handleName,
                                          boost::shared_ptr<std::set<std::string>>* slaveServers)
{
    ZValPointerObject invokedObject(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());
    // Getting private $options in Client.php.
    ZValPointerObject clientOptions(invokedObject.findPrivatePropertyByName<ZValPointerObject>(execEnv,
                                                                                        OPTIONS_VARIABLE_NAME,
                                                                                        strlen(OPTIONS_VARIABLE_NAME),
                                                                                        invokedObject.getClassEntry(execEnv)));
    if (!clientOptions){
        clientOptions= ZValPointerObject(invokedObject.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                        OPTIONS_VARIABLE_NAME,
                                                                                        strlen(OPTIONS_VARIABLE_NAME)));
        if(!clientOptions)
            return false;
    }
    // Getting private $options in ClientOptions.php.
    ZValPointerArray options(clientOptions.findPrivatePropertyByName<ZValPointerArray>(execEnv,
                                                                                        OPTIONS_VARIABLE_NAME,
                                                                                        strlen(OPTIONS_VARIABLE_NAME),
                                                                                        clientOptions.getClassEntry(execEnv)));

    // Getting private $connection in Client.php.
    ZValPointerObject connection(invokedObject.findPrivatePropertyByName<ZValPointerObject>(execEnv,
                                                                                            CONNECTION_PROPERTY_NAME,
                                                                                            strlen(CONNECTION_PROPERTY_NAME),
                                                                                            invokedObject.getClassEntry(execEnv)));
    if(!connection){
        connection= ZValPointerObject(invokedObject.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                            CONNECTION_PROPERTY_NAME,
                                                                                            strlen(CONNECTION_PROPERTY_NAME)));
    }
    // Getting value of $options['replication'] from second argument to __construct in Client.php.
    bool useReplicationScheme = false;
    if (execEnv->getParamCount() > 1) {
        ZValPointerArray constructOptions(execEnv->getParam<ZValPointerArray>(1));
        if (!constructOptions)
            return false;
        ZValPointerBool replicationValue(constructOptions.findEntryByName<ZValPointerBool>(REPLICATION_OPTION_NAME,
                                                                             strlen(REPLICATION_OPTION_NAME)));
        useReplicationScheme = replicationValue && replicationValue.getBoolValue();
    }
    // Replication case.
    if (useReplicationScheme) {
        std::string discoveredHandle;

        // set master as handleInfo name and attribute
        if (!connection)
            return false;
        ZValPointerObject masterServer(connection.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                            MASTER_ATTRIBUTE_NAME,
                                                            strlen(MASTER_ATTRIBUTE_NAME)));
        if (!masterServer)
            return false;
        ZValPointerObject masterServerParametersObject(masterServer.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                                         PARAMETERS_ATTRIBUTE_NAME,
                                                                                                         strlen(PARAMETERS_ATTRIBUTE_NAME)));
        if (!masterServerParametersObject)
            return false;
        ZValPointerArray masterServerParametersArray(masterServerParametersObject.findPrivatePropertyByName<ZValPointerArray>(execEnv,
                                                                                                                        PARAMETERS_ATTRIBUTE_NAME,
                                                                                                                        strlen(PARAMETERS_ATTRIBUTE_NAME),
                                                                                                   masterServerParametersObject.getClassEntry(execEnv)));
        if (!masterServerParametersArray)
            return false;
        ZValPointerString masterHost(masterServerParametersArray.findEntryByName<ZValPointerString>(HOST_ATTR_NAME,
                                                                                               strlen(HOST_ATTR_NAME)));
        ZValPointerAny masterPort(masterServerParametersArray.findEntryByName<ZValPointerAny>(PORT_ATTR_NAME,
                                                                                               strlen(PORT_ATTR_NAME)));
        if (!masterHost)
            return false;

        discoveredHandle = masterHost.getStringValue();
        std::string masterPortString = boost::lexical_cast<std::string>(masterPort.convertToLong());
        if (masterPortString.size() == 0)
            masterPortString = REDIS_DEFAULT_PORT;
        discoveredHandle += ":" + masterPortString;

        *handleName = boost::make_shared<std::string>(discoveredHandle);

        // Get $connection->slaves array from Client.php (structured in MasterSlaveReplication.php)
        ZValPointerArray slaveServersArray(connection.findProtectedPropertyByName<ZValPointerArray>(execEnv,
                                                                                               SLAVE_ATTRIBUTE_NAME,
                                                                                               strlen(SLAVE_ATTRIBUTE_NAME)));
        if (!slaveServersArray)
            return false;
        std::set<std::string>* slaveSet = new std::set<std::string>();
        for (auto it = slaveServersArray.begin(); it != slaveServersArray.end(); ++it) {
            ZValPointerObject slave = (*it).getValue().cast<ZValPointerObject>();
            // Get protected Predis\Connection\ConnectionParameters from slave server.
            ZValPointerObject slaveParameters(slave.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                                   PARAMETERS_ATTRIBUTE_NAME,
                                                                                                   strlen(PARAMETERS_ATTRIBUTE_NAME)));
            if (!slaveParameters)
                return false;
            // Get private $parameters from ConnectionParameters class.
            ZValPointerArray slaveParametersArray(slaveParameters.findPrivatePropertyByName<ZValPointerArray>(execEnv,
                                                                                                              PARAMETERS_ATTRIBUTE_NAME,
                                                                                                              strlen(PARAMETERS_ATTRIBUTE_NAME),
                                                                                                              slaveParameters.getClassEntry(execEnv)));
            if (!slaveParametersArray)
                return false;
            // Get host and port strings from $parameters.
            ZValPointerString slaveHost(slaveParametersArray.findEntryByName<ZValPointerString>(HOST_ATTR_NAME,
                                                                                                strlen(HOST_ATTR_NAME)));
            ZValPointerLong slavePort(slaveParametersArray.findEntryByName<ZValPointerLong>(PORT_ATTR_NAME,
                                                                                                strlen(PORT_ATTR_NAME)));
            // Null checks on host.
            if (!slaveHost)
                continue;
            std::string slaveHostString(slaveHost.getStringValue());
            if (slaveHostString.size() == 0)
                continue;
            std::string slaveServerString = slaveHostString;
            std::string slavePortString = REDIS_DEFAULT_PORT;

            if (slavePort) {
                long slavePortLong = 0;
                bool const slavePortIsLong =
                    slavePort.convertInternalFunctionParameterToLong(&slavePortLong,
                                                                        false);
                if (slavePortIsLong)
                    slavePortString = boost::lexical_cast<std::string>(slavePortLong);
            }
            slaveServerString += ":" + slavePortString;
            slaveSet->insert(slaveServerString);
        }

        // Report discovered slave servers.
        if (slaveSet->size() > 0)
            *slaveServers = boost::shared_ptr<std::set<std::string>>(slaveSet);
    }

    // Cluster or single-server case.
    else {
        // append list of servers
        std::vector<std::string> servers;

        // Get the first argument of the constructor to Predis\Client, which contains the parameters.
        ZValPointerArray parameters = execEnv->getParam(0).cast<ZValPointerArray>();
        if (!parameters)
            return false;
        ZValPointerString host(parameters.findEntryByName<ZValPointerString>(HOST_ATTR_NAME, strlen(HOST_ATTR_NAME)));
        ZValPointerAny port(parameters.findEntryByName<ZValPointerAny>(PORT_ATTR_NAME, strlen(PORT_ATTR_NAME)));

        if (!host)
            return false;

        std::string discoveredHandle = host.getStringValue();
        // TODO: Check lib/Predis/Connection/ConnectionParameters.php $defaults array for default host and port?
        long portLong = port.convertToLong();
        std::string portString = portLong > 0 ? boost::lexical_cast<std::string>(portLong)
                                                : REDIS_DEFAULT_PORT;

        discoveredHandle += ":" + portString;

        *handleName = boost::make_shared<std::string>(discoveredHandle);
    }
    return true;
}

AccessType APredisTrafficInterceptor::getAccessType(const std::string& functionName) const
{
    static PredisAccessLookupTable lookupTable;
    return lookupTable.getAccessType(functionName);
}

AccessType PredisCallTrafficInterceptor::getAccessType(const PHPExecEnvironment* execEnv) const
{
    ZValPointerString nthParameterZVal = getNthParameter(execEnv, 0).cast<ZValPointerString>();
    if (!nthParameterZVal)
        return MISC;
    // Function name of interest is actually the first argument to the __call function. execEnv->getFunctionName would just return "__call".
    std::string functionName = nthParameterZVal.getStringValue();
    if (functionName.size() == 0)
        return MISC;
    return APredisTrafficInterceptor::getAccessType(functionName);
}

AccessType PredisExecuteCommandTrafficInterceptor::getAccessType(const PHPExecEnvironment* execEnv) const
{
    ZValPointerObject command = getNthParameter(execEnv, 0).cast<ZValPointerObject>();
    if (!command)
        return MISC;
    std::string functionName = command.getClassName(execEnv);
    if (functionName.size() == 0)
        return MISC;
    return APredisTrafficInterceptor::getAccessType(functionName);
}

AccessType PredisBatchTrafficInterceptor::getAccessType(const PHPExecEnvironment* execEnv) const
{
    return READ_WRITE;
}

void* PredisCloseInterceptor::onCallableBegin(const PHPExecEnvironment*)
{
    return NULL;
}

void PredisCloseInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                        void* state)
{
    ZValPointerObject handle(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());
    if (!handle)
        return;

    boost::shared_ptr<PredisHandleInfo> handleInfo;
    handleInfo = execEnv->getHandleRegistry().getHandleInfo<PredisHandleInfo>(handle);
    // Check that handle is registered in the first place before unregistering.
    if (!handleInfo)
        return;

    execEnv->getHandleRegistry().unregisterHandle(handle);
}

const char APredisTrafficInterceptor::cacheTypeName[] = "Predis";
#endif
