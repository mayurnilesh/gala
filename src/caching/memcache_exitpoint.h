/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#ifndef __memcache__exitpoint_h
#define __memcache__exitpoint_h

#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "caching_traffic_interceptor.h"
#include "method_selector.h"

//class HandleRegistry;

namespace memcache
{
    class MemcacheHandleInfo : public AExitPointHandleInfo {
            friend class ::HandleRegistry;
            static const TypeTag typeTag;
        public:
            template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
            static inline boost::shared_ptr<MemcacheHandleInfo> create(const appdynamics::pb::Agent::ExitPointType& type) { return boost::make_shared<MemcacheHandleInfo>(type); }

            inline bool isResolved() const { return m_resolved; }
            inline void setResolved() { m_resolved = true; }
            void addToServerList(const std::string & serverName, long port);

            inline const std::vector<std::string> & getServerList() const { return m_serverList; };

        private:
            inline MemcacheHandleInfo(const appdynamics::pb::Agent::ExitPointType& type)
                : AExitPointHandleInfo(type, &typeTag)
                , m_resolved(false) {}

            bool m_resolved;
            std::vector<std::string> m_serverList;
    };

    class MemcachePropertyGenerator : public IBackendPropertyGenerator
    {
        public:
            bool getProperties(const PHPExecEnvironment* execEnv,
                               const CurrentExitCall* exitCall,
                               StringMap& properties) const;
    };
    extern MemcachePropertyGenerator memcachePropertyGenerator;

    /**
        Interceptor abstract class for memcache_connect and memcache::connect.
     */
    class AServerConnectionInterceptor : public AMethodInterceptor
                                       , protected AMethodInvocationSelector
    {
        protected:
            AServerConnectionInterceptor(const char* const className, InterceptorRegistry::ID id, AMethodInvocationSelector::CallableType type)
                : AMethodInterceptor(className, id)
                , AMethodInvocationSelector(type)
                , m_invocationType(type)
            {
            }

            virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);
            virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                        void* state);
            virtual inline bool needParamsInOnMethodBegin() const { return true; }
            virtual inline bool shouldCallOnMethodEnd() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return true; }

            inline bool isActive(const PHPExecEnvironment* execEnv) const
            {
                return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_CACHE);
            }
        private:
            AMethodInvocationSelector::CallableType const m_invocationType;
    };

    /**
        Implementation of server connection class for procedural call.
     */
    class ServerConnectionProcedureInterceptor : public AServerConnectionInterceptor
    {
        public:
            ServerConnectionProcedureInterceptor()
                : AServerConnectionInterceptor("memcache::ServerConnectionProcedureInterceptor",
                                               InterceptorRegistry::MemcacheServerConnectionProcedureInterceptor_ID,
                                               AMethodInvocationSelector::kInitProcedure)
            {
            }
    };

    /**
        Implementation of server connection class for memcache_add_server procedural call.
     */
    class AddServerConnectionProcedureInterceptor : public AServerConnectionInterceptor
    {
        public:
            AddServerConnectionProcedureInterceptor()
                : AServerConnectionInterceptor("memcache::AddServerConnectionProcedureInterceptor",
                                               InterceptorRegistry::MemcacheAddServerProcedureInterceptor_ID,
                                               AMethodInvocationSelector::kProcedure)
            {
            }
    };

    /**
        Implementation of server connection class for object-oriented call.
     */
    class ServerConnectionMethodInterceptor : public AServerConnectionInterceptor
    {
        public:
            ServerConnectionMethodInterceptor()
                : AServerConnectionInterceptor("memcache::ServerConnectionMethodInterceptor",
                                               InterceptorRegistry::MemcacheServerConnectionMethodInterceptor_ID,
                                               AMethodInvocationSelector::kMethod)
            {
            }
    };

    //TODO: Real*Interceptor?

    /**
        Interceptor abstract class for memcache_close and memcache::close.
     */
    class ACloseInterceptor : public AMethodInterceptor
                            , public AMethodInvocationSelector
    {
        protected:
            ACloseInterceptor(const char* const className, InterceptorRegistry::ID id, AMethodInvocationSelector::CallableType type)
                : AMethodInterceptor(className, id)
                , AMethodInvocationSelector(type)
            {
            }

            virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);
            virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                        void* state);
            virtual inline bool needParamsInOnMethodBegin() const { return true; }
            virtual inline bool shouldCallOnMethodEnd() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }

            inline bool isActive(const PHPExecEnvironment* execEnv) const
            {
                return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_CACHE);
            }
    };

    /**
        Implementation of memcache close class for procedural call.
     */
    class CloseProcedureInterceptor : public ACloseInterceptor
    {
        public:
            CloseProcedureInterceptor()
                : ACloseInterceptor("memcache::CloseProcedureInterceptor",
                                    InterceptorRegistry::MemcacheCloseProcedureInterceptor_ID,
                                    AMethodInvocationSelector::kProcedure)
            {
            }
    };

    /**
        Implementation of memcache close class for object-oriented call.
     */
    class CloseMethodInterceptor : public ACloseInterceptor
    {
        public:
            CloseMethodInterceptor()
                : ACloseInterceptor("memcache::CloseMethodInterceptor",
                                    InterceptorRegistry::MemcacheCloseMethodInterceptor_ID,
                                    AMethodInvocationSelector::kMethod)
            {
            }
    };

    /**
        Interceptor abstract class for memcache traffic calls (read, write).
     */
    class AMemcacheTrafficInterceptor : public ACachingTrafficInterceptor<AMemcacheTrafficInterceptor,
                                                                          MemcachePropertyGenerator,
                                                                          &memcachePropertyGenerator,
                                                                          MemcacheHandleInfo>
                                      , public AMethodInvocationSelector
    {
            friend class ACachingTrafficInterceptor<AMemcacheTrafficInterceptor,
                                                    MemcachePropertyGenerator,
                                                    &memcachePropertyGenerator,
                                                    MemcacheHandleInfo>;
        public:
            static const char cacheTypeName[];
            AMemcacheTrafficInterceptor(const char* const className, InterceptorRegistry::ID id, AMethodInvocationSelector::CallableType type)
                : ACachingTrafficInterceptor<AMemcacheTrafficInterceptor,
                                             MemcachePropertyGenerator,
                                             &memcachePropertyGenerator,
                                             MemcacheHandleInfo>(className, id)
                , AMethodInvocationSelector(type)
            {
            }
    };

    /**
        Interceptor abstract class for memcache read functions, implements abstract class for traffic calls.
     */
    class AReadInterceptor : public AMemcacheTrafficInterceptor
    {
        protected:
            AReadInterceptor(const char* const className, InterceptorRegistry::ID id, AMethodInvocationSelector::CallableType type)
                : AMemcacheTrafficInterceptor(className, id, type)
            {
            }
        public:
            AccessType getAccessType(const PHPExecEnvironment* execEnv) const { return READ; }
    };

    /**
        Implementation of memcache read class for procedural call.
     */
    class ReadProcedureInterceptor : public AReadInterceptor
    {
        public:
            ReadProcedureInterceptor()
                : AReadInterceptor("memcache::ReadProcedureInterceptor",
                       InterceptorRegistry::MemcacheReadProcedureInterceptor_ID,
                       AMethodInvocationSelector::kProcedure)
            {
            }
            virtual ~ReadProcedureInterceptor() { }
    };

    /**
        Implementation of memcache read class for object-oriented call.
     */
    class ReadMethodInterceptor : public AReadInterceptor
    {
        public:
            ReadMethodInterceptor()
                : AReadInterceptor("memcache::ReadMethodInterceptor",
                                   InterceptorRegistry::MemcacheReadMethodInterceptor_ID,
                                   AMethodInvocationSelector::kMethod)
            {
            }
            virtual ~ReadMethodInterceptor() { }
    };

    /**
        Interceptor abstract class for memcache write functions, implements abstract class for traffic calls.
     */
    class AWriteInterceptor : public AMemcacheTrafficInterceptor
    {
        protected:
            AWriteInterceptor(const char* const className, InterceptorRegistry::ID id, AMethodInvocationSelector::CallableType type)
                : AMemcacheTrafficInterceptor(className, id, type)
            {
            }
        public:
            AccessType getAccessType(const PHPExecEnvironment* execEnv) const { return WRITE; }
    };

    /**
        Implementation of memcache write class for procedural call.
     */
    class WriteProcedureInterceptor : public AWriteInterceptor
    {
        public:
            WriteProcedureInterceptor()
                : AWriteInterceptor("memcache::WriteProcedureInterceptor",
                                    InterceptorRegistry::MemcacheWriteProcedureInterceptor_ID,
                                    AMethodInvocationSelector::kProcedure)
            {
            }
            virtual ~WriteProcedureInterceptor() { }
    };

    /**
        Implementation of memcache write class for object-oriented call.
     */
    class WriteMethodInterceptor : public AWriteInterceptor
    {
        public:
            WriteMethodInterceptor()
                : AWriteInterceptor("memcache::WriteMethodInterceptor",
                                     InterceptorRegistry::MemcacheWriteMethodInterceptor_ID,
                                     AMethodInvocationSelector::kMethod)
            {
            }
            virtual ~WriteMethodInterceptor() { }
    };
}

#endif // __memcache__h
