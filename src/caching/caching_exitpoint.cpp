#include "caching_exitpoint.h"
#include "intercept.h"
#include "memcache_exitpoint.h"
#include "memcached_exitpoint.h"

#include "main/php_version.h"

#if PHP_VERSION_ID >= 50300
#include "predis_exitpoint.h"
#endif

DEFINE_ENUM(CachingExitPointIndentifyingPropertyNames, CACHING_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

/** Memcache API **/
static const char MEMCACHE_SYMBOL[] = "Memcache";
static const char* const MEMCACHE_SERVER_CONNECTION_METHODS[] = {
    "connect",
    "addServer",
    "pconnect"
};
static const char* const MEMCACHE_SERVER_CONNECTION_PROCEDURES[] = {
    "memcache_connect",
    "memcache_pconnect"
};
static const size_t MEMCACHE_SERVER_CONNECTION_METHODS_COUNT
    = sizeof(MEMCACHE_SERVER_CONNECTION_METHODS) / sizeof(MEMCACHE_SERVER_CONNECTION_METHODS[0]);
static const size_t MEMCACHE_SERVER_CONNECTION_PROCEDURES_COUNT
    = sizeof(MEMCACHE_SERVER_CONNECTION_PROCEDURES) / sizeof(MEMCACHE_SERVER_CONNECTION_PROCEDURES[0]);
static const char MEMCACHE_ADD_SERVER_CONNECTION_PROCEDURE[] = "memcache_add_server";
static const char RESET_SERVER_LIST_METHOD[] = "resetServerList";
static const char* const MEMCACHE_READ_METHODS[] =  {
    "get",
    "getExtendedStats",
    "getServerStatus",
    "getStats",
    "getVersion"
};

static const char* const MEMCACHE_READ_PROCEDURES[] =  {
    "memcache_get",
    "memcache_get_extended_stats",
    "memcache_get_server_status",
    "memcache_get_stats",
    "memcache_get_version"
};

static const char* const MEMCACHE_WRITE_METHODS[] =  {
    "add",
    "decrement",
    "delete",
    "flush",
    "increment",
    "replace",
    "set"
};

static const char* const MEMCACHE_WRITE_PROCEDURES[] =  {
    "memcache_add",
    "memcache_decrement",
    "memcache_delete",
    "memcache_flush",
    "memcache_increment",
    "memcache_replace",
    "memcache_set"
};

static const char MEMCACHE_CLOSE_METHOD[] = "close";
static const char MEMCACHE_CLOSE_PROCEDURE[] = "memcache_close";
static const size_t MEMCACHE_READ_METHODS_COUNT = sizeof(MEMCACHE_READ_METHODS) / sizeof(MEMCACHE_READ_METHODS[0]);
static const size_t MEMCACHE_WRITE_METHODS_COUNT = sizeof(MEMCACHE_WRITE_METHODS) / sizeof(MEMCACHE_WRITE_METHODS[0]);
static const size_t MEMCACHE_READ_PROCEDURES_COUNT = sizeof(MEMCACHE_READ_PROCEDURES) / sizeof(MEMCACHE_READ_PROCEDURES[0]);
static const size_t MEMCACHE_WRITE_PROCEDURES_COUNT = sizeof(MEMCACHE_WRITE_PROCEDURES) / sizeof(MEMCACHE_WRITE_PROCEDURES[0]);


#if PHP_VERSION_ID >= 50300
/** Predis API **/
static const char PREDIS_SYMBOL[] = "Predis\\Client";
static const char PREDIS_CALL_METHOD[] = "__call";
static const char PREDIS_EXECUTE_COMMAND_METHOD[] = "executeCommand";

static const char* const PREDIS_BATCH_METHODS[] = {
    "pipeline",
    "transaction",
    "multiExec", // Deprecated.
};
static const size_t PREDIS_BATCH_METHODS_COUNT = sizeof(PREDIS_BATCH_METHODS) / sizeof(PREDIS_BATCH_METHODS[0]);

static const char PREDIS_CLOSE_METHOD[] = "disconnect";
#endif

/** Memcached API **/
static const char MEMCACHED_SYMBOL[] = "Memcached";
static const char* const MEMCACHED_READ_METHODS[] =  {
    "get",
    "getAllKeys",
    "getByKey",
    "getDelayed",
    "getDelayedByKey",
    "getMulti",
    "getMultiKey",
    "getStats",
    "getVersion",
};

static const char* const MEMCACHED_WRITE_METHODS[] =  {
    "add",
    "addByKey",
    "append",
    "appendByKey",
    "cas",
    "casByKey",
    "decrement",
    "decrementByKey",
    "delete",
    "deleteByKey",
    "deleteMulti",
    "deleteMultiByKey",
    "flush",
    "increment",
    "incrementByKey",
    "prepend",
    "prependByKey",
    "replace",
    "replaceByKey",
    "set",
    "setByKey",
    "setMulti",
    "setMultiByKey",
    "touch",
    "touchByKey"
};

static const size_t MEMCACHED_READ_METHODS_COUNT = sizeof(MEMCACHED_READ_METHODS) / sizeof(MEMCACHED_READ_METHODS[0]);
static const size_t MEMCACHED_WRITE_METHODS_COUNT = sizeof(MEMCACHED_WRITE_METHODS) / sizeof(MEMCACHED_WRITE_METHODS[0]);


MemcachedConstructorInterceptor memcachedConstructorInterceptor;
MemcachedResetServerListInterceptor memcachedResetServerListInterceptor;
MemcachedReadInterceptor memcachedReadInterceptor;
MemcachedWriteInterceptor memcachedWriteInterceptor;

memcache::ServerConnectionMethodInterceptor memcacheServerConnectionMethodInterceptor;
memcache::AddServerConnectionProcedureInterceptor memcacheAddServerConnectionProcedureInterceptor;
memcache::ReadMethodInterceptor memcacheReadMethodInterceptor;
memcache::WriteMethodInterceptor memcacheWriteMethodInterceptor;
memcache::CloseMethodInterceptor memcacheCloseMethodInterceptor;

memcache::ServerConnectionProcedureInterceptor memcacheServerConnectionProcedureInterceptor;
memcache::ReadProcedureInterceptor memcacheReadProcedureInterceptor;
memcache::WriteProcedureInterceptor memcacheWriteProcedureInterceptor;
memcache::CloseProcedureInterceptor memcacheCloseProcedureInterceptor;

#if PHP_VERSION_ID >= 50300
PredisConstructorInterceptor predisConstructorInterceptor;
PredisCallTrafficInterceptor predisCallTrafficInterceptor;
PredisExecuteCommandTrafficInterceptor predisExecuteCommandTrafficInterceptor;
PredisBatchTrafficInterceptor predisBatchTrafficInterceptor;
PredisCloseInterceptor predisCloseInterceptor;
#endif

bool CachingExitPointDelegate::s_didApplyRules = false;

CachingExitPointDelegate::CachingExitPointDelegate(InterceptionEngine* interceptEngine,
                                                   TransactionMonitor* txMonitor)
    : AExitPointDelegate(interceptEngine, txMonitor)
{
}

void CachingExitPointDelegate::configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                                         const struct _zend_agent_globals* agentGlobals)
{
    boost::shared_ptr<t_BackendRulesList> cachingBackendRules(boost::make_shared<t_BackendRulesList>());
    cachingBackendRules->CopyFrom(backendConfig.cachebackendconfig());
    AExitPointDelegate::configure(cachingBackendRules, agentGlobals);
}

void CachingExitPointDelegate::applyRules(const _zend_agent_globals* agentGlobals)
{
    if (s_didApplyRules)
        return;

    s_didApplyRules = true;
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHED_SYMBOL, PHP_CONSTRUCT_METHOD), &memcachedConstructorInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHED_SYMBOL, RESET_SERVER_LIST_METHOD), &memcachedResetServerListInterceptor, true);

    for (int i = 0; i < MEMCACHE_SERVER_CONNECTION_METHODS_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_SYMBOL,
                                                                  MEMCACHE_SERVER_CONNECTION_METHODS[i]),
                                                     &memcacheServerConnectionMethodInterceptor,
                                                     true);
    for (int i = 0; i < MEMCACHE_SERVER_CONNECTION_PROCEDURES_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_SERVER_CONNECTION_PROCEDURES[i]),
                                                     &memcacheServerConnectionProcedureInterceptor,
                                                     true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_ADD_SERVER_CONNECTION_PROCEDURE),
                                                 &memcacheAddServerConnectionProcedureInterceptor,
                                                 true);


    for (int i = 0; i < MEMCACHED_READ_METHODS_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHED_SYMBOL, MEMCACHED_READ_METHODS[i]), &memcachedReadInterceptor, true);

    for (int i = 0; i < MEMCACHED_WRITE_METHODS_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHED_SYMBOL, MEMCACHED_WRITE_METHODS[i]), &memcachedWriteInterceptor, true);

    for (int i = 0; i < MEMCACHE_READ_METHODS_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_SYMBOL, MEMCACHE_READ_METHODS[i]), &memcacheReadMethodInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_SYMBOL, MEMCACHE_CLOSE_METHOD), &memcacheCloseMethodInterceptor, true);

    for (int i = 0; i < MEMCACHE_WRITE_METHODS_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_SYMBOL, MEMCACHE_WRITE_METHODS[i]), &memcacheWriteMethodInterceptor, true);

    for (int i = 0; i < MEMCACHE_READ_PROCEDURES_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_READ_PROCEDURES[i]), &memcacheReadProcedureInterceptor, true);

    for (int i = 0; i < MEMCACHE_WRITE_PROCEDURES_COUNT; ++i)
        m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_WRITE_PROCEDURES[i]), &memcacheWriteProcedureInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(MEMCACHE_CLOSE_PROCEDURE), &memcacheCloseProcedureInterceptor, true);


#if PHP_VERSION_ID >= 50300
    if (agentGlobals->G->enabled_frameworks.predis) {
        /** Predis **/
        m_interceptEngine->addInterceptorForCallable(CallableInfo(PREDIS_SYMBOL, PHP_CONSTRUCT_METHOD), &predisConstructorInterceptor);

        m_interceptEngine->addInterceptorForCallable(CallableInfo(PREDIS_SYMBOL, PREDIS_CALL_METHOD), &predisCallTrafficInterceptor);
        m_interceptEngine->addInterceptorForCallable(CallableInfo(PREDIS_SYMBOL, PREDIS_EXECUTE_COMMAND_METHOD), &predisExecuteCommandTrafficInterceptor);
        for (int i = 0; i < PREDIS_BATCH_METHODS_COUNT; ++i)
            m_interceptEngine->addInterceptorForCallable(CallableInfo(PREDIS_SYMBOL, PREDIS_BATCH_METHODS[i]), &predisBatchTrafficInterceptor);

        m_interceptEngine->addInterceptorForCallable(CallableInfo(PREDIS_SYMBOL, PREDIS_CLOSE_METHOD), &predisCloseInterceptor);
    }
#endif
}

bool CachingExitPointDelegate::processMatchRule(const PHPExecEnvironment* execEnv,
                                                const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                                                const StringMap& properties) const
{
    BOOST_ASSERT(matchRule.has_cachematchrule());

    const appdynamics::pb::Agent::BackendMatchRule_Cache& cacheMatchRule =
        matchRule.cachematchrule();
    auto notFound = properties.end();

    if (cacheMatchRule.has_serverpool()) {
        auto it = properties.find(enum2str(CachingExitPoint_SERVER_POOL));
        if (it == notFound)
            return false;
        StringMatch condition(cacheMatchRule.serverpool());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (cacheMatchRule.has_vendor()) {
        auto it = properties.find(enum2str(CachingExitPoint_VENDOR));
        if (it == notFound)
            return false;
        StringMatch condition(cacheMatchRule.vendor());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    return true;
}

void CachingExitPointDelegate::processNamingRule(const PHPExecEnvironment* execEnv,
                                                 const appdynamics::pb::Agent::BackendRule& backendRule,
                                                 const StringMap& properties,
                                                 StringMap& identifyingProperties) const
{
    BOOST_ASSERT(backendRule.namingrule().has_cachenamingrule());
    const appdynamics::pb::Agent::BackendNamingRule_Cache& cacheNamingRule =
        backendRule.namingrule().cachenamingrule();
    Expression::Context context(execEnv, &properties);

    if (cacheNamingRule.has_serverpool()) {
        evaluateProperty(enum2str(CachingExitPoint_SERVER_POOL),
                         cacheNamingRule.serverpool(),
                         context,
                         identifyingProperties);
    }

    if (cacheNamingRule.has_vendor()) {
        evaluateProperty(enum2str(CachingExitPoint_VENDOR),
                         cacheNamingRule.vendor(),
                         context,
                         identifyingProperties);
    }
}

std::string CachingExitPointDelegate::generateDisplayName(const ExitCallInfo& exitCallInfo)
{
    if (exitCallInfo.isApiCall())
        return exitCallInfo.getDisplayName();

    const StringMap& properties(exitCallInfo.getProperties());
    const appdynamics::pb::Agent::BackendRule* matchedRule = exitCallInfo.getMatchedConfigRule();
    BOOST_ASSERT(matchedRule);

    auto it = properties.find(enum2str(CachingExitPoint_SERVER_POOL));
    if (it == properties.end()) {
        return matchedRule->name();
    }

    std::string displayName;
    if (matchedRule->priority() > 0) {
        displayName.append(matchedRule->name());
        displayName.append(getDefaultDisplayNameDelimiter());
    }

    std::string serverPool(it->second);
    size_t found = serverPool.find("\n");
    if (found == std::string::npos)
        displayName.append(serverPool);
    else
        displayName.append(serverPool.substr(0, found));

    it = properties.find(enum2str(CachingExitPoint_VENDOR));
    if (it != properties.end())
        displayName.append(getDefaultDisplayNameDelimiter()).append(it->second);

    return displayName;
}
