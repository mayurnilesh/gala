#include "common.h"
#include "memcached_exitpoint.h"
#include "db/db_exitpoint.h"
#include "caching_exitpoint.h"
#include "boost/lexical_cast.hpp"
#include <sstream>
#include "emalloc_allocator.h"
#include "caching_traffic_interceptor.h"


const AHandleInfo::TypeTag MemcachedHandleInfo::typeTag;

MemcachedPropertyGenerator memcachedPropertyGenerator;

bool MemcachedPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                               const CurrentExitCall* exitCall,
                                               StringMap& properties) const
{
    zval* const invokedObject = execEnv->getInvokedObject();
    if (!invokedObject)
        return false;

    ZValPointerArray getServerListCallable(ZValPointerArray::create());
    getServerListCallable.pushEntry(ZValPointerAny::share(invokedObject));
    getServerListCallable.pushEntry(ZValPointerString::copy("getServerList"));

    ZValPointerAny returnValue;
    if (!execEnv->callCallable(&returnValue, getServerListCallable, NULL)) {
        return false;
    }

    ZValPointerArray serverList(returnValue.cast<ZValPointerArray>());
    if (!serverList)
        return false;
    std::stringstream ss;
    std::vector<std::string> servers;
    ExitCallInfo exitCallInfo(appdynamics::pb::Agent::EXIT_CACHE);

    for (auto it = serverList.begin(); it != serverList.end(); ++it) {
        ZValPointerArray entry((*it).getValue().cast<ZValPointerArray>());
        if (!entry)
            continue;

        ZValPointerString hostname(entry.findEntryByName<ZValPointerString>("host", strlen("host")));
        ZValPointerLong port(entry.findEntryByName<ZValPointerLong>("port", strlen("port")));
        if (!hostname || !port)
            continue;

        ss << hostname.getStringValue() << ":";
        ss << boost::lexical_cast<std::string>(port.getLongValue());
        servers.push_back(ss.str());
        ss.str("");
        ss.clear();
    }

    std::sort(servers.begin(), servers.end());

    // ss should be ready to use, as it's cleared at the end of the loop above
    copy(servers.begin(), servers.end(), std::ostream_iterator<std::string>(ss, "\n"));
    std::string serverPoolString(ss.str().substr(0, ss.str().size()-1));

    properties[enum2str(CachingExitPoint_SERVER_POOL)] = serverPoolString;
    properties[enum2str(CachingExitPoint_VENDOR)] = "MEMCACHED";

    return true;
};

void* MemcachedConstructorInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void  MemcachedConstructorInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                                     void* state)
{
    ZValPointerObject handle(ZValPointerAny::share(execEnv->getInvokedObject()).cast<ZValPointerObject>());

    // TODO there may be an error issued in the constructor
    if (!handle)
        return;

    boost::shared_ptr<MemcachedHandleInfo> handleInfo;

    /*
     * If there's a persistence ID, use it to try and retrieve the handle info
     * object. If not found, we create a new one and register it both under the
     * persistence ID and the object handle. This way we can use the same handle
     * info for multiple objects that have the same internal memcache
     * connection.
     */
    if (execEnv->getParamCount() > 0) {
        ZValPointerString persistString(execEnv->getParam<ZValPointerString>(0));
        if (persistString) {
            handleInfo = execEnv->getHandleRegistry().getHandleInfo<MemcachedHandleInfo>(persistString);
            if (!handleInfo) {
                handleInfo = MemcachedHandleInfo::create(appdynamics::pb::Agent::EXIT_CACHE);
                execEnv->getHandleRegistry().registerHandle(persistString, handleInfo);
            }
            execEnv->getHandleRegistry().registerHandle(handle, handleInfo);
            return;
        }
    }

    if (!handleInfo) {
        handleInfo = MemcachedHandleInfo::create(appdynamics::pb::Agent::EXIT_CACHE);
        execEnv->getHandleRegistry().registerHandle(handle, handleInfo);
    }
}

const char AMemcachedTrafficInterceptor::cacheTypeName[] = "Memcached";

void* MemcachedResetServerListInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void  MemcachedResetServerListInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                                         void* state)
{
    zval* invokedObject = execEnv->getInvokedObject();
    if (!invokedObject)
        return;

    ZValPointerObject handle(ZValPointerAny::share(invokedObject).cast<ZValPointerObject>());
    boost::shared_ptr<MemcachedHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<MemcachedHandleInfo>(handle));
    if (!handleInfo)
        return;
    handleInfo->clearResolved();
}
