#ifndef __caching_traffic_interceptor_h
#define __caching_traffic_interceptor_h

#include "exitcall_detail.h"
#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "caching_traffic_interceptor.h"
#include "db/db_exitpoint.h"
#include "boost/lexical_cast.hpp"
#include "util.h"
#include "zval_helper.h"
#include "transactions.h"

class CurrentExitCall;

enum AccessType
{
    READ,
    WRITE,
    READ_WRITE,
    MISC
};

static const char READ_CATEGORY[] = "read";
static const char WRITE_CATEGORY[] = "write";
static const char READ_WRITE_CATEGORY[] = "read_write";
static const char MISC_CATEGORY[] = "misc";

/**
    Template class for intercepting traffic, used for both memcache and memcached.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
class ACachingTrafficInterceptor : public ExitCallDetailHelper<AHandleBasedExitPointInterceptor<t_Derived,
                                                                                                t_HandleInfo>,
                                                               t_Derived,
                                                               appdynamics::pb::Agent::EXIT_CACHE>
{
        friend class ExitCallDetailHelper<AHandleBasedExitPointInterceptor<t_Derived,
                                                                           t_HandleInfo>,
                                          t_Derived,
                                          appdynamics::pb::Agent::EXIT_CACHE>;
    public:
        ACachingTrafficInterceptor(const char* const className, InterceptorRegistry::ID id);
        virtual ~ACachingTrafficInterceptor() { }
    protected:
        virtual bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const;

        virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exit_call,
                                              const PHPExecEnvironment* execEnv) const;

        virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                       uint64_t sequenceNumber) const;
        virtual boost::shared_ptr<AErrorObject>
            detectErrors(CurrentExitCall* currentExitCall,
                         const PHPExecEnvironment* phpExecEnv);

        virtual AccessType getAccessType (const PHPExecEnvironment* execEnv) const = 0;
    private:
        void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                       const PHPExecEnvironment* execEnv,
                                       const boost::shared_ptr<SnapshotExitCall>& sec) const;
};

/**
    Implementation (constructor) for traffic interception template class.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
ACachingTrafficInterceptor<t_Derived,
                           t_propertyGenerator,
                           propertyGenerator,
                           t_HandleInfo>::ACachingTrafficInterceptor(const char* const className,
                                                                     InterceptorRegistry::ID id)
    : ExitCallDetailHelper<AHandleBasedExitPointInterceptor<t_Derived, t_HandleInfo>,
                           t_Derived,
                           appdynamics::pb::Agent::EXIT_CACHE>(className, id, appdynamics::pb::Agent::EXIT_CACHE)
{
}

/**
    Returns the handle info for the traffic interceptor.

    @param handlePointer  The pointer which will be mutated to point to the requested handle info, if found.
    @param execEnv  The PHP execution environment of the interceptor.
    @return true iff the handle info is found from the execution environment.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
bool ACachingTrafficInterceptor<t_Derived,
                                t_propertyGenerator,
                                propertyGenerator,
                                t_HandleInfo>::getHandle(ZValPointerAny* handlePointer,
                                                         const PHPExecEnvironment* execEnv) const
{
    const t_Derived* derivedThis = static_cast<const t_Derived*>(this);
    ZValPointerAny thisZVal(derivedThis->getThisZVal(execEnv));
    ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
    if (!handle)
        return false;

    *handlePointer = handle;
    return true;
}

/**
    Creates exit call info.

    @param exitCall Exit call from which to extract exit call info.
    @param execEnv The PHP execution environment of the interceptor.
    @return Exit call info.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
ExitCallInfo ACachingTrafficInterceptor<t_Derived,
                                        t_propertyGenerator,
                                        propertyGenerator,
                                        t_HandleInfo>::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                                        const PHPExecEnvironment* execEnv) const
{
    BOOST_ASSERT(exitCall);
    const HandleBasedCurrentExitCall* handleExitCall = static_cast<const HandleBasedCurrentExitCall*>(exitCall);
    boost::shared_ptr<t_HandleInfo> handleInfo = boost::static_pointer_cast<t_HandleInfo>(handleExitCall->getHandleInfo());
    if (handleInfo->isResolved())
        return handleInfo->getExitCallInfo();

    const t_Derived* derivedThis = static_cast<const t_Derived*>(this);
    ExitCallInfo exitCallInfo(
        AG(kernel)->getAgentContext()
                  ->getTransactionMonitor()
                  ->getExitPointDelegate(derivedThis->getExitPointType())
                  ->makeIdentifyingProperties(execEnv, exitCall, *propertyGenerator)
    );
    if (exitCallInfo.isValid()) {
        handleInfo->setExitCallInfo(exitCallInfo);
        handleInfo->setResolved();
    }

    return exitCallInfo;
}

/**
    Creates the current exit call info, based on the state of the execution environment.

    @param execEnv The PHP execution environment of the interceptor.
    @return Exit call info of the execution environment in its current state.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
CurrentExitCall* ACachingTrafficInterceptor<t_Derived,
                                            t_propertyGenerator,
                                            propertyGenerator,
                                            t_HandleInfo>::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                                 uint64_t sequenceNumber) const
{
    CurrentExitCall *exitCall =
        AHandleBasedExitPointInterceptor<t_Derived, t_HandleInfo>::createCurrentExitCall(execEnv,
                                                                                         sequenceNumber);
    if (exitCall){
        switch (getAccessType(execEnv)) {
            case READ:
                exitCall->setCategory(READ_CATEGORY);
                break;
            case WRITE:
                exitCall->setCategory(WRITE_CATEGORY);
                break;
            case READ_WRITE:
                exitCall->setCategory(READ_WRITE_CATEGORY);
                break;
            case MISC:
                exitCall->setCategory(MISC_CATEGORY);
                break;
        }
    }

    return exitCall;
}

/**
    Adds the function name to the snapshot exit call's properties.

    @param exit_call Currently not used.
    @param execEnv The PHP execution environment of the interceptor.
    @param sec The snapshot exit call to augment with the function name.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
void ACachingTrafficInterceptor<t_Derived,
                                t_propertyGenerator,
                                propertyGenerator,
                                t_HandleInfo>::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                                         const PHPExecEnvironment* execEnv,
                                                                         const boost::shared_ptr<SnapshotExitCall>& sec) const
{
    // TODO something for detail string?
    sec->addProperty("method", execEnv->getFunctionName());
}

/**
    Detects any errors in the given exit call.

    @param currentExitCall The current exit call from which to extract any errors, if any.
    @param phpExecEnv The PHP execution environment of the interceptor.
    @return A boost::shared_ptr to any detected errors.
 */
template <class t_Derived,
          class t_propertyGenerator,
          const t_propertyGenerator *propertyGenerator,
          class t_HandleInfo>
boost::shared_ptr<AErrorObject>
ACachingTrafficInterceptor<t_Derived,
                           t_propertyGenerator,
                           propertyGenerator,
                           t_HandleInfo>::detectErrors(CurrentExitCall* currentExitCall,
                                                       const PHPExecEnvironment* phpExecEnv)
{
    return AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, t_Derived::cacheTypeName, phpExecEnv);
}

#endif // __caching_traffic_interceptor__h
