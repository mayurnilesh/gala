/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#ifndef __phpredis__exitpoint_h
#define __phpredis__exitpoint_h

#include "main/php_version.h"

#if PHP_VERSION_ID >= 50300

#include <boost/unordered_map.hpp>

#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "exitcall_detail.h"
#include "caching_traffic_interceptor.h"
#include "method_selector.h"

class PredisAccessLookupTable
{
    public:
        PredisAccessLookupTable();
        AccessType getAccessType(std::string method);

    private:
        boost::unordered_map<std::string, AccessType> m_hashTable;
};

/**
 * Handle info class for Predis extension. Stores all relevant information to identify a Predis backend,
 * including the servers as arranged in a master/slave replication scheme.
 */
class PredisHandleInfo : public AExitPointHandleInfo {
        friend class HandleRegistry;
        template <class T, class Arg1, class... Args>
        friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        template <class T>
        friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared();
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend boost::shared_ptr<T> boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<PredisHandleInfo> create(std::string handleName, boost::shared_ptr<std::set<std::string>>* slaves)
        { return boost::make_shared<PredisHandleInfo>(handleName, slaves); }
        static inline boost::shared_ptr<PredisHandleInfo> create()
        { return boost::make_shared<PredisHandleInfo>(); }
        static bool generateHandleName(const PHPExecEnvironment* execEnv,
                                       boost::shared_ptr<std::string>* handleName,
                                       boost::shared_ptr<std::set<std::string>>* slaveServers);

        inline bool isResolved() { return m_resolved; }
        inline void clearResolved() { m_resolved = false; }
        inline void setResolved() { m_resolved = true; }

        inline std::string getMaster() const { return m_master; }
        inline void setMaster(std::string master) { m_master = master; }
        inline const boost::shared_ptr<std::set<std::string>>& getSlaves() const { return m_slaves; }
        inline void setSlaves(boost::shared_ptr<std::set<std::string>>* slaves) { m_slaves = *slaves; }
        inline void addSlave(std::string slave) { m_slaves->insert(slave); }
        inline void removeSlave(std::string slave) {
            std::set<std::string>::iterator it;
            it = m_slaves->find(slave);
            if (it != m_slaves->end())
                m_slaves->erase(it);
        }
    private:
        inline PredisHandleInfo()
            : AExitPointHandleInfo(appdynamics::pb::Agent::EXIT_CACHE, &typeTag)
            , m_resolved(false) 
            , m_slaves(boost::make_shared<std::set<std::string> >()) {}
        inline PredisHandleInfo(std::string master, boost::shared_ptr<std::set<std::string>>* slaves)
            : AExitPointHandleInfo(appdynamics::pb::Agent::EXIT_CACHE, &typeTag)
            , m_resolved(false)
            , m_master(master)
            , m_slaves(*slaves) {}

        bool m_resolved;

        // For the master/slave replication case:
        std::string m_master;
        boost::shared_ptr<std::set<std::string>> m_slaves;
};

class PredisConstructorInterceptor : public AMethodInterceptor
{
    public:
        PredisConstructorInterceptor()
            : AMethodInterceptor("PredisConstructorInterceptor",
                                 InterceptorRegistry::PredisConstructorInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_CACHE);
        }
};

class PredisPropertyGenerator : public IBackendPropertyGenerator
{
    public:
        bool getProperties(const PHPExecEnvironment* execEnv,
                           const CurrentExitCall* exitCall,
                           StringMap& properties) const;
};
extern PredisPropertyGenerator predisPropertyGenerator;

class APredisTrafficInterceptor : public ACachingTrafficInterceptor<APredisTrafficInterceptor,
                                                                   PredisPropertyGenerator,
                                                                   &predisPropertyGenerator,
                                                                   PredisHandleInfo>
                                , public AMethodInvocationSelector
{
        friend class ACachingTrafficInterceptor<APredisTrafficInterceptor,
                                                PredisPropertyGenerator,
                                                &predisPropertyGenerator,
                                                PredisHandleInfo>;
    public:
        static const char cacheTypeName[];
        APredisTrafficInterceptor(const char* const className, InterceptorRegistry::ID id)
            : ACachingTrafficInterceptor<APredisTrafficInterceptor,
                                         PredisPropertyGenerator,
                                         &predisPropertyGenerator,
                                         PredisHandleInfo>(className, id)
            , AMethodInvocationSelector(AMethodInvocationSelector::kMethod)
        {
        }
        virtual ~APredisTrafficInterceptor() { }
    protected:
        virtual AccessType getAccessType(const PHPExecEnvironment* execEnv) const = 0;
        AccessType getAccessType(const std::string& functionName) const;
};

class PredisCallTrafficInterceptor : public APredisTrafficInterceptor
{
    public:
        PredisCallTrafficInterceptor()
            : APredisTrafficInterceptor("PredisCallTrafficInterceptor", InterceptorRegistry::PredisCallTrafficInterceptor_ID) { }
    protected:
        AccessType getAccessType(const PHPExecEnvironment* execEnv) const;
};

class PredisExecuteCommandTrafficInterceptor : public APredisTrafficInterceptor
{
    public:
        PredisExecuteCommandTrafficInterceptor()
            : APredisTrafficInterceptor("PredisExecuteCommandTrafficInterceptor", InterceptorRegistry::PredisExecuteCommandTrafficInterceptor_ID) { }
    protected:
        AccessType getAccessType(const PHPExecEnvironment* execEnv) const;
};

class PredisBatchTrafficInterceptor : public APredisTrafficInterceptor
{
    public:
        PredisBatchTrafficInterceptor()
            : APredisTrafficInterceptor("PredisBatchTrafficInterceptor", InterceptorRegistry::PredisBatchTrafficInterceptor_ID) { }
    protected:
        AccessType getAccessType(const PHPExecEnvironment* execEnv) const;
};

class PredisCloseInterceptor : public AMethodInterceptor
                             , public AMethodInvocationSelector
{
    public:
        PredisCloseInterceptor()
            : AMethodInterceptor("PredisCloseInterceptor", InterceptorRegistry::PredisCloseInterceptor_ID)
            , AMethodInvocationSelector(AMethodInvocationSelector::kMethod)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);
        virtual void onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);
        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }

        inline bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_CACHE);
        }
};

#endif // Check that PHP version >= 5.3
#endif // __phpredis__exitpoint_h
