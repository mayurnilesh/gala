#include "common.h"
#include "memcache_exitpoint.h"
#include "db/db_exitpoint.h"
#include "caching_exitpoint.h"
#include "boost/lexical_cast.hpp"
#include <sstream>
#include "emalloc_allocator.h"
#include "zend_ini.h"
#include "caching_traffic_interceptor.h"

static const char connectionPropertyName[] = "connection";
static const size_t connectionPropertyNameLen = sizeof(connectionPropertyName) - 1;

namespace memcache {
    const AHandleInfo::TypeTag MemcacheHandleInfo::typeTag;

    MemcachePropertyGenerator memcachePropertyGenerator;

    void MemcacheHandleInfo::addToServerList(const std::string & serverName, long port)
    {
        std::stringstream ss;
        ss << serverName << ":";
        ss << boost::lexical_cast<std::string>(port);
        m_serverList.push_back(ss.str());
        ss.str("");
        ss.clear();

        m_resolved = false;
    }

    bool MemcachePropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                                  const CurrentExitCall* exitCall,
                                                  StringMap& properties) const
    {
        BOOST_ASSERT(exitCall);
        if(!exitCall)
        {
            return false;
        }
        const HandleBasedCurrentExitCall* handleExitCall = static_cast<const HandleBasedCurrentExitCall*>(exitCall);
        boost::shared_ptr<MemcacheHandleInfo> handleInfo(
            boost::static_pointer_cast<MemcacheHandleInfo>(handleExitCall->getHandleInfo())
        );
        if (!handleInfo)
            return false;
        std::vector<std::string> servers(handleInfo->getServerList());

        if (handleInfo->getServerList().size() < 1)
            return false;

        std::sort(servers.begin(), servers.end());

        std::stringstream ss;
        copy(servers.begin(), servers.end(), std::ostream_iterator<std::string>(ss, "\n"));
        std::string serverPoolString(ss.str().substr(0, ss.str().size()-1));

        properties[enum2str(CachingExitPoint_SERVER_POOL)] = serverPoolString;
        properties[enum2str(CachingExitPoint_VENDOR)] = "MEMCACHED";

        return true;
    };

    void* AServerConnectionInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
    {
        // If this is a function that returns a new instance,
        // then there is no way get the php "this" pointer.
        // We'll handle the kInitProcedure case in onCallableEnd.
        if (m_invocationType == kInitProcedure)
            return NULL;

        ZValPointerAny thisZVal(getThisZVal(execEnv));
        if (!thisZVal)
            return NULL;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());

        if (!handle)
            return NULL;

        // If the connection property of the memcache object has not been filled in yet,
        // then any handle info associated with the memcache object is stale and
        // should be ignored.  If the memcache object does not have any handle info
        // associated with it the call to unregisterHandle will be a NOP.
        //
        // We can have stale handle information due to the php engine re-using
        // object numbers.  The handle registry uses object numbers as part of its
        // keys for looking up handle info associated with php objects.
        //
        // In an ideal world the Memcache class would have a
        // constructor we could intercept and ensure that any stale handle info
        // associated with the memcache object is cleared there.
        ZValPointerAny pool(handle.findPropertyByName<ZValPointerAny>(execEnv,
                                                                      connectionPropertyName,
                                                                      connectionPropertyNameLen));
        if (!pool)
            execEnv->getHandleRegistry().unregisterHandle(handle);
        return NULL;
    }

    void AServerConnectionInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void*)
    {
        // If addServer, connect, or pconnect failed, then the add server
        // call failed.  In that case we don't want to accumate the specified
        // server host and port into any handle information associated with
        // this memcache object so we bail out of here.
        if (execEnv->isReturnValueNullOrFalse())
            return;

        int paramCount = getParamCount(execEnv);
        if (paramCount < 1)
            return;

        ZValPointerAny thisZVal(getThisZVal(execEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());

        // Make sure our "this" value is really any object.  In practice
        // our "this" value should always be an object if we got past the
        // isReturnValueNullOrFalse check above.
        if (!handle)
            return;


        boost::shared_ptr<MemcacheHandleInfo> handleInfo;
        // If this function is an init procedure it is known
        // to return a new Memcache instance, which should not
        // be associated with any existing handle info.
        //
        // Since php will re-use object numbers ( which are used in keys in
        // the handle registry ), it is important that we don't look for
        // existing handle information when this function is an init procedure.
        if (m_invocationType != kInitProcedure) {
            handleInfo =
                execEnv->getHandleRegistry().getHandleInfo<MemcacheHandleInfo>(handle);
        }

        if (!handleInfo) {
            handleInfo = MemcacheHandleInfo::create(appdynamics::pb::Agent::EXIT_CACHE);
            execEnv->getHandleRegistry().registerHandle(handle, handleInfo);
        }

        ZValPointerAny hostZValBeforeCast(getNthParameter(execEnv, 0));

        // We can't safely coerce objects to strings, so we'll bail
        // if the hostname is specified as an object.
        if (hostZValBeforeCast.getType() == ZValPointer::ZVal_Object)
            return;


        ZValPointerString hostZVal(hostZValBeforeCast.convertToStringAsCopy(execEnv));
        BOOST_ASSERT(hostZVal);
        if(!hostZVal)
        {
            return;
        }
        std::string host(hostZVal.getStringValue());

        long port = INI_INT("memcache.default_port");

        if (paramCount > 1) {
            ZValPointerAny portZValBeforeCast(getNthParameter(execEnv, 1));
            long convertedPort = portZValBeforeCast.convertToLong();
            if (convertedPort > 0)
                port = convertedPort;
        }

        handleInfo->addToServerList(host, port);
    }

    void* ACloseInterceptor::onCallableBegin(const PHPExecEnvironment*)
    {
        return NULL;
    }

    void ACloseInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                          void* state)
    {
        ZValPointerAny thisZVal(getThisZVal(phpExecEnv));
        if (!thisZVal)
            return;

        ZValPointerObject handle(thisZVal.cast<ZValPointerObject>());
        if (!handle)
            return;

        phpExecEnv->getHandleRegistry().unregisterHandle(handle);
    }

    const char AMemcacheTrafficInterceptor::cacheTypeName[] = "Memcache";
}
