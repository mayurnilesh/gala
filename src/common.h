#ifndef __COMMON_H
#define __COMMON_H

#include <php.h>
#include <Zend/zend.h>
#include <Zend/zend_API.h>

#include "agent_logger.h"
#include "always_inline.h"
#include <string>
#include <map>
#include <boost/assert.hpp>
#include <boost/interprocess/smart_ptr/unique_ptr.hpp>
#include <boost/thread/detail/memory.hpp>

#define BOOST_CHRONO_HEADER_ONLY

#if !defined(uint64)
typedef unsigned long long uint64;
#endif
#if !defined(uint32)
typedef unsigned int uint32;
#endif
#if !defined(uint8)
typedef unsigned char uint8;
#endif

#if !defined(EXPECTED)
# if (defined (__GNUC__) && __GNUC__ > 2 ) && !defined(__INTEL_COMPILER) && !defined(DARWIN) && !defined(__hpux) && !defined(_AIX)
#  define EXPECTED(condition)   __builtin_expect(condition, 1)
#  define UNEXPECTED(condition) __builtin_expect(condition, 0)
# else
#  define EXPECTED(condition)   (condition)
#  define UNEXPECTED(condition) (condition)
# endif
#endif

#if defined(__APPLE__)

#define MAP_ANONYMOUS MAP_ANON

#ifndef __THROW
#define __THROW
#endif

#endif

typedef std::map<std::string, std::string> StringMap;

// Some C++ standard libraries do not have unique_ptr. We define our own
// to avoid lots of conditionals.
template <class T>
struct unique_ptr {
    typedef boost::interprocess::unique_ptr<T, boost::default_delete<T>> type;
};

#if PHP_VERSION_ID < 50300
inline zend_uint Z_ADDREF_P(zval *pz)
{
    return ++pz->refcount;
}

inline zend_uint Z_SET_REFCOUNT_P(zval* pz, zend_uint rc)
{
    return pz->refcount = rc;
}

inline zend_bool Z_UNSET_ISREF_P(zval* pz)
{
    return pz->is_ref = 0;
}

#ifdef HAVE_SIGSETJMP
#   define SETJMP(a) sigsetjmp(a, 0)
#   define LONGJMP(a,b) siglongjmp(a, b)
#   define JMP_BUF sigjmp_buf
#else
#   define SETJMP(a) setjmp(a)
#   define LONGJMP(a,b) longjmp(a, b)
#   define JMP_BUF jmp_buf
#endif

#if PHP_VERSION_ID >= 70100
const zval new_zval;
const zend_fcall_info empty_fcall_info = { 0, new_zval, NULL, NULL, NULL, 0, 0 };
#elif PHP_VERSION_ID >= 70000
const zval new_zval;
const zend_fcall_info empty_fcall_info = { 0, NULL, new_zval, NULL, NULL, NULL, NULL, 0, 0 };
#else
const zend_fcall_info empty_fcall_info = { 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0 };
#endif

#endif

#endif /* __COMMON_H */
