#include "backend_identifier.h"
#include "agent.h"
#include "exitpoint.h"
#include "transactions.h"

RegisteredBackendIdentifier::RegisteredBackendIdentifier(const appdynamics::pb::RegisteredBackendInfo& backendInfo)
{
    m_backend.CopyFrom(backendInfo.registeredbackend());
    m_displayName = backendInfo.exitcallinfo().displayname();
}

UnRegisteredBackendIdentifier::UnRegisteredBackendIdentifier(const ExitCallInfo& exitCallInfo)
    : m_exitCallInfo(boost::make_shared<ExitCallInfo>(exitCallInfo))
    , m_didGenerateDisplayName(false)
{
    BOOST_ASSERT(exitCallInfo.isValid());
    m_delegate = AG(kernel)->getAgentContext()
                           ->getTransactionMonitor()
                           ->getExitPointDelegate(exitCallInfo.getExitPointType());
}

void RegisteredBackendIdentifier::fillInProtobufIdentifier(appdynamics::pb::BackendIdentifier* identifier) const
{
    identifier->Clear();
    identifier->set_type(appdynamics::pb::BackendIdentifier::REGISTERED);
    identifier->mutable_registeredbackend()->CopyFrom(m_backend);
}

void RegisteredBackendIdentifier::fillInDisplayName(std::string* displayName) const
{
    *displayName = m_displayName;
}

void UnRegisteredBackendIdentifier::fillInProtobufIdentifier(appdynamics::pb::BackendIdentifier* identifier) const
{
    identifier->Clear();
    identifier->set_type(appdynamics::pb::BackendIdentifier::UNREGISTERED);
    appdynamics::pb::UnRegisteredBackend* backend = identifier->mutable_unregisteredbackend();
    appdynamics::pb::ExitCallInfo* protoExitCallInfo = backend->mutable_exitcallinfo();
    protoExitCallInfo->set_exitpointtype(m_exitCallInfo->getExitPointType());
    protoExitCallInfo->set_displayname(m_exitCallInfo->getDisplayName());
    // TODO maybe check if m_exitCallInfo already has a display name?
    fillInDisplayName(backend->mutable_exitcallinfo()->mutable_displayname());
    auto identifyingProperties = m_exitCallInfo->getProperties();
    auto protoIdentifyingProperties = protoExitCallInfo->mutable_identifyingproperties();

    BOOST_FOREACH(auto& nv, identifyingProperties) {
        appdynamics::pb::Common::NameValuePair* backEndProperty = protoIdentifyingProperties->Add();
        backEndProperty->set_name(nv.first);
        backEndProperty->set_value(nv.second);
    }
}

void UnRegisteredBackendIdentifier::fillInDisplayName(std::string* displayName) const
{
    if (!m_didGenerateDisplayName) {
        m_displayName = m_delegate->generateDisplayName(*(m_exitCallInfo.get()));
        m_didGenerateDisplayName = true;
    }
    *displayName = m_displayName;
}
