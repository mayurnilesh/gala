/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __data_interceptor_h
#define __data_interceptor_h

#include "intercept.h"

/**
    Interceptor used by HTTP data gatherers to gather session variables before
    their destruction at request end.
    <p>
    This interceptor only does anything on onCallableEnd.
    */
class HttpDataMainScriptInterceptor : public AMethodInterceptor
{
public:
    inline HttpDataMainScriptInterceptor()
        : AMethodInterceptor("HttpDataMainScriptInterceptor",
                             InterceptorRegistry::HttpDataMainScriptInterceptor_ID)
        {
        }

    virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv) { return NULL; }

    virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                void* state);

    virtual bool needParamsInOnMethodBegin() const { return false; }
    virtual bool needReturnValueInOnMethodEnd() const { return false; }
    virtual bool shouldCallOnMethodEnd() const { return true; }

    bool isActive(const PHPExecEnvironment* execEnv) const;
};

#endif
