/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#ifndef __infopoints_h
#define __infopoints_h

#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/utility.hpp>

#include "call_metrics.h"
#include "config_listener.h"
#include "intercept.h"
#include "instrumentation.h"
#include "PHPAgentProtobufs.pb.h"
#include "Instrumentation.pb.h"

namespace InfoPoint
{
    using namespace appdynamics::pb;

    class Metrics : boost::noncopyable
    {
        typedef boost::unordered_map<std::string, boost::shared_ptr<InformationPointMetrics::Custom>> t_CustomMetricsMap;

        public:
            Metrics()
                : m_callMetrics(boost::make_shared<CallMetrics>())
            {
            }

            inline const boost::shared_ptr<CallMetrics>& getCallMetrics() const { return m_callMetrics; }

            inline const t_CustomMetricsMap& getCustomMetrics() const { return m_customMetrics; }

            inline boost::shared_ptr<InformationPointMetrics::Custom> getCustomMetrics(const std::string& name,
                                                                                       Instrumentation::CustomMetricDefinition::Rollup rollup)
            {
                boost::shared_ptr<InformationPointMetrics::Custom> metrics;
                auto it = m_customMetrics.find(name);
                if (it == m_customMetrics.end()) {
                    metrics = boost::make_shared<InformationPointMetrics::Custom>();
                    metrics->set_name(name);
                    metrics->set_rollup(rollup);
                    metrics->set_maxvalue(LONG_MIN);
                    metrics->set_minvalue(LONG_MAX);
                    m_customMetrics[name] = metrics;
                } else {
                    metrics = it->second;
                }
                return metrics;
            }

        private:
            boost::shared_ptr<CallMetrics> m_callMetrics;
            t_CustomMetricsMap m_customMetrics;

    };

    class Interceptor : public AMethodInterceptor
    {
        public:
            Interceptor(const Instrumentation::InformationPoint* infoPoint,
                        uint32_t infoPointID);
            ~Interceptor() { }

            virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
            virtual bool needParamsInOnMethodBegin() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return true; }
            virtual bool shouldCallOnMethodEnd() const { return true; }

            bool isActive(const PHPExecEnvironment* execEnv) const { return m_active; }
            void setActive(bool active) { m_active = active; }

        private:
            const Instrumentation::InformationPoint* const m_infoPoint;
            const uint32_t m_infoPointID;
            bool m_active : 1;

            static AgentLogger m_logger;
    };

    class ConfigManager : IConfigListener, boost::noncopyable
    {
        public:
            ConfigManager(InterceptionEngine* interceptionEngine,
                          const boost::shared_ptr<IConfigChannel>& configChannel);
            ~ConfigManager();

            void activateProbes(bool active);

        private:
            typedef std::pair<boost::shared_ptr<InstrumentationSite>,
                              boost::shared_ptr<Interceptor>> t_infoPointProbe;
            std::vector<t_infoPointProbe> m_probes;
            boost::shared_ptr<appdynamics::pb::InformationPointConfig> m_config;
            InterceptionEngine* const m_interceptionEngine;
            AgentLogger m_logger;

            virtual void configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                       const struct _zend_agent_globals* agentGlobals);
            void configure(const boost::shared_ptr<appdynamics::pb::InformationPointConfig>& config);
            void clearProbes();
    };

}

#endif // __infopoints_h
