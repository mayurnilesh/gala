/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __ZMQTRANSPORT_H
#define __ZMQTRANSPORT_H

//
// ZMQConfigTransport
//
#include <time.h>
#include <stdint.h>

#include <string>
#include <boost/utility.hpp>
#include <boost/filesystem.hpp>
#include <boost/weak_ptr.hpp>
#include "agent_logger.h"
#include "transport.h"
#include "agent.h"

#include "config_listener.h"

namespace boost
{
    namespace filesystem
    {
        class path;
    }
}

namespace appdynamics
{
    namespace pb
    {
        class ASyncMessage;
        class BackendRegistration;
        class BTMetrics;
        class BTMetricsResponse;
        class BTInfoRequest;
        class BTInfoResponse;
        class ConfigRequest;
        class ConfigResponse;
        class Snapshot;
        class StartNodeRequest;
        class StartNodeResponse;
    }
}

namespace google
{
    namespace protobuf
    {
        class Message;
    }
}

namespace zmq
{
    class context_t;
    class socket_t;
    class message_t;
}

class IConfigChannel;
class ZMQControlTransport;

/**
    Base class for all ZMQ transport classes.
 */
class ZMQTransportBase : boost::noncopyable
{
public:
    virtual void reset() = 0;
    static inline int getBaseSocketNumber() { return m_baseSocketNumber; }

protected:
    ZMQTransportBase(zmq::context_t& zmqContext,
                     uint16_t zmqSocketNo,
                     short zmqSocketType,
                     short zmqEventMask,
                     int linger,
                     int64_t hwm,
                     const std::string& transportType,
                     const std::string& loggerName);

    virtual ~ZMQTransportBase();

    // Connection methods
    virtual void connectNow();

    void close();

    // Logger
    inline const AgentLogger& getLogger() const { return m_logger; }

    // Static message-sending methods
    static bool sendMessage(zmq::socket_t* socket, const google::protobuf::Message& message, int flags = 0);
    static bool sendConstantBytes(zmq::socket_t* socket, const char* b, size_t len, int flags);
    static bool sendZeroBytes(zmq::socket_t* socket, int flags);
    static bool sendInt64(zmq::socket_t* socket, int64_t i, int flags);

    bool doZMQPoll(long timeoutInMilliseconds, short events, const std::string& timeoutMessage);

    zmq::context_t& getZMQContext() const { return m_zmqContext; };

    virtual const std::string& getConnectString() = 0;

    template <size_t keepIndex>
    bool getMultiPartSyncResponse(zmq::message_t* response, long timeoutInMilliseconds);
    bool getMultiPartResponse(zmq::message_t* response);

    void resetSocket();

    inline bool haveConnectedSocket() const { return m_connected && m_socket; }

    bool m_connected;
    zmq::socket_t* m_socket;

    const uint16_t m_socketNumber;
    zmq::context_t& m_zmqContext;
    const short m_zmqSocketType;
    const short m_eventMask;
    const int m_linger;
    // Allow at most N messages in the queue. Overflow messages will be dropped.
    const int64_t m_hwm;
    const std::string m_transportType;
    AgentLogger m_logger;
    static int m_baseSocketNumber;
};

/**
    Base class for all ZQM transport classes that are not the ZMQControl transport.
 */
class ZMQAgentTransportBase : public ZMQTransportBase
{

public:
    virtual void reset() = 0;
protected:
    ZMQAgentTransportBase(zmq::context_t& zmqContext,
                          const boost::shared_ptr<ZMQControlTransport>& controlTransport,
                          uint16_t zmqSocketNo,
                          short zmqSocketType,
                          short zmqEventMask,
                          int linger,
                          int64_t hwm,
                          const std::string& transportType,
                          const std::string& loggerName);

    virtual const std::string& getConnectString();

    boost::shared_ptr<ZMQControlTransport> m_controlTransport;

    bool m_didComputeConnectString;
    std::string m_connectString;
};

/**
    Class used for sole purpose of asking proxy to create or retrieve existing path to a tenant-specific
    data directory.
*/
class ZMQControlTransport : public ZMQTransportBase
{
public:
    ZMQControlTransport(zmq::context_t& zmqContext, const char* zmqSocketDir, uint16_t zmqSocketNumber, const std::string& logsDir);
    virtual ~ZMQControlTransport();

    virtual void reset();

    /**
        Sends message to multi-tenant proxy to retrieve data communication directory path.
        @param timeoutInMilliseconds    Time in milliseconds for control transport to wait for synchronous
                                        response from proxy.
        @param dataDirPath              The pointer which, barring failure, is pointed to the data communication
                                        directory path.
        @return                         True iff communication succeeded without timing out and the response
                                        contained a valid data communication directory.
    */
    bool getDataDirectory(long timeoutInMilliseconds,
                          boost::filesystem::path* dataDirPath);

    virtual time_t getRequestTimestamp() const { return m_requestTimestamp; }

    template <class t_TransportClass, typename...t_Args>
    static boost::shared_ptr<t_TransportClass> createTransport(const boost::shared_ptr<ZMQControlTransport>& controlTransport,
                                                               t_Args...args);

protected:
    virtual const std::string& getConnectString();


private:
    static std::string computeConnectString(const char* zmqSocketDir, uint16_t zmqSocketNumber);

    bool sendRequest();
    bool getResponse(appdynamics::pb::StartNodeResponse* response, long timeoutInMilliseconds);

    void fillStartNodeRequest(appdynamics::pb::StartNodeRequest* request,
                              const AgentIdentity& agentIdentity);

    time_t m_requestTimestamp;

    std::string const m_connectString;
    bool m_requestSent;
    bool m_didGetDataDir;
    std::string m_startNodeSocketDir;
    std::string m_logsDir;

    boost::filesystem::path m_dataDirPath;

    std::vector<boost::weak_ptr<ZMQAgentTransportBase> > m_dependentTransports;
};

class ZMQConfigTransport : public IConfigTransport, public ZMQAgentTransportBase
{
public:
    /**
        Constructor.  Do not invoke directly use ZMQControllTransport::createTransport.
     */
    ZMQConfigTransport(zmq::context_t& zmqContext,
                        boost::shared_ptr<ZMQControlTransport> controlTransport,
                        uint16_t zmqSocketNumber);
    virtual ~ZMQConfigTransport();

    virtual void connectNow();
    virtual void reset();

    bool sendRequest(const appdynamics::pb::ASyncRequest& request);
    virtual bool getResponse(appdynamics::pb::ConfigResponse* response, long timeoutInMilliseconds);

    virtual bool isRequestSent() const { return m_request.get(); }
    virtual time_t getRequestTimestamp() const { return m_requestTimestamp; }

private:
    boost::shared_ptr<appdynamics::pb::ASyncRequest> m_request;
    time_t m_requestTimestamp;
    bool m_bound;

};

class ZMQBTInfoTransport : public IBTInfoTransport
                         , public ZMQAgentTransportBase
                         , private IConfigListener
{
public:
    /**
        Constructor.  Do not invoke directly use ZMQControllTransport::createTransport.
     */
    ZMQBTInfoTransport(zmq::context_t& zmqContext,
                       boost::shared_ptr<ZMQControlTransport> controlTransport,
                       uint16_t zmqSocketNumber,
                       boost::shared_ptr<IConfigChannel> configChannel);
    virtual ~ZMQBTInfoTransport();

    virtual bool sendRequest(const appdynamics::pb::BTInfoRequest& request);
    virtual bool getResponse(const appdynamics::pb::BTInfoRequest* request,
                             appdynamics::pb::BTInfoResponse* response,
                             long timeoutInMilliseconds);

    virtual void reset();
private:
    virtual void configChanged(const appdynamics::pb::ConfigResponse&,
                               const struct _zend_agent_globals* agentGlobals);
    boost::shared_ptr<IConfigChannel> m_configChannel;

};


class ZMQReportingTransport : public IReportingTransport, public ZMQAgentTransportBase
{
public:
    /**
        Constructor.  Do not invoke directly use ZMQControllTransport::createTransport.
     */
    ZMQReportingTransport(zmq::context_t& zmqContext,
                          boost::shared_ptr<ZMQControlTransport> controlTransport,
                          uint16_t zmqSocketNumber,
                          int linger);
    virtual ~ZMQReportingTransport();

    virtual void reset();

    bool sendMessage(const appdynamics::pb::ASyncMessage& message);

};


template <class t_TransportClass, typename...t_Args>
boost::shared_ptr<t_TransportClass> ZMQControlTransport::createTransport(const boost::shared_ptr<ZMQControlTransport>& controlTransport,
                                                                         t_Args...args)
{
    boost::shared_ptr<t_TransportClass> transport(boost::make_shared<t_TransportClass>(controlTransport->m_zmqContext,
                                                                                       controlTransport,
                                                                                       args...));
    boost::weak_ptr<ZMQAgentTransportBase> weak(boost::static_pointer_cast<ZMQAgentTransportBase>(transport));
    controlTransport->m_dependentTransports.push_back(weak);
    return transport;
}


#endif /* __ZMQTRANSPORT_H */

// vim: set fdm=marker
