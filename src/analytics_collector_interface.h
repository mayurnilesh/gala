/*
Copyright (c) AppDynamics, Inc., and its affiliates
2016
All Rights Reserved
*/
#ifndef APPD_APP_AGENT_ANALYTICS_INTERFACE_H
#define APPD_APP_AGENT_ANALYTICS_INTERFACE_H

#include <boost/variant.hpp>
#include <list>
#include <map>
#include <string>

namespace Json {
class Value;
};

namespace appd {
namespace agent {

class AnalyticsCollectorInterface
{
public:
  typedef std::map<std::string, std::string> KeyValuePair;
  typedef std::map<std::string, boost::variant<std::string, bool, uint32_t, int32_t, double> >
      UserDataType;

  struct Config
  {
    Config(const std::string& applicationId,
           const std::string& applicationName,
           const std::string& tierId,
           const std::string& tierName,
           const std::string& nodeId,
           const std::string& nodeName)
        : appId_(applicationId),
          appName_(applicationName),
          tierId_(tierId),
          tierName_(tierName),
          nodeId_(nodeId),
          nodeName_(nodeName)
    {
    }

    const std::string appId_;
    const std::string appName_;
    const std::string tierId_;
    const std::string tierName_;
    const std::string nodeId_;
    const std::string nodeName_;
  };

  struct ExitCallInfo
  {
    ExitCallInfo(const std::string& exitCallType,
                 const uint32_t avgResponseTimeMillis,
                 const uint32_t numberOfCalls,
                 const uint32_t numberOfErrors,
                 const bool isSynchronous,
                 const bool isCustomExitCall,
                 const std::string& customExitCallDefinitionId,
                 const std::string& toEntityId,
                 const std::string& toEntityType)
        : exitCallType_(exitCallType),
          avgResponseTimeMillis_(avgResponseTimeMillis),
          numberOfCalls_(numberOfCalls),
          numberOfErrors_(numberOfErrors),
          isSynchronous_(isSynchronous),
          isCustomExitCall_(isCustomExitCall),
          customExitCallDefinitionId_(customExitCallDefinitionId),
          toEntityId_(toEntityId),
          toEntityType_(toEntityType)
    {
    }

    const std::string exitCallType_;
    const uint32_t avgResponseTimeMillis_;
    const uint32_t numberOfCalls_;
    const uint32_t numberOfErrors_;
    const bool isSynchronous_;
    const bool isCustomExitCall_;
    const std::string customExitCallDefinitionId_;
    const std::string toEntityId_;
    const std::string toEntityType_;
  };

  typedef std::list<ExitCallInfo> ExitCallData;

  struct HttpData
  {
    HttpData() = default;
    HttpData(const std::string& url,
             const std::string& principal,
             const std::string& sessionId,
             const std::map<std::string, std::string>& sessionObjects,
             const std::map<std::string, std::string>& uriPathSegments,
             const std::map<std::string, std::string>& cookies,
             const std::map<std::string, std::string>& headers,
             const std::map<std::string, std::string>& parameters)
        : url_(url),
          principal_(principal),
          sessionId_(sessionId),
          sessionObjects_(sessionObjects),
          uriPathSegments_(uriPathSegments),
          cookies_(cookies),
          headers_(headers),
          parameters_(parameters)
    {
    }

    std::string url_;
    std::string principal_;
    std::string sessionId_;
    KeyValuePair sessionObjects_;
    KeyValuePair uriPathSegments_;
    KeyValuePair cookies_;
    KeyValuePair headers_;
    KeyValuePair parameters_;
  };

  struct TransactionContext
  {
    TransactionContext(const std::string& eventTimestamp,
                       const std::string& requestGUID,
                       const uint32_t transactionId,
                       const std::string& transactionName,
                       const std::string& clientRequestGUID,
                       const std::string& requestExperience,
                       const uint32_t transactionTime,
                       const bool isEntryPoint)
        : eventTimestamp_(eventTimestamp),
          requestGUID_(requestGUID),
          transactionId_(transactionId),
          transactionName_(transactionName),
          clientRequestGUID_(clientRequestGUID),
          requestExperience_(requestExperience),
          transactionTime_(transactionTime),
          isEntryPoint_(isEntryPoint)
    {
    }

    const std::string eventTimestamp_;
    const std::string requestGUID_;
    const uint32_t transactionId_;
    const std::string transactionName_;
    const std::string clientRequestGUID_;

    const std::string requestExperience_;
    const uint32_t transactionTime_;
    const bool isEntryPoint_;
  };

  struct TransactionInfo
  {
    TransactionInfo(const TransactionContext& transactionContext,
                    const ExitCallData& exitCalls,
                    const HttpData& httpData,
                    const UserDataType& userData,
                    const Config& config)
        : transactionContext_(transactionContext),
          exitCalls_(exitCalls),
          httpData_(httpData),
          userData_(userData),
          config_(config)
    {
    }

    const TransactionContext& transactionContext_;
    const ExitCallData& exitCalls_;
    const HttpData& httpData_;
    const UserDataType& userData_;
    const Config& config_;
  };

public:
  AnalyticsCollectorInterface() : enabled_(false) {}
  ~AnalyticsCollectorInterface() = default;

  void setEnabled(bool enable);
  bool enabled() const { return enabled_; }
  void reportTransactions(std::string& analyticsPayload);
  void recordTransaction(const TransactionInfo& txn);

  size_t size() const { return buffer_.size(); }
  bool hasData() const { return (size() != 0); }

private:
  void insertKeyValuePairs(Json::Value& root, const std::string& prop, const KeyValuePair& map);
  void insertUserData(Json::Value& root, const std::string& prop, const UserDataType& map);
  void buildTransactionJson(std::string& json, const TransactionInfo& txn);

protected:
  std::list<std::string> buffer_;
  bool enabled_;
};
}  // namespace agent
}  // namespace appd

#endif
