/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#ifndef __method_gatherer_h
#define __method_gatherer_h

#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/operators.hpp>
#include <boost/functional/hash.hpp>

#include "config_listener.h"
#include "instrumentation.h"
#include "intercept.h"

#include "PHPAgentProtobufs.pb.h"
#include "Instrumentation.pb.h"

namespace MethodData
{
    using namespace appdynamics::pb;
    using namespace boost::multi_index;

    class Interceptor;

    static const unsigned MAX_DATA_COLLECTORS = 100;
    static const unsigned MAX_DATA_COLLECTOR_POINTS = 100;
    static const unsigned MAX_COLLECTOR_OUTPUT_LENGTH = 2000;

    typedef std::pair<std::string, boost::shared_ptr<std::vector<ZValPointerAny>>> t_methodData;
    typedef boost::multi_index_container<
        t_methodData,
        indexed_by<
            // First index is hash-like using first member of the pair
            hashed_unique<
                member<
                    t_methodData,
                    std::string,
                    &t_methodData::first
                >
            >,
            // Second index is sequential (list-like)
            sequenced<>
        >
    > t_methodDataContainer;

    class ConfigManager : IConfigListener, boost::noncopyable
    {
        public:
            ConfigManager(InterceptionEngine* interceptionEngine,
                          const boost::shared_ptr<IConfigChannel>& configChannel);
            ~ConfigManager();

            void activateProbes(bool active);
            bool isGathererEnabledForBT(int64_t gathererID) const;

        private:

            struct BTGatherer : public boost::equality_comparable1<BTGatherer>
            {
                BTGatherer(int64_t btId, int64_t gathererId)
                    : m_btId(btId)
                    , m_gathererId(gathererId)
                {
                }

                bool operator==(const BTGatherer& other) const
                {
                    if (m_btId != other.m_btId)
                        return false;
                    if (m_gathererId != other.m_gathererId)
                        return false;
                    return true;
                }

                int64_t m_btId;
                int64_t m_gathererId;
            };

            struct BTGathererHash
            {
                size_t operator()(const BTGatherer& btGatherer) const
                {
                    size_t hash = 0;
                    boost::hash_combine(hash, btGatherer.m_btId);
                    boost::hash_combine(hash, btGatherer.m_gathererId);
                    return hash;
                }
            };

            typedef std::pair<boost::shared_ptr<InstrumentationSite>,
                              boost::shared_ptr<Interceptor>> t_methodDataGathererProbe;
            std::vector<t_methodDataGathererProbe> m_probes;
            boost::unordered_map<int64_t, boost::shared_ptr<appdynamics::pb::DataGatherer>> m_dataGatherers;
            boost::unordered_set<BTGatherer, BTGathererHash> m_btGatherersSet;
            InterceptionEngine* const m_interceptionEngine;
            AgentLogger m_logger;

            virtual void configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                       const struct _zend_agent_globals* agentGlobals);
            void setDataGatherers(const google::protobuf::RepeatedPtrField<appdynamics::pb::DataGatherer>& dataGatherers);
            void setDataGathererBTConfig(const appdynamics::pb::DataGathererBTConfig& dataGathererBTConfig);

            void clearProbes();
    };

	class Interceptor : public AMethodInterceptor
    {
        public:
            Interceptor(const ConfigManager* configManager,
                        const Instrumentation::MethodDataGathererConfig* gathererConfig);
            ~Interceptor() { }

            virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

            virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                        void* state);
            virtual bool needParamsInOnMethodBegin() const { return true; }
            virtual bool needReturnValueInOnMethodEnd() const { return true; }
            virtual bool shouldCallOnMethodEnd() const { return true; }

            bool isActive(const PHPExecEnvironment* execEnv) const { return m_active; }
            void setActive(bool active) { m_active = active; }

        private:
            const ConfigManager* m_configManager;
            const Instrumentation::MethodDataGathererConfig* const m_gathererConfig;
            bool m_active : 1;

            static AgentLogger m_logger;
    };

    class Gatherer : boost::noncopyable
    {
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);

        public:
            static inline boost::shared_ptr<Gatherer> create(const PHPExecEnvironment* execEnv)
            {
                return boost::make_shared<Gatherer>(execEnv);
            }

            void gatherData(const Instrumentation::MethodDataGathererConfig& gathererConfig);

            void fillInGatheredData(google::protobuf::RepeatedPtrField<appdynamics::pb::Common::NameValuePair>* pbContainer);

        private:
            AgentLogger m_logger;
            const PHPExecEnvironment* const m_execEnv;
            bool m_dataCollectorLimitReached;
            t_methodDataContainer m_methodData;

            inline Gatherer(const PHPExecEnvironment* execEnv)
                : m_logger(getLogger(std::string(LogContext::INSTRUMENT) + ".MethodData::Gatherer"))
                , m_execEnv(execEnv)
                , m_dataCollectorLimitReached(false)
            {
            }

            bool extractData(const Instrumentation::MethodDataToCollect& dataToCollect);
            void dataPointsToString(const std::vector<ZValPointerAny>& dataPoints,
                                    std::string* output);
    };
}


#endif // __method_gatherer_h
