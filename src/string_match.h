/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __string_match_h
#define __string_match_h

#include <algorithm>
#include <boost/utility.hpp>
#include "PHPAgentProtobufs.pb.h"
#include "common.h"
#include "agent_logger.h"

class PHPExecEnvironment;

struct real_pcre;
typedef struct real_pcre pcre;
struct pcre_extra;


/*
 * Wraps some common string matching operations.
 * <p>
 * An instance of this class holds a specific match type and pattern to be
 * tested against multiple strings.
 */
class StringMatch : public boost::noncopyable
{
    public:
        StringMatch(const appdynamics::pb::Common::StringMatchCondition& condition)
                      : m_didLogError(false)
                      , m_protobufObj(condition)
        { }

        // Default copy constructor and assignment operator implementations
        // should be sufficient.

        bool matchString(const AgentLogger& logger,
                         const PHPExecEnvironment* phpExecEnv,
                         const std::string& input);

        inline bool isEmptyPattern() const {
            return m_protobufObj.matchstrings().size() == 0;
        }
    private:
        bool m_didLogError;
        appdynamics::pb::Common::StringMatchCondition const m_protobufObj;
};

class MatchResult
{
public:
    enum Type {
        MATCHED,
        UNMATCHED,
        INVALID_REGEX,
        INVALID_REGEX_STATE,
        REGEX_ERROR,
        INVALID_CONDITION,
        EMPTY_CONDITION
    };

    MatchResult(Type type)
        : m_type(type)
        , m_regexErrorCode(-1)
    { }

    MatchResult(Type type, int regexErrorCode)
        : m_type(type)
        , m_regexErrorCode(regexErrorCode)
    { }

    inline operator Type() const { return m_type; }

    inline int getRegexErrorCode() const { return m_regexErrorCode; }

private:
    Type m_type;
    int m_regexErrorCode;
};

MatchResult matchString(const PHPExecEnvironment* phpExecEnv,
                        const appdynamics::pb::Common::StringMatchCondition& matchCondition,
                        const std::string& input);

MatchResult matchRegex(const PHPExecEnvironment* phpExecEnv,
                       const std::string& pattern,
                       const std::string& input,
                       std::vector<std::string>* subPatterns);

#endif // __string_match_h
