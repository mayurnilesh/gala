/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#ifndef __instrumentation_h
#define __instrumentation_h

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/utility.hpp>

#include "agent_logger.h"
#include "intercept.h"
#include "Instrumentation.pb.h"

class InstrumentationSite
{
    template <class T, class Arg1, class... Args>
        friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);

    public:
        static boost::shared_ptr<InstrumentationSite> create(const appdynamics::pb::Instrumentation::InstrumentationProbe_PHP& config, const AgentLogger& logger);

        inline CallableInfo getCallableInfo() const { return m_callableInfo; }

    private:
        InstrumentationSite(CallableInfo callableInfo)
            : m_callableInfo(callableInfo)
        {
        }

        CallableInfo m_callableInfo;
};

class InstrumentationUtil
{
    public:
        static bool matchCallableEnvironment(const appdynamics::pb::Instrumentation::MethodMatch& methodMatch,
                                             const PHPExecEnvironment* execEnv);

        static double getMetricFromCallable(const appdynamics::pb::Instrumentation::CustomMetricDefinition& metricDef,
                                            const PHPExecEnvironment* execEnv);
};

#endif // __instrumentation_h
