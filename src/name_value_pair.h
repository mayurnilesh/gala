/*
    Copyright 2013 AppDynamics
    All rights reserved.
*/

#ifndef __name_value_pair_h
#define __name_value_pair_h

#include <string>
#include <vector>
#include <map>


/**
    Value class for holding a name value pair.  This class should eventually
    be replaced by the protobuf class with the same name ( its in the appdynamics::pb
    namespace ).
 */
class NameValuePair
{
    public:
        NameValuePair(const std::string& name, const std::string& value) : m_name(name), m_value(value) { }

        const std::string& getName() const { return m_name; }
        const std::string& getValue() const { return m_value; }

        static void copyMapToVector(std::vector<NameValuePair>* v,
                                    const std::map<std::string, std::string>& nameValueMap);

    private:
        std::string m_name;
        std::string m_value;
};

#endif
