/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#ifndef __queue_exitpoint_h
#define __queue_exitpoint_h


#include "main/php_version.h"
#include <boost/utility.hpp>
#include "enumfactory.h"
#include "exitpoint.h"

struct _zend_agent_globals;

#define QUEUE_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL(XX) \
    XX(QueueExitPoint_HOST,"HOST",) \
    XX(QueueExitPoint_PORT,"PORT",) \
    XX(QueueExitPoint_EXCHANGE,"EXCHANGE",) \
    XX(QueueExitPoint_ROUTING_KEY,"ROUTING KEY",) \

DECLARE_ENUM(QueueExitPointIdentifyingPropertyNames, QUEUE_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

class InterceptionEngine;

#if PHP_VERSION_ID >= 50300
namespace amqp {
    // Connection interceptors.
    class ConnectionConnectInterceptor;
    class ConnectionDisconnectInterceptor;

    // Channel interceptors.
    class ChannelConstructorInterceptor;

    // Exchange interceptors.
    class ExchangeConstructorInterceptor;
    class ExchangeConfigInterceptor;
    class ExchangeWriteInterceptor;
}
#endif

/**
    Exit point delegate that sets up the interceptors to
    detect messaging backends.
 */
class QueueExitPointDelegate : public AExitPointDelegate
{
    public:
        QueueExitPointDelegate(InterceptionEngine* interceptEngine,
                               TransactionMonitor* txMonitor);

        appdynamics::pb::Agent::ExitPointType getExitPointType() const
        {
            return appdynamics::pb::Agent::EXIT_RABBITMQ;
        }

        void configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                       const struct _zend_agent_globals* agentGlobals);

    protected:
        void applyRules(const _zend_agent_globals* agentGlobals);

        bool processMatchRule(const PHPExecEnvironment* execEnv,
                              const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                              const StringMap& properties) const;

        void processNamingRule(const PHPExecEnvironment* execEnv,
                               const appdynamics::pb::Agent::BackendRule& backendRule,
                               const StringMap& properties,
                               StringMap& identifyingProperties) const;

        inline const char* getDefaultDisplayNameDelimiter() const { return "/"; }

    private:
        static bool s_didApplyRules;
};

#endif // __queue_exitpoint_h
