/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#ifndef __amqp__exitpoint_h
#define __amqp__exitpoint_h

#include "main/php_version.h"

#if PHP_VERSION_ID >= 50300

#include "common.h"
#include "correlation.h"
#include "exitcall_detail.h"
#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"

namespace amqp {
    /**
    * Handle info classes for AMQP PECL extension.. Stores all relevant
    * information to identify an AMQP backend.
    * Dependencies: Connection <= Channel <= Exchange
    */
    template <class t_Derived, class t_Base>
    class QueueHandleInfo : public t_Base {
    public:
        template <class... t_Args>
        inline static boost::shared_ptr<t_Derived> create(t_Args &&...args)
        { return boost::make_shared<t_Derived>(args...); }
        inline static boost::shared_ptr<t_Derived> create()
        { return boost::make_shared<t_Derived>(); }

    protected:
        template <class... t_Args>
        inline QueueHandleInfo(t_Args&&...args)
        : t_Base(args...) {}
    };

    /**
     * Handle info for AMQPConnection class.
     * Has a serial number -- needed to determine whether connection details
     * are stale for an Exchange.
     */
    class ConnectionHandleInfo : public QueueHandleInfo<ConnectionHandleInfo,
                                                        AHandleInfo>
    {
        template <class T>
        friend typename boost::detail::sp_if_not_array<T>::type
            boost::make_shared();
    public:
        // Making the typeTag public to work around a g++ bug.
        static const AHandleInfo::TypeTag typeTag;

        inline std::string getHost() { return m_host; };
        inline void setHost(std::string host) { m_host = host; };
        inline std::string getPort() { return m_port; };
        inline void setPort(std::string port) { m_port = port; };
        inline long getSerial() { return m_serial; };
        inline void incrementSerial() { ++m_serial; };
    protected:
        inline ConnectionHandleInfo()
            : QueueHandleInfo<ConnectionHandleInfo, AHandleInfo>(&typeTag)
            , m_serial(0)
        { }
        std::string m_host;
        std::string m_port;
        long m_serial;
    };

    /**
     * Mix-in class for AMQP objects that have parent components
     * (e.g. new AMQPChannel($amqpConnection))
     */
    template <class t_Parent>
    class DownstreamQueueHandleInfo {
    public:
        inline void setParent(boost::shared_ptr<t_Parent> parent)
        { m_parent = parent; };
        inline boost::shared_ptr<t_Parent> getParent() const
        { return m_parent; };
    protected:
        boost::shared_ptr<t_Parent> m_parent;
    };

    class ExchangeHandleInfo
        : public QueueHandleInfo<ExchangeHandleInfo, AExitPointHandleInfo>
        , public DownstreamQueueHandleInfo<ConnectionHandleInfo>
    {
        template <class T, class Arg1, class... Args>
        friend typename boost::detail::sp_if_not_array<T>::type
            boost::make_shared(Arg1 &&, Args &&...);
    public:
        // Making the typeTag public to work around a g++ bug.
        static const AHandleInfo::TypeTag typeTag;

        inline std::string getName() { return m_name; };
        inline void setName(std::string name) { m_name = name; };
        bool isValidExchange() const;

        inline bool isResolved() const { return m_resolved; }
        inline void clearResolved() { m_resolved = false; }
        inline void setResolved() { m_resolved = true; }

        inline void initializedHeaders() { m_initializedHeaders = true; }
        inline void deletedHeaders() { m_initializedHeaders = false; }
        inline bool headersWereInitialized() { return m_initializedHeaders; }

        inline void modifyStack() { m_stackModified = true; }
        inline void stackRestored() { m_stackModified = false; }
        inline bool stackWasModified() { return m_stackModified; }

        inline void setStackState(
                unique_ptr<PHPExecEnvironment::ArgStackState>::type state)
        { m_stackState.swap(state); }
        inline void clearStackState() { m_stackState.reset(); }

    protected:
        inline ExchangeHandleInfo(long serial)
            : QueueHandleInfo<ExchangeHandleInfo, AExitPointHandleInfo>(
                appdynamics::pb::Agent::EXIT_RABBITMQ,
                &ExchangeHandleInfo::typeTag)
            , m_resolved(false)
            , m_serial(serial)
            , m_initializedHeaders(false)
            , m_stackModified(false)
        { }

        std::string m_name;

        bool m_resolved;
        const long m_serial;
        bool m_initializedHeaders;
        bool m_stackModified;
        unique_ptr<PHPExecEnvironment::ArgStackState>::type m_stackState;
    };


    /****************
     * Interceptors *
     ****************/

    /** Constructor interceptors **/


    class AConstructorInterceptor : public AMethodInterceptor
    {
    public:
        AConstructorInterceptor(const char* const interceptorName,
                                InterceptorRegistry::ID id)
            : AMethodInterceptor(interceptorName, id) { }

        inline bool needParamsInOnMethodBegin() const { return true; }
        inline bool shouldCallOnMethodEnd() const { return true; }
        inline bool needReturnValueInOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_RABBITMQ);
        }
    };

    class ChannelConstructorInterceptor : public AConstructorInterceptor
    {
    public:
        ChannelConstructorInterceptor(): AConstructorInterceptor(
                "amqp::ChannelConstructorInterceptor",
                InterceptorRegistry::AMQPChannelConstructorInterceptor_ID)
        { }

        void* onCallableBegin(const PHPExecEnvironment* execEnv);
        void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);
    };

    class ExchangeConstructorInterceptor : public AConstructorInterceptor
    {
    public:
        ExchangeConstructorInterceptor() : AConstructorInterceptor(
                "amqp::ExchangeConstructorInterceptor",
                InterceptorRegistry::AMQPExchangeConstructorInterceptor_ID)
        { }

        void* onCallableBegin(const PHPExecEnvironment* execEnv);
        void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);
    };

    /** Connection Interceptors **/
    class AConnectInterceptor : public AMethodInterceptor
    {
    public:
        AConnectInterceptor(const char* const interceptorName,
                            InterceptorRegistry::ID id)
            : AMethodInterceptor(interceptorName, id) {}

        virtual bool reportErrorIfNeeded(const PHPExecEnvironment* execEnv);

        void* onCallableBegin(const PHPExecEnvironment* execEnv) = 0;
        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state) = 0;

        inline bool needParamsInOnMethodBegin() const { return true; }
        inline bool shouldCallOnMethodEnd() const { return true; }
        inline bool needReturnValueInOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_RABBITMQ);
        }
    };

    class ConnectionConnectInterceptor : public AConnectInterceptor
    {
    public:
        ConnectionConnectInterceptor() : AConnectInterceptor(
                "amqp::ConnectionConnectInterceptor",
                InterceptorRegistry::AMQPConnectionConnectInterceptor_ID)

        { }
        void* onCallableBegin(const PHPExecEnvironment* execEnv);
        void  onCallableEnd(const PHPExecEnvironment* execEnv,
                            void* state);
    };

    class ConnectionDisconnectInterceptor : public AConnectInterceptor
    {
    public:
        ConnectionDisconnectInterceptor() : AConnectInterceptor(
                "amqp::ConnectionDisconnectInterceptor",
                InterceptorRegistry::AMQPConnectionDisconnectInterceptor_ID)

        { }
        void* onCallableBegin(const PHPExecEnvironment* execEnv);
        void  onCallableEnd(const PHPExecEnvironment* execEnv,
                            void* state);
    };

    /** Exchange interceptors **/
    class ExchangeWriteInterceptor :
        public CorrelationEmitter<AHandleBasedExitPointInterceptor<
                                    ExchangeWriteInterceptor,
                                    ExchangeHandleInfo>,
                                  ExchangeWriteInterceptor,
                                  appdynamics::pb::Agent::EXIT_RABBITMQ>
    {
        friend class CorrelationEmitter<
            AHandleBasedExitPointInterceptor<
                ExchangeWriteInterceptor,
                ExchangeHandleInfo>,
            ExchangeWriteInterceptor,
            appdynamics::pb::Agent::EXIT_RABBITMQ>;
        friend class ExitCallDetailHelper<
                                          AHandleBasedExitPointInterceptor<
                                              ExchangeWriteInterceptor,
                                              ExchangeHandleInfo>,
                                          ExchangeWriteInterceptor,
                                          appdynamics::pb::Agent::EXIT_RABBITMQ>;
    public:
        ExchangeWriteInterceptor()
            : CorrelationEmitter<
                                 AHandleBasedExitPointInterceptor<ExchangeWriteInterceptor, ExchangeHandleInfo>,
                                 ExchangeWriteInterceptor,
                                 appdynamics::pb::Agent::EXIT_RABBITMQ>("amqp::ExchangeWriteInterceptor",
                                                                 InterceptorRegistry::AMQPExchangeWriteInterceptor_ID,
                                                                 appdynamics::pb::Agent::EXIT_RABBITMQ)
            , m_clearCorrelationHeader(false)
        {
        }
        virtual ~ExchangeWriteInterceptor() { }

        void  onCallableEnd(const PHPExecEnvironment* execEnv,
                            void* state);

        mutable bool m_clearCorrelationHeader;
    protected:
        ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                      const PHPExecEnvironment* execEnv) const;
        bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const;
        void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                       const PHPExecEnvironment* execEnv,
                                       const boost::shared_ptr<SnapshotExitCall>& sec) const;
        void emitCorrelationInfo(TransactionContext* context,
                                 CurrentExitCall* exitCall,
                                 const PHPExecEnvironment* execEnv) const;

    };

    class ExchangeConfigInterceptor : public AMethodInterceptor
    {
    public:
        ExchangeConfigInterceptor() : AMethodInterceptor(
                "amqp::ExchangeConfigInterceptor",
                InterceptorRegistry::AMQPExchangeConfigInterceptor_ID)
        { }
        void* onCallableBegin(const PHPExecEnvironment* execEnv);
        void  onCallableEnd(const PHPExecEnvironment* execEnv,
                            void* state);

        inline bool needParamsInOnMethodBegin() const { return true; }
        inline bool shouldCallOnMethodEnd() const { return true; }
        inline bool needReturnValueInOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_RABBITMQ);
        }
    };

}

#endif // Check that PHP version >= 5.3
#endif // __amqp__exitpoint_h
