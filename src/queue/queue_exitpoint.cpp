
#include "main/php_version.h"
#include "queue_exitpoint.h"
#include "intercept.h"
#include "amqp_exitpoint.h"
#include <boost/assign/list_inserter.hpp>

DEFINE_ENUM(QueueExitPointIdentifyingPropertyNames, QUEUE_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

#if PHP_VERSION_ID >= 50300
namespace amqp {
    static const char CONNECTION_SYMBOL[] = "AMQPConnection";
    static const char CHANNEL_SYMBOL[] = "AMQPChannel";
    static const char EXCHANGE_SYMBOL[] = "AMQPExchange";

    /** Connection **/
    static const char CONNECTION_CONNECT_METHOD[] = "connect";
    static const char CONNECTION_DISCONNECT_METHOD[] = "disconnect";

    /** Exchange **/
    static const char EXCHANGE_CONFIG_METHOD[] = "setName";
    static const char EXCHANGE_WRITE_METHOD[] = "publish";
}

amqp::ConnectionConnectInterceptor connectionConnectInterceptor;
amqp::ConnectionDisconnectInterceptor connectionDisconnectInterceptor;

amqp::ChannelConstructorInterceptor channelConstructorInterceptor;

amqp::ExchangeConstructorInterceptor exchangeConstructorInterceptor;
amqp::ExchangeConfigInterceptor exchangeConfigInterceptor;
amqp::ExchangeWriteInterceptor exchangeWriteInterceptor;
#endif

bool QueueExitPointDelegate::s_didApplyRules = false;

QueueExitPointDelegate::QueueExitPointDelegate(InterceptionEngine* interceptEngine,
                                               TransactionMonitor* txMonitor)
    : AExitPointDelegate(interceptEngine, txMonitor)
{
    boost::assign::push_back(m_displayNameSchema)
        (CONFIG_RULE_PROP_NAME,                             " - ")
        (enum2str(QueueExitPoint_HOST),        (const char*) NULL)
        (enum2str(QueueExitPoint_PORT),                       ":")
        (enum2str(QueueExitPoint_ROUTING_KEY), (const char*) NULL)
        (enum2str(QueueExitPoint_EXCHANGE),    (const char*) NULL);
}

void QueueExitPointDelegate::configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                                       const struct _zend_agent_globals* agentGlobals)
{
    boost::shared_ptr<t_BackendRulesList> rabbitmqBackendRules(boost::make_shared<t_BackendRulesList>());
    rabbitmqBackendRules->CopyFrom(backendConfig.rabbitmqbackendconfig());
    AExitPointDelegate::configure(rabbitmqBackendRules, agentGlobals);
}

void QueueExitPointDelegate::applyRules(const _zend_agent_globals* agentGlobals)
{
    if (s_didApplyRules)
        return;

    s_didApplyRules = true;
#if PHP_VERSION_ID >= 50300
    /** Constructor interceptors **/
    m_interceptEngine->addInterceptorForCallable(CallableInfo(amqp::CHANNEL_SYMBOL, PHP_CONSTRUCT_METHOD), &channelConstructorInterceptor);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(amqp::EXCHANGE_SYMBOL, PHP_CONSTRUCT_METHOD), &exchangeConstructorInterceptor);

    /** Connection interceptors **/
    m_interceptEngine->addInterceptorForCallable(CallableInfo(amqp::CONNECTION_SYMBOL, amqp::CONNECTION_CONNECT_METHOD), &connectionConnectInterceptor);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(amqp::CONNECTION_SYMBOL, amqp::CONNECTION_DISCONNECT_METHOD), &connectionDisconnectInterceptor);

    /** Exchange interceptors **/
    m_interceptEngine->addInterceptorForCallable(CallableInfo(amqp::EXCHANGE_SYMBOL, amqp::EXCHANGE_CONFIG_METHOD), &exchangeConfigInterceptor);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(amqp::EXCHANGE_SYMBOL, amqp::EXCHANGE_WRITE_METHOD), &exchangeWriteInterceptor);
#endif
}

bool QueueExitPointDelegate::processMatchRule(const PHPExecEnvironment* execEnv,
                                              const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                                              const StringMap& properties) const
{
    BOOST_ASSERT(matchRule.has_rabbitmqmatchrule());

    const appdynamics::pb::Agent::BackendMatchRule_Rabbitmq& rabbitmqMatchRule =
        matchRule.rabbitmqmatchrule();
    auto notFound = properties.end();

    if (rabbitmqMatchRule.has_host()) {
        auto it = properties.find(enum2str(QueueExitPoint_HOST));
        if (it == notFound)
            return false;
        StringMatch condition(rabbitmqMatchRule.host());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (rabbitmqMatchRule.has_port()) {
        auto it = properties.find(enum2str(QueueExitPoint_PORT));
        if (it == notFound)
            return false;
        StringMatch condition(rabbitmqMatchRule.port());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (rabbitmqMatchRule.has_routingkey()) {
        auto it = properties.find(enum2str(QueueExitPoint_ROUTING_KEY));
        if (it == notFound)
            return false;
        StringMatch condition(rabbitmqMatchRule.routingkey());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (rabbitmqMatchRule.has_exchange()) {
        auto it = properties.find(enum2str(QueueExitPoint_EXCHANGE));
        if (it == notFound)
            return false;
        StringMatch condition(rabbitmqMatchRule.exchange());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    return true;
}

void QueueExitPointDelegate::processNamingRule(const PHPExecEnvironment* execEnv,
                                               const appdynamics::pb::Agent::BackendRule& backendRule,
                                               const StringMap& properties,
                                               StringMap& identifyingProperties) const
{
    BOOST_ASSERT(backendRule.namingrule().has_rabbitmqnamingrule());
    const appdynamics::pb::Agent::BackendNamingRule_Rabbitmq& rabbitmqNamingRule =
        backendRule.namingrule().rabbitmqnamingrule();
    Expression::Context context(execEnv, &properties);

    if (rabbitmqNamingRule.has_host()) {
        evaluateProperty(enum2str(QueueExitPoint_HOST),
                         rabbitmqNamingRule.host(),
                         context,
                         identifyingProperties);
    }

    if (rabbitmqNamingRule.has_port()) {
        evaluateProperty(enum2str(QueueExitPoint_PORT),
                         rabbitmqNamingRule.port(),
                         context,
                         identifyingProperties);
    }

    if (rabbitmqNamingRule.has_routingkey()) {
        evaluateProperty(enum2str(QueueExitPoint_ROUTING_KEY),
                         rabbitmqNamingRule.routingkey(),
                         context,
                         identifyingProperties);
    }

    if (rabbitmqNamingRule.has_exchange()) {
        evaluateProperty(enum2str(QueueExitPoint_EXCHANGE),
                         rabbitmqNamingRule.exchange(),
                         context,
                         identifyingProperties);
    }
}
