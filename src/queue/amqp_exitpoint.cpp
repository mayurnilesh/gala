#include "main/php_version.h"

#include "queue_exitpoint.h"
#include "amqp_exitpoint.h"
#if PHP_VERSION_ID >= 50300
#include "common.h"
#include "boost/lexical_cast.hpp"
#include <sstream>
#include "emalloc_allocator.h"
#include "zend_ini.h"
#include "intercept.h"

static const std::string RABBITMQ_DEFAULT_PORT = "5672";

static const char HOST_ATTR_NAME[] = "host";
static const char PORT_ATTR_NAME[] = "port";
static const char GET_HOST_FUNCTION[] = "getHost";
static const char GET_PORT_FUNCTION[] = "getPort";
static const char EXCHANGE_PUBLISH_HEADERS_ARRAY[] = "headers";
static const char AMQP_NOPARAM_NAME[] = "AMQP_NOPARAM";

const AHandleInfo::TypeTag amqp::ExchangeHandleInfo::typeTag;
const AHandleInfo::TypeTag amqp::ConnectionHandleInfo::typeTag;

class AMQPPropertyGenerator
{
    public:
        bool getProperties(const PHPExecEnvironment* execEnv,
                           const CurrentExitCall* exitCall,
                           const boost::shared_ptr<amqp::ExchangeHandleInfo>& handleInfo,
                           StringMap& properties) const;
};
AMQPPropertyGenerator amqpPropertyGenerator;

bool AMQPPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                          const CurrentExitCall* exitCall,
                                          const boost::shared_ptr<amqp::ExchangeHandleInfo>& handleInfo,
                                          StringMap& properties) const
{
    if (!handleInfo)
        return false;

    if (execEnv->getParamCount() < 2)
        return false;

    // Run up the chain to get all the handle infos needed.
    boost::shared_ptr<amqp::ConnectionHandleInfo> connection
        = handleInfo->getParent();

    std::string host = connection->getHost();
    std::string port = connection->getPort();
    if (port.size() == 0)
        port = RABBITMQ_DEFAULT_PORT;
    std::string exchange = handleInfo->getName();

    ZValPointerString routingKeyZval(
            execEnv->getParam(1).cast<ZValPointerString>());
    std::string routingKey = routingKeyZval.getStringValue();

    properties[enum2str(QueueExitPoint_HOST)]        = host;
    properties[enum2str(QueueExitPoint_PORT)]        = port;
    properties[enum2str(QueueExitPoint_EXCHANGE)]    = exchange;
    properties[enum2str(QueueExitPoint_ROUTING_KEY)] = routingKey;

    return true;
};

bool amqp::AConnectInterceptor::reportErrorIfNeeded(const PHPExecEnvironment* execEnv)
{
    zval* exceptionObject = execEnv->getExceptionObject();
    if (exceptionObject) {
        ErrorMonitor* errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->reportException(execEnv, exceptionObject);
        return true;
    }
    return false;
}

void* amqp::ConnectionConnectInterceptor::onCallableBegin(
        const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void amqp::ConnectionConnectInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    if (reportErrorIfNeeded(execEnv))
        return;

    zval* invokedObject = execEnv->getInvokedObject();
    if (!invokedObject)
        return;

    ZValPointerAny returnValue;

    // Call PHP function getHost on AMQPConnection class.
    std::string host;

    ZValPointerArray getHostCallable(ZValPointerArray::create());
    getHostCallable.pushEntry(ZValPointerAny::share(invokedObject));
    getHostCallable.pushEntry(ZValPointerString::copy(GET_HOST_FUNCTION));

    if(!execEnv->callCallable(&returnValue, getHostCallable, NULL))
        return;

    if ((host =
            returnValue.cast<ZValPointerString>().getStringValue()).size() == 0)
        return;

    // Call PHP function getPort on AMQPConnection class.
    long port;

    ZValPointerArray getPortCallable(ZValPointerArray::create());
    getPortCallable.pushEntry(ZValPointerAny::share(invokedObject));
    getPortCallable.pushEntry(ZValPointerString::copy(GET_PORT_FUNCTION));

    if(!execEnv->callCallable(&returnValue, getPortCallable, NULL))
        return;

    port = returnValue.cast<ZValPointerLong>().getLongValue();

    // Assign default port value if no port found in AMQPConnection object.
    if (port <= 0)
        port = INI_INT("amqp.port");

    std::string portString(boost::lexical_cast<std::string>(port));

    boost::shared_ptr<ConnectionHandleInfo> handleInfo(
            execEnv->getHandleRegistry().getHandleInfo<ConnectionHandleInfo>(
                ZValPointerAny::share(invokedObject)
                )
            );

    if (handleInfo) {
        handleInfo->incrementSerial();
    }
    else {
        handleInfo = amqp::ConnectionHandleInfo::create();
    }

    handleInfo->setHost(host);
    handleInfo->setPort(portString);

    execEnv->getHandleRegistry().registerHandle(
            ZValPointerAny::share(invokedObject),
            handleInfo);
}

void* amqp::ConnectionDisconnectInterceptor::onCallableBegin(
        const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void amqp::ConnectionDisconnectInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    if (reportErrorIfNeeded(execEnv))
        return;

    ZValPointerObject invokedObject(
            ZValPointerAny::share(execEnv->getInvokedObject())
            .cast<ZValPointerObject>());
    if (!invokedObject)
        return;

    boost::shared_ptr<ConnectionHandleInfo> handleInfo(
            execEnv->getHandleRegistry().getHandleInfo<ConnectionHandleInfo>(
                invokedObject));

    // In this case, disconnect() was called before a connection was ever
    // established. Return.
    if (!handleInfo)
        return;

    handleInfo->incrementSerial();
}

void* amqp::ChannelConstructorInterceptor::onCallableBegin(
        const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void amqp::ChannelConstructorInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    ZValPointerObject invokedObject(
            ZValPointerAny::share(execEnv->getInvokedObject())
            .cast<ZValPointerObject>());
    if (!invokedObject)
        return;

    // Channel requires exactly one argument. That argument is the
    // AMQPConnection object. Bail if it's not there.
    if (execEnv->getParamCount() == 0)
        return;

    ZValPointerObject connectionArgument(
            execEnv->getParam<ZValPointerObject>(0));
    boost::shared_ptr<ConnectionHandleInfo> connection(
            execEnv->getHandleRegistry().getHandleInfo<ConnectionHandleInfo>(
                connectionArgument));

    // If Channel is being constructed with invalid or null Connection,
    // do not report metrics or register a handleInfo.
    if (!connection)
        return;

    execEnv->getHandleRegistry().registerHandle(invokedObject, connection);
}

void* amqp::ExchangeConstructorInterceptor::onCallableBegin(
        const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void amqp::ExchangeConstructorInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    ZValPointerObject invokedObject(
            ZValPointerAny::share(execEnv->getInvokedObject())
            .cast<ZValPointerObject>());
    if (!invokedObject)
        return;

    // Exchange requires exactly one argument. That argument is the
    // AMQPChannel object. Bail if it's not there.
    if (execEnv->getParamCount() == 0)
        return;
    ZValPointerObject channelArgument(
            execEnv->getParam(0).cast<ZValPointerObject>());

    boost::shared_ptr<ConnectionHandleInfo> connection(
            execEnv->getHandleRegistry()
            .getHandleInfo<ConnectionHandleInfo>(channelArgument));

    // If channel argument is not in handle registry, bail.
    if (!connection)
        return;

    boost::shared_ptr<ExchangeHandleInfo> exchangeHandleInfo(
            amqp::ExchangeHandleInfo::create(connection->getSerial())
            );
    exchangeHandleInfo->setParent(connection);
    execEnv->getHandleRegistry().registerHandle(invokedObject,
                                                exchangeHandleInfo);
}

void amqp::ExchangeWriteInterceptor::emitCorrelationInfo(
        TransactionContext* context,
        CurrentExitCall* exitCall,
        const PHPExecEnvironment* execEnv) const
{
    boost::shared_ptr<ExchangeHandleInfo> exchange(
            execEnv->getHandleRegistry().getHandleInfo<ExchangeHandleInfo>(
                ZValPointerAny::share(execEnv->getInvokedObject()))
            );

    if (!exchange)
        return;

    const int paramCount = execEnv->getParamCount();
    // The required params are not there, bail.
    if (paramCount < 2)
        return;

    ZValPointerArray attributes(ZValPointerArray::create());

    if (paramCount >= 4)
        attributes = execEnv->getParam<ZValPointerArray>(3);


    // Invariant: $attributes should always exist, because
    // if it didn't already, it was created above in case 3.
    BOOST_ASSERT(attributes);
    ZValPointerArray headers(attributes.findEntryByName<ZValPointerArray>(
                EXCHANGE_PUBLISH_HEADERS_ARRAY,
                strlen(EXCHANGE_PUBLISH_HEADERS_ARRAY)));

    if (!headers) {
        headers = ZValPointerArray::create();
        exchange->initializedHeaders();
    }

    std::string corrHeaderString(
            TransactionCorrelator::getCorrelationHeader(
                context,
                exitCall,
                m_correlationLogger)
            );

    ZValPointerString corrHeaderZVal(ZValPointerString::copy(corrHeaderString.c_str()));

    if (!headers.putEntryByName(
                const_cast<char*>(CorrelationHeader::MAIN_HEADER),
                CorrelationHeader::MAIN_HEADER_SIZE,
                ZValPointerAny(corrHeaderZVal)))
        return;

    if (!attributes.putEntryByName(EXCHANGE_PUBLISH_HEADERS_ARRAY,
                                   strlen(EXCHANGE_PUBLISH_HEADERS_ARRAY),
                                   ZValPointerAny(headers)))
        return;

    exitCall->addSnapshotDebugProperty("Correlation Header", corrHeaderString);

    std::vector<ZValPointerAny> newArgs;

    switch (paramCount) {
        case 2: // Optional third parameter $flags was not set, set default.
        {
            long constantVal = 0;
            execEnv->getConstantAsLong(AMQP_NOPARAM_NAME,
                                       strlen(AMQP_NOPARAM_NAME),
                                       &constantVal);
            ZValPointerAny flagZval(ZValPointerLong::create(constantVal));
            newArgs.push_back(flagZval);
        }
        case 3: // Optional fourth parameter $attributes not set, create.
        {
            newArgs.push_back(attributes);
            exchange->setStackState(
                    execEnv->pushArgs(newArgs)
                    );
            exchange->modifyStack();
        }
    }
    m_clearCorrelationHeader = true;
}

void amqp::ExchangeWriteInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    CurrentExitCall* currentExitCall = reinterpret_cast<CurrentExitCall*>(state);
    if (!currentExitCall)
        return;

    ZValPointerObject invokedObject(
            ZValPointerAny::share(execEnv->getInvokedObject())
            .cast<ZValPointerObject>());
    if (!invokedObject)
        return;

    if (execEnv->getParamCount() < 2)
        return;

    boost::shared_ptr<ExchangeHandleInfo> exchange(
            execEnv->getHandleRegistry()
            .getHandleInfo<ExchangeHandleInfo>(invokedObject));

    if (!exchange)
        return;

    if (!exchange->isValidExchange())
        return;

    if (m_clearCorrelationHeader) {
        // There should be 4 arguments to:
        // AMQPExchange::publish ( string $message
        //                         , string $routing_key
        //                         [, int $flags = AMQP_NOPARAM
        //                         [, array $attributes = array() ]] )
        if (execEnv->getParamCount() < 4)
            return;

        if (exchange->stackWasModified()) {
            BOOST_ASSERT(execEnv->getParamCount() == 4);
            exchange->clearStackState();
        }
        else {
            /** Case where $attributes or $headers already existed **/
            // If attributes or the inner array headers does not exist, return.
            ZValPointerArray attributes(execEnv->getParam<ZValPointerArray>(3));
            if (!attributes)
                return;
            ZValPointerArray headersZVal(
                    attributes.findEntryByName<ZValPointerArray>(
                        EXCHANGE_PUBLISH_HEADERS_ARRAY,
                        strlen(EXCHANGE_PUBLISH_HEADERS_ARRAY))
                    );

            if (!headersZVal)
                return;

            // If the headers array had to be initialized, just delete it all.
            if (exchange->headersWereInitialized()) {
                if (!attributes.deleteEntryByKey(
                            const_cast<char*>(EXCHANGE_PUBLISH_HEADERS_ARRAY),
                            strlen(EXCHANGE_PUBLISH_HEADERS_ARRAY)))
                    return;
                exchange->deletedHeaders();
            }
            // Otherwise, delete only the correlation header from the headers
            // array.
            else{
                if (!headersZVal.deleteEntryByKey(
                            const_cast<char*>(CorrelationHeader::MAIN_HEADER),
                            CorrelationHeader::MAIN_HEADER_SIZE))
                    return;
            }
        }
    }

    AExitCallInterceptor::onCallableEnd(execEnv, state);
}

bool amqp::ExchangeWriteInterceptor::getHandle(
        ZValPointerAny* handlePointer,
        const PHPExecEnvironment* execEnv) const
{
    return *handlePointer =
        ZValPointerAny::share(execEnv->getInvokedObject())
        .cast<ZValPointerObject>();
}

bool amqp::ExchangeHandleInfo::isValidExchange() const
{
    boost::shared_ptr<amqp::ConnectionHandleInfo> connection(this->getParent());
    return m_serial == connection->getSerial();
}


ExitCallInfo amqp::ExchangeWriteInterceptor::makeExitCallInfo(
        const CurrentExitCall* exitCall,
        const PHPExecEnvironment* execEnv) const
{
    BOOST_ASSERT(exitCall);
    const HandleBasedCurrentExitCall* handleExitCall
        = static_cast<const HandleBasedCurrentExitCall*>(exitCall);
    boost::shared_ptr<ExchangeHandleInfo> handleInfo
        = boost::static_pointer_cast<ExchangeHandleInfo>(
                handleExitCall->getHandleInfo());

    if (!handleInfo->isValidExchange())
        return ExitCallInfo();

    if (execEnv->getParamCount() < 2)
        return ExitCallInfo();

    if (handleInfo->isResolved())
        return handleInfo->getExitCallInfo();

    StringMap properties;
    if (!amqpPropertyGenerator.getProperties(execEnv, exitCall, handleInfo, properties))
        return ExitCallInfo();

    ExitCallInfo exitCallInfo(
        AG(kernel)->getAgentContext()
                  ->getTransactionMonitor()
                  ->getExitPointDelegate(getExitPointType())
                  ->makeIdentifyingProperties(execEnv, exitCall, properties)
    );

    handleInfo->setExitCallInfo(exitCallInfo);
    handleInfo->setResolved();

    return exitCallInfo;
}

void amqp::ExchangeWriteInterceptor::addSnapshotExitCallDetail(
        const CurrentExitCall* exit_call,
        const PHPExecEnvironment* execEnv,
        const boost::shared_ptr<SnapshotExitCall>& sec) const
{
    // If the write method contains less than two parameters
    // (a minimum of 2: message and routingKey are necessary), bail.
    if (execEnv->getParamCount() < 2)
        return;
    ZValPointerString routingKey(
            execEnv->getParam(1).cast<ZValPointerString>());

    // If the routing key is invalid, bail.
    if (!routingKey)
        return;

    sec->addProperty(enum2str(QueueExitPoint_ROUTING_KEY),
                     routingKey.getStringValue());
    sec->addProperties(exit_call->getSnapshotDebugProperties());
}

void* amqp::ExchangeConfigInterceptor::onCallableBegin(
        const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void  amqp::ExchangeConfigInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    ZValPointerObject invokedObject(
            ZValPointerAny::share(execEnv->getInvokedObject())
            .cast<ZValPointerObject>()
            );

    if (!invokedObject)
        return;

    if (execEnv->getParamCount() < 1)
        return;

    boost::shared_ptr<ExchangeHandleInfo> exchange(
            execEnv->getHandleRegistry().getHandleInfo<ExchangeHandleInfo>(
                invokedObject)
            );

    if (!exchange)
        return;

    ZValPointerString exchangeName(execEnv->getParam<ZValPointerString>(0));
    if (!exchangeName)
        return;

    std::string exchangeStr(exchangeName.getStringValue());
    if (exchangeStr.size() == 0)
        return;

    exchange->setName(exchangeStr);
}
#endif
