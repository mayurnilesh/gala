/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#include "data_interceptor.h"

#include "agent.h"
#include "request_context.h"
#include "transactions.h"

bool HttpDataMainScriptInterceptor::isActive(const PHPExecEnvironment* execEnv) const
{
    // Don't gather HTTP data for CLI programs.
    if (execEnv->isCLISAPI())
        return false;

    return AG(kernel)->getAgentContext()
                     ->getSnapshotManager()
                     ->getHTTPDataGatherer()
                     ->enabled();
}

void HttpDataMainScriptInterceptor::onCallableEnd(
        const PHPExecEnvironment* execEnv,
        void* state)
{
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    txMonitor->getSnapshotManager()
             ->getHTTPDataGatherer()
             ->process(execEnv, txMonitor->getTransactionContext());
}
