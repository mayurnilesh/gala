/*
Copyright (c) AppDynamics, Inc., and its affiliates
2018
All Rights Reserved
*/

#ifndef APPD_APP_AGENT_ANALYTICS_H
#define APPD_APP_AGENT_ANALYTICS_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <time.h>
#include <list>
using boost::asio::ip::tcp;
using boost::property_tree::ptree; using boost::property_tree::read_json; using boost::property_tree::write_json;

class http_client
{
  public:
  http_client(boost::asio::io_service& io_service, const std::string& server, const std::string& path);

private:
  void handle_resolve(const boost::system::error_code& err,
      tcp::resolver::iterator endpoint_iterator);

  void handle_connect(const boost::system::error_code& err);

  void handle_write_request(const boost::system::error_code& err);

  void handle_read_status_line(const boost::system::error_code& err);

  void handle_read_headers(const boost::system::error_code& err);

  std::string recordTransaction();

  bool reportTransaction();

  void handle_read_content(const boost::system::error_code& err);

  time_t lastReported_;
  std::list<std::string> buffer_;
  tcp::resolver resolver_;
  tcp::socket socket_;
  boost::asio::streambuf request_;
  boost::asio::streambuf response_;
};

#endif
