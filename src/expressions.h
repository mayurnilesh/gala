/*
   Copyright 2014 AppDynamics.
   All rights reserved.
*/

#ifndef __expressions_h
#define __expressions_h

#include "zval_helper.h"
#include "Instrumentation.pb.h"
#include <boost/utility.hpp>

namespace Expression
{
    class Context : boost::noncopyable
    {
        public:
            explicit Context(const PHPExecEnvironment* execEnv)
                : m_execEnv(execEnv)
                , m_identifyingProperties(NULL)
            {
            }

            Context(const PHPExecEnvironment* execEnv,
                    const StringMap* identifyingProperties)
                : m_execEnv(execEnv)
                , m_identifyingProperties(identifyingProperties)
            {
            }

            inline const PHPExecEnvironment* getPHPExecEnvironment() const
            {
                return m_execEnv;
            }

            inline const StringMap* getIdentifyingProperties() const
            {
                return m_identifyingProperties;
            }

        private:
            const PHPExecEnvironment* const m_execEnv;
            const StringMap* m_identifyingProperties;
    };

    inline AgentLogger& getLogger()
    {
        static AgentLogger logger = NullLoggerPtr;
        if (logger == NullLoggerPtr) {
            logger = ::getLogger(std::string(LogContext::AGENT) + ".Expression");
        }
        return logger;
    }

    inline double getDoubleValue(const ZValPointerAny& input)
    {
        switch (input.getType()) {
            case ZValPointer::ZVal_Null:
                return 0.0;

            case ZValPointer::ZVal_Long:
                return (double) input.cast<ZValPointerLong>().getLongValue();

            case ZValPointer::ZVal_Double:
                return input.cast<ZValPointerDouble>().getDoubleValue();

            case ZValPointer::ZVal_Bool:
                return input.cast<ZValPointerBool>().getBoolValue() ? 1.0 : 0.0;

            case ZValPointer::ZVal_String:
                return input.cast<ZValPointerString>().getDoubleValue();

            default:
                LOG4CXX_DEBUG(getLogger(), "cannot get double from value of type " << input.getType());
                BOOST_ASSERT_MSG(false, "cannot get double from value unless it is null, long, double, bool, or string");
                return 0.0;
        }
    }

    inline bool getBooleanValue(const ZValPointerAny &input)
    {
        switch (input.getType()) {
            case ZValPointer::ZVal_Bool:
                return input.cast<ZValPointerBool>().getBoolValue();

            case ZValPointer::ZVal_Null:
                return false;

            case ZValPointer::ZVal_Double:
                return input.cast<ZValPointerDouble>().getDoubleValue() != 0.0;

            case ZValPointer::ZVal_Long:
                return input.cast<ZValPointerLong>().getLongValue() != 0;

            case ZValPointer::ZVal_String:
                return !input.cast<ZValPointerString>().getStringValue().empty();

            case ZValPointer::ZVal_Array:
                return input.cast<ZValPointerArray>().size() > 0;

            case ZValPointer::ZVal_Object:
            case ZValPointer::ZVal_Resource:
                // Objects and resources always refer to some underlying opaque
                // structure, which we'll consider non-empty by default.
                return true;

            default:
                BOOST_ASSERT_MSG(false, "cannot get boolean from value unless it is null, bool, double, long, string, array, object, or resource");
                return false;
        }
    }

    using namespace appdynamics::pb;

    ZValPointerAny evaluate(const Instrumentation::ExpressionNode& node,
                            const Context& context);
};

#endif // __expressions_h
