/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include "reporting.h"
#include "transport.h"
#include "transactions.h"
#include "timer.h"
#include "callgraph/call_graph_node.h"
#include "snapshot.h"
#include "snapshot_exitcall.h"
#include "request_context.h"
#include "correlation.h"
#include "call_metrics.h"
#include "infopoints.h"
#include "utf8_sanitize.h"
#include <boost/unordered_map.hpp>

static const long BTINFO_RESP_TIMEOUT_IN_MILLISECONDS = 10;

static inline void convertCallGraphNodeToProtobuf(const CallGraph::Node* node,
                                                  appdynamics::pb::CallElement* pb_node)
{
    pb_node->set_timetaken(node->getExecutionTime());

    const CallGraph::NodeName& name = node->getName();
    const char* const className = name.getClassName();

    if (className)
        pb_node->set_klass(className);

    const char* const methodName = name.getMethodName();

    if (methodName)
        pb_node->set_method(methodName);

    const char* const callerFileName = node->getCallerFilename();
    if (callerFileName)
        pb_node->set_filename(callerFileName);

    unsigned const callerLineNumber = node->getCallerLineNumber();
    pb_node->set_linenumber(callerLineNumber);

    appdynamics::pb::CallElement_Type const callElemenType = node->getCallElementType();
    pb_node->set_type(callElemenType);

    const std::vector< boost::shared_ptr<SnapshotExitCall> >& snapshotExitCalls =
        node->getSnapshotExitCalls();

    BOOST_FOREACH(auto it, snapshotExitCalls) {
        appdynamics::pb::SnapshotExitCall* pb_sec = pb_node->mutable_exitcalls()->Add();
        it->getBackendID()->fillInProtobufIdentifier(pb_sec->mutable_backendidentifier());
        pb_sec->set_detailstring(it->getDetailString());
        pb_sec->set_errordetails(it->getErrorDetails());
        pb_sec->set_timetaken(it->getTimeTakenInMs());
        pb_sec->set_sequenceinfo(it->getSequenceInfo());
        pb_sec->set_count(1);

        BOOST_FOREACH(auto& nv, it->getProperties()) {
            appdynamics::pb::Common::NameValuePair* pb_prop = pb_sec->mutable_properties()->Add();
            pb_prop->set_name(nv->getName());
            pb_prop->set_value(sanitizeUTF8(nv->getValue()));
        }

        const appdynamics::pb::BoundParameters* const boundParams = it->getBoundParams();
        if (boundParams)
            pb_sec->mutable_boundparameters()->CopyFrom(*boundParams);
    }
}

static inline void flattenCallGraphToList(const AgentLogger& logger,
                                          const CallGraph::Node* lastRoot,
                                          google::protobuf::RepeatedPtrField<appdynamics::pb::CallElement>* pb_list,
                                          uint64_t requestElapsedTimeMS)
{
    std::list<const CallGraph::Node*> node_queue;

    // Since the call graph can have multiple "roots"
    // We need to enqueue them all and make them all
    // children of a root node for the whole request.
    const CallGraph::Node* currentRoot = lastRoot;
    size_t rootNodesCount = 0;
    unsigned totalScriptTime = 0;
    while (currentRoot)
    {
        node_queue.push_back(currentRoot);
        totalScriptTime += currentRoot->getExecutionTime();
        currentRoot = currentRoot->getNextSiblingNode();
        ++rootNodesCount;
    }

    appdynamics::pb::CallElement* requestRootNode = pb_list->Add();
    requestRootNode->set_numchildren(rootNodesCount);
    if (totalScriptTime > requestElapsedTimeMS) {
        LOG4CXX_ERROR(logger, "script time > request time: " << totalScriptTime
                                                             << " > "
                                                             << requestElapsedTimeMS);
    }
    uint64_t const requestNodeTime =
        std::max<uint64_t>(requestElapsedTimeMS, totalScriptTime);
    requestRootNode->set_timetaken(requestNodeTime);
    requestRootNode->set_method("{request}");
    requestRootNode->set_type(appdynamics::pb::CallElement_Type_INTERNAL);


    while (node_queue.size() != 0)
    {
        const CallGraph::Node* current_node = node_queue.back();
        node_queue.pop_back();
        size_t calledNodesCount = current_node->getCalledNodesCount();

        const CallGraph::Node* calledNode =
            current_node->getFirstCalledNode();
        while (calledNode != NULL)
        {
            node_queue.push_front(calledNode);
            calledNode = calledNode->getNextSiblingNode();
        }

        appdynamics::pb::CallElement* pb_node = pb_list->Add();
        pb_node->set_numchildren(calledNodesCount);

        convertCallGraphNodeToProtobuf(current_node, pb_node);
    }
}

static inline void fillInErrorProtobuf(const boost::shared_ptr<MessageErrorObject>& error,
                                       const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts,
                                       appdynamics::pb::Error* pbError)
{
    pbError->set_phperrorthreshold(error->getLevel());
    pbError->set_displayname(error->getName());
    pbError->set_errormessage(error->getDetail());
    pbError->set_count(errorCounts.find(error)->second);
}

static inline void addMessageErrorToSnapshot(const boost::shared_ptr<MessageErrorObject>& error,
                                             const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts,
                                             appdynamics::pb::Snapshot* snaphot)
{
    appdynamics::pb::Error* pbError = snaphot->mutable_errorinfo()->mutable_errors()->Add();
    fillInErrorProtobuf(error, errorCounts, pbError);
}

static inline void addMessageErrorToBTErrors(const boost::shared_ptr<MessageErrorObject>& error,
                                             const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts,
                                             appdynamics::pb::BTErrors* btErrors)
{
    appdynamics::pb::Error* pbError = btErrors->mutable_errorinfo()->mutable_errors()->Add();
    fillInErrorProtobuf(error, errorCounts, pbError);
}

static inline void fillInExceptionProtobuf(boost::unordered_map<size_t, unsigned>* stackIDMap,
                                           google::protobuf::RepeatedPtrField<appdynamics::pb::StackTrace>* stackTraces,
                                           const boost::shared_ptr<AExceptionBase>& error,
                                           appdynamics::pb::Exception* exceptionProtobuf)
{
    exceptionProtobuf->set_klass(error->getClass());
    std::string message = (boost::format("\"%1%\" thrown in %2% on line %3%")
                            % error->getMessage() % error->getOriginFilename() % error->getOriginLineno()).str();
    exceptionProtobuf->set_message(message);
    unsigned stackTraceID;
    if (getStackTraceProtobufID(error.get(), stackIDMap, stackTraces, &stackTraceID)) {
        exceptionProtobuf->set_stacktraceid(stackTraceID);
    }
}

static inline void addThrowableErrorToExceptionInfo(boost::unordered_map<size_t, unsigned>* stackIDMap,
                                                    const boost::shared_ptr<ExceptionObject>& error,
                                                    const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts,
                                                    appdynamics::pb::ExceptionInfo* exceptionInfo)
{
    google::protobuf::RepeatedPtrField<appdynamics::pb::StackTrace>* stackTraces =
        exceptionInfo->mutable_stacktraces();

    appdynamics::pb::RootException* newRootException = exceptionInfo->add_exceptions();
    newRootException->set_count(errorCounts.find(error)->second);
    fillInExceptionProtobuf(stackIDMap,
                            stackTraces,
                            error,
                            newRootException->mutable_root());
    boost::shared_ptr<AExceptionBase> currentCause(error->getExceptionCause());
    while (currentCause)
    {
        appdynamics::pb::Exception* currentCauseProtoBuf = newRootException->add_causes();
        fillInExceptionProtobuf(stackIDMap,
                                stackTraces,
                                currentCause,
                                currentCauseProtoBuf);
        currentCause = currentCause->getExceptionCause();
    }
}

static inline void addThrowableErrorToSnapshot(boost::unordered_map<size_t, unsigned>* stackIDMap,
                                               const boost::shared_ptr<ExceptionObject>& error,
                                               const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts,
                                               appdynamics::pb::Snapshot* snapshot)
{
    appdynamics::pb::ExceptionInfo* exceptionInfo = snapshot->mutable_exceptioninfo();
    addThrowableErrorToExceptionInfo(stackIDMap,
                                     error,
                                     errorCounts,
                                     exceptionInfo);
}

static inline void addThrowableErrorToBTErrors(boost::unordered_map<size_t, unsigned>* stackIDMap,
                                               const boost::shared_ptr<ExceptionObject>& error,
                                               const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts,
                                               appdynamics::pb::BTErrors* btErrors)
{
    appdynamics::pb::ExceptionInfo* exceptionInfo = btErrors->mutable_exceptioninfo();
    addThrowableErrorToExceptionInfo(stackIDMap,
                                     error,
                                     errorCounts,
                                     exceptionInfo);
}

TransactionReporter::TransactionReporter(boost::shared_ptr<IReportingTransport> reportingTransport,
                                         boost::shared_ptr<IBTInfoTransport> btInfoTransport)
    : m_reportingTransport(reportingTransport)
    , m_btInfoTransport(btInfoTransport)
    , m_didLastBTInfoResponseTimeOut(false)
    , m_logger(getLogger(std::string(LogContext::REPORT) + ".TransactionReporter"))
    , m_btInfoResponseTimeoutInMS(BTINFO_RESP_TIMEOUT_IN_MILLISECONDS)
{
}

bool TransactionReporter::reportTransaction(const TransactionContext* transactionContext,
                                            boost::shared_ptr<RequestContext>& requestContext,
                                            bool resetTransaction)
{
    appdynamics::pb::BTInfoRequest* infoRequest = new appdynamics::pb::BTInfoRequest();

    // Used for API start transaction case, to cancel RINIT-initiated transaction.
    if (resetTransaction) {
        // Get current request ID and add that to the transactionsToCancel protobuf field.
        infoRequest->add_transactionstocancel(requestContext->getProxyRequestID());

        // Generate new proxy request ID and reset proxy message ID.
        requestContext->setProxyRequestID(AG(kernel)->generateRandomNumber());
        requestContext->resetProxyMessageID();
    }

    infoRequest->set_requestid(requestContext->getProxyRequestID());
    infoRequest->set_messageid(requestContext->nextProxyMessageID());
    transactionContext->getTransaction()->fillInBTIdentifier(infoRequest->mutable_btidentifier());

    const boost::shared_ptr<CorrelationHeader>& corrHeader(transactionContext->getCorrelationHeader());

    if (transactionContext->isContinuingTransaction()) {
        BOOST_ASSERT(!!corrHeader);
        appdynamics::pb::Correlation& correlationInfo = *(infoRequest->mutable_correlation());
        correlationInfo.set_incomingbackendid(corrHeader->getSelfResolutionBackendID());
        correlationInfo.set_incomingsnapshotenabled(corrHeader->isSnapshotEnabled());
        correlationInfo.set_donotselfresolve(corrHeader->getDoNotSelfResolve());
        correlationInfo.set_exitcallsequence(corrHeader->getExitCallSequence());
        BOOST_FOREACH(const auto& link, corrHeader->getComponentLinks()) {
            appdynamics::pb::ComponentLink& componentLink =
                *(correlationInfo.mutable_componentlinks()->Add());
            componentLink.set_fromcomponentid(link.getFromComponentID());
            componentLink.set_tocomponentid(link.getToComponentID());
            componentLink.set_exitpointtype(link.getExitPointType());
        }
    }
    else if (corrHeader && corrHeader->getType() == CorrelationHeader::CROSSAPP) {
        uint64_t const crossAppUnResolvedBackendId =
            corrHeader->getCrossAppUnResolvedBackendID();
        if (crossAppUnResolvedBackendId) {
            infoRequest->set_crossappcorrelationbackendid(crossAppUnResolvedBackendId);
        }

        bool const incomingSnapEnabled = corrHeader->isSnapshotEnabled();
        infoRequest->set_incomingcrossappsnapshotenabled(incomingSnapEnabled);
    }

    requestContext->setBTInfoRequest(infoRequest);
    // clear out any previous response, because we'll be expecting a new one
    requestContext->setBTInfoResponse(NULL);

    return m_btInfoTransport->sendRequest(*infoRequest);
}

bool TransactionReporter::receiveTransactionInfo(const boost::shared_ptr<RequestContext>& requestContext, bool waitAgain)
{
    appdynamics::pb::BTInfoResponse* response = requestContext->getBTInfoResponse();
    if (response)
        return true;
    if (!waitAgain && m_didLastBTInfoResponseTimeOut)
        return false;
    response = new appdynamics::pb::BTInfoResponse;
    bool received = m_btInfoTransport->getResponse(requestContext->getBTInfoRequest(), response, m_btInfoResponseTimeoutInMS);
    if (received) {
        requestContext->setBTInfoResponse(response);
    } else {
        delete response;
        m_didLastBTInfoResponseTimeOut = true;
    }
    return received;
}

static inline bool shouldAttachSnapshot(appdynamics::pb::BTInfoResponse* btInfoResponse,
                                        const TransactionContext* transactionContext,
                                        uint64_t requestElapsedTimeMS,
                                        appdynamics::pb::SnapshotInfo::Trigger* snapshotTrigger)
{
    bool attachSnapshot = false;

    if (btInfoResponse) {
        if (btInfoResponse->issnapshotrequired()) {
            attachSnapshot = true;
            *snapshotTrigger = appdynamics::pb::SnapshotInfo::REQUIRED;
        } else if (btInfoResponse->sendsnapshotiferror()
                && transactionContext->isErrorTransaction()) {
            attachSnapshot = true;
            *snapshotTrigger = appdynamics::pb::SnapshotInfo::ERROR;
        } else if (btInfoResponse->has_currentslowthreshold()
                && btInfoResponse->currentslowthreshold() < requestElapsedTimeMS) {
            attachSnapshot = true;
            *snapshotTrigger = appdynamics::pb::SnapshotInfo::SLOW;
        }
    }

    return attachSnapshot;
}

bool TransactionReporter::reportTransactionDetails(const PHPExecEnvironment* execEnv,
                                                   const TransactionContext* transactionContext,
                                                   uint64_t requestEndTimeStamp,
                                                   const boost::shared_ptr<RequestContext>& requestContext,
                                                   Snapshot* snapshot)
{
    appdynamics::pb::ASyncMessage message;
    message.set_type(appdynamics::pb::ASyncMessage::BTDETAILS);
    appdynamics::pb::BTDetails& details =
        *(message.mutable_btdetails());

    bool const haveSnapshotTriggers = receiveTransactionInfo(requestContext, true);
    appdynamics::pb::BTDetails_BTInfoState btInfoState =
        haveSnapshotTriggers ? appdynamics::pb::BTDetails_BTInfoState_RESPONSE_RECEIVED
                             : appdynamics::pb::BTDetails_BTInfoState_MISSING_RESPONSE;
    details.set_btinfostate(btInfoState);
    if (!haveSnapshotTriggers) {
        LOG4CXX_WARN(m_logger, "did not receive BTInfoResponse, not attaching snapshot");
    }

    details.mutable_btinforequest()->CopyFrom(*(requestContext->getBTInfoRequest()));

    appdynamics::pb::BTMetrics& metrics =
        *(details.mutable_btmetrics());

    uint64_t const requestStartTimeStamp = transactionContext->getStartTime().getTimestamp();
    uint64_t const requestElapsedTimeMS =
        AgentKernel::getTimer()->getElapsedTime(requestStartTimeStamp, requestEndTimeStamp) / 1000;

    metrics.set_iserror(transactionContext->isErrorTransaction());
    metrics.set_timetaken(requestElapsedTimeMS);

    size_t peakMemoryUsageInBytes = execEnv->callWithTSRMContext(zend_memory_peak_usage, 0);
    metrics.set_maxrequestmemorysize(peakMemoryUsageInBytes);

    BOOST_FOREACH(auto it, requestContext->getRegisteredExitCallMetrics())
    {
        appdynamics::pb::BackendMetric& backendMetrics =
            *(metrics.mutable_backendmetrics()->Add());
        boost::shared_ptr<CallMetrics> exitCallMetric(it.second);

        it.first.getBackendID()->fillInProtobufIdentifier(backendMetrics.mutable_backendidentifier());
        backendMetrics.set_category(it.first.getCategory());
        // Convert microseconds to milliseconds, since that's what proxy requires
        backendMetrics.set_timetaken(exitCallMetric->getTotalTime() / 1000);
        backendMetrics.set_numofcalls(exitCallMetric->getNumCalls());
        backendMetrics.set_numoferrors(exitCallMetric->getNumErrors());
        backendMetrics.set_mincalltime(exitCallMetric->getMinCallTime() / 1000);
        backendMetrics.set_maxcalltime(exitCallMetric->getMaxCallTime() / 1000);
    }
    metrics.set_timestamp(transactionContext->getStartTime().getWallTime());

    // Information points
    BOOST_FOREACH(auto it, requestContext->getInfoPointMetrics())
    {
        appdynamics::pb::InformationPointMetrics& pbMetrics =
            *(details.mutable_infopointmetrics()->Add());

        boost::shared_ptr<CallMetrics> callMetrics(it.second->getCallMetrics());

        pbMetrics.set_infopointid(it.first);
        // Convert microseconds to milliseconds, since that's what proxy requires
        pbMetrics.set_timetaken(callMetrics->getTotalTime() / 1000);
        pbMetrics.set_numofcalls(callMetrics->getNumCalls());
        pbMetrics.set_numoferrors(callMetrics->getNumErrors());
        pbMetrics.set_mincalltime(callMetrics->getMinCallTime() / 1000);
        pbMetrics.set_maxcalltime(callMetrics->getMaxCallTime() / 1000);

        BOOST_FOREACH(auto custom, it.second->getCustomMetrics())
        {
            appdynamics::pb::InformationPointMetrics::Custom *pbCustom =
                pbMetrics.mutable_custommetrics()->Add();
            pbCustom->CopyFrom(*(custom.second));
        }
    }

    // Snapshot evaluation

    bool attachSnapshot = false;
    appdynamics::pb::SnapshotInfo::Trigger snapshotTrigger;

    // evaluate continuing/required/slow/error triggers
    const appdynamics::pb::SnapshotInfo::Trigger* const policyTrigger =
        requestContext->evaluateSnapshotPolicy(requestElapsedTimeMS, true, transactionContext);
    if (policyTrigger) {
        snapshotTrigger = *policyTrigger;
        attachSnapshot = true;
    }

    if (attachSnapshot) {
        appdynamics::pb::SnapshotInfo& snapshotInfo =
            *(details.mutable_snapshotinfo());
        snapshotInfo.set_trigger(snapshotTrigger);
        fillInSnapshot(requestContext,
                       transactionContext,
                       requestElapsedTimeMS,
                       snapshot,
                       snapshotInfo.mutable_snapshot());
    } else {
        fillInErrors(transactionContext, details);
    }

    boost::shared_ptr<AgentTransaction> agentBT(transactionContext->getTransaction());

    bool success = m_reportingTransport->sendMessage(message);
    if (success) {
        LOG4CXX_INFO(m_logger, "BT details sent: "
                               << agentBT->getName()
                               << "[" << agentBT->getID()
                               << "], time: "
                               << requestElapsedTimeMS
                               << ", error: "
                               << transactionContext->isErrorTransaction()
                               << ", snapshot: "
                               << attachSnapshot
                               << ", requestID: "
                               << requestContext->getProxyRequestID());
    } else {
        LOG4CXX_WARN(m_logger, "BT details could not be sent: "
                               << agentBT->getName()
                               << "[" << agentBT->getID()
                               << "]");
    }

    // Generate new proxy request ID and reset proxy message ID.
    requestContext->setProxyRequestID(AG(kernel)->generateRandomNumber());
    requestContext->resetProxyMessageID();

    return success;
}


void TransactionReporter::reportSelfReResolution(int64_t backendID)
{
    appdynamics::pb::ASyncMessage message;
    message.set_type(appdynamics::pb::ASyncMessage::SELFRERESOLUTION);
    appdynamics::pb::SelfReResolution* const selfResolutionMessage =
        message.mutable_selfreresolution();
    selfResolutionMessage->set_backendid(backendID);
    bool success = m_reportingTransport->sendMessage(message);
    if (success) {
        LOG4CXX_INFO(m_logger, "SELFRERESOLUTION sent. backendID: "
                               << backendID);
    } else {
        LOG4CXX_WARN(m_logger, "SELFRERESOLUTION could not be sent. backendID: "
                               << backendID);
    }
}

void TransactionReporter::fillInSnapshot(const boost::shared_ptr<RequestContext>& requestContext,
                                         const TransactionContext* txContext,
                                         uint64_t requestElapsedTimeMS,
                                         Snapshot* snapshot,
                                         appdynamics::pb::Snapshot* pbSnapshot)
{
    pbSnapshot->set_timestamp(txContext->getStartTime().getWallTime());
    pbSnapshot->mutable_httprequestdata()->set_url(snapshot->getURL());
    pid_t const pid = getpid();
    pbSnapshot->set_processid(pid);
    pbSnapshot->set_snapshotguid(requestContext->getRequestGUID());

    boost::shared_ptr<CorrelationHeader> const corrHeader(txContext->getCorrelationHeader());
    if (corrHeader && corrHeader->getType() == CorrelationHeader::CROSSAPP) {
        std::string const incomingGUID(corrHeader->getSubHeader(CorrelationHeader::REQUEST_GUID_HEADER));
        if (!incomingGUID.empty()) {
            pbSnapshot->set_upstreamcrossappsnapshotguid(incomingGUID);
        }
    }

    const CallGraph::Graph& graph = snapshot->getCallGraph();
    const CallGraph::Node* const lastCallGraphRoot = graph.getLastRoot();
    flattenCallGraphToList(m_logger,
                           lastCallGraphRoot,
                           pbSnapshot->mutable_callgraph()->mutable_callelements(),
                           requestElapsedTimeMS);

    const ErrorRegistry* const errorRegistry = requestContext->getErrorRegistry();
    const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts =
        errorRegistry->getErrors();

    boost::unordered_map<size_t, unsigned> stackTraceHashCodeToStackTraceID;

    BOOST_FOREACH(auto& error, errorRegistry->getErrorsInReportedOrder())
    {
        switch (error->getType())
        {
        case ERROR_MESSAGE:
            addMessageErrorToSnapshot(boost::static_pointer_cast<MessageErrorObject>(error),
                                      errorCounts,
                                      pbSnapshot);
            break;
        case ERROR_EXCEPTION:
            addThrowableErrorToSnapshot(&stackTraceHashCodeToStackTraceID,
                                        boost::static_pointer_cast<ExceptionObject>(error),
                                        errorCounts,
                                        pbSnapshot);
            break;
        }

    }

    snapshot->getDBCalls().mergeIntoSnapshot(pbSnapshot);
    requestContext->eumContext().onReportSnapshot(pbSnapshot);
    snapshot->mergeHttpDataIntoSnapshot(pbSnapshot);
    txContext->mergeMethodDataIntoSnapshot(pbSnapshot);
}

void TransactionReporter::fillInErrors(const TransactionContext* context,
                                       appdynamics::pb::BTDetails& btDetails)
{
    const ErrorRegistry* const errorRegistry = context->getErrorRegistry();
    const std::vector<boost::shared_ptr<AErrorObject> >& errorsInReportedOrder =
        errorRegistry->getErrorsInReportedOrder();
    if (errorsInReportedOrder.empty())
        return;

    appdynamics::pb::BTErrors* pbBTErrors = btDetails.mutable_errors();

    const boost::unordered_map<boost::shared_ptr<AErrorObject>, unsigned>& errorCounts =
        errorRegistry->getErrors();

    boost::unordered_map<size_t, unsigned> stackTraceHashCodeToStackTraceID;

    BOOST_FOREACH(auto& error, errorsInReportedOrder)
    {
        switch (error->getType())
        {
        case ERROR_MESSAGE:
            addMessageErrorToBTErrors(boost::static_pointer_cast<MessageErrorObject>(error),
                                      errorCounts,
                                      pbBTErrors);
            break;
        case ERROR_EXCEPTION:
            addThrowableErrorToBTErrors(&stackTraceHashCodeToStackTraceID,
                                        boost::static_pointer_cast<ExceptionObject>(error),
                                        errorCounts,
                                        pbBTErrors);
            break;
        }

    }
}
