/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#include <algorithm>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>
#include "exitpoint.h"
#include "transactions.h"
#include "agent.h"
#include "current_exit_call.h"
#include "backend_resolver.h"
#include "snapshot.h"
#include "correlation.h"
#include "utf8_sanitize.h"
#include "util.h"

// The Java agent trims the host names of http/db hosts to 50 characters and URL paths to 100.
// This constant is used to do the same below in AHTTPPropertyGenerator::getProperties.
const size_t MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY = 50;
const size_t MAX_URL_LEN_IN_IDENTIFYING_PROPERTY       = 100;

/**
 * For debugging/logging output.
 */
std::ostream& operator<<(std::ostream& out, const appdynamics::pb::Agent::ExitPointType& type)
{
    out << appdynamics::pb::Agent::ExitPointType_Name(type);
    return out;
}

/**
 * For debugging/logging output.
 */
std::ostream& operator<<(std::ostream& out, const ExitCallInfo& e)
{
    out << "Exit Point Type [" << e.getExitPointType() << "] Properties [ ";
    BOOST_FOREACH(auto& p, e.getProperties()) {
        out << p.first << "=" << p.second << " ";
    }
    out << "] Display name [" << e.getDisplayName() << "]";
    return out;
}

/* {{{ AExitPointDelegate methods */

AExitPointDelegate::AExitPointDelegate(InterceptionEngine* interceptEngine,
                                       TransactionMonitor* txMonitor)
    : m_txMonitor(txMonitor)
    , m_interceptEngine(interceptEngine)
    , m_detectionDisabled(false)
{
    m_logger = getLogger(std::string(LogContext::TX_SERVICE) + ".AExitPointDelegate");
}

void AExitPointDelegate::configure(const boost::shared_ptr<t_BackendRulesList>& backendRules,
                                   const struct _zend_agent_globals* agentGlobals)
{
    m_backendRules = backendRules;
    if (m_backendRules->size() == 0) {
        m_detectionDisabled = true;
    } else {
        m_detectionDisabled = false;
    }

    LOG4CXX_DEBUG(m_logger, (m_detectionDisabled ? "Disabling" : "Enabling")
                            << " instrumentation of backend type "
                            << appdynamics::pb::Agent::ExitPointType_Name(getExitPointType())
                            << " due to config");

    applyRules(agentGlobals);
}

// TODO maybe remove exitCall parameter
ExitCallInfo AExitPointDelegate::makeIdentifyingProperties(const PHPExecEnvironment* execEnv,
                                                           const CurrentExitCall* exitCall,
                                                           const StringMap& properties) const
{
    appdynamics::pb::Agent::BackendRule* matchedRule
        = getMatchingBackendRule(execEnv, properties);

    if (!matchedRule) {
        LOG4CXX_TRACE(m_logger, "No matching "
                             << appdynamics::pb::Agent::ExitPointType_Name(getExitPointType())
                             << " config rule found for properties: "
                             << stringMapToPrintableString(properties));
        return ExitCallInfo();
    }

    LOG4CXX_TRACE(m_logger, "Found matching "
                             << appdynamics::pb::Agent::ExitPointType_Name(getExitPointType())
                             << " config rule \""
                             << matchedRule->name()
                             << "\" for properties: "
                             << stringMapToPrintableString(properties));

    StringMap identifyingProperties;
    processNamingRule(execEnv, *matchedRule, properties, identifyingProperties);
    if (identifyingProperties.empty()) {
        if (matchedRule->has_name()) {
            identifyingProperties[CONFIG_RULE_PROP_NAME] = matchedRule->name();
        } else {
            return ExitCallInfo();
        }
    }

    LOG4CXX_TRACE(m_logger, "Final identifying properties for backend "
                            << appdynamics::pb::Agent::ExitPointType_Name(getExitPointType())
                            << ": "
                            << stringMapToPrintableString(identifyingProperties));

    ExitCallInfo exitCallInfo(getExitPointType(), identifyingProperties);
    exitCallInfo.setMatchedConfigRule(matchedRule);

    return exitCallInfo;
}

ExitCallInfo AExitPointDelegate::makeIdentifyingProperties(const PHPExecEnvironment* execEnv,
                                                           const CurrentExitCall* exitCall,
                                                           const IBackendPropertyGenerator& generator) const
{
    StringMap properties;
    if (generator.getProperties(execEnv, exitCall, properties))
        return makeIdentifyingProperties(execEnv, exitCall, properties);
    else
        return ExitCallInfo();
}

appdynamics::pb::Agent::BackendRule* AExitPointDelegate::getMatchingBackendRule(
    const PHPExecEnvironment* execEnv,
    const StringMap& properties) const
{
    auto end = m_backendRules->end();
    for (auto it = m_backendRules->begin(); it != end; ++it) {
        if (it->priority() == 0  // OOTB discovery
            ||
            processMatchRule(execEnv, it->matchrule(), properties)) {
            return &(*it);
        }
    }

    return NULL;
}

std::string AExitPointDelegate::generateDisplayName(const ExitCallInfo& exitCallInfo)
{
    if (exitCallInfo.isApiCall())
        return exitCallInfo.getDisplayName();

    // TODO can displayName = exitCall.getMatchedConfig().getName(); ever happen?
    std::string displayName;
    computeDisplayName(exitCallInfo.getProperties(),
                       m_displayNameSchema,
                       *exitCallInfo.getMatchedConfigRule(),
                       &displayName);
    return displayName;
}

void AExitPointDelegate::computeDisplayName(const StringMap& properties,
                                            const t_DisplayNameOptionList& displayNameSchema,
                                            const appdynamics::pb::Agent::BackendRule& matchedRule,
                                            std::string* displayName)
{
    std::string name;
    bool first = true;
    auto notFound = properties.end();

    BOOST_FOREACH(const auto& displayOption, displayNameSchema) {
        const std::string* optionValue = NULL;

        if (displayOption.first == CONFIG_RULE_PROP_NAME) {
            if (matchedRule.priority() > 0)
                optionValue = &matchedRule.name();
        } else {
            auto property = properties.find(displayOption.first);
            if (property != notFound)
                optionValue = &property->second;
        }
        if (!optionValue)
            continue;

        if (first) {
            // Append the config rule name and its suffix
            if (optionValue == &matchedRule.name()) {
                name.append(*optionValue);
                if (displayOption.second)
                    name.append(displayOption.second);
            } else {
                if (displayOption.second)
                    name.append(displayOption.second);
                name.append(*optionValue);
            }
        } else {
            if (displayOption.second) {
                name.append(displayOption.second);
            } else {
                name.append(getDefaultDisplayNameDelimiter());
            }
            name.append(*optionValue);
        }
        first = false;
    }

    // last resort, just use config rule name
    if (name.empty())
        name.append(matchedRule.name());

    *displayName = name;
}

void AExitPointDelegate::evaluateProperty(const char* propertyName,
                                          const appdynamics::pb::Instrumentation::ExpressionNode& propertyNode,
                                          const Expression::Context& context,
                                          StringMap& identifyingProperties) const
{
    ZValPointerAny result(evaluate(propertyNode, context));
    if (result.getType() != ZValPointer::ZVal_Null) {
        if (result.getType() != ZValPointer::ZVal_String) {
            result = result.convertToStringAsCopy(context.getPHPExecEnvironment());
        }
        ZValPointerString propertyValue(result.cast<ZValPointerString>());
        if (!propertyValue.isNullOrEmpty())
            identifyingProperties[propertyName] = propertyValue.getStringValue();
    }
}

/* }}} */

/* {{{ AExitCallInterceptor methods */

void* AExitCallInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();

    TransactionContext* txContext = txMonitor->getTransactionContext();

    // txContext can be NULL if the current transaction is
    // "excluded", or if the agent in a REGISTERED state.
    // Removing this check would cause onCallableEnd below to
    // crash since it relies on the fact that the returned state
    // pointer is null if there is no txContext.  We'd also crash
    // in cleanupExitCall
    if (!txContext)
        return NULL;

    // If we have an exit call inside an exit call, then ignore the inner
    // exit call.
    CurrentExitCall* existingExitCall = txContext->getCurrentExitCall();
    if (existingExitCall)
        return NULL;

    ExitCallInfo exitCallInfo;

    uint64_t const nextExitCallSequenceNumber = txContext->getExitCallCounter() + 1;

    CurrentExitCall* currentExitCall =
        createCurrentExitCall(phpExecEnv, nextExitCallSequenceNumber);

    if (!currentExitCall)
        return NULL;

    if (resolveBackendOnCallableBegin()) {
        exitCallInfo = makeExitCallInfo(currentExitCall, phpExecEnv);
        if (!exitCallInfo.isValid()) {
            delete currentExitCall;
            return NULL;
        }
        currentExitCall->setExitCallInfo(exitCallInfo);
        ExitCallRegistry* exitCallRegistry =
            txMonitor->getTransactionRegistry()->getExitCallRegistry();
        exitCallRegistry->resolveExitCall(currentExitCall);
    }

    txContext->setCurrentExitCall(currentExitCall);

    txContext->incrementExitCallCounter();

    setupCorrelation(txContext, currentExitCall, phpExecEnv);

    return currentExitCall;
}

void AExitCallInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                         void* state)
{
    CurrentExitCall* currentExitCall = reinterpret_cast<CurrentExitCall*>(state);
    if (!currentExitCall)
        return;

    currentExitCall->endExitCall();

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* txContext = txMonitor->getTransactionContext();

    // This check is not actually needed, because the currentExitCall should be
    // null if the txContext is null, but this check makes the code easier to understand.
    if (!txContext)
        return;

    BOOST_ASSERT(txContext);


    if (resolveBackendOnCallableEnd()) {
        /*
         * Don't attempt to resolve if we resolved it in onCallableBegin().
         */
        if (!currentExitCall->hasBackendID() && !currentExitCall->getExitCallInfo()) {
            ExitCallInfo exitCallInfo = makeExitCallInfo(currentExitCall, phpExecEnv);
            if (!exitCallInfo.isValid())
            {
                cleanupExitCall(currentExitCall);
                return;
            }

            currentExitCall->setExitCallInfo(exitCallInfo);
            ExitCallRegistry* exitCallRegistry =
                txMonitor->getTransactionRegistry()->getExitCallRegistry();
            exitCallRegistry->resolveExitCall(currentExitCall);
        }
    }

    if (reportMetrics()) {
        boost::shared_ptr<AErrorObject> errorObject(detectErrors(currentExitCall, phpExecEnv));
        txContext->reportExitCall(currentExitCall);
        populateSnapshot(txContext, currentExitCall, errorObject, phpExecEnv);
    }

    cleanupExitCall(currentExitCall);
}

bool AExitCallInterceptor::isExitCallToBeDetected() const
{
    return AG(G)->isExitPointEnabled(getExitPointType());
}

bool AExitCallInterceptor::resolveBackendOnCallableEnd() const
{
    return false;
}

bool AExitCallInterceptor::reportMetrics() const
{
    return true;
}

void AExitCallInterceptor::populateSnapshot(TransactionContext* context,
                                            CurrentExitCall* exitCall,
                                            const boost::shared_ptr<AErrorObject>& errorObject,
                                            const PHPExecEnvironment* execEnv)
{
    // If we don't have a backend id, then
    // there is no way or reason to add this
    // exit call to the snapshot.
    if (!exitCall->hasBackendID())
        return;
    if (!context)
        return;
    if (!context->snapshotEnabled())
        return;
    const boost::shared_ptr<SnapshotManager>& snapshotSvc =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getSnapshotManager();

    uint64_t timeTakenInMS = exitCall->getElapsedTimeInUs() / 1000;

    addExitCallDetailForSnapshot(execEnv,
                                 context,
                                 exitCall,
                                 errorObject,
                                 snapshotSvc,
                                 timeTakenInMS);
}

boost::shared_ptr<SnapshotExitCall>
AExitCallInterceptor::createSnapshotExitCall(TransactionContext* txContext,
                                             const CurrentExitCall* exitCall,
                                             const boost::shared_ptr<AErrorObject>& errorObject,
                                             uint64_t timeTakenInMS) const
{
    if (!exitCall->hasBackendID())
        return boost::shared_ptr<SnapshotExitCall>();

    appdynamics::pb::Agent::ExitPointType type = getExitPointType();

    std::string exitCallSequence(TransactionCorrelator::getExitCallSequenceString(txContext, exitCall->getSequenceNumber()));
    boost::shared_ptr<SnapshotExitCall> sec(
            boost::make_shared<SnapshotExitCall>(exitCall->getBackendID(),
                                                 timeTakenInMS,
                                                 exitCallSequence));

    if (errorObject)
        sec->setErrorDetails(errorObject->getSummary());

    return sec;
}

void AExitCallInterceptor::reportSnapshotExitCall(const boost::shared_ptr<SnapshotExitCall>& sec,
                                                  const PHPExecEnvironment* execEnv) const
{
    execEnv->getAgentGlobals().callGraphCollectionState.addSnapshotExitCall(sec);
}

void AExitCallInterceptor::cleanupExitCall(void* state) const
{
    CurrentExitCall* currentExitCall = reinterpret_cast<CurrentExitCall*>(state);
    if (!currentExitCall)
        return;

    TransactionContext* txContext =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();

    // We could assert that txContext is not null here and remove
    // the null check from the if statement below because currentExitCall
    // should be null if txContext is null.  However, this code is easier
    // to understand if we keep the null check in the if statement below.
    if (txContext && txContext->getCurrentExitCall() == currentExitCall)
        txContext->setCurrentExitCall(NULL);
    delete currentExitCall;
}

void AExitCallInterceptor::setupCorrelation(TransactionContext* context,
                                            CurrentExitCall* exitCall,
                                            const PHPExecEnvironment* phpExecEnv)
{
    // default implementation is to do nothing
}

boost::shared_ptr<AErrorObject>
AExitCallInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                   const PHPExecEnvironment* phpExecEnv)
{
    return detectExceptions(currentExitCall, phpExecEnv);
}

boost::shared_ptr<AErrorObject>
AExitCallInterceptor::detectExceptions(CurrentExitCall* currentExitCall,
                                       const PHPExecEnvironment* phpExecEnv)
{
    zval* exceptionObject = phpExecEnv->getExceptionObject();
    if (!exceptionObject)
        return boost::shared_ptr<AErrorObject>();
    if (currentExitCall)
        currentExitCall->incrementErrorCount(1);
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return errorMonitor->reportException(phpExecEnv, exceptionObject);
}

boost::shared_ptr<AErrorObject>
AExitCallInterceptor::detectLoggedErrorsDuringExitCall(CurrentExitCall* currentExitCall,
                                                       const char* backendName,
                                                       const PHPExecEnvironment* phpExecEnv)
{
    if (!currentExitCall)
        return boost::shared_ptr<AErrorObject>();
    const std::string* errorMessage = currentExitCall->getErrorMessage();
    if (!errorMessage)
        return boost::shared_ptr<AErrorObject>();

    currentExitCall->incrementErrorCount(1);
    ErrorMonitor* errorMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return errorMonitor->reportSyntheticException(phpExecEnv, backendName, *errorMessage);
}

template <class t_ExitPointDelegate>
boost::shared_ptr<t_ExitPointDelegate> AExitCallInterceptor::getExitPointDelegate() const
{
    boost::shared_ptr<AExitPointDelegate> delegate(
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getExitPointDelegate(getExitPointType())
    );
    return boost::static_pointer_cast<t_ExitPointDelegate>(delegate);
}

/* }}} */

ExitCallInfo::ExitCallInfo(const appdynamics::pb::ExitCallInfo& protobuf)
    : m_type(protobuf.exitpointtype())
    , m_displayName(protobuf.displayname())
    , m_isValid(true)
    , m_isApiCall(false)
    , m_matchedConfigRule(NULL)
{
    convertPBNameValuePairsToMap(protobuf.identifyingproperties(), m_properties);
}

/* {{{ ExitCallRegistry methods */

void ExitCallRegistry::resolveExitCall(CurrentExitCall* currentExitCall)
{
    const ExitCallInfo* exitCallInfo = currentExitCall->getExitCallInfo();
    if (!exitCallInfo || !exitCallInfo->isValid())
        return;

    BackendIdentifierPtr const backendID(getBackendIdentifier(*exitCallInfo));
    if (backendID != NULL) {
        currentExitCall->setBackendID(backendID);
        resolveComponentID(currentExitCall);
    }
}

void ExitCallRegistry::resolveComponentID(CurrentExitCall* exitCall)
{
    if (exitCall->hasComponentID())
        return;

    if (exitCall->hasRegisteredBackendID()) {
        boost::shared_ptr<RegisteredBackendIdentifier> const backendID(
            boost::static_pointer_cast<RegisteredBackendIdentifier>(exitCall->getBackendID()));
        const auto& registeredBackend = backendID->getRegisteredBackend();
        if (registeredBackend.has_componentid()) {
            std::string componentIDString;
            componentIDString.reserve(16);
            if (registeredBackend.componentisforeignappid()) {
                componentIDString = "A";
            }
            componentIDString += boost::lexical_cast<std::string>(registeredBackend.componentid());
            exitCall->setComponentID(componentIDString);
        }
        else
            exitCall->setComponentID(generateUnresolvedCallInfo(registeredBackend.backendid()));
    }
}

void ExitCallRegistry::updateBackendInfo(const appdynamics::pb::BackendInfo& backendInformation)
{
    m_backends.clear();
    m_resolvedBackends.clear();
    m_selfReResolvedBackends.clear();
    BOOST_FOREACH(const auto& registeredBackEndInfo, backendInformation.registeredbackends())
    {
        ExitCallInfo exitCallInfo(registeredBackEndInfo.exitcallinfo());
        BackendIdentifierPtr newID(
            boost::make_shared<RegisteredBackendIdentifier>(registeredBackEndInfo));
        const appdynamics::pb::RegisteredBackend& registeredBackend =
            registeredBackEndInfo.registeredbackend();
        m_backends[exitCallInfo] = newID;

        // Don't put backends involved in cross app correlation into the resolved backends
        // map.  The resolved backends map is only used for re-resolution of backends
        // in this application.
        if (registeredBackend.has_componentid() && (!registeredBackend.componentisforeignappid()))
            m_resolvedBackends[registeredBackend.backendid()] = registeredBackend.componentid();
    }

    BOOST_FOREACH(const auto& foreignBackendInfo, backendInformation.foreignregisteredbackends())
    {
        // Don't put backends involved in cross app correlation into the resolved backends
        // map.  The resolved backends map is only used for re-resolution of backends
        // in this application.
        if (foreignBackendInfo.has_componentid() && (!foreignBackendInfo.componentisforeignappid()))
            m_resolvedBackends[foreignBackendInfo.backendid()] = foreignBackendInfo.componentid();
    }
}

std::string ExitCallRegistry::generateUnresolvedCallInfo(int64_t backendID)
{
    auto it = m_unresolvedBackendCache.find(backendID);
    if (it != m_unresolvedBackendCache.end())
        return it->second;

    std::string backendCallInfo(CorrelationHeader::BACKEND_CALL_PREFIX);
    backendCallInfo.append(boost::lexical_cast<std::string>(backendID));
    backendCallInfo.append(CorrelationHeader::BACKEND_CALL_SUFFIX);
    m_unresolvedBackendCache[backendID] = backendCallInfo;

    return backendCallInfo;
}

int64_t ExitCallRegistry::getComponentForForeignBackendID(int64_t backendID) const
{
    auto it = m_resolvedBackends.find(backendID);
    if (it != m_resolvedBackends.end())
        return it->second;
    else
        return -1;
}

void ExitCallRegistry::reResolveBackendToSelf(TransactionReporter* transactionReporter,
                                              int64_t backendID)
{
    BOOST_ASSERT(transactionReporter != NULL);
    BOOST_ASSERT(backendID >= 0);
    if (m_selfReResolvedBackends.insert(backendID).second) {
        // We have not reported a self re-resolution for the specified backendID
        // since the last time we received backend information from the proxy.
        transactionReporter->reportSelfReResolution(backendID);
    }
    else {
        LOG4CXX_TRACE(m_logger, "Backend re-resolution for backend ["
                                << backendID
                                << "] is already pending.");
    }
}

void ExitCallRegistry::onMonitorDisable()
{
    m_backends.clear();
    m_resolvedBackends.clear();
    m_selfReResolvedBackends.clear();
}

void ExitCallRegistry::onMonitorEnable()
{
}

BackendIdentifierPtr ExitCallRegistry::getBackendIdentifier(const ExitCallInfo& info)
{
    auto i = m_backends.find(info);
    if (i == m_backends.end()) {
        // Cap the number of backends in any given request.
        // m_maxBackendsCount should be set to double the number of
        // allowed backends.  We need double the number of backends because all
        // the m_backends contains registered backends that the proxy sent us
        // in the last config response and unregistered backends we found in requests
        // processed since the last config response was received.  All of the registered
        // backends in the map might be stale and about to be removed when we receive the
        // next config response from the proxy.
        if (m_backends.size() >= m_maxBackendsCount)
            return BackendIdentifierPtr();

        BackendIdentifierPtr newID(boost::make_shared<UnRegisteredBackendIdentifier>(info));
        if (info.getProperties().empty())
            LOG4CXX_DEBUG(m_logger, "reported exit call with empty identifying properties"); // TODO << info);
        m_backends[info] = newID;
        return newID;
    }
    return i->second;
}

void ExitCallRegistry::reset()
{
    m_backends.clear();
}

/* }}} */

// vim: set fdm=marker:
