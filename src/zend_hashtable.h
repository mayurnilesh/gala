/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef _zend_hashtable_iterator_h
#define _zend_hashtable_iterator_h

#include <zend.h>
#include <stdint.h>
#include <string>
#include <php_version.h>
#include <zend_compatibility.h>


// Forward declaration
template <class t_ValueTypeAdapter> class ZendHashTable;
class ZValHashTableValueAdapter;

/**
    Template that allows clients to access key and value
    of an entry in a php engine HashTable.

    This is a value class.

    Instances of this class should not outlive the php engine
    HashTable which they point into.

    This is a template because the values in a hash table in
    the php engine are void*'s which are then casted to some other
    type on access.

    @param t_ValueTypeAdapter A class with a static function that returns
    t_ValueTypeAdapter::t_ValueType given the void* of an entry in a
    php engine HashTable.
 */
template <class t_ValueTypeAdapter>
class ZendHashTableEntry
{
    friend class ZendHashTable<t_ValueTypeAdapter>;
public:
    typedef typename t_ValueTypeAdapter::t_ValueType t_ValueType;

    ZendHashTableEntry(const ZendHashTableEntry<t_ValueTypeAdapter>& other);
    ZendHashTableEntry<t_ValueTypeAdapter>& operator=(const ZendHashTableEntry<t_ValueTypeAdapter>&);

    /**
        @return true if the key of the hash table entry is a string, false otherwise.
     */
    bool keyIsString() const;

    /**
        @return true if the key of the hash table entry is a long, false otherwise.
     */
    bool keyIsLong() const;

    /**
        @return if keyIsString returns true, this method will return a copy of the
        entry's string key, otherwise this method will return the empty string.
     */
    std::string getKeyAsString() const;

    /**
        @return if keyIsLong returns true, this method will return the entry's key as
        a ulong, otherwise this method will return 0.
     */
    ulong getKeyAsLong() const;

    /**
        @return The value of the entry.
     */
    t_ValueType getValue() const;

private:
    ZendHashTableEntry(const HashTable* hashTable, HashPosition position);

    const HashTable* m_hashTable;
    HashPosition m_position;
};

/**
    A typesafe wrapper for a php engine HashTable, that provides
    entry access and iteration.

    Instances of this class should not outlive the php engine
    HashTable's they wrap.
 */
template <class t_ValueTypeAdapter = ZValHashTableValueAdapter>
class ZendHashTable
{
public:
    typedef typename t_ValueTypeAdapter::t_ValueType t_ValueType;
    ZendHashTable(const ZendHashTable<t_ValueTypeAdapter>& other);
    ZendHashTable<t_ValueTypeAdapter>& operator=(const ZendHashTable<t_ValueTypeAdapter>& other);

    /**
        Iterator that provides a read-only iteration of the entries of a
        php engine HashTable.  The HashTable should not be modified during the
        lifetime of an instance of this class.

        This is a value class.

        An instance of this class should not outlive the php engine
        HashTable it points into.
     */
    class const_iterator
    {
        friend class ZendHashTable<t_ValueTypeAdapter>;
    public:
        const_iterator(const const_iterator& other);
        const_iterator& operator=(const const_iterator& other);
        const_iterator& operator++();
        const_iterator operator++(int);
        bool operator==(const const_iterator& other);
        bool operator!=(const const_iterator& other);
        ZendHashTableEntry<t_ValueTypeAdapter> operator*() const;

    private:
        enum IsEnd { End };
        const_iterator(const HashTable* hashTable, HashPosition position);
        const_iterator(const HashTable* hashTable, IsEnd);

        void checkForEnd();

        bool m_isEnd;
        const HashTable* m_hashTable;
        HashPosition m_position;
    };

    /**
        Static factory method that wraps the specified php engine HashTable.

        This method does not take ownership of the specified php engine HashTable,
        that is why this method is called "share".

        @param hashTable php engine HashTable to make a type safe wrapper for.
        @return A typesafe wrapper of the specified php engine HashTable.
     */
    static ZendHashTable<t_ValueTypeAdapter> share(HashTable* hashTable);

    /**
        @return A const_iterator to the beginning.
     */
    const_iterator begin() const;

    /**
        @return A const_iterator to the end.
     */
    const_iterator end() const;

    /**
        Looks up an entry by string name.
        @param key Name of the entry to lookup.
        @param keyLen Length in bytes including the terminating null of the name of
        the entry to lookup.
        @param value pointer to a location to write the value of the entry with the
        specified name if such an entry exists.  This parameter may be null.
        @return true if an entry with the specified name exists in the HashTable,
        false otherwise.
     */
    bool find(const char* key, size_t keyLen, t_ValueType* value) const;

    /**
        Looks up an entry by string name.
        @param key Name of the entry to lookup.
        @param value pointer to a location to write the value of the entry with the
        specified name if such an entry exists.  This parameter may be null.
        @return true if an entry with the specified name exists in the HashTable,
        false otherwise.
     */
    bool find(const std::string& key, t_ValueType* value) const;

    /**
        Looks up an entry by index.
        @param key The index to lookup the value for.
        @param value pointer to a location to write the value of the entry with the
        specified index if such an entry exists.  This parameter may be null.
        @return true if an entry with the specified index exists in the HashTable,
        false otherwise.
     */
    bool find(long key, t_ValueType* value) const;

    /**
        @return whether this hashtable is valid (has a non-NULL pointer to Zend HashTable)
     */
    inline bool isValid() const { return m_hashTable != NULL; }

    inline size_t size() const { return zend_hash_num_elements(m_hashTable); }

    inline const HashTable* get() const { return m_hashTable; }

    inline bool isRecursing() const { return m_recursionDepth >= MAX_RECURSION_DEPTH; }
    inline void recurseIn() { ++m_recursionDepth; }
    inline void recurseOut() { --m_recursionDepth; }
private:
    ZendHashTable(HashTable* hashTable);
    HashTable* m_hashTable;
    mutable int m_recursionDepth;
    static const int MAX_RECURSION_DEPTH = 1;
};

template <class t_ValueTypeAdapter>
inline ZendHashTableEntry<t_ValueTypeAdapter>::ZendHashTableEntry(const ZendHashTableEntry<t_ValueTypeAdapter>& other)
    : m_hashTable(other.m_hashTable), m_position(other.m_position)
{
}

template <class t_ValueTypeAdapter>
inline ZendHashTableEntry<t_ValueTypeAdapter>::ZendHashTableEntry(const HashTable* hashTable, HashPosition position)
    : m_hashTable(hashTable), m_position(position)
{
}

template <class t_ValueTypeAdapter>
inline ZendHashTableEntry<t_ValueTypeAdapter>& ZendHashTableEntry<t_ValueTypeAdapter>::operator=(const ZendHashTableEntry<t_ValueTypeAdapter>& other)
{
    m_hashTable = other.m_hashTable;
    m_position = other.m_position;
    return *this;
}

template <class t_ValueTypeAdapter>
bool ZendHashTableEntry<t_ValueTypeAdapter>::keyIsString() const
{
    return zend_hash_get_current_key_type_ex(const_cast<HashTable*>(m_hashTable), const_cast<HashPosition*>(&m_position)) == HASH_KEY_IS_STRING;
}

template <class t_ValueTypeAdapter>
bool ZendHashTableEntry<t_ValueTypeAdapter>::keyIsLong() const
{
    return zend_hash_get_current_key_type_ex(const_cast<HashTable*>(m_hashTable), const_cast<HashPosition*>(&m_position)) == HASH_KEY_IS_LONG;
}

template <class t_ValueTypeAdapter>
std::string ZendHashTableEntry<t_ValueTypeAdapter>::getKeyAsString() const
{
    char* keyString = NULL;
    uint32_t keyStringLen = 0;
    zend_ulong keyLong = 0;
#if PHP_VERSION_ID >= 70000
    zend_string* key = NULL;
    int keyType =
        zend_hash_get_current_key_ex(const_cast<HashTable*>(m_hashTable), &key, &keyLong, const_cast<HashPosition*>(&m_position));
    if (key != NULL) {
        keyString = ZSTR_VAL(key);
        keyStringLen = ZSTR_LEN(key) + 1;
    }
#else
    int keyType =
        zend_hash_get_current_key_ex(const_cast<HashTable*>(m_hashTable), &keyString, &keyStringLen, &keyLong, false, const_cast<HashPosition*>(&m_position));
#endif
    // If the key is not really a string or if it is a zero
    // length string call the default constructor of std::string
    // without looking at the keyString variable ( which could in theory be bogus ).
    if ((keyType != HASH_KEY_IS_STRING ) || (keyStringLen == 0))
        return std::string();
    return std::string(keyString, keyStringLen - 1);
}

template <class t_ValueTypeAdapter>
ulong ZendHashTableEntry<t_ValueTypeAdapter>::getKeyAsLong() const
{
    zend_ulong keyLong = 0;
#if PHP_VERSION_ID >= 70000
    zend_string* key = NULL;
    zend_hash_get_current_key_ex(const_cast<HashTable*>(m_hashTable), &key, &keyLong, const_cast<HashPosition*>(&m_position));
#else
    char* keyString = NULL;
    uint32_t keyStringLen = 0;
    zend_hash_get_current_key_ex(const_cast<HashTable*>(m_hashTable), &keyString, &keyStringLen, &keyLong, true, const_cast<HashPosition*>(&m_position));
#endif
    return keyLong;
}

template <class t_ValueTypeAdapter>
typename ZendHashTableEntry<t_ValueTypeAdapter>::t_ValueType ZendHashTableEntry<t_ValueTypeAdapter>::getValue() const
{
#if PHP_VERSION_ID >= 70000
    zval* foundValue = NULL;
    foundValue = zend_hash_get_current_data_ex(const_cast<HashTable*>(m_hashTable), const_cast<HashPosition*>(&m_position));
    // TODO: not sure if "dereferencing" the reference is the right way to go.
    // may need actual reference?
    while (foundValue && Z_TYPE_P(foundValue) == IS_REFERENCE)
        foundValue = Z_REFVAL_P(foundValue);
    return t_ValueTypeAdapter::toValue(foundValue);
#else
    void* rawValue = NULL;
    zend_hash_get_current_data_ex(const_cast<HashTable*>(m_hashTable), &rawValue, const_cast<HashPosition*>(&m_position));
    return t_ValueTypeAdapter::toValue(rawValue);
#endif
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter>::ZendHashTable(const ZendHashTable<t_ValueTypeAdapter>& other)
    : m_hashTable(other.m_hashTable), m_recursionDepth(other.m_recursionDepth)
{
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter>::ZendHashTable(HashTable* hashTable)
    : m_hashTable(hashTable), m_recursionDepth(0)
{
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter>& ZendHashTable<t_ValueTypeAdapter>::operator=(const ZendHashTable<t_ValueTypeAdapter>& other)
{
    m_hashTable = other.m_hashTable;
    m_recursionDepth = other.m_recursionDepth;
    return *this;
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter> ZendHashTable<t_ValueTypeAdapter>::share(HashTable* hashTable)
{
    return ZendHashTable<t_ValueTypeAdapter>(hashTable);
}

template <class t_ValueTypeAdapter>
inline typename ZendHashTable<t_ValueTypeAdapter>::const_iterator ZendHashTable<t_ValueTypeAdapter>::begin() const
{
    // If we are wrapping a null hash table,
    // then just return the end iterator so
    // iteration loops will immediately terminate.
    //
    // TODO create an exception policy
    if (!m_hashTable)
        return end();
    HashPosition beginPosition;
    zend_hash_internal_pointer_reset_ex(m_hashTable, &beginPosition);
    return ZendHashTable<t_ValueTypeAdapter>::const_iterator(m_hashTable, beginPosition);
}

template <class t_ValueTypeAdapter>
inline typename ZendHashTable<t_ValueTypeAdapter>::const_iterator ZendHashTable<t_ValueTypeAdapter>::end() const
{
    return ZendHashTable<t_ValueTypeAdapter>::const_iterator(m_hashTable, const_iterator::End);
}

template <class t_ValueTypeAdapter>
inline bool ZendHashTable<t_ValueTypeAdapter>::find(const char* key, size_t keyLen, typename ZendHashTable<t_ValueTypeAdapter>::t_ValueType* value) const
{
#if PHP_VERSION_ID >= 70000
    // zend_hash_str_find/update/del/etc. takes strlen *without* terminating null byte
    zval* foundValue = zend_hash_str_find(m_hashTable, const_cast<char*>(key), keyLen - 1);
    if (foundValue == NULL)
        return false;
    // TODO: not sure if "dereferencing" the reference is the right way to go.
    // may need actual reference?
    while (Z_TYPE_P(foundValue) == IS_REFERENCE)
        foundValue = Z_REFVAL_P(foundValue);
    if (value)
        *value = t_ValueTypeAdapter::toValue(foundValue);
#else
    void* rawValue = NULL;
    int lookupResult = zend_hash_find(m_hashTable, const_cast<char*>(key), keyLen, &rawValue);
    if (lookupResult != SUCCESS)
        return false;
    if (value)
        *value = t_ValueTypeAdapter::toValue(rawValue);
#endif
    return true;
}

template <class t_ValueTypeAdapter>
inline bool ZendHashTable<t_ValueTypeAdapter>::find(const std::string& key, typename ZendHashTable<t_ValueTypeAdapter>::t_ValueType* value) const
{
    return find(key.c_str(), key.size() + 1, value);
}

template <class t_ValueTypeAdapter>
inline bool ZendHashTable<t_ValueTypeAdapter>::find(long key, typename ZendHashTable<t_ValueTypeAdapter>::t_ValueType* value) const
{
#if PHP_VERSION_ID >= 70000
    zval* foundValue = zend_hash_index_find(m_hashTable, key);
    if (foundValue == NULL)
        return false;
    // TODO: not sure if "dereferencing" the reference is the right way to go.
    // may need actual reference?
    while (Z_TYPE_P(foundValue) == IS_REFERENCE)
        foundValue = Z_REFVAL_P(foundValue);
    if (value)
        *value = t_ValueTypeAdapter::toValue(foundValue);
#else
    void* rawValue = NULL;
    int lookupResult = zend_hash_index_find(m_hashTable, key, &rawValue);
    if (lookupResult != SUCCESS)
        return false;
    if (value)
        *value = t_ValueTypeAdapter::toValue(rawValue);
#endif
    return true;
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter>::const_iterator::const_iterator(const ZendHashTable<t_ValueTypeAdapter>::const_iterator& other)
    : m_isEnd(other.m_isEnd)
    , m_hashTable(other.m_hashTable)
    , m_position(other.m_position)
{
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter>::const_iterator::const_iterator(const HashTable* hashTable, HashPosition position)
    : m_isEnd(false)
    , m_hashTable(hashTable)
    , m_position(position)
{
    checkForEnd();
}

template <class t_ValueTypeAdapter>
inline ZendHashTable<t_ValueTypeAdapter>::const_iterator::const_iterator(const HashTable* hashTable, ZendHashTable<t_ValueTypeAdapter>::const_iterator::IsEnd)
    : m_isEnd(true)
    , m_hashTable(hashTable)
#if PHP_VERSION_ID >= 70000
    , m_position(0)
#else
    , m_position(NULL)
#endif
{
}

template <class t_ValueTypeAdapter>
inline typename ZendHashTable<t_ValueTypeAdapter>::const_iterator& ZendHashTable<t_ValueTypeAdapter>::const_iterator::operator=(const ZendHashTable<t_ValueTypeAdapter>::const_iterator& other)
{
    m_isEnd = other.m_isEnd;
    m_hashTable = other.m_hashTable;
    m_position = other.m_position;
    return *this;
}

template <class t_ValueTypeAdapter>
inline typename ZendHashTable<t_ValueTypeAdapter>::const_iterator& ZendHashTable<t_ValueTypeAdapter>::const_iterator::operator++()
{
    bool hasNext =
        zend_hash_move_forward_ex(const_cast<HashTable*>(m_hashTable), &m_position);
    checkForEnd();
    return *this;
}

template <class t_ValueTypeAdapter>
inline typename ZendHashTable<t_ValueTypeAdapter>::const_iterator ZendHashTable<t_ValueTypeAdapter>::const_iterator::operator++(int)
{
    ZendHashTable<t_ValueTypeAdapter>::const_iterator result(*this);
    ++(*this);
    return result;
}

template <class t_ValueTypeAdapter>
inline bool ZendHashTable<t_ValueTypeAdapter>::const_iterator::operator==(const ZendHashTable<t_ValueTypeAdapter>::const_iterator& other)
{
    if (m_isEnd != other.m_isEnd)
        return false;
    if (m_hashTable != other.m_hashTable)
        return false;
    if (m_position != other.m_position)
        return false;
    return true;
}

template <class t_ValueTypeAdapter>
inline bool ZendHashTable<t_ValueTypeAdapter>::const_iterator::operator!=(const ZendHashTable<t_ValueTypeAdapter>::const_iterator& other)
{
    return !(this->operator==(other));
}

template <class t_ValueTypeAdapter>
inline ZendHashTableEntry<t_ValueTypeAdapter> ZendHashTable<t_ValueTypeAdapter>::const_iterator::operator*() const
{
    return ZendHashTableEntry<t_ValueTypeAdapter>(m_hashTable, m_position);
}

template <class t_ValueTypeAdapter>
inline void ZendHashTable<t_ValueTypeAdapter>::const_iterator::checkForEnd()
{
    if (zend_hash_get_current_key_type_ex(const_cast<HashTable*>(m_hashTable), &m_position) == HASH_KEY_NON_EXISTANT)
    {
        m_isEnd = true;
#if PHP_VERSION_ID >= 70000
        m_position = 0;
#else
        m_position = NULL;
#endif
    }
}

#endif
