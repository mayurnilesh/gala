#ifndef __UTIL_H
#define __UTIL_H

#include "common.h"

/* {{{ class IMonitorStateListener */

/**
 * Interface for listeners that need to act on monitoring disabled/enabled
 * state. No references are owned by this interface, hence, no virtual
 * destructor.
 */
class IMonitorStateListener {
    public:
        virtual void onMonitorDisable() = 0;
        virtual void onMonitorEnable() = 0;
};
/* }}} */

std::string stringMapToPrintableString(const StringMap& map);
char* agent_sprintf(int min_size, const char* fmt, ...);
const char* agent_get_base_filename(const char *filename);

#endif /* __UTIL_H */
