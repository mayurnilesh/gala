/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __time_tsc_h
#define __time_tsc_h

extern "C" {
#include <php.h>
#include "ext/standard/php_rand.h"

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* {{{ Global macro constants */

#ifdef __FreeBSD__
# if __FreeBSD_version >= 700110
#   include <sys/resource.h>
#   include <sys/cpuset.h>
#   define cpu_set_t cpuset_t
#   define SET_AFFINITY(pid, size, mask) \
           cpuset_setaffinity(CPU_LEVEL_WHICH, CPU_WHICH_TID, -1, size, mask)
#   define GET_AFFINITY(pid, size, mask) \
           cpuset_getaffinity(CPU_LEVEL_WHICH, CPU_WHICH_TID, -1, size, mask)
# else
#   error "This version of FreeBSD does not support cpusets"
# endif /* __FreeBSD_version */
#elif __APPLE__
/*
 * Patch for compiling in Mac OS X Leopard
 */
#    include <mach/mach_init.h>
#    include <mach/thread_policy.h>
#    define cpu_set_t thread_affinity_policy_data_t
#    define CPU_SET(cpu_id, new_mask) \
        (*(new_mask)).affinity_tag = (cpu_id + 1)
#    define CPU_ZERO(new_mask)                 \
        (*(new_mask)).affinity_tag = THREAD_AFFINITY_TAG_NULL
#   define SET_AFFINITY(pid, size, mask)       \
        thread_policy_set(mach_thread_self(), THREAD_AFFINITY_POLICY, mask, \
                          THREAD_AFFINITY_POLICY_COUNT)
#else
/* For sched_getaffinity, sched_setaffinity */
# include <sched.h>
# define SET_AFFINITY(pid, size, mask) sched_setaffinity(0, size, mask)
# define GET_AFFINITY(pid, size, mask) sched_getaffinity(0, size, mask)
#endif /* __FreeBSD__ */

#ifdef linux
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE /* To enable CPU_ZERO and CPU_SET, etc.     */
#endif
#endif

}

#include "common.h"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

/**
    Class to provide microsecond time stamps.
    <p>
    This class currently uses the rdtsc x86 instruction
    which requires us to set the processor affinity of the
    process using this class.  In an ideal works we could
    switch to using the clock_gettime methods on linux, however
    those APIs may not be available in all the versions of linux
    we need to work with.
    <p>
    Currently this class only supports x86 CPU architectures.
 */
class Timer {
    public:
        Timer();
        ~Timer() {
            clear_frequencies();
        }

        /*
         * Initializes timer parameters. Has to be called only once.
         */
        int init();

        /*
         * Callback for beginning of request.
         */
        void onRequestBegin(uint64_t nanoSecondsToSleep);

        /**
         * Callback for end of request.
         */
        void onRequestEnd();

        /*
         * Returns current timestamp, in cpu ticks.
         */
        uint64_t getTimestamp() const;

        /*
         * Returns elapsed time, in microseconds, since the given start
         * timestamp.
         */
        uint64_t getElapsedTime(uint64_t startTime) const;

        /**
            @return elapsed time, in microseconds, between the given
            start and end timestamps.
         */
        uint64_t getElapsedTime(uint64_t startTime, uint64_t endTime) const;

        /* Returns current system time, in milliseconds. */
        static uint64_t getCurrentTime();

    private:
        /* This array is used to store cpu frequencies for all available logical
         * cpus.  For now, we assume the cpu frequencies will not change for power
         * saving or other reasons. If we need to worry about that in the future, we
         * can use a periodical timer to re-calculate this arrary every once in a
         * while (for example, every 1 or 5 seconds). */
        double *cpu_frequencies;

        /* The number of logical CPUs this machine has. */
        uint32 num_cpus;

        /* The saved cpu affinity. */
        cpu_set_t prev_mask;

        /* The cpu id current process is bound to. (default 0) */
        uint32 cur_cpu_id;

        /* Random CPU number generation. */
        boost::random::mt19937 randGen;
        boost::random::uniform_int_distribution<> randCPU;

        /* Prevent copy and assignment */
        Timer(const Timer& t);
        Timer& operator=(const Timer& rhs);

        double calc_cpu_frequency();
        void calc_all_cpu_frequencies();
        void clear_frequencies();
        int bind_to_cpu(uint32 cpu_id);
        int bind_to_rand_cpu();
        int restore_cpu_affinity(cpu_set_t * prev_mask);

        uint64_t stamp() const;
        uint64_t us_from_tsc(uint64_t count) const;
        uint64_t tsc_from_us(uint64_t usecs) const;

        /* Accessors */
        inline uint32 get_num_cpus() {
            return num_cpus;
        }
        inline uint32 get_cur_cpu_id() {
            return cur_cpu_id;
        }
        inline const double* get_cpu_frequencies() {
            return cpu_frequencies;
        }

};

static long get_us_interval(struct timeval *start, struct timeval *end) {
    return (((end->tv_sec - start->tv_sec) * 1000000)
            + (end->tv_usec - start->tv_usec));
}

inline Timer::Timer() {
    /* Initialize cpu_frequencies and cur_cpu_id. */
    cpu_frequencies = NULL;
    cur_cpu_id = 0;

    randGen.seed(getCurrentTime());
}

inline int Timer::init() {
    /* Get the number of available logical CPUs. */
    num_cpus = sysconf(_SC_NPROCESSORS_CONF);

    /* Get the cpu affinity mask. */
#ifndef __APPLE__
    if (GET_AFFINITY(0, sizeof(cpu_set_t), &prev_mask) < 0) {
        perror("getaffinity");
        return FAILURE;
    }
#else
    CPU_ZERO(&(prev_mask));
#endif

    calc_all_cpu_frequencies();

    randCPU.param(boost::random::uniform_int_distribution<>::param_type(0, num_cpus-1));
    return SUCCESS;
}

inline void Timer::onRequestBegin(uint64_t nanoSecondsToSleep) {
    bind_to_rand_cpu();
}

inline void Timer::onRequestEnd()
{
}

/**
 * Get time stamp counter (TSC) value via 'rdtsc' instruction.
 *
 * @return 64 bit unsigned integer
 */
inline uint64_t Timer::stamp() const {
    uint32 __a,__d;
    uint64_t val;
    asm volatile("rdtsc" : "=a" (__a), "=d" (__d));
    (val) = ((uint64_t)__a) | (((uint64_t)__d)<<32);
    return val;
}

/**
 * Convert from TSC counter values to equivalent microseconds.
 *
 * @param uint64 count, TSC count value
 * @return 64 bit unsigned integer
 */
inline uint64_t Timer::us_from_tsc(uint64_t count) const  {
    if (!cpu_frequencies) return 0.0;
    return (uint64_t) (count / cpu_frequencies[cur_cpu_id]);
}

/**
 * Convert microseconds to equivalent TSC counter ticks
 *
 * @param uint64 microseconds
 * @return 64 bit unsigned integer
 */
inline uint64_t Timer::tsc_from_us(uint64_t usecs) const {
    if (!cpu_frequencies) return 0;
    return (uint64_t) (usecs * cpu_frequencies[cur_cpu_id]);
}

inline uint64_t Timer::getCurrentTime()
{
    struct timespec tv;
    clock_gettime(CLOCK_REALTIME, &tv);
    return (uint64_t)tv.tv_sec * 1000L + tv.tv_nsec / 1000000L;
}

/**
 * Bind the current process to a specified CPU. This function is to ensure that
 * the OS won't schedule the process to different processors, which would make
 * values read by rdtsc unreliable.
 *
 * @param uint32 cpu_id, the id of the logical cpu to be bound to.
 * @return int, 0 on success, and -1 on failure.
 */
inline int Timer::bind_to_cpu(uint32 cpu_id) {
    cpu_set_t new_mask;

    CPU_ZERO(&new_mask);
    CPU_SET(cpu_id, &new_mask);

    if (SET_AFFINITY(0, sizeof(cpu_set_t), &new_mask) < 0) {
        perror("setaffinity");
        return -1;
    }

    /* record the cpu_id the process is bound to. */
    cur_cpu_id = cpu_id;

    return 0;
}

/**
 * Bind the process to a random CPU.
 */
inline int Timer::bind_to_rand_cpu()
{
    return bind_to_cpu((int)randCPU(randGen));
}

/**
 * This is a microbenchmark to get cpu frequency the process is running on. The
 * returned value is used to convert TSC counter values to microseconds.
 *
 * @return double
 */
inline double Timer::calc_cpu_frequency() {
    struct timeval start;
    struct timeval end;

    if (gettimeofday(&start, 0)) {
        perror("gettimeofday");
        return 0.0;
    }
    uint64_t tsc_start = stamp();
    /* Sleep for 5 miliseconds. Comparing with gettimeofday's few microseconds
     * execution time, this should be enough. */
    usleep(5000);
    if (gettimeofday(&end, 0)) {
        perror("gettimeofday");
        return 0.0;
    }
    uint64_t tsc_end = stamp();
    return (tsc_end - tsc_start) * 1.0 / (get_us_interval(&start, &end));
}

/**
 * Calculate frequencies for all available cpus.
 */
inline void Timer::calc_all_cpu_frequencies() {
    int id;
    double frequency;

    cpu_frequencies = (double *)malloc(sizeof(double) * num_cpus);
    if (cpu_frequencies == NULL) {
        return;
    }

    /* Iterate over all cpus found on the machine. */
    for (id = 0; id < num_cpus; ++id) {
        /* Only get the previous cpu affinity mask for the first call. */
        if (bind_to_cpu(id)) {
            clear_frequencies();
            return;
        }

        /* Make sure the current process gets scheduled to the target cpu. This
         * might not be necessary though. */
        usleep(0);

        frequency = calc_cpu_frequency();
        if (frequency == 0.0) {
            clear_frequencies();
            return;
        }
        cpu_frequencies[id] = frequency;
    }

    restore_cpu_affinity(&prev_mask);
}

/**
 * Restore cpu affinity mask to a specified value. It returns 0 on success and
 * -1 on failure.
 *
 * @param cpu_set_t * prev_mask, previous cpu affinity mask to be restored to.
 * @return int, 0 on success, and -1 on failure.
 */
inline int Timer::restore_cpu_affinity(cpu_set_t * prev_mask) {
    if (SET_AFFINITY(0, sizeof(cpu_set_t), prev_mask) < 0) {
        perror("restore setaffinity");
        return -1;
    }
    /* default value ofor cur_cpu_id is 0. */
    cur_cpu_id = 0;
    return 0;
}

/**
 * Reclaim the memory allocated for cpu_frequencies and restore affinity.
 */
inline void Timer::clear_frequencies() {
    if (cpu_frequencies) {
        free(cpu_frequencies);
        cpu_frequencies = NULL;
    }
    restore_cpu_affinity(&prev_mask);
}

inline uint64_t Timer::getTimestamp() const
{
    return stamp();
}

inline uint64_t Timer::getElapsedTime(uint64_t startTime) const
{
    return us_from_tsc(stamp() - startTime);
}

inline uint64_t Timer::getElapsedTime(uint64_t startTime, uint64_t endTime) const
{
    return us_from_tsc(endTime - startTime);
}

#endif /* __time_tsc_h */
