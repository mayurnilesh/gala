/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#ifndef __CONFIGS_H
#define __CONFIGS_H

#include <boost/unordered_set.hpp>
#include <boost/utility.hpp>
#include <exception>
#include <map>
#include <string>

#include "common.h"
#include "config_listener.h"
#include "config_channel.h"
#include "PHPAgentProtobufs.pb.h"

class MatchPointConfig;
class ZMQConfigTransport;
class ZMQControlTransport;
struct _zend_agent_globals;

class ConfigChannel : public IConfigChannel, boost::noncopyable {
    public:
        ConfigChannel(const boost::shared_ptr<ZMQControlTransport>& controlTransport);
        ~ConfigChannel();

        void requestUpdate();
        void checkForUpdate(const struct _zend_agent_globals* agentGlobals, long timeOutInMilliseconds);

        // Accessors

        bool isAgentEnabled();
        bool isInitialized();

        void addListener(IConfigListener* listener);

        inline const std::string& getControllerGUID() const { return m_agentIdentity.controllerguid(); }
        inline const std::string& getAccountGUID() const { return m_agentIdentity.accountguid(); }

        inline int64_t getNodeID() const { return m_agentIdentity.nodeid(); }
        inline int64_t getTierID() const { return m_agentIdentity.tierid(); }
        inline int64_t getAppID() const  { return m_agentIdentity.appid(); }
        inline int64_t getTimestampSkew() const { return m_timestampSkew; }
        void setTimestampSkew(int64_t timestampSkew);

        void setRequestInterval(long interval) { config_request_interval = interval; }

    private:
        appdynamics::pb::ConfigResponse::AgentState config_state;
        appdynamics::pb::AgentIdentity m_agentIdentity;
        AgentLogger logger;
        boost::shared_ptr<ZMQConfigTransport> m_transport;

        int64_t last_version;
        time_t last_response_time;

        int num_config_requests;
        long config_request_interval;

        boost::unordered_set<IConfigListener*> configListeners;

        void setState(appdynamics::pb::ConfigResponse::AgentState new_state,
                      const struct _zend_agent_globals* agentGlobals);
        void onReceivedConfigResponse(const appdynamics::pb::ConfigResponse& configResponse,
                                      const struct _zend_agent_globals* agentGlobals);
        long getRequestInterval() { return config_request_interval; }
};

#endif /* __CONFIGS_H */
