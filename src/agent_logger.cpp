/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include <log4cxx/logger.h>
#include <log4cxx/rolling/rollingfileappenderskeleton.h>
#include <log4cxx/xml/domconfigurator.h>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <stdarg.h>
#include <stdio.h>
#include <string>
#include "agent_logger.h"
#include <agent_version.h>
#include "zend_constants.h"

using namespace log4cxx;
using namespace log4cxx::xml;
using namespace log4cxx::helpers;

const int NullLoggerPtr = 0;

/*
 * Initialize the log context names.
 */
const char* LogContext::AGENT = "agent";
const char* LogContext::TX_SERVICE = "agent.tx";
const char* LogContext::CONFIG = "agent.config";
const char* LogContext::ERROR = "agent.error";
const char* LogContext::REPORT = "agent.report";
const char* LogContext::SNAPSHOT = "agent.snapshot";
const char* LogContext::DATAGATHERER = "agent.datagatherer";
const char* LogContext::INSTRUMENT = "agent.instrument";
const char* LogContext::CORRELATION = "agent.correlation";
const char* LogContext::EXIT_HTTP = "agent.exit.http";
const char* LogContext::EXIT_CACHING = "agent.exit.caching";
const char* LogContext::EXIT_DB = "agent.exit.db";
const char* LogContext::INFOPOINT = "agent.infopoint";

log4cxx::LevelPtr RequestDebugLoggingManager::m_previousLogLevel;

void preForkInitLogging()
{
}

void initLogging(const std::string& configFilePath TSRMLS_DC) {
    std::string pidStr(boost::lexical_cast<std::string>(getpid()));
    MDC::put("pid", pidStr);
    MDC::put("version", std::string(AGENT_VERSION));

#if PHP_VERSION_ID >= 70000
    zval* result = zend_get_constant_str("PHP_VERSION", sizeof("PHP_VERSION")-1);
    if (result != NULL) {
        MDC::put("phpVersion", std::string(Z_STRVAL_P(result)));
    }
    // no need for zval_dtor(result), constructor not called on result in zend_get_constant_str().
#else
    zval phpVersion;
    if (zend_get_constant("PHP_VERSION", sizeof("PHP_VERSION")-1, &phpVersion TSRMLS_CC)) {
        MDC::put("phpVersion", std::string(Z_STRVAL(phpVersion)));
        zval_dtor(&phpVersion);
    }
#endif

    DOMConfigurator::configure(configFilePath);
}

bool getLoggingDirectory(boost::filesystem::path* path)
{
    AppenderList appenders = Logger::getRootLogger()->getAllAppenders();
    for (AppenderList::iterator it = appenders.begin(); it != appenders.end(); ++it) {
        if ((*it)->instanceof(FileAppender::getStaticClass())) {
            FileAppender* fileAppender = (FileAppender*) (*it)->cast(FileAppender::getStaticClass());
            *path = fileAppender->getFile();
            path->remove_filename();
            return true;
        }
    }
    return false;
}

AgentLogger getLogger(const std::string& name) {
    LoggerPtr loggerPtr(Logger::getLogger(name));
    if (!loggerPtr) {
        logStartupError(boost::format("Cannot initialize log4cxx - null logger: %1%") % name);
        return Logger::getRootLogger();
    }
    return loggerPtr;
}
