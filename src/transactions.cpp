/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

#include <boost/foreach.hpp>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/functional/hash.hpp>
#include <sstream>
#include "agent.h"
#include "transactions.h"
#include "call_metrics.h"
#include "correlation.h"
#include "current_exit_call.h"
#include "util.h"
#include "http/httpentrypoint.h"
#include "http/httpexitpoint.h"
#include "db/db_exitpoint.h"
#include "caching/caching_exitpoint.h"
#include "webservices/webservices_exitpoint.h"
#include "queue/queue_exitpoint.h"
#include "clientrypoint.h"
#include "snapshot.h"
#include "request_context.h"
#include "frameworks/mvc.h"
#include "frameworks/drupal7.h"
#include "frameworks/wordpress3.h"
#include "method_gatherer.h"
#include "webservices/webservices_entrypoint.h"
#include "zmqtransport.h"
#include "reporting.h"

void convertPBNameValuePairsToMap(const google::protobuf::RepeatedPtrField<appdynamics::pb::Common::NameValuePair>& nameValues,
                                  StringMap& nvMap)
{
    nvMap.clear();
    int nvMap_size = nameValues.size();
    for (int i = 0; i < nvMap_size; i++) {
        const appdynamics::pb::Common::NameValuePair& nvp = nameValues.Get(i);
        nvMap[nvp.name()] = nvp.value();
    }
}

std::ostream& operator<<(std::ostream& out, const TxInfo& tx) {
    out << "Transaction([" << tx.getName() << "], entry point type [" << tx.getEntryPointType() << "] component ID [" << tx.getComponentID() << "])";
    return out;
}

std::ostream& operator<<(std::ostream& out, const AgentTransaction& tx) {
    out << "Agent Transaction [" << tx.getName() << "], entry point type [" << tx.getEntryPointType() << "] component ID [" << tx.getComponentID() << "]";
    return out;
}

/* {{{ ITransactionAcceptor methods */
ITransactionAcceptor::~ITransactionAcceptor()
{
}
/* }}} */

/* {{{ ATransactionAcceptor methods */
bool ATransactionAcceptor::acceptTransaction(const PHPExecEnvironment* phpExecEnv,
                                             const boost::shared_ptr<IMatchPointPayload>& payload)
{
    LOG4CXX_DEBUG(m_logger, "Looking for transaction type [" << getEntryPointName(getEntryPointType()) << "]");

    if (!payload)
        return false;

    // Most of the entry points are HTTP-based, so we set the snapshot URL field
    // to the request URL by default. Non-HTTP entry points can override it as
    // they see fit.
    boost::shared_ptr<HTTPPayload> httpPayload(phpExecEnv->getHTTPPayload());
    BOOST_ASSERT(httpPayload != NULL);

    if (getEntryPointType() != appdynamics::pb::Agent::PHP_CLI) {
        LOG4CXX_DEBUG(m_logger, "Request URL: " << httpPayload->getURL());
        m_txMonitor->getSnapshotManager()->setURL(httpPayload->getURL());
    }

    boost::shared_ptr<CorrelationHeader> corrHeader(checkForContinuingTransaction(payload));

    if (corrHeader && (corrHeader->getType() == CorrelationHeader::INAPP)) {
        // Found a continuing transaction. At this point the transaction context
        // should exist and we can just return;
        LOG4CXX_DEBUG(m_logger, "Found a continuing transaction ["
                                << getEntryPointName(getEntryPointType()) << "]");
        return true;
    }

    if (phpExecEnv->getAgentGlobals().request_context->isBTDetectionDisabled()) {
        LOG4CXX_DEBUG(m_logger, "Transaction detection disabled, ending here");
        return false;
    }

    // We need to deal with a NONE type header in matchOrDiscoverTransaction
    bool const isCorrHeaderValid = !corrHeader || (corrHeader->getType() != CorrelationHeader::NONE);

    boost::shared_ptr<AgentTransaction> agent_tx(matchOrDiscoverTransaction(payload, isCorrHeaderValid));
    if (!agent_tx)
        return false;

    // If we got past the check above, then we have an agent transaction.
    // The transaction may not be registered with the controller ( and may
    // never be if we detect a framework that gives us a better name ).
    LOG4CXX_DEBUG(m_logger, "identified transaction [" << *agent_tx << "]");
    if (!m_txMonitor->getConfigChannel()->isInitialized())
        return false;

    TransactionContext* const txContext =
        m_txMonitor->addTransaction(agent_tx,
                                  getEntryPointType(),
                                  phpExecEnv->getAgentGlobals().request_context->getStartTime());
    if (!txContext)
        return false;

    if (corrHeader) {
        txContext->setCorrelationHeader(corrHeader);
    }
    txContext->setTransactionPayload(payload);
    return true;
}

boost::shared_ptr<AgentTransaction> ATransactionAcceptor::matchOrDiscoverTransaction(
    const boost::shared_ptr<IMatchPointPayload>& payload,
    bool const isCorrHeaderValid)
{
    // First, try to customMatch the TX. If successfully matched, check for a potential
    // previously custom matched transaction and compare its priority to the newly found one.
    // If not custom matched, check if we still have a previously custom matched transaction,
    // which supersedes anything we discover ourselves.

    // TODO:
    // when all of the following occurs:
    // 1. incoming correlation header was determined as type NONE
    // 2. we already have a valid txContext from before
    // 3. that txContext was created with a coninuing registered transaction
    // then we can still end up here.
    // We cannot check txContext->getTransaction()->isCustom() because the matchCriteria
    // was not set in the continuing registered transaction since all we knew was its btID.
    // Our current solution is to create the isCorrHeaderValid flag, but we need to assess the
    // need for a NONE type corrHeader and change the flow of the code to not end up here with
    // a NONE type corrHeader.
    boost::shared_ptr<AgentTransaction> matchedTransaction;
    TransactionContext* txContext = m_txMonitor->getTransactionContext();
    bool matched = customMatchTransaction(&matchedTransaction, payload);

    if (matched) {
        // custom matched transaction found, but
        // is there a previously matched transaction?
        if (isCorrHeaderValid && txContext && txContext->getTransaction()->isCustom()) {
            boost::shared_ptr<MatchCriteriaLocal> oldCriteria(
                    boost::static_pointer_cast<MatchCriteriaLocal>(txContext->getTransaction()->getMatchCriteria()));
            boost::shared_ptr<MatchCriteriaLocal> newCriteria(
                    boost::static_pointer_cast<MatchCriteriaLocal>(matchedTransaction->getMatchCriteria()));
            // does the previosly matched transaction have equal or higher priority?
            if (oldCriteria->getCustomMatchRule()->priority() >= newCriteria->getCustomMatchRule()->priority()) {
                LOG4CXX_DEBUG(m_logger, "Custom rules matched, but already have an existing custom BT with equal or higher priority");
                return boost::shared_ptr<AgentTransaction>();
            }
        }
        // return the new matchedTransaction only if priority is higher than a previously discovered one
        // or there is no previously discovered one
        return matchedTransaction;
    }

    // Was there a custom matched transaction before that?
    if (isCorrHeaderValid && txContext && txContext->getTransaction()->isCustom())
    {
        LOG4CXX_DEBUG(m_logger, "No custom rules matched, but already have existing custom BT, not doing transaction discovery");
        return boost::shared_ptr<AgentTransaction>();
    }

    if (isDiscoveryEnabled())
    {
        LOG4CXX_DEBUG(m_logger, "No custom rules matched, doing transaction discovery");
        return discoverTransaction(payload);
    }

    LOG4CXX_DEBUG(m_logger, "Discovery disabled, monitoring only custom transactions");
    return boost::shared_ptr<AgentTransaction>();
}

boost::shared_ptr<AgentTransaction> ATransactionAcceptor::discoverTransaction(const boost::shared_ptr<IMatchPointPayload>& payload)
{
    if (!isDiscoveryEnabled())
        return boost::shared_ptr<AgentTransaction>();

    // If we don't have discovery config, then isDiscoveryEnabled
    // should have returned false.
    BOOST_ASSERT(m_matchPointConfig->get()->has_discoveryconfig());

    if (matchTransactionRules(payload, m_matchPointConfig->get()->discoveryconfig().excludes()))
    {
        LOG4CXX_DEBUG(m_logger, "Exclude rule matched, discovered BT will not be reported");
        return boost::shared_ptr<AgentTransaction>();
    }

    std::string tx_name(getNameFromNamingScheme(payload));

    if (tx_name.empty()) {
        LOG4CXX_DEBUG(m_logger, "empty transaction name for "
                                    << m_matchPointConfig->get()->discoveryconfig().DebugString()
                                    << ", type: "
                                    << getEntryPointName(getEntryPointType()));
        return boost::shared_ptr<AgentTransaction>();
    }

    boost::shared_ptr<AgentTransaction> agentTx(getTransaction(tx_name, NULL));
    return agentTx;

}

bool ATransactionAcceptor::customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                                  const boost::shared_ptr<IMatchPointPayload>& payload)
{
    return false;
}

bool ATransactionAcceptor::matchTransactionRules(const boost::shared_ptr<IMatchPointPayload>& payload,
                                                 const google::protobuf::RepeatedPtrField<appdynamics::pb::Agent::EntryPointMatchCondition>& matchConditions)
{
    if (matchConditions.size() == 0)
        return false;
    for (auto it = matchConditions.begin(); it != matchConditions.end(); ++it) {
        if (matchTransactionRule(payload, *it))
            return true;
    }
    return false;
}

bool ATransactionAcceptor::matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& payload,
                                                const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition)
{
    return false;
}

bool ATransactionAcceptor::isDiscoveryEnabled()
{
    const appdynamics::pb::Agent::MatchPointConfig* const config =
        m_matchPointConfig->get();
    if (!config->has_discoveryconfig())
        return false;
    return config->discoveryconfig().enabled();
}

ATransactionAcceptor::~ATransactionAcceptor()
{
}

boost::shared_ptr<AgentTransaction> ATransactionAcceptor::getTransaction(const std::string& transactionName,
                                                                         const appdynamics::pb::Agent::MatchPointConfig::CustomMatch* customMatch)
{
    std::string txName(transactionName);
    // This makes using external APIs safe as the TX names can be really long.
    if (txName.length() > 100) {
        txName.erase(100);
    }

    appdynamics::pb::Agent::EntryPointType entryPointType = getEntryPointType();
    std::string entryPointTypeName(getEntryPointName(entryPointType));
    int64_t componentID = m_txMonitor->getConfigChannel()->getTierID();
    TxInfo txInfo(txName, entryPointTypeName, componentID);
    TransactionRegistry* const registry = m_txMonitor->getTransactionRegistry();

    // If the transaction is excluded, return empty transaction.
    if (m_txMonitor->isTransactionExcluded(txName, entryPointType)) {
        LOG4CXX_TRACE(m_logger, "excluding transaction ["
                                    << txName
                                    << "] for type ["
                                    << entryPointTypeName
                                    << "]");
        return boost::shared_ptr<AgentTransaction>();
    }

    const boost::unordered_map<TxInfo, int64_t>& transactions = registry->getTransactions();
    auto it = transactions.find(txInfo);
    const bool foundExistingBT = it != transactions.end();

    // If we found an existing BT entry for the TxInfo,
    // then we don't need to worry about overflow.  If the existing
    // entry we found is the NULL_BT_ID, then that is still ok because
    // the proxy will prevent too many BTs from being registed.  The next
    // config response from the proxy will cause the transaction map to get
    // rebuilt and then we won't have any NULL_BT_ID's anymore.
    if (!foundExistingBT) {
        const size_t totalTransactionCount = registry->getTotalNumberOfTransactions();
        // If the number of transactions exceeds the maximum, return empty transaction.
        if (totalTransactionCount >= TransactionRegistry::MAX_TRANSACTIONS) {
            LOG4CXX_DEBUG(m_logger, "dropping transaction ["
                                    << txName
                                    << "] for type ["
                                    << entryPointTypeName
                                    << "]");
            return boost::shared_ptr<AgentTransaction>();
        }
    }
    // If transaction is not found in the transaction registry,
    // or if the BT id is not yet known,
    if ((!foundExistingBT) || (it->second == AgentTransaction::NULL_BT_ID)) {
        boost::shared_ptr<AgentTransaction> agentTx;
        // If a custom match rule is passed in, generate the custom-matched BT.
        if (customMatch) {
            agentTx = AgentTransaction::createMatchedUnRegisteredTransaction(txName,
                                                                             entryPointTypeName,
                                                                             m_matchPointConfig,
                                                                             customMatch,
                                                                             componentID);
        } else { // else, generate the discovered BT.
            agentTx = AgentTransaction::createDiscoveredUnRegisteredTransaction(txName,
                                                                                entryPointTypeName,
                                                                                m_matchPointConfig,
                                                                                componentID);
        }
        // Register the BT.
        registry->addUnRegisteredTransaction(agentTx);
        m_txMonitor->getTxLogger()->logIdentifiedTx(txName, entryPointType, true);

        return agentTx;
    }

    const int64_t txID = it->second;
    // If transaction is found in transaction registry, return that transaction.
    BOOST_ASSERT(txID != AgentTransaction::NULL_BT_ID);
    return AgentTransaction::createRegisteredTransaction(txName,
                                                         entryPointTypeName,
                                                         m_matchPointConfig,
                                                         customMatch,
                                                         componentID,
                                                         txID);

}

/* }}} */

/* {{{ AgentTransaction methods */

const std::string AgentTransaction::getDisplayName() const
{
    const std::string& name(m_txInfo.getName());
    if (name.empty())
        return boost::lexical_cast<std::string>(m_txID);

    std::ostringstream out;
    out << "name=" << name << ", id=" << m_txID << "";
    return out.str();
}

// these two following methods assume that the transaction
// has a valid/set m_criteria. Will throw NPE on unset criteria
bool AgentTransaction::isAutoDiscovered() const
{
    return getMatchCriteria()->isAutoDiscovered();
}

bool AgentTransaction::isCustom() const
{
    return getMatchCriteria()->isCustom();
}

void AgentTransaction::fillInBTIdentifier(appdynamics::pb::BTIdentifier* btIdentifier)
{
    if (isRegistered()) {
        btIdentifier->set_type(m_continuing ? appdynamics::pb::BTIdentifier::REMOTE_REGISTERED
                                            : appdynamics::pb::BTIdentifier::REGISTERED);
        btIdentifier->set_btid(getID());
    } else {
        if (m_continuing) {
            btIdentifier->set_type(appdynamics::pb::BTIdentifier::REMOTE_UNREGISTERED);
            appdynamics::pb::UnRegisteredRemoteBT& unregBT = *(btIdentifier->mutable_unregisteredremotebt());
            if (isAutoDiscovered()) {
                unregBT.set_matchcriteriatype(appdynamics::pb::UnRegisteredRemoteBT::DISCOVERED);
                boost::shared_ptr<MatchCriteriaContinuing> matchCriteria(
                        boost::static_pointer_cast<MatchCriteriaContinuing>(getMatchCriteria()));
                unregBT.set_namingschemetype(matchCriteria->getNamingSchemeType());
            } else {
                unregBT.set_matchcriteriatype(appdynamics::pb::UnRegisteredRemoteBT::CUSTOM);
            }

            unregBT.set_btname(getName());
            unregBT.set_entrypointtype(getEntryPointType());

        } else {
            btIdentifier->set_type(appdynamics::pb::BTIdentifier::UNREGISTERED);
            appdynamics::pb::UnRegisteredBT& unregBT = *(btIdentifier->mutable_unregisteredbt());
            appdynamics::pb::BTInfo& btInfo = *(unregBT.mutable_btinfo());

            btInfo.set_internalname(getName());

            appdynamics::pb::Agent::EntryPointType entryPointType(appdynamics::pb::Agent::PHP_WEB);
            EntryPointType_Parse(getEntryPointType(), &entryPointType);
            btInfo.set_entrypointtype(entryPointType);

            if (!isAutoDiscovered()) {
                boost::shared_ptr<MatchCriteriaLocal> matchCriteria(
                        boost::static_pointer_cast<MatchCriteriaLocal>(getMatchCriteria()));
                unregBT.set_custommatchpointdefinitionid(matchCriteria->getCustomMatchRule()->id());
            }
        }
    }
}

/* }}} */

/* {{{ TransactionContext methods */
TransactionContext::TransactionContext(const boost::shared_ptr<RequestContext>& requestContext,
                                       const boost::shared_ptr<AgentTransaction>& tx,
                                       appdynamics::pb::Agent::EntryPointType type,
                                       const TimePoint& startTime)
    : m_requestContext(requestContext)
    , m_agentTX(tx)
    , m_entryPointType(type)
    , m_origin(RINIT)
    , m_currentExitCall(NULL)
    , m_snapshotEnabled(true)
    , m_downstreamDetectionDisabled(false)
    , m_initiatingInterceptor(NULL)
    , m_exitCallCounter(0)
    , m_continuingTransaction(false)
    , m_correlationHeader()
    , m_txPayload()
    , m_startTime(startTime)
{
}

TransactionContext::~TransactionContext()
{
}

void TransactionContext::clearCurrentExitCall()
{
    delete m_currentExitCall;
    m_currentExitCall = NULL;
}

const std::string TransactionContext::getTransactionName()
{
    if (m_agentTX) {
        return m_agentTX->getDisplayName();
    }

    return std::string();
}

bool TransactionContext::isErrorTransaction() const
{
    return m_requestContext->isErrorRequest();
}

ErrorRegistry* TransactionContext::getErrorRegistry() const
{
    return m_requestContext->getErrorRegistry();
}

void TransactionContext::reportExitCallData(const BackendIdentifierPtr& backendID,
                                            const std::string& category,
                                            long timeTaken,
                                            long callCount,
                                            unsigned errorCount)
{
    m_requestContext->reportExitCallData(backendID,
                                         category,
                                         timeTaken,
                                         callCount,
                                         errorCount);
}

void TransactionContext::reportExitCall(const CurrentExitCall* currentExitCall)
{
    // If we don't have a backend id, then
    // there is no way or reason to report
    // the amount of time taken.
    if (!currentExitCall->hasBackendID())
        return;

    m_requestContext->reportExitCallData(currentExitCall->getBackendID(),
                                         currentExitCall->getCategory(),
                                         currentExitCall->getElapsedTimeInUs(),
                                         1,
                                         currentExitCall->getErrorCount());
}

const boost::unordered_map<ExitCallMetricsKey, boost::shared_ptr<CallMetrics>>& TransactionContext::getRegisteredExitCallMetrics() const
{
    return m_requestContext->getRegisteredExitCallMetrics();
}

void TransactionContext::gatherDataForMethod(const PHPExecEnvironment* execEnv,
                                             const appdynamics::pb::Instrumentation::MethodDataGathererConfig& gathererConfig)
{
    if (!m_methodDataGatherer)
        m_methodDataGatherer = MethodData::Gatherer::create(execEnv);

    m_methodDataGatherer->gatherData(gathererConfig);
}

void TransactionContext::mergeMethodDataIntoSnapshot(appdynamics::pb::Snapshot* pbSnapshot) const
{
    if (!m_methodDataGatherer)
        return;
    m_methodDataGatherer->fillInGatheredData(pbSnapshot->mutable_methodinvocationdata());
}

/* }}} */

/* {{{ TransactionMonitor methods */
TransactionMonitor::TransactionMonitor(AgentContext* agentContext)
    : m_logger(getLogger(std::string(LogContext::TX_SERVICE) + "TransactionMonitor"))
    , m_configChannel(agentContext->getConfigChannel())
    , m_snapshotManager(agentContext->getSnapshotManager())
    , tx_registry(agentContext->getConfigChannel())
    , tx_reporter()
    , m_transactionContext(NULL)
    , m_enabled(true)
{
    boost::shared_ptr<IBTInfoTransport> btInfoTransport =
        ZMQControlTransport::createTransport<ZMQBTInfoTransport>(agentContext->getControlTransport(),
                                                                 ZMQTransportBase::getBaseSocketNumber(),
                                                                 m_configChannel);

    boost::shared_ptr<IReportingTransport> reportingTransport =
        ZMQControlTransport::createTransport<ZMQReportingTransport>(agentContext->getControlTransport(),
                                                                    ZMQTransportBase::getBaseSocketNumber() + 1,
                                                                    AG(reporting_linger));

    tx_reporter.reset(new TransactionReporter(reportingTransport, btInfoTransport));

    agentContext->registerBailoutHandler(this);
    agentContext->registerBailoutHandler(&m_errorMonitor);
    m_configChannel->addListener(this);
}

TransactionMonitor::~TransactionMonitor()
{
}

void TransactionMonitor::disable()
{
    m_enabled = false;
    tx_registry.onMonitorDisable();
    m_errorMonitor.onMonitorDisable();
}

void TransactionMonitor::enable()
{
    m_enabled = true;
    m_errorMonitor.onMonitorEnable();
    tx_registry.onMonitorEnable();
}

void TransactionMonitor::reset()
{
    tx_registry.reset();
    tx_logger.reset();
    tx_reporter->resetBTInfoResponseTimeOutFlag();
}

void TransactionMonitor::configChanged(const appdynamics::pb::ConfigResponse& configResponse,
                                       const struct _zend_agent_globals* agentGlobals)
{
    // Initialize entry/exit point delegates on the very first config response
    if (m_entryPointDelegates.empty()) {
        createEntryPointDelegates(agentGlobals->icept_engine);
        createExitPointDelegates(agentGlobals->icept_engine);
    }

    LOG4CXX_TRACE(m_logger, "transaction config response: " << configResponse.DebugString());

    if (configResponse.has_txconfig()) {
        boost::shared_ptr<appdynamics::pb::TransactionConfig> txConfig(boost::make_shared<appdynamics::pb::TransactionConfig>());
        txConfig->CopyFrom(configResponse.txconfig());
        auto end = m_entryPointDelegates.end();
        for (auto i = m_entryPointDelegates.begin(); i != end; ++i)
            i->second->configure(txConfig, agentGlobals);
    }

    if (configResponse.has_bckconfig() && configResponse.bckconfig().has_phpdefinition()) {
        auto end = m_exitPointDelegates.end();
        for (auto i = m_exitPointDelegates.begin(); i != end; ++i)
            i->second->configure(configResponse.bckconfig().phpdefinition(), agentGlobals);
    }

    if ((configResponse.has_command()) && (configResponse.command() == appdynamics::pb::ConfigResponse::RESET))
        reset();

    if (configResponse.has_txinfo()) {
        const appdynamics::pb::TransactionInfo& txInfo = configResponse.txinfo();

        const auto& registeredBTs = txInfo.registeredbts();

        tx_registry.setRegisteredTransactions(registeredBTs);
        tx_logger.clearDiscoveredTransactions(registeredBTs);

        if (txInfo.blacklistedandexcludedbts_size() > 0)
        {
            boost::unordered_set<std::pair<std::string, appdynamics::pb::Agent::EntryPointType>> excluded;
            BOOST_FOREACH(const auto& btInfo, txInfo.blacklistedandexcludedbts())
            {
                appdynamics::pb::Agent::EntryPointType entryPointType = btInfo.entrypointtype();
                excluded.insert(std::make_pair(btInfo.internalname(), entryPointType));
            }

            setExcludedTransactions(excluded);
        }
    }

    if (configResponse.has_bckinfo()) {
        const appdynamics::pb::BackendInfo& be_info = configResponse.bckinfo();
        tx_registry.getExitCallRegistry()->updateBackendInfo(be_info);
    }

    if (configResponse.has_errorconfig()) {
        m_errorMonitor.configure(configResponse.errorconfig());
    }
}

void TransactionMonitor::createEntryPointDelegates(InterceptionEngine* interceptEngine)
{
    typedef std::pair<appdynamics::pb::Agent::EntryPointType, boost::shared_ptr<AEntryPointDelegate>> t_Entry;

    // The frameworks may have CLI-based scripts for cron jobs and other things,
    // but they certainly will not hit the same entry points that we're looking
    // for normally. Thus, if we're running under CLI SAPI, we register only the
    // CLI entry point.
    if (AG(is_cli_sapi)) {
        boost::shared_ptr<AEntryPointDelegate> const cli(boost::make_shared<CLI::EntryPointDelegate>(interceptEngine, this));
        m_entryPointDelegates.insert(t_Entry(appdynamics::pb::Agent::PHP_CLI, cli));
    } else {
        boost::shared_ptr<AEntryPointDelegate> const http(boost::make_shared<HTTPEntryPointDelegate>(interceptEngine, this));
        m_entryPointDelegates.insert(t_Entry(appdynamics::pb::Agent::PHP_WEB, http));

        boost::shared_ptr<AEntryPointDelegate> const mvc(boost::make_shared<MVCEntryPointDelegate>(interceptEngine, this));
        m_entryPointDelegates.insert(t_Entry(appdynamics::pb::Agent::PHP_MVC, mvc));

        boost::shared_ptr<AEntryPointDelegate> const drupal(boost::make_shared<Drupal7::EntryPointDelegate>(interceptEngine, this));
        m_entryPointDelegates.insert(t_Entry(appdynamics::pb::Agent::PHP_DRUPAL, drupal));

        boost::shared_ptr<AEntryPointDelegate> const wordpress(boost::make_shared<Wordpress3::EntryPointDelegate>(interceptEngine, this));
        m_entryPointDelegates.insert(t_Entry(appdynamics::pb::Agent::PHP_WORDPRESS, wordpress));

        boost::shared_ptr<AEntryPointDelegate> const webservices(boost::make_shared<WebServices::EntryPointDelegate>(interceptEngine, this));
        m_entryPointDelegates.insert(t_Entry(appdynamics::pb::Agent::PHP_WEB_SERVICE, webservices));
    }
}

void TransactionMonitor::createExitPointDelegates(InterceptionEngine* interceptEngine)
{
    typedef std::pair<appdynamics::pb::Agent::ExitPointType, boost::shared_ptr<AExitPointDelegate>> t_Entry;

    boost::shared_ptr<AExitPointDelegate> const http(boost::make_shared<HTTPExitPointDelegate>(interceptEngine, this));
    m_exitPointDelegates.insert(t_Entry(appdynamics::pb::Agent::EXIT_HTTP, http));

    boost::shared_ptr<AExitPointDelegate> const db(boost::make_shared<DBExitPointDelegate>(interceptEngine, this));
    m_exitPointDelegates.insert(t_Entry(appdynamics::pb::Agent::EXIT_DB, db));

    boost::shared_ptr<AExitPointDelegate> const cache(boost::make_shared<CachingExitPointDelegate>(interceptEngine, this));
    m_exitPointDelegates.insert(t_Entry(appdynamics::pb::Agent::EXIT_CACHE, cache));

    boost::shared_ptr<AExitPointDelegate> const webservice(boost::make_shared<WebservicesExitPointDelegate>(interceptEngine, this));
    m_exitPointDelegates.insert(t_Entry(appdynamics::pb::Agent::EXIT_WEBSERVICE, webservice));

    boost::shared_ptr<AExitPointDelegate> const queue(boost::make_shared<QueueExitPointDelegate>(interceptEngine, this));
    m_exitPointDelegates.insert(t_Entry(appdynamics::pb::Agent::EXIT_RABBITMQ, queue));
}

void TransactionMonitor::disableBTDetection()
{
    if (m_requestContext)
        m_requestContext->setBTDetectionDisabled(true);
    if (m_transactionContext)
        m_transactionContext->setDownstreamDetectionDisabled(true);
}

void TransactionMonitor::onRequestBegin(const PHPExecEnvironment* phpExecEnv,
                                        const boost::shared_ptr<RequestContext>& requestContext)
{
    CXXGlobals& cxxGlobals = phpExecEnv->getAgentGlobals();
    delete m_transactionContext;
    m_transactionContext = NULL;
    requestContext->setProxyRequestID(AG(kernel)->generateRandomNumber());
    m_errorMonitor.onRequestBegin(requestContext);
    m_snapshotManager->startSnapshot(&cxxGlobals.callGraphCollectionState);
    m_requestContext = requestContext;
    MethodInterceptorDelegate::setDisabled(!isEnabled());

    // Fill in the array that caches exit point enabled status for faster look-up
    if (m_configChannel->isAgentEnabled()) {
        BOOST_FOREACH(const auto& it, m_exitPointDelegates) {
            cxxGlobals.setExitPointEnabled(it.first, !it.second->isDetectionDisabled());
        }
    }
}

void TransactionMonitor::onRequestEnd(const PHPExecEnvironment* phpExecEnv,
                                      const boost::shared_ptr<RequestContext>& requestContext,
                                      bool isRequestEnding)
{
    CallGraph::GraphCollectionState* callGraphCollectionState =
        &(phpExecEnv->getAgentGlobals().callGraphCollectionState);
    Snapshot* snapshot = m_snapshotManager->finishSnapshot(callGraphCollectionState);

    const Timer* const timer = AgentKernel::getTimer();
    uint64_t requestEndTimeStamp = timer->getTimestamp();

    if (isRequestEnding)
        callGraphCollectionState->clear(NULL);
    else
        callGraphCollectionState->onTransactionRestart(NULL, requestEndTimeStamp);

    if (m_transactionContext) {
        getTransactionReporter()->reportTransactionDetails(phpExecEnv,
                                                           m_transactionContext,
                                                           requestEndTimeStamp,
                                                           m_requestContext,
                                                           snapshot);
    }

    /**
     * Deletes snapshot data, including the callgraph.
     */
    m_snapshotManager->resetSnapshot(callGraphCollectionState);
    if (!isRequestEnding)
        m_snapshotManager->startSnapshot(callGraphCollectionState, false);

    m_errorMonitor.onTransactionEnd();

    requestContext->resetProxyMessageID();
    getTransactionReporter()->resetBTInfoResponseTimeOutFlag();

    /*
     * Delete the context. All transaction data should be gone now.
     */
    delete m_transactionContext;
    m_transactionContext = NULL;

    m_requestContext->clear();
}

void TransactionMonitor::afterBailout()
{
    if (m_transactionContext)
        m_transactionContext->clearCurrentExitCall();
}

void TransactionMonitor::abortTransaction()
{
    if (!isEnabled())
        return;

    if (!m_transactionContext)
        return;

    delete m_transactionContext;
    m_transactionContext = NULL;
}

TransactionContext* TransactionMonitor::addTransaction(const boost::shared_ptr<AgentTransaction>& agentTx,
                                                       appdynamics::pb::Agent::EntryPointType entryPointType,
                                                       const TimePoint& startTime)
{
    if (!isEnabled())
        return NULL;

    if (m_transactionContext) {
        delete m_transactionContext;
        m_transactionContext = NULL;
    }

    m_transactionContext = new TransactionContext(m_requestContext,
                                                  agentTx,
                                                  entryPointType,
                                                  startTime);
    return m_transactionContext;
}

TransactionContext*
TransactionMonitor::addContinuingTransaction(const boost::shared_ptr<CorrelationHeader>& corrHeader,
                                             appdynamics::pb::Agent::EntryPointType interceptingEntryPointType,
                                             const TimePoint& startTime)
{
    if (!isEnabled())
        return NULL;

    if (m_transactionContext) {
        delete m_transactionContext;
        m_transactionContext = NULL;
    }

    std::string btIDHeader(corrHeader->getSubHeader(CorrelationHeader::BUSINESS_TRANSACTION_ID_HEADER));
    std::string btNameHeader(corrHeader->getSubHeader(CorrelationHeader::BUSINESS_TRANSACTION_NAME_HEADER));

    boost::shared_ptr<AgentTransaction> transaction;

    if (!btIDHeader.empty()) {
        int64_t btID = AgentTransaction::NULL_BT_ID;
        try {
            btID = boost::lexical_cast<int64_t>(btIDHeader);
        } catch (const boost::bad_lexical_cast& e) {
            LOG4CXX_WARN(m_logger, "Malformed BT id in correlation header, not monitoring transaction");
            return NULL;
        }

        transaction = AgentTransaction::createContinuingRegisteredTransaction(btID);

    } else if (!btNameHeader.empty()) {
        std::string btEntryPointTypeHeader(corrHeader->getSubHeader(CorrelationHeader::ENTRY_POINT_TYPE_HEADER));
        std::string btComponentHeader(corrHeader->getSubHeader(CorrelationHeader::BT_COMPONENT_MAPPING_HEADER));
        std::string btMatchCriteriaTypeHeader(corrHeader->getSubHeader(CorrelationHeader::MATCH_CRITERIA_TYPE_HEADER));
        std::string btMatchCriteriaValueHeader(corrHeader->getSubHeader(CorrelationHeader::MATCH_CRITERIA_VALUE_HEADER));

        if (btEntryPointTypeHeader.empty()) {
            LOG4CXX_WARN(m_logger, "Missing BT entry point type in correlation header, not monitoring transaction");
            return NULL;
        }

        int64_t btComponentID;
        try {
            btComponentID = boost::lexical_cast<int64_t>(btComponentHeader);
        } catch (const boost::bad_lexical_cast& e) {
            LOG4CXX_WARN(m_logger, "Malformed BT component id in correlation header, not monitoring transaction");
            return NULL;
        }

        boost::shared_ptr<MatchCriteriaContinuing> matchCriteria;
        if (btMatchCriteriaTypeHeader == CorrelationHeader::MATCH_CRITERIA_TYPE_DISCOVERED) {
            // auto-discovery
            appdynamics::pb::Agent::MatchPointConfig_Discovery discoveryConfig;
            discoveryConfig.mutable_namingscheme()->set_type(btMatchCriteriaValueHeader);
            matchCriteria = boost::make_shared<MatchCriteriaContinuing>(discoveryConfig);
        } else {
            // custom
            appdynamics::pb::Agent::MatchPointConfig_CustomMatch customMatch;
            customMatch.set_btname(btMatchCriteriaValueHeader);
            matchCriteria = boost::make_shared<MatchCriteriaContinuing>(customMatch);
        }

        transaction = AgentTransaction::createContinuingUnRegisteredTransaction(btNameHeader, btEntryPointTypeHeader, btComponentID, matchCriteria);

    } else {
        LOG4CXX_WARN(m_logger, "Invalid correlation header, did not find BT id or name");
        return NULL;
    }

    m_transactionContext = new TransactionContext(m_requestContext,
                                                  transaction,
                                                  interceptingEntryPointType,
                                                  startTime);
    m_transactionContext->setContinuingTransaction(true);
    m_requestContext->setRequestGUID(corrHeader->getSubHeader(CorrelationHeader::REQUEST_GUID_HEADER));

    return m_transactionContext;
}

bool TransactionMonitor::isTransactionExcluded(const std::string& name, appdynamics::pb::Agent::EntryPointType entryPointType)
{
    if (excluded_txs.count(std::make_pair(name, entryPointType)) > 0) {
        return true;
    }
    return false;
}

AEntryPointDelegate*
TransactionMonitor::getEntryPointDelegate(appdynamics::pb::Agent::EntryPointType type)
{
    auto entry(m_entryPointDelegates.find(type));
    if (entry != m_entryPointDelegates.end())
        return entry->second.get();

    return NULL;
}

AExitPointDelegate*
TransactionMonitor::getExitPointDelegate(appdynamics::pb::Agent::ExitPointType type)
{
    auto entry(m_exitPointDelegates.find(type));
    if (entry != m_exitPointDelegates.end())
        return entry->second.get();

    return NULL;
}
/* }}} */

/* {{{ TransactionRegistry methods */
TransactionRegistry::~TransactionRegistry()
{
}

void TransactionRegistry::setRegisteredTransactions(const google::protobuf::RepeatedPtrField<appdynamics::pb::RegisteredBT>& registered)
{
    m_transactions.clear();

    int64_t const componentID = m_configChannel->getTierID();

    auto const registeredEnd = registered.end();
    for (auto i = registered.begin() ; i != registeredEnd; ++i) {
        const appdynamics::pb::RegisteredBT& registeredBT = *i;
        const appdynamics::pb::BTInfo& registeredBTInfo =
            registeredBT.btinfo();
        int64_t const btID = registeredBT.id();
        if (btID <= 0) {
            LOG4CXX_WARN(m_logger,
                         "invalid registered transaction received from proxy, name = ["
                            << registeredBTInfo.internalname()
                            << "], id = ["
                            << btID
                            << "]");
            continue;
        }
        TxInfo txInfo(registeredBTInfo.internalname(),
                      appdynamics::pb::Agent::EntryPointType_Name(registeredBTInfo.entrypointtype()),
                      componentID);
        m_transactions[txInfo] = btID;
    }
}

void TransactionRegistry::addUnRegisteredTransaction(const boost::shared_ptr<AgentTransaction>& discoveredTransaction)
{
    const TxInfo& txInfo = discoveredTransaction->getTxInfo();
    if (txInfo.isValid()) {
        m_transactions[txInfo] = AgentTransaction::NULL_BT_ID;
        LOG4CXX_DEBUG(m_logger, "added new transaction " << txInfo);
    } else {
        LOG4CXX_WARN(m_logger, "invalid transaction info, not adding");
    }
}

void TransactionRegistry::reset()
{
    m_transactions.clear();
    exitcall_registry.reset();
}

void TransactionRegistry::onMonitorDisable()
{
    m_transactions.clear();
    exitcall_registry.onMonitorDisable();
}

void TransactionRegistry::onMonitorEnable()
{
}

/* }}} */

/* {{{ TxLogger methods */
TxLogger::~TxLogger()
{
    logDroppedTxStats();
}

void TxLogger::logIdentifiedTx(const std::string& txName,
                               appdynamics::pb::Agent::EntryPointType entryPointType,
                               bool isAutoDiscovered)
{
    if (discovered.count(txName) > 0) {
        return;
    }

    discovered.insert(txName);
    ++tx_index;
    LOG4CXX_TRACE(logger,
                  "Discovered BT #"
                    << tx_index
                    << " Name ["
                    << txName
                    << "] Type ["
                    << getEntryPointName(entryPointType)
                    << "] Custom ["
                    << !isAutoDiscovered << "]");
}

void TxLogger::logDroppedTx(const std::string& txName,
                            appdynamics::pb::Agent::EntryPointType entryPointType,
                            bool isAutoDiscovered)
{
    if (dropped_index == 0) {
        LOG4CXX_WARN(logger,
                     "Transaction limit of "
                        << TransactionRegistry::MAX_TRANSACTIONS
                        << " reached, only excluded/dropped BTs will be logged from now");
    }

    std::pair<std::string, appdynamics::pb::Agent::EntryPointType> droppedID(txName, entryPointType);
    auto it = dropped.find(droppedID);
    if (it != dropped.end()) {
        it->second++;
        return;
    }

    if (dropped_index > MAX_DROPPED_INDEX) {
        return;
    }

    ++dropped_index;

    LOG4CXX_DEBUG(logger,
                  "Dropped BT #"
                    << dropped_index
                    << " Name ["
                    << txName
                    << "] Type ["
                    << getEntryPointName(entryPointType)
                    << "] Custom ["
                    << !isAutoDiscovered
                    << "]");
    dropped[droppedID] = 1;

    /*
     * Log dropped BT stats on a multiple of MAX_TRANSACTIONS
     */
    if ((dropped_index % TransactionRegistry::MAX_TRANSACTIONS) == 0)
        logDroppedTxStats();
}

void TxLogger::logDroppedTxStats() const
{
    if (dropped.empty()) {
        return;
    }

    LOG4CXX_DEBUG(logger, "Logging dropped transactions stats");
    BOOST_FOREACH(const auto& it, dropped) {
        LOG4CXX_DEBUG(logger, "Dropped BT [" << it.first.first << "] Load [" << it.second << "]");
    }
}

void TxLogger::clearDiscoveredTransactions(const google::protobuf::RepeatedPtrField<appdynamics::pb::RegisteredBT>& registered)
{
    auto const registeredEnd = registered.end();
    for (auto i = registered.end() ; i != registeredEnd; ++i) {
        const appdynamics::pb::RegisteredBT& registeredBT = *i;
        const appdynamics::pb::BTInfo& registeredBTInfo =
            registeredBT.btinfo();
        discovered.erase(registeredBTInfo.internalname());
    }
}

void TxLogger::reset()
{
    discovered.clear();
    dropped.clear();
    tx_index = dropped_index = 0;
}


/* }}} */

// vim: set fdm=marker fdl=0 fdc=2:
