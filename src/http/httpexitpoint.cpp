/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */

/*
 * Reduced array-bounds to a warning due to boost/gcc issues
 * described here:
 * https://bugzilla.redhat.com/show_bug.cgi?id=555771
 * As long as we build on centos5, we're stuck.
 */

#pragma GCC diagnostic warning "-Warray-bounds"

#include "agent.h"
#include "httpexitpoint.h"
#include "fopen_exitpoint.h"
#include "curl_exitpoint.h"
#include "drupal7_http_exitpoint.h"
#include "zend1_http_exitpoint.h"
#include "zend2_http_exitpoint.h"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/assign/list_inserter.hpp>
#include <boost/lexical_cast.hpp>
#include "naming.h"
#include "expressions.h"
#include "php_url.h"

extern "C"
{
#include "ext/standard/url.h"
}

DEFINE_ENUM(HTTPExitPointIndentifyingPropertyNames, HTTP_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

FOpenInterceptor fopenInterceptor;
FileGetContentsInterceptor fileGetContentsInterceptor;
CurlInitInterceptor curlInitInterceptor;
CurlSetoptInterceptor curlSetoptInterceptor;
CurlSetoptArrayInterceptor curlSetoptArrayInterceptor;
CurlExecInterceptor curlExecInterceptor;
CurlCloseInterceptor curlCloseInterceptor;
CurlMultiAddHandleInterceptor curlMultiAddHandleInterceptor;
CurlMultiRemoveHandleInterceptor curlMultiRemoveHandleInterceptor;
CurlMultiExecInterceptor curlMultiExecInterceptor;
CurlMultiSelectInterceptor curlMultiSelectInterceptor;
CurlMultiInfoReadInterceptor curlMultiInfoReadInterceptor;
Drupal7::StreamSocketClientInterceptor drupal7StreamSocketClientInterceptor;
Drupal7::HTTPRequestInterceptor drupal7HTTPRequestInterceptor;
Zend1::HTTPClientRequestInterceptor zend1HTTPClientRequestInterceptor;
#if PHP_VERSION_ID >= 50300
Zend2::HTTPDoRequestInterceptor zend2HTTPDoRequestInterceptor;
Zend2::HTTPClientRequestInterceptor zend2HTTPClientRequestInterceptor;
#endif

static const char FOPEN_SYMBOL[] = "fopen";
static const char FILEGETCONTENTS_SYMBOL[] = "file_get_contents";

static const char CURL_INIT_SYMBOL[] = "curl_init";
static const char CURL_SETOPT_SYMBOL[] = "curl_setopt";
static const char CURL_SETOPT_ARRAY_SYMBOL[] = "curl_setopt_array";
static const char CURL_EXEC_SYMBOL[] = "curl_exec";
static const char CURL_CLOSE_SYMBOL[] = "curl_close";

static const char CURL_MULTI_ADD_HANDLE_SYMBOL[] = "curl_multi_add_handle";
static const char CURL_MULTI_REMOVE_HANDLE_SYMBOL[] = "curl_multi_remove_handle";
static const char CURL_MULTI_EXEC_SYMBOL[] = "curl_multi_exec";
static const char CURL_MULTI_SELECT_SYMBOL[] = "curl_multi_select";
static const char CURL_MULTI_INFO_READ_SYMBOL[] = "curl_multi_info_read";

namespace Drupal7
{
    static const char HTTP_REQUEST_SYMBOL[] = "drupal_http_request";
}

namespace Zend1
{
    static const char HTTP_CLIENT_SYMBOL[] = "Zend_Http_Client";
    static const char VARIEN_HTTP_CLIENT_SYMBOL[] = "Varien_Http_Client";
    static const char HTTP_CLIENT_REQUEST_METHOD[] = "request";
}

namespace Zend2
{
    static const char HTTP_CLIENT_SYMBOL[] = "Zend\\Http\\Client";
    static const char HTTP_CLIENT_SEND_METHOD[] = "send";
}

int curlResourceType = 0;
int curlMultiResourceType = 0;

const AHandleInfo::TypeTag HTTPExitPointHandleInfo::typeTag;

bool HTTPExitPointDelegate::s_didApplyRules = false;

HTTPExitPointDelegate::HTTPExitPointDelegate(InterceptionEngine* interceptEngine,
                                             TransactionMonitor* txMonitor)
    : AExitPointDelegate(interceptEngine, txMonitor)
{
    curlResourceType      = zend_fetch_list_dtor_id("curl");
    curlMultiResourceType = zend_fetch_list_dtor_id("curl_multi");

    boost::assign::push_back(m_displayNameSchema)
        (CONFIG_RULE_PROP_NAME,              " - ")
        (enum2str(HTTPExitPoint_HOST),   "http://")
        (enum2str(HTTPExitPoint_PORT),         ":")
        (enum2str(HTTPExitPoint_URL),           "")
        (enum2str(HTTPExitPoint_QUERY_STRING), "?");
}

void HTTPExitPointDelegate::configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                                      const struct _zend_agent_globals* agentGlobals)
{
    boost::shared_ptr<t_BackendRulesList> httpBackendRules(boost::make_shared<t_BackendRulesList>());
    httpBackendRules->CopyFrom(backendConfig.httpbackendconfig());
    AExitPointDelegate::configure(httpBackendRules, agentGlobals);
}

void HTTPExitPointDelegate::applyRules(const struct _zend_agent_globals* agentGlobals)
{
    if (s_didApplyRules)
        return;

    s_didApplyRules = true;
    m_interceptEngine->addInterceptorForCallable(CallableInfo(FOPEN_SYMBOL), &fopenInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(FILEGETCONTENTS_SYMBOL), &fileGetContentsInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_INIT_SYMBOL), &curlInitInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_SETOPT_SYMBOL), &curlSetoptInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_SETOPT_ARRAY_SYMBOL), &curlSetoptArrayInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_EXEC_SYMBOL), &curlExecInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_CLOSE_SYMBOL), &curlCloseInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_MULTI_ADD_HANDLE_SYMBOL), &curlMultiAddHandleInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_MULTI_REMOVE_HANDLE_SYMBOL), &curlMultiRemoveHandleInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_MULTI_EXEC_SYMBOL), &curlMultiExecInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_MULTI_SELECT_SYMBOL), &curlMultiSelectInterceptor, true);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(CURL_MULTI_INFO_READ_SYMBOL), &curlMultiInfoReadInterceptor, true);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(Drupal7::HTTP_REQUEST_SYMBOL), &drupal7HTTPRequestInterceptor);

    m_interceptEngine->addInterceptorForCallable(CallableInfo(Zend1::HTTP_CLIENT_SYMBOL, Zend1::HTTP_CLIENT_REQUEST_METHOD), &zend1HTTPClientRequestInterceptor);
    m_interceptEngine->addInterceptorForCallable(CallableInfo(Zend1::VARIEN_HTTP_CLIENT_SYMBOL, Zend1::HTTP_CLIENT_REQUEST_METHOD), &zend1HTTPClientRequestInterceptor);

#if PHP_VERSION_ID >= 50300
    m_interceptEngine->addInterceptorForCallable(CallableInfo(Zend2::HTTP_CLIENT_SYMBOL, Zend2::HTTP_CLIENT_SEND_METHOD), &zend2HTTPClientRequestInterceptor);
#endif
}

bool HTTPExitPointDelegate::processMatchRule(const PHPExecEnvironment* execEnv,
                                             const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                                             const StringMap& properties) const
{
    BOOST_ASSERT(matchRule.has_httpmatchrule());

    const appdynamics::pb::Agent::BackendMatchRule_HTTP& httpMatchRule =
        matchRule.httpmatchrule();
    auto notFound = properties.end();

    if (httpMatchRule.has_host()) {
        auto it = properties.find(enum2str(HTTPExitPoint_HOST));
        if (it == notFound)
            return false;
        StringMatch condition(httpMatchRule.host());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (httpMatchRule.has_port()) {
        auto it = properties.find(enum2str(HTTPExitPoint_PORT));
        if (it == notFound)
            return false;
        StringMatch condition(httpMatchRule.port());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (httpMatchRule.has_url()) {
        auto it = properties.find(enum2str(HTTPExitPoint_URL));
        if (it == notFound)
            return false;
        StringMatch condition(httpMatchRule.url());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }

    if (httpMatchRule.has_querystring()) {
        auto it = properties.find(enum2str(HTTPExitPoint_QUERY_STRING));
        if (it == notFound)
            return false;
        StringMatch condition(httpMatchRule.querystring());
        if (!condition.matchString(m_logger, execEnv, it->second))
            return false;
    }
    return true;
}

void HTTPExitPointDelegate::processNamingRule(const PHPExecEnvironment* execEnv,
                                              const appdynamics::pb::Agent::BackendRule& backendRule,
                                              const StringMap& properties,
                                              StringMap& identifyingProperties) const
{
    BOOST_ASSERT(backendRule.namingrule().has_httpnamingrule());
    const appdynamics::pb::Agent::BackendNamingRule_HTTP& httpNamingRule =
        backendRule.namingrule().httpnamingrule();
    Expression::Context context(execEnv, &properties);

    if (httpNamingRule.has_host()) {
        evaluateProperty(enum2str(HTTPExitPoint_HOST),
                         httpNamingRule.host(),
                         context,
                         identifyingProperties);
    }

    if (httpNamingRule.has_port()) {
        evaluateProperty(enum2str(HTTPExitPoint_PORT),
                         httpNamingRule.port(),
                         context,
                         identifyingProperties);
    }

    if (httpNamingRule.has_url()) {
        evaluateProperty(enum2str(HTTPExitPoint_URL),
                         httpNamingRule.url(),
                         context,
                         identifyingProperties);
    }

    if (httpNamingRule.has_querystring()) {
        evaluateProperty(enum2str(HTTPExitPoint_QUERY_STRING),
                         httpNamingRule.querystring(),
                         context,
                         identifyingProperties);
    }
}

bool AHTTPPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                           const CurrentExitCall* exitCall,
                                           StringMap& properties) const
{
    std::string url;

    if (!getURL(execEnv, url))
        return false;

    PHPUrl parsedURL(url);
    if (!parsedURL)
        return false;

    // If we don't get a non-null scheme that is http or https,
    // then this is not an http backend and we should ignore it.
    const char* scheme = parsedURL.scheme();
    bool hasScheme = scheme != NULL;
    bool isHTTP = hasScheme && strcasecmp(scheme, "http") == 0;
    bool isHTTPS = (!isHTTP) && hasScheme && (strcasecmp(scheme, "https") == 0);
    if ((!isHTTP) && (!isHTTPS)) {
        return false;
    }

    if (!parsedURL.host())
        return false;

    std::string host(parsedURL.host());
    if (host.size() > MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY)
        host.resize(MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY);
    properties[enum2str(HTTPExitPoint_HOST)] = host;

    unsigned short port(parsedURL.port());
    if (port == 0)
    {
        // if the port was not explicitly specified we have to figure it out!
        if (isHTTP)
            port = 80;
        else if (isHTTPS)
            port = 443;
    }
    properties[enum2str(HTTPExitPoint_PORT)] = boost::lexical_cast<std::string>(port);

    if (parsedURL.path()) {
        std::string path(parsedURL.path());
        // the URL property should not have ending slashes
        boost::trim_right_if(path, boost::is_from_range('/', '/'));
        if (path.size() > MAX_URL_LEN_IN_IDENTIFYING_PROPERTY)
            path.resize(MAX_URL_LEN_IN_IDENTIFYING_PROPERTY);
        if (path.empty())
            path = "/";
        properties[enum2str(HTTPExitPoint_URL)] = path;
    }

    if (parsedURL.query()) {
        properties[enum2str(HTTPExitPoint_QUERY_STRING)] = parsedURL.query();
    }

    return true;
}

bool getHTTPUrlFromStreamUrl(std::string& httpUrl, const std::string& streamUrl)
{
    httpUrl = streamUrl;
    return true;
}
