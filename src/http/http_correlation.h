#ifndef __http_correlation_h
#define __http_correlation_h

#include <php.h>
#include <ext/standard/php_fopen_wrappers.h>
#include <string>
#include <boost/mpl/if.hpp>

typedef boost::mpl::if_c< PHP_VERSION_ID >= 50600, const char*, char*>::type t_StreamCharType;

#if PHP_VERSION_ID >= 70000
typedef php_stream* (*t_phpURLStreamOpener)(php_stream_wrapper *wrapper, t_StreamCharType path, t_StreamCharType mode,
                                            int options, zend_string **opened_path, php_stream_context *context STREAMS_DC TSRMLS_DC);
php_stream* agentURLStreamOpener(php_stream_wrapper *wrapper, t_StreamCharType path, t_StreamCharType mode,
                                 int options, zend_string **opened_path, php_stream_context *context STREAMS_DC TSRMLS_DC);

#else
typedef php_stream* (*t_phpURLStreamOpener)(php_stream_wrapper *wrapper, t_StreamCharType path, t_StreamCharType mode,
                                            int options, char **opened_path, php_stream_context *context STREAMS_DC TSRMLS_DC);
php_stream* agentURLStreamOpener(php_stream_wrapper *wrapper, t_StreamCharType path, t_StreamCharType mode,
                                 int options, char **opened_path, php_stream_context *context STREAMS_DC TSRMLS_DC);
#endif


/**
 * This structure keeps track of the correlation info to be propagated on the
 * next HTTP-wrappers based exit call.
 *
 * captureURLStreamOpener can be called repeatedly (for every exit call).
 */
struct HTTPCorrelationContext
{
    t_phpURLStreamOpener realURLStreamOpener;
    std::string correlationHeader;

    HTTPCorrelationContext()
        : realURLStreamOpener(NULL)
    { }

    inline void captureURLStreamOpener()
    {
        if (!realURLStreamOpener) {
            realURLStreamOpener = php_stream_http_wrapper.wops->stream_opener;
            php_stream_http_wrapper.wops->stream_opener = agentURLStreamOpener;
        }
    }
};

#endif // __http_correlation_h
