/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */
#ifndef __HTTPENTRYPOINT_H
#define __HTTPENTRYPOINT_H

#include "entrypoint.h"
#include "zval_helper.h"
#include "naming.h"
#include "util.h"
#include "name_value_pair.h"
#include "correlation.h"

#include <string>
#include <boost/utility.hpp>

class PHPExecEnvironment;

class HTTPEntryPointInterceptor : public AEntryPointInterceptor {
    public:
        HTTPEntryPointInterceptor();
        ~HTTPEntryPointInterceptor();
        virtual boost::shared_ptr<IMatchPointPayload>createPayload(const PHPExecEnvironment* execEnv);
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() const;
    protected:
        void detectErrors(const PHPExecEnvironment* phpExecEnv, void* state);
        bool isCorrelationHeaderPresent(const PHPExecEnvironment *execEnv) const;
    private:
        IServerEnvironment* server_env;
};


class HTTPEntryPointDelegate : public AEntryPointDelegate {
    public:
        HTTPEntryPointDelegate(InterceptionEngine* interceptEngine,
                               TransactionMonitor* txMonitor);
        ~HTTPEntryPointDelegate();

    protected:
        virtual void configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                               const struct _zend_agent_globals* agentGlobals);
        virtual void initAcceptor();
        virtual void applyRules(const struct _zend_agent_globals* agentGlobals);

    private:
        static bool s_didApplyRules;
};

class HTTPTransactionAcceptor : public ATransactionAcceptor {
    public:
        HTTPTransactionAcceptor(const boost::shared_ptr<MatchPointConfig>& config,
                                TransactionMonitor* txMonitor)
            : ATransactionAcceptor(config, txMonitor)
        {
        }

        virtual std::string getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& iPayload);

        virtual bool matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& payload,
                                          const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition);
        virtual bool matchRepeatedGetPostKeyValue(const PHPExecEnvironment* execEnv,
                                                  const ::google::protobuf::RepeatedPtrField< ::appdynamics::pb::Agent::KeyValueMatch >* matchDictionary,
                                                  ZValPointerArray* payloadGetMap,
                                                  ZValPointerArray* payloadPostMap,
                                                  const boost::shared_ptr<MatchPointConfig>& config,
                                                  const AgentLogger& logger);
        virtual bool customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                            const boost::shared_ptr<IMatchPointPayload>& payload);
    protected:
        virtual appdynamics::pb::Agent::EntryPointType getEntryPointType() { return appdynamics::pb::Agent::PHP_WEB; }
        static bool matchHTTPHeader(const ::google::protobuf::RepeatedPtrField< ::appdynamics::pb::Agent::KeyValueMatch >* matchDictionary,
                                    const boost::shared_ptr<HTTPPayload>& payload,
                                    const boost::shared_ptr<MatchPointConfig>& config,
                                    const AgentLogger& logger);
        virtual boost::shared_ptr<CorrelationHeader>
            checkForContinuingTransaction(const boost::shared_ptr<IMatchPointPayload>& iPayload);
};

/**
    IMatchPointPayload implementation created by the
    HTTPEntryPointInterceptor.
 */
class HTTPPayload : public IMatchPointPayload {
    public:
        HTTPPayload(const PHPExecEnvironment* execEnv,
                    const AgentLogger& logger)
            : m_execEnv(execEnv)
            , m_logger(logger)
            , m_isHTTPs(false)
            , m_didGetMethod(false)
            , m_didGetURL(false)
            , m_didGetURLWithQuery(false)
            , m_didGetCookies(false)
            , m_didGetGetParams(false)
            , m_didGetPostParams(false)
            , m_didGetHost(false)
            , m_didGetPort(false)
            , m_didGetReferrer(false)
            , m_didGetHTTPS(false)
            , m_didGetCorrelationHeader(false)
            , m_didGetSessionArray(false)
        {
        }

        /**
            @return The value of the $_SERVER superglobal.
         */
        ZValPointerArray getServerArray() const;

        const std::string& getMethod() const;

        /**
            @return The URL of the current request, without the query
            string.
         */
        const std::string& getURL() const;

        /**
            @return The URL of the current request, including the query
            string.
         */
        const std::string& getURLWithQuery() const;

        /**
            @return The value of the $_COOKIE superglobal.
         */
        ZValPointerArray getCookies() const;

        /**
            @return The value of the $_GET superglobal.
         */
        ZValPointerArray getGetParams() const;

        /**
            @return The value of the $_POST superglobal.
         */
        ZValPointerArray getPostParams() const;

        /**
            @return The value of $_SERVER['SERVER_NAME']
         */
        const std::string& getHost() const;

        /**
            @return The value of $_SERVER['SERVER_PORT']
         */
        const std::string& getPort() const;

        const PHPExecEnvironment* getExecEnv() const;

        /**
            @return The value of $_SERVER['HTTP_REFERER']
         */
        const std::string& getReferrer() const;

        /**
            Retrieves the value of an HTTP header with a specified name.
            @param headerName Name of the header whose value should be retrieved
            @param Out parameter which receives the value of the header with the specified
            name.
            @return true If there is an HTTP header with the specified name,
            false otherwise.
         */
        bool getHeaderValue(const std::string& headerName, std::string* headerValue) const;

        /**
            @return true if the current request is using https, false otherwise.
         */
        bool isHTTPs() const;

        inline const PHPExecEnvironment* getExecEnvironment() const { return m_execEnv; }

        inline const std::string& getIncomingCorrelationHeader() const;

        /**
            Sets the sessionId argument to the current session ID.
            @return true iff the session is loaded and we can find its globals.
        */
        bool getSessionId(std::string* sessionId) const;

        /**
            @return The value of the $_SESSION superglobal.
         */
        ZValPointerArray getSessionArray() const;

    private:
        const PHPExecEnvironment* const m_execEnv;
        AgentLogger const m_logger;
        mutable std::string m_method;
        mutable std::string m_url;
        mutable std::string m_host;
        mutable std::string m_port;
        mutable std::string m_urlWithQuery;
        mutable SafeModuleShutdownWrapper<ZValPointerAny> m_cookiesArray;
        mutable SafeModuleShutdownWrapper<ZValPointerAny> m_getParamsArray;
        mutable SafeModuleShutdownWrapper<ZValPointerAny> m_postParamsArray;
        mutable std::string m_referrer;
        mutable bool m_isHTTPs;
        mutable std::string m_incomingCorrelationHeader;
        mutable SafeModuleShutdownWrapper<ZValPointerAny> m_sessionArray;

        mutable bool m_didGetMethod : 1;
        mutable bool m_didGetURL : 1;
        mutable bool m_didGetURLWithQuery : 1;
        mutable bool m_didGetCookies : 1;
        mutable bool m_didGetGetParams : 1;
        mutable bool m_didGetPostParams : 1;
        mutable bool m_didGetHost : 1;
        mutable bool m_didGetPort : 1;
        mutable bool m_didGetReferrer : 1;
        mutable bool m_didGetHTTPS : 1;
        mutable bool m_didGetCorrelationHeader : 1;
        mutable bool m_didGetSessionArray : 1;
};

#endif /* __HTTPENTRYPOINT_H */
