#include "http/curl_exitpoint.h"
#include "zval_helper.h"
#include "emalloc_allocator.h"
#include "php_url.h"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>

/*
 * TODO
 * - Intercept curl_copy_handle()
 */

/* {{{ PHP Curl structure imports */

/*
 * These structures are here so that we can access CURL *cp handle based on the
 * PHP curl resource and pass it to curl_getinfo() API. They're taken from
 * ext/curl/php_curl.h file, which unfortunately is not available as a public
 * header when PHP development headers are installed.  We're reasonably sure
 * that php_curl structure will not change between minor PHP versions.
 *
 * The fallback is to use zend_call_function() to invoke curl_getinfo() PHP
 * wrapper. If the overhead proves too big, we may construct a simplified
 * calling environment ourselves and invoke the handler directly.
 */

// no difference between PHP 5 and 7
struct _php_curl_error  {
    char str[CURL_ERROR_SIZE + 1];
    int  no;
};

struct _php_curlm {
    int    still_running;
    CURLM *multi;
    zend_llist easyh;
};

// the php_curl struct changed, as well as curl_php_send_headers
#if PHP_VERSION_ID >= 70000
struct _php_curl_send_headers {
    zend_string* str;
};
struct _php_curl {
    CURL                         *cp;
    void                         *handlers;
    zend_resource                *res;
    void                         *to_free;
    struct _php_curl_send_headers header;
    struct _php_curl_error        err;
    /* rest is omitted */
};

#else

struct _php_curl_send_headers {
    char *str;
    size_t str_len;
};
struct _php_curl {
    struct _php_curl_error   err;
    void                     *to_free;
    struct _php_curl_send_headers header;
    /* rest is omitted */
};
#endif

/* }}} */

const AHandleInfo::TypeTag CurlHandleInfo::typeTag;
const AHandleInfo::TypeTag CurlMultiHandleInfo::typeTag;

namespace {

    static const char CURL_MULTI_INFO_READ_SYMBOL[] = "curl_multi_info_read";
    static const char CURL_SETOPT_SYMBOL[] = "curl_setopt";

    /*
     * These values will be constant for the lifetime of the process
     * across all threads because it is defined in a PHP extension
     * which can not change its value, nor have a per thread value.
     */
    static long curlOptUrlConst = -1;
    static long curlOptHttpHeaderConst = -1;

    static bool isSameCurlMsgHandle(const ZValPointerAny& message, const ZValPointerResource& handle)
    {
        ZValPointerResource doneHandle(message.cast<ZValPointerArray>().findEntryByName<ZValPointerResource>("handle", strlen("handle")));
        return (doneHandle && doneHandle.isIdentical(handle));
    }

    void replaceCurlHeaders(const ZValPointerResource& curlHandle,
                            const ZValPointerArray& newHeaders,
                            const PHPExecEnvironment* execEnv)
    {
        ZValPointerString callable(ZValPointerString::copy(CURL_SETOPT_SYMBOL));
        ZValPointerAny returnValue;
        ZValPointerLong option(ZValPointerLong::create(curlOptHttpHeaderConst));
        std::vector<ZValPointerAny> callableParams = boost::assign::list_of<ZValPointerAny>(curlHandle)(option)(newHeaders);

        execEnv->callCallable(&returnValue, callable, &callableParams);
    }
}

static boost::shared_ptr<AErrorObject> detectCurlError(const php_curl* curl,
                                                       CurrentExitCall* currentExitCall,
                                                       ErrorMonitor* errorMonitor,
                                                       const PHPExecEnvironment* execEnv)
{
    if (!curl)
        return boost::shared_ptr<AErrorObject>();

    if (curl->err.str[0] == '\0')
        return boost::shared_ptr<AErrorObject>();


    const std::string errorMessage(curl->err.str);
    if (currentExitCall)
        currentExitCall->incrementErrorCount(1);
    return errorMonitor->reportSyntheticException(execEnv, "Curl", errorMessage);
}

/* {{{ CurlMultiHandleInfo methods */

void CurlMultiHandleInfo::addCurlHandle(const boost::shared_ptr<CurlHandleInfo>& handleInfo)
{
    if (!handleInfo)
        return;

    m_curlHandleInfos.push_back(handleInfo);
}

void CurlMultiHandleInfo::removeCurlHandle(const boost::shared_ptr<CurlHandleInfo>& handleInfo)
{
    if (!handleInfo)
        return;

    unsigned index = 0;
    const php_curl* const curl = handleInfo->getPhpCurl();
    for (auto it = m_curlHandleInfos.begin(); it != m_curlHandleInfos.end(); ++it) {
        if ((*it)->getPhpCurl() == curl) {
            index = it - m_curlHandleInfos.begin();
            m_curlHandleInfos.erase(it);
            if (!m_curlExitCalls.empty()) {
                m_curlExitCalls.erase(m_curlExitCalls.begin() + index);
            }
            break;
        }
    }

/* In PHP 7, when we call the erase method below, the curl message array in
 * m_curlMessageList gets deleted, but with it also every element in the array
 * gets destroyed. In PHP 5, every element is destroyed if the hashtable/array was
 * initialized with a valid pointer to a destructor, which is not the case for Curl.

 * TODO: Not deleting the curl message array therefore may be the cause of a memory leak!
 */
#if PHP_VERSION_ID < 70000
    /*
     * We also need to remove any messages corresponding to this handle from
     * the message list.
     */
    if (!m_curlMessageList.empty()) {
        m_curlMessageList.erase(remove_if(m_curlMessageList.begin(),
                                          m_curlMessageList.end(),
                                          boost::bind(isSameCurlMsgHandle, _1, handleInfo->getResource())),
                                m_curlMessageList.end());
    }
#endif
}

/* }}} */

/* {{{ CurlCurrentExitCall methods */
void CurlCurrentExitCall::startExitCall()
{
    m_startTimestamp = AgentKernel::getTimer()->getTimestamp();
}

uint64_t CurlCurrentExitCall::endExitCall()
{
    if (m_done)
        return m_elapsedTimeInUs;

    const Timer* timer = AgentKernel::getTimer();
    m_elapsedTimeInUs += timer->getElapsedTime(m_startTimestamp);
    return m_elapsedTimeInUs;
}

/* }}} */

/* {{{ class CurlPropertyGenerator and methods */

/*
 * An HTTP backend resolver that obtains the URL from the exit call specified in
 * getExitCallInfo().
 *
 * We have to go through these contortions because BackendResolver's
 * initializeExitCallInfo() does not take a CurrentExitCall. A better way would
 * be to have an overloaded version of it that acted purely on CurrentExitCall
 * to create ExitCallInfo from.
 */
class CurlPropertyGenerator : public AHTTPPropertyGenerator {
    public:
        CurlPropertyGenerator() : m_exitCall(NULL) { }

        bool getProperties(const PHPExecEnvironment* execEnv,
                           const CurrentExitCall* exitCall,
                           StringMap& properties) const;

        virtual bool getURL(const PHPExecEnvironment* execEnv,
                            std::string& url) const;
    private:
        mutable const HTTPCurrentExitCall* m_exitCall;
};

static CurlPropertyGenerator curlPropertyGenerator;


bool CurlPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                          const CurrentExitCall* exitCall,
                                          StringMap& properties) const
{
    m_exitCall = static_cast<const HTTPCurrentExitCall*>(exitCall);
    return AHTTPPropertyGenerator::getProperties(execEnv, exitCall, properties);
}

/**
 * Provides the URL to initializeExitCallInfo().
 */
bool CurlPropertyGenerator::getURL(const PHPExecEnvironment* execEnv,
                                   std::string& url) const
{
    if (m_exitCall) {
        url = m_exitCall->getURL();
        return true;
    }

    return false;
}

/* }}} */

/* {{{ ACurlURLCaptureInterceptor methods */

/*
 * Nothing to do in onCallableBegin(), because the capture is always done in
 * onCallableEnd() to unify the handle usage among derived classes.
 */
void* ACurlURLCaptureInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void ACurlURLCaptureInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                               void* state)
{
    std::string url;
    ZValPointerAny handle;
    ZValPointerAny headers;
    bool gotURL = false;
    bool gotHeaders = false;

    /*
     * Cache the value of the CURLOPT_URL constant for use in derived
     * interceptors.
     */
    if (curlOptUrlConst == -1) {
        static const char CURLOPT_URL[] = "CURLOPT_URL";
        static const char CURLOPT_HTTPHEADER[] = "CURLOPT_HTTPHEADER";
        execEnv->getConstantAsLong(CURLOPT_URL, strlen(CURLOPT_URL), &curlOptUrlConst);
        execEnv->getConstantAsLong(CURLOPT_HTTPHEADER, strlen(CURLOPT_HTTPHEADER), &curlOptHttpHeaderConst);
    }

    /*
     * Try to find the URL or the headers first.
     * If unsuccessful, there is no need to go further.
     */
    gotURL = getURL(url, execEnv);
    gotHeaders = getHeaders(&headers, execEnv);
    if (!gotURL && !gotHeaders)
        return;

    if (!getHandle(&handle, execEnv)) {
        LOG4CXX_WARN(m_logger, "could not obtain CURL handle from the calling environment");
        return;
    }

    /*
     * Look up the CurlHandleInfo. If found, the URL must have been specified
     * previously via one of the curl_* functions, so we just overwrite it.
     * Otherwise, create a new handle info.
     */
    boost::shared_ptr<CurlHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(handle));
    if (!handleInfo) {
        handleInfo = CurlHandleInfo::create(handle.cast<ZValPointerResource>());
        execEnv->getHandleRegistry().registerHandle(handle, handleInfo);
    }

    if (gotURL)
        handleInfo->setURL(url);
    if (gotHeaders)
        handleInfo->setHeaders(headers);
}

/* }}} */

/* {{{ CurlInitInterceptor methods */

bool CurlInitInterceptor::getHandle(ZValPointerAny* handlePointer,
                                    const PHPExecEnvironment* execEnv) const
{
    // FIXME return_value might be false if curl_init() fails
    *handlePointer = ZValPointerAny::share(execEnv->getReturnValue());

    if (!(*handlePointer))
        return false;

    return true;
}

static inline std::string extractHostFromPath(const char* path)
{
    const char* firstSlash = strchr(path, '/');
    if (!firstSlash)
        return std::string(path);
    size_t hostLength = firstSlash - path;
    if (!hostLength)
        return std::string();
    return std::string(path, hostLength);
}

static inline std::string mungeURLForCurl(const std::string& url)
{
    PHPUrl parsedURL(url);

    if (!parsedURL)
        return url;

    const bool hasScheme = parsedURL.scheme() != NULL;
    if (hasScheme)
        return url;

    const bool hasHost = parsedURL.host();
    const bool hasPath = parsedURL.path();
    if ((!hasHost) && (!hasPath)) {
        // No host and no path, just give up....
        return url;
    }

    const std::string hostName(hasHost ? std::string(parsedURL.host()) : extractHostFromPath(parsedURL.path()));

    if (hostName.empty())
        return url;

    const std::locale& classicLocale = std::locale::classic();

    if (boost::algorithm::istarts_with(hostName, "FTP.", classicLocale))
        return url;

    if (boost::algorithm::istarts_with(hostName, "DICT.", classicLocale))
        return url;

    if (boost::algorithm::istarts_with(hostName, "LDAP.", classicLocale))
        return url;

    if (boost::algorithm::istarts_with(hostName, "IMAP.", classicLocale))
        return url;

    if (boost::algorithm::istarts_with(hostName, "SMTP.", classicLocale))
        return url;

    if (boost::algorithm::istarts_with(hostName, "POP3.", classicLocale))
        return url;

    return std::string("http://") + url;
}

bool CurlInitInterceptor::getURL(std::string& url,
                                 const PHPExecEnvironment* execEnv) const
{
    ZValPointerString urlParamZVal(execEnv->getParam<ZValPointerString, true>(0));
    if (!urlParamZVal)
        return false;
    url = mungeURLForCurl(urlParamZVal.getStringValue());
    return true;
}

bool CurlInitInterceptor::getHeaders(ZValPointerAny* headers,
                                     const PHPExecEnvironment* execEnv) const
{
    // nothing to do
    return false;
}

bool CurlInitInterceptor::needReturnValueInOnMethodEnd() const
{
    return true;
}

/* }}} */

/* {{{ CurlSetoptInterceptor methods */

bool CurlSetoptInterceptor::getHandle(ZValPointerAny* handlePointer,
                                      const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 3)
        return false;

    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return false;

    *handlePointer = curlHandle;
    return true;
}

bool CurlSetoptInterceptor::getURL(std::string& url,
                                   const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 3)
        return false;

    ZValPointerLong option(execEnv->getParam<ZValPointerLong>(1));
    // Check for CURLOPT_URL option
    if (!option || option.getLongValue() != curlOptUrlConst)
        return false;

    ZValPointerString value(execEnv->getParam<ZValPointerString>(2));
    if (!value)
        return false;

    url = mungeURLForCurl(value.getStringValue());
    return true;
}

bool CurlSetoptInterceptor::getHeaders(ZValPointerAny* headers,
                                     const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 3)
        return false;

    ZValPointerLong option(execEnv->getParam<ZValPointerLong>(1));
    // Check for CURLOPT_HTTPHEADER option
    if (!option || option.getLongValue() != curlOptHttpHeaderConst)
        return false;

    ZValPointerArray value(execEnv->getParam<ZValPointerArray>(2));
    if (!value)
        return false;

    *headers = value;
    return true;
}

bool CurlSetoptInterceptor::needReturnValueInOnMethodEnd() const
{
    return false;
}

/* }}} */

/* {{{ CurlSetoptArrayInterceptor methods */
bool CurlSetoptArrayInterceptor::getHandle(ZValPointerAny* handlePointer,
                                           const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 2)
        return false;

    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return false;

    *handlePointer = curlHandle;
    return true;
}

bool CurlSetoptArrayInterceptor::getURL(std::string& url,
                                        const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 2)
        return false;

    ZValPointerArray options(execEnv->getParam<ZValPointerArray>(1));
    if (!options)
        return false;

    /*
     * Try to find CURLOPT_URL option in the option array.
     */
    ZValPointerString value(options.findEntryByLong<ZValPointerString>(curlOptUrlConst));
    if (!value)
        return false;

    url = mungeURLForCurl(value.getStringValue());
    return true;
}

bool CurlSetoptArrayInterceptor::getHeaders(ZValPointerAny* headers,
                                            const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 2)
        return false;

    ZValPointerArray options(execEnv->getParam<ZValPointerArray>(1));
    if (!options)
        return false;

    /*
     * Try to find CURLOPT_HTTPHEADER option in the option array.
     */
    ZValPointerArray value(options.findEntryByLong<ZValPointerArray>(curlOptHttpHeaderConst));
    if (!value)
        return false;

    *headers = value;
    return true;
}

bool CurlSetoptArrayInterceptor::needReturnValueInOnMethodEnd() const
{
    return true;
}
/* }}} */

/* {{{ CurlExecInterceptor methods */

CurlExecInterceptor::CurlExecInterceptor()
    : t_Base("CurlExecInterceptor",
             InterceptorRegistry::CurlExecInterceptor_ID,
             appdynamics::pb::Agent::EXIT_HTTP)
{
}

CurrentExitCall* CurlExecInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                            uint64_t sequenceNumber) const
{
    if (execEnv->getParamCount() < 1)
        return NULL;

    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return NULL;

    boost::shared_ptr<CurlHandleInfo> handleInfo = execEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(curlHandle);
    if (!handleInfo)
        return NULL;

    CurlCurrentExitCall* exitCall = new CurlCurrentExitCall(sequenceNumber);
    exitCall->setURL(handleInfo->getURL());
    return exitCall;
}

ExitCallInfo CurlExecInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                   const PHPExecEnvironment* execEnv) const
{
    BOOST_ASSERT(exitCall);
    return AG(kernel)->getAgentContext()
                     ->getTransactionMonitor()
                     ->getExitPointDelegate(getExitPointType())
                     ->makeIdentifyingProperties(execEnv, exitCall, curlPropertyGenerator);
}

void CurlExecInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                    const PHPExecEnvironment* execEnv,
                                                    const boost::shared_ptr<SnapshotExitCall>& sec) const
{
    const CurlCurrentExitCall* curlExitCall = static_cast<const CurlCurrentExitCall*>(exit_call);
    sec->setDetailString(curlExitCall->getURL());
    sec->addProperty("function", "curl_exec");
    sec->addProperties(exit_call->getSnapshotDebugProperties());
}

boost::shared_ptr<AErrorObject>
CurlExecInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                  const PHPExecEnvironment* execEnv)
{
    boost::shared_ptr<AErrorObject> errorObject(AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, "Curl", execEnv));

    if (errorObject)
        return errorObject;

    if (execEnv->getParamCount() < 1)
        return boost::shared_ptr<AErrorObject>();

    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return boost::shared_ptr<AErrorObject>();

    const php_curl* curl = getPhpCurlStructFromResource(curlHandle);
    ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
    return detectCurlError(curl, currentExitCall, errorMonitor, execEnv);
}

void CurlExecInterceptor::emitCorrelationInfo(TransactionContext* context,
                                              CurrentExitCall* exitCall,
                                              const PHPExecEnvironment* execEnv) const
{
    if (execEnv->getParamCount() < 1)
        return;

    // Curl handle is the first parameter to curl_exec()
    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return;

    boost::shared_ptr<CurlHandleInfo> handleInfo = execEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(curlHandle);
    if (!handleInfo)
        return;

    std::string corrHeaderValue(TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
    exitCall->addSnapshotDebugProperty("Correlation Header", corrHeaderValue);

    ZValPointerArray currentHeaders((handleInfo->getHeaders()).cast<ZValPointerArray>());
    std::string header;
    TransactionCorrelator::makeFullHeader(header, corrHeaderValue);

    if (!currentHeaders) {
      ZValPointerArray newHeaders = ZValPointerArray::create();
      newHeaders.pushEntry(header);
      replaceCurlHeaders(curlHandle, newHeaders, execEnv);
    } else {
      currentHeaders.pushEntry(header);
      replaceCurlHeaders(curlHandle, currentHeaders, execEnv);
    }
}

void CurlExecInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
{
    if (execEnv->getParamCount() < 1)
        return;

    // Curl handle is the first parameter to curl_exec()
    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return;

    boost::shared_ptr<CurlHandleInfo> handleInfo = execEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(curlHandle);
    if (!handleInfo)
        return;

    ZValPointerArray origHeaders((handleInfo->getHeaders()).cast<ZValPointerArray>());
    if (origHeaders) {
        replaceCurlHeaders(curlHandle, origHeaders, execEnv);
    }

    t_Base::onCallableEnd(execEnv, state);
}

/* }}} */

/* {{{ CurlCloseInterceptor methods */

void* CurlCloseInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void CurlCloseInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                         void* state)
{
    if (execEnv->getParamCount() < 1)
        return;

    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return;

    execEnv->getHandleRegistry().unregisterHandle(curlHandle);
}

/* }}} */

/* {{{ CurlMultiAddHandleInterceptor methods */

void* CurlMultiAddHandleInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void CurlMultiAddHandleInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                                  void* state)
{
    // Only consider 0L as the success value
    ZValPointerLong returnVal(ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerLong>());
    if (!returnVal)
        return;
    if (returnVal.getLongValue() != 0)
        return;

    if (execEnv->getParamCount() < 2)
        return;

    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(1));

    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));
    if (!handleInfo) {
        handleInfo = CurlMultiHandleInfo::create();
        execEnv->getHandleRegistry().registerHandle(multiHandle, handleInfo);
    }

    boost::shared_ptr<CurlHandleInfo> curlHandleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(curlHandle));
    if (!curlHandleInfo) {
        // somehow this escaped our interceptors and didn't get registered, so just abort
        return;
    }

    handleInfo->addCurlHandle(curlHandleInfo);
}

/* }}} */

/* {{{ CurlMultiRemoveHandleInterceptor methods */

void* CurlMultiRemoveHandleInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void CurlMultiRemoveHandleInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                                     void* state)
{
    // Only consider 0L as the success value
    ZValPointerLong returnVal(ZValPointerAny::share(execEnv->getReturnValue()).cast<ZValPointerLong>());
    if (!returnVal)
        return;
    if (returnVal.getLongValue() != 0)
        return;

    if (execEnv->getParamCount() < 2)
        return;

    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    ZValPointerResource curlHandle(execEnv->getParam<ZValPointerResource>(1));
    if (!multiHandle || !curlHandle)
        return;

    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));
    if (!handleInfo)
        return;

    boost::shared_ptr<CurlHandleInfo> curlHandleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(curlHandle));
    if (!curlHandleInfo) {
        // somehow this escaped our interceptors and didn't get registered, so just abort
        return;
    }

    handleInfo->removeCurlHandle(curlHandleInfo);
}

/* }}} */

/* {{{ CurlMultiExecInterceptor methods */

void* CurlMultiExecInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* txContext = txMonitor->getTransactionContext();

    // txContext can be NULL if the current transaction is
    // "excluded", or if the agent in a REGISTERED state.
    if (!txContext)
        return NULL;

    if (execEnv->getParamCount() < 2)
        return NULL;

    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!multiHandle)
        return NULL;

    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));
    if (!handleInfo)
        return NULL;

    boost::ptr_vector<CurlCurrentExitCall>& exitCalls = handleInfo->getExitCalls();
    const std::vector<boost::shared_ptr<CurlHandleInfo>>& curlHandleInfos = handleInfo->getCurlHandleInfos();
    size_t const nHandleInfos = curlHandleInfos.size();
    size_t const nExitCalls = exitCalls.size();

    if (nExitCalls < nHandleInfos) {
        exitCalls.reserve(curlHandleInfos.size());
        for (unsigned i = 0; i < nExitCalls; ++i) {
            CurlCurrentExitCall& existingExitCall = exitCalls[i];
            existingExitCall.startExitCall();
        }

        ExitCallRegistry* exitCallRegistry = txMonitor->getTransactionRegistry()->getExitCallRegistry();

        // TODO mark exit calls started within the scope of another
        // exit call, so that we don't report metrics for them in onCallableEnd.
        for (unsigned i = nExitCalls; i < nHandleInfos; ++i) {
            uint64_t const nextExitCallSequenceNumber =
                txContext->getExitCallCounter() + 1;
            CurlCurrentExitCall* curlExitCall =
                new CurlCurrentExitCall(nextExitCallSequenceNumber);
            boost::shared_ptr<CurlHandleInfo> const entry(curlHandleInfos[i]);
            curlExitCall->setHandleInfo(entry);
            curlExitCall->setURL(entry->getURL());

            ExitCallInfo exitCallInfo = makeExitCallInfo(curlExitCall, execEnv);
            if (exitCallInfo.isValid()) {
                curlExitCall->setExitCallInfo(exitCallInfo);
                exitCallRegistry->resolveExitCall(curlExitCall);
                txContext->incrementExitCallCounter();
                setupCorrelation(txContext, curlExitCall, execEnv);
                curlExitCall->startExitCall();
            }
            exitCalls.push_back(curlExitCall);
        }
    } else {
        BOOST_ASSERT(nExitCalls == nHandleInfos);
        BOOST_FOREACH(auto& existingExitCall, exitCalls) {
            existingExitCall.startExitCall();
        }
    }

    return &exitCalls;
}

void CurlMultiExecInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                             void* state)
{
    // if state is NULL, something went wrong in onCallableBegin
    if (!state)
        return;

    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));

    boost::ptr_vector<CurlCurrentExitCall>& curlExitCalls = handleInfo->getExitCalls();
    BOOST_FOREACH(auto& exitCall, curlExitCalls) {
        exitCall.endExitCall();
    }

    boost::shared_ptr<TransactionMonitor> txMonitor =
        AG(kernel)->getAgentContext()->getTransactionMonitor();
    TransactionContext* txContext = txMonitor->getTransactionContext();
    // This null check is not really needed, because state ( which we check above ) should
    // be null if txContext is null.  However, having this null check here makes this
    // code easier to understand.
    if (!txContext)
        return;

    BOOST_ASSERT(txContext);


    int msgsLeft;
    CURLMsg* msg;
    std::list<ZValPointerAny>& msgList = handleInfo->getCurlMessageList();
    const std::vector<boost::shared_ptr<CurlHandleInfo>>& curlHandleInfos = handleInfo->getCurlHandleInfos();

    ErrorMonitor* errorMonitor = txMonitor->getErrorMonitor();

    ZValPointerAny callable;
    APPD_ZVAL_STRING_DUP_TRUE(callable.get(), const_cast<char*>(CURL_MULTI_INFO_READ_SYMBOL));
    ZValPointerAny returnValue;

    std::vector<ZValPointerAny> callableParams;
    callableParams.push_back(multiHandle);

    while (true) {
        execEnv->callCallable(&returnValue, callable, &callableParams);

        // We only accept arrays as successful return value.
        if (returnValue.getType() != ZValPointer::ZVal_Array)
            break;

        msgList.push_back(returnValue);

        // Currently the only valid value for 'msg' field is CURLMSG_DONE, so
        // we don't check for it. This may change in the future though.

        // Get the completed handle out of the 'handle' field. If invalid, keep
        // polling curl for the next entry.
        ZValPointerResource doneHandle(returnValue.cast<ZValPointerArray>().findEntryByName<ZValPointerResource>("handle", strlen("handle")));
        if (!doneHandle)
            continue;

        for (auto it = curlHandleInfos.begin(); it != curlHandleInfos.end(); ++it) {
            const ZValPointerResource& curlHandle = (*it)->getResource();
            if (!curlHandle)
                continue;
            if (!curlHandle.isIdentical(doneHandle))
                continue;
            const unsigned index = it - curlHandleInfos.begin();
            BOOST_ASSERT(index < curlExitCalls.size());
            CurlCurrentExitCall& exitCall = curlExitCalls[index];
            if (!exitCall.getExitCallInfo())
                continue;

            exitCall.markDone();
            txContext->reportExitCall(&exitCall);
            boost::shared_ptr<AErrorObject> errorObject(detectCurlError((*it)->getPhpCurl(), &exitCall, errorMonitor, execEnv));
            populateSnapshot(txContext, &exitCall, errorObject, execEnv);
        }
    }
}

void CurlMultiExecInterceptor::emitCorrelationInfo(TransactionContext* context,
                                                   CurrentExitCall* exitCall,
                                                   const PHPExecEnvironment* execEnv) const
{
    boost::shared_ptr<CurlHandleInfo> handleInfo(static_cast<CurlCurrentExitCall*>(exitCall)->getHandleInfo());
    if (!handleInfo)
        return;

    context->setCurrentExitCall(exitCall);
    std::string corrHeaderValue(TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
    exitCall->addSnapshotDebugProperty("Correlation Header", corrHeaderValue);
    context->setCurrentExitCall(NULL);

    ZValPointerArray currentHeaders((handleInfo->getHeaders()).cast<ZValPointerArray>());
    std::string header;
    TransactionCorrelator::makeFullHeader(header, corrHeaderValue);

    if (!currentHeaders) {
      ZValPointerArray newHeaders = ZValPointerArray::create();
      newHeaders.pushEntry(header);
      replaceCurlHeaders(handleInfo->getResource(), newHeaders, execEnv);
    } else {
      currentHeaders.pushEntry(header);
      replaceCurlHeaders(handleInfo->getResource(), currentHeaders, execEnv);
    }
}

ExitCallInfo CurlMultiExecInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                        const PHPExecEnvironment* execEnv) const
{
    BOOST_ASSERT(exitCall);
    return AG(kernel)->getAgentContext()
                     ->getTransactionMonitor()
                     ->getExitPointDelegate(getExitPointType())
                     ->makeIdentifyingProperties(execEnv, exitCall, curlPropertyGenerator);
}

void CurlMultiExecInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                         const PHPExecEnvironment* execEnv,
                                                         const boost::shared_ptr<SnapshotExitCall>& sec) const
{
    const CurlCurrentExitCall* curlExitCall = static_cast<const CurlCurrentExitCall*>(exit_call);
    sec->setDetailString(curlExitCall->getURL());
    sec->addProperty("function", "curl_multi_exec");
    sec->addProperties(exit_call->getSnapshotDebugProperties());
}

CurlMultiExecInterceptor::CurlMultiExecInterceptor()
    : t_Base("CurlMultiExecInterceptor",
             InterceptorRegistry::CurlMultiExecInterceptor_ID,
             appdynamics::pb::Agent::EXIT_HTTP)
{
}

void* CurlMultiSelectInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    TransactionContext* txContext =
        AG(kernel)->getAgentContext()->getTransactionMonitor()->getTransactionContext();

    // If there is no current transaction or if it is not
    // yet registered, then ignore the exit calls because
    // we will never report metrics for them anyway.
    //
    if (!txContext)
        return NULL;

    if (execEnv->getParamCount() < 1)
        return NULL;

    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    if (!multiHandle)
        return NULL;

    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));
    if (!handleInfo)
        return NULL;

    boost::ptr_vector<CurlCurrentExitCall>& exitCalls = handleInfo->getExitCalls();
    BOOST_FOREACH(auto& exitCall, exitCalls) {
        exitCall.startExitCall();
    }
    return &exitCalls;
}

void CurlMultiSelectInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                               void* state)
{
    // if state is NULL, something went wrong in onCallableBegin
    if (!state)
        return;

    PHPExecEnvironment* phpEnv = AG(G)->exec_env.get();
    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(phpEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));

    boost::ptr_vector<CurlCurrentExitCall>& curlExitCalls = handleInfo->getExitCalls();
    BOOST_FOREACH(auto& exitCall, curlExitCalls) {
        exitCall.endExitCall();
    }
}

/* }}} */

/* {{{ CurlMultiInfoReadInterceptor methods */

void* CurlMultiInfoReadInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
{
    return NULL;
}

void CurlMultiInfoReadInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv,
                                                 void* state)
{
    if (execEnv->getParamCount() < 1)
        return;

    PHPExecEnvironment* phpEnv = AG(G)->exec_env.get();
    ZValPointerResource multiHandle(execEnv->getParam<ZValPointerResource>(0));
    boost::shared_ptr<CurlMultiHandleInfo> handleInfo(execEnv->getHandleRegistry().getHandleInfo<CurlMultiHandleInfo>(multiHandle));
    if (!handleInfo)
        return;

    std::list<ZValPointerAny>& msgList = handleInfo->getCurlMessageList();
    if (msgList.empty())
        return;

    auto curlHandleInfos = handleInfo->getCurlHandleInfos();
    const ZValPointerAny msg(msgList.front());
    msgList.pop_front();

    zval* returnValue = execEnv->getReturnValue();

    // Destroy previous contents of the return value and copy the message,
    // incrementing the refcount via ::share().
    zval* msgZVal = msg.get();
    Z_ADDREF_P(msgZVal);
    zval_dtor(returnValue);
    *returnValue = *msgZVal;

    if (execEnv->getParamCount() > 1) {
        zval* const param1 = execEnv->getParam<zval*>(1);
        zval_dtor(param1);
        ZVAL_LONG(param1, msgList.size());
    }
}

/* }}} */

/* {{{ CurlCopyHandleInterceptor methods */

void* CurlCopyHandleInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    return NULL;
}

void CurlCopyHandleInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                              void* state)
{
    if (phpExecEnv->getParamCount() < 1)
        return;

    ZValPointerResource sourceHandle(phpExecEnv->getParam<ZValPointerResource>(0));
    boost::shared_ptr<CurlHandleInfo> handleInfo(phpExecEnv->getHandleRegistry().getHandleInfo<CurlHandleInfo>(sourceHandle));
    if (!handleInfo)
        return;

    ZValPointerResource newHandle(ZValPointerAny::share(phpExecEnv->getReturnValue()).cast<ZValPointerResource>());
    if (!newHandle)
        return;

    boost::shared_ptr<CurlHandleInfo> newHandleInfo(CurlHandleInfo::create(newHandle));
    phpExecEnv->getHandleRegistry().registerHandle(newHandle, newHandleInfo);

    newHandleInfo->setURL(handleInfo->getURL());
}

/* }}} */

/* {{{ CurlResetInterceptor methods */

void* CurlResetInterceptor::onCallableBegin(const PHPExecEnvironment* phpExecEnv)
{
    return NULL;
}

void CurlResetInterceptor::onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                         void* state)
{
    if (phpExecEnv->getParamCount() < 1)
        return;

    ZValPointerResource curlHandle(phpExecEnv->getParam<ZValPointerResource>(0));
    if (!curlHandle)
        return;

    phpExecEnv->getHandleRegistry().unregisterHandle(curlHandle);
    boost::shared_ptr<CurlHandleInfo> handleInfo(CurlHandleInfo::create(curlHandle));
    phpExecEnv->getHandleRegistry().registerHandle(curlHandle, handleInfo);
}

/* }}} */

// vim: set fdm=marker:

