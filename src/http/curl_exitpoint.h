#ifndef __curl_exitpoint_h
#define __curl_exitpoint_h

#include "common.h"
#include "exitpoint.h"
#include "handle_tracking_exitpoint.h"
#include "httpexitpoint.h"
#include <boost/ptr_container/ptr_vector.hpp>
#include "exitcall_detail.h"
#include "correlation.h"

typedef struct _php_curl php_curl;
typedef struct _php_curlm php_curlm;

inline php_curl* getPhpCurlStructFromResource(const ZValPointerResource& handle)
{
    void* resource = NULL;
    php_curl* curl = NULL;
    int actualType;

    if (handle && curlResourceType) {
#if PHP_VERSION_ID >= 70000
        resource = handle.getResource();
        actualType = handle.getResourceType();
#else
        resource = zend_list_find(handle.getResource(), &actualType);
#endif
        if (resource && actualType == curlResourceType) {
            curl = reinterpret_cast<php_curl*>(resource);
        }
    }

    return curl;
}

inline php_curlm* getPhpCurlMultiStructFromResource(const ZValPointerResource& handle)
{
    void* resource = NULL;
    php_curlm* curlm = NULL;
    int actualType;

    if (handle && curlMultiResourceType) {
#if PHP_VERSION_ID >= 70000
        resource = handle.getResource();
        actualType = handle.getResourceType();
#else
        resource = zend_list_find(handle.getResource(), &actualType);
#endif
        if (resource && actualType == curlMultiResourceType) {
            curlm = reinterpret_cast<php_curlm*>(resource);
        }
    }

    return curlm;
}

/* {{{ class CURLHandleInfo */

/**
 * Handle info class that holds the php_curl pointer and the URL associated with
 * the a CURL handle.
 * Instances of this class can be created only via the static create() method.
 */
class CurlHandleInfo : public AHandleInfo {
        friend class HandleRegistry;
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<CurlHandleInfo> create(const ZValPointerResource& curlResource) { return boost::make_shared<CurlHandleInfo>(curlResource); }

        inline const std::string& getURL() const { return m_url; }
        inline void setURL(const std::string& url) { m_url = url; }

        inline const ZValPointerAny& getHeaders() const { return m_headers; }
        inline void setHeaders(const ZValPointerAny& headers) { m_headers = headers; }

        inline const php_curl* getPhpCurl() const { return m_phpCurl; }
        inline ZValPointerResource& getResource() { return m_curlResource; }

    protected:
        CurlHandleInfo(const ZValPointerResource& curlResource)
            : AHandleInfo(&typeTag)
            , m_phpCurl(getPhpCurlStructFromResource(curlResource))
            , m_curlResource(curlResource)
        {
        }

    private:
        const php_curl* const m_phpCurl;
        SafeModuleShutdownWrapper<ZValPointerResource> m_curlResource;
        std::string m_url;
        SafeModuleShutdownWrapper<ZValPointerAny> m_headers;
};

/* }}} */

/* {{{ class CurlCurrentExitCall */

class CurlCurrentExitCall : public HTTPCurrentExitCall {
    public:
        CurlCurrentExitCall(uint64_t sequenceNumber)
            : HTTPCurrentExitCall(sequenceNumber)
            , m_done(false) {}
        virtual ~CurlCurrentExitCall() {}

        void startExitCall();
        uint64_t endExitCall();
        inline void markDone() { m_done = true; }

        inline void setHandleInfo(const boost::shared_ptr<CurlHandleInfo>& handleInfo)
        {
            m_handleInfo = handleInfo;
        }
        inline const boost::shared_ptr<CurlHandleInfo>& getHandleInfo() const
        {
            return m_handleInfo;
        }

    private:
        boost::shared_ptr<CurlHandleInfo> m_handleInfo;
        bool m_done;
};

/* }}} */

/* {{{ class CurlMultiHandleInfo */

class CurlMultiHandleInfo : public AHandleInfo {
        friend class HandleRegistry;
        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<CurlMultiHandleInfo> create()
            { return boost::make_shared<CurlMultiHandleInfo>(); }

        void addCurlHandle(const boost::shared_ptr<CurlHandleInfo>& handleInfo);
        void removeCurlHandle(const boost::shared_ptr<CurlHandleInfo>& handleInfo);
        inline const std::vector<boost::shared_ptr<CurlHandleInfo>>& getCurlHandleInfos() const { return m_curlHandleInfos; }

        inline boost::ptr_vector<CurlCurrentExitCall>& getExitCalls() { return m_curlExitCalls; }
        inline std::list<ZValPointerAny>& getCurlMessageList() { return m_curlMessageList; }

        inline CurlMultiHandleInfo()
            : AHandleInfo(&typeTag)
        {
        }

    private:


        std::vector<boost::shared_ptr<CurlHandleInfo>> m_curlHandleInfos;
        boost::ptr_vector<CurlCurrentExitCall> m_curlExitCalls;
        std::list<ZValPointerAny> m_curlMessageList;
};

/* }}} */


/* {{{ class ACurlURLCaptureInterceptor */

/**
 * An abstract base class for the interceptors that capture the URL parameter
 * set on a CURL handle that curl_exec() will run on in the future. These use
 * the handle registry to store an instance of URLHandleInfo for a given CURL
 * handle.
 */
class ACurlURLCaptureInterceptor : public AMethodInterceptor {
    public:
        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
        }

    protected:
        ACurlURLCaptureInterceptor(const char* const className,
                                   InterceptorRegistry::ID interceptorID)
            : AMethodInterceptor(className, interceptorID)
            , m_logger(getLogger(std::string(LogContext::EXIT_HTTP) + "." + className))
        {
        }

        /**
         * This method is called to obtain the URL string. Returns true if the
         * derived interceptors was able to find the URL string in the
         * parameters.
         */
        virtual bool getURL(std::string& url,
                            const PHPExecEnvironment* execEnv) const = 0;

        /**
         * This method is called to obtain the HTTP headers array.
         * Returns true if the derived interceptors was able to find the headers
         * in the parameters.
         */
        virtual bool getHeaders(ZValPointerAny* headers,
                                const PHPExecEnvironment* execEnv) const = 0;

        /**
         * This method is supposed to return the CURL handle to use when
         * registering or looking up the URLHandleInfo.
         */
        virtual bool getHandle(ZValPointerAny* handlePointer,
                               const PHPExecEnvironment* execEnv) const = 0;

    private:
        AgentLogger m_logger;
};

/* }}} */

/* {{{ class CurlInitInterceptor */

/**
 * Interceptor for curl_init() function, which takes an optional URL to
 * initialize the handle with.
 */
class CurlInitInterceptor : public ACurlURLCaptureInterceptor {
    public:
        CurlInitInterceptor()
            : ACurlURLCaptureInterceptor("CurlInitInterceptor",
                                         InterceptorRegistry::CurlInitInterceptor_ID)
        {
        }
        bool needReturnValueInOnMethodEnd() const;

    protected:
        bool getURL(std::string& url,
                    const PHPExecEnvironment* execEnv) const;
        bool getHeaders(ZValPointerAny* headers,
                        const PHPExecEnvironment* execEnv) const;
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;
};

/* }}} */

/* {{{ class CurlSetoptInterceptor */

/**
 * Interceptor for curl_setopt() function which may be used to specify the URL
 * for an existing handle.
 */
class CurlSetoptInterceptor : public ACurlURLCaptureInterceptor {
    public:
        CurlSetoptInterceptor()
            : ACurlURLCaptureInterceptor("CurlSetoptInterceptor",
                                         InterceptorRegistry::CurlSetoptInterceptor_ID)
        {
        }
        bool needReturnValueInOnMethodEnd() const;

    protected:
        bool getURL(std::string& url,
                    const PHPExecEnvironment* execEnv) const;
        bool getHeaders(ZValPointerAny* headers,
                        const PHPExecEnvironment* execEnv) const;
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;
};
/* }}} */

/* {{{ class CurlSetoptArrayInterceptor */

/**
 * Interceptor for curl_setopt_array() function which may be used to specify the
 * URL for an existing handle.
 */
class CurlSetoptArrayInterceptor : public ACurlURLCaptureInterceptor {
    public:
        CurlSetoptArrayInterceptor()
            : ACurlURLCaptureInterceptor("CurlSetoptArrayInterceptor",
                                         InterceptorRegistry::CurlSetoptArrayInterceptor_ID)
        {
        }
        bool needReturnValueInOnMethodEnd() const;

    protected:
        bool getURL(std::string& url,
                    const PHPExecEnvironment* execEnv) const;
        bool getHeaders(ZValPointerAny* headers,
                        const PHPExecEnvironment* execEnv) const;
        bool getHandle(ZValPointerAny* handlePointer,
                       const PHPExecEnvironment* execEnv) const;
};
/* }}} */

/* {{{ class CurlExecInterceptor */

/**
 * An exit point interceptor for curl_exec(). It uses the given CURL handle to
 * look up URLHandleInfo in the handle registry and resolve the backend.
 */
class CurlExecInterceptor : public CorrelationEmitter<AExitCallInterceptor,
                                                      CurlExecInterceptor,
                                                      appdynamics::pb::Agent::EXIT_HTTP>
{
        // Give base class template access to addSnapshotExitCallDetail and
        // emitCorrelationInfo.
        friend class ExitCallDetailHelper<AExitCallInterceptor,
                                          CurlExecInterceptor,
                                          appdynamics::pb::Agent::EXIT_HTTP>;
        friend class CorrelationEmitter<AExitCallInterceptor,
                                        CurlExecInterceptor,
                                        appdynamics::pb::Agent::EXIT_HTTP>;
        typedef CorrelationEmitter<AExitCallInterceptor,
                                   CurlExecInterceptor,
                                   appdynamics::pb::Agent::EXIT_HTTP> t_Base;
    public:
        CurlExecInterceptor();

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

    protected:
        bool resolveBackendOnCallableBegin() const { return true; }

        CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                               uint64_t sequenceNumber) const;

        ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                      const PHPExecEnvironment* execEnv) const;

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* execEnv);
    private:

        /**
           Called by base class template to populate the specified SnapshotExitCall.
           @param exitCall The current exit call object.
           @param execEnv PHP execution environment.
           @param sec SnapshotExitCall to populate.
         */
        void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                       const PHPExecEnvironment* execEnv,
                                       const boost::shared_ptr<SnapshotExitCall>& sec) const;

        void emitCorrelationInfo(TransactionContext* context,
                                 CurrentExitCall* exitCall,
                                 const PHPExecEnvironment* execEnv) const;
};

/* }}} */

/* {{{ class CurlCloseInterceptor */

class CurlCloseInterceptor : public AMethodInterceptor {
    public:
        CurlCloseInterceptor()
            : AMethodInterceptor("CurlCloseInterceptor",
                                 InterceptorRegistry::CurlCloseInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
        }
};

/* }}} */

/* {{{ CurlMulti* classes */

class CurlMultiAddHandleInterceptor : public AMethodInterceptor {
    public:
        CurlMultiAddHandleInterceptor()
            : AMethodInterceptor("CurlMultiAddHandleInterceptor",
                                 InterceptorRegistry::CurlMultiAddHandleInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
        }
};

class CurlMultiRemoveHandleInterceptor : public AMethodInterceptor {
    public:
        CurlMultiRemoveHandleInterceptor()
            : AMethodInterceptor("CurlMultiRemoveHandleInterceptor",
                                 InterceptorRegistry::CurlMultiRemoveHandleInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return true; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
        }
};

class CurlMultiExecInterceptor : public CorrelationEmitter<AExitCallInterceptor,
                                                           CurlMultiExecInterceptor,
                                                           appdynamics::pb::Agent::EXIT_HTTP>
{
        // Give base class template access to addSnapshotExitCallDetail and
        // emitCorrelationInfo.
        friend class ExitCallDetailHelper<AExitCallInterceptor,
                                          CurlMultiExecInterceptor,
                                          appdynamics::pb::Agent::EXIT_HTTP>;
        friend class CorrelationEmitter<AExitCallInterceptor,
                                        CurlMultiExecInterceptor,
                                        appdynamics::pb::Agent::EXIT_HTTP>;
        typedef CorrelationEmitter<AExitCallInterceptor,
                                   CurlMultiExecInterceptor,
                                   appdynamics::pb::Agent::EXIT_HTTP> t_Base;
    public:
        CurlMultiExecInterceptor();

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual inline bool needReturnValueInOnMethodEnd() const { return true; }

    protected:
        virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                              const PHPExecEnvironment* execEnv) const;

        /*
         * These are never used.
         */
        bool resolveBackendOnCallableBegin() const { return false; }
        CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                               uint64_t sequenceNumber) const { return NULL; }
    private:
        /**
           Called by base class template to populate the specified SnapshotExitCall.
           @param exitCall The current exit call object.
           @param execEnv PHP execution environment.
           @param sec SnapshotExitCall to populate.
         */
        void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                       const PHPExecEnvironment* execEnv,
                                       const boost::shared_ptr<SnapshotExitCall>& sec) const;

        void emitCorrelationInfo(TransactionContext* context,
                                 CurrentExitCall* exitCall,
                                 const PHPExecEnvironment* execEnv) const;
};

class CurlMultiSelectInterceptor : public AMethodInterceptor {
    public:
        CurlMultiSelectInterceptor()
            : AMethodInterceptor("CurlMultiSelectInterceptor",
                                 InterceptorRegistry::CurlMultiSelectInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual inline bool needReturnValueInOnMethodEnd() const { return false; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
        }
};

class CurlMultiInfoReadInterceptor : public AMethodInterceptor {
    public:
        CurlMultiInfoReadInterceptor()
            : AMethodInterceptor("CurlMultiInfoReadInterceptor",
                                 InterceptorRegistry::CurlMultiInfoReadInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* execEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual inline bool needReturnValueInOnMethodEnd() const { return true; }

        bool isActive(const PHPExecEnvironment* execEnv) const
        {
            return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
        }
};

class CurlCopyHandleInterceptor : public AMethodInterceptor {
    public:
        CurlCopyHandleInterceptor()
            : AMethodInterceptor("CurlCopyHandleInterceptor",
                                 InterceptorRegistry::CurlCopyHandleInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual inline bool needReturnValueInOnMethodEnd() const { return true; }
};

class CurlResetInterceptor : public AMethodInterceptor {
    public:
        CurlResetInterceptor()
            : AMethodInterceptor("CurlResetInterceptor",
                                 InterceptorRegistry::CurlResetInterceptor_ID)
        {
        }

        virtual void* onCallableBegin(const PHPExecEnvironment* phpExecEnv);

        virtual void  onCallableEnd(const PHPExecEnvironment* phpExecEnv,
                                    void* state);

        virtual inline bool needParamsInOnMethodBegin() const { return true; }
        virtual inline bool shouldCallOnMethodEnd() const { return true; }
        virtual bool needReturnValueInOnMethodEnd() const { return false; }
};

/* }}} */

#endif /* __curl_exitpoint_h */

// vim: set fdm=marker:
