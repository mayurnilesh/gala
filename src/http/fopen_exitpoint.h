/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __fopen_exitpoint_h
#define __fopen_exitpoint_h

#include <string>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <stdint.h>

#include "handle_tracking_exitpoint.h"
#include "exitcall_detail.h"
#include "correlation.h"

/* {{{ class FOpenInterceptor */

class HTTPExitPointHandleInfo;

/**
    Interceptor for fopen() php function.  We need to hook fopen
    to time how long it executes for HTTP requests and to remember the
    url of the HTTP request.
 */
class FOpenInterceptor : public CorrelationEmitter<HandleInitExitPointInterceptor<FOpenInterceptor>,
                                                   FOpenInterceptor,
                                                   appdynamics::pb::Agent::EXIT_HTTP>
{
    // Give base class template access to addSnapshotExitCallDetail and
    // emitCorrelationInfo.
    friend class ExitCallDetailHelper<HandleInitExitPointInterceptor<FOpenInterceptor>,
                                                                     FOpenInterceptor,
                                                                     appdynamics::pb::Agent::EXIT_HTTP>;
    friend class CorrelationEmitter<HandleInitExitPointInterceptor<FOpenInterceptor>,
                                    FOpenInterceptor,
                                    appdynamics::pb::Agent::EXIT_HTTP>;
    typedef CorrelationEmitter<HandleInitExitPointInterceptor<FOpenInterceptor>,
                               FOpenInterceptor,
                               appdynamics::pb::Agent::EXIT_HTTP> t_Base;
public:
    friend class HandleInitExitPointInterceptor<FOpenInterceptor>;
    FOpenInterceptor();
    virtual ~FOpenInterceptor() { }
protected:
    virtual bool resolveBackendOnCallableBegin() const;
    virtual bool resolveBackendOnCallableEnd() const;

    virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                          const PHPExecEnvironment* execEnv) const;

    virtual bool getHandle(ZValPointerAny* handlePointer,
                           const PHPExecEnvironment* execEnv) const;

    virtual bool initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                const ExitCallInfo* currentExitCallInfo,
                                const PHPExecEnvironment* execEnv) const;

    virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                         const PHPExecEnvironment* phpExecEnv);
private:
    /**
       Called by base class template to populate the specified SnapshotExitCall.
       @param exitCall The current exit call object.
       @param execEnv PHP execution environment.
       @param sec SnapshotExitCall to populate.
     */
    void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                   const PHPExecEnvironment* execEnv,
                                   const boost::shared_ptr<SnapshotExitCall>& sec) const;

    void emitCorrelationInfo(TransactionContext* context,
                             CurrentExitCall* exitCall,
                             const PHPExecEnvironment* execEnv) const;
};

/* }}} */


class FileGetContentsInterceptor : public CorrelationEmitter<AExitCallInterceptor,
                                                             FileGetContentsInterceptor,
                                                             appdynamics::pb::Agent::EXIT_HTTP>
{
        // Give base class templates access to addSnapshotExitCallDetail and
        // emitCorrelationInfo.
        friend class CorrelationEmitter<AExitCallInterceptor,
                                        FileGetContentsInterceptor,
                                        appdynamics::pb::Agent::EXIT_HTTP>;
        friend class ExitCallDetailHelper<AExitCallInterceptor,
                                          FileGetContentsInterceptor,
                                          appdynamics::pb::Agent::EXIT_HTTP>;
        typedef CorrelationEmitter<AExitCallInterceptor,
                                   FileGetContentsInterceptor,
                                   appdynamics::pb::Agent::EXIT_HTTP> t_Base;
    public:
        FileGetContentsInterceptor();

    protected:
        virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                       uint64_t sequenceNumber) const;

        virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                              const PHPExecEnvironment* execEnv) const;

        virtual bool resolveBackendOnCallableBegin() const { return true; }

        virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                             const PHPExecEnvironment* phpExecEnv);
    private:
        /**
           Called by base class template to populate the specified SnapshotExitCall.
           @param exitCall The current exit call object.
           @param execEnv PHP execution environment.
           @param sec SnapshotExitCall to populate.
         */
        void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                       const PHPExecEnvironment* execEnv,
                                       const boost::shared_ptr<SnapshotExitCall>& sec) const;

        void emitCorrelationInfo(TransactionContext* context,
                                 CurrentExitCall* exitCall,
                                 const PHPExecEnvironment* execEnv) const;

};



#endif

// vim: set fdm=marker:
