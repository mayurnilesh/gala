/*
   Copyright 2013 AppDynamics.
   All rights reserved.
 */

#include <boost/algorithm/string.hpp>
#include <boost/unordered_set.hpp>
#include <ctype.h>
#include <limits>

#include "agent.h"
#include "callgraph/exec_stack_frame.h"
#include "correlation.h"
#include "naming.h"
#include "httpentrypoint.h"
#include "snapshot.h"

extern "C"
{
#include "ext/standard/url.h"
#include "ext/session/php_session.h"
}

bool HTTPEntryPointDelegate::s_didApplyRules = false;

static bool matchKeyValue(const PHPExecEnvironment* execEnv,
                          const appdynamics::pb::Agent::KeyValueMatch* keyValueMatch,
                          ZValPointerArray* payloadMap,
                          const boost::shared_ptr<MatchPointConfig>& config,
                          const AgentLogger& logger)
{
    if (keyValueMatch->key().matchstrings_size() == 0)
        return true;

    using namespace appdynamics::pb::Agent;
    // If keyValueMatch key or value are repeated, not a valid key/value mapping.
    if (keyValueMatch->key().matchstrings_size() > 1 || keyValueMatch->value().matchstrings_size() > 1)
        return false;

    // Iterate over each key/value pair in payload map
    // if check for existence, return true once key is found
    // else if compare value, return

    const std::string& key = keyValueMatch->key().matchstrings(0);
    ZValPointerAny value;
    bool foundKey = payloadMap->findEntryByName(key.c_str(), key.size(), &value);
    // If key not found, return false.
    if (!foundKey)
        return false;

    // If key found, return bool depending on KeyValueMatch::Type.
    if (keyValueMatch->type() == KeyValueMatch::CHECK_FOR_EXISTENCE)
        return true;
    else if (keyValueMatch->type() == KeyValueMatch::COMPARE_VALUE) {
        const std::string valueString = value.cast<ZValPointerString>().getStringValue();
        return config->matchAndLogError(execEnv, &(keyValueMatch->value()), valueString, logger);
    }
    // Invalid Type
    else
        return false;
}

static bool matchRepeatedKeyValue(const PHPExecEnvironment* execEnv,
                                  const ::google::protobuf::RepeatedPtrField< ::appdynamics::pb::Agent::KeyValueMatch >* matchDictionary,
                                  ZValPointerArray* payloadMap,
                                  const boost::shared_ptr<MatchPointConfig>& config,
                                  const AgentLogger& logger)
{
    // TODO: Returns true if matchDictionary has 0 elements. Is this desirable?
    for ( auto it = matchDictionary->begin() ; it != matchDictionary->end() ; ++it ) {
        if (!matchKeyValue(execEnv, &(*it), payloadMap, config, logger))
            return false;
    }
    return true;
}


HTTPEntryPointInterceptor httpInterceptor;

HTTPEntryPointInterceptor::HTTPEntryPointInterceptor()
    : AEntryPointInterceptor("HTTPEntryPointInterceptor",
                             InterceptorRegistry::HTTPEntryPointInterceptor_ID)
{
    // TODO get from factory/registry
    server_env = new ApacheEnvironment();
}

HTTPEntryPointInterceptor::~HTTPEntryPointInterceptor()
{
    delete server_env;
}

appdynamics::pb::Agent::EntryPointType HTTPEntryPointInterceptor::getEntryPointType() const
{
    return appdynamics::pb::Agent::PHP_WEB;
}

void HTTPEntryPointInterceptor::detectErrors(const PHPExecEnvironment* phpExecEnv,
                                             void* state)
{
    AEntryPointInterceptor::detectErrors(phpExecEnv, state);
    /*
     * This is the outermost interceptor and if the exception passes
     * this point, it'll get logged in the error callback as well.
     * This is undesirable because we already report the exception
     * here. So we tell the monitor to expect this and filter out
     * the "Uncaught exception" error.
     */
    if (phpExecEnv->doesExceptionObjectExist()) {
        ErrorMonitor* errorMonitor =
            AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        errorMonitor->expectExceptionError();
    }
}

bool HTTPEntryPointInterceptor::isCorrelationHeaderPresent(const PHPExecEnvironment *execEnv) const
{
    boost::shared_ptr<HTTPPayload> payload(execEnv->getHTTPPayload());
    return !payload->getIncomingCorrelationHeader().empty();
}

boost::shared_ptr<IMatchPointPayload> HTTPEntryPointInterceptor::createPayload(const PHPExecEnvironment* execEnv)
{
    boost::shared_ptr<HTTPPayload> payload(execEnv->getHTTPPayload());

    CallGraph::ExecStackFrame* const topStackElement =
        execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
    if (topStackElement)
        topStackElement->setCallElementType(appdynamics::pb::CallElement_Type_HTTP);

    return payload;
}

/**************************
   HTTPEntryPointDelegate
 **************************/

HTTPEntryPointDelegate::HTTPEntryPointDelegate(InterceptionEngine* interceptEngine,
                                               TransactionMonitor* txMonitor)
 : AEntryPointDelegate(interceptEngine, txMonitor)
{
}

void HTTPEntryPointDelegate::configure(const boost::shared_ptr<appdynamics::pb::TransactionConfig>& txConfig,
                                       const struct _zend_agent_globals* agentGlobals)
{
    const appdynamics::pb::Agent::MatchPointConfig* configProto = &(txConfig->http());
    boost::shared_ptr<MatchPointConfig> config(boost::make_shared<MatchPointConfig>(txConfig,
                                                                                    configProto));
    AEntryPointDelegate::configure(config, agentGlobals);
}

void HTTPEntryPointDelegate::initAcceptor()
{
    m_txAcceptor = boost::make_shared<HTTPTransactionAcceptor>(m_matchPointConfig,
                                                               m_txMonitor);
}

void HTTPEntryPointDelegate::applyRules(const struct _zend_agent_globals* agentGlobals)
{
    if (s_didApplyRules)
        return;
    s_didApplyRules = true;
    m_interceptEngine->addInterceptorForCallable(CallableInfo(ROOT_SYMBOL), &httpInterceptor);
}

HTTPEntryPointDelegate::~HTTPEntryPointDelegate()
{
}

boost::shared_ptr<CorrelationHeader>
    HTTPTransactionAcceptor::checkForContinuingTransaction(const boost::shared_ptr<IMatchPointPayload>& iPayload)
{
    boost::shared_ptr<HTTPPayload> payload(boost::static_pointer_cast<HTTPPayload>(iPayload));
    std::string correlationHeader(payload->getIncomingCorrelationHeader());
    if (correlationHeader.empty()) {
        LOG4CXX_DEBUG(getLogger(), "HTTP correlation header not found");
        return boost::shared_ptr<CorrelationHeader>();
    }

    LOG4CXX_DEBUG(getLogger(), "HTTP correlation header found [" << correlationHeader << "]");
    return TransactionCorrelator::processContinuingTransaction(
        correlationHeader, getEntryPointType(), AG(G)->request_context->getStartTime(), getLogger());
}

std::string HTTPTransactionAcceptor::getNameFromNamingScheme(const boost::shared_ptr<IMatchPointPayload>& iPayload)
{
    boost::shared_ptr<HTTPPayload> payload(boost::static_pointer_cast<HTTPPayload>(iPayload));
    const std::map<std::string, std::string>& properties =
        getConfig()->getDiscoveryProperties();
    return getHTTPEntryPointBTName(getLogger(),
                                   payload,
                                   &properties);
}

bool HTTPTransactionAcceptor::matchTransactionRule(const boost::shared_ptr<IMatchPointPayload>& iPayload,
                                                   const appdynamics::pb::Agent::EntryPointMatchCondition& matchCondition)
{
    appdynamics::pb::Agent::EntryPointMatchCondition::HTTPMatchRule matchRule = matchCondition.http();
    const boost::shared_ptr<HTTPPayload> payload = boost::static_pointer_cast<HTTPPayload>(iPayload);
    const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();

    bool matches = true;
    if (matchRule.has_method())
        matches = matches && boost::iequals(HTTPMethod_Name(matchRule.method()), payload->getMethod());
    if (matchRule.has_uri())
        matches = matches && configPtr->matchAndLogError(payload->getExecEnv(), &(matchRule.uri()), payload->getURL(), getLogger());
    if (matchRule.has_host())
        matches = matches && configPtr->matchAndLogError(payload->getExecEnv(), &(matchRule.host()), payload->getHost(), getLogger());
    if (matchRule.has_port())
        matches = matches && configPtr->matchAndLogError(payload->getExecEnv(), &(matchRule.port()), payload->getPort(), getLogger());

    typedef appdynamics::pb::Common::StringMatchCondition::Type MatchType ;

    ZValPointerArray getParams = payload->getGetParams();
    ZValPointerArray postParams = payload->getPostParams();
    ZValPointerArray cookies = payload->getCookies();

    matches = matches && matchRepeatedGetPostKeyValue(payload->getExecEnv(), &(matchRule.params()), &getParams, &postParams, configPtr, getLogger())
                      && matchRepeatedKeyValue(payload->getExecEnv(), &(matchRule.cookies()), &cookies, configPtr, getLogger())
                      && matchHTTPHeader(&(matchRule.headers()), payload, configPtr, getLogger());

    return matches;
}

bool HTTPTransactionAcceptor::matchRepeatedGetPostKeyValue(const PHPExecEnvironment* execEnv,
                                                           const ::google::protobuf::RepeatedPtrField< ::appdynamics::pb::Agent::KeyValueMatch >* matchDictionary,
                                                           ZValPointerArray* payloadGetMap,
                                                           ZValPointerArray* payloadPostMap,
                                                           const boost::shared_ptr<MatchPointConfig>& config,
                                                           const AgentLogger& logger)
{
    boost::unordered_set<std::string> unmatchedKeys;
    for ( auto it = matchDictionary->begin() ; it != matchDictionary->end() ; ++it )
        if (!matchKeyValue(execEnv, &(*it), payloadGetMap, config, logger))
            if (it->key().matchstrings_size() > 0) {
                std::string key = it->key().matchstrings(0);
                unmatchedKeys.insert(key);
            }
    for ( auto it = matchDictionary->begin() ; it != matchDictionary->end() ; ++it )
        if (!matchKeyValue(execEnv, &(*it), payloadPostMap, config, logger)) {
            if (it->key().matchstrings_size() > 0) {
                std::string key = it->key().matchstrings(0);
                if (unmatchedKeys.count(it->key().matchstrings(0)) > 0)
                    return false;
            }
        }
    return true;
}

bool HTTPTransactionAcceptor::customMatchTransaction(boost::shared_ptr<AgentTransaction>* matchedTransaction,
                                                  const boost::shared_ptr<IMatchPointPayload>& iPayload)
{
    *matchedTransaction = boost::shared_ptr<AgentTransaction>();

    const boost::shared_ptr<MatchPointConfig> configPtr = getConfig();
    const appdynamics::pb::Agent::MatchPointConfig* config = configPtr->get();
    BOOST_ASSERT(config->enabled());

    const auto& customDefinitions = config->customdefinitions();

    int maxPriority = std::numeric_limits<int>::min();
    const appdynamics::pb::Agent::MatchPointConfig_CustomMatch* prioritizedMatchRule = NULL;

    for (auto it = customDefinitions.begin(); it != customDefinitions.end(); ++it)
    {
        if (!it->condition().has_http())
            continue;

        LOG4CXX_TRACE(getLogger(), "Attempting to match custom match rule: " << it->btname());
        bool matches = matchTransactionRule(iPayload, it->condition());

        if (!matches)
            continue;

        LOG4CXX_DEBUG(getLogger(), "Potential match rule found: " << it->btname() << " Priority: " << it->priority() << " Current maximum matching priority: " << maxPriority);

        if(it->priority() > maxPriority) {
            prioritizedMatchRule = &(*it);
            maxPriority = it->priority();
        }
    }
    if (!prioritizedMatchRule)
        return false;

    std::string baseNameWithSplit = prioritizedMatchRule->btname();
    LOG4CXX_DEBUG(getLogger(), "Final custom match rule matched: " << baseNameWithSplit);

    if (prioritizedMatchRule->condition().http().properties_size() > 0) {
        std::map<std::string, std::string> splitNameValuePairs;
        convertPBNameValuePairsToMap(prioritizedMatchRule->condition().http().properties(), splitNameValuePairs);

        boost::shared_ptr<HTTPPayload> payload(boost::static_pointer_cast<HTTPPayload>(iPayload));
        baseNameWithSplit = transformNameWithSplit(payload, prioritizedMatchRule->btname(), payload->getURL(), splitNameValuePairs);
    }

    *matchedTransaction = getTransaction(baseNameWithSplit, prioritizedMatchRule);
    return true;
}

bool HTTPTransactionAcceptor::matchHTTPHeader(const ::google::protobuf::RepeatedPtrField< ::appdynamics::pb::Agent::KeyValueMatch >* keyValueMatch,
                                              const boost::shared_ptr<HTTPPayload>& payload,
                                              const boost::shared_ptr<MatchPointConfig>& config,
                                              const AgentLogger& logger)
{
    using namespace appdynamics::pb::Agent;

    for ( auto it = keyValueMatch->begin() ; it != keyValueMatch->end() ; ++it)
    {
        // If keyValueMatch key or value are repeated, not a valid key/value mapping.
        if (it->key().matchstrings_size() > 1 || it->value().matchstrings_size() > 1)
            return false;
        std::string value;
        if (!payload->getHeaderValue(it->key().matchstrings(0), &value))
            return false;
        if (it->type() == KeyValueMatch::COMPARE_VALUE && ! config->matchAndLogError(payload->getExecEnv(), &(it->value()), value, logger))
            return false;
    }
    return true;
}

static const char SERVER_VAR_NAME[] = "_SERVER";
static const char PHP_SELF_KEY_NAME[] = "PHP_SELF";
static const char PHP_REQUEST_URI_NAME[] = "REQUEST_URI";
static const char PHP_HTTPS_NAME[] = "HTTPS";
static const char PHP_SERVER_HOST_NAME[] = "SERVER_NAME";
static const char PHP_SERVER_PORT_NAME[] = "SERVER_PORT";
static const char PHP_REQUEST_METHOD_NAME[] = "REQUEST_METHOD";
static const char PHP_REFERER_NAME[] = "HTTP_REFERER";
static const char COOKIE_VAR_NAME[] = "_COOKIE";
static const char GET_VAR_NAME[] = "_GET";
static const char POST_VAR_NAME[] = "_POST";
static const char SESSION_VAR_NAME[] = "_SESSION";


ZValPointerArray HTTPPayload::getServerArray() const
{
    ZValPointerAny serverArray;
    m_execEnv->getAutoGlobalValue(SERVER_VAR_NAME, strlen(SERVER_VAR_NAME), &serverArray);
    ZValPointerArray ret(serverArray.cast<ZValPointerArray>());
    if (!ret) {
        LOG4CXX_ERROR(m_logger, "Unable to get _SERVER!");
    }
    return ret;
}

/*
 * HTTPPayload
 */

const std::string& HTTPPayload::getMethod() const
{
    if (m_didGetMethod)
        return m_method;
    m_didGetMethod = true;

    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return m_method;

    ZValPointerString requestMethodVal(serverArray.findEntryByName<ZValPointerString>(PHP_REQUEST_METHOD_NAME,
                                                                                      strlen(PHP_REQUEST_METHOD_NAME)));

    if (!requestMethodVal)
        return m_method;

    m_method = requestMethodVal.getStringValue();

    return m_method;

}

const std::string& HTTPPayload::getURL() const
{
    if (m_didGetURL)
        return m_url;

    m_didGetURL = true;
    std::string urlWithQuery(getURLWithQuery());
    if (urlWithQuery.empty()) {
        return m_url;
    }

    bool has_leading_slash = (urlWithQuery[0] == '/');
    bool has_trailing_slash = (urlWithQuery.length() > 1 && urlWithQuery[urlWithQuery.length() - 1] == '/');
    if (has_leading_slash || has_trailing_slash) {
        boost::trim_if(urlWithQuery, boost::is_from_range('/', '/'));
        if (has_leading_slash)
            urlWithQuery.insert(urlWithQuery.begin(), '/');
        if (has_trailing_slash && urlWithQuery[urlWithQuery.length() - 1] != '/')
            urlWithQuery += '/';
    }

    php_url* const parsedURL = php_url_parse_ex(urlWithQuery.c_str(),
                                                urlWithQuery.length());
    if (!parsedURL) {
        LOG4CXX_ERROR(m_logger, "Failed to parse _SERVER['REQUEST_URI']: " << urlWithQuery);
        return m_url;
    }

    const char* const path = parsedURL->path;
    if (path)
        m_url = path;

    php_url_free(parsedURL);

    if (m_url.empty()) {
        LOG4CXX_ERROR(m_logger, "Path part in _SERVER['REQUEST_URI'] is empty: " << urlWithQuery);
    }

    return m_url;
}

const std::string& HTTPPayload::getHost() const
{
    if (m_didGetHost)
        return m_host;
    m_didGetHost = true;

    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return m_host;

    ZValPointerAny serverHostVal(serverArray.findEntryByName<ZValPointerAny>(PHP_SERVER_HOST_NAME,
                                                                             strlen(PHP_SERVER_HOST_NAME)));

    if (!serverHostVal)
        return m_host;
    if (serverHostVal.getType() == ZValPointer::ZVal_Object)
        return m_host;

    m_host = serverHostVal.convertToStringAsCopy(m_execEnv).getStringValue();
    return m_host;
}

const std::string& HTTPPayload::getPort() const
{
    if (m_didGetPort)
        return m_port;
    m_didGetPort = true;

    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return m_port;

    ZValPointerAny serverPortVal(serverArray.findEntryByName<ZValPointerAny>(PHP_SERVER_PORT_NAME,
                                                                             strlen(PHP_SERVER_PORT_NAME)));

    if (!serverPortVal)
        return m_port;
    if (serverPortVal.getType() == ZValPointer::ZVal_Object)
        return m_port;

    m_port = serverPortVal.convertToStringAsCopy(m_execEnv).getStringValue();
    return m_port;
}

const std::string& HTTPPayload::getURLWithQuery() const
{
    if (m_didGetURLWithQuery)
        return m_urlWithQuery;
    m_didGetURLWithQuery = true;

    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return m_urlWithQuery;

    ZValPointerString requestURIVal(serverArray.findEntryByName<ZValPointerString>(PHP_REQUEST_URI_NAME,
                                                                                   strlen(PHP_REQUEST_URI_NAME)));

    if (!requestURIVal) {
        LOG4CXX_ERROR(m_logger, "Unable to get _SERVER['REQUEST_URI']!");
        return m_urlWithQuery;
    }
    m_urlWithQuery = requestURIVal.getStringValue();
    if (m_urlWithQuery.empty()) {
        LOG4CXX_ERROR(m_logger, "_SERVER['REQUEST_URI'] is empty!");
        return m_urlWithQuery;
    }
    return m_urlWithQuery;
}

ZValPointerArray HTTPPayload::getCookies() const
{
    if (m_didGetCookies)
        return m_cookiesArray.get().cast<ZValPointerArray>();

    m_didGetCookies = true;
    m_execEnv->getAutoGlobalValue(COOKIE_VAR_NAME, strlen(COOKIE_VAR_NAME), &(m_cookiesArray.get()));
    return m_cookiesArray.get().cast<ZValPointerArray>();
}

ZValPointerArray HTTPPayload::getGetParams() const
{
    if (m_didGetGetParams)
        return m_getParamsArray.get().cast<ZValPointerArray>();

    m_didGetGetParams = true;
    m_execEnv->getAutoGlobalValue(GET_VAR_NAME, strlen(GET_VAR_NAME), &(m_getParamsArray.get()));
    return m_getParamsArray.get().cast<ZValPointerArray>();
}

ZValPointerArray HTTPPayload::getPostParams() const
{
    if (m_didGetPostParams)
        return m_postParamsArray.get().cast<ZValPointerArray>();

    m_didGetPostParams = true;
    m_execEnv->getAutoGlobalValue(POST_VAR_NAME, strlen(POST_VAR_NAME), &(m_postParamsArray.get()));
    return m_postParamsArray.get().cast<ZValPointerArray>();
}

const std::string& HTTPPayload::getReferrer() const
{
    if (m_didGetReferrer)
        return m_referrer;
    m_didGetReferrer = true;

    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return m_referrer;

    ZValPointerString referrerVal(serverArray.findEntryByName<ZValPointerString>(PHP_REFERER_NAME,
                                                                                 strlen(PHP_REFERER_NAME)));

    if (!referrerVal)
        return m_referrer;

    m_referrer = referrerVal.getStringValue();
    return m_referrer;
}

bool HTTPPayload::getHeaderValue(const std::string& headerName,
                                 std::string* headerValue) const
{
    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return false;

    static const char HTTP_[] = "HTTP_";


    std::string serverVariableNameForHeader;
    serverVariableNameForHeader.reserve(sizeof(HTTP_) + headerName.size());
    serverVariableNameForHeader.append(HTTP_, sizeof(HTTP_) - 1);

    auto headerNameEnd = headerName.end();
    for (auto i = headerName.begin(); i != headerNameEnd; ++i)
    {
        char currChar = *i;
        if (currChar == '-')
            currChar = '_';
        else
            currChar = toupper(*i);
        serverVariableNameForHeader.append(1, currChar);
    }

    ZValPointerString headerValueZVal(serverArray.findEntryByName<ZValPointerString>(serverVariableNameForHeader.c_str(),
                                                                                     serverVariableNameForHeader.size()));
    if (!headerValueZVal)
        return false;
    *headerValue = headerValueZVal.getStringValue();
    return true;
}

bool HTTPPayload::isHTTPs() const
{
    if (m_didGetHTTPS)
        return m_isHTTPs;
    m_didGetHTTPS = true;

    ZValPointerArray serverArray(getServerArray());
    if (!serverArray)
        return m_isHTTPs;

    ZValPointerAny httpsVal(serverArray.findEntryByName<ZValPointerAny>(PHP_HTTPS_NAME,
                                                                        strlen(PHP_HTTPS_NAME)));
    m_isHTTPs = httpsVal;
    return m_isHTTPs;
}

const PHPExecEnvironment* HTTPPayload::getExecEnv() const
{
    return m_execEnv;
}

const std::string& HTTPPayload::getIncomingCorrelationHeader() const
{
    if (m_didGetCorrelationHeader)
        return m_incomingCorrelationHeader;

    m_didGetCorrelationHeader = true;
    std::string correlationHeader;
    if (!getHeaderValue(CorrelationHeader::MAIN_HEADER, &correlationHeader))
        return m_incomingCorrelationHeader;

    m_incomingCorrelationHeader = correlationHeader;
    return m_incomingCorrelationHeader;
}

bool HTTPPayload::getSessionId(std::string* sessionId) const
{
    // Gather session entries
    const auto& sessionGlobals =
        m_execEnv->getAgentGlobals().delayed_ext_globals.get_session();

    if (!sessionGlobals)
        return false;
    if (sessionGlobals->session_status != php_session_active)
        return false;
    if (sessionGlobals->id == NULL)
        return false;
    *sessionId = ZSTR_VAL(sessionGlobals->id);
    return true;
}

ZValPointerArray HTTPPayload::getSessionArray() const
{
    if (m_didGetSessionArray)
        return m_sessionArray.get().cast<ZValPointerArray>();

    m_didGetSessionArray = true;
    m_execEnv->getAutoGlobalValue(SESSION_VAR_NAME, strlen(SESSION_VAR_NAME), &(m_sessionArray.get()));
    return m_sessionArray.get().cast<ZValPointerArray>();
}
