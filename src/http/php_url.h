/*
   Copyright 2014 AppDynamics.
   All rights reserved.
   @author David Kim (david.kim@appdynamics.com)
 */

#ifndef __PHP_URL_CLASS__
#define __PHP_URL_CLASS__

#include <string>
#include <boost/utility.hpp>
#include <boost/algorithm/string.hpp>

struct php_url;

/**
 * Data and memory management class for parsing URLs, essentially a wrapper
 * around php_url_parse that frees the resulting php_url data structure
 * in the destructor.
 */
class PHPUrl : boost::noncopyable
{
public:
    /**
     * Constructor, takes URL as argument. Calls php_url_parse_ex().
     *
     * @argument String URL.
     */
    PHPUrl(const std::string& url);

    /**
     * Destructor, frees the underlying php_url. Calls php_url_free().
     */
    ~PHPUrl();

    operator bool() const { return m_parsedURL; }

    /********************************************************************
     * <scheme>://<user>:<pass>@<host>:<port>/<path>?<query>#<fragment> *
     ********************************************************************/

    /**
     * Returns URL scheme.
     * @return char* scheme
     */
    const char* scheme() const;

    /**
     * Returns URL user.
     * @return char* user
     */
    const char* user() const;

    /**
     * Returns URL pass.
     * @return char* pass
     */
    const char* pass() const;

    /**
     * Returns URL host.
     * @return char* host
     */
    const char* host() const;

    /**
     * Returns URL port.
     * @return char* port
     */
    int port() const;

    /**
     * Returns URL path.
     * @return char* path
     */
    const char* path() const;

    /**
     * Returns URL query.
     * @return char* query
     */
    const char* query() const;

    /**
     * Returns URL fragment.
     * @return char* fragment
     */
    const char* fragment() const;

private:
    PHPUrl();
    php_url* m_parsedURL;
};

#endif // __PHP_URL_CLASS__
