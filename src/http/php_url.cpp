/*
   Copyright 2014 AppDynamics.
   All rights reserved.
   @author David Kim (david.kim@appdynamics.com)
 */

#include "php_url.h"
#include <ctype.h>
#include <string>
#include <boost/algorithm/string.hpp>
#include "agent.h"

extern "C"
{
#include "ext/standard/url.h"
}

struct php_url;

/**
 * Data and memory management class for parsing URLs, essentially a wrapper
 * around php_url_parse that frees the resulting php_url data structure
 * in the destructor.
 */

/*******************************************************************
* <scheme>://<user>:<pass>@<host>:<port>/<path>?<query>#<fragment> *
********************************************************************/

PHPUrl::PHPUrl(const std::string& url)
{
    m_parsedURL = php_url_parse_ex(url.c_str(), url.size());
}

PHPUrl::PHPUrl()
{
}

PHPUrl::~PHPUrl()
{
    if (m_parsedURL)
        php_url_free(m_parsedURL);
}

const char* PHPUrl::scheme() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->scheme;
}

const char* PHPUrl::user() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->user;
}

const char* PHPUrl::pass() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->pass;
}

const char* PHPUrl::host() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->host;
}

int PHPUrl::port() const
{
    if (!m_parsedURL)
        return 0;
    return m_parsedURL->port;
}

const char* PHPUrl::path() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->path;
}

const char* PHPUrl::query() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->query;
}

const char* PHPUrl::fragment() const
{
    if (!m_parsedURL)
        return NULL;
    return m_parsedURL->fragment;
}
