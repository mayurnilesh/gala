/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

/*
 * Reduced array-bounds to a warning due to boost/gcc issues
 * described here:
 * https://bugzilla.redhat.com/show_bug.cgi?id=555771
 * As long as we build on centos5, we're stuck.
 */

#pragma GCC diagnostic warning "-Warray-bounds"

#include "zend1_http_exitpoint.h"
#include "httpexitpoint.h"
#include "emalloc_allocator.h"
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

namespace Zend1
{
    namespace Private
    {
        class HTTPPropertyGenerator : public IBackendPropertyGenerator
        {
            public:
                virtual bool getProperties(const PHPExecEnvironment* execEnv,
                                           const CurrentExitCall* exitCall,
                                           StringMap& properties) const;

                static bool getURL(std::string* url,
                                   const PHPExecEnvironment* execEnv);

                static ZValPointerObject castToZendHTTPURL(const PHPExecEnvironment* execEnv,
                                                           const ZValPointerAny& val);

                static ZValPointerObject getURIProperty(const PHPExecEnvironment* execEnv);

            private:
                static bool getProtectedStringProperty(std::string* value,
                                                       const PHPExecEnvironment* execEnv,
                                                       const ZValPointerObject& zendHTTPURL,
                                                       const char* propertyName,
                                                       size_t propertyNameLen);

                static bool getPortNumberAsString(std::string* value,
                                                  const PHPExecEnvironment* execEnv,
                                                  const ZValPointerObject& zendHTTPURL);

        };

        static const char ZEND_HTTP_CLIENT_URI_PROPERTY_NAME[] = "uri";
        static const size_t ZEND_HTTP_CLIENT_URI_PROPERTY_NAME_LEN = sizeof(ZEND_HTTP_CLIENT_URI_PROPERTY_NAME) - 1;

        static const char ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME[] = "headers";
        static const size_t ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME_LEN = sizeof(ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME) - 1;

        static const char ZEND_URI_HTTP_CLASS_NAME[] = "Zend_Uri_Http";
        static const size_t ZEND_URI_HTTP_CLASS_NAME_LEN = sizeof(ZEND_URI_HTTP_CLASS_NAME) - 1;

        static const char ZEND_URI_HTTP_SCHEME_PROPERTY_NAME[] = "_scheme";
        static const size_t ZEND_URI_HTTP_SCHEME_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_SCHEME_PROPERTY_NAME) - 1;
        static const char ZEND_URI_HTTP_USERNAME_PROPERTY_NAME[] = "_username";
        static const size_t ZEND_URI_HTTP_USERNAME_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_USERNAME_PROPERTY_NAME) - 1;
        static const char ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME[] = "_password";
        static const size_t ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME) - 1;
        static const char ZEND_URI_HTTP_HOST_PROPERTY_NAME[] = "_host";
        static const size_t ZEND_URI_HTTP_HOST_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_HOST_PROPERTY_NAME) - 1;
        static const char ZEND_URI_HTTP_PORT_PROPERTY_NAME[] = "_port";
        static const size_t ZEND_URI_HTTP_PORT_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_PORT_PROPERTY_NAME) - 1;
        static const char ZEND_URI_HTTP_PATH_PROPERTY_NAME[] = "_path";
        static const size_t ZEND_URI_HTTP_PATH_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_PATH_PROPERTY_NAME) - 1;
        static const char ZEND_URI_HTTP_QUERY_PROPERTY_NAME[] = "_query";
        static const size_t ZEND_URI_HTTP_QUERY_PROPERTY_NAME_LEN = sizeof(ZEND_URI_HTTP_QUERY_PROPERTY_NAME) - 1;

        static const char ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME[] = "code";
        static const size_t ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME_LEN = sizeof(ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME) - 1;
        static const char ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME[] = "message";
        static const size_t ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME_LEN = sizeof(ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME) - 1;

        bool HTTPPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                          const CurrentExitCall* exitCall,
                                          StringMap& properties) const
        {
            ZValPointerObject zendHTTPURL(getURIProperty(execEnv));
            if (!zendHTTPURL)
                return false;

            std::string host;
            if (getProtectedStringProperty(&host,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_HOST_PROPERTY_NAME,
                                           ZEND_URI_HTTP_HOST_PROPERTY_NAME_LEN)) {
                if (host.size() > MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY)
                   host.resize(MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY);
                properties[enum2str(HTTPExitPoint_HOST)] = host;
            }

            std::string portAsString;
            if (getPortNumberAsString(&portAsString,
                                      execEnv,
                                      zendHTTPURL)) {
                properties[enum2str(HTTPExitPoint_PORT)] = portAsString;
            }

            std::string path;
            if (getProtectedStringProperty(&path,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_PATH_PROPERTY_NAME,
                                           ZEND_URI_HTTP_PATH_PROPERTY_NAME_LEN)) {
                boost::trim_right_if(path, boost::is_from_range('/', '/'));
                if (path.size() > MAX_URL_LEN_IN_IDENTIFYING_PROPERTY)
                    path.resize(MAX_URL_LEN_IN_IDENTIFYING_PROPERTY);
                if (path.empty())
                    path = "/";
                properties[enum2str(HTTPExitPoint_URL)] = path;
            }

            std::string query;
            if (getProtectedStringProperty(&query,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_QUERY_PROPERTY_NAME,
                                           ZEND_URI_HTTP_QUERY_PROPERTY_NAME_LEN)) {
                properties[enum2str(HTTPExitPoint_QUERY_STRING)] = query;
            }

            return true;
        }

        bool HTTPPropertyGenerator::getURL(std::string* url,
                                   const PHPExecEnvironment* execEnv)
        {
            ZValPointerObject zendHTTPURL(getURIProperty(execEnv));
            if (!zendHTTPURL)
                return false;

            std::string scheme;
            if (!getProtectedStringProperty(&scheme,
                                            execEnv,
                                            zendHTTPURL,
                                            ZEND_URI_HTTP_SCHEME_PROPERTY_NAME,
                                            ZEND_URI_HTTP_SCHEME_PROPERTY_NAME_LEN))
                return false;

            std::string host;
            if (!getProtectedStringProperty(&host,
                                            execEnv,
                                            zendHTTPURL,
                                            ZEND_URI_HTTP_HOST_PROPERTY_NAME,
                                            ZEND_URI_HTTP_HOST_PROPERTY_NAME_LEN))
                return false;

            std::string portAsString;
            if (!getPortNumberAsString(&portAsString,
                                       execEnv,
                                       zendHTTPURL))
                return false;

            std::string password;
            if (getProtectedStringProperty(&password,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME,
                                           ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME_LEN))
                password = ":" + std::string(password.size(), '*');

            std::string auth;
            if (getProtectedStringProperty(&auth,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_USERNAME_PROPERTY_NAME,
                                           ZEND_URI_HTTP_USERNAME_PROPERTY_NAME_LEN)) {
                auth += password;
                auth += '@';
            }

            std::string path;
            getProtectedStringProperty(&path,
                                       execEnv,
                                       zendHTTPURL,
                                       ZEND_URI_HTTP_PATH_PROPERTY_NAME,
                                       ZEND_URI_HTTP_PATH_PROPERTY_NAME_LEN);

            std::string query;
            getProtectedStringProperty(&query,
                                       execEnv,
                                       zendHTTPURL,
                                       ZEND_URI_HTTP_QUERY_PROPERTY_NAME,
                                       ZEND_URI_HTTP_QUERY_PROPERTY_NAME_LEN);
            *url = scheme
                 + "://"
                 + auth
                 + host
                 + ":"
                 + portAsString
                 + path
                 + query;
            return true;
        }

        ZValPointerObject HTTPPropertyGenerator::castToZendHTTPURL(const PHPExecEnvironment* execEnv,
                                                                       const ZValPointerAny& val)
        {
            ZValPointerObject obj(val.cast<ZValPointerObject>());
            if (!obj)
                return obj;

            zend_class_entry* const zendUriHttpClass =
                execEnv->lookupClass(ZEND_URI_HTTP_CLASS_NAME,
                                     ZEND_URI_HTTP_CLASS_NAME_LEN);
            // If the Zend_Uri_Http class does not yet exist, then there are
            // no instances of Zend_Uri_Http.
            if (!zendUriHttpClass)
                return ZValPointerObject();
            if (!obj.isInstanceOf(execEnv, zendUriHttpClass))
                return ZValPointerObject();

            return obj;
        }

        ZValPointerObject HTTPPropertyGenerator::getURIProperty(const PHPExecEnvironment* execEnv)
        {
            zval* rawThisZVal = execEnv->getInvokedObject();
            if (!rawThisZVal)
                return ZValPointerObject();

            ZValPointerObject thisZVal(ZValPointerAny::share(rawThisZVal).cast<ZValPointerObject>());
            if (!thisZVal)
                return ZValPointerObject();

            ZValPointerObject uriZVal(thisZVal.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                              ZEND_HTTP_CLIENT_URI_PROPERTY_NAME,
                                                                                              ZEND_HTTP_CLIENT_URI_PROPERTY_NAME_LEN));
            return castToZendHTTPURL(execEnv, uriZVal);
        }

        bool HTTPPropertyGenerator::getProtectedStringProperty(std::string* value,
                                                                   const PHPExecEnvironment* execEnv,
                                                                   const ZValPointerObject& zendHTTPURL,
                                                                   const char* propertyName,
                                                                   size_t propertyNameLen)
        {
            BOOST_ASSERT(value);
            ZValPointerString valueZVal(zendHTTPURL.findProtectedPropertyByName<ZValPointerString>(execEnv,
                                                                                                   propertyName,
                                                                                                   propertyNameLen));
            if (!valueZVal)
                return false;
            if (valueZVal.isNullOrEmpty())
                return false;

            *value = valueZVal.getStringValue();
            return true;
        }

        bool HTTPPropertyGenerator::getPortNumberAsString(std::string* value,
                                                              const PHPExecEnvironment* execEnv,
                                                              const ZValPointerObject& zendHTTPURL)
        {
            ZValPointerAny valueZVal(zendHTTPURL.findProtectedPropertyByName<ZValPointerAny>(execEnv,
                                                                                             ZEND_URI_HTTP_PORT_PROPERTY_NAME,
                                                                                             ZEND_URI_HTTP_PORT_PROPERTY_NAME_LEN));
            switch (valueZVal.getType())
            {
                case ZValPointer::ZVal_String:
                    {
                        ZValPointerString valueStringVal(valueZVal.cast<ZValPointerString>());
                        if (valueStringVal.isNullOrEmpty())
                            return false;
                        *value = valueStringVal.getStringValue();
                        return true;
                    }
                    break;
                case ZValPointer::ZVal_Long:
                    {
                        ZValPointerLong valueLongVal(valueZVal.cast<ZValPointerLong>());
                        long valueLong = valueLongVal.getLongValue();
                        *value = boost::lexical_cast<std::string>(valueLong);
                        return true;
                    }
                    break;
            }
            return false;
        }

        static HTTPPropertyGenerator HTTPPropertyGenerator;
    }

    using namespace Private;

    HTTPClientRequestInterceptor::HTTPClientRequestInterceptor()
        : t_Base("Zend1::HTTPClientRequestInterceptor",
                 InterceptorRegistry::Zend1HttpClientRequestInterceptor_ID,
                 appdynamics::pb::Agent::EXIT_HTTP)
        , m_clearCorrelationHeader(false)
    {

    }

    void HTTPClientRequestInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
    {
        if (m_clearCorrelationHeader) {
            zval* rawThisZVal = execEnv->getInvokedObject();
            if (!rawThisZVal)
                return;

            ZValPointerObject thisZVal(ZValPointerAny::share(rawThisZVal).cast<ZValPointerObject>());
            if (!thisZVal)
                return;

            ZValPointerAny headersZVal(
                thisZVal.findProtectedPropertyByName<ZValPointerAny>(execEnv,
                                                                     ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME,
                                                                     ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME_LEN));
            if (!headersZVal)
                return;

            zval* headers = headersZVal.get();

#if PHP_VERSION_ID >= 70000
            zend_hash_str_del(Z_ARRVAL_P(headers), CorrelationHeader::MAIN_HEADER, CorrelationHeader::MAIN_HEADER_SIZE);
#else
            zend_hash_del(Z_ARRVAL_P(headers), const_cast<char*>(CorrelationHeader::MAIN_HEADER), CorrelationHeader::MAIN_HEADER_SIZE+1);
#endif
        }

        t_Base::onCallableEnd(execEnv, state);
    }

    CurrentExitCall* HTTPClientRequestInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                         uint64_t sequenceNumber) const
    {
        return new CurrentExitCall(sequenceNumber);
    }

    ExitCallInfo HTTPClientRequestInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                                const PHPExecEnvironment* execEnv) const
    {
        return AG(kernel)->getAgentContext()
                         ->getTransactionMonitor()
                         ->getExitPointDelegate(getExitPointType())
                         ->makeIdentifyingProperties(execEnv, exitCall, HTTPPropertyGenerator);
    }


    void HTTPClientRequestInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                                 const PHPExecEnvironment* phpExecEnv,
                                                                 const boost::shared_ptr<SnapshotExitCall>& sec) const
    {
        std::string url;
        if (HTTPPropertyGenerator::getURL(&url, phpExecEnv)) {
            sec->setDetailString(url);
            sec->addProperty("function", phpExecEnv->getCallableName());
        }
        sec->addProperties(exit_call->getSnapshotDebugProperties());
    }

    boost::shared_ptr<AErrorObject>
    HTTPClientRequestInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                               const PHPExecEnvironment* phpExecEnv)
    {
        boost::shared_ptr<AErrorObject> exceptionErrorObject(detectExceptions(currentExitCall,
                                                                              phpExecEnv));
        if (exceptionErrorObject)
            return exceptionErrorObject;

        zval* rawRetZVal = phpExecEnv->getReturnValue();
        if (!rawRetZVal)
            return boost::shared_ptr<AErrorObject>();
        ZValPointerObject retObject(ZValPointerAny::share(rawRetZVal).cast<ZValPointerObject>());
        if (!retObject)
            return boost::shared_ptr<AErrorObject>();

        ZValPointerLong resultCodeZVal(retObject.findProtectedPropertyByName<ZValPointerLong>(phpExecEnv,
                                                                                              ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME,
                                                                                              ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME_LEN));
        if (!resultCodeZVal)
            return boost::shared_ptr<AErrorObject>();

        long resultCode = resultCodeZVal.getLongValue();

        // All codes from 200 to 399 are not errors.
        if ((resultCode >= 200) && (resultCode < 400))
            return boost::shared_ptr<AErrorObject>();

        ZValPointerString errorStringZVal(retObject.findProtectedPropertyByName<ZValPointerString>(phpExecEnv,
                                                                                                   ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME,
                                                                                                   ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME_LEN));
        std::string errorMessage;
        if (errorStringZVal) {
            errorMessage = errorStringZVal.getStringValue();
        }
        else {
            errorMessage = std::string("Error code, ")
                         + boost::lexical_cast<std::string>(resultCode)
                         + ", no further details";
        }
        currentExitCall->incrementErrorCount(1);
        ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        return errorMonitor->reportSyntheticException(phpExecEnv, "Zend_HTTP_Client", errorMessage);
    }

    void HTTPClientRequestInterceptor::emitCorrelationInfo(TransactionContext* context,
                                                           CurrentExitCall* exitCall,
                                                           const PHPExecEnvironment* execEnv) const
    {
        zval* rawThisZVal = execEnv->getInvokedObject();
        if (!rawThisZVal)
            return;

        ZValPointerObject thisZVal(ZValPointerAny::share(rawThisZVal).cast<ZValPointerObject>());
        if (!thisZVal)
            return;

        ZValPointerArray headersZVal(
            thisZVal.findProtectedPropertyByName<ZValPointerArray>(execEnv,
                                                                   ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME,
                                                                   ZEND_HTTP_CLIENT_HEADERS_PROPERTY_NAME_LEN));
        if (!headersZVal)
            return;

        std::string corrHeaderString(TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
        exitCall->addSnapshotDebugProperty("Correlation Header", corrHeaderString);

        ZValPointerArray header(ZValPointerArray::create());
        header.pushEntry(CorrelationHeader::MAIN_HEADER);
        header.pushEntry(corrHeaderString);

        headersZVal.putEntryByName(CorrelationHeader::MAIN_HEADER, CorrelationHeader::MAIN_HEADER_SIZE, header);
        m_clearCorrelationHeader = true;
    }

}
