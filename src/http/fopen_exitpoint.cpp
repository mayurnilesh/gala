#include "fopen_exitpoint.h"

#include <iostream>
#include <boost/utility.hpp>
#include <boost/make_shared.hpp>

extern "C"
{
    #include <php.h>
    #include <ext/standard/url.h>
}

#include "services.h"
#include "timer.h"
#include "zval_helper.h"
#include "agent.h"
#include "httpexitpoint.h"

#include "current_exit_call.h"
#include "util.h"
#include "backend_resolver.h"

#include "emalloc_allocator.h"


extern php_stream_wrapper php_stream_http_wrapper;

/**
    AHTTPPropertyGenerator implementation for the fopen() interceptor. It
    obtains the URL from the connected resource, which is then used by
    AHTTPBackendResolver for the resolution.
 */
class FOpenPropertyGenerator : public AHTTPPropertyGenerator
{
    public:
        virtual bool getURL(const PHPExecEnvironment* execEnv,
                            std::string& url) const;
};

static FOpenPropertyGenerator fopenGenerator;

/* {{{ FOpenInterceptor methods */

FOpenInterceptor::FOpenInterceptor()
    : t_Base("FOpenInterceptor",
             InterceptorRegistry::FOpenExitPointInterceptor_ID,
             appdynamics::pb::Agent::EXIT_HTTP)
    {
}

bool FOpenInterceptor::resolveBackendOnCallableBegin() const
{
    return true;
}

bool FOpenInterceptor::resolveBackendOnCallableEnd() const
{
    return false;
}

void FOpenInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                 const PHPExecEnvironment* execEnv,
                                                 const boost::shared_ptr<SnapshotExitCall>& sec) const
{
    const boost::shared_ptr<AExitPointHandleInfo>& handleInfo = static_cast<const HandleBasedCurrentExitCall*>(exit_call)->getHandleInfo();
    const std::string& url = boost::static_pointer_cast<HTTPExitPointHandleInfo>(handleInfo)->getUrl();
    sec->setDetailString(url);
    sec->addProperty("function", "fopen");
    sec->addProperties(exit_call->getSnapshotDebugProperties());
}

void FOpenInterceptor::emitCorrelationInfo(TransactionContext* context,
                                           CurrentExitCall* exitCall,
                                           const PHPExecEnvironment* execEnv) const
{
    AG(G)->httpCorrelationContext.correlationHeader.assign(
        TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
    AG(G)->httpCorrelationContext.captureURLStreamOpener();
    exitCall->addSnapshotDebugProperty("Correlation Header",
                                       AG(G)->httpCorrelationContext.correlationHeader);
}

bool FOpenInterceptor::getHandle(ZValPointerAny* handlePointer,
                                 const PHPExecEnvironment* execEnv) const
{
    *handlePointer = ZValPointerAny::share(execEnv->getReturnValue());

    if (!(*handlePointer))
        return false;

    ZValPointerResource streamResource(handlePointer->cast<ZValPointerResource>());
    if (!streamResource)
        return false;

    return true;
}

bool FOpenInterceptor::initHandleInfo(boost::shared_ptr<AExitPointHandleInfo>* handleInfo,
                                      const ExitCallInfo* currentExitCallInfo,
                                      const PHPExecEnvironment* execEnv) const
{
    boost::shared_ptr<HTTPExitPointHandleInfo> httpHandleInfo;

    if (currentExitCallInfo) {
        httpHandleInfo = HTTPExitPointHandleInfo::create(*currentExitCallInfo);
    } else {
        // It's okay to pass NULL for exit call here, because our property
        // generator doesn't use it for anything.
        ExitCallInfo exitCallInfo(makeExitCallInfo(NULL, execEnv));
        if (!exitCallInfo.isValid())
            return false;
        httpHandleInfo = HTTPExitPointHandleInfo::create(exitCallInfo);
    }

    std::string url;
    if (fopenGenerator.getURL(execEnv, url))
    {
        httpHandleInfo->setUrl(url);
    }

    *handleInfo = httpHandleInfo;
    return true;
}

ExitCallInfo FOpenInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                const PHPExecEnvironment* execEnv) const
{
    return AG(kernel)->getAgentContext()
                     ->getTransactionMonitor()
                     ->getExitPointDelegate(getExitPointType())
                     ->makeIdentifyingProperties(execEnv, exitCall, fopenGenerator);
}

boost::shared_ptr<AErrorObject>
FOpenInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                               const PHPExecEnvironment* phpExecEnv)
{
    return AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, "HTTP", phpExecEnv);
}

/* }}} */


/* {{{ FileGetContentsInterceptor methods */

FileGetContentsInterceptor::FileGetContentsInterceptor()
    : t_Base("FileGetContentsInterceptor",
             InterceptorRegistry::FileGetContentsInterceptor_ID,
             appdynamics::pb::Agent::EXIT_HTTP)
{
}

CurrentExitCall*
FileGetContentsInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                  uint64_t sequenceNumber) const
{
    return new CurrentExitCall(sequenceNumber);
}

ExitCallInfo
FileGetContentsInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                             const PHPExecEnvironment* execEnv) const
{
    return AG(kernel)->getAgentContext()
                     ->getTransactionMonitor()
                     ->getExitPointDelegate(getExitPointType())
                     ->makeIdentifyingProperties(execEnv, exitCall, fopenGenerator);
}

void
FileGetContentsInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                      const PHPExecEnvironment* execEnv,
                                                      const boost::shared_ptr<SnapshotExitCall>& sec) const
{
    std::string url;
    if (fopenGenerator.getURL(execEnv, url))
    {
        sec->setDetailString(url);
        sec->addProperty("function", "file_get_contents");
    }
    sec->addProperties(exit_call->getSnapshotDebugProperties());
}

boost::shared_ptr<AErrorObject>
FileGetContentsInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                         const PHPExecEnvironment* phpExecEnv)
{
    return AExitCallInterceptor::detectLoggedErrorsDuringExitCall(currentExitCall, "HTTP", phpExecEnv);
}

void FileGetContentsInterceptor::emitCorrelationInfo(TransactionContext* context,
                                                     CurrentExitCall* exitCall,
                                                     const PHPExecEnvironment* execEnv) const
{
    AG(G)->httpCorrelationContext.correlationHeader.assign(
        TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
    AG(G)->httpCorrelationContext.captureURLStreamOpener();
    exitCall->addSnapshotDebugProperty("Correlation Header",
                                       AG(G)->httpCorrelationContext.correlationHeader);
}

/* }}} */

bool FOpenPropertyGenerator::getURL(const PHPExecEnvironment* execEnv,
                                    std::string& url) const
{
    // If the param count is wrong, then
    // we don't understand this method!
    if (execEnv->getParamCount() < 1)
        return false;

    ZValPointerString param0(execEnv->getParam<ZValPointerString>(0));
    if (!param0)
        return false;

    std::string streamUrl = param0.getStringValue();
    if (getHTTPUrlFromStreamUrl(url, streamUrl))
        return true;
    else
        return false;
}

// vim: set fdm=marker:
