/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __zend1_http_exitpoint_h
#define __zend1_http_exitpoint_h

#include "exitpoint.h"
#include "exitcall_detail.h"
#include "correlation.h"

namespace Zend1
{
    /**
        Interceptor for Zend_Http_Client::request.  Zend_Http_Client::request does
        a synchronous HTTP request using any number of different "adapters".  One of the
        possible adapters uses the php sockets API to make HTTP requests.  This interceptor
        reports Zend_Http_Client::request calls as HTTP exit calls otherwise we'd just
        detect socket exit calls ( assuming we ever implement support for detecting socket
        exit points! ).
     */
    class HTTPClientRequestInterceptor : public CorrelationEmitter<AExitCallInterceptor,
                                                                   HTTPClientRequestInterceptor,
                                                                   appdynamics::pb::Agent::EXIT_HTTP>
    {
        // Give base class template access to addSnapshotExitCallDetail and
        // emitCorrelationInfo.
        friend class CorrelationEmitter<AExitCallInterceptor,
                                       HTTPClientRequestInterceptor,
                                        appdynamics::pb::Agent::EXIT_HTTP>;
        friend class ExitCallDetailHelper<AExitCallInterceptor,
                                          HTTPClientRequestInterceptor,
                                          appdynamics::pb::Agent::EXIT_HTTP>;
        typedef CorrelationEmitter<AExitCallInterceptor,
                                   HTTPClientRequestInterceptor,
                                   appdynamics::pb::Agent::EXIT_HTTP> t_Base;
        public:
            HTTPClientRequestInterceptor();
            virtual void  onCallableEnd(const PHPExecEnvironment* execEnv,
                                        void* state);
        protected:
            virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                           uint64_t sequenceNumber) const;

            virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                                  const PHPExecEnvironment* execEnv) const;

            virtual bool resolveBackendOnCallableBegin() const { return true; }


            virtual bool resolveBackendOnCallableEnd() const { return false; }

            virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                                 const PHPExecEnvironment* phpExecEnv);
        private:
            /**
               Called by base class template to populate the specified SnapshotExitCall.
               @param exitCall The current exit call object.
               @param execEnv PHP execution environment.
               @param sec SnapshotExitCall to populate.
             */
            void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                           const PHPExecEnvironment* phpExecEnv,
                                           const boost::shared_ptr<SnapshotExitCall>& sec) const;

            void emitCorrelationInfo(TransactionContext* context,
                                     CurrentExitCall* exitCall,
                                     const PHPExecEnvironment* execEnv) const;

            mutable bool m_clearCorrelationHeader;
    };
}

#endif
