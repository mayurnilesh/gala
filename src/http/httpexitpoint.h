/*
   Copyright 2012 AppDynamics.
   All rights reserved.
 */
#ifndef __http_exitpoint_h
#define __http_exitpoint_h

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include "main/php_version.h"

#include "exitpoint.h"
#include "backend_resolver.h"
#include "current_exit_call.h"
#include "enumfactory.h"
#include "handle_registry.h"
#include "handle_tracking_exitpoint.h"

struct _zend_agent_globals;

/*
 * The types of exit points we support.
 */
#define HTTP_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL(XX) \
    XX(HTTPExitPoint_HOST,"HOST",) \
    XX(HTTPExitPoint_PORT,"PORT",) \
    XX(HTTPExitPoint_URL,"URL",)  \
    XX(HTTPExitPoint_QUERY_STRING,"QUERY STRING",)

DECLARE_ENUM(HTTPExitPointIndentifyingPropertyNames, HTTP_EXITPOINT_IDENTIFYING_PROPERTY_NAMES_DECL)

// True global
extern int curlResourceType;
extern int curlMultiResourceType;

class InterceptionEngine;
class FOpenInterceptor;
class FileGetContentsInterceptor;
class CurlInitInterceptor;
class CurlSetoptInterceptor;
class CurlSetoptArrayInterceptor;
class CurlExecInterceptor;
class CurlCloseInterceptor;
class CurlMultiAddHandleInterceptor;
class CurlMultiRemoveHandleInterceptor;
class CurlMultiExecInterceptor;
class CurlMultiSelectInterceptor;
class CurlMultiInfoReadInterceptor;

namespace Drupal7
{
    class HTTPRequestInterceptor;
    class StreamSocketClientInterceptor;
}

namespace Zend1
{
    class HTTPClientRequestInterceptor;
}

namespace Zend2
{
    class HTTPClientRequestInterceptor;
    class HTTPDoRequestInterceptor;
}

/*
 * This function is responsible for obtaining the real HTTP URL from the given
 * stream URL, which may contains multiple levels of wrappers, e.g. given this
 * stream URL:
 *    compress.zlib2://http://foo.com/
 * the function would return:
 *    http://foo.com/
 * Returns false if the stream URL does not contain an HTTP URL.
 */
bool getHTTPUrlFromStreamUrl(std::string& httpUrl, const std::string& streamUrl);

/**
    Exit point delegate that sets up the interceptors to
    detect HTTP backends.
 */
class HTTPExitPointDelegate : public AExitPointDelegate
{
public:
    HTTPExitPointDelegate(InterceptionEngine* interceptEngine,
                          TransactionMonitor* txMonitor);

    appdynamics::pb::Agent::ExitPointType getExitPointType() const
    {
        return appdynamics::pb::Agent::EXIT_HTTP;
    }

    void configure(const appdynamics::pb::Agent::BackendConfig_PHP& backendConfig,
                   const struct _zend_agent_globals* agentGlobals);

protected:
    void applyRules(const struct _zend_agent_globals* agentGlobals);

    bool processMatchRule(const PHPExecEnvironment* execEnv,
                          const appdynamics::pb::Agent::BackendMatchRule& matchRule,
                          const StringMap& properties) const;

    void processNamingRule(const PHPExecEnvironment* execEnv,
                           const appdynamics::pb::Agent::BackendRule& backendRule,
                           const StringMap& properties,
                           StringMap& identifyingProperties) const;

private:
    static bool s_didApplyRules;
};


/**
 * HTTP-specific subclass that tracks additional information.
 */
class HTTPExitPointHandleInfo : public AExitPointHandleInfo
{
        friend class HandleRegistry;

        static const TypeTag typeTag;
    public:
        template <class T, class Arg1, class... Args>
            friend typename boost::detail::sp_if_not_array<T>::type boost::make_shared(Arg1 &&, Args &&...);
        static inline boost::shared_ptr<HTTPExitPointHandleInfo> create(const ExitCallInfo& exitCallInfo) { return boost::make_shared<HTTPExitPointHandleInfo>(exitCallInfo); }
        inline void setUrl(const std::string& url)
        {
            m_httpUrl = url;
        }
        inline const std::string& getUrl() const { return m_httpUrl; }

    protected:
        inline HTTPExitPointHandleInfo(const ExitCallInfo& exitCallInfo)
            : AExitPointHandleInfo(exitCallInfo, &typeTag)
        {
        }

    private:
        std::string m_httpUrl;
};

/*
 * A base class for HTTP property generators. Given a URL, parses it to extract
 * potential identifying properties. Calls getURL() virtual method to obtain the
 * actual URL from the derived classes.
 */
class AHTTPPropertyGenerator : public IBackendPropertyGenerator
{
    public:
       virtual bool getProperties(const PHPExecEnvironment* execEnv,
                                  const CurrentExitCall* exitCall,
                                  StringMap& properties) const;

       virtual bool getURL(const PHPExecEnvironment* execEnv,
                           std::string& url) const = 0;
};

/*
 * An exit call class for HTTP connections. Stores the URL that the connection
 * operates on.
 */
class HTTPCurrentExitCall : public CurrentExitCall {
    public:
        HTTPCurrentExitCall(uint64_t sequenceNumber) : CurrentExitCall(sequenceNumber) {}
        virtual ~HTTPCurrentExitCall() {}

        inline void setURL(const std::string& url) { m_url = url; }
        inline const std::string& getURL() const { return m_url; }

    private:
        std::string m_url;
};

#endif
