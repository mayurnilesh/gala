#include "http_correlation.h"
#include "agent.h"
#include "correlation.h"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>

static inline void phpStreamSetContextOption(php_stream_context* context,
                                             const char* category,
                                             const char* option,
                                             const std::string& newValue)
{
    // php_stream_context_set_option copies the zval contents, so we don't
    // need to copy the string here
    zval newValueZVal;
    APPD_ZVAL_STRINGL_DUP_FALSE(&newValueZVal, const_cast<char*>(newValue.c_str()), newValue.length());
    php_stream_context_set_option(context, category, option, &newValueZVal);
}
#if PHP_VERSION_ID >= 70000
php_stream* agentURLStreamOpener(php_stream_wrapper *wrapper, t_StreamCharType path, t_StreamCharType mode,
                                 int options, zend_string **opened_path, php_stream_context *context STREAMS_DC TSRMLS_DC)
{
#else
php_stream* agentURLStreamOpener(php_stream_wrapper *wrapper, t_StreamCharType path, t_StreamCharType mode,
                                 int options, char **opened_path, php_stream_context *context STREAMS_DC TSRMLS_DC)
{
#endif
    // Simply proxy to the original opener if there is no correlation header.
    if (AG(G)->httpCorrelationContext.correlationHeader.empty())
        return AG(G)->httpCorrelationContext.realURLStreamOpener(wrapper, path, mode, options, opened_path, context STREAMS_CC TSRMLS_CC);

    php_stream* stream = NULL;
    bool volatile inBailout = false;

    {
        std::string header;
        TransactionCorrelator::makeFullHeader(header, AG(G)->httpCorrelationContext.correlationHeader);

#if PHP_VERSION_ID >= 70000

        zval* httpCategoryPtr = NULL;
        zval categoryOrig;
        zval categoryCopy;

        // zend_hash_str_find/update/del/etc. takes strlen *without* terminating null byte
        httpCategoryPtr = zend_hash_str_find(Z_ARRVAL(context->options), "http", sizeof("http") - 1);
        if (httpCategoryPtr) {
            categoryOrig = *httpCategoryPtr;
            categoryCopy = categoryOrig;
            *httpCategoryPtr = categoryCopy;
        }
#else
        // The context may or may not have "http" category already. If it does, we
        // copy the "http" options array and replace it with our own.
        zval** httpCategoryPtr = NULL;
        zval* volatile categoryOrig = NULL;
        zval* categoryCopy = NULL;

        // TODO make this code use zval_helper ( adding features to zval_helper if needed ).
        if (SUCCESS == zend_hash_find(Z_ARRVAL_P(context->options), "http", sizeof("http"), (void**)&httpCategoryPtr)) {
            // Copy the "http" category array from the incoming context and
            // make the it point to the copy.
            categoryOrig = *httpCategoryPtr;
            ALLOC_INIT_ZVAL(categoryCopy);
            *categoryCopy = *categoryOrig;
            zval_copy_ctor(categoryCopy);
            INIT_PZVAL(categoryCopy);
            *httpCategoryPtr = categoryCopy;
        }
#endif

        // Needed to work around gcc 4.4 bug on 32-bit linux.
        static const char* const crlf = "\r\n";

#if PHP_VERSION_ID >= 70000
        zval* option = php_stream_context_get_option(context, "http", "header");
        if (option != NULL) {
            ZValPointerAny optionZVal(ZValPointerAny::share(option));
#else
        zval** option = NULL;
        if (php_stream_context_get_option(context, "http", "header", &option) == SUCCESS) {
            ZValPointerAny optionZVal(ZValPointerAny::share(*option));
#endif
            ZValPointer::ZValType const optionType = optionZVal.getType();
            if (optionType == ZValPointer::ZVal_String) {
                // Existing header is a string, need to append new header to the end.
                std::string existingHeader(optionZVal.cast<ZValPointerString>().getStringValue());
                boost::algorithm::trim_right_if(existingHeader, boost::algorithm::is_any_of(crlf));

                std::string const newHeader(existingHeader + crlf + header + crlf);
                phpStreamSetContextOption(context, "http", "header", newHeader);
            }
            else if (optionType == ZValPointer::ZVal_Array) {
                ZValPointerString headerZVal(ZValPointerString::copy(header));
                ZValPointerArray optionArray(optionZVal.cast<ZValPointerArray>());
                optionArray.pushEntry(headerZVal);
            }
        } else {
            phpStreamSetContextOption(context, "http", "header", header + crlf);
        }

        // Clear the header in case there is a recursive HTTP call (from user
        // wrapper, for example) back here.
        AG(G)->httpCorrelationContext.correlationHeader.clear();
        zend_try {
            stream = AG(G)->httpCorrelationContext.realURLStreamOpener(wrapper, path, mode, options, opened_path, context STREAMS_CC TSRMLS_CC);
        }
        zend_catch {
            inBailout = true;
        }
        zend_end_try();

        if (httpCategoryPtr == NULL) {
            // We did not replace the "http" category with a copy, so we can
            // just blow away the auto-created category array.
#if PHP_VERSION_ID >= 70000
            // zend_hash_str_find/update/del/etc. takes strlen *without* terminating null byte
            zend_hash_str_del(Z_ARRVAL(context->options), "http", sizeof("http") - 1);
#else
            zend_hash_del(Z_ARRVAL_P(context->options), "http", sizeof("http"));
#endif
        } else {
            // Replace the "http" category array with the original
            *httpCategoryPtr = categoryOrig;
#if PHP_VERSION_ID < 70000
            // ctor not called on categoryCopy in PHP 7.
            zval_ptr_dtor(&categoryCopy);
#endif
        }
    }
    if (inBailout) {
        LONGJMP(*EG(bailout), FAILURE);
    }

    return stream;
}
