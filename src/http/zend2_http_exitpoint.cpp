/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/

/*
 * Reduced array-bounds to a warning due to boost/gcc issues
 * described here:
 * https://bugzilla.redhat.com/show_bug.cgi?id=555771
 * As long as we build on centos5, we're stuck.
 */

#pragma GCC diagnostic warning "-Warray-bounds"

#include "main/php_version.h"

#if PHP_VERSION_ID >= 50300

#include "zend2_http_exitpoint.h"
#include "httpexitpoint.h"
#include "emalloc_allocator.h"
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

static const char ZEND2_HTTP_CLIENT_SYMBOL[] = "Zend\\Http\\Client";
static const char ZEND2_DO_REQUEST_METHOD[] = "doRequest";

namespace Zend2
{
    namespace Private
    {
        inline bool getProtectedStringProperty(std::string* value,
                                               const PHPExecEnvironment* execEnv,
                                               const ZValPointerObject& obj,
                                               const char* propertyName,
                                               size_t propertyNameLen)
        {
            BOOST_ASSERT(value);
            ZValPointerString valueZVal(obj.findProtectedPropertyByName<ZValPointerString>(execEnv,
                                                                                           propertyName,
                                                                                           propertyNameLen));
            if (!valueZVal)
                return false;
            if (valueZVal.isNullOrEmpty())
                return false;

            *value = valueZVal.getStringValue();
            return true;
        }

        class HTTPPropertyGenerator : public IBackendPropertyGenerator
        {
            public:
                virtual bool getProperties(const PHPExecEnvironment* execEnv,
                                           const CurrentExitCall* exitCall,
                                           StringMap& properties) const;

                static bool getURL(std::string* url,
                                   const PHPExecEnvironment* execEnv);

                static ZValPointerObject castToZendHTTPURL(const PHPExecEnvironment* execEnv,
                                                           const ZValPointerAny& val);

                static ZValPointerObject getHTTPURIObject(const PHPExecEnvironment* execEnv);

            private:

                static bool getPortNumberAsString(std::string* value,
                                                  const PHPExecEnvironment* execEnv,
                                                  const ZValPointerObject& zendHTTPURL);

                static ZValPointerObject getHTTPRequestObject(const PHPExecEnvironment* execEnv);

                /**
                    Helper class that deals with the fact that sometimes
                    we have to parse the user/password out of the "userinfo" field
                    of the Zend\Uri\Http class.
                 */
                class ZendHTTPURLAuthInfo : boost::noncopyable
                {
                public:
                    ZendHTTPURLAuthInfo(const PHPExecEnvironment* execEnv,
                                        const ZValPointerObject& zendHTTPURL);
                    bool getUsername(std::string* username);
                    bool getPassword(std::string* password);
                private:
                    const std::string& getUserInfo();

                    const PHPExecEnvironment* const m_execEnv;
                    ZValPointerObject m_zendHTTPURL;
                    bool m_didGetUserInfo;
                    std::string m_userInfo;

                };

        };

        static const char ZEND_HTTP_CLIENT_REQUEST_PROPERTY_NAME[] = "request";
        static const char ZEND_HTTP_REQUEST_URI_PROPERTY_NAME[] = "uri";
        static const char ZEND_URI_HTTP_CLASS_NAME[] = "Zend\\Uri\\Http";
        static const char ZEND_URI_HTTP_SCHEME_PROPERTY_NAME[] = "scheme";
        static const char ZEND_URI_HTTP_USERINFO_PROPERTY_NAME[] = "userInfo";
        static const char ZEND_URI_HTTP_USER_PROPERTY_NAME[] = "user";
        static const char ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME[] = "password";
        static const char ZEND_URI_HTTP_HOST_PROPERTY_NAME[] = "host";
        static const char ZEND_URI_HTTP_PORT_PROPERTY_NAME[] = "port";
        static const char ZEND_URI_HTTP_PATH_PROPERTY_NAME[] = "path";
        static const char ZEND_URI_HTTP_QUERY_PROPERTY_NAME[] = "query";
        static const char ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME[] = "statusCode";
        static const char ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME[] = "reasonPhrase";
        static const char ZEND_HTTP_RESPONSE_RECOMMENDED_MESSAGES_PROPERTY_NAME[] = "recommendedReasonPhrases";

        bool HTTPPropertyGenerator::getProperties(const PHPExecEnvironment* execEnv,
                                          const CurrentExitCall* exitCall,
                                          StringMap& properties) const
        {
            ZValPointerObject zendHTTPURL(getHTTPURIObject(execEnv));
            if (!zendHTTPURL)
                return false;

            std::string host;
            if (getProtectedStringProperty(&host,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_HOST_PROPERTY_NAME,
                                           strlen(ZEND_URI_HTTP_HOST_PROPERTY_NAME))) {
                if (host.size() > MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY)
                   host.resize(MAX_HOST_NAME_LEN_IN_IDENTIFYING_PROPERTY);
                properties[enum2str(HTTPExitPoint_HOST)] = host;
            }

            std::string portAsString;
            if (getPortNumberAsString(&portAsString,
                                      execEnv,
                                      zendHTTPURL)) {
                properties[enum2str(HTTPExitPoint_PORT)] = portAsString;
            }

            std::string path;
            if (getProtectedStringProperty(&path,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_PATH_PROPERTY_NAME,
                                           strlen(ZEND_URI_HTTP_PATH_PROPERTY_NAME))) {
                boost::trim_right_if(path, boost::is_from_range('/', '/'));
                if (path.size() > MAX_URL_LEN_IN_IDENTIFYING_PROPERTY)
                    path.resize(MAX_URL_LEN_IN_IDENTIFYING_PROPERTY);
                if (path.empty())
                    path = "/";
                properties[enum2str(HTTPExitPoint_URL)] = path;
            }

            std::string query;
            if (getProtectedStringProperty(&query,
                                           execEnv,
                                           zendHTTPURL,
                                           ZEND_URI_HTTP_QUERY_PROPERTY_NAME,
                                           strlen(ZEND_URI_HTTP_QUERY_PROPERTY_NAME))) {
                properties[enum2str(HTTPExitPoint_QUERY_STRING)] = query;
            }

            return true;
        }

        bool HTTPPropertyGenerator::getURL(std::string* url,
                                   const PHPExecEnvironment* execEnv)
        {
            ZValPointerObject zendHTTPURL(getHTTPURIObject(execEnv));
            if (!zendHTTPURL)
                return false;

            std::string scheme;
            if (!getProtectedStringProperty(&scheme,
                                            execEnv,
                                            zendHTTPURL,
                                            ZEND_URI_HTTP_SCHEME_PROPERTY_NAME,
                                            strlen(ZEND_URI_HTTP_SCHEME_PROPERTY_NAME)))
                return false;

            std::string host;
            if (!getProtectedStringProperty(&host,
                                            execEnv,
                                            zendHTTPURL,
                                            ZEND_URI_HTTP_HOST_PROPERTY_NAME,
                                            strlen(ZEND_URI_HTTP_HOST_PROPERTY_NAME)))
                return false;

            std::string portAsString;
            if (!getPortNumberAsString(&portAsString,
                                       execEnv,
                                       zendHTTPURL))
                return false;

            std::string auth;
            {
                ZendHTTPURLAuthInfo authInfo(execEnv, zendHTTPURL);
                std::string password;
                if (authInfo.getPassword(&password))
                    password = ":" + std::string(password.size(), '*');

                if (authInfo.getUsername(&auth)) {
                    auth += password;
                    auth += '@';
                }
            }

            std::string path;
            getProtectedStringProperty(&path,
                                       execEnv,
                                       zendHTTPURL,
                                       ZEND_URI_HTTP_PATH_PROPERTY_NAME,
                                       strlen(ZEND_URI_HTTP_PATH_PROPERTY_NAME));

            std::string query;
            getProtectedStringProperty(&query,
                                       execEnv,
                                       zendHTTPURL,
                                       ZEND_URI_HTTP_QUERY_PROPERTY_NAME,
                                       strlen(ZEND_URI_HTTP_QUERY_PROPERTY_NAME));
            *url = scheme
                 + "://"
                 + auth
                 + host
                 + ":"
                 + portAsString
                 + path
                 + query;
            return true;
        }

        ZValPointerObject HTTPPropertyGenerator::castToZendHTTPURL(const PHPExecEnvironment* execEnv,
                                                           const ZValPointerAny& val)
        {
            ZValPointerObject obj(val.cast<ZValPointerObject>());
            if (!obj)
                return obj;

            zend_class_entry* const zendUriHttpClass =
                execEnv->lookupClass(ZEND_URI_HTTP_CLASS_NAME,
                                     strlen(ZEND_URI_HTTP_CLASS_NAME));
            // If the Zend\Uri\Http class does not yet exist, then there are
            // no instances of Zend\Uri\Http.
            if (!zendUriHttpClass)
                return ZValPointerObject();
            if (!obj.isInstanceOf(execEnv, zendUriHttpClass))
                return ZValPointerObject();

            return obj;
        }

        ZValPointerObject HTTPPropertyGenerator::getHTTPURIObject(const PHPExecEnvironment* execEnv)
        {
            ZValPointerObject requestObject(getHTTPRequestObject(execEnv));
            if (!requestObject)
                return ZValPointerObject();


            ZValPointerObject uriZVal(requestObject.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                                   ZEND_HTTP_REQUEST_URI_PROPERTY_NAME,
                                                                                                   strlen(ZEND_HTTP_REQUEST_URI_PROPERTY_NAME)));
            return castToZendHTTPURL(execEnv, uriZVal);
        }

        bool HTTPPropertyGenerator::getPortNumberAsString(std::string* value,
                                                  const PHPExecEnvironment* execEnv,
                                                  const ZValPointerObject& zendHTTPURL)
        {
            ZValPointerAny valueZVal(zendHTTPURL.findProtectedPropertyByName<ZValPointerAny>(execEnv,
                                                                                             ZEND_URI_HTTP_PORT_PROPERTY_NAME,
                                                                                             strlen(ZEND_URI_HTTP_PORT_PROPERTY_NAME)));
            switch (valueZVal.getType())
            {
                case ZValPointer::ZVal_String:
                    {
                        ZValPointerString valueStringVal(valueZVal.cast<ZValPointerString>());
                        if (valueStringVal.isNullOrEmpty())
                            return false;
                        *value = valueStringVal.getStringValue();
                        return true;
                    }
                    break;
                case ZValPointer::ZVal_Long:
                    {
                        ZValPointerLong valueLongVal(valueZVal.cast<ZValPointerLong>());
                        long valueLong = valueLongVal.getLongValue();
                        *value = boost::lexical_cast<std::string>(valueLong);
                        return true;
                    }
                    break;
                case ZValPointer::ZVal_Null:
                    {
                        // Ugh....  Often, the port field will be null, so
                        // we have to use the scheme to figure out the port number.
                        std::string scheme;
                        if (!getProtectedStringProperty(&scheme,
                                                        execEnv,
                                                        zendHTTPURL,
                                                        ZEND_URI_HTTP_SCHEME_PROPERTY_NAME,
                                                        strlen(ZEND_URI_HTTP_SCHEME_PROPERTY_NAME)))
                            return false;
                        if (scheme == "http") {
                            *value = "80";
                            return true;
                        }
                        else if ( scheme == "https") {
                            *value = "443";
                            return true;
                        }
                    }
                    break;
            }
            return false;
        }

        ZValPointerObject HTTPPropertyGenerator::getHTTPRequestObject(const PHPExecEnvironment* execEnv)
        {
            // The method we intercept, send, can receive the request
            // via an optional argument.
            if (execEnv->getParamCount() > 0) {
                ZValPointerObject requestObject(execEnv->getParam<ZValPointerObject>(0));
                if (requestObject)
                    return requestObject;

            }
            // If the send method was not passed a non-null request object, then
            // the send methods gets the request object from the 'request' field.
            zval* rawThisZVal = execEnv->getInvokedObject();
            if (!rawThisZVal)
                return ZValPointerObject();

            ZValPointerObject thisZVal(ZValPointerAny::share(rawThisZVal).cast<ZValPointerObject>());
            ZValPointerObject requestObject(thisZVal.findProtectedPropertyByName<ZValPointerObject>(execEnv,
                                                                                                    ZEND_HTTP_CLIENT_REQUEST_PROPERTY_NAME,
                                                                                                    strlen(ZEND_HTTP_CLIENT_REQUEST_PROPERTY_NAME)));
            return requestObject;
        }

        HTTPPropertyGenerator::ZendHTTPURLAuthInfo::ZendHTTPURLAuthInfo(const PHPExecEnvironment* execEnv,
                                                                const ZValPointerObject& zendHTTPURL)
            : m_execEnv(execEnv)
            , m_zendHTTPURL(zendHTTPURL)
            , m_didGetUserInfo(false)
        {
        }

        bool HTTPPropertyGenerator::ZendHTTPURLAuthInfo::getUsername(std::string* username)
        {
            if (getProtectedStringProperty(username,
                                           m_execEnv,
                                           m_zendHTTPURL,
                                           ZEND_URI_HTTP_USER_PROPERTY_NAME,
                                           strlen(ZEND_URI_HTTP_USER_PROPERTY_NAME)))
                return true;
            const std::string& userInfo = getUserInfo();
            if (userInfo.empty())
                return false;
            std::string::size_type firstColon = userInfo.find(':');
            if (firstColon == std::string::npos) {
                *username = userInfo;
                return true;
            }
            else if (firstColon != 0) {
                *username = userInfo.substr(0, firstColon);
                return true;
            }
            return false;
        }


        bool HTTPPropertyGenerator::ZendHTTPURLAuthInfo::getPassword(std::string* password)
        {
            if (getProtectedStringProperty(password,
                                           m_execEnv,
                                           m_zendHTTPURL,
                                           ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME,
                                           strlen(ZEND_URI_HTTP_PASSWORD_PROPERTY_NAME)))
                return true;
            const std::string& userInfo = getUserInfo();
            if (userInfo.empty())
                return false;
            std::string::size_type firstColon = userInfo.find(':');
            if (firstColon == std::string::npos)
                return false;
            std::string::size_type passwordLen = (userInfo.length() - firstColon) - 1;
            if (passwordLen == 0)
                return false;
            *password = userInfo.substr(firstColon + 1, passwordLen);
            return true;
        }


        const std::string& HTTPPropertyGenerator::ZendHTTPURLAuthInfo::getUserInfo()
        {
            if (m_didGetUserInfo)
                return m_userInfo;
            m_didGetUserInfo = true;
            getProtectedStringProperty(&m_userInfo,
                                       m_execEnv,
                                       m_zendHTTPURL,
                                       ZEND_URI_HTTP_USERINFO_PROPERTY_NAME,
                                       strlen(ZEND_URI_HTTP_USERINFO_PROPERTY_NAME));
            return m_userInfo;
        }

        static HTTPPropertyGenerator HTTPPropertyGenerator;
    }

    using namespace Private;

    HTTPClientRequestInterceptor::HTTPClientRequestInterceptor()
        : t_Base("Zend2::HTTPClientRequestInterceptor",
                 InterceptorRegistry::Zend2HttpClientRequestInterceptor_ID,
                 appdynamics::pb::Agent::EXIT_HTTP)
    {
    }

    CurrentExitCall* HTTPClientRequestInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                         uint64_t sequenceNumber) const
    {
        return new CurrentExitCall(sequenceNumber);
    }

    ExitCallInfo HTTPClientRequestInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                                const PHPExecEnvironment* execEnv) const
    {
        return AG(kernel)->getAgentContext()
                         ->getTransactionMonitor()
                         ->getExitPointDelegate(getExitPointType())
                         ->makeIdentifyingProperties(execEnv, exitCall, HTTPPropertyGenerator);
    }


    void HTTPClientRequestInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                                 const PHPExecEnvironment* phpExecEnv,
                                                                 const boost::shared_ptr<SnapshotExitCall>& sec) const
    {
        std::string url;
        if (HTTPPropertyGenerator::getURL(&url, phpExecEnv))
        {
            sec->setDetailString(url);
            sec->addProperty("function", phpExecEnv->getCallableName());
        }
        sec->addProperties(exit_call->getSnapshotDebugProperties());
    }

    boost::shared_ptr<AErrorObject>
    HTTPClientRequestInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                               const PHPExecEnvironment* phpExecEnv)
    {
        boost::shared_ptr<AErrorObject> exceptionErrorObject(detectExceptions(currentExitCall,
                                                                              phpExecEnv));
        if (exceptionErrorObject)
            return exceptionErrorObject;

        zval* rawRetZVal = phpExecEnv->getReturnValue();
        if (!rawRetZVal)
            return boost::shared_ptr<AErrorObject>();
        ZValPointerObject retObject(ZValPointerAny::share(rawRetZVal).cast<ZValPointerObject>());
        if (!retObject)
            return boost::shared_ptr<AErrorObject>();

        ZValPointerLong resultCodeZVal(retObject.findProtectedPropertyByName<ZValPointerLong>(phpExecEnv,
                                                                                              ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME,
                                                                                              strlen(ZEND_HTTP_RESPONSE_CODE_PROPERTY_NAME)));
        if (!resultCodeZVal)
            return boost::shared_ptr<AErrorObject>();

        long resultCode = resultCodeZVal.getLongValue();

        // All codes from 200 to 399 are not errors.
        if ((resultCode >= 200) && (resultCode < 400))
            return boost::shared_ptr<AErrorObject>();

        std::string errorMessage;
        if (!getProtectedStringProperty(&errorMessage,
                                        phpExecEnv,
                                        retObject,
                                        ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME,
                                        strlen(ZEND_HTTP_RESPONSE_MESSAGE_PROPERTY_NAME))) {
            ZValPointerArray recommendedMessages(retObject.findProtectedPropertyByName<ZValPointerArray>(phpExecEnv,
                                                                                                         ZEND_HTTP_RESPONSE_RECOMMENDED_MESSAGES_PROPERTY_NAME,
                                                                                                         strlen(ZEND_HTTP_RESPONSE_RECOMMENDED_MESSAGES_PROPERTY_NAME)));
            if (recommendedMessages) {
                ZValPointerString recommendedMessage(recommendedMessages.findEntryByLong<ZValPointerString>(resultCode));
                if (recommendedMessage)
                    errorMessage = recommendedMessage.getStringValue();
            }
        }
        if (errorMessage.empty())
             errorMessage = std::string("Error code, ")
                         + boost::lexical_cast<std::string>(resultCode)
                         + ", no further details";

        currentExitCall->incrementErrorCount(1);
        ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        return errorMonitor->reportSyntheticException(phpExecEnv, "Zend\\Http\\Client", errorMessage);
    }

    void HTTPClientRequestInterceptor::emitCorrelationInfo(TransactionContext* context,
                                                           CurrentExitCall* exitCall,
                                                           const PHPExecEnvironment* execEnv) const
    {
        std::string corrHeaderValue(TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
        exitCall->addSnapshotDebugProperty("Correlation Header", corrHeaderValue);
        AG(G)->httpCorrelationContext.correlationHeader = corrHeaderValue;

        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->addInterceptorForCallable(CallableInfo(ZEND2_HTTP_CLIENT_SYMBOL,
                                                                ZEND2_DO_REQUEST_METHOD),
                                                   &zend2HTTPDoRequestInterceptor);
    }

    void* HTTPDoRequestInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
    {
        ZValPointerString headerValue(ZValPointerString::copy(AG(G)->httpCorrelationContext.correlationHeader));
        AG(G)->httpCorrelationContext.correlationHeader.clear();
        if (headerValue.isNullOrEmpty())
            return NULL;

        if (execEnv->getParamCount() < 4)
            return NULL;

        ZValPointerArray headersArray(execEnv->getParam<ZValPointerArray>(3));
        if (!headersArray)
            return NULL;

        headersArray.putEntryByName(CorrelationHeader::MAIN_HEADER,
                                    CorrelationHeader::MAIN_HEADER_SIZE,
                                    headerValue);

        return NULL;
    }

    void HTTPDoRequestInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
    {
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->removeInterceptorForCallable(CallableInfo(ZEND2_HTTP_CLIENT_SYMBOL,
                                                                   ZEND2_DO_REQUEST_METHOD),
                                                      this);
    }

}

#endif
