/*
   Copyright 2013 AppDynamics.
   All rights reserved.
*/
#ifndef __drupal7_http_exitpoint_h
#define __drupal7_http_exitpoint_h

#include "exitpoint.h"
#include "exitcall_detail.h"
#include "correlation.h"

namespace Drupal7
{
    class StreamSocketClientInterceptor : public AMethodInterceptor
    {
        public:
            StreamSocketClientInterceptor()
                : AMethodInterceptor("StreamSocketClientInterceptor",
                                     InterceptorRegistry::Drupal7StreamSocketClientInterceptor_ID)
            {
            }

            void* onCallableBegin(const PHPExecEnvironment* execEnv);

            void onCallableEnd(const PHPExecEnvironment* execEnv, void* state);

            virtual inline bool needParamsInOnMethodBegin() const { return false; }
            virtual inline bool shouldCallOnMethodEnd() const { return false; }
            virtual bool needReturnValueInOnMethodEnd() const { return false; }

            bool isActive(const PHPExecEnvironment* execEnv) const
            {
                return execEnv->getAgentGlobals().isExitPointEnabled(appdynamics::pb::Agent::EXIT_HTTP);
            }
    };


    /**
        Interceptor for drupal_http_request.  drupal_http_request does
        a synchronous HTTP request using the php socket API.  This interceptor
        reports drupal_http_request calls as HTTP exit calls otherwise we'd just
        detect socket exit calls ( assuming we ever implement support for detecting socket
        exit points! ).
     */
    class HTTPRequestInterceptor : public CorrelationEmitter<AExitCallInterceptor,
                                                             HTTPRequestInterceptor,
                                                             appdynamics::pb::Agent::EXIT_HTTP>
    {
        // Give base class template access to addSnapshotExitCallDetail
        // and emitCorrelationInfo.
        friend class CorrelationEmitter<AExitCallInterceptor,
                                        HTTPRequestInterceptor,
                                        appdynamics::pb::Agent::EXIT_HTTP>;
        friend class ExitCallDetailHelper<AExitCallInterceptor,
                                          HTTPRequestInterceptor,
                                          appdynamics::pb::Agent::EXIT_HTTP>;
        typedef CorrelationEmitter<AExitCallInterceptor,
                                   HTTPRequestInterceptor,
                                   appdynamics::pb::Agent::EXIT_HTTP> t_Base;
        public:
            HTTPRequestInterceptor();
        protected:
            virtual CurrentExitCall* createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                           uint64_t sequenceNumber) const;

            virtual ExitCallInfo makeExitCallInfo(const CurrentExitCall* exitCall,
                                                  const PHPExecEnvironment* execEnv) const;

            virtual bool resolveBackendOnCallableBegin() const { return true; }


            virtual bool resolveBackendOnCallableEnd() const { return false; }

            virtual boost::shared_ptr<AErrorObject> detectErrors(CurrentExitCall* currentExitCall,
                                                                 const PHPExecEnvironment* phpExecEnv);
        private:

            /**
               Called by base class template to populate the specified SnapshotExitCall.
               @param exitCall The current exit call object.
               @param execEnv PHP execution environment.
               @param sec SnapshotExitCall to populate.
             */
            void addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                           const PHPExecEnvironment* phpExecEnv,
                                           const boost::shared_ptr<SnapshotExitCall>& sec) const;

            void emitCorrelationInfo(TransactionContext* context,
                                     CurrentExitCall* exitCall,
                                     const PHPExecEnvironment* execEnv) const;
    };
}

extern Drupal7::StreamSocketClientInterceptor drupal7StreamSocketClientInterceptor;

#endif
