/*
   Copyright 2012 AppDynamics.
   All rights reserved.
*/

#include "drupal7_http_exitpoint.h"
#include "httpexitpoint.h"
#include "emalloc_allocator.h"
#include "callgraph/exec_stack_frame.h"
#include <boost/lexical_cast.hpp>

namespace Drupal7
{
    static const char HTTP_REQUEST_SYMBOL[] = "drupal_http_request";
    static const char STREAM_SOCKET_CLIENT_FUNC[] = "stream_socket_client";

    namespace Private
    {
        class HTTPGenerator : public AHTTPPropertyGenerator
        {
            public:
                virtual bool getURL(const PHPExecEnvironment* phpExecEnv,
                                    std::string& url) const;
        };

        bool HTTPGenerator::getURL(const PHPExecEnvironment* phpExecEnv,
                                   std::string& url) const
        {
            if (phpExecEnv->getParamCount() < 1)
                return false;
            ZValPointerString urlZVal(phpExecEnv->getParam<ZValPointerString>(0));
            if (!urlZVal)
                return false;
            url = urlZVal.getStringValue();
            return true;
        }

        static HTTPGenerator httpGenerator;
    }

    using namespace Private;

    HTTPRequestInterceptor::HTTPRequestInterceptor()
        : t_Base("Drupal7::HTTPRequestInterceptor",
                 InterceptorRegistry::Drupal7HTTPRequestInterceptor_ID,
                 appdynamics::pb::Agent::EXIT_HTTP)
    {
    }

    CurrentExitCall* HTTPRequestInterceptor::createCurrentExitCall(const PHPExecEnvironment* execEnv,
                                                                   uint64_t sequenceNumber) const
    {
        return new CurrentExitCall(sequenceNumber);
    }

    ExitCallInfo HTTPRequestInterceptor::makeExitCallInfo(const CurrentExitCall* exitCall,
                                                          const PHPExecEnvironment* execEnv) const
    {
        return AG(kernel)->getAgentContext()
                         ->getTransactionMonitor()
                         ->getExitPointDelegate(getExitPointType())
                         ->makeIdentifyingProperties(execEnv, exitCall, httpGenerator);
    }


    void HTTPRequestInterceptor::addSnapshotExitCallDetail(const CurrentExitCall* exit_call,
                                                           const PHPExecEnvironment* phpExecEnv,
                                                           const boost::shared_ptr<SnapshotExitCall>& sec) const
    {
        std::string url;
        if (httpGenerator.getURL(phpExecEnv, url))
        {
            sec->setDetailString(url);
            sec->addProperty("function", "drupal_http_request");
        }
        sec->addProperties(exit_call->getSnapshotDebugProperties());
    }

    /**
        Helper function to convert HTTP status codes to an integer.
     */
    static inline bool getResultCode(long* resultCode, const ZValPointerAny& val)
    {
        switch (val.getType()) {
            case ZValPointer::ZVal_Long:
                *resultCode = val.cast<ZValPointerLong>().getLongValue();
                return true;
            case ZValPointer::ZVal_String:
            {
                try {
                    std::string valString(val.cast<ZValPointerString>().getStringValue());
                    *resultCode = boost::lexical_cast<long>(valString);
                    return true;
                }
                catch (...) {
                    return false;
                }
            }
            default:
                return false;
        }
    }

    boost::shared_ptr<AErrorObject>
    HTTPRequestInterceptor::detectErrors(CurrentExitCall* currentExitCall,
                                         const PHPExecEnvironment* phpExecEnv)
    {
        zval* rawRetZVal = phpExecEnv->getReturnValue();
        if (!rawRetZVal)
            return boost::shared_ptr<AErrorObject>();
        ZValPointerObject retObject(ZValPointerAny::share(rawRetZVal).cast<ZValPointerObject>());
        if (!retObject)
            return boost::shared_ptr<AErrorObject>();

        static const char codePropertyName[] = "code";
        static const size_t codePropertyNameLen = sizeof(codePropertyName) - 1;

        ZValPointerAny resultCodeZVal(retObject.findPropertyByName<ZValPointerAny>(phpExecEnv,
                                                                                   codePropertyName,
                                                                                   codePropertyNameLen));
        if (!resultCodeZVal)
            return boost::shared_ptr<AErrorObject>();

        long resultCode = 0;
        if (!getResultCode(&resultCode, resultCodeZVal))
            return boost::shared_ptr<AErrorObject>();

        // All codes from 200 to 399 are not errors.
        if ((resultCode >= 200) && (resultCode < 400))
            return boost::shared_ptr<AErrorObject>();

        static const char errorPropertyName[] = "error";
        static const size_t errorPropertyNameLen = sizeof(errorPropertyName) - 1;
        ZValPointerString errorStringZVal(retObject.findPropertyByName<ZValPointerString>(phpExecEnv,
                                                                                          errorPropertyName,
                                                                                          errorPropertyNameLen));
        std::string errorMessage;
        if (errorStringZVal) {
            errorMessage = errorStringZVal.getStringValue();
        }
        else {
            errorMessage = std::string("Error code, ")
                         + boost::lexical_cast<std::string>(resultCode)
                         + ", no further details";
        }
        currentExitCall->incrementErrorCount(1);
        ErrorMonitor* errorMonitor = AG(kernel)->getAgentContext()->getTransactionMonitor()->getErrorMonitor();
        return errorMonitor->reportSyntheticException(phpExecEnv, "drupal_http_request", errorMessage);
    }

    void HTTPRequestInterceptor::emitCorrelationInfo(TransactionContext* context,
                                                     CurrentExitCall* exitCall,
                                                     const PHPExecEnvironment* execEnv) const
    {
        AG(G)->httpCorrelationContext.correlationHeader.assign(
            TransactionCorrelator::getCorrelationHeader(context, exitCall, m_correlationLogger));
        exitCall->addSnapshotDebugProperty("Correlation Header",
                                           AG(G)->httpCorrelationContext.correlationHeader);

        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->addInterceptorForCallable(CallableInfo(STREAM_SOCKET_CLIENT_FUNC),
                                                   &drupal7StreamSocketClientInterceptor);
    }

    void* StreamSocketClientInterceptor::onCallableBegin(const PHPExecEnvironment* execEnv)
    {
        // unregister the interceptor since it's already been hit
        InterceptionEngine* interceptEngine = AG(icept_engine);
        interceptEngine->removeInterceptorForCallable(CallableInfo(STREAM_SOCKET_CLIENT_FUNC),
                                                      this);

        // We need to check if the caller of stream_socket_client is
        // drupal_http_request. If drupal_http_request is
        // not the caller, then we need to bail out of here.
        CallGraph::ExecStackFrame* thisCallFrame =
            execEnv->getAgentGlobals().callGraphCollectionState.getTopStackFrame();
        BOOST_ASSERT(thisCallFrame != NULL);

        CallGraph::ExecStackFrame* callerFrame = thisCallFrame->getCallerFrame();

        // If we don't have a caller frame then drupal_http_request
        // is not calling us.
        if (!callerFrame)
            return NULL;

        const CallGraph::NodeName& callerNodeName = callerFrame->getNodeName();

        // If our caller is a method of a class then it is
        // not drupal_http_request.
        if (callerNodeName.getClassName() != NULL)
            return NULL;

        // Check the method name of our caller to see if it is
        // drupal_http_request.
        BOOST_ASSERT(callerNodeName.getMethodName() != NULL);
        if (strcmp(callerNodeName.getMethodName(), HTTP_REQUEST_SYMBOL) != 0)
            return NULL;

        ZValPointerArray optionsArray(execEnv->getSymbolFromSymbolTable("options").cast<ZValPointerArray>());
        if (!optionsArray)
            return NULL;

        ZValPointerAny headersArray(optionsArray.findEntryByName<ZValPointerAny>("headers", strlen("headers")));
        if (headersArray.getType() != ZValPointer::ZVal_Array)
            return NULL;

        zval* headers = headersArray.get();

        add_assoc_stringl_ex(headers,
                             const_cast<char*>(CorrelationHeader::MAIN_HEADER),
                             CorrelationHeader::MAIN_HEADER_SIZE+1,
                             const_cast<char*>(AG(G)->httpCorrelationContext.correlationHeader.c_str()),
                             AG(G)->httpCorrelationContext.correlationHeader.size() APPD_DUP_TRUE);

        AG(G)->httpCorrelationContext.correlationHeader.clear();
        return NULL;
    }

    void StreamSocketClientInterceptor::onCallableEnd(const PHPExecEnvironment* execEnv, void* state)
    {
        // no-op
    }

}
