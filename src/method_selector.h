#ifndef __method_selector_h
#define __method_selector_h

class AMethodInvocationSelector
{
    protected:
        enum CallableType
        {
            kInitProcedure,
            kConstructor,
            kProcedure,
            kMethod
        };

        /**
            @param  See comments on getThisZVal, getNthParameter,
            and getParamCount.
         */
        AMethodInvocationSelector(CallableType type);

        /**
            Gets the zval for "this".
              - If this is an interceptor for a global
                function, the "this" is in the first parameter.
              - If this is an interceptor for a method of an object,
                then "this is returned by phpExecEnv->getInvokedObject.
              - If this is an interceptor for a mysqli global function
                that creates a link, then "this" is in the return value
                of the function.
         */
        ZValPointerAny getThisZVal(const PHPExecEnvironment* phpExecEnv) const;

        /**
            Gets the nth parameter of the intercepted function or method adjusting for
            the fact that the first argument of mysqli global functions is the
            "this" value.
         */
        ZValPointerAny getNthParameter(const PHPExecEnvironment* phpExecEnv, unsigned n) const;

        /**
            Counts the number of Parameters adjusting for the fact that the first
            argument of mysqli global functions is the "this" value.
         */
        unsigned getParamCount(const PHPExecEnvironment* phpExecEnv) const;

        /**
            @return true if the first parameter of the callable is the "this" value,
            false otherwise.
         */
        bool firstParamIsThis() const;
    private:
        CallableType m_type;

        zval* getThisRawZVal(const PHPExecEnvironment* phpExecEnv) const;
};

inline AMethodInvocationSelector::AMethodInvocationSelector(AMethodInvocationSelector:: CallableType type)
    : m_type(type)
{
}

inline ZValPointerAny AMethodInvocationSelector::getThisZVal(const PHPExecEnvironment* phpExecEnv) const
{
    zval* rawZVal = getThisRawZVal(phpExecEnv);
    if (!rawZVal)
        return ZValPointerAny();
    return ZValPointerAny::share(rawZVal);
}

inline zval* AMethodInvocationSelector::getThisRawZVal(const PHPExecEnvironment* phpExecEnv) const
{
    switch (m_type)
    {
        case AMethodInvocationSelector::kInitProcedure:
            return phpExecEnv->getReturnValue();
        case AMethodInvocationSelector::kConstructor:
        case AMethodInvocationSelector::kMethod:
            return phpExecEnv->getInvokedObject();
        case AMethodInvocationSelector::kProcedure:
        default:
            return phpExecEnv->getParam<zval*, true>(0);
    }
}

inline ZValPointerAny AMethodInvocationSelector::getNthParameter(const PHPExecEnvironment* execEnv,
                                                                 unsigned n) const
{
    if (m_type != AMethodInvocationSelector::kProcedure) {
        BOOST_ASSERT(n < execEnv->getParamCount());
        return execEnv->getParam(n);
    }
    ++n;
    BOOST_ASSERT(n > 0);
    BOOST_ASSERT(n < execEnv->getParamCount());
    return execEnv->getParam(n);
}

inline unsigned AMethodInvocationSelector::getParamCount(const PHPExecEnvironment* execEnv) const
{
    unsigned const paramCount = execEnv->getParamCount();
    if ((m_type != AMethodInvocationSelector::kProcedure) || (!paramCount))
        return paramCount;
    return paramCount - 1;
}

inline bool AMethodInvocationSelector::firstParamIsThis() const
{
    return m_type == AMethodInvocationSelector::kProcedure;
}

#endif // __method_selector_h
